<?php
error_reporting(E_ALL);
/*function video_image($url){
    $image_url = parse_url($url);
    if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
        $array = explode("&", $image_url['query']);
        $imageData=  @file_get_contents("https://img.youtube.com/vi/".substr($array[0], 2)."/mqdefault.jpg");
        if($hash===false){
            return 'NULL';
        }else{
            return "https://img.youtube.com/vi/".substr($array[0], 2)."/mqdefault.jpg";
        }
    } else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
        $hash = unserialize(@file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
        if($hash===false){
            return 'NULL';
        }else{
            return $hash[0]["thumbnail_small"];
        }
    }
}

echo video_image('https://vimeo.com/127758642');*/

/*$details = json_decode(file_get_contents("http://ipinfo.io/172.17.124.186/json"));
print_r($details);*/

//echo mail('deepika123desai@gmail.com','hi','test message');

?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Circles</title>
    <style>
      #map-canvas {
        height: 380px;
        width: 680px;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script>
// This example creates circles on the map, representing
// populations in North America.
var cityCircle;
var radius = 30;

function initialize() {
  // Create the map.
  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(40.714352, -74.005973),
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
    
    var populationOptions = {
      strokeColor: '#5f84f2',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#7eb4f9',
      fillOpacity: 0.4,
      map: map,
      center: new google.maps.LatLng(40.714352, -74.005973),
      radius: radius * 1000

    };
    // Add the circle for this city to the map.
    cityCircle = new google.maps.Circle(populationOptions);
  }


//google.maps.event.addDomListener(window, 'load', initialize);


    </script>
  </head>
  <body onload="initialize();">
    <div id="map-canvas"></div>
  </body>
</html>


