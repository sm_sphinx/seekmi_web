<?php
include("config/configure.php");
include("config/database.php");
db_connect();

$action = $_REQUEST['action'];
$timestamp = $_REQUEST['timestamp'];
$key = $_REQUEST['key'];
header('Content-type: application/json; charset=utf-8');

/* * ************************ */

define('MY_KEY', 'Sph!nxRock');
define('LIMIT', 10);

/* * ************************ */

//Get Services List
if ($action == "getServiceList") {
    $keyword = $_REQUEST['keyword'];    
    $gen_key = md5($timestamp . MY_KEY);
    /*if($gen_key == $key)
    {*/
        if($keyword!=''){
            $sql = "SELECT serviceName,serviceId from tblservices where serviceName like '%".$keyword. "%' and status='Y'";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                while ($row_list = db_fetch_array($query)) {                
                    $arr_list[] = $row_list;
                }
                echo '{"status":"Success","code":"100","message":"OK","serviceList":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Service Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
    /*}
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    } */
}

//Get Option List
if ($action == "getOptionList") {
    $serviceid = $_REQUEST['serviceid'];    
    $gen_key = md5($timestamp . MY_KEY);
   // if($gen_key == $key)
    //{
        if($serviceid!=''){
            $sql = "SELECT * from tblquestions where serviceId = '".$serviceid."' and active='Y'";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                while ($row_list = db_fetch_array($query)) { 
                    $sql_opt = "SELECT optionId,optionText,optionType from tblansweroptions where questionId = '".$row_list['questionId']."' and active='Y'";   
                    $query_opt = db_query($sql_opt);
                    $num_opt = db_num_rows($query_opt);                    
                    if ($num_opt > 0) {
                        while ($row_list_opt = db_fetch_array($query_opt)) {
                          $row_list['option_list'][]=$row_list_opt;
                        }
                    }else{
                        $row_list['option_list']='';
                    }
                    $arr_list[] = $row_list;
                }               
                echo '{"status":"Success","code":"100","message":"OK","optionList":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Option Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
    /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    } */
}

//Add project
if ($action == "addProject") {
    $serviceid = $_REQUEST['serviceid']; 
    $userid = $_REQUEST['userid']; 
    $email = $_REQUEST['email']; 
    $password = $_REQUEST['password'];
    $full_name = $_REQUEST['name'];   
   // $phone = $_REQUEST['phone'];   
    $json_data = $_REQUEST['json_data'];    
    $gen_key = md5($timestamp . MY_KEY);
    $json_data_arr=array();
    $json_data='[
                    {
                        "questionId": "1",
                        "optionId": "1",                   
                        "optionVal": "",
                        "extras": ""                   
                    },
                    {
                        "questionId": "2",
                        "optionId": "3",                    
                        "optionVal": "",
                        "extras": [
                            {
                                "miles": "20"                            
                            }
                         ]
                    },
                    {
                        "questionId": "3",
                        "optionId": [4,5,6],                    
                        "optionVal": "",
                        "extras": ""
                    },
                    {
                        "questionId": "4",
                        "optionId": "",
                        "optionType": "",
                        "optionVal": "testing data",
                        "extras": ""
                    },
                    {
                        "questionId": "5",
                        "optionId": "2",                    
                        "optionVal": "",
                        "extras": [
                            {
                                "date": "2015-04-20",
                                "time": "02:00 AM",
                                "hours": "5"
                            }
                         ]
                    },
                    {
                        "questionId": "6",
                        "optionId": "4",                    
                        "optionVal": "",
                        "extras": [
                            {
                                "other": "other data"
                            }
                         ]
                    } 
              ]';
    $json_data = stripslashes($json_data);
    $json_data_arr = json_decode(str_replace('\"', '', $json_data));
    $today=date('y-m-d H:i:s');  
    
   // print_r($json_data_arr);
   // die;
    //if($gen_key == $key)
    //{
        if($serviceid!='' && $json_data_arr!=''){
           if($userid=='' && $email!='' && $password!=''){
                $sql = "SELECT userId,password from tbluser where email = '".$email."' and status='Y'";   
                $query = db_query($sql);
                $num = db_num_rows($query);
                if($num > 0){
                  $row_us = db_fetch_array($query);
                  if($row_us['password']==md5($password)){
                     $userid = $row_us['userId'];
                  }else{
                     $userid ='';
                  }
                }else{
                  $sql_data_array['email'] = $email;
                  //$sql_data_array['phone'] = $phone;
                  $full_nm_arr=array();
                  if($full_name!=''){
                    $full_nm_arr=explode(' ',$full_name);
                  }                                
                  $sql_data_array=array();
                  $sql_data_array['firstname'] = $full_nm_arr[0];
                  $sql_data_array['lastname'] = $full_nm_arr[1];
                  $sql_data_array['userType'] = 'user';
                  $sql_data_array['password'] = md5($password);
                  $sql_data_array['createdDate'] = $today;
                  $sql_data_array['modifiedDate'] = $today;
                  $sql_data_array['status'] = 'Y';
                  db_perform('tbluser', $sql_data_array);                  
                  $userid = db_insert_id();
                  
                  $not_data_array=array();
                  $not_data_array['userId'] = $userid;
                  $not_data_array['createdDate'] = $today;
                  $not_data_array['modifiedDate'] = $today;
                  db_perform('tblemailnotifications', $not_data_array);
                }
           }
           if($userid!=''){
                $proj_data_array=array();
                $proj_data_array['serviceId'] = $serviceid;
                $proj_data_array['userId'] = $userid;  
                $proj_data_array['status'] = 'In Progress';
                $proj_data_array['createdDate'] = $today;
                $proj_data_array['modifiedDate'] = $today;                
                db_perform('tbluserprojects', $proj_data_array);                  
                $projectid = db_insert_id();
                foreach($json_data_arr as $json_data_row) {
                    $optionId=$json_data_row->optionId;
                    if(is_array($optionId)){
                        for($i=0;$i<count($optionId);$i++)
                        {
                            if($optionId[$i]!=''){
                                $dt_data_array=array();
                                $dt_data_array['projectId'] = $projectid;
                                $dt_data_array['questionId'] = $json_data_row->questionId;
                                $dt_data_array['optionId'] = $optionId[$i];
                                $dt_data_array['optionVal'] = $json_data_row->optionVal;
                                if($json_data_row->extras!=''){
                                $dt_data_array['extras'] = json_encode($json_data_row->extras);                  
                                }
                                db_perform('tblprojectdetails', $dt_data_array); 
                            }
                        }
                    }else{
                        $dt_data_array=array();
                        $dt_data_array['projectId'] = $projectid;
                        $dt_data_array['questionId'] = $json_data_row->questionId;
                        $dt_data_array['optionId'] = $optionId;
                        $dt_data_array['optionVal'] = $json_data_row->optionVal;
                        if($json_data_row->extras!=''){
                        $dt_data_array['extras'] = json_encode($json_data_row->extras);                  
                        }
                        db_perform('tblprojectdetails', $dt_data_array);
                    }
                }
               echo '{"status":"Success","code":"100","message":"OK"}';
           }else{
              echo '{"status":"Error","code":"101","message":"Incorrect Email/Password"}'; 
           }
          
            
        }else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
    /*}
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    } */
}

//Get Project List
if ($action == "getProjectList") {
    $userid = $_REQUEST['userid'];    
    $gen_key = md5($userid . $timestamp . MY_KEY);
    //if($gen_key == $key)
    //{
        if($userid!=''){
            $sql = "SELECT a.projectId,a.serviceId,b.serviceName,a.status,a.createdDate from tbluserprojects as a join tblservices as b on a.serviceId=b.serviceId where a.userId = '".$userid."'";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                while ($row_list = db_fetch_array($query)) { 
                    $sql_pros = "SELECT b.businessName,b.businessLogo from tblqouterequest as a join tbluserprovider as b on a.prosId=b.userId where a.projectId = '".$row_list['projectId']."'";   
                    $query_pros = db_query($sql_pros);
                    $num_pros = db_num_rows($query_pros);                    
                    if ($num_pros > 0) {
                        while ($row_list_pros = db_fetch_array($query_pros)) {
                          $row_list['pros_list'][]=$row_list_pros;
                        }
                    }else{
                        $row_list['pros_list']='';
                    }
                    $row_list['pros_list_count']=$num_pros;
                    $arr_list[] = $row_list;
                }               
                echo '{"status":"Success","code":"100","message":"OK","projectList":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Project Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
   /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    }*/ 
}
//Get Project List
if ($action == "getProjectDetails") {
    $projectid = $_REQUEST['projectid'];    
    $gen_key = md5($projectid . $timestamp . MY_KEY);
    //if($gen_key == $key)
    //{
        if($projectid!=''){
            $sql = "SELECT a.projectId,a.serviceId,b.serviceName,a.status,a.createdDate from tbluserprojects as a join tblservices as b on a.serviceId=b.serviceId where a.projectId = '".$projectid."'";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                $row_list = db_fetch_array($query);
                $sql_pros = "SELECT b.businessName,b.businessLogo,a.quotePrice,a.priceType,a.quoteMessage,a.addedOn,a.prosId as professionalId from tblqouterequest as a join tbluserprovider as b on a.prosId=b.userId where a.projectId = '".$row_list['projectId']."'";   
                $query_pros = db_query($sql_pros);
                $num_pros = db_num_rows($query_pros);                    
                if ($num_pros > 0) {
                    while ($row_list_pros = db_fetch_array($query_pros)) {
                      $row_list['pros_list'][]=$row_list_pros;
                    }
                }else{
                    $row_list['pros_list']='';
                }
                $row_list['pros_list_count']=$num_pros;
                $arr_list[] = $row_list;                               
                echo '{"status":"Success","code":"100","message":"OK","projectDetails":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Project Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
   /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    }*/ 
}
db_close();
?>