<?php
include("config/configure.php");
include("config/database.php");
db_connect();

$action = $_REQUEST['action'];
$timestamp = $_REQUEST['timestamp'];
$key = $_REQUEST['key'];
header('Content-type: application/json; charset=utf-8');

/* * ************************ */

define('MY_KEY', 'Sph!nxRock');
define('LIMIT', 10);

/* * ************************ */
//Login
if ($action == "loginUser") {
    $uemail = $_REQUEST['email'];
    $gen_key = md5($uemail . $timestamp . MY_KEY);
    if($gen_key == $key) 
    {	    
    $pass = $_REQUEST['password'];

    if ($uemail != '' && $pass != '') {
        $sql = "SELECT userId,userPhoto,firstname,lastname,facebookProfileId from tbluser where email='" . $uemail . "' and password='" . md5($pass) . "'";
        $query = db_query($sql);
        $num = db_num_rows($query);
        if ($num == 0) {
            echo '{"status":"Error","code":"101","message":"Invalid Email/Password."}';
        } else {
            $row = db_fetch_array($query);
            $username=$row['firstname']." ".$row['lastname'];
            if($row['facebookProfileId']!=''){
                $user_photo=$row['userPhoto'];
            }else{
                if($row['userPhoto']!=''){
                    $user_photo=WEB_ADDRESS.'/user_images/'.$row['userPhoto'];
                }else{
                   $user_photo=''; 
                }
            }
            $sql_notf = "SELECT notificationId from tblemailnotifications where userId='" . $row['userId'] . "'";
            $query_notf = db_query($sql_notf);
            $num_notf = db_num_rows($query_notf);
            if($num_notf == 0){
                $sql_data_array='';
                $sql_data_array['userId'] = $row['userId'];
                $sql_data_array['updates'] = 'Y';
                $sql_data_array['tips'] = 'Y';
                $sql_data_array['offers'] = 'Y';
                $sql_data_array['userFeedback'] = 'Y';
                $sql_data_array['createdDate'] = $today;
                $sql_data_array['modifiedDate'] = $today;                
                db_perform('tblemailnotifications', $sql_data_array);  
            }
            echo '{"status":"Success","code":"100","message":"OK","userId":"' . $row['userId'] . '","userName":"'.$username.'","userPhoto":"'.$user_photo.'"}';
        }
    } else {
        echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
    }
   }
   else
   {
      echo '{"status":"Error","code":"103","message":"Invalid Key"}';
   } 
}

//Forgot Password
if ($action == "forgotPassword") {
    
    $email = $_REQUEST['email'];
    $gen_key = md5($email . $timestamp . MY_KEY);
    //if($gen_key == $key)
    //{	

    if ($email != '') {
        $qry_email = db_query("select userId,firstname from tbluser where email='" . $email . "' and status='Y'");
        $res_email = db_fetch_array($qry_email);
        if ($res_email['userId'] == '') {
            echo '{"status": "Error" ,"message":"Email not exists","code":"101"}'; // if database error encounter
        } else {
            $characters = array(
                "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M",
                "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                "1", "2", "3", "4", "5", "6", "7", "8", "9");

            //make an "empty container" or array for our keys
            $keys = array();
            //first count of $keys is empty so "1", remaining count is 1-6 = total 7 times
            while (count($keys) < 7) {
                //"0" because we use this to FIND ARRAY KEYS which has a 0 value
                //"-1" because were only concerned of number of keys which is 32 not 33
                //count($characters) = 33
                $x = mt_rand(0, count($characters) - 1);
                if (!in_array($x, $keys)) {
                    $keys[] = $x;
                }
            }

            foreach ($keys as $key) {
                $random_chars .= $characters[$key];
            }

            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: SeekMi <support@seekmi.com>' . "\r\n";
            $message = "Hello " . $res_email['firstname'] . ",<br/><br/>
            As you requested, your SeekMi password has been reset. To set a new password, click on the link below.<br/><br/>	
            <a href='" . WEB_ADDRESS . "/profile/password/" . md5($random_chars) . "'>" . WEB_ADDRESS . "/profile/password/" . md5($random_chars) . "</a><br/>
            If you didn't request this password reset, let us know right away at <a href='mailto:support@seekmi.com'>support@seekmi.com</a>    
            <br/><br/>
            Thank you,<br/><br/>	
            SeekMi Support<br/>
            <a href='".WEB_ADDRESS."'>SeekMi</a>";
            $subject = "Reset Password - SeekMi";

            if (mail($email, $subject, $message, $headers)) {
                $query = db_query("UPDATE tbluser SET  `modifiedDate` =  '" . date('Y-m-d H:i:s') . "', `resetPasswordToken` = '" . md5($random_chars) . "' WHERE userId = '" . $res_email['userId'] . "'");
                echo '{"status":"Success","code":"100", "message":"OK"}';
            } else {
                echo '{"status": "Error" ,"message":"Can not send the email, please try again later.","code":"104"}';
            }
        }
      } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
      }
     /*}
     else
     {
      echo '{"status":"Error","code":"103","message":"Invalid Key"}';
     }*/ 
}

//Get Services List
if ($action == "getServiceList") {
    $keyword = $_REQUEST['keyword'];    
    $gen_key = md5($timestamp . MY_KEY);
    /*if($gen_key == $key)
    {*/
        //if($keyword!=''){
         if($keyword!=''){
            $sql = "SELECT subserviceName as serviceName,subserviceId as serviceId from tblsubservices where subserviceName like '%".$keyword. "%' and status='Y' group by subserviceName order by subserviceName asc";   
         }else{
            $sql = "SELECT subserviceName as serviceName,subserviceId as serviceId from tblsubservices where status='Y' group by subserviceName order by subserviceName asc";    
         }
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                while ($row_list = db_fetch_array($query)) {                
                    $arr_list[] = $row_list;
                }
                echo '{"status":"Success","code":"100","message":"OK","serviceList":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Service Found"}';
            }
        /*} else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }*/
    /*}
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    } */
}

//Get Option List
if ($action == "getOptionList") {
    $serviceid = $_REQUEST['serviceid'];    
    $gen_key = md5($timestamp . MY_KEY);
   // if($gen_key == $key)
    //{
        if($serviceid!=''){
            $sql = "SELECT * from tblquestions where serviceId = '".$serviceid."' and active='Y'";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                while ($row_list = db_fetch_array($query)) { 
                    $sql_opt = "SELECT optionId,optionText,optionType from tblansweroptions where questionId = '".$row_list['questionId']."' and active='Y'";   
                    $query_opt = db_query($sql_opt);
                    $num_opt = db_num_rows($query_opt);                    
                    if ($num_opt > 0) {
                        while ($row_list_opt = db_fetch_array($query_opt)) {
                          $row_list['option_list'][]=$row_list_opt;
                        }
                    }else{
                        $row_list['option_list']=array();
                    }
                    $arr_list[] = $row_list;
                }               
                echo '{"status":"Success","code":"100","message":"OK","optionList":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Option Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
    /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    } */
}

//Add project
if ($action == "addProject") {
    $serviceid = $_REQUEST['serviceid']; 
    $userid = $_REQUEST['userid']; 
    $email = $_REQUEST['email']; 
    $password = $_REQUEST['password'];
    $full_name = $_REQUEST['name'];   
   // $phone = $_REQUEST['phone'];   
    $json_data = $_REQUEST['json_data']; 
    $provId = $_REQUEST['provId'];
    $cityId = $_REQUEST['cityId'];
    $districtId = $_REQUEST['districtId'];
    $gen_key = md5($timestamp . MY_KEY);
    $json_data_arr=array();
    /*$json_data='[
                    {
                        "questionId": "1",
                        "optionId": "1",                   
                        "optionVal": "",
                        "extras": ""                   
                    },
                    {
                        "questionId": "2",
                        "optionId": "3",                    
                        "optionVal": "",
                        "extras": [
                            {
                                "miles": "20"                            
                            }
                         ]
                    },
                    {
                        "questionId": "3",
                        "optionId": [4,5,6],                    
                        "optionVal": "",
                        "extras": ""
                    },
                    {
                        "questionId": "4",
                        "optionId": "",
                        "optionType": "",
                        "optionVal": "testing data",
                        "extras": ""
                    },
                    {
                        "questionId": "5",
                        "optionId": "2",                    
                        "optionVal": "",
                        "extras": [
                            {
                                "date": "2015-04-20",
                                "time": "02:00 AM",
                                "hours": "5"
                            }
                         ]
                    },
                    {
                        "questionId": "6",
                        "optionId": "4",                    
                        "optionVal": "",
                        "extras": ""
                    } 
              ]';*/
    $json_data = stripslashes($json_data);
    $json_data_arr = json_decode(str_replace('\"', '', $json_data));
    $today=date('y-m-d H:i:s');  
    
   // print_r($json_data_arr);
   // die;
    //if($gen_key == $key)
    //{
        if($serviceid!='' && $json_data_arr!=''){
           if($userid=='' && $email!='' && $password!=''){
                $sql = "SELECT userId,password,userPhoto,facebookProfileId,firstname,lastname from tbluser where email = '".$email."' and status='Y'";   
                $query = db_query($sql);
                $num = db_num_rows($query);
                if($num > 0){
                  $row_us = db_fetch_array($query);
                  if($row_us['password']==md5($password)){
                     $userid = $row_us['userId'];
                  }else{
                     $userid ='';
                  }                  
                }else{
                  
                  //$sql_data_array['phone'] = $phone;
                  $full_nm_arr=array();
                  if($full_name!=''){
                    $full_nm_arr=explode(' ',$full_name);
                  }                                
                  $sql_data_array=array();
                  $sql_data_array['email'] = $email;
                  $sql_data_array['firstname'] = $full_nm_arr[0];
                  $sql_data_array['lastname'] = $full_nm_arr[1];
                  $sql_data_array['userType'] = 'user';
                  $sql_data_array['password'] = md5($password);
                  $sql_data_array['createdDate'] = $today;
                  $sql_data_array['modifiedDate'] = $today;
                  $sql_data_array['status'] = 'Y';
                  db_perform('tbluser', $sql_data_array);                  
                  $userid = db_insert_id();
                  
                  $not_data_array=array();
                  $not_data_array['userId'] = $userid;
                  $not_data_array['createdDate'] = $today;
                  $not_data_array['modifiedDate'] = $today;
                  db_perform('tblemailnotifications', $not_data_array);                  
                }
           }
           if($userid!=''){
                $sql11 = "SELECT userPhoto,facebookProfileId,firstname,lastname from tbluser where userId = '".$userid."'";   
                $query11 = db_query($sql11);
                $row_us11 = db_fetch_array($query11);
                $username=$row_us11['firstname']." ".$row_us11['lastname'];
                if($row_us11['facebookProfileId']!=''){
                    $user_photo=$row_us11['userPhoto'];
                }else{
                    if($row_us11['userPhoto']!=''){
                       $user_photo=WEB_ADDRESS.'/user_images/'.$row_us11['userPhoto'];
                    }else{
                       $user_photo=''; 
                    }
                }
               
                $sql2 = "SELECT provName,cityName,districtName from tblprovincelist as a, tblcitylist as b,tbldistrictlist as c where a.provId='".$provId."' and b.cityId='".$cityId."' and c.districtId='".$districtId."'";   
                $query2 = db_query($sql2);
                $row_us2 = db_fetch_array($query2);
                $address=$row_us2['cityName'].",".$row_us2['districtName'].",".$row_us2['provName'];
                $latlongval=getLnt($address);
                if($latlongval!=''){
                    $latitude=$latlongval['lat'];
                    $longitude=$latlongval['lng'];
                }else{
                    $latitude='';
                    $longitude='';
                }
                $proj_data_array=array();
                $proj_data_array['serviceId'] = $serviceid;
                $proj_data_array['userId'] = $userid;  
                $proj_data_array['provinceId'] = $provId;
                $proj_data_array['cityId'] = $cityId;
                $proj_data_array['districtId'] = $districtId;
                $proj_data_array['status'] = 'Pending';
                $proj_data_array['latitude'] = $latitude;
                $proj_data_array['longitude'] = $longitude;
                $proj_data_array['createdDate'] = $today;
                $proj_data_array['modifiedDate'] = $today;                
                db_perform('tbluserprojects', $proj_data_array);                  
                $projectid = db_insert_id();
                foreach($json_data_arr as $json_data_row) {
                    $optionId=$json_data_row->optionId;
                    //if(is_array($optionId)){
                    if(strstr($optionId, ',')){
                        $option_arr='';
                        $option_arr=explode(',',$optionId);
                        for($i=0;$i<count($option_arr);$i++)
                        {
                            if($option_arr[$i]!=''){
                                $dt_data_array=array();
                                $dt_data_array['projectId'] = $projectid;
                                $dt_data_array['questionId'] = $json_data_row->questionId;
                                $dt_data_array['optionId'] = $option_arr[$i];
                                if($json_data_row->otherOptionId==$option_arr[$i]){
                                    $dt_data_array['optionVal'] = $json_data_row->optionValue;
                                }else{
                                   $dt_data_array['optionVal'] =''; 
                                }
                                if($json_data_row->extras!=''){
                                $dt_data_array['extras'] = json_encode($json_data_row->extras);                  
                                }
                                db_perform('tblprojectdetails', $dt_data_array); 
                            }
                        }
                    }else{
                        $dt_data_array=array();
                        $dt_data_array['projectId'] = $projectid;
                        $dt_data_array['questionId'] = $json_data_row->questionId;
                        $dt_data_array['optionId'] = $optionId;
                        $dt_data_array['optionVal'] = $json_data_row->optionValue;
                        if($json_data_row->extras!=''){
                        $dt_data_array['extras'] = json_encode($json_data_row->extras);                  
                        }
                        db_perform('tblprojectdetails', $dt_data_array);
                    }
                }
                 /* send project request */
                 if($latitude!='' && $longitude!=''){
                    $sql_setting = "SELECT settingVal from tblsettings where settingName='distanceSetting'";   
                    $query_setting = db_query($sql_setting);
                    $row_setting = db_fetch_array($query_setting);
                    $distancekm=$row_setting['settingVal'];                    
                    $sql_expert = "SELECT a.userId,b.firstname,b.lastname,b.email,( 3959 * acos( cos( radians(".$latitude.")) * cos( radians(a.latitude)) * cos(radians(a.longitude) - radians(".$longitude.")) + sin(radians(".$latitude.")) * sin(radians(a.latitude)))) AS miles "
                . "FROM tbluserprovider as a join tbluser as b on(a.userId=b.userId) left join tbluserservices as c on(a.userId=c.userId) "
                . "where c.serviceId='".$serviceid."' and b.status='Y' group by a.userId HAVING miles <= '".$distancekm."' ORDER BY a.rating desc";   
                    $query_expert = db_query($sql_expert); 
                    $num_expert = db_num_rows($query_expert);     
                    if($num_expert > 0){
                       	
                        while($expert_row = db_fetch_array($query_expert)){
                           $expert_arr=array();
                           $expert_arr=array('prosId'=>$expert_row['userId'],'projectId'=>$projectid,'status'=>'Pending','addedOn'=>$today,'modifiedOn'=>$today);                           
                           db_perform('tblquoterequest', $expert_arr);     
                           /* send mail to experts */                           	
//                            $this->email->to($expert_row->email); 
//                            $this->email->from('support@seekmi.com', 'SeekMi');
//                            $this->email->subject('Work Request at SeekMi');
//                            $htmlMessage='<table><tr><td><b>Hello,</b></td></tr></table>';
//                            $htmlMessage .='<p>You have one project request as per your profile at SeekMi .<br/>Please click the following link to proceed further step:'.'</p>';
//                            $htmlMessage .="<table><tr><td><a href='".$this->config->config['base_url']."profile/requests'>".$this->config->config['base_url']."profile/requests</a></td></tr>";
//
//                            $htmlMessage .="<tr><td>&nbsp;</td></tr><tr><td>Thanks again for being a valued customer.</td></tr><tr><td>&nbsp;</td></tr>";
//                            $htmlMessage .="<tr><td>Thanks,</td></tr>		
//                            <tr><td>SeekMi Support</td></tr>
//                            <tr><td><a href='".$this->config->config['base_url']."'>SeekMi</a></td></tr>";
//                            $htmlMessage .='<tr><td></td></tr></table>';
//                            $this->email->message($htmlMessage);
//                            $this->email->send();
                           /* send mail to experts */
                       }
                   }
                 } 
              /* send project request */  
               echo '{"status":"Success","code":"100","message":"OK","userId":"'.$userid.'","userName":"'.$username.'","userPhoto":"'.$user_photo.'"}';
           }else{
              echo '{"status":"Error","code":"101","message":"Incorrect Email/Password"}'; 
           }
        }else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
    /*}
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    } */
}

//Get Project List
if ($action == "getProjectList") {
    $userid = $_REQUEST['userid'];    
    $gen_key = md5($userid . $timestamp . MY_KEY);
    //if($gen_key == $key)
    //{
        if($userid!=''){
            $sql = "SELECT a.projectId,a.serviceId,b.subserviceName as serviceName,a.status,a.createdDate from tbluserprojects as a join tblsubservices as b on a.serviceId=b.subserviceId where a.userId = '".$userid."'";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                while ($row_list = db_fetch_array($query)) { 
                    $sql_pros = "SELECT b.businessName,a.status,a.requestId,c.userPhoto from tblquoterequest as a join tbluserprovider as b on a.prosId=b.userId join tbluser as c on c.userId=b.userId where a.projectId = '".$row_list['projectId']."' and a.status!='Pending'";   
                    $query_pros = db_query($sql_pros);
                    $num_pros = db_num_rows($query_pros);                    
                    if ($num_pros > 0) {
                        while ($row_list_pros = db_fetch_array($query_pros)) {
                            if($row_list_pros['userPhoto']!=''){
                               $row_list_pros['userPhoto']=WEB_ADDRESS.'/user_images/'.$row_list_pros['userPhoto'];
                            }else{
                               $row_list_pros['userPhoto']=WEB_ADDRESS.'/images/100x100.png'; 
                            }
                            $sql_msg = "SELECT messageId from tblquotemessage where quoteId = '".$row_list_pros['requestId']."'";   
                            $query_msg = db_query($sql_msg);
                            $num_msg = db_num_rows($query_msg); 
                            $row_list_pros['unread_msg_count']=$num_msg;
                            $row_list['pros_list'][]=$row_list_pros;
                        }
                    }else{
                        $row_list['pros_list']=array();
                    }
                    $row_list['pros_list_count']=$num_pros;
                    $arr_list[] = $row_list;
                }               
                echo '{"status":"Success","code":"100","message":"OK","projectList":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Project Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
   /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    }*/ 
}

//Get Project List
if ($action == "getProjectDetails") {
    $projectid = $_REQUEST['projectid'];    
    $gen_key = md5($projectid . $timestamp . MY_KEY);
    //if($gen_key == $key)
    //{
        if($projectid!=''){
            $sql = "SELECT a.projectId,a.serviceId,b.subserviceName as serviceName,a.status,a.createdDate from tbluserprojects as a join tblsubservices as b on a.serviceId=b.subserviceId where a.projectId = '".$projectid."'";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                $row_list = db_fetch_array($query);
                $sql_pros = "SELECT b.businessName,a.quotePrice,a.priceType,a.quoteMessage,a.addedOn,a.prosId as professionalId,b.reviewCount,b.rating,a.status,a.requestId,b.providerId,c.userPhoto from tblquoterequest as a join tbluserprovider as b on a.prosId=b.userId join tbluser as c on c.userId=b.userId where a.projectId = '".$row_list['projectId']."' and a.status!='Pending'";   
                $query_pros = db_query($sql_pros);
                $num_pros = db_num_rows($query_pros);                    
                if ($num_pros > 0) {
                    while ($row_list_pros = db_fetch_array($query_pros)) {
                        
                        if($row_list_pros['userPhoto']!=''){
                            $row_list_pros['userPhoto']=WEB_ADDRESS.'/user_images/'.$row_list_pros['userPhoto'];
                         }else{
                            $row_list_pros['userPhoto']=WEB_ADDRESS.'/images/100x100.png'; 
                         }
                        $sql_msg = "SELECT messageId from tblquotemessage where quoteId = '".$row_list_pros['requestId']."'";   
                        $query_msg = db_query($sql_msg);
                        $num_msg = db_num_rows($query_msg); 
                        $row_list_pros['unread_msg_count']=$num_msg;
                      $row_list['pros_list'][]=$row_list_pros;
                    }
                }else{
                    $row_list['pros_list']=array();
                }
                $row_list['pros_list_count']=$num_pros;
                $arr_list[] = $row_list;                               
                echo '{"status":"Success","code":"100","message":"OK","projectDetails":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Project Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
   /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    }*/ 
}

//Add Review
if ($action == "addReview") {
    $rating = $_REQUEST['rating'];
    $userid = $_REQUEST['userId'];
    $prosId = $_REQUEST['prosId'];
    $reviewText = $_REQUEST['reviewText'];
      
    $gen_key = md5($userid . $timestamp . MY_KEY);
    if($gen_key == $key)
    {   
        $tday = date('Y-m-d H:i:s');
        if ($userid!='') {
            $sql = "SELECT reviewId from tblbusinessreview where userId='".$userid."' and prosId='".$prosId."'";
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num == 0) {                
                $rev_array = array("userId" => $userid,
                    "rating" => $rating,
                    "reviewContent" => $reviewText,
                    "prosId" => $prosId,              
                    "createdOn" => $tday                
                );
                db_perform('tblbusinessreview', $rev_array);
                $reviewid = db_insert_id();
                $sql_rev = "SELECT rating from tblbusinessreview where prosId='".$prosId."'";
                $query_rev = db_query($sql_rev);
                $num_rev = db_num_rows($query_rev);
                if($num_rev > 0){
                    $ratingCnt=0;
                }else{
                    $rating_sum=0;
                    while($row_list_rev = db_fetch_array($query_rev)) {
                        $rating_sum=$rating_sum+$row_list_rev['rating'];
                    }
                    $ratingCnt=$rating_sum/$num_rev;
                }
                db_query("update tbluserprovider set reviewCount=reviewCount+1, rating='".$ratingCnt."' where userId='".$prosId."'");
                echo '{"status":"Success","code":"100","message":"OK"}';
            } else {
                echo '{"status":"Error","code":"101","message":"Review already added"}';           
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
    }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    } 
}

//Get Quote Details
if ($action == "getQuoteDetails") {
    $projectId = $_REQUEST['projectId'];   
    $prosId = $_REQUEST['prosId'];   
    $gen_key = md5($prosId . $timestamp . MY_KEY);
   // if($gen_key == $key)
    //{
        if($projectId!='' && $prosId!='' && $projectId!='0' && $prosId!='0'){
            $sql = "SELECT b.businessName,a.quotePrice,a.priceType,a.quoteMessage,a.addedOn,a.prosId as professionalId,b.reviewCount,b.rating,a.status,a.requestId,b.providerId,c.userPhoto from tblquoterequest as a join tbluserprovider as b on a.prosId=b.userId join tbluser as c on b.userId=c.userId where a.projectId = '".$projectId."' and a.prosId='".$prosId."'";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();    
                $row_list_pros = db_fetch_array($query);
                    if($row_list_pros['userPhoto']!=''){
                        $row_list_pros['userPhoto']=WEB_ADDRESS.'/user_images/'.$row_list_pros['userPhoto'];
                     }else{
                        $row_list_pros['userPhoto']=''; 
                     }
                    $sql_msg = "SELECT a.message,concat(b.firstname,b.lastname) as messageFrom, concat(c.firstname,c.lastname) as messageTo, a.readFlag,a.createdOn as quoteDate,a.messageId,b.userPhoto from tblquotemessage as a, tbluser as b ,tbluser as c where a.messageFrom=b.userId and a.messageTo=c.userId and quoteId = '".$row_list_pros['requestId']."'";   
                    $query_msg = db_query($sql_msg);
                    $num_msg = db_num_rows($query_msg); 
                    if($num_msg > 0){
                        while($row_list_msg = db_fetch_array($query_msg)) {
                            if($row_list_msg['userPhoto']!=''){
                                $row_list_msg['userPhoto']=WEB_ADDRESS.'/user_images/'.$row_list_msg['userPhoto'];
                            }else{
                                $row_list_msg['userPhoto']=''; 
                            }
                            $row_list_msg['quoteDate']=get_timeago(strtotime($row_list_msg['quoteDate']));
                            $row_list_pros['msg_list'][]=$row_list_msg;
                        }
                    }else{
                       $row_list_pros['msg_list']=array(); 
                    }
                $arr_list[] = $row_list_pros;                               
                echo '{"status":"Success","code":"100","message":"OK","quoteDetails":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Quote Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
   /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    }*/ 
}

//Get Profile Details
if ($action == "getProfileDetails") {
    $projectId = $_REQUEST['projectId'];   
    $prosId = $_REQUEST['prosId'];   
    $gen_key = md5($prosId . $timestamp . MY_KEY);
   // if($gen_key == $key)
    //{
        if($projectId!='' && $prosId!='' && $projectId!='0' && $prosId!='0'){
            $sql = "SELECT b.businessName,b.reviewCount,b.rating,b.providerId,b.bioInfo,b.foundingYear,b.employeeCount,b.website,a.streetAddress,c.cityName,d.provName,a.zipcode,a.phone,a.userPhoto from tbluser as a join tbluserprovider as b on a.userId=b.userId left join tblcitylist as c on (a.city=c.cityId) left join tblprovincelist as d on (a.province=d.provId) where a.userId='".$prosId."'";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();    
                $row_list_pros = db_fetch_array($query);
                if($row_list_pros['foundingYear']!=''){
                   $currentYear=date('Y');
                   if($currentYear > $row_list_pros['foundingYear']){
                       $row_list_pros['foundingYear']=$currentYear-$row_list_pros['foundingYear'];
                   }else{
                      $row_list_pros['foundingYear']='';
                   }
                }else{
                   $row_list_pros['foundingYear']=''; 
                }
                if($row_list_pros['userPhoto']!=''){
                    $row_list_pros['userPhoto']=WEB_ADDRESS.'/user_images/'.$row_list_pros['userPhoto'];
                }else{
                    $row_list_pros['userPhoto']=''; 
                }
                $sql_msg = "SELECT b.questionText,a.answerText from tblprovideranswers as a join tblprofilequestions as b on (a.questionId=b.queId) where userId = '".$prosId."'";   
                $query_msg = db_query($sql_msg);
                $num_msg = db_num_rows($query_msg); 
                if($num_msg > 0){
                    while ($row_list_msg = db_fetch_array($query_msg)) {
                        $row_list_pros['qa_list'][]=$row_list_msg;
                    }
                }else{
                   $row_list_pros['qa_list']=array(); 
                }
                
                $sql_rev = "SELECT a.reviewContent,a.rating,a.createdOn as reviewDate,concat(b.firstname,b.lastname) as username,b.userPhoto from tblbusinessreview as a join tbluser as b on (a.userId=b.userId) where a.prosId = '".$prosId."'";   
                $query_rev = db_query($sql_rev);
                $num_rev = db_num_rows($query_rev); 
                if($num_rev > 0){
                    while ($row_list_rev = db_fetch_array($query_rev)) {
                        
                        if($row_list_rev['userPhoto']!=''){
                            $row_list_rev['userPhoto']=WEB_ADDRESS.'/user_images/'.$row_list_rev['userPhoto'];
                        }else{
                            $row_list_rev['userPhoto']=''; 
                        }
                        $row_list_rev['reviewDate']=date('d M,Y',strtotime($row_list_rev['reviewDate']));
                        $row_list_pros['review_list'][]=$row_list_rev;
                    }
                }else{
                   $row_list_pros['review_list']=array(); 
                }
                
                $sql_media = "SELECT mediaType,mediaPath,mediaTitle from tblbusinessmedia where userId = '".$prosId."'";   
                $query_media = db_query($sql_media);
                $num_media = db_num_rows($query_media); 
                if($num_media > 0){
                    while ($row_list_media = db_fetch_array($query_media)) {                        
                        if($row_list_media['mediaPath']!=''){
                            $row_list_media['mediaPath']=WEB_ADDRESS.'/business_images/'.$row_list_media['mediaPath'];
                        }else{
                            $row_list_media['mediaPath']=''; 
                        }
                        $row_list_pros['media_list'][]=$row_list_media;
                    }
                }else{
                   $row_list_pros['media_list']=array(); 
                }
                
                $arr_list[] = $row_list_pros;                               
                echo '{"status":"Success","code":"100","message":"OK","profileDetails":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No Profile Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
   /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    }*/ 
}

//Add Quote Message
if ($action == "sendQuoteMessage") {
    $quoteId = $_REQUEST['requestId'];
    $messageFrom = $_REQUEST['messageFrom'];
    $messageTo = $_REQUEST['messageTo'];
    $message = $_REQUEST['message'];
      
    $gen_key = md5($timestamp . MY_KEY);
    if($gen_key == $key)
    {   
        $tday = date('Y-m-d H:i:s');
        if ($messageFrom!='' && $messageTo!='' && $message!='' && $quoteId!='') {
            /*$sql = "SELECT reviewId from tblbusinessreview where userId='".$userid."' and prosId='".$prosId."'";
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num == 0) {*/                
                $rev_array = array("quoteId" => $quoteId,
                    "messageFrom" => $messageFrom,
                    "messageTo" => $messageTo,
                    "message" => $message,  
                    "readFlag"=>'N',
                    "createdOn" => $tday                
                );
                db_perform('tblquotemessage', $rev_array);
                //$reviewid = db_insert_id();
                echo '{"status":"Success","code":"100","message":"OK"}';
            /*} else {
                echo '{"status":"Error","code":"101","message":"Review already added"}';           
            }*/
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
    }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    } 
}

//Get Province List
if ($action == "getProvinceList") {    
    $gen_key = md5($timestamp . MY_KEY);
    //if($gen_key == $key)
    //{
        
        $sql = "SELECT provId,provName from tblprovincelist order by provName asc";   
        $query = db_query($sql);
        $num = db_num_rows($query);
        if ($num > 0) {
            $arr_list = array();
            while ($row_list = db_fetch_array($query)) { 
                $arr_list[] = $row_list;
            }               
            echo '{"status":"Success","code":"100","message":"OK","provinceList":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
        } else {
            echo '{"status":"Error","code":"101","message":"No Province Found"}';
        }
        
   /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    }*/ 
}

//Get City List
if ($action == "getCityList") {  
    $provId=$_REQUEST['provId'];
    $gen_key = md5($timestamp . MY_KEY);
    //if($gen_key == $key)
    //{
        if($provId!=''){
            $sql = "SELECT cityId,cityName from tblcitylist where provinceId='".$provId."' order by cityName asc";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                while ($row_list = db_fetch_array($query)) { 
                    $arr_list[] = $row_list;
                }               
                echo '{"status":"Success","code":"100","message":"OK","cityList":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No City Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
   /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    }*/ 
}

//Get City List
if ($action == "getDistrictList") {  
    $cityId=$_REQUEST['cityId'];
    $gen_key = md5($timestamp . MY_KEY);
    //if($gen_key == $key)
    //{
        if($cityId!=''){
            $sql = "SELECT districtId,districtName from tbldistrictlist where cityId='".$cityId."' order by districtName asc";   
            $query = db_query($sql);
            $num = db_num_rows($query);
            if ($num > 0) {
                $arr_list = array();
                while ($row_list = db_fetch_array($query)) { 
                    $arr_list[] = $row_list;
                }               
                echo '{"status":"Success","code":"100","message":"OK","districtList":' . json_encode($arr_list, JSON_ERROR_UTF8 | JSON_HEX_TAG) . '}';
            } else {
                echo '{"status":"Error","code":"101","message":"No District Found"}';
            }
        } else {
            echo '{"status":"Error","code":"102","message":"Insufficient Parameter List"}';
        }
   /* }
    else
    {
    echo '{"status":"Error","code":"103","message":"Invalid Key"}';
    }*/ 
}
db_close();

function getLnt($address){
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address).",indonesia&sensor=false";
    $result_string = file_get_contents($url);
    $result = json_decode($result_string, true);
    $result1[]=$result['results'][0];
    $result2[]=$result1[0]['geometry'];
    $result3[]=$result2[0]['location'];
    return $result3[0];
}

function get_timeago( $ptime )
{
    $estimate_time = time() - $ptime;

    if( $estimate_time < 1 )
    {
        return 'less than 1 second ago';
    }

    $condition = array(
                12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;

        if( $d >= 1 )
        {
            $r = round( $d );
            return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
        }
    }
}
?>