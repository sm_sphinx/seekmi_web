<?php
class App extends CI_Controller
{
       function __construct()
       {
           parent::__construct();    
           $this->load->helper('url');
	   $this->load->database();
	   $this->load->model('app_model');
	   $this->load->library('session');
	   $this->load->library('pagination');  
           date_default_timezone_set($this->config->config['constTimeZone']);
           ini_set('MAX_EXECUTION_TIME', -1);
       }
	 
       function index()
       {
            if($this->session->userdata('is_logged_in')==true)
            {
                 redirect('user/users');  	 
            }
            else
            {
                 $data['page_title'] = 'Login Page';
                 $this->load->view('admin/login_view', $data); 
            } 		
        }
	
	function lists($id)
	{            
            $this->logged_in_user();
            $userId = (int)$this->session->userdata('userId');
            $data['data_page']=$this->input->post('page');		
            $data['data_limit']=$this->input->post('limit');
            $data['keyword']=$this->input->post('keyword');
            $page =$data['data_page'];
            $data['cur_page'] = $page;
            $page -= 1;
            $data['per_page'] = $data['data_limit'];
            $data['previous_btn'] = true;
            $data['next_btn'] = true;
            $data['first_btn'] = true;
            $data['last_btn'] = true;
            $data['page_title'] = 'Manage Apps';
            $start = $page * $data['per_page'];
            $data['list_count']=$this->app_model->getAllUserAppsCount($data['keyword'],$id);
            $data['list_res']=$this->app_model->getAllUserApps($data['per_page'],$start,$data['keyword'],$id);
            $this->load->view('admin/pagi/app_list_pagi_view',$data);
	}
	
	function add_app($id)
	{
            $this->logged_in_user();		
            $data['page_title'] = 'Create App';
            $data['uId'] = $id;           
            $data['country_list'] = $this->app_model->getAllCounties();
            $this->load->view('admin/set_app_info',$data);
	}	
	
	function edit_app($id,$uid)
	{
            $this->logged_in_user();		
            $data['page_title'] = 'Edit App'; 
            $data['uId'] = $uid;           
            $data['country_list'] = $this->app_model->getAllCounties();                
            $data['app_info'] = $this->app_model->getAppInfo($id);
            $data['app_images'] = $this->app_model->getAppImages($id);
            $this->load->view('admin/set_app_info',$data);
	}
	
	function saveAppInfo($id)
	{
            //print_r($_POST);
            //print_r($_FILES);
            //die;
            $this->logged_in_user();		
            $data['appname'] = $this->input->post('app_name');
            $data['appemail'] = $this->input->post('app_email'); 
            $data['website'] = $this->input->post('app_website');
            $data['company'] = $this->input->post('app_company');
            $data['address'] = $this->input->post('app_address');
            $data['city'] = $this->input->post('app_city');
            $data['state'] = $this->input->post('app_state');
            $data['country'] = $this->input->post('app_country');
            $data['zipcode'] = $this->input->post('app_zipcode');
            $data['phone'] = $this->input->post('app_phone');
            $data['cellphone'] = $this->input->post('app_cellphone');
            $data['latitude'] = $this->input->post('latitude');
            $data['longitude'] = $this->input->post('longitude');
            $data['otherinfo'] = $this->input->post('app_otherinfo');
            //$app_logo_hid = $this->input->post('app_logo_hid');
            $map_image_hid = $this->input->post('map_image_hid');
            $data['userId'] = $id;
            $appId = $this->input->post('app_id');             
            $today = date('Y-m-d H:i:s');
            //$flag='';
            /*if($_FILES['app_logo']['name']!='')
            {
               $rename = '';		
               $filename = $_FILES['app_logo']['name'];
               $ext = explode(".",$filename);
               $config['upload_path'] = './app_logo/';
               $config['allowed_types'] = 'gif|jpg|png';
               $config['max_size'] = '0';

               $config['remove_spaces'] = TRUE;
               $rename = uniqid('logo_');
               $config['file_name'] = $rename;	
               $this->load->library('upload');			
               $this->upload->initialize($config);

               if($this->upload->do_upload('app_logo'))
               {
                  $data['applogo'] =  $rename.".".$ext[1];				 
               }
               else
               {
                  echo $this->upload->display_errors();exit();
               }
            }*/
            if($_FILES['map_image']['name']!='')
            {
               $rename = '';		
               $filename = $_FILES['map_image']['name'];
               $ext = explode(".",$filename);
               $config['upload_path'] = './map_images/';
               $config['allowed_types'] = 'gif|jpg|png';
               $config['max_size'] = '0';

               $config['remove_spaces'] = TRUE;
               $rename = uniqid('map_');
               $config['file_name'] = $rename;	
               $this->load->library('upload');			
               $this->upload->initialize($config);

               if($this->upload->do_upload('map_image'))
               {
                  $data['map_image'] =  $rename.".".$ext[1];				 
               }
               else
               {
                  echo $this->upload->display_errors();exit();
               }
            }
            $appImage = array();
            $old_image=array();
            $flg=false;
            $old_image[1] =$this->input->post('app_image_old_1');		
            $old_image[2] =$this->input->post('app_image_old_2');		
            $old_image[3] =$this->input->post('app_image_old_3');		
            $old_image[4] =$this->input->post('app_image_old_4');
            $old_image[5] =$this->input->post('app_image_old_5');
            $old_image[6] =$this->input->post('app_image_old_6');
            for($i=1; $i<=6; $i++)
            {
                if($_FILES['app_image'.$i]['name']!='')
                {
                        $flg=true;
                        $rename = '';
                        $image_upload = 'app_image'.$i;
                        $filename = $_FILES[$image_upload]['name'];
                        $ext = explode(".",$filename);
                        $config['upload_path'] = './app_images/';
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = '600';                           
                        $config['remove_spaces'] = TRUE;
                        $rename = uniqid('image_').'_'.$i;
                        $config['file_name'] = $rename;	
                        $this->load->library('upload');			
                        $this->upload->initialize($config);

                    if($this->upload->do_upload($image_upload))
                    {                               
                           if($old_image[$i]!='')
                           {
                                $imageURL= './app_image/'.$old_image[$i];
                                if (file_exists($imageURL))
                                {
                                    unlink($imageURL);
                                }                                
                            }					  
                            $old_image[$i] =  $rename.".".$ext[1];				 
                          }
                          else
                          {
                             echo $this->upload->display_errors();exit();
                          }
                }			
                unset($config);
            }
            //if($flag==''){
            if($appId==0){               
                 $data['createdDate'] = $today;
                 $data['modifiedDate'] = $today;                 
                 $this->app_model->insertApp($data); 
                 $appId=$this->db->insert_id();
                 $flag='success';             
            }else{
                $data['modifiedDate'] = $today;
                /*if($_FILES['app_logo']['name']!='')
                {			
                    if($app_logo_hid!='')
                    {
                        $imageURL= './app_logo/'.$app_logo_hid;
                        if(file_exists($imageURL))
                        {
                            unlink($imageURL);
                        }
                    }				
                }
                else
                {
                   $data['applogo'] = $app_logo_hid;
                }*/
                if($_FILES['map_image']['name']!='')
                {			
                    if($map_image_hid!='')
                    {
                        $imageURL= './map_images/'.$map_image_hid;
                        if(file_exists($imageURL))
                        {
                            unlink($imageURL);
                        }
                    }				
                }
                else
                {
                   $data['map_image'] = $map_image_hid;
                }
                $this->app_model->updateApp($data,$appId);
                $flag='success';
            }
            if($flg==true){
                $this->db->delete('tblappimages', array('appid'=>$appId));		
                for($i=1; $i<= 6; $i++)
                {
                  if($old_image[$i]!='')
                  {
                        $sql_arr = array('appid' => $appId,
                                         'appimage' => $old_image[$i]
                                        ); 
                        $this->db->insert('tblappimages', $sql_arr);
                  }
                }
            }
            redirect('admin/user/edit_user/'.$id);
            //}
            //echo $flag;
	}
		
	function deleteApp()
	{
            $this->logged_in_user();		
            $uid = $this->input->post('userId');
            $this->db->delete('tbluser', array('userId' => $uid)); 
            $this->db->delete('tblapp', array('userId' => $uid)); 
            echo 'success';
	}
        
        function manage_build($id)
        {
            $this->logged_in_user();		
            $data['page_title'] = 'Manage Build'; 
            $data['appId'] = $id;                                      
            $data['app_info'] = $this->app_model->getAppInfo($id);            
            $this->load->view('admin/manage_build_page_view',$data);
        }
        
        function submit_build(){
         
            $this->logged_in_user();            
            $data['status'] = $this->input->post('txtStatus');
            $uid = $this->input->post('user_id');
            $ios_apk_hid = $this->input->post('ios_apk_hid');
            $android_apk_hid = $this->input->post('android_apk_hid');           
            $appId = $this->input->post('app_id');             
           
            
            if($_FILES['txtIosBuild']['name']!='')
            {               
               if($ios_apk_hid!='')
               {
                    $imageURL= './app_apk/ios/'.$ios_apk_hid;
                    if(file_exists($imageURL))
                    {
                        unlink($imageURL);
                    }
               } 
               $rename = '';		
               $filename = $_FILES['txtIosBuild']['name'];
               $ext = explode(".",$filename);
               $config['upload_path'] = './app_apk/ios/';
               $config['allowed_types'] = 'apk';
               $config['max_size'] = '0';

               $config['remove_spaces'] = TRUE;
               $rename = uniqid('apk_');
               $config['file_name'] = $rename;	
               $this->load->library('upload');			
               $this->upload->initialize($config);

               if($this->upload->do_upload('txtIosBuild'))
               {
                  $data['ios_apk'] =  $rename.".".$ext[1];				 
               }
               else
               {
                  echo $this->upload->display_errors();exit();
               }
            }else{
                $data['ios_apk'] = $ios_apk_hid;
            }
            if($_FILES['txtAndroidBuild']['name']!='')
            {
               if($android_apk_hid!='')
               {
                    $imageURL= './app_apk/android/'.$android_apk_hid;
                    if(file_exists($imageURL))
                    {
                        unlink($imageURL);
                    }
               }
               $rename = '';		
               $filename = $_FILES['txtAndroidBuild']['name'];
               $ext = explode(".",$filename);
               $config['upload_path'] = './app_apk/android/';
               $config['allowed_types'] = 'apk';
               $config['max_size'] = '0';

               $config['remove_spaces'] = TRUE;
               $rename = uniqid('apk_');
               $config['file_name'] = $rename;	
               $this->load->library('upload');			
               $this->upload->initialize($config);

               if($this->upload->do_upload('txtAndroidBuild'))
               {
                  $data['android_apk'] =  $rename.".".$ext[1];				 
               }
               else
               {
                  echo $this->upload->display_errors();exit();
               }
            }else{
                $data['android_apk'] = $android_apk_hid;
            } 
            if($data['status']=='Published'){
              $order_start_date = date('Y-m-d');
              $order_end_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime($order_start_date)) . " + 1 year"));             
              $this->db->query("update `tblorderdetail` as a, `tblorder` as b set a.order_start_date='".$order_start_date."', a.order_end_date='".$order_end_date."' WHERE a.`orderid`=b.`orderid` and b.appid='".$appId."'");  
              $this->db->query("update `tblapp` set expiry_date='".$order_end_date."' WHERE appId='".$appId."'");                
            }
            /*$this->load->model('user_model');
            $user_row=$this->user_model->getUserInfo($uid);
            $this->load->library('email');				 			
            $config['mailtype'] = "html";
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");		
            $this->email->to($uemail); 
            $this->email->from('admin@mrassistance.com', 'mrassistance.com');
            $this->email->subject('Reset Password at Mr.Assistance.com');
            $htmlMessage='<table><tr><td><b>Dear '.$res->customer_first_name.' '.$res->customer_last_name. '</b></td></tr></table>';
            $htmlMessage .='<p style="color:red;">Thank you for being a Mr. Assistance customer.<br/>Please click the following link to reset your password:'.'</p>';
            $htmlMessage .="<table><tr><td><a href='".$this->config->config['base_url']."reset_password/".base64_encode($res->customer_id)."'>".$this->config->config['base_url']."reset_password/".base64_encode($res->customer_id)."</a></td></tr>";

            $htmlMessage .="<tr><td>&nbsp;</td></tr><tr><td>Thanks again for being a valued customer. Your privacy is important to us and we appreciate you taking the time to reset your password.</td></tr><tr><td>&nbsp;</td></tr>";
            $htmlMessage .="<tr><td>Thanks,</td></tr>		
            <tr><td>Mr. Assistance Support</td></tr>
            <tr><td><a href='".$this->config->config['base_url']."'>www.mrassistance.com</a></td></tr>";
            $htmlMessage .='<tr><td></td></tr></table>';
            $this->email->message($htmlMessage);
            $this->email->send();*/
            
            $this->app_model->updateApp($data,$appId);
            redirect('admin/user/edit_user/'.$uid);
        }
        
        function download_source_code()
        {
            $this->logged_in_user();              
            $appid = $this->input->post('appid');
            $type = $this->input->post('type');
            $step = $this->input->post('step');
            $num = $this->input->post('num');
            $app_info = $this->app_model->getAppInfo($appid);
            $app_images = $this->app_model->getAppImages($appid);          
            if($app_info!=''){                
                if($step=='tile' && $app_info->latitude!='0.00000000' && $app_info->longitude!='0.00000000'){
                    $lat=$app_info->latitude;
                    $lon=$app_info->longitude;
                    if($num=='16'){
                    $this->recursiveRemoveDirectory("./source_code/img/MapLink");
                    $this->recursiveRemoveDirectory("./Montego/platforms/android/assets/www/img/MapLink");
                    }
                    $zoom=$num;
                    $boundry_arr=array();
                    $boundry_arr['16']=array('x'=>'2','y'=>'2');
                    $boundry_arr['17']=array('x'=>'3','y'=>'3');
                    $boundry_arr['18']=array('x'=>'6','y'=>'6');
                    $boundry_arr['19']=array('x'=>'12','y'=>'12');
                    
                    $xtile = floor((($lon + 180) / 360) * pow(2, $zoom));                        
                    $ytile = floor((1 - log(tan(deg2rad($lat)) + 1 / cos(deg2rad($lat))) / pi()) /2 * pow(2, $zoom));

                    $xtileplus=$xtile+$boundry_arr[$zoom]['x'];
                    $ytileplus=$ytile+$boundry_arr[$zoom]['y'];
                    $xtileminus=$xtile-$boundry_arr[$zoom]['x'];
                    $ytileminus=$ytile-$boundry_arr[$zoom]['y'];

                    for($kk=$xtileminus;$kk<=$xtileplus;$kk++){ 
                        for($ll=$ytileminus;$ll<=$ytileplus;$ll++){
                            $this->createTiles($kk,$ll,$zoom);  
                        }
                    }                    
                    if($num=='19'){
                        echo "html"."||0";
                        exit();
                    }else{
                        echo "tile"."||".($num+1);
                        exit();
                    }
                }
                if($step=='html'){
                  $init_res=$this->createAppInitHtml($appid,$type);
                  $home_res=$this->createAppHomeHtml($app_info->appname,$type);
                  $info_res=$this->createAppInfoHtml($app_info,$type);
                  $photo_res=$this->createAppPhotoHtml($app_info,$app_images,$type);
                  $other_info_res=$this->createAppOtherInfoHtml($app_info,$type);
                  if($init_res==1 && $home_res==1 && $info_res==1 && $photo_res==1 && $other_info_res==1){
                    echo "zip||0";
                  }else{
                    echo "error";
                  }                     
                }
                if($step=='zip'){
                    $source_code_folder="source_code_zip/".$type;
                    $renamefl=$type.'_'.time();
                    shell_exec('zip -r '.$renamefl.'.zip '.$_SERVER["DOCUMENT_ROOT"].'/montego/Montego/');
                    shell_exec( " mv ".$renamefl.".zip ".$_SERVER["DOCUMENT_ROOT"]."/montego/".$source_code_folder." 2>&1 " );
                    $downloaded_path=shell_exec('find -iname '.$renamefl.'.zip');                    
                    if(file_exists('./source_code_zip/'.$type.'/'.$renamefl.'.zip')){                         
                    $this->db->query("update `tblapp` set ".$type."_source_code_path='".$renamefl.".zip' WHERE appId='".$appid."'");
                    echo "done||".$source_code_folder."/".$renamefl.".zip"; 
                   }
                }
            }
            
        }
        
        function createAppInitHtml($appId,$tp)
        {
             if($appId!=''){                 
                 if(file_exists('./source_code')){                    
                     $dest_file='./Montego/platforms/'.$tp.'/assets/www/index.html';
                     $html=file_get_contents('./source_code/index.html');
                     $search = array("{#dbappid}"); 
                     $replace = array($appId); 
                     $newHtml = str_replace($search, $replace, $html);
                     file_put_contents($dest_file, $newHtml);
                     return true;
                 }else{
                     return false;
                 }
             }else{   
               return false;
             }
        } 
        
        function createAppHomeHtml($app_name,$tp)
        {
             if($app_name!=''){                 
                 if(file_exists('./source_code')){                    
                     $dest_file='./Montego/platforms/'.$tp.'/assets/www/templates/home.html';
                     $html=file_get_contents('./source_code/templates/home.html');
                     $search = array("{#appName}"); 
                     $replace = array($app_name); 
                     $newHtml = str_replace($search, $replace, $html);
                     file_put_contents($dest_file, $newHtml);
                     return true;
                 }else{
                     return false;
                 }
             }else{   
               return false;
             }
        }        
        
        function createAppInfoHtml($app_info,$tp)
        {
             if($app_info!=''){                 
                 if(file_exists('./source_code')){                    
                     $dest_file='./Montego/platforms/'.$tp.'/assets/www/templates/appinfo.html';
                     $html=file_get_contents('./source_code/templates/appinfo.html');
                     $search = array("{#appName}", "{#appWebsite}", "{#appEmail}", "{#appAddress}", "{#appCity}", "{#appLatitude}", "{#appLongitude}", "{#appPhone}"); 
                     $replace = array($app_info->appname,$app_info->website,$app_info->appemail,$app_info->address,$app_info->city,$app_info->latitude,$app_info->longitude,$app_info->phone); 
                     $newHtml = str_replace($search, $replace, $html);
                     file_put_contents($dest_file, $newHtml);
                     return true;
                 }else{
                     return false;
                 }
             }else{   
               return false;
             }
        }
        
        function createAppPhotoHtml($app_info,$app_images,$tp)
        {
             if($app_images!=''){                 
                 if(file_exists('./source_code')){ 
                       
                     for($k=1;$k<=6;$k++){
                         if(file_exists('./source_code/img/img'.$k.'.jpg')){
                            unlink('./source_code/img/img'.$k.'.jpg');
                         }
                         if(file_exists('./Montego/platforms/'.$tp.'/assets/www/img/img'.$k.'.jpg')){
                            unlink('./Montego/platforms/'.$tp.'/assets/www/img/img'.$k.'.jpg');
                         }
                     }
                     $n=1;
                     foreach($app_images as $app_image_row)
                     {                   
                        $file= './app_images/'.$app_image_row->appimage;  
                        $newfile = './source_code/img/'.'img'.$n.'.jpg';
                        copy($file, $newfile);
                        $n++;
                     }
                     if($app_info->map_image!=''){
                         if(file_exists('./map_images/'.$app_info->map_image)){
                            $mapfile= './map_images/'.$app_info->map_image;  
                            $mapnewfile = './source_code/img/map.png';
                            copy($mapfile, $mapnewfile);
                         }
                     }
                     if(file_exists('./source_code/img/img1.jpg')){                         
                         //$this->recursiveRemoveDirectory("./Montego/platforms/android/assets/www/img/MapLink"); 
                         $this->copydir('./source_code/img','./Montego/platforms/'.$tp.'/assets/www/img');
                         $this->copydir('./source_code/img/MapLink/16','./Montego/platforms/'.$tp.'/assets/www/img/MapLink/16');
                         $this->copydir('./source_code/img/MapLink/17','./Montego/platforms/'.$tp.'/assets/www/img/MapLink/17');
                         $this->copydir('./source_code/img/MapLink/18','./Montego/platforms/'.$tp.'/assets/www/img/MapLink/18');
                         $this->copydir('./source_code/img/MapLink/19','./Montego/platforms/'.$tp.'/assets/www/img/MapLink/19');
                     }                     
                     $dest_file='./Montego/platforms/'.$tp.'/assets/www/templates/appphotos.html';
                     $html=file_get_contents('./source_code/templates/appphotos.html');
                     $search = array("{#appName}"); 
                     $replace = array($app_info->appname); 
                     $newHtml = str_replace($search, $replace, $html);
                     file_put_contents($dest_file, $newHtml);
                     return true;
                 }else{
                     return false;
                 }
             }else{   
               return false;
             }
        }
        
        function createAppOtherInfoHtml($app_info,$tp)
        {
             if($app_info!=''){                 
                 if(file_exists('./source_code')){                    
                     $dest_file='./Montego/platforms/'.$tp.'/assets/www/templates/otherinfo.html';
                     $html=file_get_contents('./source_code/templates/otherinfo.html');
                     $search = array("{#appName}","{#appOtherInfo}"); 
                     $replace = array($app_info->appname,$app_info->otherinfo); 
                     $newHtml = str_replace($search, $replace, $html);
                     file_put_contents($dest_file, $newHtml);
                     return true;
                 }else{
                     return false;
                 }
             }else{   
               return false;
             }
        } 
        
        function check_source_code_avail(){
            $this->logged_in_user();              
            $appid = $this->input->post('appid');
            $type = $this->input->post('type');           
            $app_info = $this->app_model->getAppInfo($appid);
            $type_source_code=$type."_source_code_path";
            if($app_info->$type_source_code!=''){
                echo $app_info->$type_source_code;
            }else{
                echo '';
            }
        }
        
        function copydir($source,$destination)
        {
            if(!is_dir($destination)){
            $oldumask = umask(0); 
            mkdir($destination, 01777); // so you get the sticky bit set 
            umask($oldumask);
            }
            $dir_handle = @opendir($source) or die("Unable to open");
            while ($file = readdir($dir_handle)) 
            {
            if($file!="." && $file!=".." && !is_dir("$source/$file"))
            copy("$source/$file","$destination/$file");
            }
            closedir($dir_handle);
        }
        
        function recursiveRemoveDirectory($directory)
        {
            foreach(glob("{$directory}/*") as $file)
            {
                if(is_dir($file)) { 
                    $this->recursiveRemoveDirectory($file);
                } else {
                    unlink($file);
                }
            }
            //rmdir($directory);
        }
        
        function createTiles($x,$y,$z){
            $ttl = 86400; //cache timeout in seconds  
            if (!file_exists('./source_code/img/MapLink/'.$z)) {
                mkdir('./source_code/img/MapLink/'.$z, 0777, true);
            }
            $file = "./source_code/img/MapLink/${z}/${x}_$y.png";
            if (!is_file($file))
            {
              $server = array();
              $server[] = 'a.tile.openstreetmap.org';
              $server[] = 'b.tile.openstreetmap.org';
              $server[] = 'c.tile.openstreetmap.org';
              $url = 'http://'.$server[array_rand($server)];
              $url .= "/".$z."/".$x."/".$y.".png";
              $ch = curl_init($url);
              $fp = fopen($file, "w");
              curl_setopt($ch, CURLOPT_FILE, $fp);
              curl_setopt($ch, CURLOPT_HEADER, 0);
              curl_exec($ch);
              curl_close($ch);
              fflush($fp);    // need to insert this line for proper output when tile is first requestet
              fclose($fp);
            }
        }
	
	function logged_in_user()
	{
            if($this->session->userdata('userId')=='')
            {
                 redirect($this->config->config['base_url']."admin/");
            }
	}	
}
?>