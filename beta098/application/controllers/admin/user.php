<?php

class User extends CI_Controller
{
      function __construct()
      {
           parent::__construct();    
           $this->load->helper('url');
	   $this->load->database();
	   $this->load->model('user_model');
	   $this->load->library('session');
	   $this->load->library('pagination');   
           date_default_timezone_set($this->config->config['constTimeZone']);
      }
	 
      function index()
      {
            if($this->session->userdata('is_logged_in')==true)
            {
                 redirect('admin/user/users');  	 
            }
            else
            {
                 $data['page_title'] = 'Login Page';
                 $this->load->view('admin/login_view', $data); 
            } 		
       }
	
	function my_account()
	{	
            $this->logged_in_user();
            $data['page_title'] = 'My Account';
            $userId = (int)$this->session->userdata('userId');
            $data['user_info'] = $this->user_model->getAdminInfo($userId);
            $this->load->view('admin/my_profile_view',$data);
	}	
	
	function users()
	{	
            $this->logged_in_user();
            if($this->session->userdata('user_type')=='admin'){
              $data['page_title'] = 'Manage Customer';
            }else{
              $data['page_title'] = 'My Customer';  
            }
            $this->load->view('admin/users_view',$data);
	}	
	
	function users_pagi()
	{
            $this->logged_in_user();
            $userId = (int)$this->session->userdata('userId');
            $data['data_page']=$this->input->post('page');		
            $data['data_limit']=$this->input->post('limit');
            $data['keyword']=$this->input->post('keyword');
            $page =$data['data_page'];
            $data['cur_page'] = $page;
            $page -= 1;
            $data['per_page'] = $data['data_limit'];
            $data['previous_btn'] = true;
            $data['next_btn'] = true;
            $data['first_btn'] = true;
            $data['last_btn'] = true;
            if($this->session->userdata('user_type')=='admin'){
              $data['page_title'] = 'Manage Customer';
            }else{
              $data['page_title'] = 'My Customer';  
            }
            $start = $page * $data['per_page'];
            $data['list_count']=$this->user_model->getAllUserCount($data['keyword']);
            $data['list_res']=$this->user_model->getAllUser($data['per_page'],$start,$data['keyword']);
            $this->load->view('admin/pagi/users_pagi_view',$data);
	}
	
	function add_user()
	{
            $this->logged_in_user();		
            $data['page_title'] = 'Add Customer';
            $this->load->model('reseller_model');
            $data['reseller_list'] = $this->reseller_model->getAllActiveReseller();
            $this->load->view('admin/set_user_info',$data);
	}
	
	
	function edit_user($uid)
	{
            $this->logged_in_user();		
            $data['page_title'] = 'Edit Customer'; 
            $this->load->model('reseller_model');
            $data['reseller_list'] = $this->reseller_model->getAllActiveReseller();                 
            $data['user_info'] = $this->user_model->getUserInfo($uid);
            $this->load->view('admin/set_user_info',$data);
	}
	
	function saveUserInfo()
	{
            $this->logged_in_user();		
            $data['firstname'] = $this->input->post('txtFirstname');
            $data['lastname'] = $this->input->post('txtLastname'); 
            $data['email'] = $this->input->post('txtEmail');
            $data['resellerId'] = $this->input->post('txtReseller');
            $userId = $this->input->post('user_id');
            $data['active'] = $this->input->post('txtActive');
            $today = date('Y-m-d H:i:s');
            if($userId==0){
               $checkEmail=$this->user_model->checkEmailExists($data['email']);
               if($checkEmail==1){
                 $data['password'] = md5($this->input->post('txtPassword'));
                 $data['user_type'] = 'customer';
                 $data['createdDate'] = $today;
                 $data['modifiedDate'] = $today;
                 $this->user_model->insertUser($data);
                 echo 'success';
               }  else {
                 echo 'already';  
               }
            }else{
                $data['modifiedDate'] = $today;
                if($this->input->post('change_password_check')=='1'){
                    $data['password'] = md5($this->input->post('txtPassword'));
                }
                $this->user_model->updateUser($data,$userId);
                echo 'success';
            }           
	}
	
	function set_user_password()
	{
            $this->logged_in_user();		
            $password = trim($this->input->post('nPassword'));
            $data['password'] = md5($password);
            $userId = $this->input->post('userId');
            $this->user_model->updateUser($data,$userId);
            echo 'success';
	}
	
	function login_submit()
	{	
            $pdata['useremail']=$this->input->post('email');
            $pdata['userpass']=$this->input->post('password');
            $row=$this->user_model->check_user_login($pdata);	

            if($row!='')
            {	
                $this->session->set_userdata('user_email',$row->email);				
                $this->session->set_userdata('userId',$row->userId);
                $this->session->set_userdata('user_type',$row->user_type);
                $this->session->set_userdata('is_logged_in',true);	
                echo "success";	
            }
            else
            {
                echo "failed";
            }		
	}
	
	function deleteUser()
	{
            $this->logged_in_user();		
            $uid = $this->input->post('userId');
            $this->db->delete('tbluser', array('userId' => $uid)); 
            $this->db->delete('tblapp', array('userId' => $uid)); 
            echo 'success';
	}
	
	function login()
	{	   
            $data['page_title'] = 'Login';
	    $this->load->view('admin/login_view',$data); 		
	}
	 
	function logout()
	{
            $usertype=$this->session->userdata('user_type');
            $sess_items = array('user_email' => '', 'userId' => '','is_logged_in'=> false, 'user_type' => '');
            $this->session->unset_userdata($sess_items);
            //$this->session->sess_destroy();
            if($usertype=='admin'){
              redirect('/admin');
            }else{
              redirect('/');  
            }
	}
		
	function change_password()
	{
            $oldpassword = $this->input->post('oPassword');			
            $newpassword = $this->input->post('nPassword');						
            $pdata['cPassword']=$oldpassword;
            $pdata['nPassword']=$newpassword;
            $userId = $this->input->post('userId');
            $check_pwd=$this->user_model->check_old_password($userId, $pdata['cPassword']);	
            if($check_pwd > 0)
            {
                    $data = array('password' => md5($pdata['nPassword']));
                $this->user_model->updateAdmin($data,$userId);	
                    echo 'success';
            }
            else
            {
               echo 'wrong_old';
            }		 
	}
		
	function byte_convert($size) 
	{
            # size smaller then 1kb
            if ($size < 1024) return $size . '===Byte';
            # size smaller then 1mb
            if ($size < 1048576) return sprintf("%4.2f===KB", $size/1024);
            # size smaller then 1gb
            if ($size < 1073741824) return sprintf("%4.2f===MB", $size/1048576);
            # size smaller then 1tb
            if ($size < 1099511627776) return sprintf("%4.2f===GB", $size/1073741824);
            # size larger then 1tb
            else return sprintf("%4.2f===TB", $size/1073741824);
	}
	
	function logged_in_user()
	{
            if($this->session->userdata('userId')=='')
            {
                 redirect($this->config->config['base_url']."admin/");
            }
	}	
}
?>