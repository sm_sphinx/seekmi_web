<?php

class Reseller extends CI_Controller
{
      function __construct()
      {
           parent::__construct();    
           $this->load->helper('url');
	   $this->load->database();
	   $this->load->model('reseller_model');
	   $this->load->library('session');
	   $this->load->library('pagination'); 
           date_default_timezone_set($this->config->config['constTimeZone']);
      }
	 
      function index()
      {
            if($this->session->userdata('is_logged_in')==true)
            {
                 redirect('reseller/lists');  	 
            }
            else
            {
                 $data['page_title'] = 'Login Page';
                 $this->load->view('admin/login_view', $data); 
            } 		
       }
			
	function lists()
	{	
            $this->logged_in_user();
            $data['page_title'] = 'Manage Reseller';
            $this->load->view('admin/reseller_view',$data);
	}	
	
	function lists_pagi()
	{
            $this->logged_in_user();
            $userId = (int)$this->session->userdata('userId');
            $data['data_page']=$this->input->post('page');		
            $data['data_limit']=$this->input->post('limit');
            $data['keyword']=$this->input->post('keyword');
            $page =$data['data_page'];
            $data['cur_page'] = $page;
            $page -= 1;
            $data['per_page'] = $data['data_limit'];
            $data['previous_btn'] = true;
            $data['next_btn'] = true;
            $data['first_btn'] = true;
            $data['last_btn'] = true;
            $data['page_title'] = 'Manage Reseller';
            $start = $page * $data['per_page'];
            $data['list_count']=$this->reseller_model->getAllResellerCount($data['keyword']);
            $data['list_res']=$this->reseller_model->getAllReseller($data['per_page'],$start,$data['keyword']);
            $this->load->view('admin/pagi/reseller_pagi_view',$data);
	}
	
	function add_reseller()
	{
            $this->logged_in_user();		
            $data['page_title'] = 'Add Reseller';
            $this->load->view('admin/set_reseller_info',$data);
	}
	
	
	function edit_reseller($uid)
	{
            $this->logged_in_user();		
            $data['page_title'] = 'Edit Reseller';            	
            $data['user_info'] = $this->reseller_model->getResellerInfo($uid);
            $this->load->view('admin/set_reseller_info',$data);
	}
	
	function saveResellerInfo()
	{
            $this->logged_in_user();		
            $data['firstname'] = $this->input->post('txtFirstname');
            $data['lastname'] = $this->input->post('txtLastname'); 
            $data['email'] = $this->input->post('txtEmail');
            $userId = $this->input->post('user_id');
            $data['active'] = $this->input->post('txtActive');
            $today = date('Y-m-d H:i:s');
            if($userId==0){
               $checkEmail=$this->reseller_model->checkEmailExists($data['email']);
               if($checkEmail==1){
                 $data['password'] = md5($this->input->post('txtPassword'));
                 $data['user_type'] = 'reseller';
                 $data['createdDate'] = $today;
                 $data['modifiedDate'] = $today;
                 $this->reseller_model->insertReseller($data);
                 echo 'success';
               }  else {
                 echo 'already';  
               }
            }else{
                $data['modifiedDate'] = $today;
                if($this->input->post('change_password_check')=='1'){
                    $data['password'] = md5($this->input->post('txtPassword'));
                }
                $this->reseller_model->updateReseller($data,$userId);
                echo 'success';
            }           
	}
	
	function deleteReseller()
	{
            $this->logged_in_user();		
            $uid = $this->input->post('userId');
            $this->db->delete('tbluser', array('userId' => $uid)); 
            $this->db->delete('tblapp', array('userId' => $uid)); 
            echo 'success';
	}
               
        function view_customer($id)
	{
            $this->logged_in_user();
            $userId = (int)$this->session->userdata('userId');
            $data['data_page']=$this->input->post('page');		
            $data['data_limit']=$this->input->post('limit');
            $page =$data['data_page'];
            $data['cur_page'] = $page;
            $page -= 1;
            $data['per_page'] = $data['data_limit'];
            $data['previous_btn'] = true;
            $data['next_btn'] = true;
            $data['first_btn'] = true;
            $data['last_btn'] = true;            
            $start = $page * $data['per_page'];
            $data['list_count']=$this->reseller_model->getAllResellerCustomerCount($id);
            $data['list_res']=$this->reseller_model->getAllResellerCustomer($id,$data['per_page'],$start);
            $this->load->view('admin/pagi/reseller_customer_list_pagi_view',$data);
	}
		
	function byte_convert($size) 
	{
            # size smaller then 1kb
            if ($size < 1024) return $size . '===Byte';
            # size smaller then 1mb
            if ($size < 1048576) return sprintf("%4.2f===KB", $size/1024);
            # size smaller then 1gb
            if ($size < 1073741824) return sprintf("%4.2f===MB", $size/1048576);
            # size smaller then 1tb
            if ($size < 1099511627776) return sprintf("%4.2f===GB", $size/1073741824);
            # size larger then 1tb
            else return sprintf("%4.2f===TB", $size/1073741824);
	}
	
	function logged_in_user()
	{
            if($this->session->userdata('userId')=='')
            {
                 redirect($this->config->config['base_url']."admin/");
            }
	}	
}
?>