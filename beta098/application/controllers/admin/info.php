<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info extends CI_Controller 
{

	function __construct()
	{
                parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->database();
		$this->load->library('session');
                $this->load->model('info_model');
	   	$this->load->library('pagination');
                date_default_timezone_set($this->config->config['constTimeZone']);
	}
	
	function contents()
	{
		$this->logged_in_user();
		$data['page_title'] = 'Info Page';
		$pageCode = 'info';
		$langCode = 'en';
		$data['page_code'] = 'info';
		$data['langCode'] = $langCode;
		$data['contents'] = $this->info_model->getPageContents($pageCode,$langCode);	
		$this->load->view('info_view',$data);
	}

	function saveInfo()
	{	
		$this->logged_in_user();
		$loginid = $this->session->userdata('userId');
		
		$infoId=$this->input->post('info_id');
		$pageCode=trim($this->input->post('page_code'));
		$langCode=$this->input->post('lang_code');
		$pageContents=$this->input->post('txt_content');
		$date = date('Y-m-d H:i:s');
		
		$data = array('pageContents' => $pageContents,
					  'modifiedDate' =>$date,
					 );
		$where_array = array('pageCode'=>$pageCode, 'langCode'=>$langCode);
		$this->info_model->updateData('tblinfo',$data,$where_array); 

		redirect('info/contents');
		
	}

	function logged_in_user()
	{
		if($this->session->userdata('userId')=='')
		{
			redirect($this->config->config['base_url']);
		}
	}
	
}