<?php
class Bid extends CI_Controller
{
    function __construct()
    {
        parent::__construct();    
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('bid_model');
        $this->load->library('session');
        $this->load->library('pagination');  
        date_default_timezone_set($this->config->config['constTimeZone']);       
    }
    
    function create()
    {       
        if($this->session->userdata('is_logged_in')==true)
        {
          $data['priceType']=$this->input->post('estimate_type');
          if($data['priceType']=='fixed'){
              $data['quotePrice']=$this->input->post('estimate_fixed_price_per_unit');
          }
          if($data['priceType']=='no_price'){
              $data['quotePrice']='';
          }
          if($data['priceType']=='hourly'){
              $data['quotePrice']=$this->input->post('estimate_hourly_price_per_unit');
          }
          $data['quoteMessage']=$this->input->post('message');
          $data['status']='Accepted';
          $data['readFlag']='Y';
          $requestId=$this->input->post('req');
          $this->db->update('tblquoterequest', $data, array('requestId' => $requestId));
          $this->load->model('service_model');
          $serviceInfo=$this->service_model->getSubServiceInfo($this->input->post('service'));          
          $this->db->where('userId', $this->session->userdata('userId'));
          $this->db->set('credits', 'credits - ' . (int) $serviceInfo->avlCredit, FALSE); 
          $this->db->update('tbluser');
          //redirect('profile/requests');
          redirect('profile/work');
        }
        else
        {
          redirect('user/login');
        } 
    }
    
    function details($id,$qid)
    {       
        if($this->session->userdata('is_logged_in')==true)
        {
           $this->load->model('service_model');  
           $this->load->model('profile_model');  
           $data['page_title']='Quote';
           $data['quote_id']=$qid;
           $data['project_id']=$id;
           $data['project_row'] = $this->service_model->getUserProjectDetails($id);
           $data['quote_row'] = $this->service_model->getExpertDetails($qid);
           $data['media_count'] = $this->profile_model->getMediaCount($data['quote_row']->professionalId);
           $data['quote_list'] = $this->service_model->getProjectExpertList($id);
           $data['message_list'] = $this->bid_model->getQuoteMessage($qid);
           $data['req_data']=$this->service_model->getRequestQuestions($id);
           $optionArr=array();           
           if($data['req_data']!=''){
               $option_extra='';
               foreach($data['req_data'] as $req_row){
                  $optArr=array(); 
                  if($req_row->optionVal=='' || $req_row->optionVal=='0'){
                    $option_data=$this->service_model->getRequestAnswers($req_row->questionId,$id);                   
                    if(!empty($option_data)){                        
                        foreach($option_data as $option_row){
                            if($option_row->optionType=='miles'){
                                if($option_row->extras!=''){
                                  $extraArr=json_decode($option_row->extras);
                                  $option_extra=$extraArr->miles." kilometer";
                                }else{
                                  $option_extra='';
                                }
                            }else{
                                 $option_extra='';
                            }
                            $optArr[]=array('optionText'=>$option_row->optionText,'optionType'=>$option_row->optionType,'optionExtra'=>$option_extra);
                        }
                    }else{
                        $optArr=array();
                    }
                  }else{                     
                     $optArr[]=array('optionText'=>$req_row->optionVal,'optionType'=>'','optionExtra'=>$option_extra); 
                  }
                 $optionArr[$req_row->questionId]=$optArr;
               }
           }  
           $data['req_option_data']=$optionArr;
           $this->load->view('project_detail_page_view',$data); 
        }
        else
        {
           redirect('user/login?target=bid/details/'.$id.'/'.$pid);
        } 
    }
    
    function reply_message()
    {
        if($this->session->userdata('is_logged_in')==true)
        {
           $pdata['quoteId']=$this->input->post('qid');
           $pdata['messageFrom']=$this->input->post('msgFrom');
           $pdata['messageTo']=$this->input->post('msgTo');
           $pdata['message']=$this->input->post('msg_content');
           $pdata['readFlag']='N';
           $pdata['createdOn']=date('Y-m-d H:i:s');          
           $this->db->insert('tblquotemessage', $pdata); 
           $this->load->model('profile_model');  
           
            $apikey='EQ4Z41iMdCc7uiznTzzUVw';
            $this->load->library('mandrill', array($apikey));
            $mandrill_ready = NULL;
            try {
                   $this->mandrill->init( $apikey );
                   $mandrill_ready = TRUE;
            } catch(Mandrill_Exception $e) {
                   $mandrill_ready = FALSE;
            }
            if($mandrill_ready){
                if($this->session->userdata('user_type')=='user'){
                    /* send message reply for professional */                       
                    $pros_user_data=$this->profile_model->getUserInfo($pdata['messageTo']);    
                    $firstname=$pros_user_data->firstname;
                    $lastname=$pros_user_data->lastname;  
                    $request_data=$this->profile_model->getCustomerRequestDetails($pdata['quoteId']); 
                    $project_data=$this->profile_model->getProjectOwner($request_data->projectId);
                    if(get_cookie('language') == 'english') {
                        $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/pros-received-msg-template.html');
                        $service_name=$project_data->subserviceName;
                        $subjectName='Your client has a message pending for you!';
                    }else{
                        $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/pros-received-msg-template-id.html');    
                        $service_name=$project_data->subserviceNameIND;
                        $subjectName='Klien Anda telah mengirimkan pesan untuk Anda.';
                    }                    
                    
                    $request_detail_url=$this->config->config['base_url'].'bid/details/'.$request_data->projectId.'/'.$pdata['quoteId'];               
                    $htmlMessage=str_replace('{REQUEST_DETAIL_URL}',$request_detail_url, $htmlMessage); 
                    $htmlMessage=str_replace('{SERVICE_NAME}',$service_name, $htmlMessage); 
                    $htmlMessage=str_replace('{PROS_FULLNAME}', $firstname." ".$lastname, $htmlMessage);  
                    $htmlMessage=str_replace('{USER_FULLNAME}', $this->session->userdata('user_full_name'), $htmlMessage);    
                    $email = array(
                        'html' => $htmlMessage, //Consider using a view file
                        'text' => '',
                        'subject' => $subjectName,
                        'from_email' => 'support@seekmi.com',
                        'from_name' => 'Seekmi',
                        'to' => array(array('email' => $pros_user_data->email)) 
                        );
                    $this->mandrill->messages_send($email);
                  }
                  
                  if($this->session->userdata('user_type')=='professional'){
                    /* send message reply for user */            
                    $user_dt=$this->profile_model->getUserInfo($pdata['messageFrom']);    
                    $firstname=$user_dt->firstname;
                    $lastname=$user_dt->lastname;  
                    $request_data=$this->profile_model->getCustomerRequestDetails($pdata['quoteId']); 
                    $project_data=$this->profile_model->getProjectOwner($request_data->projectId);
                    if(get_cookie('language') == 'english') {
                        $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/user-received-msg-template.html');
                        $service_name=$project_data->subserviceName;
                        $subjectName='You have a message pending!';
                    }else{
                        $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/user-received-msg-template-id.html');    
                        $service_name=$project_data->subserviceNameIND;
                        $subjectName='Anda memiliki pesan dari '.$this->session->userdata('user_full_name');
                    }                    
                    
                    $quote_detail_url=$this->config->config['base_url'].'profile/view_request/'.$pdata['quoteId'];               
                    $htmlMessage=str_replace('{QUOTE_DETAIL_URL}',$quote_detail_url, $htmlMessage); 
                    $htmlMessage=str_replace('{SERVICE_NAME}',$service_name, $htmlMessage); 
                    $htmlMessage=str_replace('{PROS_FULLNAME}', $this->session->userdata('user_full_name'), $htmlMessage);  
                    $htmlMessage=str_replace('{USER_FULLNAME}', $firstname." ".$lastname, $htmlMessage);    
                    $email = array(
                        'html' => $htmlMessage, //Consider using a view file
                        'text' => '',
                        'subject' => $subjectName,
                        'from_email' => 'support@seekmi.com',
                        'from_name' => 'Seekmi',
                        'to' => array(array('email' => $user_dt->email)) 
                        );
                    $this->mandrill->messages_send($email); 
                }
          }
            $user_data=$this->profile_model->getUserInfo($this->session->userdata('userId'));  
            $html='<li class="item message-item" style="display: block;">';
            if($user_data->userPhoto==''){
            $html.='<img class="profile-picture" src="'.$this->config->config['base_url'].'images/100x100.png">';
            }else if($user_data->facebookProfileId!=''){
            $html.='<img class="profile-picture" src="'.$user_data->userPhoto.'">';
            }else{
            $html.='<img class="profile-picture" src="'.$this->config->config['base_url'].'user_images/thumb/'.$user_data->userPhoto.'">';
            }
            $html.='<div class="item-content">
                    <time class="item-time">'.$this->get_timeago(strtotime($pdata['createdOn'])).'</time>
                    <h4 class="item-author">'.$user_data->firstname.' '.$user_data->lastname.'</h4>
                    <div class="bid-message-body">'. $pdata['message'].'</div></div></li>';
            echo $html;
        }
        else
        {
           //redirect('user/login');
            echo 'session_out';
        }
    }
    
    function change_project_status()
    {
        $quoteid=$this->input->post('qid');
        $prjid=$this->input->post('pid');
        $cdata['status']=$this->input->post('sts');
        
        if($cdata['status']=='Hired'){
            $pdata['status']='Declined';
            $pdata['modifiedOn']=date('Y-m-d H:i:s');
            $this->db->update('tblquoterequest', $cdata, array('projectId' => $prjid, 'requestId' => $quoteid));
            $this->db->update('tblquoterequest', $pdata, array('projectId' => $prjid, 'requestId !=' => $quoteid, 'status !=' => 'Pending'));
            $this->db->update('tbluserprojects', array('status'=>$cdata['status']), array('projectId' => $prjid));  
            echo 'success';
        }else{
            $cdata['status']=$this->input->post('sts');
            $cdata['modifiedOn']=date('Y-m-d H:i:s');
            $this->db->update('tblquoterequest', $cdata, array('requestId' => $quoteid));  
                
                $apikey='EQ4Z41iMdCc7uiznTzzUVw';
                $this->load->library('mandrill', array($apikey));
                $mandrill_ready = NULL;
                try {
                       $this->mandrill->init( $apikey );
                       $mandrill_ready = TRUE;
                } catch(Mandrill_Exception $e) {
                       $mandrill_ready = FALSE;
                }
                if($mandrill_ready){
                /* Declined mail for professional */ 
                $this->load->model('profile_model');  
                $pros_user_data=$this->profile_model->getCustomerRequestDetails($quoteid);    
                $firstname=$pros_user_data->firstname;
                $lastname=$pros_user_data->lastname;                
                if(get_cookie('language') == 'english') {
                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/pros-declined-task-template.html');
                }else{
                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/pros-declined-task-template-id.html');    
                }                
                $htmlMessage=str_replace('{PROS_FULLNAME}', $firstname." ".$lastname, $htmlMessage);  
                $htmlMessage=str_replace('{USER_FULLNAME}', $this->session->userdata('user_full_name'), $htmlMessage);    
                $email = array(
                    'html' => $htmlMessage, //Consider using a view file
                    'text' => '',
                    'subject' => 'Task Declined at Seekmi',
                    'from_email' => 'support@seekmi.com',
                    'from_name' => 'Seekmi',
                    'to' => array(array('email' => $pros_user_data->email)) //Check documentation for more details on this one                
                    );

                $this->mandrill->messages_send($email); 
                
                /* Declined mail for user */                
                $prj_data=$this->profile_model->getProjectOwner($prjid);                                  
                if(get_cookie('language') == 'english') {
                    $servicename=$prj_data->subserviceName;
                    $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/user-declined-task-template.html');
                }else{
                    $servicename=$prj_data->subserviceNameIND;
                    $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/user-declined-task-template-id.html');    
                }  
                $quote_url=$this->config->config['base_url'].'bid/details/'.$prjid.'/'.$quoteid;               
                $htmlMessage=str_replace('{DETAILS}',$servicename, $htmlMessage);  
                $htmlMessage=str_replace('{USER_FULLNAME}', $this->session->userdata('user_full_name'), $htmlMessage);
                $htmlMessage=str_replace('{QUOTE_DETAIL_URL}',$quote_url, $htmlMessage);
                $email = array(
                    'html' => $htmlMessage, //Consider using a view file
                    'text' => '',
                    'subject' => 'You have Declined Task at Seekmi',
                    'from_email' => 'support@seekmi.com',
                    'from_name' => 'Seekmi',
                    'to' => array(array('email' => $this->session->userdata('user_email'))) //Check documentation for more details on this one                
                    );

                $this->mandrill->messages_send($email); 
              }
           
            echo '<div aria-hidden="true" class="icon icon-24 icon-info"></div> 
            <p>You declined this quote, <time>'. $this->get_timeago(strtotime($cdata['modifiedOn'])).'</time>.</p>';
        }
    }
    
    function add_review($id)
    {
        if($this->session->userdata('is_logged_in')==true)
        {                  
           $this->load->model('profile_model');  
           $data['user_data']=$this->profile_model->getUserInfo($id);           
           $data['page_title']='Review '.$data['user_data']->firstname." ".$data['user_data']->lastname;
           $data['business_info']=$this->profile_model->getUserProviderInfo($id);
           $data['check_review']=$this->bid_model->checkAlreadyReviewed($this->session->userdata('userId'),$id);
           $this->load->view('add_review_page_view',$data); 
        }
        else
        {
           redirect('user/login?target=bid/add_review'.$id);
        }
    }
    
    function submit_review()
    {
        if($this->session->userdata('is_logged_in')==true)
        {
           $reviewMsg=$this->input->post('revMsg');
           $score=$this->input->post('score');
           $prosId=$this->input->post('usr');
           $check_review=$this->bid_model->checkAlreadyReviewed($this->session->userdata('userId'),$prosId);
           if($check_review > 0){
               echo 'already';
           }else{
               $rev_arr=array('rating'=>$score,'reviewContent'=>$reviewMsg,'userId'=>$this->session->userdata('userId'),'prosId'=>$prosId,'createdOn'=>date('Y-m-d H:i:s'));
               $this->db->insert('tblbusinessreview', $rev_arr); 
               $rev_cnt=$this->bid_model->getReviewCount($prosId);
               $rating_list=$this->bid_model->getUserRating($prosId);
               $rating_sum=0;
               foreach($rating_list as $rating_row) {
                    $rating_sum=$rating_sum+$rating_row->rating;
               }
               $ratingCnt=$rating_sum/$rev_cnt;
               $ratingCnt = floor($ratingCnt * 2) / 2;
               
               $this->db->update('tbluserprovider', array('reviewCount'=> $rev_cnt,'rating'=>$ratingCnt), array('userId' => $prosId));  
               echo 'success';
           }
        }
        else
        {
           redirect('user/login');
        }
    }
    
    function get_timeago( $ptime )
    {
        $estimate_time = time() - $ptime;
        if( $estimate_time < 1 )
        {
            return 'less than 1 second ago';
        }
        $condition = array(
                    12 * 30 * 24 * 60 * 60  =>  'year',
                    30 * 24 * 60 * 60       =>  'month',
                    24 * 60 * 60            =>  'day',
                    60 * 60                 =>  'hour',
                    60                      =>  'minute',
                    1                       =>  'second'
        );
        foreach( $condition as $secs => $str )
        {
            $d = $estimate_time / $secs;
            if( $d >= 1 )
            {
                $r = round( $d );
                return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
    }
    
    function update_project_status()
    {
         if($this->session->userdata('is_logged_in')==true)
         {
           $pid=$this->input->post('change_status_projectid');
           $status=$this->input->post('change_status_options');
           $comment=$this->input->post('change_status_comment');
           $this->bid_model->updateProjectStatus($this->session->userdata('userId'),$pid,$status,$comment);
           if($status=='Completed'){
                $this->load->model('profile_model');  
                $pros_user_data= $this->profile_model->getProjectCompletedPros($pid);  
                if($pros_user_data!=''){
                    $firstname=$pros_user_data->firstname;
                    $lastname=$pros_user_data->lastname;
                    $submit_review_url=$this->config->config['base_url'].'bid/add_review/'.$pros_user_data->userId;
                    if(get_cookie('language') == 'english') {
                    $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/pros-completed-task-template.html');
                    }else{
                    $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/pros-completed-task-template-id.html');    
                    }
                    $htmlMessage=str_replace('{SUBMIT_REVIEW_URL}', $submit_review_url, $htmlMessage); 
                    $htmlMessage=str_replace('{PROS_FULLNAME}', $firstname." ".$lastname, $htmlMessage);  
                    $htmlMessage=str_replace('{USER_FULLNAME}', $this->session->userdata('user_full_name'), $htmlMessage);    
                    $email = array(
                        'html' => $htmlMessage, //Consider using a view file
                        'text' => '',
                        'subject' => 'Task Completed at Seekmi',
                        'from_email' => 'support@seekmi.com',
                        'from_name' => 'Seekmi',
                        'to' => array(array('email' => $pros_user_data->email)) //Check documentation for more details on this one                
                        );

                    $this->mandrill->messages_send($email);  
                }
           }
           echo 'success';
         }
         else
         {
           echo 'failed';
         }          
     }
}
?>