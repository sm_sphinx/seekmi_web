<?php
class Home extends CI_Controller
{
       function __construct()
       {
           parent::__construct();    
           $this->load->helper('url');
	   $this->load->database();	   
	   $this->load->library('session');
	   $this->load->library('pagination');   
           //date_default_timezone_set($this->config->config['constTimeZone']);
       }
	 
       function index()
       {          
           $data['page_title'] = 'Home Page';
           $this->load->model('setting_model');
           $data['province_list']=$this->setting_model->getProvincesList();
           $this->load->view('home_page_view', $data); 
       }
       
       function get_services()
       {           
         $keyword=$this->input->get('search');
         $this->load->model('service_model');
         $res=$this->service_model->getAllServicesByKeyword($keyword);
         
         if(!empty($res)){           
            foreach($res as $row){
                $arr=array();
                $arr['text']=$row->serviceName;               
                $arr['value']=$row->serviceName;
                $new_arr[]=$arr;
            } 
            //print_r($new_arr);
         }else{
             $new_arr='';
         }
         
         echo json_encode($new_arr);
       }
       
       function get_services_pros()
       {           
         $keyword=$this->input->get('search');
         $this->load->model('service_model');
         $res=$this->service_model->getAllServicesByKeyword($keyword);
         
         if(!empty($res)){
            $new_arr=array();
            //$new_arr[]=array('text'=>'&nbsp;','value'=>'&nbsp;');
            foreach($res as $row){
                $arr=array();
                $arr['text']=$row->serviceName;               
                $arr['value']=$row->serviceName;
                $new_arr[]=$arr;
            } 
            //print_r($new_arr);
         }else{
             $new_arr='';
         }
         
         echo json_encode($new_arr);
       }
       
       function about()
       {          
           $data['page_title'] = 'About Us';
           $this->load->view('about_page_view', $data); 
       }
       
       function privacy()
       {          
           $data['page_title'] = 'Privacy';
           $this->load->view('privacy_page_view', $data); 
       }
       
       function pricing()
       {          
           $data['page_title'] = 'Pricing';
           $this->load->view('pricing_page_view', $data); 
       }
       
       function support()
       {          
           $data['page_title'] = 'Seekmi Support';
           $this->load->view('support_page_view', $data); 
       }
       
       function contact_us()
       {          
           $data['page_title'] = 'Contact Us';
           $this->load->view('contact_us_page_view', $data); 
       }
       
       function partner_program()
       {          
           $data['page_title'] = 'Partner Program';
           $this->load->view('partner_program_page_view', $data); 
       }
       
       function terms_service()
       {          
           $data['page_title'] = 'Terms of Service';
           $this->load->view('terms_service_page_view', $data); 
       }
       
       function submit_request()
       {
           echo 'success';
       }
	
       function logged_in_user()
       {
            if($this->session->userdata('userId')=='')
            {
                 redirect($this->config->config['base_url']);
            }
	}
        
        function changeLanguage() {

        $cookie = array(
            'name' => 'language',
            'value' => trim($this->input->post('language')),
            'expire' => time() + 86500,
        );
        set_cookie($cookie);
        
        $cookie = array(
            'name' => 'wp_language',
            'value' => (trim($this->input->post('language'))=='english')?'en':'id',
            'expire' => time() + 86500,
        );
        set_cookie($cookie);
        
        $this->session->unset_userdata('language');
        $this->session->set_userdata('language', trim($this->input->post('language')));
        //print_R($this->session->all_userdata());
    }

}
?>