<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HAuth extends CI_Controller {

	
        function __construct()
        {
            parent::__construct();    
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('user_model');
            $this->load->library('session');            
            date_default_timezone_set($this->config->config['constTimeZone']);       
        }
        
        public function index()
	{
                
		$this->load->view('hauth/home');
	}

	public function login($provider)
	{            
            log_message('debug', "controllers.HAuth.login($provider) called");
            try
            {
                
                log_message('debug', 'controllers.HAuth.login: loading HybridAuthLib');
                $this->load->library('HybridAuthLib');
          
                if ($this->hybridauthlib->providerEnabled($provider))
                {                   
                    log_message('debug', "controllers.HAuth.login: service $provider enabled, trying to authenticate.");
                    $service = $this->hybridauthlib->authenticate($provider);

                    if ($service->isUserConnected())
                    {
                        log_message('debug', 'controller.HAuth.login: user authenticated.');
                        $user_profile = $service->getUserProfile();                       
                        log_message('info', 'controllers.HAuth.login: user profile:'.PHP_EOL.print_r($user_profile, TRUE));

                        //$data['user_profile'] = $user_profile;

                        //$this->load->view('hauth/done',$data);                                        
                        $today=date('Y-m-d H:i:s');
                        $checkValid=$this->user_model->checkEmailExists($user_profile->email);
                        if($checkValid==1){
                            $pdata['firstname']=$user_profile->firstName; 
                            $pdata['lastname']=$user_profile->lastName; 
                            $pdata['email']=$user_profile->email;
                            $pdata['googlePlusProfileId']=$user_profile->identifier;   
                            $pdata['userPhoto']=$user_profile->photoURL;
                            $pdata['createdDate']=$today; 
                            $pdata['modifiedDate']=$today; 
                            if (!empty($_SERVER['HTTP_CLIENT_IP'])){
                                 $ip=$_SERVER['HTTP_CLIENT_IP'];
                               //Is it a proxy address
                            }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                               $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
                            }else{
                               $ip=$_SERVER['REMOTE_ADDR'];
                            }
                            $pdata['ip_address']=$ip;
                            if($ip!=''){
                               $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
                               $locStr='';
                               if(isset($details->city)){
                                    $locStr.=$details->city;
                                }
                                if(isset($details->region)){
                                    if($locStr!=''){
                                        $locStr.=", ";
                                    }
                                    $locStr.=$details->region;
                                }
                                if(isset($details->country)){
                                    if($locStr!=''){
                                        $locStr.=", ";
                                    }
                                    $locStr.=$details->country;
                                }
                                $pdata['locationInfo']=$locStr;
                                if(isset($details->loc)){
                                  $pdata['latlong']=$details->loc;
                                }else{
                                  $pdata['latlong']='';  
                                }
                            }else{
                               $pdata['locationInfo']='';
                               $pdata['latlong']=''; 
                            }
                            $pdata['browserInfo']=$this->browser();
                            $pdata['userType']='user';
                            $pdata['status']='N';
                            $this->db->insert('tblseekmeusers', $pdata); 
                            $userId=$this->db->insert_id();                            
                        }else{
                            $user_row=$this->user_model->getUserIdByEmail($user_profile['email']);
                            $userId=$user_row->userId;   
                        }
                        $row=$this->user_model->getUserInfo($userId);
                        $this->session->set_userdata('user_email',$user_profile['email']);
                        $this->session->set_userdata('userId',$row->userId);
                        $this->session->set_userdata('user_type',$row->userType);
                        $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
                        $this->session->set_userdata('user_photo',$row->userPhoto);
                        $this->session->set_userdata('facebook_id',$row->facebookProfileId);
                        $this->session->set_userdata('google_plus_id',$row->googlePlusProfileId);
                        $this->session->set_userdata('is_logged_in',true); 
                        redirect('profile/dashboard');
                    }
                    else // Cannot authenticate user
                    {
                            show_error('Cannot authenticate user');
                    }
                }
                else // This service is not enabled.
                {
                        log_message('error', 'controllers.HAuth.login: This provider is not enabled ('.$provider.')');
                        show_404($_SERVER['REQUEST_URI']);
                }
            }
            catch(Exception $e)
            {
                    $error = 'Unexpected error';
                    switch($e->getCode())
                    {
                            case 0 : $error = 'Unspecified error.'; break;
                            case 1 : $error = 'Hybriauth configuration error.'; break;
                            case 2 : $error = 'Provider not properly configured.'; break;
                            case 3 : $error = 'Unknown or disabled provider.'; break;
                            case 4 : $error = 'Missing provider application credentials.'; break;
                            case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
                                     //redirect();
                                     if (isset($service))
                                     {
                                            log_message('debug', 'controllers.HAuth.login: logging out from service.');
                                            $service->logout();
                                     }
                                     show_error('User has cancelled the authentication or the provider refused the connection.');
                                     break;
                            case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
                                     break;
                            case 7 : $error = 'User not connected to the provider.';
                                     break;
                    }

                    if (isset($service))
                    {
                            $service->logout();
                    }

                    log_message('error', 'controllers.HAuth.login: '.$error);
                    show_error('Error authenticating user.');
            }
	}

	public function endpoint()
	{
            

		log_message('debug', 'controllers.HAuth.endpoint called.');
		log_message('info', 'controllers.HAuth.endpoint: $_REQUEST: '.print_r($_REQUEST, TRUE));

		if ($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			log_message('debug', 'controllers.HAuth.endpoint: the request method is GET, copying REQUEST array into GET array.');
			$_GET = $_REQUEST;
		}

		log_message('debug', 'controllers.HAuth.endpoint: loading the original HybridAuth endpoint script.');
		require_once APPPATH.'/third_party/hybridauth/index.php';

	}
        
        function browser()
        {
           $user_agent = $_SERVER['HTTP_USER_AGENT'];
           $browsers = array(
                               'Chrome' => array('Google Chrome','Chrome/(.*)\s'),
                               'MSIE' => array('Internet Explorer','MSIE\s([0-9\.]*)'),
                               'Firefox' => array('Firefox', 'Firefox/([0-9\.]*)'),
                               'Safari' => array('Safari', 'Version/([0-9\.]*)'),
                               'Opera' => array('Opera', 'Version/([0-9\.]*)')
                               ); 

           $browser_details = array();

               foreach ($browsers as $browser => $browser_info){
                   if (preg_match('@'.$browser.'@i', $user_agent)){
                       $browser_details['name'] = $browser_info[0];
                           preg_match('@'.$browser_info[1].'@i', $user_agent, $version);
                       $browser_details['version'] = $version[1];
                           break;
                   } else {
                       $browser_details['name'] = 'Unknown';
                       $browser_details['version'] = 'Unknown';
                   }
               }

           return $browser_details['name'].' Version: '.$browser_details['version'];
       }
}

/* End of file hauth.php */
/* Location: ./application/controllers/hauth.php */
