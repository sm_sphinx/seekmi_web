<?php
class App extends CI_Controller
{
       function __construct()
       {
           parent::__construct();    
           $this->load->helper('url');
	   $this->load->database();
	   $this->load->model('app_model');
	   $this->load->library('session');
	   $this->load->library('pagination'); 
           $this->const_data['const_business_email'] = 'demo1@demo.com'; //live : pay@mirasexy.com / DEMO : demo1@demo.com;
           $this->const_data['const_production'] = false; //Its false by default and will use sandbox   
           date_default_timezone_set($this->config->config['constTimeZone']);
       }
       
       function saveAppInfo()
       {
            //print_r($_POST);
            //print_r($_FILES);
            //die;
            $this->logged_in_user();		
            $data['appname'] = $this->input->post('app_name');
            $data['appemail'] = $this->input->post('app_email'); 
            $data['website'] = $this->input->post('app_website');
            $data['company'] = $this->input->post('app_company');
            $data['address'] = $this->input->post('app_address');
            $data['city'] = $this->input->post('app_city');
            $data['state'] = $this->input->post('app_state');
            $data['country'] = $this->input->post('app_country');
            $data['zipcode'] = $this->input->post('app_zipcode');
            $data['phone'] = $this->input->post('app_phone');
            $data['cellphone'] = $this->input->post('app_cellphone');            
            $data['otherinfo'] = $this->input->post('app_otherinfo');
            $data['other_property_link'] = $this->input->post('app_otherlink');
            //$app_logo_hid = $this->input->post('app_logo_hid');            
            $data['userId'] = $this->input->post('user_id');
            $appId = $this->input->post('app_id');             
            $today = date('Y-m-d H:i:s');
            //$flag='';
            /*if($_FILES['app_logo']['name']!='')
            {
               $rename = '';		
               $filename = $_FILES['app_logo']['name'];
               $ext = explode(".",$filename);
               $config['upload_path'] = './app_logo/';
               $config['allowed_types'] = 'gif|jpg|png';
               $config['max_size'] = '0';

               $config['remove_spaces'] = TRUE;
               $rename = uniqid('logo_');
               $config['file_name'] = $rename;	
               $this->load->library('upload');			
               $this->upload->initialize($config);

               if($this->upload->do_upload('app_logo'))
               {
                  $data['applogo'] =  $rename.".".$ext[1];				 
               }
               else
               {
                  echo $this->upload->display_errors();exit();
               }
            }*/           
            
            if($appId==''){               
                 $data['createdDate'] = $today;
                 $data['modifiedDate'] = $today;
                 $data['address_updated']='Y';
                 $data['status'] = 'Draft';                 
                 $this->app_model->insertApp($data); 
                 $appId=$this->db->insert_id();
                 $flag='success';             
            }else{
                $app_res = $this->app_model->getAppInfo($appId);
                if($app_res->latitude!='0.00000000' || $app_res->longitude!='0.00000000'){
                    if($data['address']!=$app_res->address || $data['city']!=$app_res->city || $data['country']!=$app_res->country || $data['zipcode']!=$app_res->zipcode){
                      $data['address_updated']='Y';  
                    }else{                  
                      //$data['address_updated']='N';                 
                    }
                }else{
                    $data['address_updated']='Y';     
                }
                $data['modifiedDate'] = $today;
                /*if($_FILES['app_logo']['name']!='')
                {			
                    if($app_logo_hid!='')
                    {
                        $imageURL= './app_logo/'.$app_logo_hid;
                        if(file_exists($imageURL))
                        {
                            unlink($imageURL);
                        }
                    }				
                }
                else
                {
                   $data['applogo'] = $app_logo_hid;
                } */               
                $this->app_model->updateApp($data,$appId);
                $flag='success';
            }
            redirect('user/app_step2/'.$data['userId'].'/'.$appId);
	}
        
       function saveMapLatLong()
       {
            $this->logged_in_user();          
            $data['latitude'] = $this->input->post('latitude');
            $data['longitude'] = $this->input->post('longitude');  
            $data['address_updated']='N'; 
            $data['userId'] = $this->input->post('user_id');
            $appId = $this->input->post('app_id');             
            $today = date('Y-m-d H:i:s');            
            
            if($appId!=''){                                
                $data['modifiedDate'] = $today;               
                $this->app_model->updateApp($data,$appId);
                $flag='success';
            }
            redirect('user/app_step3/'.$data['userId'].'/'.$appId);
       }
        
       function saveMapInfo()
       {
            //print_r($_POST);
            //print_r($_FILES);
            //die;
            $this->logged_in_user();		
            $map_image_hid = $this->input->post('map_image_hid');
            $data['userId'] = $this->input->post('user_id');
            $appId = $this->input->post('app_id');             
            $today = date('Y-m-d H:i:s');
            
            //$flag='';
            /*if($_FILES['map_image']['name']!='')
            {
               $rename = '';		
               $filename = $_FILES['map_image']['name'];
               $ext = explode(".",$filename);
               $config['upload_path'] = './map_images/';
               $config['allowed_types'] = 'gif|jpg|png';
               $config['max_size'] = '0';

               $config['remove_spaces'] = TRUE;
               $rename = uniqid('map_');
               $config['file_name'] = $rename;	
               $this->load->library('upload');			
               $this->upload->initialize($config);

               if($this->upload->do_upload('map_image'))
               {
                  $data['map_image'] =  $rename.".".$ext[1];				 
               }
               else
               {
                  echo $this->upload->display_errors();exit();
               }
            }*/
            if($this->input->post('img_val')!=''){
                //Get the base-64 string from data
                $filteredData=substr($this->input->post('img_val'), strpos($this->input->post('img_val'), ",")+1);
                //Decode the string
                $unencodedData=base64_decode($filteredData);
                //Save the image
                $rename = uniqid('map_');
                file_put_contents('./map_images/'.$rename.'.jpeg', $unencodedData);
                $data['map_image'] = $rename.'.jpeg';
            }else{
                $data['map_image'] = $map_image_hid;
            }
            
            $data['modifiedDate'] = $today;
            if($this->input->post('img_val')!='')
            {			
                if($map_image_hid!='')
                {
                    $imageURL= './map_images/'.$map_image_hid;
                    if(file_exists($imageURL))
                    {
                        unlink($imageURL);
                    }
                }				
            }
            else
            {
               $data['map_image'] = $map_image_hid;
            }
            
            $this->app_model->updateApp($data,$appId);                     
            redirect('user/app_step4/'.$data['userId'].'/'.$appId);
	}
        
       function saveAppImages()
       {
            //print_r($_POST);
            //print_r($_FILES);
            //die;
            $this->logged_in_user();                        
            $data['userId'] = $this->input->post('user_id');
            $appId = $this->input->post('app_id');             
            $today = date('Y-m-d H:i:s');
            $appImage = array();
            $old_image=array();
            $flg=false;
            $old_image[1] =$this->input->post('app_image_old_1');		
            $old_image[2] =$this->input->post('app_image_old_2');		
            $old_image[3] =$this->input->post('app_image_old_3');		
            $old_image[4] =$this->input->post('app_image_old_4');
            $old_image[5] =$this->input->post('app_image_old_5');
            $old_image[6] =$this->input->post('app_image_old_6');
           /* for($i=1; $i<=6; $i++)
            {
                if($_FILES['app_image'.$i]['name']!='')
                {
                        $flg=true;
                        $rename = '';
                        $image_upload = 'app_image'.$i;
                        $filename = $_FILES[$image_upload]['name'];
                        $ext = explode(".",$filename);
                        $config['upload_path'] = './app_images/';
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = '600';                           
                        $config['remove_spaces'] = TRUE;
                        $rename = uniqid('image_').'_'.$i;
                        $config['file_name'] = $rename;	
                        $this->load->library('upload');			
                        $this->upload->initialize($config);

                    if($this->upload->do_upload($image_upload))
                    {  
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = './app_images/'.$rename.".".$ext[1];
                        $config['new_image'] = './app_images/';
                        $config['file_name'] = $rename;
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 640;
                        $config['thumb_marker'] = "";
                        $this->load->library('image_lib');
                        $this->image_lib->initialize($config);				  
                        $this->image_lib->resize();
                        
                        if($old_image[$i]!='')
                        {
                             $imageURL= './app_image/'.$old_image[$i];
                             if (file_exists($imageURL))
                             {
                                 unlink($imageURL);
                             }                                
                         }					  
                         $old_image[$i] =  $rename.".".$ext[1];				 
                       }
                       else
                       {
                          echo $this->upload->display_errors();exit();
                       }
                }			
                unset($config);
            }*/
           // if($flg==true){
            $this->db->delete('tblappimages', array('appid'=>$appId));		
            for($i=1; $i<= 6; $i++)
            {
              if($old_image[$i]!='')
              {
                    $sql_arr = array('appid' => $appId,
                                     'appimage' => $old_image[$i]
                                    ); 
                    $this->db->insert('tblappimages', $sql_arr);
              }
            }
            //}                  
            redirect('user/app_step5/'.$data['userId'].'/'.$appId);
	}
        
        function uploadAppImage(){
            require('./extras/Uploader.php');
            $upload_dir = './upload_files/';
            //$this->load->library('FileUpload');
            $uploader = new FileUpload('uploadfile');

            // Handle the upload
            $result = $uploader->handleUpload($upload_dir);           

            if (!$result) {
              exit(json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg())));  
            }
            $config['image_library'] = 'gd2';
            $config['source_image'] = $uploader->getSavedFile();
            $config['new_image'] = './app_images/';
            $config['file_name'] = $uploader->getFileName();
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 640;
            $config['thumb_marker'] = "";
            $this->load->library('image_lib');
            $this->image_lib->initialize($config);				  
            $this->image_lib->resize();
            if(file_exists($uploader->getSavedFile())){
                unlink($uploader->getSavedFile());
            }
            echo json_encode(array('success' => true,'file'=>$uploader->getFileName()));
        }
        
        function preview(){
            $this->logged_in_user();                        
            $data['userId'] = $this->input->post('userId');
            $appId = $this->input->post('appId'); 
            $type = $this->input->post('type');
            $this->load->model('app_model');
            $data['app_info'] = $this->app_model->getAppInfo($appId);
            if($type=='home'){
                $this->load->view('app_home_preview_view',$data); 
            }
            if($type=='info'){
                $this->load->view('app_info_preview_view',$data); 
            }
            if($type=='photos'){  
                $data['app_images'] = $this->app_model->getAppImages($appId);
                $this->load->view('app_photos_preview_view',$data); 
            }
            if($type=='otherinfo'){               
                $this->load->view('app_other_info_preview_view',$data); 
            }
            if($type=='waypointmap'){               
                $this->load->view('app_waypoint_map_preview_view',$data); 
            }
            if($type=='offline_osm'){               
                $this->load->view('app_offline_osm_preview_view',$data); 
            }
            if($type=='online_osm'){               
                $this->load->view('app_online_osm_preview_view',$data); 
            }
            if($type=='online_map'){               
                $this->load->view('app_online_map_preview_view',$data); 
            }
            
        }
        
        function savePaymentInfo(){
            $userid = (int) $this->session->userdata('userId');            
            $appid = trim($this->input->post('app_id'));
            $payment_type = trim($this->input->post('payment_type'));
            $amount = trim($this->input->post('app_prc'));
            
            if($payment_type == 'paypal') {
                $title = "APP creation in Montego";
                $custom_str = $appid;
                $config['business'] = $this->const_data['const_business_email'];
                $config['cpp_header_image'] = ''; //Image header url [750 pixels wide by 90 pixels high]
                $config['return'] = '' . $this->config->config['base_url'] . 'app/success_payment';
                $config['cancel_return'] = '' . $this->config->config['base_url'] . 'user/app_step6/'.$appid;
                $config['notify_url'] = '' . $this->config->config['base_url'] . 'app/notify_payment';
                $config['production'] = $this->const_data['const_production']; //Its false by default and will use sandbox
                //$config['discount_rate_cart'] 	= 20; //This means 20% discount
                $config["invoice"] = $userid; //The invoice id
                $config["lc"] = 'EN'; //Languaje [EN,ES]
                $config["currency_code"] = 'EUR'; //http://bit.ly/anciiH
                $config["tax"] = '';
                $config["custom"] = $custom_str;

                $this->load->library('paypal', $config);

                #$this->paypal->add(<name>,<price>,<quantity>[Default 1],<code>[Optional]);

                $this->paypal->add($title, $amount, 1); //First item
                //$this->paypal->add('Pants',40); 	  //Second item
                //$this->paypal->add('Blowse',10,10,'B-199-26'); //Third item with code
                $this->paypal->pay(); //Proccess the payment
            } else {
                $cr_no = trim($this->input->post('cr_no'));
                $cr_name = trim($this->input->post('cr_name'));
                $cr_mn = trim($this->input->post('cr_mn'));
                $cr_yr = trim($this->input->post('cr_yr'));
                $cr_cvv = trim($this->input->post('cr_cvv'));
                $cr_fname = trim($this->input->post('cr_fname'));
                $cr_lname = trim($this->input->post('cr_lname'));
                $txtaddress = trim($this->input->post('bill_address'));
                $country = trim($this->input->post('bill_country'));
                $state = trim($this->input->post('bill_state'));
                $city = trim($this->input->post('bill_city'));
                $txtzip = trim($this->input->post('bill_zip'));                
                $mobile = trim($this->input->post('bill_phone'));
                $email = $this->session->userdata('user_email');
                $description = 'APP creation in Montego';

                // Authorize.net lib
                $this->load->library('authorize_net');

                $auth_net = array(
                    'x_card_num' => $cr_no, // 4111111111111111 Visa
                    'x_exp_date' => $cr_mn . '/' . $cr_yr,
                    'x_card_code' => $cr_cvv,
                    'x_description' => $description,
                    'x_amount' => $amount,
                    'x_first_name' => $cr_fname,
                    'x_last_name' => $cr_lname,
                    'x_address' => $txtaddress,
                    'x_city' => $city,
                    'x_state' => $state,
                    'x_zip' => $txtzip,
                    'x_country' => 'US',
                    'x_phone' => $mobile,
                    'x_email' => $email,
                    'x_customer_ip' => $this->input->ip_address()
                );

                $this->authorize_net->setData($auth_net);
                if ($this->authorize_net->authorizeAndCapture()) {
                    echo $transactionId = $this->authorize_net->getTransactionId();
                    die;
                    $approvalCode = $this->authorize_net->getApprovalCode();
                    $today = date('Y-m-d H:i:s');

                    $data_order = array(                     
                        'price' => $amount,                        
                        'createdOn' => $today,                       
                        'appid' => $appid,
                        'userid' => $userid,
                        'order_status' => 'success'
                    );
                    $this->app_model->insertData('tblorder', $data_order);  
                    $lastId=$this->db->insert_id();
                    $order_detail_data=array(
                        'orderid'=>$lastId,
                        'price' => $amount,                    
                        'transactionid' => $transactionId,
                        'approval_code' => $approvalCode,
                        'payment_type' => $payment_type,
                        'createdOn' => $today
                    );
                    $this->app_model->insertData('tblorderdetail', $order_detail_data); 
                    $this->app_model->updateApp(array('status'=>'Pending'),$appid);
                    redirect('app/payment_success/'.$transactionId);
                }else{
                    $error=$this->authorize_net->getError();
                    redirect('app/payment_cancel');
                    //$data['page_title'] = 'Payment Cancelled';
                    //$data['error'] = $error;
                    //$this->load->view('payment_cancel_page_view', $data);
                }
            }
        }
        
        public function success_payment() {
            $txn_id = '';
            if ($this->input->post()) {
                $received_post = $this->input->post();
                $txn_id = $received_post['txn_id'];
                $this->palceOrder($received_post);
            }
            redirect('app/payment_success/' . $txn_id . '');
        }

        public function notify_payment() {
            /* echo "<pre>";
              $received_post = $this->input->post();
              print_r($received_post); */
            $received_post = $this->input->post();
            //print_r($received_post);
            $this->palceOrder($received_post);
        }
        
        function palceOrder($received_post) {
            //echo $payer_id = $received_post['payer_id'];
            //echo "<pre>";
            //print_r($received_post);

            $payer_id = $received_post['payer_id'];
            $txn_id = $received_post['txn_id'];
            $payment_date = date('Y-m-d H:i:s', strtotime($received_post['payment_date']));
            $payer_email = $received_post['payer_email'];
            $total_amount = $received_post['mc_gross'];
            $userid = $received_post['invoice'];
            //$total_tax = $received_post['tax'];
            $appid = $received_post['custom'];
            $payment_type = 'paypal';
            $today = date('Y-m-d H:i:s');
            $start_date = date('Y-m-d');

            $transactionID_cnt = $this->app_model->checkIfOrderExists($txn_id, $payer_id);
            if($transactionID_cnt == 0) {
                $order_data = array(                    
                    'appid' => $appid,
                    'userid' => $userid,
                    'price' => $total_amount,                    
                    'order_status' => 'success',
                    'createdOn' => $today                    
                );                
                $this->app_model->insertData('tblorder', $order_data); 
                $lastId=$this->db->insert_id();
                $order_detail_data=array(
                    'orderid'=>$lastId,
                    'price' => $total_amount,                    
                    'transactionid'=>$txn_id,
                    'payment_type' => $payment_type,                    
                    'payer_email' => $payer_email,
                    'payer_id' => $payer_id,
                    'createdOn' => $today
                );
                $this->app_model->insertData('tblorderdetail', $order_detail_data); 
                $this->app_model->updateApp(array('status'=>'Pending'),$appid);
            }
    }

    public function payment_cancel() 
    {                
        $data['page_title'] = 'Payment Cancelled';
        $this->load->view('payment_cancel_page_view', $data);
    }
    
    function payment_success($txnid)
    {
        $data['page_title'] = 'Payment Success';
        $data['txnid'] = $txnid;
        $this->load->view('payment_success_page_view', $data);
    }
    
    function my_apps(){
        $uid=$this->input->post('userId');
        $data['app_count']=$this->app_model->getUserAppsCount($uid);  
        $data['app_data']=$this->app_model->getUserApps($uid); 
        $this->load->view('my_application_page_view', $data);
    }
    
    function delete_app(){
        $appid=$this->input->post('appId');
        $data['status']='Deleted';
        $this->app_model->updateApp($data,$appid);
        echo 'success';
    }
    
    function my_transactions(){
        $uid=$this->input->post('userId');
        $data['trans_count']=$this->app_model->getUserTransactionsCount($uid);  
        $data['trans_data']=$this->app_model->getUserTransactions($uid); 
        $this->load->view('my_transaction_page_view', $data);
    }
    
    function checkDuplicateApp()
    {
        $this->logged_in_user();
        $app_id = (int)trim($this->input->post('app_id'));
        $app_name = trim($this->input->post('app_name'));
        $res = $this->app_model->checkAppExists($app_name,$app_id); 
        if($res==0)
        {
           echo 'success';
        }
        else
        {
           echo "already";
        }
    }
        
    function logged_in_user()
    {
        if($this->session->userdata('userId')=='')
        {
             redirect($this->config->config['base_url']);
        }
    }	
}
?>