<?php
class Services extends CI_Controller
{
       function __construct()
       {
           parent::__construct();    
           $this->load->helper('url');
	   $this->load->database();	   
	   $this->load->library('session');
           $this->load->model('service_model');
	   $this->load->library('pagination');              
       }
       
       function search($services)
       {    
           if($services!=''){
                $data['page_title'] = 'Search';
                $data['service']=urldecode($services);
                //$data['zipcode']=$zipcode;                
                $question_list=$this->service_model->getQuestionariesList($data['service']);    
                
                if(empty($question_list)){                   
                    $dbServiceRes=$this->service_model->getServiceIdByTag($data['service']); 
                    if(!empty($dbServiceRes)){
                        if($dbServiceRes->isEnglish=='Y'){
                           $question_list=$this->service_model->getQuestionariesList($dbServiceRes->subserviceName);  
                        }else{
                           $question_list=$this->service_model->getQuestionariesList($dbServiceRes->subserviceNameIND);  
                        }
                    }
                }
                $option_arr=array();
                $question_arr=array();
                $serviceId='';
                if(!empty($question_list)){
                    foreach($question_list as $question_row){
                        $question_arr[$question_row->type][]=$question_row;
                        $option_list=$this->service_model->getOptionList($question_row->questionId);
                        if($option_list!=''){
                            foreach($option_list as $option_row){
                                $option_arr[$question_row->questionId][]=$option_row;
                            }
                        }
                       $serviceId=$question_row->serviceId;
                    }
                   $srch_keyword_arr=array('searchQueryKeyword'=>$data['service'],'isExists'=>'Y','isSuggested'=>'N','createdDate'=>date('Y-m-d H:i:s'));
                   $this->db->insert('tblsearchquery', $srch_keyword_arr); 
                }else{
                   $srch_keyword_arr=array('searchQueryKeyword'=>$data['service'],'isExists'=>'N','isSuggested'=>'Y','createdDate'=>date('Y-m-d H:i:s'));
                   $this->db->insert('tblsearchquery', $srch_keyword_arr);  
                }
                $service_arr=explode(' ',$data['service']);
                $data['suggestion_list']=$this->service_model->getServiceSuggestionList($service_arr);                 
                /*if(empty($data['suggestion_list'])){
                    $services=substr($data['service'], 0, 3);
                    $data['suggestion_list']=$this->service_model->getServiceSuggestionList($services,'after');  
                }*/
                $data['serviceId']=$serviceId;
                /*$data['pros_count']=$this->service_model->getProsCountForService($serviceId);
                if($data['pros_count']==0){
                    $data['pros_service_list']=$this->service_model->getServiceListForPros();
                }*/
                
                $data['is_featured']=$this->service_model->checkServiceIsFeatured($serviceId);
                $data['question_list']=$question_arr;
                $data['option_list']=$option_arr;          
                $this->load->model('setting_model');
                $data['province_list']=$this->setting_model->getProvincesOtherList();                 
                $this->load->view('search_service_page_view', $data); 
           }else{
               redirect('/');
           }
       }
             
       
       function submit_request()
       {
           /*echo "<pre>";
           print_r($this->input->post());
           die;*/
           $questIds=$this->input->post('questionIds');
           $serviceid=$this->input->post('serviceid');
           //$zipcode=$this->input->post('zipcode');
           $provinceId=$this->input->post('province');
           $cityId=$this->input->post('city');
           $districtId=$this->input->post('district');
           if($questIds!=''){
              $today=date('Y-m-d H:i:s');
              if($this->session->userdata('userId')==''){
                  $this->load->model('user_model');
                  $pdata['useremail']=$this->input->post('usr_email');
                  $pdata['userpass']=$this->input->post('usr_pwd');
                  $pdata['usr_phone']=$this->input->post('usr_phone');
                  $usr_name=$this->input->post('usr_name');
                  $fullname=explode(" ",$usr_name);
                  if(isset($fullname[0])){
                      $pdata['firstname']=$fullname[0];
                  }else{
                      $pdata['firstname']='';
                  }
                  if(isset($fullname[1])){
                      $pdata['lastname']=$fullname[1];
                  }else{
                      $pdata['lastname']='';
                  }
                  $checkusr=$this->user_model->checkEmailExists($pdata['useremail']);
                  if($checkusr==''){                    
                    $row=$this->user_model->check_customer_login($pdata);
                    if($row!=''){
                       if($row->status=='Y'){
                          $this->db->update('tbluser', array('firstname'=>$pdata['firstname'],'lastname'=>$pdata['lastname']), array('userId' => $row->userId));
                          $userId=$row->userId;
                          $usr_row=$this->user_model->getUserInfo($userId);
                          $this->session->set_userdata('user_email',$usr_row->email);
                          $this->session->set_userdata('userId',$usr_row->userId);
                          $this->session->set_userdata('user_type',$usr_row->userType);
                          $this->session->set_userdata('user_full_name',$usr_row->firstname." ".$usr_row->lastname);
                          $this->session->set_userdata('user_photo',$usr_row->userPhoto);
                          $this->session->set_userdata('facebook_id',$usr_row->facebookProfileId);
                          $this->session->set_userdata('is_logged_in',true); 
                       }else{
                          echo "inactive";
                          exit();
                       }
                    }else{                      
                       echo "invalid";
                       exit();
                    }
                  }else{
                      if($this->input->post('user_preference')=='email_sms'){
                          $user_arr= array('firstname'=>$pdata['firstname'],'lastname'=>$pdata['lastname'],'email'=>$pdata['useremail'],'password'=>md5($pdata['userpass']),'userType'=>'user','phone'=>$pdata['usr_phone'],'status'=>'N','smsNotification'=>'Y','createdDate'=>$today,'modifiedDate'=>$today);
                      }else{
                          $user_arr= array('firstname'=>$pdata['firstname'],'lastname'=>$pdata['lastname'],'email'=>$pdata['useremail'],'password'=>md5($pdata['userpass']),'userType'=>'user','phone'=>$pdata['usr_phone'],'status'=>'N','smsNotification'=>'N','createdDate'=>$today,'modifiedDate'=>$today);
                      }                      
                      $this->db->insert('tbluser', $user_arr); 
                      $userId=$this->db->insert_id();
                      $notif_arr=array('userId'=>$userId,'updates'=>'Y','tips'=>'Y','offers'=>'Y','userFeedback'=>'Y','createdDate'=>$today,'modifiedDate'=>$today);
                      $this->db->insert('tblemailnotifications', $notif_arr);
                      $token=md5($userId.time());
                      $this->db->update('tbluser', array('confirmationToken'=>$token), array('userId' => $userId));
                      $activate_url=$this->config->config['base_url'].'user/activate/'.$token;
                      $htmlMessage=file_get_contents($this->config->config['main_base_url'].'signup-email-template.html');
                      $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage); 
                      $htmlMessage=str_replace(' {FULLNAME}', $pdata['firstname']." ".$pdata['lastname'], $htmlMessage);
                      $apikey='EQ4Z41iMdCc7uiznTzzUVw';
                      $this->load->library('mandrill', array($apikey));
                      $mandrill_ready = NULL;
                      try {
                           $this->mandrill->init( $apikey );
                           $mandrill_ready = TRUE;
                      } catch(Mandrill_Exception $e) {
                           $mandrill_ready = FALSE;
                      }
                      if($mandrill_ready) {
                             //Send us some email!
                             $email = array(
                                 'html' => $htmlMessage, //Consider using a view file
                                 'text' => '',
                                 'subject' => 'Your User Account at Seekmi',
                                 'from_email' => 'support@seekmi.com',
                                 'from_name' => 'Seekmi',
                                 'to' => array(array('email' => $pdata['useremail'])) //Check documentation for more details on this one                
                                 );

                             $result = $this->mandrill->messages_send($email);           
                      }
                      $usr_row=$this->user_model->getUserInfo($userId);
                      $this->session->set_userdata('user_email',$usr_row->email);
                      $this->session->set_userdata('userId',$usr_row->userId);
                      $this->session->set_userdata('user_type',$usr_row->userType);
                      $this->session->set_userdata('user_full_name',$usr_row->firstname." ".$usr_row->lastname);
                      $this->session->set_userdata('user_photo',$usr_row->userPhoto);
                      $this->session->set_userdata('facebook_id',$usr_row->facebookProfileId);
                      $this->session->set_userdata('is_logged_in',true); 
                  }
              }else{
                  $userId=$this->session->userdata('userId');
              }
              //echo $userId;
              //die;
              $this->load->model('setting_model');
              $address_row=$this->setting_model->getFullAdrress($provinceId,$cityId,$districtId);
              $address=$address_row->cityName.",".$address_row->districtName.",".$address_row->provName;
              $latlongval=$this->getLnt($address);
              if($latlongval!=''){
                  $latitude=$latlongval['lat'];
                  $longitude=$latlongval['lng'];
              }else{
                  $latitude='';
                  $longitude='';
              }
              $prj_arr=array();
              $prj_arr=array('userId'=>$userId,'serviceId'=>$serviceid,'status'=>'Pending','provinceId'=>$provinceId,'cityId'=>$cityId,'districtId'=>$districtId,'latitude'=>$latitude,'longitude'=>$longitude,'createdDate'=>$today,'modifiedDate'=>$today);
              $this->db->insert('tbluserprojects', $prj_arr);  
              $projectId=$this->db->insert_id();
              $questArr=explode(",",$questIds); 
              for($k=0;$k<count($questArr);$k++){
                $optionIds= $this->input->post('question_'.$questArr[$k]);
                $questType= $this->input->post('qustType_'.$questArr[$k]);
                $otherOptionId= $this->input->post('otherOptionId_'.$questArr[$k]);
                $optionVal= $this->input->post('serviceOtherText_'.$questArr[$k]);
                $optionType= $this->input->post('optionType_'.$questArr[$k]);               
                if($questType=='select' || $questType=='checkbox' || $questType=='radio'){
                    if(is_array($optionIds)){
                         for($i=0;$i<count($optionIds);$i++)
                         {
                             if($optionIds[$i]!=''){
                                $dt_data_array=array();
                                $dt_data_array['projectId'] = $projectId;
                                $dt_data_array['questionId'] = $questArr[$k];                            
                                $dt_data_array['optionId'] = $optionIds[$i];
                                if($otherOptionId==$optionIds[$i]){
                                   $dt_data_array['optionVal'] = $optionVal;
                                }else{
                                   $dt_data_array['optionVal'] =''; 
                                }
                                if($optionType=='miles'){
                                  if($otherOptionId==$optionIds[$i]){
                                    $extra_arr=array();
                                    $extra_arr["miles"]=$this->input->post('miles_input_'.$optionIds[$i]);                                   
                                    $dt_data_array['extras']=json_encode($extra_arr);
                                  }
                                }
                                if($optionType=='datetime_hours'){
                                  $extra_arr=array();
                                  $extra_arr["date"]=$this->input->post('date_input_'.$optionIds[$i]);  
                                  $extra_arr["time"]=$this->input->post('time_input_'.$optionIds[$i]);
                                  $extra_arr["hours"]=$this->input->post('hours_input_'.$optionIds[$i]);                                 
                                  $dt_data_array['extras']=json_encode($extra_arr);                                  
                                }
                                $this->db->insert('tblprojectdetails', $dt_data_array); 
                             }
                         }
                    }else{
                        $dt_data_array=array();
                        $dt_data_array['projectId'] = $projectId;
                        $dt_data_array['questionId'] = $questArr[$k];                            
                        $dt_data_array['optionId'] = $optionIds;
                        if($otherOptionId==$optionIds){
                           $dt_data_array['optionVal'] = $optionVal;
                        }else{
                           $dt_data_array['optionVal'] =''; 
                        }
                        if($optionType=='miles'){
                          if($otherOptionId==$optionIds){
                            $extra_arr=array();
                            $extra_arr["miles"]=$this->input->post('miles_input_'.$optionIds); 
                            $dt_data_array['extras']=json_encode($extra_arr);
                          }
                        }
                        if($optionType=='datetime_hours'){
                          $extra_arr=array();
                          $extra_arr["date"]=$this->input->post('date_input_'.$optionIds);  
                          $extra_arr["time"]=$this->input->post('time_input_'.$optionIds);
                          $extra_arr["hours"]=$this->input->post('hours_input_'.$optionIds);                         
                          $dt_data_array['extras']=json_encode($extra_arr);                                  
                        }
                        $this->db->insert('tblprojectdetails', $dt_data_array); 
                    }
                }else{
                    if($questType=='address'){
                        $optVal=$address;
                    }else{
                        $optVal=$optionIds;
                    }
                    $detail_arr=array();
                    $detail_arr=array('projectId'=>$projectId,'optionId'=>'','questionId'=>$questArr[$k],'optionVal'=>$optVal);
                    $this->db->insert('tblprojectdetails', $detail_arr); 
                }
              }
              /* send project request */
                 if($latitude!='' && $longitude!=''){                   
                   $distancekm=$this->setting_model->getSettingValue('distanceSetting');
                   $expert_list=$this->service_model->getExpertsByProject($serviceid,$latitude,$longitude,$distancekm);                  
                   
                   $apikey='EQ4Z41iMdCc7uiznTzzUVw';
                   $this->load->library('mandrill', array($apikey));
                   $mandrill_ready = NULL;
                   try {
                          $this->mandrill->init( $apikey );
                          $mandrill_ready = TRUE;
                   } catch(Mandrill_Exception $e) {
                          $mandrill_ready = FALSE;
                   }
                   if($mandrill_ready) {
                       
                        if(!empty($expert_list)){
                            
                            foreach($expert_list as $expert_row){
                                $expert_arr=array();
                                $email=array();
                                $pros_firstname=$expert_row->firstname;
                                $pros_lastname=$expert_row->lastname;
                                $expert_arr=array('prosId'=>$expert_row->userId,'projectId'=>$projectId,'status'=>'Pending','addedOn'=>$today,'modifiedOn'=>$today);
                                $this->db->insert('tblquoterequest', $expert_arr); 

                                $request_url=$this->config->config['base_url'].'profile/requests/'.$expert_row->userId;
                                if(get_cookie('language') == 'english') {
                                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/new-job-request-template.html');
                                }else{
                                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/new-job-request-template-id.html');    
                                }
                                $htmlMessage=str_replace('{JOB_REQUEST_URL}', $request_url, $htmlMessage); 
                                $htmlMessage=str_replace('{PROS_FULLNAME}', $pros_firstname." ".$pros_lastname, $htmlMessage);  
                                $htmlMessage=str_replace('{FULLNAME}', $this->session->userdata('user_full_name'), $htmlMessage);    
                                $email = array(
                                    'html' => $htmlMessage, //Consider using a view file
                                    'text' => '',
                                    'subject' => 'Work Request at Seekmi',
                                    'from_email' => 'support@seekmi.com',
                                    'from_name' => 'Seekmi',
                                    'to' => array(array('email' => $expert_row->email)) //Check documentation for more details on this one                
                                    );

                                $this->mandrill->messages_send($email); 
                                // send mail to experts 
                            }
                        }
                   }                   
                   
                    
                 } 
              /* send project request */              
              echo 'success';
              exit();
           }else{
               echo 'failed';
               exit();
           }
       }
       
       function add_credit()
       {
           $this->logged_in_user();
           $data['page_title']='Select miCoins Package';
           $this->load->model('setting_model');
           $data['package_list']=$this->setting_model->getCreditPackages();
           $this->load->view('add_credit_page_view',$data); 
       }
       
       function buy_credit($id)
       {
           $this->logged_in_user();
           $data['page_title']='Buy Credit';
           $this->load->model('setting_model');
           $data['package_row']=$this->setting_model->getCreditPackageDetails($id);            
           require_once($_SERVER['DOCUMENT_ROOT'].'/veritrans/Veritrans.php');
           Veritrans_Config::$serverKey = "VT-server-SvEbajij_LiMSprgx6F6A437";
            // Uncomment for production environment
             Veritrans_Config::$isProduction = false;

            // Uncomment to enable sanitization
             Veritrans_Config::$isSanitized = false;

            // Uncomment to enable 3D-Secure
            // Veritrans_Config::$is3ds = true;
            $today=date('Y-m-d H:i:s');
            $order_id=rand();
            $order_data=array('user_id'=>$this->session->userdata('userId'),'order_id'=>$order_id,'order_amount'=>$data['package_row']->amount,'credits'=>$data['package_row']->credit,'createdDate'=>$today,'modifiedDate'=>$today);
            $this->db->insert('tblcredittranscations', $order_data); 
            $params = array(
                "vtweb" => array (
                      "enabled_payments" => array("credit_card","bank_transfer","telkomsel_cash","xl_tunai","bbm_money","indosat_dompetku")
                    ),
               'transaction_details' => array(
                  'order_id' => $order_id,
                  'gross_amount' => $data['package_row']->amount,
                )
            );            
            redirect(Veritrans_VtWeb::getRedirectionUrl($params));          
       }
       
       function payment($type){
           
           if($type=='notification'){
               require_once($_SERVER['DOCUMENT_ROOT'].'/veritrans/Veritrans.php');
               Veritrans_Config::$isProduction = false;
               Veritrans_Config::$serverKey = 'VT-server-SvEbajij_LiMSprgx6F6A437';
               $notif = new Veritrans_Notification();
               $transaction = $notif->transaction_status;
               $type = $notif->payment_type;
               $order_id = $notif->order_id;
               $fraud = $notif->fraud_status;
               $order_data=array(
                   'status_code'=>$notif->status_code,
                   'status_message'=>$notif->status_message,
                   'transaction_id'=>$notif->transaction_id,
                   'masked_card'=>$notif->masked_card,
                   'payment_type'=>$notif->payment_type,
                   'transaction_status'=>$notif->transaction_status,
                   'fraud_status'=>$notif->fraud_status,
                   'approval_code'=>$notif->approval_code,
                   'bank_name'=>$notif->bank,
                   'eci_code'=>$notif->eci,
                   'createdDate'=>$notif->transaction_time
               );
               $this->db->update('tblcredittranscations', $order_data, array('order_id' => $order_id));
               $order_details=$this->service_model->getOrderDetails($order_id);               
               if ($transaction == 'capture') {                   
                  // For credit card transaction, we need to check whether transaction is challenge by FDS or not
                  if($type == 'credit_card'){                      
                    if($fraud == 'challenge'){                       
                      // TODO set payment status in merchant's database to 'Challenge by FDS'
                      // TODO merchant should decide whether this transaction is authorized or not in MAP  
                      } 
                      else {                          
                      // TODO set payment status in merchant's database to 'Success'
                        $this->db->where('userId', $order_details->user_id);
                        $this->db->set('credits', 'credits + ' . (int) $order_details->credits, FALSE); 
                        $this->db->update('tbluser'); 
                      }
                    }
                  }
               else if ($transaction == 'settlement'){
                  // TODO set payment status in merchant's database to 'Settlement'
                  //echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
                    $this->db->where('userId', $order_details->user_id);
                    $this->db->set('credits', 'credits + ' . (int) $order_details->credit, FALSE); 
                    $this->db->update('tbluser'); 
               } 
               else if($transaction == 'pending'){
                // TODO set payment status in merchant's database to 'Pending'
                //echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
               } 
               else if ($transaction == 'deny') {
                // TODO set payment status in merchant's database to 'Denied'
                //echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
               }
           }
           if($type=='finish'){
              $data['page_title']='Payment Success'; 
              $data['order_id']=$this->input->get('order_id');
              $this->load->view('payment_success_page_view',$data);   
           }           
           if($type=='error' || $type=='unfinish'){
              $data['page_title']='Payment Error';
              $this->load->view('payment_cancel_page_view',$data);  
           }           
       }
       
       function add_suggest_service()
       {
           $user_service=$this->input->post('user_service');
           $checkRes=$this->service_model->checkSuggestedService($user_service);
           if($checkRes == 0){
               $data_arr=array('serviceName'=>$user_service,'createdDate'=>date('Y-m-d H:i:s'));
               $this->db->insert('tblsuggestedservices', $data_arr); 
               echo 'success';
           }else{
               echo 'already';
           }
       }
       
       function getLnt($address){
            $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address).",indonesia&sensor=false";
            $result_string = file_get_contents($url);
            $result = json_decode($result_string, true);
            $result1[]=$result['results'][0];
            $result2[]=$result1[0]['geometry'];
            $result3[]=$result2[0]['location'];
            return $result3[0];
        }
              
       function logged_in_user()
       {
            if($this->session->userdata('userId')=='')
            {
                 redirect($this->config->config['base_url']);
            }
	}	
}
?>