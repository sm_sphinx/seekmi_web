<?php
class Pros extends CI_Controller
{
    function __construct()
    {
        parent::__construct();    
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('pros_model');
        $this->load->library('session');
        $this->load->library('pagination');  
        //date_default_timezone_set($this->config->config['constTimeZone']);
    }
    
    function check_email()
    {
        $useremail=$this->input->post('e');
        echo $res = $this->webuser_model->check_customer_email($useremail); 
    }
	 
    function welcome($id='')
    {
       $data['page_title'] = 'Welcome to Seekmi';
       $data['catId']=$id;
       $this->load->model('service_model');
       $data['categories']=$this->service_model->getCategories();   
       $this->load->view('welcome_page_view', $data);       
    }
    
    /*function register($id)
    {
       $data['page_title'] = 'Welcome to Seekmi';
       $data['catId']=$id;
       $this->load->model('service_model');
       $data['services']=$this->service_model->getServicesByCategoryId($id);
       $this->load->model('setting_model');
       $data['province_list']=$this->setting_model->getProvincesOtherList();   
       $this->load->view('register_pros_page_view', $data);       
    }*/
    
    function register($refCode='')
    {
       $data['page_title'] = 'Welcome to Seekmi'; 
       $data['referral_code']=$refCode;
       $this->load->library('recaptcha');
       $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
       $this->load->view('signup_pros_page_view', $data);       
    }
    
//    function register_submit()
//    {
//       //print_r($this->input->post());
//       //die;        
//       $today=date('Y-m-d H:i:s');
//       $pdata['firstname']=$this->input->post('firstName');
//       $pdata['lastname']=$this->input->post('lastName');
//       $pdata['email']=$this->input->post('usr_email');    
//       $pdata['password']=md5($this->input->post('password'));
//       $pdata['streetAddress']=$this->input->post('street'); 
//      // $pdata['aptName']=$this->input->post('unit');  
//       $pdata['city']=$this->input->post('city');  
//       $pdata['district']=$this->input->post('district'); 
//       $pdata['province']=$this->input->post('province'); 
//       $pdata['phone']=$this->input->post('phone'); 
//       $pdata['zipcode']=$this->input->post('zip'); 
//       $pdata['categoryId']=$this->input->post('category'); 
//       if($this->input->post('sms_notification')){
//           $pdata['smsNotification']='Y';
//       }else {
//           $pdata['smsNotification']='N';
//       }
//       $pdata['userType']='professional';
//       $pdata['status']='Y';
//       $pdata['createdDate']=$today;
//       $pdata['modifiedDate']=$today;                  
//       $this->pros_model->insertUser($pdata);
//       $userId=$this->db->insert_id();
//       $cdata['userId']=$userId;
//       $cdata['businessName']=$this->input->post('business_name');
//       $cdata['website']=$this->input->post('website');
//       $cdata['bussinessDescription']=$this->input->post('description');
//       if($this->input->post('privacy')=='1'){
//           $showFullAddress='N';
//       }else{
//           $showFullAddress='Y';
//       }
//       $preferanceArr=$this->input->post('preferences');
//       if(in_array('travel_toprovider',$preferanceArr)){
//           $cdata['travelToProvider']='Y';
//       }else{
//           $cdata['travelToProvider']='N';
//       }
//       
//       if(in_array('travel_remote',$preferanceArr)){
//           $cdata['travelRemote']='Y';
//       }else{
//           $cdata['travelRemote']='N';
//       }
//       $cdata['distanceMiles']='';       
//       if(in_array('travel_tocustomer',$preferanceArr)){
//           $cdata['travelToCustomer']='Y';
//           $cdata['distanceMiles']=$this->input->post('distance');
//       }else{
//           $cdata['travelToCustomer']='N';
//           $cdata['distanceMiles']='';
//       }
//       $this->pros_model->insertProvider($cdata);
//       $servicesArr=$this->input->post('services');
//       if(!empty($servicesArr)){
//           for($k=0;$k<count($servicesArr);$k++){
//               $serv_arr='';
//               if($this->input->post('serviceOtherText_'.$servicesArr[$k])!=''){
//                  $serviceText=$this->input->post('serviceOtherText_'.$servicesArr[$k]);
//               }else{
//                  $serviceText=''; 
//               }
//               $serv_arr=array('userId'=>$userId,'serviceId'=>$servicesArr[$k],'serviceText'=>$serviceText);
//               $this->db->insert('tbluserservices', $serv_arr); 
//           }           
//       }
//       $row=$this->pros_model->getUserInfo($userId);
//       $this->session->set_userdata('user_email',$pdata['email']);
//       $this->session->set_userdata('userId',$row->userId);
//       $this->session->set_userdata('user_type',$row->userType);
//       $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
//       $this->session->set_userdata('user_photo',$row->userPhoto);
//       $this->session->set_userdata('facebook_id',$row->facebookProfileId);
//       $this->session->set_userdata('is_logged_in',true);    
//       if($row->userType=='user'){
//        redirect('profile/dashboard');
//       }else{
//        redirect('profile/requests');   
//       }
//    }
    
    /*function register_submit()
    {
       //print_r($this->input->post());
       //die;        
       $today=date('Y-m-d H:i:s');
       $pdata['firstname']=$this->input->post('firstName');
       $pdata['lastname']=$this->input->post('lastName');
       $pdata['email']=$this->input->post('usr_email');    
       $pdata['password']=md5($this->input->post('password'));
       $pdata['streetAddress']=$this->input->post('street'); 
       //$pdata['aptName']=$this->input->post('unit');  
       $pdata['city']=$this->input->post('city');  
       $pdata['district']=$this->input->post('district'); 
       $pdata['province']=$this->input->post('province'); 
       $pdata['phone']=$this->input->post('phone'); 
       $pdata['zipcode']=$this->input->post('zip'); 
       $pdata['categoryId']=$this->input->post('category'); 
       if($this->input->post('sms_notification')){
           $pdata['smsNotification']='Y';
       }else {
           $pdata['smsNotification']='N';
       }
       $pdata['userType']='professional';
       $pdata['status']='Y';
       $pdata['createdDate']=$today;
       $pdata['modifiedDate']=$today;        
       if (!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip=$_SERVER['HTTP_CLIENT_IP'];
          //Is it a proxy address
       }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
       }else{
          $ip=$_SERVER['REMOTE_ADDR'];
       }
       $pdata['ip_address']=$ip;
       if($ip!=''){
          $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
          $locStr='';
          if(isset($details->city)){
              $locStr.=$details->city;
          }
          if(isset($details->region)){
              if($locStr!=''){
                  $locStr.=", ";
              }
              $locStr.=$details->region;
          }
          if(isset($details->country)){
              if($locStr!=''){
                  $locStr.=", ";
              }
              $locStr.=$details->country;
          }
          $pdata['locationInfo']=$locStr;
          if(isset($details->loc)){
            $pdata['latlong']=$details->loc;
          }else{
            $pdata['latlong']='';  
          }
       }else{
          $pdata['locationInfo']='';
          $pdata['latlong']=''; 
       }
       $pdata['browserInfo']=$this->browser();
       $this->pros_model->insertUser($pdata);
       $userId=$this->db->insert_id();
       $cdata['userId']=$userId;
       $cdata['businessName']=$this->input->post('business_name');
       $cdata['website']=$this->input->post('website');
       $cdata['bussinessDescription']=$this->input->post('description');
       if($this->input->post('privacy')=='1'){
           $showFullAddress='N';
       }else{
           $showFullAddress='Y';
       }
       $preferanceArr=$this->input->post('preferences');
       if(in_array('travel_toprovider',$preferanceArr)){
           $cdata['travelToProvider']='Y';
       }else{
           $cdata['travelToProvider']='N';
       }
       
       if(in_array('travel_remote',$preferanceArr)){
           $cdata['travelRemote']='Y';
       }else{
           $cdata['travelRemote']='N';
       }
       $cdata['distanceMiles']='';       
       if(in_array('travel_tocustomer',$preferanceArr)){
           $cdata['travelToCustomer']='Y';
           $cdata['distanceMiles']=$this->input->post('distance');
       }else{
           $cdata['travelToCustomer']='N';
           $cdata['distanceMiles']='';
       }
       if($pdata['zipcode']!=''){
          $latlongval=$this->getLnt($pdata['zipcode']);
          if($latlongval!=''){
              $cdata['latitude']=$latlongval['lat'];
              $cdata['longitude']=$latlongval['lng'];
          }
       }
       $this->pros_model->insertProvider($cdata);
       $servicesArr=$this->input->post('services');       
       if(!empty($servicesArr)){
           for($k=0;$k<count($servicesArr);$k++){
               $serv_arr='';
               if($this->input->post('serviceOtherText_'.$servicesArr[$k])!=''){
                  $serviceText=$this->input->post('serviceOtherText_'.$servicesArr[$k]);
               }else{
                  $serviceText=''; 
               }
               $serv_arr=array('userId'=>$userId,'serviceId'=>$servicesArr[$k],'serviceText'=>$serviceText);
               $this->db->insert('tbluserservices', $serv_arr); 
           }           
       }
       if($this->input->post('subservice')){
        $subServicesArr=$this->input->post('subservice'); 
        if(!empty($subServicesArr)){
            for($m=0;$m<count($subServicesArr);$m++){
                $sserv_arr='';
                $sserviceText=''; 
                $sserv_arr=array('userId'=>$userId,'serviceId'=>$subServicesArr[$m],'isSubservice'=>'Y','serviceText'=>$sserviceText);
                $this->db->insert('tbluserservices', $sserv_arr); 
            }           
        }
       }
       $row=$this->pros_model->getUserInfo($userId);
       $this->session->set_userdata('user_email',$pdata['email']);
       $this->session->set_userdata('userId',$row->userId);
       $this->session->set_userdata('user_type',$row->userType);
       $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
       $this->session->set_userdata('user_photo',$row->userPhoto);
       $this->session->set_userdata('facebook_id',$row->facebookProfileId);
       $this->session->set_userdata('is_logged_in',true);           
       if($row->userType=='user'){
        redirect('profile/dashboard');
       }else{
        redirect('profile/requests');   
       }
    }*/
    
    
    function register_submit()
    {            
       $today=date('Y-m-d H:i:s');
       $pdata['firstname']=$this->input->post('firstName');
       $pdata['lastname']=$this->input->post('lastName');
       $pdata['email']=$this->input->post('usr_email');    
       $pdata['password']=md5($this->input->post('password'));
       $pdata['services']=implode(',', $this->input->post('services'));
       $referral_code_or_email=$this->input->post('referral_code_or_email'); 
       /*if($this->input->post('sms_notification')){
            $pdata['smsNotification']='Y';
       }else {
            $pdata['smsNotification']='N';
       }*/
       $this->load->library('recaptcha');
       $this->recaptcha->recaptcha_check_answer();
       if($this->recaptcha->getIsValid()) {
             $flag='true';
             if($referral_code_or_email!=''){
                  $checkReferEmailCode=$this->pros_model->checkReferralEmailOrCode($referral_code_or_email);
                  if($checkReferEmailCode!=''){                       
                      $flag='refer_code_email';
                  }else{
                      $flag='false';
                  }
             } 
             
             if($flag=='true' || $flag=='refer_code_email'){
                $pdata['userType']='professional';
                $pdata['status']='N';
                $pdata['createdDate']=$today;
                $pdata['modifiedDate']=$today; 
                if (!empty($_SERVER['HTTP_CLIENT_IP'])){
                     $ip=$_SERVER['HTTP_CLIENT_IP'];
                   //Is it a proxy address
                }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                   $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
                }else{
                   $ip=$_SERVER['REMOTE_ADDR'];
                }
                $pdata['ip_address']=$ip;
                if($ip!=''){
                   $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
                   $locStr='';
                   if(isset($details->city)){
                       $locStr.=$details->city;
                   }
                   if(isset($details->region)){
                       if($locStr!=''){
                           $locStr.=", ";
                       }
                       $locStr.=$details->region;
                   }
                   if(isset($details->country)){
                       if($locStr!=''){
                           $locStr.=", ";
                       }
                       $locStr.=$details->country;
                   }
                   $pdata['locationInfo']=$locStr;
                   if(isset($details->loc)){
                     $pdata['latlong']=$details->loc;
                   }else{
                     $pdata['latlong']='';  
                   }
                }else{
                   $pdata['locationInfo']='';
                   $pdata['latlong']=''; 
                }
                $pdata['browserInfo']=$this->browser();
                $this->pros_model->insertUser($pdata);
                $userId=$this->db->insert_id();
                $cdata['userId']=$userId;              
                $this->pros_model->insertProvider($cdata);   
                $this->load->model('service_model');
                $servicesArr=$this->input->post('services');
                if(!empty($servicesArr)){
                    for($k=0;$k<count($servicesArr);$k++){                        
                        $servId='';
                        $servId=$this->service_model->getServicesId($servicesArr[$k]);
                        if($servId!=''){
                            $serv_arr=array('userId'=>$userId,'serviceId'=>$servId,'serviceText'=>'');
                            $this->db->insert('tbluserservices', $serv_arr); 
                        }                        
                    }           
                }
                
                $token=md5($userId.time());
                $this->db->update('tbluser', array('confirmationToken'=>$token), array('userId' => $userId));
                $activate_url=$this->config->config['base_url'].'pros/activate/'.$token;
                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'signup-vendor-email-template.html');
                $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage);
                $apikey='EQ4Z41iMdCc7uiznTzzUVw';
                $this->load->library('mandrill', array($apikey));
                $mandrill_ready = NULL;
                try {
                      $this->mandrill->init( $apikey );
                      $mandrill_ready = TRUE;
                } catch(Mandrill_Exception $e) {
                      $mandrill_ready = FALSE;
                }
                if($mandrill_ready) {
                      //Send us some email!
                      $email = array(
                          'html' => $htmlMessage, //Consider using a view file
                          'text' => '',
                          'subject' => $this->config->config['subject_for_professional'],
                          'from_email' => 'support@seekmi.com',
                          'from_name' => 'Seekmi',
                          'to' => array(array('email' => $pdata['email'])) //Check documentation for more details on this one                
                          );

                      $result = $this->mandrill->messages_send($email);           
                }       
                //redirect('pros/thankyou');
                /* referral code */
              if($this->input->post('referral_code')!=''){
                  $checkRefer=$this->pros_model->getReferredByUser($this->input->post('referral_code'));
                  if($checkRefer==''){                      
                  }else{
                      $referredById=$checkRefer->userId;
                      $this->load->model('setting_model');
                      $credit=$this->setting_model->getSettingValue('referral_credit');
                      $this->db->where('userId', $userId);
                      $this->db->set('referredBy', $referredById);
                      $this->db->set('credits', 'credits + ' . (int) $credit, FALSE); 
                      $this->db->update('tbluser');
                      
                      $order_data=array('to'=>$referredById,'from'=>$userId,'voucher'=>'Y','creditto'=>0,'creditfrom'=>$credit,'createdDate'=>$today);
                      $this->db->insert('tblrewardsetting', $order_data); 
                      
                  }
              }
              /* referral code*/
              
              /* referral code or email */
              if($flag=='refer_code_email'){                    
                    $user_data=$this->pros_model->getUserInfo($userId);
                    $referredById=$checkReferEmailCode->userId;                            
                    $this->load->model('setting_model');
                    $credit=$this->setting_model->getSettingValue('referral_credit');
                    $this->db->where('userId', $userId);
                    $this->db->set('referredBy', $referredById);
                    $this->db->set('credits', 'credits + ' . (int) $credit, FALSE); 
                    $this->db->update('tbluser');

                    $reward_data=array('to'=>$referredById,'from'=>$userId,'voucher'=>'Y','creditto'=>0,'creditfrom'=>$credit,'createdDate'=>$today);
                    $this->db->insert('tblrewardsetting', $reward_data);
              }
              echo 'success';
            }else{
                echo 'referral_invalid_code_email';
            }
        } else {
          echo 'incorrect_captcha';
       }
    }
    
    function thankyou()
    {
         $data['page_title'] = 'Thank You';
         $data['status']='new';
         $this->load->view('thank_you_pros_page_view', $data);   
    }

    function activate($token)
    {
         $res=$this->pros_model->checkValidConfirmationToken($token); 
         if($res==''){
             $data['status']='invalid';
         }else{
             if($res->status=='Y'){
               $data['status']='already';    
             } else {
               $data['status']='valid';
               $this->db->update('tbluser', array('confirmationToken'=>'','status'=>'Y'), array('userId' => $res->userId));
             }
         }
         $data['page_title'] = 'Activated Account';         
         $this->load->view('confirm_account_pros_page_view', $data);   
    }

    function login()
    {
       $data['page_title'] = 'Welcome back to Seekmi';
       $this->load->view('login_page_view', $data); 
    }
    
    function fb_login(){
        $this->load->library('facebook'); // Automatically picks appId and secret from config
        $user = $this->facebook->getUser(); 

        if($user) {
            $user_profile = $this->facebook->api('/me');
            $today=date('Y-m-d H:i:s');
            $checkValid=$this->user_model->checkEmailExists($user_profile['email']);
            if($checkValid==1){
                $pdata['firstname']=$user_profile['first_name'];
                $pdata['lastname']=$user_profile['last_name'];
                $pdata['email']=$user_profile['email'];
                $pdata['facebookProfileId']=$user_profile['id'];
                $pdata['userPhoto']='https://graph.facebook.com/'.$user_profile['id'].'/picture?type=large';
                $pdata['userType']='user';
                $pdata['status']='Y';
                $pdata['createdDate']=$today;
                $pdata['modifiedDate']=$today; 
                $this->user_model->insertUser($pdata);
                $userId=$this->db->insert_id();
            }else{
                $user_row=$this->user_model->getUserIdByEmail($user_profile['email']);
                $userId=$user_row->userId;                
            }
            $row=$this->user_model->getUserInfo($userId);
            $this->session->set_userdata('user_email',$user_profile['email']);
            $this->session->set_userdata('userId',$row->userId);
            $this->session->set_userdata('user_type',$row->user_type);
            $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
            $this->session->set_userdata('user_photo',$row->userPhoto);
            $this->session->set_userdata('facebook_id',$row->facebookProfileId);
            $this->session->set_userdata('is_logged_in',true);           
            redirect('user/dashboard');
            
        } else {
             $login_url = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('user/fb_login'), 
                'scope' => array("email") // permissions here
            ));
            redirect($login_url);
        }
        
    }
    
    function login_submit(){
        $pdata['useremail']=$this->input->post('login_email');
        $pdata['userpass']=$this->input->post('login_password');
        $rem=$this->input->post('remember');
        $row=$this->user_model->check_customer_login($pdata);			
        if($row!='')
        {		    
            $this->session->set_userdata('user_email',$pdata['useremail']);
            $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
            $this->session->set_userdata('user_photo',$row->userPhoto);
            $this->session->set_userdata('userId',$row->userId);
            $this->session->set_userdata('user_type',$row->userType);
            $this->session->set_userdata('is_logged_in',true);
            $this->session->set_userdata('facebook_id',$row->facebookProfileId);
            if($rem==1){
              $this->input->set_cookie('useremail', $pdata['email'], time()+(60*60*24*365));
              $this->input->set_cookie('password', $pdata['password'], time()+(60*60*24*365));
            }
            echo "success";			 
        }
        else
        {
            echo "failed";
        }
    }
	 
    function logout()
    {	        
        $sess_items = array('user_email' => '', 'userId' => '','is_logged_in'=> false,'user_type'=>'','user_full_name'=>'','user_photo'=>'','facebook_id'=>'');
        $this->session->unset_userdata($sess_items);
         $this->load->library('facebook');
        // Logs off session from website
        $this->facebook->destroySession();
        //$this->session->sess_destroy();
        redirect('/');
    }
    
    function forgot_password()
    {
       $uemail=$this->input->post('frgt_pwd_email');
       $res=$this->user_model->getUserIdByEmail($uemail);
       
       if(!empty($res))
       {
           //echo $uid=$this->user_model->encrypt($res->userId,'Sphinx');
            $uid=base64_encode($res->userId);
        //$res1=$this->webuser_model->updatePassword($uemail,$res->customer_id,$res->customer_first_name,$res->customer_last_name);
            $this->load->library('email');				 			
            $config['mailtype'] = "html";
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");		
            $this->email->to($uemail); 
            $this->email->from('admin@montego.com', 'Montego');
            $this->email->subject('Reset Password at Montego');
            $htmlMessage='<table><tr><td><b>Hello,</b></td></tr></table>';
            $htmlMessage .='<p>Thank you for being a Montego customer.<br/>Please click the following link to reset your password:'.'</p>';
            $htmlMessage .="<table><tr><td><a href='".$this->config->config['base_url']."user/reset_password/".$uid."'>".$this->config->config['base_url']."user/reset_password/".$uid."</a></td></tr>";

            $htmlMessage .="<tr><td>&nbsp;</td></tr><tr><td>Thanks again for being a valued customer. Your privacy is important to us and we appreciate you taking the time to reset your password.</td></tr><tr><td>&nbsp;</td></tr>";
            $htmlMessage .="<tr><td>Thanks,</td></tr>		
            <tr><td>Montego Support</td></tr>
            <tr><td><a href='".$this->config->config['base_url']."'>Montego</a></td></tr>";
            $htmlMessage .='<tr><td></td></tr></table>';
            $this->email->message($htmlMessage);
            //$this->email->send();
            
            if (!$this->email->send())
            {
                echo "Email not sent";      
            }else{
                $this->db->update('tbluser', array('reset_password_flag'=>'N'), array('userId' => $res->userId));
                echo 'sent';
            }
       }
       else
       {
            echo 'Account does not exist with this email.';
       }	   
    }
    function check_email_exists(){
        $email=$this->input->post('user_email');    
        $checkValid=$this->user_model->checkEmailExists($email);
        if($checkValid==1)
        {
          echo 'true';
        }
        else
        {
          echo 'false';
        }
    }   

    function my_profile()
     {       
        if($this->session->userdata('is_logged_in')==true)
        {
         $data['user_data']= $this->user_model->getUserInfo($this->session->userdata('userId')); 
         $data['page_title']='My Profile';
         $data['userId']=$this->session->userdata('userId');         
         //$this->load->model('setting_model');
         //$data['predefined_text'] = $this->setting_model->getWelcomeText();
         $this->load->view('my_profile_page_view',$data); 
        }
        else
        {
          redirect('/');
        } 
     }


     function reset_password($uid)
     {
        //$uid=$this->user_model->decrypt($uid,'Sphinx');
        $uid=base64_decode($uid);
        $data['uid']=$uid; 
        $data['page_title']='Reset Password';
        $data['user_info']=$this->user_model->getUserInfo($uid);
        $this->load->view('reset_password_page_view',$data); 		 
     }

     function change_password()
     {
        $pass=$this->input->post('change_pwd_input');
        $usid=$this->input->post('userid');
        $res= $this->user_model->change_password($pass,$usid); 
        if($res==1)
        {
           $this->db->update('tbluser', array('reset_password_flag'=>'Y'), array('userId' => $usid));
           echo 'success';
        }
     }

     function update_profile()
     {
        $data['firstname']=$this->input->post('profile_fname');
        $data['lastname']=$this->input->post('profile_lname');
        $data['phone']=$this->input->post('profile_phone');
        $data['address']=$this->input->post('profile_address');
        $data['city']=$this->input->post('profile_city');
        $data['state']=$this->input->post('profile_state');
        $data['country']=$this->input->post('profile_country');
        $uid=$this->input->post('prof_userid');
        $res= $this->user_model->updateUser($data,$uid);         
        echo "success";        
     }

     function upload($userid)
    {
        $config['upload_path'] = './user_images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '8192';
        $config['remove_spaces'] = TRUE;
        $rename = strtotime(date("Y-m-d H:i:s"));
        $config['file_name'] = $rename;
        $config['width'] = 170;
        $config['height'] = 166;

        $this->load->library('upload', $config);

        $filename = $_FILES['uploadfile']['name'];
        $ext = explode(".",$filename);
        $size=filesize($_FILES['uploadfile']['tmp_name']);

        if($size > 1025*1024)
        {
           echo "size limit";
        }
        else
        {
            if($this->upload->do_upload('uploadfile'))
            {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './user_images/'.$rename.".".$ext[1];
                    $config['new_image'] = './user_images/thumbs/';
                    $config['file_name'] = $rename;
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 100;
                    $config['height'] = 100;
                    $config['thumb_marker'] = "";

                    $this->load->library('image_lib', $config);

                    $this->image_lib->resize();

                    $res=$this->webuser_model->update_image($userid,$rename.".".$ext[1]);
                    if($res!='')
                    {
                            if(file_exists('./user_images/'.$res))
                            {
                                    unlink('./user_images/'.$res);
                            }
                            if(file_exists('./user_images/thumbs/'.$res))
                            {
                                    unlink('./user_images/thumbs/'.$res);
                            }
                    }
                    echo $rename.".".$ext[1];
            }
            else
            {
                    echo "failed";
            }
        }
    }

    function check_login()
    {
        $pdata['useremail']=$this->input->post('e');
        $pdata['userpass']=$this->input->post('pass');

        $row=$this->webuser_model->check_customer_login($pdata);			
        if($row!='')
        {		    
                echo "success";			 
        }
        else
        {
                echo "failed";
        }
     }
     
    function getSubservices()
    {      
       $serviceid=$this->input->post('services');      
       $this->load->model('service_model');
       $servicelist=$this->service_model->getSubservicesByServiceId($serviceid);   
       $html='';
       if($servicelist!=''){
           foreach($servicelist as $service_row){
               $html.= ' <div class="">
                        <label aria-selected="false" role="option" class="select-option">
                            <input type="checkbox" class="radio" name="subservice[]" value="'.$service_row->subserviceId.'">
                            '.$service_row->subserviceName.'
                        </label>
                    </div>';
           }
       }
       echo $html;
    }
     
     function getCityList()
    {      
       $cid=$this->input->post('id');
       $this->load->model('setting_model');
       $citylist=$this->setting_model->getCityList($cid);      
       $html='<option value="">--Select--</option>';
       if($citylist!=''){
           foreach($citylist as $city_row){
               $html.= '<option value="'.$city_row->cityId.'">'.$city_row->cityName.'</option>';
           }
       }
       echo $html;
    }
    
    function getDistrictList()
    {      
       $did=$this->input->post('id');
       $this->load->model('setting_model');
       $distlist=$this->setting_model->getDistrictList($did);      
       $html='<option value="">--Select--</option>';
       if($distlist!=''){
           foreach($distlist as $dist_row){
               $html.= '<option value="'.$dist_row->districtId.'">'.$dist_row->districtName.'</option>';
           }
       }
       echo $html;
    }
    
    function resend_activation_link()
    {        
        $userId=$this->input->post('userid');
        $row=$this->pros_model->getUserInfo($userId);
        $user_email=$row->email;
        $token=md5($userId.time());
        $this->db->update('tbluser', array('confirmationToken'=>$token,'modifiedDate'=>date('Y-m-d H:i:s')), array('userId' => $userId));
        $activate_url=$this->config->config['base_url'].'user/activate/'.$token;
        $htmlMessage=file_get_contents($this->config->config['main_base_url'].'signup-email-template.html');
        $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage); 
        $htmlMessage=str_replace(' {FULLNAME}', '', $htmlMessage);
        $apikey='EQ4Z41iMdCc7uiznTzzUVw';
        $this->load->library('mandrill', array($apikey));
        $mandrill_ready = NULL;
        try {
             $this->mandrill->init( $apikey );
             $mandrill_ready = TRUE;
        } catch(Mandrill_Exception $e) {
             $mandrill_ready = FALSE;
        }
        if($mandrill_ready) {
             //Send us some email!
             $email = array(
                 'html' => $htmlMessage, //Consider using a view file
                 'text' => '',
                 'subject' => 'Your User Account at Seekmi',
                 'from_email' => 'support@seekmi.com',
                 'from_name' => 'Seekmi',
                 'to' => array(array('email' => $user_email)) //Check documentation for more details on this one                
                 );

             $result = $this->mandrill->messages_send($email);  
             echo 'success';
        } else {
           echo 'failed';   
        }
     }
     
     function assign_service(){
        $user_list=$this->pros_model->getAllProsUsersNotService();
        if($user_list!=''){
            $this->load->model('service_model');
            foreach($user_list as $user_row){
                
                if($user_row->serviceId==NULL && $user_row->services!=''){
                    echo $user_row->userId;
                   
                   $this->db->delete('tbluserservices', array('userId' => $user_row->userId));  
                   $servicesArr=array();
                   $servicesArr=explode(',',$user_row->services);
                    if(!empty($servicesArr)){
                        for($k=0;$k<count($servicesArr);$k++){                            
                            $servId='';
                            $servId=$this->service_model->getServicesId($servicesArr[$k]);
                            if($servId!=''){
                                $serv_arr=array('userId'=>$user_row->userId,'serviceId'=>$servId,'serviceText'=>'');
                                $this->db->insert('tbluserservices', $serv_arr); 
                            }
                        }           
                    }               
                }
               
            }
        }
     } 
        
    function getLnt($zip){
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip).",indonesia&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[]=$result['results'][0];
        $result2[]=$result1[0]['geometry'];
        $result3[]=$result2[0]['location'];
        return $result3[0];
    }
    
    function browser()
     {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $browsers = array(
                            'Chrome' => array('Google Chrome','Chrome/(.*)\s'),
                            'MSIE' => array('Internet Explorer','MSIE\s([0-9\.]*)'),
                            'Firefox' => array('Firefox', 'Firefox/([0-9\.]*)'),
                            'Safari' => array('Safari', 'Version/([0-9\.]*)'),
                            'Opera' => array('Opera', 'Version/([0-9\.]*)')
                            ); 

        $browser_details = array();

            foreach ($browsers as $browser => $browser_info){
                if (preg_match('@'.$browser.'@i', $user_agent)){
                    $browser_details['name'] = $browser_info[0];
                        preg_match('@'.$browser_info[1].'@i', $user_agent, $version);
                    $browser_details['version'] = $version[1];
                        break;
                } else {
                    $browser_details['name'] = 'Unknown';
                    $browser_details['version'] = 'Unknown';
                }
            }

        return $browser_details['name'].' Version: '.$browser_details['version'];
    }
}
?>