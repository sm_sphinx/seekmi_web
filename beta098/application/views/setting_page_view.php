<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

        
    <div class="wrapper content settings" style="min-height: 0px;margin-bottom:30px;">        
        <div class="dynamic-row">
            <div class="column-12">
                <h2><?php echo SETTING_ACCOUNT_TITLE;?></h2>
            </div>
        </div>

        <div class="dynamic-row" style="padding-top:20px;">
            <div class="column-4 picture">
            <?php if($user_data->userPhoto==''){ ?>
              <img data-component="profile-picture" src="<?=$this->config->config['base_url']?>images/img-profile-missing.png">
            <?php }else if($user_data->facebookProfileId!=''){ ?>
                <img data-component="profile-picture" src="<?=$user_data->userPhoto?>">
            <?php }else{ ?>
                <img data-component="profile-picture" src="<?=$this->config->config['base_url']?>user_images/<?=$user_data->userPhoto?>">
            <?php } ?> 
                
                 <a class="change-picture" data-action="upload" href="javascript:;" id="uploadBtn" accept="image/*" capture>                    
                    <span aria-hidden="true" class="icon-font icon-font-camera" id="uploadBtn"></span>
                    <br>
                    <span><?php echo BUTTON_ADD;?></span>
                 </a>
                <div id="msgBox"></div>
               
            </div>

            <div class="column-8 user-info">
                
                <ul>
                    <li>
                        <a href="<?=$this->config->config['base_url']?>profile/account_edit" title="edit name" class="primary user">
                            <?=$user_data->firstname?> <?=$user_data->lastname?>
                            <span class="edit">edit</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->config->config['base_url']?>profile/account_edit" title="edit email" class="primary email">
                            <?=$user_data->email?>
                            <span class="edit"><?php echo BUTTON_EDIT;?></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->config->config['base_url']?>profile/password" title="edit password" class="primary lock">
                            Password
                            <span class="edit"><?php echo BUTTON_EDIT;?></span>
                        </a>
                    </li>
                    <?php if($this->session->userdata('user_type')=='professional'){ ?>
                    <li>
                        <a class="primary location" title="edit address" href="<?=$this->config->config['base_url']?>profile/services#basic_info">
                            <?php 
                            if($user_address->streetAddress!=''){
                                echo $user_address->streetAddress;
                            }
                            if($user_address->cityName!=''){
                                echo ", ".$user_address->cityName;
                            }
                            if($user_address->districtName!=''){
                                echo ", ".$user_address->districtName;
                            }
                            if($user_address->provName!=''){
                                echo ", ".$user_address->provName;
                            }
                            if($user_address->zipcode!=''){
                                echo " ".$user_address->zipcode;
                            }
                            ?>                            
                            <span class="edit"><?php echo BUTTON_EDIT;?></span>
                        </a>
                    </li>
                    <li>
                        <a class="primary phone" title="edit phone" href="<?=$this->config->config['base_url']?>profile/services#basic_info">
                                <?php echo $user_data->phone; ?>
                            <span class="edit"><?php echo BUTTON_EDIT;?></span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
                <hr>
                <div class="mobile-gap"></div>                
                <ul>
                    <li>
                        Referral Url:&nbsp;<input type="text" readonly="" value="<?=$this->config->config['base_url']?>pros/register/<?php echo $referralCode; ?>" name="referral_url" style="width: 492px;"/>
                    </li>
                     <li>
                        Referral Code:&nbsp;<input type="text" readonly="" value="<?php echo $referralCode; ?>" name="referral_code" style="width: 200px;"/>
                    </li>
                </ul>
               </div>
               <div class="mobile-gap">                
             </div>
            </div>
        </div> 
<div class="wrapper content notifications settings" style="min-height: 0px; margin-top:30px; margin-bottom: 30px;">
                
<form accept-charset="ISO-8859-1" action="#" method="post" class="good-form small" name="notification-settings" id="notification-settings">
        <div class="dynamic-row notification-error">
            <div class="column-12">
                <div style="display: none;" class="alert-closable">
                    <input type="checkbox" class="alert-close">
                    <div data-error-target="" class="alert-error">
                    </div>
                </div>
            </div>
        </div>
        <div class="dynamic-row">
            <div class="column-6">
                 <div data-account-notifications="">
                    <h2><?php echo SETTING_ANNOUNCE_TEXT;?></h2>
                    <h3>Email</h3>
                    <ul class="account">
                    <li>              
                        <label class="switch-label"><?php echo SETTING_UPDATE_TEXT;?></label>
                        <div class="switch">
                            <input type="checkbox" value="1" id="updates" class="user_notification" <?php if($notification_data->updates=='Y'){ ?>checked="checked"<?php } ?> name="user_updates">
                           <label class="switch-trigger" for="updates"></label>
                        </div>
                    </li>
                    <li>
                        <label class="switch-label"><?php echo SETTING_PROFILETIPS_TEXT;?></label>
                        <div class="switch">
                            <input type="checkbox" value="1" id="tips" class="user_notification" <?php if($notification_data->tips=='Y'){ ?>checked="checked"<?php } ?> name="user_tips">
                            <label class="switch-trigger" for="tips"></label>
                        </div>
                    </li>
                    <li>
                        <label class="switch-label"><?php echo SETTING_SPECOFFER_TEXT;?></label>
                        <div class="switch">
                            <input type="checkbox" value="1" id="offers" class="user_notification" <?php if($notification_data->offers=='Y'){ ?>checked="checked"<?php } ?> name="user_offers">
                            <label class="switch-trigger" for="offers"></label>
                        </div>
                    </li>
                    <li>
                        <label class="switch-label"><?php echo SETTING_SURVEYS_TEXT;?></label>
                        <div class="switch">
                            <input type="checkbox" value="1" id="userFeedback" class="user_notification" <?php if($notification_data->userFeedback=='Y'){ ?>checked="checked"<?php } ?> name="user_feedback">
                            <label class="switch-trigger" for="userFeedback"></label>
                        </div>
                    </li>
                    </ul>
                </div>
                <div class="mobile-gap"></div>
            </div>
        </div>
</form>
</div>
<div class="wrapper content settings" style="min-height: 0px; margin-top:10px;">   
<ul>
    <li>
        <a href="<?=$this->config->config['base_url']?>profile/delete_account" title="delete account" class="delete-link primary cancel">
            <?php echo BUTTON_DELETE_ACC;?>
        </a>
    </li>
</ul>
</div>
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/SimpleAjaxUploader.js"></script>	
<script type="text/javascript">
$(document).ready(function () {
    $('.user_notification').click(function() {
        var val=''
        if(this.checked==true){
            val='Y';
        }else{
            val='N';
        }
        $.post(Host+'profile/update_notification', { type: this.id, val: val }, function(data) {            
        });  
    });
});

var btn = document.getElementById('uploadBtn')
msgBox = document.getElementById('msgBox');

var uploader = new ss.SimpleUpload({
  button: btn,
  url: Host+'profile/upload_image',
  name: 'uploadfile',
  hoverClass: 'hover',
  focusClass: 'focus',
  responseType: 'json',
  startXHR: function() {
     // progressOuter.style.display = 'block'; // make progress bar visible
      //this.setProgressBar( progressBar );
  },
  onSubmit: function() {
      msgBox.innerHTML = ''; // empty the message box
      btn.innerHTML = 'Uploading...'; // change button text to "Uploading..."
    },
  onComplete: function( filename, response ) {
     
      //btn.innerHTML = 'Choose Another File';
      //progressOuter.style.display = 'none'; // hide progress bar when upload is completed

      /*if ( response =='') {
          msgBox.innerHTML = 'Unable to upload file';
          return;
      }*/

      if (response.success == true ) {
          location.reload();
          //msgBox.innerHTML = '<strong>' + escapeTags( filename ) + '</strong>' + ' successfully uploaded.';          
      } else {
          if ( response.msg )  {
              
              msgBox.innerHTML = escapeTags( response.msg );

          } else {
              
              msgBox.innerHTML = '<?php echo VAL_UPLOAD_ERROR;?>';
          }
      }
    },
  onError: function() {
     
      //progressOuter.style.display = 'none';
      //msgBox.innerHTML = 'Unable to upload file';
    }
  });

</script>
</body>
</html>

