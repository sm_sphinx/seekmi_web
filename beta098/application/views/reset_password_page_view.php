<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Seekmi - <?=$page_title?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<style type="text/css">
.glorious-header1{
    background: none repeat scroll 0 0 #fff!important;
    border-bottom: 1px solid #e6e6e6!important;
}
</style>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid box-shadow multiple-backgrounds ng-scope">
    <?php include('common_view.php'); ?>
    <?php include('header_view.php'); ?>
<div class="wrapper content reset-password-wrapper">
    <div class="dynamic-row">
        <h1 class="title-wrapper">
            <?php echo RESET_TEXT;?>
        </h1>
        <div class="alert-closable alert-hide not-exit-error">            
            <div class="alert-error">
                <p>
                    <?php echo EMAIL_NOT_EXIST_ERROR; ?>
                </p>                
            </div>
        </div>
        <div class="alert-closable alert-hide inactive-error">            
            <div class="alert-error">
                <p>
                    <?php echo USER_INACTIVE_RESET_ERROR; ?>
                </p>                
            </div>
        </div>
        <div class="alert-closable alert-hide banned-error">            
            <div class="alert-error">
                <p>
                   <?php echo USER_BANNED_RESET_ERROR; ?> 
                </p>                
            </div>
        </div>
        <div class="alert-closable alert-hide suspend-error">            
            <div class="alert-error">
                <p>
                   <?php echo USER_SUSPEND_RESET_ERROR; ?> 
                </p>                
            </div>
        </div>
        <div class="box" id="reset-password-success">                          
        <form accept-charset="ISO-8859-1" action="#" method="post" name="reset-password" id="reset-password">
            <fieldset>
               <div class="form-field">
                   <label for="email"><?php echo RESET_EMAIL_TEXT;?></label>
                   <input type="text" tabindex="100" name="usr_email" id="usr_email">
               </div>
            </fieldset>
            <fieldset>
                <button tabindex="101" class="bttn blue submit-bttn" type="submit"><?php echo BUTOON_RESETPASS;?></button>
            </fieldset>
        </form>
     </div>
   </div>
</div>
<? include('footer_mini_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
$(function() {
    $("#usr_email").focus();
});
$(document).ready(function () {
    $("#reset-password").validate({
        rules: {
            usr_email: {
                required: true,
                email:true,
                //remote: '<?=$this->config->config['base_url']?>user/check_email_exists_status'                
            }
        },          
        messages: {            
            usr_email: {
                required: "You must enter an email",
                email: "Please enter a valid email"
                //remote: "There is no account with this email"
            }
        },
        submitHandler: function(form) {
            $.post(Host+'user/forgot_password', $("#reset-password").serialize(), function(data) {
                if(data=='not_exist'){
                    $('.not-exit-error').show();
                    $('.banned-error').hide();
                    $('.suspend-error').hide();
                    $('.inactive-error').hide();
                }else if(data=='banned'){
                    $('.not-exit-error').hide();
                    $('.banned-error').show();
                    $('.suspend-error').hide();
                    $('.inactive-error').hide();
                }else if(data=='banned'){
                    $('.not-exit-error').hide();
                    $('.banned-error').hide();
                    $('.suspend-error').show();
                    $('.inactive-error').hide();
                }else if(data=='inactive'){
                    $('.not-exit-error').hide();
                    $('.banned-error').hide();
                    $('.suspend-error').hide();
                    $('.inactive-error').show();
                }else{
                    $('.not-exit-error').hide();
                    $('.banned-error').hide();
                    $('.suspend-error').hide();
                    $('.inactive-error').hide();
                    $('#reset-password-success').html(data); 
                }
            });     
        }
    });    
});
</script>
</body>
</html>

