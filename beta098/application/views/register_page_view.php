<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Seekmi - <?=$page_title?></title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<style type="text/css">
.glorious-header1{
    background: none repeat scroll 0 0 #fff!important;
    border-bottom: 1px solid #e6e6e6!important;
}
</style>
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<?php include('common_view.php'); ?>
<?php include('header_view.php'); ?>

<div class="wrapper choose-account-wrapper">
    <div class="dynamic-row">
 <h1 class="title-wrapper">
            <?php echo REGISTER_CREATE_TEXT;?>
        </h1>   
        
                <div class="box service-pro-account">
           <form accept-charset="ISO-8859-1" action="<?=$this->config->config['base_url']?>user/register_submit" method="post" name="register" id="register" novalidate>
          <fieldset>
                    <ul>
                        <ul class="dynamic-row">
                            <li class="column-6">
                                <div class="form-field">
                                    <label for="usr_first_name"><?php echo REGISTER_FNAME;?></label>
                                    <input type="text" tabindex="100" name="usr_first_name" id="usr_first_name">
                                </div>
                            </li>
                            <li class="column-6">
                                <div class="form-field">
                                    <label for="usr_last_name"><?php echo REGISTER_LNAME;?></label>
                                    <input type="text" tabindex="101" name="usr_last_name" id="usr_last_name">
                                </div>
                            </li>
                        </ul>
                        <div class="form-field">
                            <label for="usr_email"><?php echo REGISTER_EMAILADD;?></label>
                            <input type="text" tabindex="102" name="usr_email" id="usr_email">
                        </div>
                        <div class="form-field">
                            <label for="usr_password"><?php echo REGISTER_CREATEPASS;?></label>
                            <input type="password" tabindex="103" name="usr_password" id="usr_password">
                        </div>
                    </ul>
                </fieldset>

                <p>
                    <?php echo REGISTER_TERMSTEXT;?>
                    <a href="<?=$this->config->config['main_base_url']?>terms/" target="_blank"><?php echo REGISTER_TERMS;?></a> and
                    <a href="<?=$this->config->config['main_base_url']?>privacy/" target="_blank"><?php echo REGISTER_PRIVACY;?></a>.
                </p>

                <fieldset>
                    <div class="form-field">
                        <button tabindex="104" class="bttn blue submit-bttn" type="submit"><?php echo BUTTON_CREATEACCOUNT;?></button>
                    </div>
                </fieldset>

            </form>
        </div>

                <h3 class="or choose-account">
            or
        </h3>

                <div class="box standard-account">
           
                <div class="login-with-facebook-wrapper">
            <h3 class="or">
                
            </h3>
            <a class="bttn facebook-bttn" href="<?=$this->config->config['base_url']?>user/fb_login">
                <div class="icon-font icon-font-facebook"></div>
                <span class="separator">
                    |
                </span>
                <?php echo REGISTER_SIGNUPFB;?>
            </a>
             <center><p>
                <?php echo REGISTER_WALLMSG;?>
            </p></center>
            <a tabindex="103" id="show" style="background:#ac0000; border:#ac0000; margin-top: 10px; padding: 15px 0;width: 100%;" class="bttn blue submit-bttn" onclick="location.href='http://www.seekmi.com/beta098/hauth/login/Google'"> 
                <div class="icon-font icon-font-gplus googlp" style="margin-right:0px;"></div>
                <span class="separator">
                    |
                </span>&nbsp;Sign up with Google &nbsp;&nbsp;&nbsp;
            </a>
            <h3 class="or">
                
            </h3>
        </div>
        </div>
          <h3 class="or choose-account">
          
        </h3>

         <div class="box standard-account prosbtn" style="border:none;">
          <fieldset>
         <div class="form-field">
                        <button tabindex="104" class="bttn blue submit-bttn" type="button" onClick="javascript:location.href='<?=$this->config->config['base_url']?>pros/register/'"><?php echo REGISTER_AS_PROFESSIONAL;?></button>
                    </div></fieldset></div>

    </div>
</div>

<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
$(function() {
    $("#usr_first_name").focus();
});

$(document).ready(function () {
    var validator = $("#register").validate({
        rules: {
            usr_first_name: {
                required: true,
                minlength: 2
            },
            usr_last_name: {
                required: true,
                minlength: 2
            },
            usr_email: {
                required: true,
                email: true,
                remote: "<?=$this->config->config['base_url']?>user/check_email_exists"
            },
            usr_password: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            usr_first_name: {
                required: "Please enter your first name",
                minlength: "Too short"
            },
            usr_last_name: {
                required: "Please enter your last name",
                minlength: "Too short"
            },
            usr_email: {
                required: "You must enter an email",
                email: "Please enter a valid email",
                remote: "Sorry, that email is already taken"
            },
            usr_password: {
                required: "You must enter a password",
                minlength: "Too short (minimum of 5 characters)"
            }
        }
    });

    $('#usr_email').blur(function() {
        $('#usr_email').val($.trim($('#usr_email').val()));
        validator.element('#usr_email');
    });
});
</script>
</body>
</html>

