<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/message.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/profile.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/jquery.bxslider.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-responsive primo-fluid service-profile box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>
<div style="" class="profile-content">
    <div class="profile-header">        
        <div class="profile-basics">
            <div class="profile-picture-container">
                <div class="profile-picture">
                  <?php if($user_data->userPhoto==''){ ?>
                    <img alt="<?php echo $user_data->firstname; ?> <?php echo $user_data->lastname; ?>" src="<?=$this->config->config['base_url']?>images/img-profile-missing.png">                
                  <?php }else{ ?>
                      <img alt="<?php echo $user_data->firstname; ?> <?php echo $user_data->lastname; ?>" src="<?=$this->config->config['base_url']?>user_images/<?=$user_data->userPhoto?>">
                  <?php } ?> 
                </div>
                <div class="profile-basic-details">
                    <div class="profile-identity">
                        <h1 class="profile-title"><?php echo $business_info->businessName; ?></h1>
                        <div class="profile-business-relationship"><?php echo $user_data->firstname; ?> <?php echo $user_data->lastname; ?></div>
                    </div>
                    <?php if($business_info->tagLine!=''){ ?>
                    <span class="profile-tagline">
                        “<span class="ng-binding"><?=$business_info->tagLine?></span>”
                    </span>
                    <?php } ?>
                    <ul class="profile-attributes">
                        <li class="location"><?php echo $city_nm; ?>, <?php echo $province_nm; ?></li>
                        <li class="phone"><?php echo $user_data->phone; ?></li>
                    </ul>
                </div>
            </div>
            <div class="profile-header-reviews">
                <button class="tiny bttn white" onclick="javascript:window.location.href='#reviews';">
                    <span class="service-rating service-rating-large">
                        <span class="star-rating star-rating-large stars-<?=str_replace('.','_',$business_info->rating)?>"></span>
                    </span>
                    <span class="service-rating-review-count"><?=$business_info->reviewCount?>&nbsp;reviews</span>
                </button>
            </div>
          </div>
    </div>
    <?php if(!empty($media_list)){ ?>
    <div class="profile-media" id="media">        
        <div class="stampede-carousel" style="height: 412px;">
          
               <ul class="stampede-content bxslider">
                <?php foreach($media_list as $media_row){ ?>
                   <li class="stampede-current">
                     <div class="stampede-picture-container">
                         <?php if($media_row->mediaType=='Image'){  ?>
                         <img src="<?php echo $this->config->config['base_url'].'business_images/'.$media_row->mediaPath; ?>">
                         <?php }else{ ?>                          
                         <iframe src="<?=$media_row->mediaPath?>" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                         <?php } ?>
                     </div>                  
                   </li>
               <?php } ?>
             </ul>     
          
        </div>
    </div>
     <?php } ?>

    
    <div class="full-block service-info-block">
        <div class="service-info-page-section">
            <div class="service-info-container">
                <div class="service-info">
                    <div class="service-bio-section">
                        <h3 class="section-header">Bio</h3>
                          <?php if($business_info->bioInfo==''){ ?>
                             <p class="empty-section">
                                Tell your story!
                             </p>
                            <?php }else{ ?>
                             <p class="service-bio body-text">
                                <?php echo $business_info->bioInfo; ?>
                             </p>
                          <?php } ?>  
                    </div>
                    <div>
                        <h3 class="section-header">Services</h3>
                        <p class="service-description body-text" style="white-space:pre-line;">
                            <?php echo $business_info->bussinessDescription; ?>  
                        </p>
                    </div>
                    <div class="service-external-links">
<!--                          <a class="offsite-link offsite-link-facebook" title="Facebook" target="_blank" href="">
                                Facebook<span class="offsite-link-metric">87 likes</span>
                          </a>-->
                        <?php if($business_info->website!=''){ ?>
                        <a class="offsite-link offsite-link-website" title="<?=$business_info->website?>" target="_blank" href="http://<?=$business_info->website?>">
                            <?php echo $business_info->website; ?>
                            <span class="offsite-link-metric">website</span>
                        </a>
                        <?php } ?>                        
                     </div>
                </div>
                <div class="service-sidebar">
                    <div>
                        <h3 class="section-header ng-scope">About</h3>                        
                        <h3 class="business-name"><?php echo $business_info->businessName; ?></h3>
                        <?php if($business_info->foundingYear!=''){ 
                            $currentYear=date('Y');
                            if($currentYear > $business_info->foundingYear){
                               $year_founding=$currentYear-$business_info->foundingYear;
                            ?>
                        <p class="years-in-business"><?php echo $year_founding; ?> years in business</p>
                        <?php }} ?>
                        <?php if($business_info->employeeCount!='' && $business_info->employeeCount!='0'){ ?>
                        <p class="employee-count"><?php echo $business_info->employeeCount; ?> employees</p>
                        <?php } if($business_info->travelToCustomer=='Y'){ ?>
                        <p class="service-travels">Travels up to
                           <span><?php echo $business_info->distanceMiles; ?></span> miles
                        </p>
                        <?php } ?>
                     </div>
<!--                     <div class="credentials">
                          <h3 class="section-header">Credentials</h3>
                              <div class="credential">
                                    <p>
                                        <span>
                                            <i class="jobs-completed"></i>
                                        </span>
                                        hired 8 times
                                    </p>
                                </div> 
                              <div class="credential">
                                    <p>
                                        <span class="gold">
                                            <i></i>
                                        </span>
                                        Gold profile
                                    </p>
                                </div>                           
                      </div>-->
                  </div>
            </div>
        </div>
    </div>
    
    <?php if($answer_count== 0){ ?>       
    <div class="full-block service-questions-edit-container" style="padding-bottom:1em;">
            <div class="service-questions-edit">
                <h3 class="section-header"><span>Questions &amp; Answers</span></h3>                
                <p class="empty-section">
                    Answer some frequently asked questions about your service.
                </p>                
            </div>
        </div>
        <?php }else{ ?>
         <div class="full-block service-questions-container">
            <div class="service-questions">
                <h3 class="section-header"><span>Questions &amp; Answers</span></h3> 
                
                    <?php
                    $i=1;                    
                    foreach($answer_list as $answer_row){
                        if($answer_row->answerText!=''){?>
                    <?php if($i==1){ ?><div><?php } ?>
                    <div class="service-question-column">
                        <div class="service-question-answer">
                            <h3><?=$answer_row->questionText?></h3>
                            <p><?=$answer_row->answerText?></p>
                        </div>
                    </div>
                   <?php if($i%3==0){ ?></div><div><?php } ?>
                   <?php if($i==$answer_count){ ?></div><?php } ?>
                   <?php  $i++;}} ?>                        
                
            </div>
        </div>
    <?php } ?>
    
    <div class="full-block reviews-block" id="reviews">
        <div class="reviews-container">
            <h3 class="section-header">Reviews</h3>
            <?php if(!empty($review_list)){ ?>
               <ul class="review-list">
                <?php 
                    $ratingArr = array("0"=>"", "1"=>"one","2"=>"two","3"=>"three","4"=>"four", "5"=>"five");
                    foreach($review_list as $review_row){ ?>   
                <li>
                    <p class="review-rating-wrap">
                    <span>
                        <span class="score small <?=$ratingArr[$review_row->rating]?>"><?=$review_row->rating?>/5 stars</span>                        
                    </span>            
                    <span class="review-date-wrap">
                        <?=date('F j, Y',strtotime($review_row->createdOn))?>
                    </span>
                    </p>
                    <?php if($review_row->userPhoto==''){ ?>
                        <img src="<?=$this->config->config['base_url']?>images/100x100.png" width="50" height="50" class="review-thumb">
                    <?php }else if($review_row->facebookProfileId!=''){ ?>
                        <img src="<?=$review_row->userPhoto?>" width="50" height="50" class="review-thumb">
                    <?php }else{ ?>
                        <img src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$review_row->userPhoto?>" width="50" height="50" class="review-thumb">
                    <?php } ?>                      
                    <p class="review-body">
                        <?=$review_row->reviewContent?>
                    </p>
                    <p><span class="reviewer"><?=$review_row->firstname?> 
                            <?php if($review_row->lastname!=''){ echo $review_row->lastname[0]."."; } ?>
                       </span>
                    </p>
                </li>
                    <?php } ?>
              </ul>
              <?php }else{ ?>
              <p>
                    There are no reviews for <?php echo $business_info->businessName; ?>.
                </p>
              <?php } ?>
              <a class="bttn tiny white leave-review" href="<?=$this->config->config['base_url']?>bid/add_review/<?=$userId?>">
                   Leave a Review
              </a>
          </div>
    </div>

<!--            <div class="full-block recent-work-block">
            <div class="recent-work-container">
                <h3 class="section-header">Recent Work</h3>
                <ul>
                                            <li>
                            <h4>House Cleaning (Recurring)</h4>
                            <p>
                                April 23, 2015
                            </p>
                        </li>
                                            <li>
                            <h4>House Cleaning (Recurring)</h4>
                            <p>
                                April 22, 2015
                            </p>
                        </li>
                                            <li>
                            <h4>House Cleaning (One Time)</h4>
                            <p>
                                April 21, 2015
                            </p>
                        </li>
                                            <li>
                            <h4>House Cleaning (Recurring)</h4>
                            <p>
                                April 20, 2015
                            </p>
                        </li>
                                            <li>
                            <h4>House Cleaning (One Time)</h4>
                            <p>
                                April 17, 2015
                            </p>
                        </li>
                                            <li>
                            <h4>House Cleaning (One Time)</h4>
                            <p>
                                April 17, 2015
                            </p>
                        </li>
                                            <li>
                            <h4>House Cleaning (Recurring)</h4>
                            <p>
                                April 6, 2015
                            </p>
                        </li>
                                            <li>
                            <h4>House Cleaning (One Time)</h4>
                            <p>
                                April 7, 2015
                            </p>
                        </li>
                                    </ul>
            </div>
        </div>       -->
    
    </div>
<div id="feedback-tab"><a data-show-feedback="" href="#">Contact Us</a></div>
<? include('footer_mini_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/SimpleAjaxUploader.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/profile.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.bxslider.js"></script> 
<script type="text/javascript">
   
    $('.bxslider').bxSlider({    
        auto: true,
        video: true,
        captions: true,
        slideWidth: 580,
        minSlides: 2,
        maxSlides: 3,
        slideMargin: 10,
        hideControlOnEnd: true
  });
</script>
</body>
</html>

