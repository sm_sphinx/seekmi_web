<!doctype html>
<html lang="en" class="no-js">
<!-- Mirrored from nunforest.com/site/element-demo/boxed/pricing.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Jan 2015 06:53:45 GMT -->
<head>
    <title><?=$page_title?> | Montego</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/bootstrap.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/magnific-popup.css" media="screen">	
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/animate.css" media="screen">
<!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/style.css" media="screen">
    
</head>
<body id="top">
<? include('header_view.php'); ?>
<!-- ####################################################################################################### -->
<div id="content">

<!-- page-banner-section -->
<div class="section-content map-section">
        <div class="map"></div>
</div>

<!-- contact-section -->
<div class="section-content contact-section">
        <div class="container">
                <div class="row">
                        <div class="col-md-9">
                                <form id="contact-form">
                                        <h2>Send us a message</h2>
                                        <div class="row">
                                                <div class="col-md-6">
                                                        <input name="name" id="name" type="text" placeholder="Name">
                                                </div>
                                                <div class="col-md-6">
                                                        <input name="mail" id="mail" type="text" placeholder="Email">
                                                </div>
                                        </div>
                                        <textarea name="comment" id="comment" placeholder="Message"></textarea>
                                        <div class="submit-area">
                                                <input type="submit" id="submit_contact" value="Send Message">
                                                <div id="msg" class="message"></div>								
                                        </div>
                                </form>
                        </div>
                        <div class="col-md-3">
                                <div class="contact-information">
                                        <h2>Contact Info</h2>
                                        <ul class="information-list">
                                                <li><i class="fa fa-map-marker"></i><span>1400 Defense Pentagon, Arlington County, Virginia, United States</span></li>
                                                <li><i class="fa fa-phone"></i><span>+1 703-697-1776</span><span>+1 605-315-8544</span></li>
                                                <li><i class="fa fa-envelope-o"></i><a href="#">nunforest@gmail.com</a></li>
                                        </ul>
                                        <ul class="social-list">
                                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
                                                <li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                                        </ul>
                                </div>
                        </div>

                </div>
        </div>
</div>
</div>
<!-- ####################################################################################################### -->
<? include('footer_view.php'); ?>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.migrate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/waypoint.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/plugins-scroll.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/gmap3.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/script.js"></script>  	
</body>
</html>
