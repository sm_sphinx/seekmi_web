<!doctype html>
<html lang="en" class="no-js">
<!-- Mirrored from nunforest.com/site/element-demo/boxed/pricing.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Jan 2015 06:53:45 GMT -->
<head>
    <title><?=$page_title?> | Montego</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/bootstrap.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/magnific-popup.css" media="screen">	
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/animate.css" media="screen">
<!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/style.css" media="screen">
    
    <link rel="stylesheet" href="<?=$this->config->config['base_url']?>css/tmm_form_wizard_style_demo.css" />
    <link rel="stylesheet" href="<?=$this->config->config['base_url']?>css/grid.css" />
    <link rel="stylesheet" href="<?=$this->config->config['base_url']?>css/tmm_form_wizard_layout.css" />
    <link rel="stylesheet" href="<?=$this->config->config['base_url']?>css/fontello.css" />
    <link rel="stylesheet" href="<?=$this->config->config['base_url']?>js/leaflet/leaflet.css" />     
    
</head>
<body id="top">
<? include('header_view.php'); ?>
<!-- ####################################################################################################### -->
<!-- content 
================================================== -->
<div id="content">

<!-- page-banner-section -->
<div class="section-content page-banner-section2">
        <div class="container">
                <div class="row">
                        <div class="col-sm-6">
                                <h1>My Profile</h1>
                        </div>
                        <div class="col-sm-6">
                                <ul>
                                        <li><a href="#">Home</a></li>
                                        <li><a href="support.html">My Profile</a></li>
                                </ul>
                        </div>
                </div>
        </div>
</div>
<!-- page-banner-section -->
		
<!-- contact-section -->
<div class="section-content section-content shoping-section ">
            <div class="tab-box">
                    <div class="container">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="myTab">
                            <?php if(isset($this->session->userdata['userId'])){ ?>
                            <li>
                               <a onclick="location.href='<?=$this->config->config['base_url']?>user/create_app/<?=$this->session->userdata('userId')?>';" href="javascript:void(0);">Create Application</a>
                            </li>
                            <?php } ?>
                            <li class="active">
                               <a href="#description" data-toggle="tab">My Profile</a>
                            </li>
                            <?php if($this->session->userdata('user_type')=='customer'){ ?>
                            <li>
                                <a href="#reviews" onclick="loadData('my_application')" data-toggle="tab">My Application</a>
                            </li>
                            <li>
                               <a href="#aditional-info" onclick="loadData('my_transaction')" data-toggle="tab"> My Transaction  </a>
                            </li>
                            <li>
                               <a href="#welcome-email" data-toggle="tab">  Welcome Email   </a>
                            </li>
                            <?php } ?>
                        </ul>

<div class="tab-content">
        <div class="tab-pane active" id="description">
                <div class="tab-data-content">
                    <div class="bhoechie-tab-content active">
                           <h2> My Profile </h2>
                           <form class="personal-cart-information" name="user-profile-form" id="user-profile-form">
                               <input type="hidden" name="prof_userid" value="<?=$userId?>">
                                   <div class="alert alert-success alert-success-profile" role="alert" style="display: none;">                                        
                                        Profile updated successfully
                                   </div>
                                   <div class="row">
                                           <div class="col-md-6">
                                                <label>First Name</label>
                                                <input type="text" name="profile_fname" value="<?=$user_data->firstname?>">
                                                <label>Email</label>
                                                <input type="text" name="profile_email" disabled="true" value="<?=$user_data->email?>">
                                                <label>Address</label>
                                                <input type="text" name="profile_address" value="<?=$user_data->address?>">
                                                <label>State</label>
                                                <input type="text" name="profile_state" value="<?=$user_data->state?>">                                                
                                           </div>

                                           <div class="col-md-6">
                                               <label>Last Name</label>
                                               <input type="text" name="profile_lname" value="<?=$user_data->lastname?>">
                                               <label>Phone</label>
                                               <input type="text" name="profile_phone" value="<?=$user_data->phone?>">
                                               <label>City</label>
                                               <input type="text" name="profile_city" value="<?=$user_data->city?>">
                                               <label>Country</label>
                                               <select name="profile_country">
                                                   <option>Select your country</option>
                                                   <?php foreach($country_list as $country_row){ 
                                                       if($user_data->country==$country_row->code){
                                                           $sel='selected="selected"';                                                           
                                                       }else{
                                                           $sel='';
                                                       }
                                                       ?>
                                                   <option value="<?=$country_row->code?>" <?=$sel?>><?=$country_row->name?></option>
                                                   <?php } ?>
                                                </select>
                                           </div>                                       
                                       <div class="col-md-12">
                                           <button id="profile_btn" type="submit" name="profile_btn" class="btn btn-success">Save</button> 
                                           <button type="button" class="btn btn-warning" href="javascript:void(0);" onclick="$('#changePasswordModal').modal('show');" style="margin-left: 10px;">Change Password</button>
                                       </div>                                       
                                   </div>
                           </form>
                    </div>
                </div>
        </div>
<?php if($this->session->userdata('user_type')=='customer'){ ?>
        <div class="tab-pane" id="reviews">
            <div class="tab-data-content">
                <div class="bhoechie-tab-content">
                    <h2> My Application </h2>
                    <div class="products-table table-responsive  banner-text2-section" id="my-application-grid" style="background-color:transparent; padding:0;">                               
                    </div>
                 </div>
              </div>
        </div>
        
        <div class="tab-pane" id="aditional-info">
            <div class="tab-data-content">
                 <h2> My Transaction </h2>
                 <div class="products-table table-responsive" id="my-transaction-grid"></div>
             </div>
        </div>
        
        <div class="tab-pane" id="welcome-email">
             <div class="tab-data-content">
                       
                       <h2>  Welcome Email </h2>
                       <p>Welcome email is simpliest way to send your app to your guests or customers.<br/>Welcome email gives you opportunity to put all services you provide in simple PDF file among with links to
your application (both andriod or iOS).</p>
                       <p>You just need to insert your text or use our preformatted text and hit generate button after you will be show
option to save this Welcome email PDF file on your computer and you can send it to all your clients via common email software.<br/>
We have format text for you so it is much easier For you to generate your PDF file but if you want to use your own text you can do it</p>	
                        <h3>  I Want to Use </h3>
                        <div class="tab-box">
                        <ul class="nav nav-tabs" id="myTab" style="border:none;">
                        <li class="active">
                        <a href="#description1" data-toggle="tab">Predifined Text</a>
                        </li>
                        <li>
                            <a href="#reviews1" data-toggle="tab" onclick="jQuery('#user_heading_name').val(jQuery('#heading_name').val());">My Own Text</a>
                        </li>
                        </ul>

                        <div class="tab-content" style="border:none; padding:15px 0 0;">
                            <div class="tab-pane active" id="description1">
                            <div class="tab-data-content" id="contact-form">
                                <form class="" name="welcome-email-predefined-form" id="welcome-email-predefined-form">
                                <input type="text" value="" name="heading_name" id="heading_name" placeholder="Name of accomodation or business object"/>                               
                                <textarea readonly="true" id="predefined_text" class="span12 ckeditor m-wrap"  name="predefined_text"><?=$predefined_text?></textarea>
                                <div class="submit-area" style="margin-top:20px;">
                                <input type="submit" value="Generate & Download Welcome Email" style="text-transform:uppercase;">								
                                </div>
                                </form>
                            </div>
                            </div>
                            <div class="tab-pane" id="reviews1">
                            <div class="tab-data-content">
                            <div class="tab-pane active" id="description1">
                            <div class="tab-data-content" id="contact-form">
                                <form class="" name="welcome-email-form" id="welcome-email-form">
                                <input type="text" value="" name="user_heading_name" id="user_heading_name" placeholder="Name of accomodation or business object"/>                               
                                <textarea placeholder="Message" class="span12 ckeditor m-wrap" id="user_defined_text" name="user_defined_text"></textarea>
                                <div class="submit-area" style="margin-top:20px;">
                                <input type="submit" value="Generate & Download Welcome Email" style="text-transform:uppercase;">								
                                </div>
                                </form>
                            </div>
                            </div>
                            </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
<?php } ?>
         </div>
     </div>
</div>
</div>

</div>
<!-- End content -->

<!-- Model Popup -->
<div class="modal fade bs-modal-sm" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">        
      <div class="modal-body">       
            <form class="form-horizontal" name="user-change-password-form" id="user-change-password-form">
                <input type="hidden" name="userid" id="userid" value="<?=$userId?>"/> 
            <fieldset>
                <code class="alert-success-change-pwd alert-success" style="display:none;">                
                    Password updated successfully.
                </code>
            <!-- Sign In Form -->
            <!-- Text input-->
            <div class="control-group">
              <label class="control-label" for="change_pwd_input">Password:</label>
              <div class="controls">
                <input id="change_pwd_input" name="change_pwd_input" type="password" class="form-control" placeholder="Password" class="input-medium" required="">
              </div>
            </div>

            <!-- Password input-->
            <div class="control-group">
              <label class="control-label" for="change_conf_pwd_input">Confirm Password:</label>
              <div class="controls">
                <input id="change_conf_pwd_input" name="change_conf_pwd_input" class="form-control" type="password" placeholder="Confirm Password" class="input-medium">
              </div>
            </div>

            <!-- Button -->
            <div class="control-group">
              <label class="control-label" for="change_pwd_btn"></label>
              <div class="controls">
                  <button id="signin" type="submit" name="change_pwd_btn" class="btn btn-success">Update</button>
              </div>
            </div>
            </fieldset>
            </form>        
      </div>
      <div class="modal-footer">
      <center>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </center>
      </div>
    </div>
  </div>
</div>
<!-- Model Popup -->
<!-- ####################################################################################################### -->
<? include('footer_view.php'); ?>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.migrate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/plugins-scroll.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/script.js"></script>	
<!--[if lt IE 9]>
                <script src="<?=$this->config->config['base_url']?>js/respond.min.js"></script>
<![endif]-->
<script src="<?=$this->config->config['base_url']?>js/tmm_form_wizard_custom.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>assets/plugins/ckeditor/ckeditor.js"></script>  
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/login.js"></script>

<script type="text/javascript">
    /*var editor;
// The instanceReady event is fired, when an instance of CKEditor has finished
// its initialization.
CKEDITOR.on( 'instanceReady', function( ev ) {
editor = ev.editor;
// Show this "on" button.
document.getElementById( 'predefined_text' ).style.display = '';
// Event fired when the readOnly property changes.
editor.on( 'readOnly', function() {
document.getElementById( 'predefined_text' ).style.display = this.readOnly ? 'none' : '';
});
editor.setReadOnly(true);
});*/
    
    CKEDITOR.replace( 'predefined_text', {
	toolbar: [
		{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
		[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo','Bold', 'Italic']
		
	]
    });
    
    CKEDITOR.replace( 'user_defined_text', {
	toolbar: [
		{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
		[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo','Bold', 'Italic']
		
	]
    });


   function loadData(tp){
       if(tp=='my_application'){
            jQuery.ajax({
                 type: "POST",
                 url: '<?=base_url()?>app/my_apps',
                 data: "userId=<?=$userId?>",
                 success: function(msg)
                 {
                    jQuery('#my-application-grid').html(msg);                  
                 }
             });
        }
        if(tp=='my_transaction'){
            jQuery.ajax({
                 type: "POST",
                 url: '<?=base_url()?>app/my_transactions',
                 data: "userId=<?=$userId?>",
                 success: function(msg)
                 {
                    jQuery('#my-transaction-grid').html(msg);                  
                 }
             });
        }
   }
   
   function deleteUserApp(appid){
        var r = confirm("Are you sure want to delete this APP?");
        if(r == true) {
            jQuery.ajax({
                 type: "POST",
                 url: '<?=base_url()?>app/delete_app',
                 data: "appId="+appid,
                 success: function(msg)
                 {
                    if(msg=='success'){
                        loadData('my_application');
                    }    
                 }
             });
        } else {
            return false;
        }
   }
</script>	
</body>
</html>
