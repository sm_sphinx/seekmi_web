<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="mainimage" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$page_title?> - Seekmi</title>
<meta content="<?=$this->config->config['base_url']?>pros/register" property="og:url"/>
<meta content="<?=$page_title?>" property="og:title"/>
<meta content="Seekmi" property="og:site_name"/>
<meta property="og:description" content="Get things done by hiring experienced local service professionals." />
<meta property="og:image" content="<?=$this->config->config['base_url']?>images/LogoSymbolWordSquare.png"/>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/home/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/signup.css">
<link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/jquery.tokenize.css" />
 <style type="text/css">
    .help-block {
    color: #d51818;
    display: block;
    margin-bottom: 10px;
    margin-top: 5px;
    font-size: 11.2px;
   }
   .wrapper.content {
    margin: 63px auto !important;
    min-height: 420px;
}
   #services\[\]-error,#preferences\[\]-error{
       float:left;
       font-size: 12px;
       margin-left: -28px;
       margin-top: -37px;
   }
   .has-error .checkbox, .has-error .checkbox-inline, .has-error label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label {
    color: #d51818;
   }
   .has-error input,.has-error select,.has-error textarea{
      border-color: #a94442;
      box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset; 
   }   
   #agree_check-error.help-block{
        margin-bottom: 0 !important;
        margin-top: -33px !important;
   }   
   @media only screen and (max-width : 760px) {
    #recaptcha_challenge_image{
    margin: 0 !important;
    width: 100% !important;
    height: auto !important;
    }
    #recaptcha_response_field
    {
    margin: 0 !important;
    width: 100% !important;
    height: auto !important;
    }
    .recaptchatable #recaptcha_image {
    margin: 0 !important;
    width: 100% !important;
    height: auto !important;
    }
    .recaptchatable .recaptcha_r1_c1, 
    .recaptchatable .recaptcha_r3_c1, 
    .recaptchatable .recaptcha_r3_c2, 
    .recaptchatable .recaptcha_r7_c1, 
    .recaptchatable .recaptcha_r8_c1, 
    .recaptchatable .recaptcha_r3_c3, 
    .recaptchatable .recaptcha_r2_c1, 
    .recaptchatable .recaptcha_r4_c1, 
    .recaptchatable .recaptcha_r4_c2, 
    .recaptchatable .recaptcha_r4_c4, 
    .recaptchatable .recaptcha_image_cell {

    margin: 0 !important;
    width: 100% !important;
    background: none !important;
    height: auto !important;
    }

}
.glorious-header ul.nav-menu ul a, .glorious-header .nav-menu ul ul a {
    background-color: #fff;
    color: #000;
    margin: 0;
    padding-right: 13px !important;
    width: 64px;
}
 </style>
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-responsive primo-fluid box-shadow multiple-backgrounds">
    <?php include('common_view.php'); ?>
    <?php include('home_header_view.php'); ?>

<div class="wrapper content">
    <div>
      <form name="pros_register_form" method="POST" id="pros_register_form" action="<?=$this->config->config['base_url']?>pros/register_submit">
            
        <div id="main_div_2" class="box account">            
                <div class="box-header">
                    <h2><?php echo SIGNUPREGI_HOW_TEXT;?></h2>
                    <h3>
                        <?php echo SIGNUPREGI_TELLUS_TEXT;?>
                    </h3>  
                    
                    <div class="form-field-sms-preference form-field form-field" expr="account.sms" field="">
                        <div>
                          <label for="services"><?php echo SIGNUPREGI_PUTALL_TEXT;?>(,)</label>
                          <select name="services[]" id="services" class="tokenize-sample required" multiple="multiple"></select>
                        </div>
                    </div>
                </div>
                <div class="box-content">
                    <div class="conditions">                      
                        <p>
                            <?php echo SIGNUPREGI_NOTE_TEXT;?>
                        </p><br/>
                    </div>
                    <fieldset>
                        <div class="dynamic-row">
                            <div class="form-field-name-segment form-field form-field" expr="account.firstName" field="">
                                <div>
                                <label for="first-name"><?php echo REGISTER_FNAME;?></label>
                                <input type="text" minlength="2" name="firstName" id="first-name" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-field-name-segment form-field form-field" expr="account.lastName" field="">
                                <div>
                                <label for="last-name"><?php echo REGISTER_LNAME;?></label>
                                <input type="text" minlength="2" required="" name="lastName" id="last-name" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="dynamic-row">
                            <div class="form-field-name-segment form-field form-field" expr="account.usr_email" field="">
                                <div>
                                <label for="usr-email"><?php echo REGISTER_EMAILADD;?></label>
                                <input type="email" name="usr_email" id="usr_email" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-field-name-segment form-field form-field" expr="account.phone" field="">
                                <div>
                                <label for="phone"><?php echo PHONE_NUMBER_PLACEHOLDER;?></label>
                                <input type="text" pattern="[0-9]*" name="phone" id="phone" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="dynamic-row">
                            <div class="form-field-name-segment form-field form-field" expr="account.password" field="">
                                <div>
                                <label for="password"><?php echo LOGIN_PASS;?></label>
                                <input type="password" minlength="5" name="password" id="password" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-field-name-segment form-field form-field" expr="account.confpassword" field="">
                                <div>
                                <label for="confirm-password"><?php echo LABEL_REPEATPASS;?></label>
                                <input type="password" minlength="5" name="confirmPassword" id="confirmPassword" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="alert-closable alert-hide captcha-error">            
                            <div class="alert-error">                                
                                <p>
                                    Incorrect captcha.
                                </p>
                            </div>
                        </div>
                        <div class="dynamic-row">
                            <div class="form-field form-field" expr="account.password" field="">
                                <div>
                                  <?php echo $recaptcha_html; ?>
                                </div>
                            </div>
                        </div>
                        <?php /* <div class="form-field-sms-preference form-field form-field" expr="account.sms" field="">
                            <div>
                            <label class="inline">
                                <input type="checkbox" name="sms_notification" value="1">
                               <?php echo LABEL_NOTIFYME;?>
                            </label>
                            </div>
                        </div> */ ?>
                    </fieldset>                    
                </div>
            
            
            <div class="form-field form-field-nav box-footer" style="padding-bottom:0px;">
                    <div class="conditions" style="margin-bottom:35px; margin-top:0px;">
                        <p>
                            <input type="checkbox" name="agree_check" value="1" class="radio tocustomer_check" step="vertical-align:top;"/>&nbsp;
                            <span style="margin-left: 12px;"><?php echo LABEL_BYCHECKINGTREMS;?><a href="<?=$this->config->config['base_url']?>terms/" target="_blank">http://www.seekmi.com/terms</a>
                            <?php echo LABEL_CONDOFSERVICES;?><a href="<?=$this->config->config['base_url']?>privacy/" target="_blank">http://www.seekmi.com/privacy</a> 
                            <?php echo LABEL_PRIVACYPOLICY;?></span>
                        </p>
                    </div> 
                    <?php /* <div class="conditions">                       
                        <p>
                           <?php echo SIGNUPREGI_SMS_TEXT; ?>
                        </p>
                    </div> */ ?>
               </div>
            <div class="form-field form-field-nav box-footer" style="padding-bottom:0px; padding-top: 0px;">
                <div class="alert-closable alert-hide referral-code-email-error">            
                    <div class="alert-error">                                
                        <p>
                            <?php echo SIGNUP_REFERRAL_CODE_ERROR_TEXT; ?>
                        </p>
                    </div>
                </div>
                <div class="form-field form-field" id="referralCodeEmailDiv" >
                    <div>  
                    <label for="referral_code_or_email"><?php echo SIGNUP_REFERRAL_CODE_TEXT;?></label>
                    <input type="text" name="referral_code_or_email" id="referral_code_or_email" class="form-field-name-segment">
                    </div>
                </div>
            </div>
            <div class="box-footer">               
                <div class="nav-container" role="presentation">                      
                    <button  class="bttn" type="submit">
                       <span><?php echo BUTTON_SUBMIT; ?></span>                           
                    </button>
                    <?php /* <a class="back" href="<?=$this->config->config['base_url']?>#professionalDiv">← <?php echo LABEL_RETURN;?></a> */ ?>
                </div> 
            </div>
        </div>
     

        <ul class="reasons">
            <li class="clients clients1">
                <h4><?php echo SIGNUPREGI_GETNEWCLIENTS_TEXT;?></h4>
            </li>
            <li class="control-requests control-requests1">
                <h4><?php echo SIGNUPREGI_CONTROL_TEXT;?></h4>
            </li>
            <li class="leads leads1">
                <h4><?php echo SIGNUPREGI_REGISTER_TEXT;?></h4>
            </li>
        </ul>
          <input type="hidden" value="<?php echo $referral_code; ?>" name="referral_code"/>
        </form>
    </div>
</div>
<script type="text/javascript">
     var Host='<?=$this->config->config['base_url']?>';    
</script>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/common.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.tokenize.js"></script>
<script>
$('#language').change(function(){
        var language = $('#language').val();
             $.ajax({
                type: 'POST',
                url: "<?php echo base_url();?>home/changeLanguage",
                data:"language="+language,
                success: function (data) {
                       window.location.reload(); 
                    }
             });   
             });
</script> 
<script type="text/javascript">  
  $('#services').tokenize({
    datas: "<?=$this->config->config['base_url']?>home/get_services_pros"
  });
  
$.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Please enter only characters");

$.validator.addMethod("phonenumber", function(value, element) {
  return this.optional(element) || /[0-9-()+]+$/.test(value);
}, "Please enter valid phone number"); 
  
var form = $("#pros_register_form");
var validator = form.validate({
        ignore: [],
        errorElement: 'span',
        errorClass: 'help-block',                    
        highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-field').addClass("has-error");
        },
        unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-field').removeClass("has-error");
        },
        rules: {                        
             'services[]': {  
                required: true,
             },
             firstName:{
                required: true,
                lettersonly: true
             },
             lastName:{
                required: true,
                lettersonly: true
             },
             usr_email:{
                required: true,
                email:true,
                remote: "<?=$this->config->config['base_url']?>user/check_email_exists"
             },
             phone:{
                required: true,
                phonenumber: true
             },
             password:{
                required: true,
                minlength: 5
             },
             confirmPassword:{
                required: true,
                equalTo: "#password"
             },
             recaptcha_response_field:{
                 required: true                 
             },
             agree_check:{
                required: true
             }/*,
             referral_code_or_email:{
                required: '#referral_code_or_email_check:checked',
             }*/
        },
        messages:{
               //'services[]': "Please add at least one service",
               usr_email: {                               
                    remote: "Sorry, that email is already taken"
               }

        },
        submitHandler: function(form) {
            $.post('<?=$this->config->config['base_url']?>pros/register_submit', $("#pros_register_form").serialize(), function(data) {
                if(data=='incorrect_captcha'){
                    $('.captcha-error').show();
                    $( "#recaptcha_reload" ).trigger( "click" );
                    $('.referral-code-email-error').hide();
                }else if(data=='referral_invalid_code_email'){
                    $('.referral-code-email-error').show();
                    $( "#recaptcha_reload" ).trigger( "click" );
                    $('.captcha-error').hide();
                }else{
                    $('.captcha-error').hide();
                    $('.referral-code-email-error').hide();
                    location.href='<?=$this->config->config['base_url']?>pros/thankyou';
                }
            });     
        }
});




$('#usr_email').blur(function() {
    $('#usr_email').val($.trim($('#usr_email').val()));
    validator.element('#usr_email');
});


/*$('input[name="referral_code_or_email_check"]').click(function(){
    if($('input[name="referral_code_or_email_check"]').attr('checked')){
       $('#referralCodeEmailDiv').show();
   }else{
       $('#referralCodeEmailDiv').hide();
   }
  
});
*/     

function getSubServices()
{  
   var selectedGroups  = new Array();
   $("#service_class input[name^'services[]']:checked").each(function() {     
        selectedGroups.push($(this).val());
   });
    $.post('<?=$this->config->config['base_url']?>pros/getSubservices', { services:selectedGroups }, function(data) {
        if(data!=''){
            $('#subServiceDiv').html(data);
            setPageView(1,6,'next');
        }else{
            setPageView(1,2,'next');
        }
    });  
}
</script>
</body>
</html>
      
      
     