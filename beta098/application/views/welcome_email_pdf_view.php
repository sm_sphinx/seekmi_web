<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
        <title>Montego :: Email</title>        
        <style type="text/css">
		html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,dl,dt,dd,ol,nav ul,nav li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline;}
article, aside, details, figcaption, figure,footer, header, hgroup, menu, nav, section {display: block;}
ol,ul{list-style:none;}
blockquote,q{quotes:none;}
blockquote:before,blockquote:after,q:before,q:after{content:'';content:none;}
table{border-collapse:collapse;border-spacing:0;}
/* start editing from here */
a{text-decoration:none;}
.txt-rt{text-align:right;}/* text align right */
.txt-lt{text-align:left;}/* text align left */
.txt-center{text-align:center;}/* text align center */
.float-rt{float:right;}/* float right */
.float-lt{float:left;}/* float left */
.clear{clear:both;}/* clear float */
.pos-relative{position:relative;}/* Position Relative */
.pos-absolute{position:absolute;}/* Position Absolute */
.vertical-base{	vertical-align:baseline;}/* vertical align baseline */
.vertical-top{	vertical-align:top;}/* vertical align top */
.underline{	padding-bottom:5px;	border-bottom: 1px solid #eee; margin:0 0 20px 0;}/* Add 5px bottom padding and a underline */
nav.vertical ul li{	display:block;}/* vertical menu */
nav.horizontal ul li{	display: inline-block;}/* horizontal menu */
img{max-width:100%;}
/*img[width],img[height]{width:auto;height:auto;} wordpress hack to reset the attributes (while making responsive) */
body{
	background-color:#FFFFFF;
	font-family:Arial, Helvetica, sans-serif;
}
article{
	font-size:14px;
	color:#565656;
}
article,
article.underline{
	padding:20px;
	margin-bottom:0;
}
article img{
	margin:10px 0;
	padding:5px;
	border:1px solid #eee;
}
a{ color:#38A4D8;}
a:hover{ color:#2489b9;}
/* headings */
h1, h2, h3, h4, h5, h6{
	font-weight:bold;
	color:#565656;
}
h1,h2{line-height:24px;}
h1{
	font-size: 24px;
	margin:0 0 .2em 0;
}
h2{
	font-size: 18px;
	margin:0 0 .3em 0;
}
h3{
	font-size: 16px;
	margin:0 0 .4em 0;
}
h4{
	font-size: 14px;
}
h5, h6{
	font-size: 12px;
}
h4, h5, h6{
	margin:0 0 .6em 0;	
}
/* paragraph */
p{
	line-height:26px;
	margin:0 0 16px 0;
}
/* TextFormatting */

/* -- header -- */
header{
 background: none repeat scroll 0 0 #51800c;
    border-bottom: 1px solid #51800c;
    margin-bottom: 30px;
    padding: 20px 0;
}
.logo{
	padding:10px;
	width:90%;
	margin:0 auto;
	text-align:center;
}
.logo a,
.logo img{
	display:block;
	text-align:center;
}
footer{
	border-top:1px solid #eee;
	background:#e3f2f9;
	color:#4d4d4d;
}
footer p{	
	font-size:12px;
	margin:0;
}
.copy{
	padding:10px;
}
.copy a{
	color:#c52525;
}
/* -- menu -- */

.post-content{
	float:left;
	width:60%;
	padding:0 0 0 20px;
}
        h1{
		color:#000000!important; font-size:35px!important; text-align:center!important;line-height:40px!important;	
		}
		 h2{
		color:#51800c!important; font-size:35px!important; text-align:center!important;line-height:40px!important;	
		}
		
		h4{
		color:#000!important; font-size:22px!important; line-height:30px!important; font-weight:300!important; 
		}
		
				h5{
		color:#51800c!important; font-size:22px!important; line-height:35px!important; font-weight:300!important; 
		}
		
		
		article{
	font-size:14px;
	color:#565656;
}

.container{
	margin:0 auto;
	width:70%;
 background-clip: padding-box;
    background-color: #ffffff;
    border: 1px solid #ddd;
    border-radius: 4px;
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
    opacity: 0.97;
}



@media (min-width: 300px) and (max-width:600px) {
	
        h1{
		color:#000000!important; font-size:28px!important; text-align:center!important;line-height:40px!important;	
		}
		 h2{
		color:#51800c!important; font-size:26px!important; text-align:center!important;line-height:35px!important;	
		}
		
		h4{
		color:#000!important; font-size:16px!important; line-height:20px!important; font-weight:300!important; 
		}	
	

h5 {
    color: #51800c !important;
    font-size: 17px !important;
    font-weight: 300 !important;
    line-height: 26px !important;
}
	  }
        </style>
    </head>
    
    
<body>
<div class="container">
	<header>
        <div class="logo">
            <center><img src="images/logo.png" alt="" /></center>
        </div>
        <center><h3 style="color:#FFFFFF; font-weight:400;">Welcome to the first app builder that helps accommodation owners <br>
guide their customers, so that they safety reach their destination. 
</h3></center>
		<div class="clear"></div>
    </header>
    <div class="content">
    <article>
        <h1 style="text-transform:uppercase;"><?=$heading?></h1>
        <h2>Welcome email </h2><br><br>
        <h4><?=$content?></h4><br/>
  <center><h4 style="color:#51800c!important;">More info about this app and how it works you can Find on this link </h4></center>
  <center> <img src="images/logo2.png" alt="" /></center>
    
  <center><h5 style="color:#000;">DOWNLOAD AND INSTALL APP FROM FOlLOWING LINKS </h5></center>
    <center> <a href="https://itunes.apple.com/in/genre/ios/id36?mt=8"><img src="images/appstore-button.png" alt="" /></a>  <a href="https://play.google.com/store?hl=en"><img src="images/android-email.png" alt="" /></a></center>
    </article>
   </div>
    <footer>

       <center> <p class="copy">&copy; 2015 magento.com | All right reserved.</p></center>
    </footer>
    </div>
</body>
</html>
