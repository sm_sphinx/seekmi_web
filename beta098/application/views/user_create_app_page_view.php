<!doctype html>
<html lang="en" class="no-js">
<!-- Mirrored from nunforest.com/site/element-demo/boxed/pricing.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Jan 2015 06:53:45 GMT -->
<head>
    <title><?=$page_title?> | Montego</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/bootstrap.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/magnific-popup.css" media="screen">	
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/animate.css" media="screen">
<!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/style.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/jquery-steps.css">
<!--    <link href="<?=$this->config->config['base_url']?>css/gsdk-base.css" rel="stylesheet" />  -->
    <script type="text/javascript" src="<?=$this->config->config['base_url']?>js/ScribbleMap.js"></script> 

<style type="text/css">
.navbar-collapse.collapse {
display: block !important;
height: auto !important;
overflow: visible !important;
padding-bottom: 11px!important;
}
</style>
</head>
<body id="top">
<? include('header_view.php'); ?>
<!-- ####################################################################################################### -->
<div id="content">
    <div class="section-content page-banner-section2">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                   <h1>Create your Own APP</h1>
                </div>
                <div class="col-sm-6">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>Create APP</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="section-content pricing-section">
        <form id="user-app-form" action="#" class="wizard clearfix" novalidate="novalidate" enctype="multipart/form-data">          
            <input type="hidden" name="user_id" id="user_id" value="<?=$userid?>"/>
            <input type="hidden" name="latitude" id="latitude" value=""/>
            <input type="hidden" name="longitude" id="longitude" value=""/>
            <input type="hidden" name="app_logo_hid" id="app_logo_hid" value=""/>
            <input type="hidden" name="map_image_hid" id="map_image_hid" value=""/>
            <h3>Step</h3>
            <fieldset>
            <legend>Provide your app details</legend>

            <label for="app_name">APP Name *</label>
            <input id="app_name" name="app_name" type="text">
            <span class="help-inline">etc. Hotel Albatros, Restaurant Horizon, Car service John Doe</span><br/>
            <label for="app_email">Email *</label>
            <input id="app_email" name="app_email" type="text">
            <label for="app_logo">APP Logo</label>
            <input id="app_logo" name="app_logo" type="file"> 
            <label for="app_website">Website</label>
            <input id="app_website" name="app_website" type="text"> 
            <label for="app_company">Company *</label>
            <input id="app_company" name="app_company" type="text"> 
            <span class="help-inline">name of company, hotel chain, business chain</span><br/>
            <label for="app_address">Address *</label>
            <input id="app_address" name="app_address" type="text"> 
            <label for="app_city">City *</label>
            <input id="app_city" name="app_city" type="text">
            <label for="app_state">State *</label>
            <input id="app_state" name="app_state" type="text">
            <label for="app_state">Country *</label>
            <select name="app_country" id="app_country">
                <option value="">Select</option>   
                <?php 
                 foreach($country_list as $country_row){                   
                   echo '<option value="'.$country_row->code.'" '.$sel.'>'.$country_row->name.'</option>';  
                 }
                ?>
            </select>
            <label for="app_zipcode">Zip *</label>
            <input id="app_zipcode" name="app_zipcode" type="text">
            <label for="app_phone">Phone *</label>
            <input id="app_phone" name="app_phone" type="text">
            <span class="help-inline">etc. Hotel Albatros, Restaurant Horizon, Car service John Doe</span><br/>
            <label for="app_cellphone">Cell Phone </label>
            <input id="app_cellphone" name="app_cellphone" type="text">
            <span class="help-inline">etc. Hotel Albatros, Restaurant Horizon, Car service John Doe</span><br/>
            <label for="app_otherinfo">Other Info</label>
            <textarea id="app_otherinfo" name="app_otherinfo"></textarea>
            <p>(*) Mandatory</p>
            </fieldset>
       
        
            <h3>Step</h3>
            <fieldset>
            <legend>Mark Your Object On Google Map</legend>

            <div id="ScribbleMap" style="width: 980px; height: 600px;"></div>
            <label class="control-label">Upload Map Image<span class="required">*</span></label>          
            <input type="file" class="default" name="map_image">
            <span class="help-inline">Mark you object on Google Map and save this map using save image. That image will be uploaded here.</span>
            <p>(*) Mandatory</p>
            </fieldset>

            <h3>Step</h3>
            <fieldset>
            <legend>You are to young</legend>

            <p>Please go away ;-)</p>
            </fieldset>

            <h3>Finish</h3>
            <fieldset>
            <legend>Terms and Conditions</legend>

            <input id="acceptTerms-2" name="acceptTerms" type="checkbox" class="required"> <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
            </fieldset>
         </form>
    </div>
    
    <!--<div class="wizard-container"> 
                <form action="" method="">
                <div class="card wizard-card ct-wizard-orange" id="wizard">
                
              
                
                    	<div class="wizard-header">
                        	<h3>
                        	   <b>BUILD</b> YOUR PROFILE <br>
                        	   <small>This information will let us know more about you.</small>
                        	</h3>
                    	</div>
                    	<ul>
                            <li><a href="#about" data-toggle="tab">About</a></li>
                            <li><a href="#account" data-toggle="tab">Account</a></li>
                            <li><a href="#address" data-toggle="tab">Address</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="about">
                              <div class="row">
                                  <h4 class="info-text"> Let's start with the basic information</h4>
                                  <div class="col-sm-4 col-sm-offset-1">
                                     <div class="picture-container">
                                          <div class="picture">
                                              <img src="images/default-avatar.png" class="picture-src" id="wizardPicturePreview" title=""/>
                                              <input type="file" id="wizard-picture">
                                          </div>
                                          <h6>Choose Picture</h6>
                                      </div>
                                  </div>
                                  <div class="col-sm-6">
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">First Name</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Andrew...">
                                      </div>
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Last Name</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Smith...">
                                      </div>
                                  </div>
                                  <div class="col-sm-10 col-sm-offset-1">
                                      <div class="form-group">
                                          <label>Email</label>
                                          <input type="email" class="form-control" placeholder="andrew@creative-tim.com">
                                      </div>
                                  </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="account">
                                <h4 class="info-text"> What are you doing? </h4>
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-radio">
                                                <input type="radio" name="job" value="Design">
                                                <div class="icon">
                                                    <i class="fa fa-pencil"></i>
                                                </div>
                                                <h6>Design</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-radio">
                                                <input type="radio" name="job" value="Code">
                                                <div class="icon">
                                                    <i class="fa fa-terminal"></i>
                                                </div>
                                                <h6>Code</h6>
                                            </div>
                                            
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice"  data-toggle="wizard-radio">
                                                <input type="radio" name="job" value="Develop">
                                                <div class="icon">
                                                    <i class="fa fa-laptop"></i>
                                                </div>
                                                <h6>Develop</h6>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="address">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="info-text"> Are you living in a nice area? </h4>
                                    </div>
                                    <div class="col-sm-7 col-sm-offset-1">
                                         <div class="form-group">
                                            <labe>Street Name</label>
                                            <input type="text" class="form-control" placeholder="5h Avenue">
                                          </div>
                                    </div>
                                    <div class="col-sm-3">
                                         <div class="form-group">
                                            <label>Street Number</label>
                                            <input type="text" class="form-control" placeholder="242">
                                          </div>
                                    </div>
                                    <div class="col-sm-5 col-sm-offset-1">
                                         <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control" placeholder="New York...">
                                          </div>
                                    </div>
                                    <div class="col-sm-5">
                                         <div class="form-group">
                                            <label>Country</label><br>
                                             <select name="country" class="form-control">
                                                <option value="Afghanistan"> Afghanistan </option>
                                                <option value="Albania"> Albania </option>
                                                <option value="Algeria"> Algeria </option>
                                                <option value="American Samoa"> American Samoa </option>
                                                <option value="Andorra"> Andorra </option>
                                                <option value="Angola"> Angola </option>
                                                <option value="Anguilla"> Anguilla </option>
                                                <option value="Antarctica"> Antarctica </option>
                                                <option value="...">...</option>
                                            </select>
                                          </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-footer">
                            	<div class="pull-right">
                                    <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' name='next' value='Next' />
                                    <input type='button' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' />
        
                                </div>
                                <div class="pull-left">
                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                                </div>
                                <div class="clearfix"></div>
                        </div>	
                </div>
                </form>
            </div> 
    -->

</div>
<!-- ####################################################################################################### -->
<? include('footer_view.php'); ?>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.migrate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/plugins-scroll.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/script.js"></script>	
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.steps.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>	
<script>
    var sm;
    var types = [];
      
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/appscript.js"></script>
<!--<script src="<?=$this->config->config['base_url']?>js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
	<script src="<?=$this->config->config['base_url']?>js/wizard.js"></script>-->
	
</body>
</html>
