<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

<div class="wrapper content">    
    <div class="page-title">
        <h1><?php echo DASH_PROJECT_TEXT;?></h1>
        <a class="bttn" href="<?=$this->config->config['base_url']?>profile/search_service"><?php echo DASH_ADD_PROJECT;?></a>
    </div>
    <div class="projects">
    <?php if(!empty($project_list)){ 
    foreach($project_list as $project_row){     
    ?>
    <div class="project inactive">
        <h2 class="title" <?php if(!empty($expert_list[$project_row->projectId])){ ?>onclick="location.href='<?=$this->config->config['base_url']?>bid/details/<?php echo $project_row->projectId; ?>/<?=$expert_list[$project_row->projectId][0]->requestId?>';" style="cursor: pointer;" <?php } ?>>
           <?php echo $project_row->serviceName; ?>
        </h2>

        <p title="Created on <?php echo date('F d, Y',strtotime($project_row->createdDate)); ?>" class="created-on" <?php if(!empty($expert_list[$project_row->projectId])){ ?>onclick="location.href='<?=$this->config->config['base_url']?>bid/details/<?php echo $project_row->projectId; ?>/<?=$expert_list[$project_row->projectId][0]->requestId?>';" style="cursor: pointer;" <?php } ?>>
           <?php echo date('F d, Y',strtotime($project_row->createdDate)); ?>
        </p>

        <?php
        if($project_row->status!='Completed' && $project_row->status!='Cancelled')
        {
            ?>
            <a href="javascript:void(0);" class="change-status" onclick="javascript:show_update_status_popup(<?php echo $project_row->projectId; ?>);">
              <?php echo DASH_UPDATE_STATUS;?>
            </a>
            <?php
        }
        ?>
        
        <div class="footer">
            <ul class="pros">
                <?php if(!empty($expert_list[$project_row->projectId])){ 
                    $service_name='';
                    foreach($expert_list[$project_row->projectId] as $expert_row){?>
                     <li class="pro">
                    <span class="tooltip tooltip-name">
                    <?php if($expert_row->userPhoto==''){ ?>
                     <a href="<?=$this->config->config['base_url']?>bid/details/<?php echo $project_row->projectId; ?>/<?=$expert_row->requestId?>" style="background-image: url('<?=$this->config->config['base_url']?>images/100x100.png');">
                    <?php }else if($expert_row->facebookProfileId!=''){ ?>
                         <a href="<?=$this->config->config['base_url']?>bid/details/<?php echo $project_row->projectId; ?>/<?=$expert_row->requestId?>" style="background-image: url('<?=$expert_row->userPhoto?>');">
                    <?php }else{ ?>
                      <a href="<?=$this->config->config['base_url']?>bid/details/<?php echo $project_row->projectId; ?>/<?=$expert_row->requestId?>" style="background-image: url('<?=$this->config->config['base_url']?>user_images/thumb/<?=$expert_row->userPhoto?>');">
                    <?php }if($expert_row->status=='Hired'){ $service_name=$expert_row->businessName; ?>
                         <span class="complete"><?php echo DASH_COMPLETE;?></span>
                         <?php } ?>
                     </a>
                     <span class="tooltip-text"><?=$expert_row->businessName?></span>
                    </span>
                    </li>
                <?php }} ?>
            </ul>
           <?php if(empty($expert_list[$project_row->projectId]) && $project_row->status!='Completed' && $project_row->status!='Cancelled'){ ?>    
            <div class="exposition"><h4><?php echo DASH_NO_PROFESSIONAL;?></h4>
                <p>
                    <?php //echo DASH_UNFORTUNATELY_TEXT. strtolower($project_row->serviceName). DASH_NEEDS_TEXT; ?> 
                    <?php echo DASH_UNFORTUNATELY_TEXT; ?> 
                </p>
            </div>
           <?php }else if($project_row->status=='Hired'){ ?>
              <div class="exposition">
               <p> <span class="service-name"><?php echo DASH_HIRING_TEXT." ".$project_row->serviceName;?></span></p>
              </div>
          <?php }else if($project_row->status=='Completed'){ ?>
              <div class="exposition">
               <p> <span class="service-name"><?php echo DASH_COMPLETED_TEXT." ".$project_row->serviceName;?></span></p>
              </div>
          <?php }else if($project_row->status=='Cancelled'){ ?>
              <div class="exposition">
               <p> <span class="service-name"><?php echo $project_row->serviceName." ".DASH_CANCELLED_TEXT;?></span></p>
              </div>
          <?php }else{ ?>
            <div class="exposition">
                <p>
                    <?php echo count($expert_list[$project_row->projectId]); ?> 
                    <?php if(count($expert_list[$project_row->projectId])==1){ echo 'professional '.DASH_IS_AVAIL_TEXT; }else{ echo 'professionals '.DASH_AVAIL_TEXT; } ?>
                   
                </p>
            </div>
          <?php } ?>

        </div>
    </div>
    <?php } } ?>

   </div>
</div>                                        

<div class="mobile-app-banner full-block">
    <div class="wrapper body-text">
        <div class="dynamic-row">
            <div class="copy-column">
                <h2><?php echo DASH_HEADING_TEXT;?></h2>
                <p><?php echo DASH_WEHAVE_TEXT;?></p>
                <form>
                    <fieldset>
                        <div class="input-prepend input-append">
                            <span class="add-on">
                                <span aria-hidden="true" class="icon-font icon-font-mobile">
                                </span>
                            </span>
                            <input type="text" placeholder="Phone number" name="phone_number">
                            <button class="bttn" type="submit"><?php echo BUTTON_TEXTMELINK;?></button>
                        </div>
                    </fieldset>
                </form>
                <div class="download-button">
                    <a href="#">
                        <img src="<?=$this->config->config['base_url']?>images/download-on-the-app-store.png">
                    </a>
                </div>
            </div>
            <div class="column-5 iphone"></div>
        </div>
    </div>
</div>

<div class="support-options curtain update-status-popup" style="display: none;">
    <div class="box box-modal box-secondary-emphasized curtain-inner">        
        <form name="update_status_form" id="update_status_form" method="post" action="#" novalidate>
            <div class="box-content">
                <p class="body-text">
                  <div class="form-field">
                        <label for="labeled-inputs-182">Update Status :</label>

                            <div class="describable-select">
                                <select required="" name="change_status_options" id="change_status_options">
                                    <option value="Completed">Completed</option>
                                    <option value="Cancelled">Cancelled</option>
                                </select>
                            </div>
                        <input type="hidden" value="" name="change_status_projectid" id="change_status_projectid">
                        <div class="subtext-form">&nbsp;</div>
                        <div class="form-field">
                            <label for="labeled-inputs-182">Comment :</label>
                            <textarea required="" name="change_status_comment" id="change_status_comment"></textarea>

                        </div>
                   </div>
                </p>
                <div class="subtext-form">&nbsp;</div>
                <p style="text-align: center;"><button class="bttn blue close-update-status-popup" type="button">Cancel</button>&nbsp;&nbsp;&nbsp;
                    <button class="bttn blue submit-update-status-popup" type="submit">Submit</button></p>
            </div>
        </form>
    </div>
</div>

<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
$(function() {
    $("#usr_first_name").focus();
});

$(document).ready(function () {    
    var validator = $("#register").validate({
        rules: {
            usr_first_name: {
                required: true,
                minlength: 2
            },
            usr_last_name: {
                required: true,
                minlength: 2
            },
            usr_email: {
                required: true,
                email: true,
                remote: "<?=$this->config->config['base_url']?>user/check_email_exists"
            },
            usr_password: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            usr_first_name: {
                required: "<?php echo VAL_REQFNAME;?>",
                minlength: "<?php echo VAL_MINLENGTH;?>"
            },
            usr_last_name: {
                required: "<?php echo VAL_REQLNAME;?>",
                minlength: "<?php echo VAL_MINLENGTH;?>"
            },
            usr_email: {
                required: "<?php echo VAL_REQEMAIL;?>",
                email: "<?php echo VAL_EMAIL;?>",
                remote: "<?php echo VAL_EMAIL_EXISTS;?>"
            },
            usr_password: {
                required: "<?php echo VAL_REQPASS;?>",
                minlength: "<?php echo VAL_PASSMIN_LENGTH;?>"
            }
        }
    });

    $('#usr_email').blur(function() {
        $('#usr_email').val($.trim($('#usr_email').val()));
        validator.element('#usr_email');
    });
    
    $(".close-update-status-popup").click(function(){
        $('.update-status-popup').hide();
        $('#change_status_options').val('Completed');
        $('#change_status_projectid').val('');
        $('#change_status_comment').val('');
    });
    
    $("#update_status_form").validate({
        rules: {
            change_status_options: {
                required: true,
            },
            change_status_comment: {
                required: true,
            },
        },
        submitHandler: function(form) {
            $.post(Host+'bid/update_project_status', $("#update_status_form").serialize(), function(data) {
                location.href='<?=$this->config->config['base_url']?>profile/dashboard';
            });     
        }
    });
});

function show_update_status_popup(pid)
{
    $('.update-status-popup').show();
    $('#change_status_projectid').val(pid);
}
</script>
</body>
</html>

