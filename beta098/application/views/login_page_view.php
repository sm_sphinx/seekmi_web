<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Seekmi - <?=$page_title?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<style type="text/css">
.glorious-header1{
    background: none repeat scroll 0 0 #fff!important;
    border-bottom: 1px solid #e6e6e6!important;
}
</style>
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid box-shadow multiple-backgrounds">
<?php include('common_view.php'); ?>
<?php include('header_view.php'); ?>
<div class="wrapper content login-wrapper">
    <div class="dynamic-row">

        <h1 class="title-wrapper">
            <?php echo LOGIN_WELCOME_BACK;?>
        </h1>
        <div class="alert-closable alert-hide login-error">
            <input class="alert-close" type="checkbox"/>
            <div class="alert-error">
                <p>
                    <?php echo LOGIN_USERNAME_ERROR;?>
                </p>
                <p>
                    <?php echo LOGIN_SIGNUP_TEXT;?> <a href="<?=$this->config->config['base_url']?>user/register"><?php echo LOGIN_SIGNUP;?></a>.
                </p>
            </div>
        </div>
        <div class="box">
            
            <form accept-charset="ISO-8859-1" action="#" method="post" name="login" id="login" novalidate>
                
                <fieldset>
                    <div class="form-field">
                        <label for="login_email"><?php echo LOGIN_EMAIL;?></label><input type="email" tabindex="100" name="login_email" id="login_email">
                    </div>
                    <div class="form-field no-validation-marker">
                        <label for="login_password"><?php echo LOGIN_PASS;?> </label><input type="password" tabindex="101" name="login_password" id="login_password">
                    </div>
                    <div class="form-field switch-field">
                        <label class="switch-label"><?php echo LOGIN_REM;?></label>
                        <div class="switch"><input type="checkbox" value="1" tabindex="102" name="remember" id="remember_1">
                            <label class="switch-trigger" for="remember_1"></label>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="form-field">
                        <button tabindex="103" class="bttn blue submit-bttn" type="submit"><?php echo BUTTON_LOGIN;?></button>
                    </div>
                </fieldset>                    
            </form>
        </div>

        <div class="login-with-facebook-wrapper">
            <h3 class="or">
                <?php echo LOGIN_OR_TEXT; ?>
            </h3>
            <a class="bttn facebook-bttn" href="<?=$this->config->config['base_url']?>user/fb_login">
                <div class="icon-font icon-font-facebook"></div>
               &nbsp;
                <span style="color:#fff;" class="separator">
                    |
                </span>
                <?php echo REGISTER_SIGNUPFB;?>
            </a>
            <a onclick="location.href='<?=$this->config->config['base_url']?>hauth/login/Google'" class="bttn blue submit-bttn" style="background:#ac0000; border:#ac0000; margin-top: 10px; padding: 15px 0;width: 100%;" id="show" tabindex="103"> 
                <div style="margin-right:0px;" class="icon-font icon-font-gplus googlp"></div>
                <span class="separator">
                    |
                </span>&nbsp;<?php echo LOGIN_SIGNUP_WITH_GOOGLE;?>&nbsp;&nbsp;&nbsp;
            </a>
        </div>

        <div class="bottom-link-wrapper">
            <a href="<?=$this->config->config['base_url']?>user/reset_password">
                <?php echo LOGIN_FORGOTPASS;?>
            </a>
            <span class="separator">
                |
            </span>
            <a id="register-instead" href="<?=$this->config->config['base_url']?>user/register">
                <?php echo LOGIN_DONTHAVEACC;?>
            </a>
        </div>

        </div>
    </div>
<? include('footer_mini_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
$(function() {
    $("#login_email").focus();
});
$(document).ready(function () {
    $("#login").validate({
        rules: {
            login_email: {
                required: true,
                email:true
            },
            login_password: {
                required: true
            }
        },
        submitHandler: function(form) {
            $.post(Host+'user/login_submit', $("#login").serialize(), function(data) {
                var dtarr=data.split('||');
                if(dtarr[0]=='failed'){
                    $('.login-error').show();
                }else if(dtarr[0]=='inactive'){
                    $('.login-error').hide();
                    location.href='<?=$this->config->config['base_url']?>user/account_deactivate';
                }else if(dtarr[0]=='not_confirmed'){
                    $('.login-error').hide();
                    location.href='<?=$this->config->config['base_url']?>user/not_confirmed/'+dtarr[1];
                }else{                    
                    $('.login-error').hide();
                    <?php if($target!=''){ ?>                        
                          location.href='<?php echo $this->config->config['base_url'].$target; ?>';
                    <?php }else{ ?>
                        if(dtarr[1]=='user'){ 
                            location.href='<?=$this->config->config['base_url']?>profile/dashboard';
                        }else{ 
                            location.href='<?=$this->config->config['base_url']?>profile/requests';
                        }
                    <?php } ?>
                }
            });     
        }
    });    
});
</script>
</body>
</html>

