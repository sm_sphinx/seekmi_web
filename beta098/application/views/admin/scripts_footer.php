<style type="text/css">
.cls_largeModal{
	width:55%;
	margin-left: -28%;  }
@media (max-width: 768px) {
	.cls_largeModal{
		width:75%;
		margin-left: 2%; 
	}	
}
</style> 
    <script type="text/javascript">
	  var Host='<?php echo $this->config->config['base_url']; ?>';
	</script>
	<!-- END COPYRIGHT -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="<?=base_url()?>assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?=base_url()?>assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<?=base_url()?>assets/plugins/excanvas.min.js"></script>
	<script src="<?=base_url()?>assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="<?=base_url()?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="<?=base_url()?>assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->