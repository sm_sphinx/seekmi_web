<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<style type="text/css">
    .preloader {
        background: url("<?= base_url() ?>images/pattern2.png") repeat scroll 0 0 rgba(0, 0, 0, 0);
        height: 100%;
        left: 0;
        position: fixed;
        text-align: center;
        top: 0;
        transition: all 0.17s ease-in-out 0s;
        width: 100%;
        z-index: 2;
    }
    .preloader h2 {
        color: #434343;
        font-family: "UbuntuR","Open Sans",sans-serif;
        font-size: 60px;
        left: 0;
        padding-top: 4px;
        position: absolute;
        text-align: center;
        text-transform: uppercase;
        top: 45%;
        width: 100%;
    }
    .preloader h2 img {
        display: inline-block;
        margin-left: 30px;
        margin-top: -10px;
    }
    .loader-text-progress{
        font-size: 26px;
        font-weight: bold;
    }
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<div class="preloader" style="display: none;">
    <h2>
    Montego
    <img src="<?= base_url() ?>images/loader.gif" alt="">
    <br/><span class="loader-text-progress"></span>
    </h2>    
</div>
    <?php include('header_view.php'); ?>
    <?php
    if (isset($app_info)) {
        $userId = $app_info->userId;
        $status = $app_info->status;
        $ios_apk = $app_info->ios_apk; 
        $android_apk = $app_info->android_apk; 
        $android_source_code=$app_info->android_source_code_path;
        $ios_source_code=$app_info->ios_source_code_path;
    } else {
        $userId = '';
        $status = '';
        $ios_apk = '';   
        $android_apk='';
        $android_source_code='';
        $ios_source_code='';
    }
    ?>        
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
<?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content" id="mainDiv">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid" >
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END BEGIN STYLE CUSTOMIZER --> 
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                        <?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">							
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= base_url() ?>admin/user/edit_user/<?=$userId?>">Manage Apps</a> 
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="#"><?= $page_title ?></a></li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid profile">
                    <div class="span12">
                        <!--BEGIN TABS-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-reorder"></i><?= $page_title ?></div>

                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" name="myBuildFrm" id="myBuildFrm" method="post" action="<?= base_url() ?>admin/app/submit_build" enctype="multipart/form-data">
                                    <input type="hidden" name="user_id" id="user_id" value="<?=$userId ?>"/>  
                                    <input type="hidden" name="app_id" id="app_id" value="<?=$appId ?>"/>
                                    <input type="hidden" name="android_apk_hid" id="android_apk_hid" value="<?=$android_apk ?>"/>
                                    <input type="hidden" name="ios_apk_hid" id="ios_apk_hid" value="<?=$ios_apk ?>"/>
                                    
                                    <div class="control-group">
                                        <a href="<?= base_url() ?>montegoTest.zip" class="btn green" target="_blank"> Download ios source code</a>                                        
                                        <a href="javascript:;" onclick="download_source_code('android');" class="btn green"> Download android source code</a>                                            
                                    </div>
                                                              
                                    <div class="control-group">
                                        <label class="control-label">Upload ios build </label>
                                        <div class="controls">
                                            <div class="">
                                                <input type="file" name='txtIosBuild' id='txtIosBuild' value='' class="default"/>

                                            </div>
                                        </div>
                                    </div>
                                   <?php if($ios_apk!=''){ ?>
                                    <div class="control-group">
                                            <label class="control-label"></label>
                                            <div class="controls">
                                                <a href="<?=base_url()?>app_apk/ios/<?=$ios_apk?>" target="_blank">ios apk </a>
                                            </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <div class="control-group">
                                        <label class="control-label">Upload android build </label>
                                        <div class="controls">
                                            <div class="">
                                                <input type="file" name='txtAndroidBuild' id='txtAndroidBuild' value='' class="default"/>

                                            </div>
                                        </div>
                                    </div>   
                                   
                                   <?php if($android_apk!=''){ ?>
                                    <div class="control-group">
                                            <label class="control-label"></label>
                                            <div class="controls">
                                                <a href="<?=base_url()?>app_apk/android/<?=$android_apk?>" target="_blank">android apk </a>
                                            </div>
                                    </div>
                                    <?php } ?>
                                    
                                    <div class="control-group">
                                        <label class="control-label">Status</label>
                                        <div class="controls">
                                            <div class="">                                                                 
                                                <select name="txtStatus" id="txtStatus"> 
                                                    <option value="Draft" <?php if ($status == 'Draft') { ?> selected<?php } ?>>Draft</option> 
                                                    <option value="Pending" <?php if ($status == 'Pending') { ?> selected<?php } ?>>Pending</option> 
                                                    <option value="Processing" <?php if ($status == 'Processing') { ?> selected<?php } ?>>Processing</option> 
                                                    <option value="Published" <?php if ($status == 'Published') { ?> selected<?php } ?>>Published</option>                                            
                                                    <option value="Cancelled" <?php if ($status == 'Cancelled') { ?> selected<?php } ?>>Cancelled</option>                                            
                                                </select>                                             
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <button type="submit" id="register-submit-btn" class="btn green ">
                                            Save                                  
                                        </button>
                                        <a href="<?= base_url() ?>admin/user/edit_user/<?=$userId?>" class="btn">Cancel</a>            
                                    </div>
                                </form>
                                <!-- END FORM-->  
                            </div>                            
                           
                        </div>
                        <!--END TABS-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->            
             
        </div>
        <!-- END PAGE -->    
    </div>
    <!-- END CONTAINER -->    
<?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>  
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
    <script src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>    
    <script src="<?= base_url() ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?= base_url() ?>assets/scripts/app.js"></script>     
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();        
    });
    
    function download_source_code(type){
        var pageContent = jQuery('#mainGridContainer');
        App.blockUI(pageContent, true);
        jQuery.ajax({
                type: "POST",
                url: '<?= base_url() ?>admin/app/check_source_code_avail',
                data: "appid=<?php echo $appId; ?>&type="+type,
                success: function(msg)
                {
                      App.unblockUI(pageContent);
                      if(msg!=''){
                        var r = confirm("Are you want to regenerate source code?");
                        if (r == true) {
                           getSourceCode('android','tile','16'); 
                        } else {
                           window.open('<?=base_url()?>source_code_zip/'+type+'/'+msg);                           
                        }                           
                      }else{
                        getSourceCode('android','tile','16');
                      }
                }
            });
    }
    
    function getSourceCode(type,step,num){             
            
        if(step=='tile' && num!=''){
        var str_html='Please wait, creating tiles for zoom level '+num;
        }
        if(step=='html'){
        var str_html='Please wait, creating html for source code';
        }
        if(step=='zip'){
        var str_html='Please wait, creating zip for source code';
        }
        $('.preloader .loader-text-progress').append(str_html);   
        $('.preloader').show();          
        jQuery.ajax({
            type: "POST",
            url: '<?= base_url() ?>admin/app/download_source_code',
            data: "appid=<?php echo $appId; ?>&type="+type+"&step="+step+"&num="+num,
            success: function(msg)
            {
                if(msg=='error'){
                   $('.preloader').hide();
                   $('.preloader h4').html('');
                   alert('Error while creating source code, please try again!');
                   return  false;                           
                }else{
                   var res=msg.split('||');
                   $('.preloader').hide();
                   $('.preloader .loader-text-progress').html('');
                   if(res[0]=='done'){
                       window.open('<?=base_url()?>'+res[1]);                        
                   }else{
                       getSourceCode(type,res[0],res[1]); 
                   }
                }
            }
        });
        
    }
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>