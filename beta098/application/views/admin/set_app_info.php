<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<script type="text/javascript" src="<?=base_url()?>js/ScribbleMap.js"></script> 
<script>
    var sm;
    var types = [];
    window.onload = function () {
        sm = new ScribbleMap("ScribbleMap");
        
    };
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <?php include('header_view.php'); ?>
    <?php
    if (isset($app_info)) {
        $appId = $app_info->appId;
        $appname = $app_info->appname;
        $appemail = $app_info->appemail;
        $website = $app_info->website;
        $company = $app_info->company;
        $address = $app_info->address;
        $city = $app_info->city;
        $state = $app_info->state;
        $country = $app_info->country;
        $zipcode = $app_info->zipcode;
        $phone = $app_info->phone;
        $cellphone = $app_info->cellphone;
        $latitude = $app_info->latitude;
        $longitude = $app_info->longitude;
        //$applogo = $app_info->applogo;
        $map_image=$app_info->map_image;
        $otherinfo=$app_info->otherinfo;
        //$active = $app_info->active;
        $appImage = array('1'=>'','2'=>'','3'=>'','4'=>'','5'=>'','6'=>'');        
        $i = 1;
        if($app_images)
        {
            foreach($app_images as $app_image_row)
            {                   
                $appImage[$i] = $app_image_row->appimage;
                $i++;
            }
        }
    } else {
        $appId = 0;
        $appname = '';
        $appemail = '';
        $website = '';
        $company = '';
        $address = '';
        $city = '';
        $state = '';
        $country = '';
        $zipcode = '';
        $phone = '';
        $cellphone = '';
        $latitude = '';
        $longitude = '';
       // $applogo = '';
        $map_image='';
        $appImage = array('1'=>'','2'=>'','3'=>'','4'=>'','5'=>'','6'=>'');
        $otherinfo='';
        //$active = '';
    }
    ?>        
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
<?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content" id="mainDiv">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid" >
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END BEGIN STYLE CUSTOMIZER --> 
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                        <?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">							
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= base_url() ?>admin/user/edit_user/<?php echo $uId; ?>">Manage Apps</a> 
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="#"><?= $page_title ?></a></li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
                    <div class="span12">
                            <div class="portlet box blue" id="form_wizard_1">
                            <div class="portlet-title">
                                    <div class="caption">
                                            <i class="icon-reorder"></i> <?= $page_title ?> - <span class="step-title">Step 1 of 3</span>
                                    </div>
                                    <div class="tools hidden-phone">
                                            <a href="javascript:;" class="collapse"></a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                            <a href="javascript:;" class="remove"></a>
                                    </div>
                            </div>
                            <div class="portlet-body form">
                                <form action="<?=base_url()?>admin/app/saveAppInfo/<?=$uId?>" method="post" class="form-horizontal" id="app_form" enctype="multipart/form-data">
                                    <input type="hidden" name="app_id" id="app_id" value="<?=$appId?>"/>
                                    <input type="hidden" name="user_id" id="user_id" value="<?=$uId?>"/>
                                    <input type="hidden" name="latitude" id="latitude" value="<?=$latitude?>"/>
                                    <input type="hidden" name="longitude" id="longitude" value="<?=$longitude?>"/>
                                   <?php /* <input type="hidden" name="app_logo_hid" id="app_logo_hid" value="<?=$applogo?>"/>*/ ?>
                                    <input type="hidden" name="map_image_hid" id="map_image_hid" value="<?=$map_image?>"/>
                                    <div class="form-wizard">
                                        <div class="navbar steps">
                                        <div class="navbar-inner">
                                        <ul class="row-fluid">
                                        <li class="span3">
                                                <a href="#tab1" data-toggle="tab" class="step ">
                                                <span class="number">1</span>
                                                <span class="desc"><i class="icon-ok"></i> APP Details</span>   
                                                </a>
                                        </li>
                                        <li class="span3">
                                                <a href="#tab2" data-toggle="tab" class="step">
                                                <span class="number">2</span>
                                                <span class="desc"><i class="icon-ok"></i> Map & Navigation Info</span>   
                                                </a>
                                        </li>
                                        <li class="span3">
                                                <a href="#tab3" data-toggle="tab" class="step active">
                                                <span class="number">3</span>
                                                <span class="desc"><i class="icon-ok"></i> Upload APP Photos</span>   
                                                </a>
                                        </li>
                                        <li class="span3">
                                                <a href="#tab4" data-toggle="tab" class="step active">
                                                <span class="number">4</span>
                                                <span class="desc"><i class="icon-ok"></i> Preview Your APP</span>   
                                                </a>
                                        </li>
                                        </ul>
                                        </div>
                                        </div>
                                        <div id="bar" class="progress progress-success progress-striped">
                                                <div class="bar"></div>
                                        </div>
                                       <div class="tab-content">
                                        <div class="alert alert-error hide">
                                                <button class="close" data-dismiss="alert"></button>
                                                You have some form errors. Please check below.
                                        </div>
                                        <div class="alert alert-error-image hide">
                                                <button class="close" data-dismiss="alert"></button>
                                                <span></span>
                                        </div>
                                        <div class="alert alert-success hide">
                                                <button class="close" data-dismiss="alert"></button>
                                                Your form validation is successful!
                                        </div>
                                        <div class="tab-pane active" id="tab1">
                                            <h3 class="block">Provide your app details</h3>
                                            <div class="control-group">
                                                    <label class="control-label">APP Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <input type="text" class="span6 m-wrap" name="app_name" value="<?=$appname?>"/> 
                                                       <span class="help-inline">etc. Hotel Albatros, Restaurant Horizon, Car service John Doe</span>
                                                    </div>
                                            </div>                                                                            
                                            <div class="control-group">
                                                    <label class="control-label">Email<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="span6 m-wrap" name="app_email" value="<?=$appemail?>"/>                                                                                            
                                                    </div>
                                            </div>
                                            <?php /*<div class="control-group">
                                                    <label class="control-label">APP Logo</label>
                                                    <div class="controls">
                                                        <input type="file" class="default" name="app_logo">
                                                    </div>
                                            </div>
                                            <?php if($applogo!=''){ ?>
                                            <div class="control-group">
                                                    <label class="control-label"></label>
                                                    <div class="controls">
                                                        <img src="<?=base_url()?>app_logo/<?=$applogo?>" width="75" height="75" />
                                                    </div>
                                            </div>
                                            <?php } */?>
                                            <div class="control-group">
                                                    <label class="control-label">Website</label>
                                                    <div class="controls">
                                                        <input type="text" class="span6 m-wrap" name="app_website" value="<?=$website?>"/> 
                                                       <span class="help-inline">* optional</span>
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label">Company<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <input type="text" class="span6 m-wrap" name="app_company" value="<?=$company?>"/> 
                                                       <span class="help-inline">name of company, hotel chain, business chain</span>
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label">Address<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <input type="text" class="span6 m-wrap" name="app_address" id="app_address" value="<?=$address?>"/>                                                                                            
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label">City<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <input type="text" class="span6 m-wrap" name="app_city" id="app_city" value="<?=$city?>"/>                                                                                            
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label">State<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <input type="text" class="span6 m-wrap" name="app_state" id="app_state" value="<?=$state?>"/>                                                                                            
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label">Country<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <select name="app_country" id="country_list" class="span6">
							  <option value="">Select</option>   
                                                          <?php 
                                                           foreach($country_list as $country_row){
                                                             if($country==$country_row->code){
                                                                 $sel=' selected';
                                                             }else{
                                                                 $sel='';
                                                             }
                                                             echo '<option value="'.$country_row->code.'" '.$sel.'>'.$country_row->name.'</option>';  
                                                           }
                                                          ?>
                                                       </select>   
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label">Zip<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <input type="text" class="span6 m-wrap" name="app_zipcode" id="app_zipcode" value="<?=$zipcode?>"/>                                                                                            
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label">Phone<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <input type="text" class="span6 m-wrap" name="app_phone" value="<?=$phone?>"/>     
                                                      <span class="help-inline">etc. Hotel Albatros, Restaurant Horizon, Car service John Doe</span>
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label">Cell Phone</label>
                                                    <div class="controls">
                                                       <input type="text" class="span6 m-wrap" name="app_cellphone" value="<?=$cellphone?>"/>   
                                                       <span class="help-inline">etc. Hotel Albatros, Restaurant Horizon, Car service John Doe</span>
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label">Other Info</label>
                                                    <div class="controls">
                                                        <textarea class="span6 m-wrap" name="app_otherinfo" value="<?=$otherinfo?>"></textarea>                                                          
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab2">
                                             <h3 class="block">Mark Your Object On Google Map</h3>  
                                             <div class="control-group">
                                              <div id="ScribbleMap" style="width: 980px; height: 600px;"></div>
                                             </div>
                                             <div class="control-group">
                                                    <label class="control-label">Upload Map Image<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input type="file" class="default" name="map_image">
                                                        <span class="help-inline">Mark you object on Google Map and save this map using save image. That image will be uploaded here.</span>
                                                    </div>
                                             </div>
                                             <?php if($map_image!=''){ ?>
                                             <div class="control-group">
                                                    <label class="control-label"></label>
                                                    <div class="controls">
                                                        <img src="<?=base_url()?>map_images/<?=$map_image?>" width="75" height="75" />
                                                    </div>
                                             </div>
                                             <?php } ?>
                                        </div>
                                        <div class="tab-pane" id="tab3">
                                            <h3 class="block">Upload Photos</h3>
                                            <?php for($i=1;$i<=6;$i++){ ?>
                                            <div class="control-group">
                                                    <label class="control-label">Photo <?php echo $i; ?><span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input type="file" class="default" name="app_image<?=$i?>">
                                                        <?php if($appImage[$i]!=''){ ?>
                                                        <img src="<?=base_url()?>app_images/<?=$appImage[$i]?>" width="100" height="100" />
                                                        <?php } ?>
                                                    </div>
                                            </div>
                                            <input type="hidden" name="app_image_old_<?=$i?>" id="app_image_old_<?=$i?>" value="<?=$appImage[$i]?>"/>
                                            <?php } ?>                                            
                                       </div>  
                                       <div class="tab-pane" id="tab4">
                                            <h3 class="block">Preview Your APP</h3>
                                                                                       
                                       </div>
                                       </div>
                                        <div class="form-actions clearfix">
                                                <a href="javascript:;" class="btn button-previous">
                                                <i class="m-icon-swapleft"></i> Back 
                                                </a>
                                                <a href="javascript:;" onclick="getLatlong();" class="btn blue button-next">
                                                Continue <i class="m-icon-swapright m-icon-white"></i>
                                                </a>
                                                <a href="javascript:;" class="btn green button-submit">
                                                Submit <i class="m-icon-swapright m-icon-white"></i>
                                                </a>
                                        </div>
                                </div>
                                </form>
                            </div>
                            </div>
                    </div>
            </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER--> 
        </div>
        <!-- END PAGE -->    
    </div>
    <!-- END CONTAINER -->    
<?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>   
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>  
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>	
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>    
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js"></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?= base_url() ?>assets/scripts/app.js"></script> 
    <script type="text/javascript" src="<?=base_url()?>assets/scripts/userapp.js"></script> 
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
        Userapp.init();       
    });
    
    function getLatlong()
    {
        var geocoder = new google.maps.Geocoder();
        var address = '';
        var addr=document.getElementById("app_address").value;
        var city=document.getElementById("app_city").value;
        var state=document.getElementById("app_state").value;
        var country=document.getElementById("country_list").value;
        var zip=document.getElementById("app_zipcode").value;
        if(addr!='') address+=addr;
        if(city!='') address+=", "+city;
        if(state!='') address+=", "+state;
        if(zip!='') address+=", "+zip;
        if(country!='') address+=", "+country;
        if(address!=''){
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                document.getElementById("latitude").value=latitude;
                document.getElementById("longitude").value=longitude;
                sm.ui.setAvailableTools(["menu","edit", "line","eraser","drag","label"]);
                types.push({
                        id: "hybrid",
                        label: "Hybrid"
                    },
                    {
                        id: "satellite",
                        label: "Satellite"
                    });
                sm.ui.setMapTypes(types);
                //var bounds = sm.view.getBounds();
                //console.log(bounds);
                //var zoom = sm.view.getZoom(); 
                sm.view.setCenter([latitude, longitude]);
                sm.view.setZoom(18);
                //Adjust sizes
                var c = scribblemaps.ControlType;
                sm.ui.styleControl(c.SEARCH, { width: "340px" });
                sm.ui.styleControl(c.LINE_SETTINGS, { left: "360px","display":"none" });
                sm.ui.styleControl(c.LINE_COLOR, { left: "360px" ,"display":"none" });
                sm.ui.styleControl(c.FILL_COLOR, { left: "420px" ,"display":"none" });
                //alert("Latitude: " + latitude + "\nLongitude: " + longitude);
            } else {
                document.getElementById("latitude").value='';
                document.getElementById("longitude").value='';
                alert("Invalid Address.");
                return false;
            }
        });
      }else{
      }
      
    }    
    
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>