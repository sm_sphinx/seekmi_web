<?php include('style_header.php'); ?>
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?=$this->config->config['base_url']?>assets/css/pages/login.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
	<!-- BEGIN LOGO -->
	<div class="logo">
            <img src="<?=base_url()?>assets/img/logo.png" alt="Montego"/>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<form class="form-vertical login-form" action="" method="post">
			<h3 class="form-title">Login to your account</h3>
			<div class="alert alert-error hide">
				<button class="close" data-hide="alert"></button>
				<span>Enter any email and password</span>
			</div>
            <div class="alert alert-error1 hide">
				<button class="close" data-hide="alert"></button>
				<span>You have no account for this email/wrong password.</span>
			</div>
            <div class="alert alert-error2 hide">
				<button class="close" data-hide="alert"></button>
				<span>Your account not activate yet. Please check your mail for account activation.</span>
			</div>
            <div class="alert alert-error-forgot alert-success hide">
				<button class="close" data-hide="alert"></button>
				<span>Please check your email for reset your password.</span>
			</div>
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Email</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<input class="m-wrap placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<!--<label class="checkbox">
				<input type="checkbox" name="remember" value="1"/> Remember me
				</label>-->
				<button type="submit" class="btn green pull-right">
				Login <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>			
			
		</form>
		<!-- END LOGIN FORM -->        
		
		
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright">
		<?=date('Y')?> &copy; Montego Admin.
	</div>
	<!-- END COPYRIGHT -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<?php include('scripts_footer.php'); ?>
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=$this->config->config['base_url']?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
	<script type="text/javascript" src="<?=$this->config->config['base_url']?>assets/plugins/select2/select2.min.js"></script>     
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=$this->config->config['base_url']?>assets/scripts/app.js" type="text/javascript"></script>
	<script src="<?=$this->config->config['base_url']?>assets/scripts/login.js" type="text/javascript"></script> 
	<!-- END PAGE LEVEL SCRIPTS --> 
	<script>
	    var Host='<?=$this->config->config['base_url']?>';
		jQuery(document).ready(function() {     
		  App.init();
		  Login.init();	
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>