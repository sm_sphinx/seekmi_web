<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->    
    <ul class="page-sidebar-menu">
        <li>&nbsp;</li>	
        <?php if($this->session->userdata('user_type')=='admin'){
            $customer_title='Manage Customer';
        } else{
            $customer_title='My Customer';
        }
        ?>
        <li <?php if ($page_title == $customer_title || $page_title == 'Edit Customer' || $page_title == 'Add Customer' || $page_title=='Manage Build') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['base_url'] ?>admin/user/users">
                <i class="icon-table"></i> 
                <span class="title"><?=$customer_title?></span>
                <?php if ($page_title == $customer_title || $page_title == 'Edit Customer' || $page_title == 'Add Customer' || $page_title=='Manage Build') { ?>
                    <span class="selected"></span>
                <?php } ?>					
            </a>					
        </li> 
        <?php if($this->session->userdata('user_type')=='admin'){ ?>
        <li <?php if ($page_title == 'Manage Reseller' || $page_title == 'Edit Reseller' | $page_title == 'Add Reseller') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['base_url'] ?>admin/reseller/lists">
                <i class="icon-table"></i> 
                <span class="title">Manage Reseller</span>
                <?php if ($page_title == 'Manage Reseller' || $page_title == 'Edit Reseller' | $page_title == 'Add Reseller') { ?>
                    <span class="selected"></span>
                <?php } ?>					
            </a>					
        </li>
        <?php } ?>

    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->