<?php include('style_header.php'); ?>
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?=base_url()?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
   	<link href="<?=base_url()?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<?php include('header_view.php'); ?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid" id="mainGridContainer">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<?php include('left_view.php'); ?>
		</div>
		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- END BEGIN STYLE CUSTOMIZER --> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							My Account
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?=base_url()?>user/app_users">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#">My Account</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom tabbable-full-width">
							<ul class="nav nav-tabs">
								<li  class="active"><a href="#tab_1_2" data-toggle="tab">My Account</a></li>
							</ul>
							<div class="tab-content">
								<!--tab_1_2-->
								<div class="tab-pane row-fluid profile-account active" id="tab_1_2">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
												<ul class="ver-inline-menu tabbable margin-bottom-10">
													<li class="active">
														<a data-toggle="tab" href="#tab_1-1">
														<i class="icon-cog"></i> 
														My Personal Info
														</a> 
														<span class="after"></span>                                    
													</li>
												<?php /*?>	<li ><a data-toggle="tab" href="#tab_2-2"><i class="icon-picture"></i> <?=$webword['change_avatar']?></a></li>
												<?php */?>	<li ><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i>Change Password</a></li>
												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1-1" class="tab-pane active">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse">
															<form action="#" id="profile_info_form" name="profile_info_form" class="profile_info_form" method="post">
																<input type="hidden" id="userId" name="userId" value="<?=$user_info->userId?>">
                                                                <div class="alert alert-success hide">
                                                                    <button class="close" data-hide="alert" type="button"></button>
                                                                    User information has been updated successfully.
                                                                </div>
                                                                
                                                                <div class="control-group">
                                                                    <label class="control-label">Username<span class="required">*</span></label>
                                                                    <div class="controls">
                                                                        <div class="input-icon left">
                                                                           <i class="icon-user"></i>
                                                                            <input type="text" placeholder="Username" value="<?=$user_info->userName?>" name="txtUsername" id="txtUsername" class="m-wrap span8" readonly="true" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            	<div class="control-group">
																	<label class="control-label">Email</label>
                                                                    <div class="controls">
                                                                        <div class="input-icon left">
                                                                            <i class="icon-envelope"></i>
                                                                                <input type="text"  class="m-wrap span8" value="<?=$user_info->email?>" placeholder="Email" name="txtEmail" id="txtEmail" readonly="true" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            	<div class="submit-btn">
																    <button type="submit" id="myAccount-submit-btn" class="btn green ">Save Changes</button>
                                                                    <a href="<?=base_url()?>user/app_users" class="btn">Cancel</a>
																</div>
															</form>
														</div>
													</div>
													<?php /*?><div id="tab_2-2" class="tab-pane">
														<div style="height: auto;" id="accordion2-2" class="accordion collapse">
															<form method="post" class="user_photo_form" action="<?=$this->config->config['base_url']?>admin/user/save_profile_photo" enctype="multipart/form-data">										
																<div id="profileMessageBox"></div>
																<div class="controls">
																	<div class="thumbnail" style="width: 100px; height: 100px;" id="profileImageDiv">
                                                                      <?php if($user_info->photoURL==''){ ?>
																		<img src="http://www.placehold.it/100x100/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                                        <?php }else{ ?>
                                                                        <img src="<?=base_url()?>admin_image/thumbs/<?=$user_info->photoURL?>" alt="" />
                                                                        <?php } ?>
																	</div>
																</div>
																<div class="space10"></div>
																<div class="fileupload fileupload-new" data-provides="fileupload" >
																	
                                                                      <input type="hidden" name="profile_photo_hid" id="profile_photo_hid" value="<?=$user_info->photoURL?>" />
																</div>
																<div class="clearfix"></div>																
																<div class="space10"></div>
																<div class="submit-btn">
																	<button type="button" id="btnSubmitImage" class="btn green"><?=$webword['upload_photo']?></button>																	
																</div>
															</form>
														</div>
													</div><?php */?>
													<div id="tab_3-3" class="tab-pane">
														<div style="height: auto;" id="accordion3-3" class="accordion collapse">
															<form action="#" id="password_form" name="password_form" class="password_form" method="post">
																<input type="hidden" id="userId" name="userId" value="<?=$user_info->userId?>">
                                                                <!--<label class="control-label">Current Password</label>
																<input type="password" class="m-wrap span8" />
																<label class="control-label">New Password</label>
																<input type="password" class="m-wrap span8" />
																<label class="control-label">Re-type New Password</label>
																<input type="password" class="m-wrap span8" />
																-->
                                                                <div class="alert alert-success hide">
                                                                    <button class="close" data-hide="alert" type="button"></button>
                                                                    Password has been saved successfully.
                                                                </div>
                                                                 <div class="alert alert-error hide" id="samePasswordError">
                                                                    <button class="close" data-hide="alert" type="button"></button>
                                                                 	Old password and new password should not be same.
                                                                </div>
                                                                
                                                                <div class="control-group">
                                                                    <label class="control-label visible-ie8 visible-ie9">Old Password<span class="required">*</span></label>
                                                                    <div class="controls">
                                                                        <div class="input-icon left">
                                                                            <i class="icon-lock"></i>
                                                                            <input type="password" id="oPassword" placeholder="Old Password"  name="oPassword" class="m-wrap span8" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label visible-ie8 visible-ie9">New Password<span class="required">*</span></label>
                                                                    <div class="controls">
                                                                        <div class="input-icon left">
                                                                            <i class="icon-lock"></i>
                                                                            <input type="password" id="nPassword" placeholder="New Password"  name="nPassword" class="m-wrap span8" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                 <div class="control-group">
                                                                    <label class="control-label visible-ie8 visible-ie9">Re-type New Password<span class="required">*</span></label>
                                                                    <div class="controls">
                                                                        <div class="input-icon left">
                                                                            <i class="icon-lock"></i>
                                                                            <input type="password" placeholder="Re-type New Password"  name="rtPassword" class="m-wrap span8"  />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                              <div class="submit-btn">
																	<!--<a href="#" class="btn green">Change Password</a>-->
                                                                    <button type="submit" id="myAccount-submit-btn" class="btn green ">
																	Change Password</i>
                                                                    </button>            
																	<a href="<?=base_url()?>user/app_users" class="btn">Cancel</a>
																</div>
														</form>
														</div>
													</div>
													
												</div>
											</div>
											<!--end span9-->                                   
										</div>
									</div>
								</div>
							
								
								
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER--> 
		</div>
		<!-- END PAGE -->    
	</div>
	<!-- END CONTAINER -->
	<?=include('scripts_footer.php')?>
	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="<?=base_url()?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
	<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/select2.min.js"></script>     
	<script src="<?=base_url()?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>


    <!-- END PAGE LEVEL PLUGINS -->
   
   
<?php /*?>   <script type="text/javascript" src="<?=base_url()?>js/Ajaxfileupload-jquery-1.3.2.js" ></script>
    <script type="text/javascript" src="<?=base_url()?>js/ajaxupload.3.5.js" ></script>
    <script>	
		
		jQuery(function(){
		
		var btnUpload=jQuery('#btnSubmitImage');
		var message=jQuery('#profileMessageBox');
		
		new AjaxUpload(btnUpload, {
			action: Host+'admin/user/save_profile_photo',
			name: 'profile_photo',
			onSubmit: function(file, ext){
				 showloader();
				 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
					message.html('<div class="alert alert-block alert-error fade in"><button class="close" data-dismiss="alert"></button>Only JPG, PNG or GIF files are allowed.</div>');
					//message.text('Only JPG, PNG or GIF files are allowed.');
				
					
					hideloader();
					
					return false;
				}
				//jQuery("#profile_image_status").html('<img src="<?=base_url()?>/images/3bk.gif"  height="16" width="16">');
			},
			onComplete: function(file, response){
				//message.text('Photo uploaded sucessfully!!');
				message.html('<div class="alert alert-block alert-success fade in"><button class="close" data-dismiss="alert"></button>Photo uploaded sucessfully!!</div>');
				if(response==="error"){
					//message.text('Error.');
					message.html('<div class="alert alert-block alert-error fade in"><button class="close" data-dismiss="alert"></button>Error</div>');
				} 
				else if (response ==="not_set_file")
				{
					message.html('<div class="alert alert-block alert-error fade in"><button class="close" data-dismiss="alert"></button>Please select image for upload.</div>');
					//message.text('Please select image for upload.');
				}
				else
				{
					jQuery("#profile_photo_hid").val(response);
					jQuery("#userTopImage").attr('src', '<?=base_url()?>admin_image/thumbs/'+response);
					jQuery("#profileImageDiv").html('<img src="<?=base_url()?>admin_image/thumbs/'+response+'" id="profileImg" alt="No Image" />');
				}
				hideloader();
			}
		});
	});
	
	</script>
   <?php */?>
   


    <script src="<?=base_url()?>assets/scripts/profile_setting.js" type="text/javascript"></script> 
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
	<?php /*?><script src="<?=base_url()?>assets/scripts/app.js"></script>      
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {       
		   // initiate layout and plugins
		   App.init();
		   Setting.init();
		});
	function showloader()
	{
		jQuery('#fade').show();
		jQuery('#loader').fadeIn();	
		var popuptopmargin = (jQuery('#loader').height() + 10) / 2;
		var popupleftmargin = (jQuery('#loader').width() + 10) / 2;
	   
		jQuery('#loader').css({
			'margin-top' : -popuptopmargin,
			'margin-left' : -popupleftmargin
		});
	}

	function hideloader()
	{
		jQuery('#fade').fadeOut();
		jQuery('#loader').fadeOut();
	}
		
	</script><?php */?>
   
   <div id="loader" class="loader_class" style="position: fixed; top: 50%; left: 50%; z-index: 999999; display:none;">
	<img src="<?=base_url()?>assets/img/ajax-loading.gif" />
</div>
<div id="fade"></div>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<script src="<?=base_url()?>assets/scripts/app.js"></script>  
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {       
		   // initiate layout and plugins
		   App.init();
		   Setting.init();
		   
		});
	</script>