<!doctype html>
<html lang="en" class="no-js">
<!-- Mirrored from nunforest.com/site/element-demo/boxed/pricing.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Jan 2015 06:53:45 GMT -->
<head>
    <title><?=$page_title?> | Montego</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/bootstrap.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/magnific-popup.css" media="screen">	
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/animate.css" media="screen">
<!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/style.css" media="screen">
    
</head>
<body id="top">
<? include('header_view.php'); ?>
<!-- ####################################################################################################### -->
<div id="content">


<!-- page-banner-section -->
<div class="section-content page-banner-section2">
        <div class="container">
                <div class="row">
                        <div class="col-sm-6">
                                <h1>About Us</h1>
                        </div>
                        <div class="col-sm-6">
                                <ul>
                                    <li><a href="<?=$this->config->config['base_url']?>">Home</a></li>
                                    <li>About Us</li>
                                </ul>
                        </div>
                </div>
        </div>
</div>

<!-- about-section -->
<div class="section-content about-section bord-bottom">
        <div class="container">
                <div class="row">
                        <div class="col-md-6">
                                <div class="about-content triggerAnimation animated" data-animate="fadeInRight">
                                        <h1>Who We Are &amp; What We Do</h1>
                                        <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Prae sent non sem eu mi malesuada viverra volutpat ut libero. Nullam a venenatis tellus. Nam fen convallis tristique. In imperdiet est sed aliquet imperdiet. Nulla libero orci, cursus ut consecr ac, tempus a odio. Etiam quis massa ac ante adipiscing consectetur. Duis dui turpis, porttitor sit amet metus sed, interdum tempus ipsum. Integer aliquam sem elementum pellentesque. Donec mollis eros dolor, a ongue neque venenatis vulputate. Etiam a eros adipiscing, dapibus ante id, luctus massa. Maecenas non quam interdum, aliquam leo a, ornare mi. Integer. faucibus turpis sed leo gravida laoreet. Curabitur at ligula. </p>
                                </div>
                        </div>
                        <div class="col-md-6">
                                <div class="skills-area triggerAnimation animated" data-animate="fadeInLeft">
                                        <h1>Our Skills</h1>

                                        <div class="skills-box">
                                                <div class="skills-progress">
                                                        <p>Develope<span>87%</span></p>
                                                        <div class="meter nostrips develope">
                                                         <p style="width: 87%"></p>
                                                        </div>
                                                        <p>Design<span>95%</span></p>
                                                        <div class="meter nostrips design">
                                                         <p style="width: 95%"></p>
                                                        </div>
                                                        <p>Branding<span>98%</span></p>
                                                        <div class="meter nostrips branding">
                                                         <p style="width: 98%"></p>
                                                        </div>
                                                        <p>Web applications<span>65%</span></p>
                                                        <div class="meter nostrips web-applications">
                                                         <p style="width: 65%"></p>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>

<!-- services-section -->
<div class="section-content services-section bord-bottom">
        <div class="services-box2">
                <div class="container">
                        <div class="row">

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-css3"></i></span>
                                                <div class="services-content">
                                                        <h2>Font Awesome Integrated</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque.</p>
                                                </div>
                                        </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-cogs"></i></span>
                                                <div class="services-content">
                                                        <h2>Fully Customizable</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque.</p>
                                                </div>
                                        </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-laptop"></i></span>
                                                <div class="services-content">
                                                        <h2>Responsive &amp; Retina Ready</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque.</p>
                                                </div>
                                        </div>
                                </div>

                        </div>
                </div>
        </div>
        <div class="services-box2">
                <div class="container">
                        <div class="row">

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-css3"></i></span>
                                                <div class="services-content">
                                                        <h2>Font Awesome Integrated</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque.</p>
                                                </div>
                                        </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-cogs"></i></span>
                                                <div class="services-content">
                                                        <h2>Fully Customizable</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque.</p>
                                                </div>
                                        </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-laptop"></i></span>
                                                <div class="services-content">
                                                        <h2>Responsive &amp; Retina Ready</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque.</p>
                                                </div>
                                        </div>
                                </div>

                        </div>
                </div>
        </div>
</div>

</div>
<!-- ####################################################################################################### -->
<? include('footer_view.php'); ?>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.migrate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/waypoint.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/plugins-scroll.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/script.js"></script>  	
</body>
</html>
