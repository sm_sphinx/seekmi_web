<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SeekMi - <?=$page_title?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<style type="text/css">
.glorious-header1{
    background: none repeat scroll 0 0 #fff!important;
    border-bottom: 1px solid #e6e6e6!important;
}
</style>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
    <?php include('common_view.php'); ?>
<div class="glorious-header glorious-header1" data-section="header">
<div class="wrapper"><div class="row header-row">
<div class="header-logo">
<a href="<?=$this->config->config['base_url']?>"><img src="<?=$this->config->config['base_url']?>images/seekmilogo.png" alt="SeekMi"></a></div>
<div class="header-middle-container"></div>
</div>    
</div>
</div>


<div class="wrapper signup-wrapper">
    <div class="dynamic-row">
        <h1 class="title-wrapper">
            Submit a request 
        </h1>        
        <div class="box">
            <div class="confirmation" id="success-support" style="display:none;">
        <div class="alert-closable">
            <input type="checkbox" class="alert-close">
            <div class="alert-success">
                <p>Email sent!</p>
            </div>
        </div>
        </div>
        <form accept-charset="ISO-8859-1" action="" method="post" name="supportForm" id="supportForm" novalidate>
          <fieldset>
                    <ul>                        
                        <div class="form-field">
                            <label for="usr_email">Your email address</label>
                            <input type="text" tabindex="102" name="email_addr" id="email_addr">
                        </div>
                        <div class="form-field">
                            <label for="subject">Subject</label>
                            <input type="text" tabindex="103" name="subject" id="subject">
                        </div>
                        <div class="form-field">
                            <label for="subject">Message</label>
                            <textarea name="message" id="message"></textarea>
                            <p>Let us know how we can help. The more details your provide, the better and faster we’ll be able to assist you. We will be in touch soon.</p>
                        </div>
                        <div class="form-field">
                            <label for="subject">Are you a professional or a customer?</label>
                            <input type="text"  name="usertype" id="usertype">
                        </div>
                        <div class="form-field">
                            <label for="subject">What can we help you with?</label>
                            <input type="text" name="helptype" id="helptype">
                        </div>
                    </ul>
                </fieldset>

                <fieldset>
                    <div class="form-field">
                        <button tabindex="104" class="bttn blue submit-bttn" type="submit">Submit</button>
                    </div>
                </fieldset>

            </form>

        </div>

    </div>
</div>
<? include('footer_mini_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">

$(document).ready(function () {
    $("#supportForm").validate({
        rules: {
            email_addr: {
                required: true,
                email: true,
            },
            subject: {
                required: true                
            },
            message: {
                required: true                
            },
            usertype: {
                required: true
            },
            helptype: {
                required: true
            }
        },
        submitHandler: function(form) {       
        
            $.ajax({
                type:'post',
                url:Host+"home/submit_request",
                data:$("#supportForm").serialize(),
                success : function(data)
                {	
                    if(data=='success')
                    {
                        $('#success-support').show();
                    }
                }
            });                  
        }
    });
});
</script>
</body>
</html>

