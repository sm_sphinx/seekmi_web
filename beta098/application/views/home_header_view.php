<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4HXQT"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T4HXQT');</script>
<!-- End Google Tag Manager -->
<?php
                //echo get_cookie('qtrans_front_language');
                if(get_cookie('qtrans_front_language'))
                {
                    if(get_cookie('qtrans_front_language')=='en')
                    {
                        $cookie = array(
                            'name' => 'language',
                            'value' => 'english',
                            'expire' => time() + 86500,
                        );
                        set_cookie($cookie);
                        include($_SERVER['DOCUMENT_ROOT'].'/config/eng_constants.php');
                    }
                    else
                    {
                        $cookie = array(
                            'name' => 'language',
                            'value' => 'indonesia',
                            'expire' => time() + 86500,
                        );
                        set_cookie($cookie);
                        include($_SERVER['DOCUMENT_ROOT'].'/config/ind_constants.php');
                    }
                }
                elseif (get_cookie('language') === 'english') {
                    include($_SERVER['DOCUMENT_ROOT'].'/config/eng_constants.php');
                } else {
                    include($_SERVER['DOCUMENT_ROOT'].'/config/ind_constants.php');
                }
                ?>
<div class="glorious-header" data-section="header">
    <div class="wrapper">
        <div class="row header-row">
            <div class="header-logo">              
             <?php if (isset($this->session->userdata['userId'])) { ?>
              <?php if ($this->session->userdata('user_type') == 'professional') { ?>
                  <a href="<?= $this->config->config['base_url'] ?>profile/requests">
              <?php }else if ($this->session->userdata('user_type') == 'user'){ ?>
                  <a href="<?= $this->config->config['base_url'] ?>profile/dashboard">  
              <?php }else{ ?>
                  <a href="<?= $this->config->config['base_url'] ?>">     
              <?php }}else{ ?>
                  <a href="<?= $this->config->config['base_url'] ?>">     
              <?php } ?> 
              <img src="<?= $this->config->config['base_url'] ?>images/seekmilogo.png" alt="Seekmi"></a>
            </div>
            <div class="header-middle-container"></div>
            <div class="header-navigation ">
             <?php if (isset($this->session->userdata['userId'])) { ?>
                <a href="<?=$this->config->config['base_url']?>user/logout" rel="nofollow" class="gray-link"><?php echo HEADER_LOGOUT; ?></a>
                <?php if($this->session->userdata('user_type')=='professional'){ ?>
                <a href="<?=$this->config->config['base_url']?>profile/requests" class="bttn gray">Dashboard</a>
                <?php }if($this->session->userdata('user_type')=='user'){ ?>
                <a href="<?=$this->config->config['base_url']?>profile/dashboard" class="bttn gray">Dashboard</a>
                <?php } ?>
             <?php }else{ ?>            
                <a href="<?= $this->config->config['base_url'] ?>user/register" rel="nofollow" class="gray-link"><?php echo MENU_SIGNUP; ?></a>
                <a href="<?= $this->config->config['base_url'] ?>user/login" rel="nofollow" class="gray-link log-in-link"><?php echo HEADER_SIGNIN; ?></a>
             <?php } ?>   
            <?php /*    
                <select id="language" name="language" style="  float: right;">
                    <option value="english" <?php if(get_cookie('language') == 'english'){ ?>selected="selected" <?php }?>><img src="<?= $this->config->config['base_url'] ?>images/en.png" alt="Seekmi">EN</option>
                    <option value="indonesia" <?php if(get_cookie('language') == 'indonesia' || get_cookie('language') == ''){ ?>selected="selected" <?php }?>>ID</option>
                </select>
               */ ?> 
                <div class="menu-top-menu-container">
                    <ul id="primary-menu" class="nav-menu">
                      <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                         <?php if(get_cookie('language') == 'english'){ ?>
                            <a title="EN" href="javascript:;"><img src="<?= $this->config->config['main_base_url'] ?>app/wp-content/plugins/qtranslate-x/flags/gb.png" alt="EN">&nbsp;EN&nbsp;&nbsp;
                         <?php }else{ ?>                            
                            <a title="ID" href="javascript:;"><img src="<?= $this->config->config['main_base_url'] ?>app/wp-content/plugins/qtranslate-x/flags/id.png" alt="ID">&nbsp;ID&nbsp;&nbsp;
                         <?php } ?>
                             <img width="7" height="6" alt="" src="<?= $this->config->config['main_base_url'] ?>app/wp-content/themes/twentythirteen/images/droparrow.png"></a>
                            <ul class="sub-menu" id="language">
                               <li class="qtranxs-lang-menu-item qtranxs-lang-menu-item-en menu-item menu-item-type-custom menu-item-object-custom">
                                   <a title="EN" <?php if(get_cookie('language') == 'indonesia'){ ?>href="javascript:;"<?php } ?> lang="english"><img src="<?= $this->config->config['main_base_url'] ?>app/wp-content/plugins/qtranslate-x/flags/gb.png" alt="EN">&nbsp;EN&nbsp;&nbsp;</a>
                               </li>
                               <li class="qtranxs-lang-menu-item qtranxs-lang-menu-item-id menu-item menu-item-type-custom menu-item-object-custom menu-last">
                                   <a title="ID" <?php if(get_cookie('language') == 'english'){ ?>href="javascript:;"<?php } ?> lang="indonesia"><img src="<?= $this->config->config['main_base_url'] ?>app/wp-content/plugins/qtranslate-x/flags/id.png" alt="ID">&nbsp;ID&nbsp;&nbsp;&nbsp;</a>
                               </li>
                            </ul>
                        </li>
                    </ul>
                </div>       
     <?php
//                if ($this->session->userdata('language') === 'ind') {
//                    include(APPPATH . 'config/ind_constants.php');
//                } else {
//                    include(APPPATH . 'config/eng_constants.php');
//                }
                ?>
<!--                <select id="language" name="language">
                    <option value="english" >English</option>
                    <option value="indonesia" >Indonesia</option>
                </select>
                <div id="country-select">
                    <select style="display: none;" id="country-options" name="country-options">
                        <option title="#" value="ind">Ind</option>
                        <option title="#"  value="uk">Eng</option>
                    </select>
                </div>-->
<!--                <div class="flagInd">
                    <div class="pull-left"><img src="<?= $this->config->config['base_url'] ?>images/flagn.png" alt="Seekmi"></div>
                    <div class="pull-left">ID</div>
                </div>-->
            </div>
        </div></div></div>