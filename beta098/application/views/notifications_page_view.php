<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

<div class="settings-navigation ">
<div class="wrapper">
<a class="back" href="<?=$this->config->config['base_url']?>profile/settings">Settings</a>
<h2>Notifications</h2>
</div>
</div>
        
<div class="wrapper content notifications settings">
                
<form accept-charset="ISO-8859-1" action="#" method="post" class="good-form small" name="notification-settings" id="notification-settings">
        <div class="dynamic-row notification-error">
            <div class="column-12">
                <div style="display: none;" class="alert-closable">
                    <input type="checkbox" class="alert-close">
                    <div data-error-target="" class="alert-error">
                    </div>
                </div>
            </div>
        </div>
        <div class="dynamic-row">
            <div class="column-6">
                 <div data-account-notifications="">
                    <h2>Seekmi Announcements</h2>
                    <h3>Email</h3>
                    <ul class="account">
                    <li>              
                        <label class="switch-label">Updates</label>
                        <div class="switch">
                            <input type="checkbox" value="1" id="updates" class="user_notification" <?php if($notification_data->updates=='Y'){ ?>checked="checked"<?php } ?> name="user_updates">
                           <label class="switch-trigger" for="updates"></label>
                        </div>
                    </li>
                    <li>
                        <label class="switch-label">Profile tips</label>
                        <div class="switch">
                            <input type="checkbox" value="1" id="tips" class="user_notification" <?php if($notification_data->tips=='Y'){ ?>checked="checked"<?php } ?> name="user_tips">
                            <label class="switch-trigger" for="tips"></label>
                        </div>
                    </li>
                    <li>
                        <label class="switch-label">Special offers</label>
                        <div class="switch">
                            <input type="checkbox" value="1" id="offers" class="user_notification" <?php if($notification_data->offers=='Y'){ ?>checked="checked"<?php } ?> name="user_offers">
                            <label class="switch-trigger" for="offers"></label>
                        </div>
                    </li>
                    <li>
                        <label class="switch-label">Surveys</label>
                        <div class="switch">
                            <input type="checkbox" value="1" id="userFeedback" class="user_notification" <?php if($notification_data->userFeedback=='Y'){ ?>checked="checked"<?php } ?> name="user_feedback">
                            <label class="switch-trigger" for="userFeedback"></label>
                        </div>
                    </li>
                    </ul>
                </div>
                <div class="mobile-gap"></div>
            </div>
        </div>
</form>
</div>
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('.user_notification').click(function() {
        var val=''
        if(this.checked==true){
            val='Y';
        }else{
            val='N';
        }
        $.post(Host+'profile/update_notification', { type: this.id, val: val }, function(data) {            
        });  
    });
});
</script>
</body>
</html>

