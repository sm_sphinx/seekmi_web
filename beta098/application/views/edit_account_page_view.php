<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<?php include('header_view.php');
 function getTimeForTimezone($zone){
    $date = date('Y-m-d H:i:s');
    $usersTimezone = new DateTimeZone($zone);
    $l10nDate = new DateTime($date);
    $l10nDate->setTimeZone($usersTimezone);
    $dt=date('h:ia',strtotime($l10nDate->format('Y-m-d H:i:s')));
    return $dt;
 }

?>

<div class="wrapper content">
    <a class="bttn tiny" href="<?=$this->config->config['base_url']?>profile/settings">
        <span aria-hidden="true" class="icon-font icon-font-left-dir"></span>
        <?php echo BUTTON_BACKTOSETTINGS;?>
    </a>    
    <div class="dynamic-row settings-second-level">
        <div class="column-7">
             <form accept-charset="ISO-8859-1" class="acct-settings good-form large" name="acct-settings" id="acct-settings" method="post" action="<?=$this->config->config['base_url']?>profile/update_profile" novalidate>
                <div class="pod pod-primary form-icons">
                    <div class="pod-header">
                        <h2><?php echo EDITACC_EDIT_TEXT;?></h2>
                    </div>
                    <div class="pod-content">
                        <fieldset>
                              <legend class="icon-font-user"><?php echo EDITACC_NAME_TEXT;?></legend>
                              <div class="form-field form-field-column-6">
                                  <label class="" for="usr_first_name"><?php echo REGISTER_FNAME;?></label>
                                  <input type="text" class="" id="usr_first_name" name="usr_first_name" value="<?=$user_data->firstname?>">
                              </div>
                              <div class="form-field form-field-column-6">
                                  <label class="" for="usr_last_name"><?php echo REGISTER_LNAME;?></label>
                                  <input type="text" class="" id="usr_last_name" name="usr_last_name" value="<?=$user_data->lastname?>">
                              </div>
                              <div class="form-field">
                                  <label class="" for="usr_email"><?php echo REGISTER_EMAILADD;?></label>
                                  <input type="text" class="" id="usr_email" name="usr_email" value="<?=$user_data->email?>">
                              </div>
                        </fieldset>
                        <fieldset>
                            <legend class="icon-font-clock">Timezone</legend>
                              <div class="form-field">
                                  <label class="" for="usr_timezone"><?php echo EDITACC_TIMEZONE_TEXT;?></label>                                
                                    <select class="" id="usr_timezone" name="usr_timezone">
                                        <option <?php if($user_data->timezone=="America/New_York"){ ?>selected="selected" <?php } ?>value="America/New_York">Eastern  (currently <?=getTimeForTimezone('America/New_York')?>)</option>
                                        <option <?php if($user_data->timezone=="America/Chicago"){ ?>selected="selected" <?php } ?>value="America/Chicago">Central  (currently <?=getTimeForTimezone('America/Chicago')?>)</option>
                                        <option <?php if($user_data->timezone=="America/Denver"){ ?>selected="selected" <?php } ?>value="America/Denver">Mountain  (currently <?=getTimeForTimezone('America/Denver')?>)</option>
                                        <option <?php if($user_data->timezone=="America/Phoenix"){ ?>selected="selected" <?php } ?>value="America/Phoenix">Mountain  - Arizona (currently <?=getTimeForTimezone('America/Phoenix')?>)</option>
                                        <option <?php if($user_data->timezone=="America/Los_Angeles"){ ?>selected="selected" <?php } ?>value="America/Los_Angeles">Pacific  (currently <?=getTimeForTimezone('America/Los_Angeles')?>)</option>
                                        <option <?php if($user_data->timezone=="America/St_Kitts"){ ?>selected="selected" <?php } ?>value="America/St_Kitts">Atlantic  (currently <?=getTimeForTimezone('America/St_Kitts')?>)</option>
                                        <option <?php if($user_data->timezone=="America/Juneau"){ ?>selected="selected" <?php } ?>value="America/Juneau">Alaska  (currently <?=getTimeForTimezone('America/Juneau')?>)</option>
                                        <option <?php if($user_data->timezone=="Pacific/Honolulu"){ ?>selected="selected" <?php } ?>value="Pacific/Honolulu">Hawaii  (currently <?=getTimeForTimezone('Pacific/Honolulu')?>)</option>
                                        <option <?php if($user_data->timezone=="Pacific/Guam"){ ?>selected="selected" <?php } ?>value="Pacific/Guam">Chamorro  (currently <?=getTimeForTimezone('Pacific/Guam')?>)</option>
                                    </select>
                              </div>
                        </fieldset>                        
                        <fieldset>
                            <div class="form-field">
                              <a class="bttn blue medium" onclick="(function(btn) { if ($(btn).hasClass('disabled')) { return false; } var frm = $(btn).closest('form'); if(frm.length == 1){  frm.submit();}})(this); return false;" href="#submit"><span><?php echo BUTTON_SAVECHANGES_TEXT;?></span></a>
                              <input type="submit" value="" name="__unused__submit__" style="visibility:collapse;width:1px;height:1px;display:block;float:none;padding:0;margin-left:-9999px;position:absolute;">
                                <a class="form-link" href="<?=$this->config->config['base_url']?>profile/settings"><?php echo BUTTON_CANCEL_TEXT;?></a>
                                <a class="form-link form-void" href="#"><?php echo BUTTON_DELETE_ACC;?></a>
                            </div>
                        </fieldset>
                    </div>
                </div>
             </form>
        </div>
    </div>
</div>
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#acct-settings").validate({
        rules: {
            usr_first_name: {
                required: true,
                minlength: 2
            },
            usr_last_name: {
                required: true,
                minlength: 2
            },
            usr_email: {
                required: true,
                email: true,
                remote: "<?=$this->config->config['base_url']?>profile/check_login_email_exists"
            }
        },
        messages: {
            usr_first_name: {
                required: "<?php echo VAL_REQFNAME;?>",
                minlength: "<?php echo VAL_MINLENGTH;?>"
            },
            usr_last_name: {
                required: "<?php echo VAL_REQFNAME;?>",
                minlength: "<?php echo VAL_MINLENGTH;?>"
            },
            usr_email: {
                required: "<?php echo VAL_REQEMAIL;?>",
                email: "<?php echo VAL_EMAIL;?>",
                remote: "<?php echo VAL_EMAIL_EXISTS;?>"
            }      
        }
    });    
});
</script>
</body>
</html>

