<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/message.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/work.css">

</head>
<body class="write-review box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

<<div class="wrapper">
    <div class="message">
        <h1>Thanks for reviewing Ross!</h1>
        <div class="lines"><span></span></div>
        <p>
            Your review has been submitted.
        </p>
    </div>
    <div class="gallery">
        <div class="row-one">
            <div class="column-one">
                <div class="inner-div margin-right"></div>
            </div>
            <div class="column-two">
                <div class="inner-div"></div>
            </div>
            <div class="column-three">
                <div class="inner-div margin-left"></div>
            </div>
        </div>
        <div class="row-two">
            <div class="column-one">
                <div class="inner-div margin-right"></div>
            </div>
            <div class="column-two">
                <div class="baby-row-one">
                    <div class="baby-column-one">
                        <div class="inner-div margin-right"></div>
                    </div>
                    <div class="baby-column-two">
                        <div class="inner-div">
                            <h3>Get introduced to the right professionals for your project.</h3>
                            <a href="<?=$this->config->config['base_url']?>" class="bttn blue">Start</a>
                        </div>
                    </div>
                </div>
                <div class="baby-row-two"><div class="inner-div"></div></div>
            </div>
            <div class="column-three">
                <div class="inner-div margin-left"></div>
            </div>
        </div>
    </div>
</div>                                       

<? include('footer_mini_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
</body>
</html>

