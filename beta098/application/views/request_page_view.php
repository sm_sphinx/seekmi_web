<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/manage.css">
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<?php include('header_view.php'); ?>
<div class="wrapper content">
    <h1 class="body-text">
       <?php echo PROFILEREQ_CUSTREQ;?>
    </h1>    
    <?php if(!empty($request_list)){ ?>
    <div class="work-inbox">
        <ul>
        <?php foreach($request_list as $request_row){ ?>           
         <li class="linkable">
            <a href="<?=$this->config->config['base_url']?>profile/view_request/<?=$request_row->requestId?>">  
           <?php if($request_row->readFlag=='Y'){ ?>
           <div class="work-type lead-old-icon">
               <p><?php echo PROFILEREQ_VIEWED;?></p>
            </div>
           <?php }else{ ?>
            <div class="work-type lead-new-icon">
                <p><?php echo BUTTON_NEW;?></p>
            </div>
           <?php } ?>
            <h3><?=$request_row->firstname?> 
            <?php if($request_row->lastname!=''){ echo $request_row->lastname[0]; } ?>.</h3>
            <p class="details">
            <?php if($request_row->cityName!=''){
               echo $request_row->cityName;
            }if($request_row->provName!=''){
               echo ",".$request_row->provName."&ndash;";
            } 
            echo $request_row->subserviceName;
            ?>
<!--            &ndash; Needed within a few weeks-->
            </p>
            </a>
<!--            <p class="message-preview">Mid career, Writing resume, Manufacturing. </p>-->           
            <abbr class="timeago"><?php echo get_timeago(strtotime($request_row->addedOn)); ?></abbr>
            <div class="action-decline">
            <?php if($request_row->status=='Pending'){ ?>
                <span class="bttn nano gray" onclick="changeStatus('accept','<?=$request_row->requestId?>');">
                    <?php echo BUTTON_ACCEPT;?>
                </span>   
                    &nbsp;
                <span class="bttn nano gray" onclick="changeStatus('decline','<?=$request_row->requestId?>');">
                   <?php echo BUTTON_DECLINE;?>
                </span>
            <?php } if($request_row->status=='Accepted'){ ?>
                <span class="bttn nano gray" onclick="changeStatus('decline','<?=$request_row->requestId?>');">
                    <?php echo BUTTON_DECLINE;?>
                </span>   
            <?php } if($request_row->status=='Hired'){ ?>
                <span class="bttn nano gray">
                    <?php echo BUTTON_HIRED;?>
                </span>                      
            <?php } ?>
           </div>
         </li>
         
        <?php } ?>
         </ul>
         <div class="paginator"><h4>1 <?php echo LABEL_PAGE;?> <span>2 <?php echo LABEL_RECORDS;?></span></h4><div class="pages"></div></div>
        </div>
    <?php }else{ ?>
    <p class="body-text">
        <?php echo PROFILEREQ_NOTTHE_TEXT;?>
<!--        <a href="#">Change your category preferences.</a>-->
    </p>
    <?php } ?>
    </div>
<? include('footer_view.php'); ?>

<div class="support-options curtain first-loggedin-popup" style="display: none;">
    <div class="box box-modal box-secondary-emphasized curtain-inner">        

        <div class="box-content">
            <p class="body-text">
              <?php if($user_data->isNewLogin=='N'){ ?>
                <?php echo REQUEST_FIRST_LOGGEDIN_STARTUP_TEXT; ?>
              <?php }else{ ?>
                <?php echo REQUEST_MORE_LOGGEDIN_STARTUP_TEXT; ?>
              <?php } ?>
            </p>
            <p style="text-align: center;"><button class="bttn blue close-loggedin-popup" type="button">Close</button></p>
        </div>
    </div>
</div>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript">
    <?php if($user_data->isNewLogin=='N'){ ?>
     $(document).ready(function () {  
           $(".first-loggedin-popup").show();
           
           $(".close-loggedin-popup").click(function(){
                $.post(Host+'profile/change_first_loggedin_status', { uid:'<?=$user_data->userId?>' }, function(data) {
                    if(data=='success'){
                     location.href='<?=$this->config->config['base_url']?>profile/services/edit';
                    }
                 });
               
           });
     });
    <?php }else{ if($totalProgress < 60){ ?>
          $(document).ready(function () {  
             $(".first-loggedin-popup").show();
             $(".close-loggedin-popup").click(function(){                
               location.href='<?=$this->config->config['base_url']?>profile/services/edit';
              });
           }); 
    <?php }} ?>
function changeStatus(sts,id){
    if(sts=='decline'){
        if(confirm('Are you sure to decline the request?'))
        {
            $.post('<?=$this->config->config['base_url']?>profile/change_request_status', { sts:sts,qid:id }, function(data) {
                location.reload();
            });
        }
    }
    if(sts=='accept'){
        //if(confirm('Are you sure to accept the request?'))
        //{
          location.href='<?=$this->config->config['base_url']?>profile/add_quote/'+id;
        //}
    }
}
</script>
</body>
</html>

