<!doctype html>
<html lang="en" class="no-js">
<!-- Mirrored from nunforest.com/site/element-demo/boxed/pricing.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Jan 2015 06:53:45 GMT -->
<head>
    <title><?=$page_title?> | Montego</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/bootstrap.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/magnific-popup.css" media="screen">	
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/animate.css" media="screen">
<!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/style.css" media="screen">
    
</head>
<body id="top">
<? include('header_view.php'); ?>
<!-- ####################################################################################################### -->
<div id="content">
<!-- page-banner-section -->
<div class="section-content page-banner-section2">
        <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Partner Program</h1>
                    </div>
                    <div class="col-sm-6">
                        <ul>
                            <li><a href="<?=$this->config->config['base_url']?>">Home</a></li>
                            <li>Partner Program</li>
                        </ul>
                    </div>
                </div>
        </div>
</div>

<!-- services-section -->
<div class="section-content services-section">

        <div class="services-box1">
                <div class="container">
                        <div class="row">

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-css3"></i></span>
                                                <div class="services-content">
                                                        <h2>Font Awesome Integrated</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque dictum aliquam ornare. Sed elit lectus, ullamcorper rutrum felisda.</p>
                                                </div>
                                        </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-laptop"></i></span>
                                                <div class="services-content">
                                                        <h2>Responsive &amp; Retina Ready</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque dictum aliquam ornare. Sed elit lectus, ullamcorper rutrum felisda.</p>
                                                </div>
                                        </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-rocket"></i></span>
                                                <div class="services-content">
                                                        <h2>Powerful Admin &amp; Easy SetUp</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque dictum aliquam ornare. Sed elit lectus, ullamcorper rutrum felisda.</p>
                                                </div>
                                        </div>
                                </div>

                        </div>
                </div>
        </div>

        <div class="services-box1">
                <div class="container">
                        <div class="row">

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-cogs"></i></span>
                                                <div class="services-content">
                                                        <h2>Fully Customizable</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque dictum aliquam ornare. Sed elit lectus, ullamcorper rutrum felisda.</p>
                                                </div>
                                        </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-star-o"></i></span>
                                                <div class="services-content">
                                                        <h2>All Shortcodes that you Need</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque dictum aliquam ornare. Sed elit lectus, ullamcorper rutrum felisda.</p>
                                                </div>
                                        </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="services-post">
                                                <span><i class="fa fa-heart-o"></i></span>
                                                <div class="services-content">
                                                        <h2>Free Updates &amp; Support</h2>
                                                        <p>Aenean sed justo tincidunt, vulputate nisi sit amet, rutrum ligula. Pellentesque dictum aliquam ornare. Sed elit lectus, ullamcorper rutrum felisda.</p>
                                                </div>
                                        </div>
                                </div>

                        </div>
                </div>
        </div>

</div>

<!-- banner-text2-section -->
<div class="section-content banner-text2-section">
        <div class="container">
                <h1>Our professional team is ready to start work <a href="#">Get in touch</a></h1>					
        </div>
</div>

<!-- steps-section -->
<div class="section-content steps-section">
        <div class="title-section">
                <div class="container triggerAnimation animated" data-animate="bounceIn">
                        <h1>Steps to Success</h1>
                </div>
        </div>
        <div class="steps-box triggerAnimation animated" data-animate="bounceIn">
                <div class="container">
                        <div class="step">
                                <span>Idea</span>
                        </div>
                        <div class="step">
                                <span>Cooperation</span>
                        </div>
                        <div class="step">
                                <span>Design</span>
                        </div>
                        <div class="step">
                                <span>Develop</span>
                        </div>
                </div>
        </div>
</div>

<!-- clients-section -->
<div class="section-content clients-section">
        <div class="client-box">
                <div class="container triggerAnimation animated" data-animate="fadeIn">
                        <ul>
                                <li><a href="#"><img src="<?=$this->config->config['base_url']?>images/client.png" alt=""></a></li>
                                <li><a href="#"><img src="<?=$this->config->config['base_url']?>images/client.png" alt=""></a></li>
                                <li><a href="#"><img src="<?=$this->config->config['base_url']?>images/client.png" alt=""></a></li>
                                <li><a href="#"><img src="<?=$this->config->config['base_url']?>images/client.png" alt=""></a></li>
                                <li><a href="#"><img src="<?=$this->config->config['base_url']?>images/client.png" alt=""></a></li>
                                <li><a href="#"><img src="<?=$this->config->config['base_url']?>images/client.png" alt=""></a></li>
                        </ul>
                </div>
        </div>

</div>

</div>
<!-- End content -->
<!-- End content -->
<!-- ####################################################################################################### -->
<? include('footer_view.php'); ?>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.migrate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/waypoint.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/plugins-scroll.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/script.js"></script>  	
</body>
</html>
