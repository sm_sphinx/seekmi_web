<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/manage.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/work.css">
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

<div class="inbox-tabs">
    <div class="wrapper">
       <ul>
         <li class="in_progress <?php if($page_title=='In Progress'){ ?>active<?php } ?>">
             <a href="<?=$this->config->config['base_url']?>profile/work"><?php echo PROFILEREQ_INRECORDS;?></a>
         </li>
         <li class="hired">
             <a href="<?=$this->config->config['base_url']?>profile/work/hired" <?php if($page_title=='Hired'){ ?>active<?php } ?>><?php echo BUTTON_HIRED;?></a>
         </li>
         <li class="archived">
             <a href="<?=$this->config->config['base_url']?>profile/work/archive" <?php if($page_title=='Archived'){ ?>active<?php } ?>><?php echo BUTTON_ARCHIVED;?></a>
         </li>
        </ul>
    </div>
</div>

<div class="wrapper content">
    <div class="inbox-options">
        <h1><?=$page_title?></h1>
    </div>    
    <?php if(!empty($request_list)){ ?>
    <div class="work-inbox">
        <ul>
        <?php foreach($request_list as $request_row){ ?>           
         <li class="linkable">
            <a href="<?=$this->config->config['base_url']?>profile/view_request/<?=$request_row->requestId?>">  
           <?php if($request_row->readFlag=='Y'){ ?>
           <div class="work-type lead-old-icon">
               <p><?php echo PROFILEREQ_VIEWED;?></p>
            </div>
           <?php }else{ ?>
            <div class="work-type lead-new-icon">
                <p><?php echo BUTTON_NEW;?></p>
            </div>
           <?php } ?>
            <h3><?=$request_row->firstname?> 
            <?php if($request_row->lastname!=''){ echo $request_row->lastname[0]; } ?>.</h3>
            <p class="details">
            <?php if($request_row->cityName!=''){
            echo $request_row->cityName;
            }if($request_row->provName!=''){
               echo ",".$request_row->provName."&ndash;";
            } 
            echo $request_row->subserviceName;
            ?>
<!--            &ndash; Needed within a few weeks-->
            </p>
            </a>
<!--            <p class="message-preview">Mid career, Writing resume, Manufacturing. </p>-->           
            <abbr class="timeago"><?php echo get_timeago(strtotime($request_row->modifiedOn)); ?></abbr>
            <div class="action-decline">
            <?php if($request_row->status=='Pending'){ ?>
                <span class="bttn nano gray" onclick="changeStatus('accept','<?=$request_row->requestId?>','<?=$request_row->projectId?>');">
                    <?php echo BUTTON_ACCEPT;?>
                </span>   
                    &nbsp;
                <span class="bttn nano gray" onclick="changeStatus('decline','<?=$request_row->requestId?>','<?=$request_row->projectId?>');">
                    <?php echo BUTTON_DECLINE;?>
                </span>
            <?php } if($request_row->status=='Accepted'){ ?>
                <span class="bttn nano gray" onclick="changeStatus('decline','<?=$request_row->requestId?>','<?=$request_row->projectId?>');">
                    <?php echo BUTTON_DECLINE;?>
                </span>   
            <?php } if($request_row->status=='Hired'){ ?>
                <span class="gray">
                    <b><?php echo BUTTON_HIRED;?></b>
                </span> 
                &nbsp;
                <span class="bttn nano gray" onclick="changeStatus('complete','<?=$request_row->requestId?>','<?=$request_row->projectId?>');">
                    <?php echo DASH_COMPLETE;?>
                </span>
            <?php } ?>
            <?php if($request_row->status=='Declined'){ ?>
                <span class="gray">
                    <b><?php echo LABEL_DECLINED;?></b>
                </span>                      
            <?php } ?>
            <?php if($request_row->status=='Completed'){ ?>
                <span class="gray">
                    <b><?php echo LABEL_COMPLETED;?></b>
                </span>                      
            <?php } ?>
           </div>
         </li>
         
        <?php } ?>
         </ul>
         <div class="paginator"><h4>1 <?php echo LABEL_PAGE;?> <span>2 <?php echo LABEL_RECORDS;?></span></h4><div class="pages"></div></div>
        </div>
    <?php }else{ ?>
       <div class="work-inbox">
             <p class="empty-state">
                <span class="icon-font icon-font-tag" aria-hidden="true"></span>
                <br>
                <?=$empty_data_message?>
             </p>
        </div>
    <?php } ?>
    </div>
<?php include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript">
function changeStatus(sts,id,pid){
    
    if(sts=='decline'){
        if(confirm('Are you sure to decline the request?'))
        {
            $.post('<?=$this->config->config['base_url']?>profile/change_request_status', { sts:sts,qid:id,pid:pid }, function(data) {
                location.reload();
            }); 
        }
    }
    
    if(sts=='complete'){
        if(confirm('Are you sure to complete the project?'))
        {
            $.post('<?=$this->config->config['base_url']?>profile/change_request_status', { sts:sts,qid:id,pid:pid }, function(data) {
                location.reload();
            });
        }
    }
    
    if(sts=='accept')
    {
        if(confirm('Are you sure to accept the request?'))
        {
            location.href='<?=$this->config->config['base_url']?>profile/add_quote/'+id;
        }
    }
        
}
</script>
</body>
</html>

