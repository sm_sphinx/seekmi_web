<?php

class Pros_model extends CI_Model
{
    
	function __construct()
	{
	    parent:: __construct();
	}
	
	function insertUser($data)
	{
            $table='tbluser';
            $this->db->insert($table, $data); 
	}
        
        function insertProvider($data)
	{
            $table='tbluserprovider';
            $this->db->insert($table, $data); 
	}
	
	function updateUser($data,$id)
	{
            $table='tbluser';
            $this->db->update($table, $data, array('userId'=> $id));		
	}
	
	function getUserInfo($uid)
	{
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.userId', $uid); 
            $query = $this->db->get();		
            return $query->row();
	}
	
	function getAllPros($start=1, $per_page=10,$keyword)
	{            
            $this->db->select();
            $this->db->from('tbluser');	
            $this->db->where('userType','professional');
            if($keyword!=''){
               $where = " (firstname LIKE '%" . $keyword . "%' OR "
                        . "lastname LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            }   
            $this->db->where('services','');
            $this->db->where('status != ','deleted');
            $this->db->order_by('userId', 'DESC');
            $this->db->limit($start, $per_page);
            $query = $this->db->get();	
            //echo $this->db->last_query();
            return $query->result();
	}
	
	function getAllProsCount($keyword)
	{           
            $this->db->select();
            $this->db->from('tbluser');	
            $this->db->where('userType','professional');	
            if($keyword!=''){
               $where = " (firstname LIKE '%" . $keyword . "%' OR "
                        . "lastname LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            }    
            $this->db->where('services','');
            $this->db->where('status != ','deleted');
            $query = $this->db->get();		
            return $query->num_rows();
	}
        
        function getAllNewPros($start=1, $per_page=10,$keyword)
	{            
            $this->db->select();
            $this->db->from('tbluser');	
            $this->db->where('userType','professional');
            if($keyword!=''){
               $where = " (firstname LIKE '%" . $keyword . "%' OR "
                        . "lastname LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            }   
           // $this->db->where('services !=','');
            $this->db->where('status != ','deleted');
            $this->db->order_by('userId', 'DESC');
            $this->db->limit($start, $per_page);
            $query = $this->db->get();	
            //echo $this->db->last_query();
            return $query->result();
	}
	
	function getAllNewProsCount($keyword)
	{           
            $this->db->select();
            $this->db->from('tbluser');	
            $this->db->where('userType','professional');	
            if($keyword!=''){
               $where = " (firstname LIKE '%" . $keyword . "%' OR "
                        . "lastname LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            }    
            $this->db->where('services !=','');
            $this->db->where('status != ','deleted');
            $query = $this->db->get();		
            return $query->num_rows();
	}
	
	function getAllUserEmail()
	{
            $this->db->select('email,userId');
            $this->db->from('tbluser');	
            $this->db->where('user_type','customer');	
            $this->db->where('isActive','Y');            
            $this->db->order_by('email','ASC');	
            $query = $this->db->get();		
            return $query->result();
	}
	
	function check_user_login($adata)
	{	   
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.email', $adata['useremail']); 
            $this->db->where('tbluser.password', md5($adata['userpass'])); 
            $this->db->where('tbluser.user_type !=', 'customer'); 
            $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {
                $r=$query->row();		 	
                return $r;
	    }
	    else
	    {
		return false;		 
	    }
	}
        
        function check_customer_login($adata)
	{	   
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.email', $adata['useremail']); 
            $this->db->where('tbluser.password', md5($adata['userpass'])); 
            //$this->db->where('tbluser.user_type !=', 'admin'); 
            $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {
                $r=$query->row();		 	
                return $r;
	    }
	    else
	    {
		return false;		 
	    }
	}
        
        function checkEmailExists($useremail)
	{		
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.email', $useremail); 
            $this->db->where('tbluser.status != ', 'deleted'); 
            $query = $this->db->get();		
            if($query->num_rows()==0)
            {        	
                return true;					
            }
            else
            {
                return false;
            }		
	}
        
        function change_password($p,$u)
	{
	    $data = array('password' => md5($p));
            $this->db->where('userId',$u);
            $this->db->update('tbluser', $data);
            return true;
	}

	function encrypt($str,$key)
	{	
	   if($str!=''){
                $iv = '372adfc77ec8913b';
		$td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
                mcrypt_generic_init($td, $key, $iv);		
                $encrypted = mcrypt_generic($td, $str);
                mcrypt_generic_deinit($td);
                mcrypt_module_close($td);
                return bin2hex($encrypted);
            }else
            {
             return '';
            }
        }
	
	function decrypt($code,$key) 
	{
            $code = $this->hex2bin($code);
            $iv = '372adfc77ec8913b';
            $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
            mcrypt_generic_init($td, $key, $iv);
            $decrypted = mdecrypt_generic($td, $code);
            mcrypt_generic_deinit($td);
            mcrypt_module_close($td);
            return utf8_encode(trim($decrypted));
        }
	
	function hex2bin($hexdata)
	{
	    $bindata = '';
            for ($i = 0; $i < strlen($hexdata); $i += 2) {
             $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
            }
            return $bindata;
        }

	function getUserIdByEmail($useremail)
	{
            $query = $this->db->get_where('tbluser', array('email' => $useremail));
            return $query->row();				
	}
			
	function updatePassword($pwd,$uid)
	{
            $this->db->query('update tbluser set password="'.$pwd.'" where userId="'.$uid.'"');	
	}
		
	
	function checkUserToken($token)
	{
            $this->db->select();
            $this->db->from('tbmstusers');		
            $this->db->where('tbmstusers.resetPwdToken', $token); 
            $query = $this->db->get();		
            if($query->num_rows()==0)
            {	
                    return false;	
            }
            else
            {
                    return true;							
            }		
	}
	function getAdminInfo($uid)
	{
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.userId', $uid); 
            $query = $this->db->get();		
            return $query->row();
	}
	
	function updateAdmin($data,$id)
	{
            $table='tbluser';
            $this->db->update($table, $data, array('userId'=> $id));
	}
	
	function check_old_password($id,$old_pwd)
	{
	    $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.userId', $id); 		 
            $this->db->where('tbluser.password', md5($old_pwd)); 			
            $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {		 		
		return true;
	    }
	    else
	    {
		return false;		 
	    }
	}
        
        function checkValidConfirmationToken($token)
        {
            $this->db->select('userId,status');
            $this->db->from('tbluser');		
            $this->db->where('tbluser.confirmationToken', $token);
            $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {
                $r=$query->row();		 	
                return $r;
	    }
	    else
	    {
		return false;		 
	    }
        }
        
        function getExportAllPros($keyword)
	{            
            $this->db->select("firstname,lastname,email,phone,(CASE WHEN status = 'Y' THEN 'Active' WHEN status = 'N' THEN 'Inactive' ELSE CONCAT(UCASE(SUBSTRING(status, 1, 1)),SUBSTRING(status, 2)) END) status",false);
            $this->db->from('tbluser');	
            $this->db->where('userType','professional');
            if($keyword!=''){
               $where = " (firstname LIKE '%" . $keyword . "%' OR "
                        . "lastname LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            }            
            $this->db->order_by('userId', 'DESC');           
            return $query = $this->db->get();	
           // echo $this->db->last_query();           
            //return $query->result();
	}
               
        
        function getReferredByUser($code){
            $this->db->select('userId');
            $this->db->from('tbluser');		
            $this->db->where('tbluser.status', 'Y'); 		 
            $this->db->where('tbluser.referralCode', $code); 			
            $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {		 		
		return $query->row();
	    }
	    else
	    {
		return false;		 
	    }
        }
        
        function checkProsEmailExists($useremail)
	{		
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.email', $useremail);
            $query = $this->db->get();		
            if($query->num_rows() > 0)
            {        	
                return $query->row();			
            }
            else
            {
                return false;
            }		
	}
        
        function checkAlreadyReferred($referred_id,$uid)
	{		
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.referredBy', $referred_id);
            $this->db->where('tbluser.userId', $uid);
            $query = $this->db->get();		
            if($query->num_rows() > 0)
            {        	
                return true;			
            }
            else
            {
                return false;
            }		
	}
        
        function checkReferralEmailOrCode($refer_email_code)
        {
            $query=$this->db->query('select * from tbluser where status IN ("Y","review") and (email="'.$refer_email_code.'" or referralCode="'.$refer_email_code.'")');            
            if($query->num_rows() > 0)
            {        	
                return $query->row();			
            }
            else
            {
                return false;
            }	
        }
        
        function checkUserProviderInfoExit($uid)
        {
            $this->db->select();
            $this->db->from('tbluserprovider');	
            $this->db->where('userId', $uid);
            $query = $this->db->get();		
            if($query->num_rows() > 0)
            {        	
                return true;			
            }
            else
            {
                return false;
            }	
        }
        
        function getAllProsUsersNotService()
        {
            $query=$this->db->query("SELECT a.userId, c.serviceId, b.services FROM tbluserprovider AS a JOIN tbluser AS b ON ( a.userId = b.userId ) LEFT JOIN tbluserservices AS c ON ( a.userId = c.userId )
WHERE b.status = 'Y' GROUP BY a.userId");  
            if($query->num_rows() > 0){
                return $query->result();
            }else{
                return false;
            }
        }
                
}
?>