<?php
class App_model extends CI_Model
{    
    function __construct()
    {
        parent:: __construct();
    }

    function insertApp($data)
    {
        $table='tblapp';
        $this->db->insert($table, $data); 
    }
    
    function insertData($table,$data)
    {        
        $this->db->insert($table, $data); 
    }

    function updateApp($data,$id)
    {
        $table='tblapp';
        $this->db->update($table, $data, array('appId'=> $id));		
    }

    function getAppInfo($id)
    {
        $this->db->select();
        $this->db->from('tblapp');		
        $this->db->where('tblapp.appId', $id); 
        $query = $this->db->get();		
        return $query->row();
    }
    
    function getAppImages($id)
    {
        $this->db->select();
        $this->db->from('tblappimages');		
        $this->db->where('appid', $id); 
        $query = $this->db->get();		
        return $query->result();
    }

    function getAllUserApps($start=1, $per_page=10,$keyword,$id)
    {            
        $this->db->select();
        $this->db->from('tblapp');	            
        if($keyword!=''){
           $where = " (appname LIKE '%" . $keyword . "%' OR "
                    . "company LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        }
        $this->db->where('userId',$id);             
        $this->db->order_by('appId', 'DESC');
        $this->db->limit($start, $per_page);
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->result();
    }

    function getAllUserAppsCount($keyword,$id)
    {           
        $this->db->select();
        $this->db->from('tblapp');	           
        if($keyword!=''){
           $where = " (appname LIKE '%" . $keyword . "%' OR "
                    . "company LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        }
        $this->db->where('userId',$id);                         
        $query = $this->db->get();		
        return $query->num_rows();
    } 
    
    function checkIfOrderExists($txnid,$payerid)
    {
        $this->db->select();
        $this->db->from('tblorderdetail');
        $this->db->where('transactionid',$txnid);     
        $this->db->where('payer_id',$payerid);     
        $query = $this->db->get();		
        return $query->num_rows();    
    }
    
    function getUserApps($id)
    {            
        $this->db->select();
        $this->db->from('tblapp');
        $this->db->where('userId',$id);
        $this->db->where('status != ','Deleted');
        $this->db->order_by('appId', 'DESC');
        //$this->db->limit($start, $per_page);
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->result();
    }

    function getUserAppsCount($id)
    {           
        $this->db->select();
        $this->db->from('tblapp');	                  
        $this->db->where('userId',$id);  
        $this->db->where('status != ','Deleted');
        $query = $this->db->get();		
        return $query->num_rows();
    } 
    
    function getUserTransactions($id)
    {            
        $this->db->select('tblapp.status,tblapp.appname,tblorder.orderid,tblorderdetail.transactionid,tblorderdetail.createdOn,tblorderdetail.order_start_date,tblorderdetail.order_end_date,tblorderdetail.price');
        $this->db->from('tblorder');
        $this->db->join('tblorderdetail','tblorderdetail.orderid=tblorder.orderid');
        $this->db->join('tblapp','tblapp.appid=tblorder.appid');
        $this->db->where('tblorder.userid',$id);
        $this->db->where('tblapp.status != ','Deleted');
        //$this->db->order_by('appId', 'DESC');
        //$this->db->limit($start, $per_page);
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->result();
    }

    function getUserTransactionsCount($id)
    {           
        $this->db->select('tblapp.status,tblapp.appname,tblorder.orderid,tblorderdetail.transactionid,tblorderdetail.createdOn,tblorderdetail.order_start_date,tblorderdetail.order_end_date,tblorderdetail.price');
        $this->db->from('tblorder');
        $this->db->join('tblorderdetail','tblorderdetail.orderid=tblorder.orderid');
        $this->db->join('tblapp','tblapp.appid=tblorder.appid');
        $this->db->where('tblorder.userid',$id);
        $this->db->where('tblapp.status != ','Deleted');
        $query = $this->db->get();		
        return $query->num_rows();
    }
    
    function getAllCounties()
    {
        $this->db->select();
        $this->db->from('tblcountries');
        $query = $this->db->get();		
        return $query->result();
    }
    
    function checkAppExists($appName,$appId)
    {		
        $this->db->select();
        $this->db->from('tblapp');		
        $this->db->where('appname', $appName); 
        if($appId!=0)
        {
           $this->db->where('appId !=', $appId); 
        }
        $this->db->where('status !=', 'Deleted');
        $query = $this->db->get();		
        return $query->num_rows();
    }

    function encrypt($str,$key)
    {	
       if($str!=''){
            $iv = '372adfc77ec8913b';
            $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
            mcrypt_generic_init($td, $key, $iv);		
            $encrypted = mcrypt_generic($td, $str);
            mcrypt_generic_deinit($td);
            mcrypt_module_close($td);
            return bin2hex($encrypted);
        }else
        {
         return '';
        }
    }

    function decrypt($code,$key) 
    {
        $code = $this->hex2bin($code);
        $iv = '372adfc77ec8913b';
        $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
        mcrypt_generic_init($td, $key, $iv);
        $decrypted = mdecrypt_generic($td, $code);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return utf8_encode(trim($decrypted));
    }

    function hex2bin($hexdata)
    {
        $bindata = '';
        for ($i = 0; $i < strlen($hexdata); $i += 2) {
         $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
        }
        return $bindata;
    }	
}
?>