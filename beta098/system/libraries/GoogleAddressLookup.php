<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Project:     GoogleMapAPI V3: a PHP library inteface to the Google Map API v3
 * File:        GoogleMapV3.php
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * ORIGINAL INFO
 * link http://www.phpinsider.com/php/code/GoogleMapAPI/
 * copyright 2005 New Digital Group, Inc.
 * author Monte Ohrt <monte at ohrt dot com>
 * package GoogleMapAPI
 * version 2.5
 * 
 * REVISION NOTIFICATION
 * NOTE: This is a modified version of the original listed above.  This version
 * maintains all original GNU software licenses.
 */
/**
 * @link http://code.google.com/p/phpgooglemapapiv3/
 * @copyright 2010 Brad wedell
 * @author Brad Wedell
 * @package GoogleMapAPI (version 3)
 * @version 3.0beta
*/
/*
 * To view the full CHANGELOG, visit 
 * http://code.google.com/p/php-google-map-api/wiki/ChangeLog3
/*
For database caching, you will want to use this schema:

CREATE TABLE geocode_cache (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  lng double NOT NULL,
  lat double NOT NULL,
  query varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
);

*/

/**
 * PHP Google Maps API class
 * @package GoogleMapAPI
 * @version 3.0beta
 */
class GoogleAddressLookup{
    /**
     * Response
     * @var array
     */
    private $response;

    /**
     * Constructor
     */
    public function __construct(){
        $this->initialize();
    }

    /**
     * Initialize variables
     */
    private function initialize(){
        $this->response = array(
            'zip_code' => '',
            'state' => '',
            'state_short' => '',
            'city' => '',
            'city_short' => '',
            'country' => '',
            'country_short' => '',
            'latitude' => '',
            'longitude' => '',
            'raw_response' => ''
        );
    }

    /**
     * Sets Zup Code
     * @param int $zipCode
     */
    public function setZipCode($zipCode){
        $this->response['zip_code'] = $zipCode;

        $this->processResponse();
    }

    /**
     * Returns City full name
     *
     * @return string
     */
    public function getCity(){
        return $this->response['city'];
    }

    /**
     * Returns City short name
     *
     * @return string
     */
    public function getCityShortName(){
        return $this->response['city_short'];
    }

    /**
     * Returns State full name
     *
     * @return string
     */
    public function getState(){
        return $this->response['state'];
    }

    /**
     * Returns State short name
     *
     * @return string
     */
    public function getStateShortName(){
        return $this->response['state_short'];
    }

    /**
     * Returns Country full name
     *
     * @return string
     */
    public function getCountry(){
        return $this->response['country'];
    }

    /**
     * Returns Country short name
     *
     * @return string
     */
    public function getCountryShortName(){
        return $this->response['country_short'];
    }

    /**
     * Returns Latitude
     *
     * @return int
     */
    public function getLatitude(){
        return $this->response['latitude'];
    }

    /**
     * Returns Longitude
     *
     * @return int
     */
    public function getLongitude(){
        return $this->response['longitude'];
    }

    /**
     * Makes call to the API
     */
    private function makeCall(){
        if($this->response['zip_code']){
            $url = "http://maps.googleapis.com/maps/api/geocode/json?address={$this->response['zip_code']}&sensor=true";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
            curl_setopt($ch, CURLOPT_URL, $url);

            $this->response['raw_response'] = curl_exec($ch);
        }
    }

    /**
     * Processes the response received
     */
    private function processResponse(){
        $this->makeCall();

        if($this->response['raw_response']){
            $result = json_decode($this->response['raw_response']);

            if($result->status == 'OK'){
                if(!empty($result->results[0]->address_components)){
                    foreach($result->results[0]->address_components as $add_comp){
                        if(in_array('locality', $add_comp->types)){
                            $this->response['city'] = $add_comp->long_name;
                            $this->response['city_short'] = $add_comp->short_name;
                        }
                        elseif (in_array('administrative_area_level_1', $add_comp->types)) {
                            $this->response['state'] = $add_comp->long_name;
                            $this->response['state_short'] = $add_comp->short_name;
                        }
                        elseif (in_array('country', $add_comp->types)) {
                            $this->response['country'] = $add_comp->long_name;
                            $this->response['country_short'] = $add_comp->short_name;
                        }
                    }
                }

                if(!empty($result->results[0]->geometry)){
                    $this->response['longitude'] = $result->results[0]->geometry->location->lng;
                    $this->response['latitude'] = $result->results[0]->geometry->location->lat;
                }
            }
        }
    }
}

?>