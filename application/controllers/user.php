<?php
class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();    
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('user_model');
        $this->load->library('session');
        $this->load->library('pagination');  
        date_default_timezone_set($this->config->config['constTimeZone']);       
    }
	 
    function check_email()
    {
        $useremail=$this->input->post('e');
        echo $res = $this->webuser_model->check_customer_email($useremail); 
    }
	 
    function register()
    {
       $data['page_title'] = 'Create your account';
       $this->load->view('register_page_view', $data);       
    }
    
    function register_submit(){
       $today=date('Y-m-d H:i:s');
       $pdata['firstname']=$this->input->post('usr_first_name');
       $pdata['lastname']=$this->input->post('usr_last_name');
       $pdata['email']=$this->input->post('usr_email');    
       $pdata['password']=md5($this->input->post('usr_password'));
       $pdata['userType']='user';
       $pdata['status']='N';
       $pdata['createdDate']=$today;
       $pdata['modifiedDate']=$today;                  
       $this->user_model->insertUser($pdata);
       $userId=$this->db->insert_id();
       $row=$this->user_model->getUserInfo($userId);
       $this->session->set_userdata('user_email',$pdata['email']);
       $this->session->set_userdata('userId',$row->userId);
       $this->session->set_userdata('user_type',$row->userType);
       $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
       $this->session->set_userdata('user_photo',$row->userPhoto);
       $this->session->set_userdata('facebook_id',$row->facebookProfileId);
       $this->session->set_userdata('google_plus_id',$row->googlePlusProfileId);
       $this->session->set_userdata('user_status',$row->status);
       $this->session->set_userdata('is_logged_in',true); 
       
       $notif_arr=array('userId'=>$userId,'updates'=>'Y','tips'=>'Y','offers'=>'Y','userFeedback'=>'Y','createdDate'=>$today,'modifiedDate'=>$today);
       $this->db->insert('tblemailnotifications', $notif_arr); 
      
       $token=md5($userId.time());
       $this->db->update('tbluser', array('confirmationToken'=>$token), array('userId' => $userId));
       $activate_url=$this->config->config['base_url'].'user/activate/'.$token;
       $htmlMessage=file_get_contents($this->config->config['main_base_url'].'signup-email-template.html');
       $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage); 
       $htmlMessage=str_replace('{FULLNAME}',$pdata['firstname']." ".$pdata['lastname'] , $htmlMessage);
       $apikey='EQ4Z41iMdCc7uiznTzzUVw';
       $this->load->library('mandrill', array($apikey));
       $mandrill_ready = NULL;
       try {
            $this->mandrill->init( $apikey );
            $mandrill_ready = TRUE;
       } catch(Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
       }
       if($mandrill_ready) {
            //Send us some email!
            $email = array(
                'html' => $htmlMessage, //Consider using a view file
                'text' => '',
                'subject' => 'Your User Account at Seekmi',
                'from_email' => 'support@seekmi.com',
                'from_name' => 'Seekmi',
                'to' => array(array('email' => $pdata['email'])) //Check documentation for more details on this one                
                );

            $result = $this->mandrill->messages_send($email);           
       }
       if($row->userType=='user'){
        redirect('profile/dashboard');
       }else{
        redirect('profile/requests');   
       }
    }
    
    function forget_pass(){
       $today=date('Y-m-d H:i:s');
       
       $pdata['email']=$this->input->post('email');    
       
       $token=md5($userId.time());
       $this->db->update('tbluser', array('confirmationToken'=>$token), array('userId' => $userId));
       $activate_url=$this->config->config['base_url'].'user/activate/'.$token;
       $htmlMessage=file_get_contents($this->config->config['main_base_url'].'signup-email-template.html');
       $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage); 
       $htmlMessage=str_replace('{FULLNAME}',$pdata['firstname']." ".$pdata['lastname'] , $htmlMessage);
       $apikey='EQ4Z41iMdCc7uiznTzzUVw';
       $this->load->library('mandrill', array($apikey));
       $mandrill_ready = NULL;
       try {
            $this->mandrill->init( $apikey );
            $mandrill_ready = TRUE;
       } catch(Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
       }
       if($mandrill_ready) {
            //Send us some email!
            $email = array(
                'html' => $htmlMessage, //Consider using a view file
                'text' => '',
                'subject' => 'Your User Account at Seekmi',
                'from_email' => 'support@seekmi.com',
                'from_name' => 'Seekmi',
                'to' => array(array('email' => $pdata['email'])) //Check documentation for more details on this one                
                );

            $result = $this->mandrill->messages_send($email);           
       }
       if($row->userType=='user'){
        redirect('profile/dashboard');
       }else{
        redirect('profile/requests');   
       }
    }

    function login()
    {       
       $data['page_title'] = 'Welcome back to Seekmi';       
       if(isset($_GET['target'])){
        $data['target']=$this->input->get('target');
       }else{
          $data['target']=''; 
       }
       $this->load->view('login_page_view', $data); 
       
    }
    
    function fb_login(){
        $this->load->library('facebook'); // Automatically picks appId and secret from config
        $user = $this->facebook->getUser(); 

        if($user) {
            $user_profile = $this->facebook->api('/me');
            if(isset($user_profile['email'])){ 
            $today=date('Y-m-d H:i:s');
            $checkValid=$this->user_model->checkEmailExists($user_profile['email']);
            if($checkValid==1){
                $pdata['firstname']=$user_profile['first_name'];
                $pdata['lastname']=$user_profile['last_name'];
                $pdata['email']=$user_profile['email'];
                $pdata['facebookProfileId']=$user_profile['id'];
                $pdata['userPhoto']='https://graph.facebook.com/'.$user_profile['id'].'/picture?type=large';
                $pdata['userType']='user';
                $pdata['status']='N';
                $pdata['createdDate']=$today;
                $pdata['modifiedDate']=$today; 
                $this->user_model->insertUser($pdata);
                $userId=$this->db->insert_id();
                $token=md5($userId.time());
                $this->db->update('tbluser', array('confirmationToken'=>$token), array('userId' => $userId));
                $activate_url=$this->config->config['base_url'].'user/activate/'.$token;
                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'signup-email-template.html');
                $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage);       
                $htmlMessage=str_replace('{FULLNAME}', $pdata['firstname']." ".$pdata['lastname'], $htmlMessage);
                $apikey='EQ4Z41iMdCc7uiznTzzUVw';
                $this->load->library('mandrill', array($apikey));
                $mandrill_ready = NULL;
                try {
                     $this->mandrill->init( $apikey );
                     $mandrill_ready = TRUE;
                } catch(Mandrill_Exception $e) {
                     $mandrill_ready = FALSE;
                }
                if($mandrill_ready) {
                     //Send us some email!
                     $email = array(
                         'html' => $htmlMessage, //Consider using a view file
                         'text' => '',
                         'subject' => 'Your User Account at Seekmi',
                         'from_email' => 'support@seekmi.com',
                         'from_name' => 'Seekmi',
                         'to' => array(array('email' => $pdata['email'])) //Check documentation for more details on this one                
                         );

                     $result = $this->mandrill->messages_send($email);           
                }
            }else{
                $user_row=$this->user_model->getUserIdByEmail($user_profile['email']);
                $userId=$user_row->userId;                
            }
            $row=$this->user_model->getUserInfo($userId);
            $this->session->set_userdata('user_email',$user_profile['email']);
            $this->session->set_userdata('userId',$row->userId);
            $this->session->set_userdata('user_type',$row->userType);
            $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
            $this->session->set_userdata('user_photo',$row->userPhoto);
            $this->session->set_userdata('facebook_id',$row->facebookProfileId);
            $this->session->set_userdata('google_plus_id',$row->googlePlusProfileId);
            $this->session->set_userdata('user_status',$row->status);
            $this->session->set_userdata('is_logged_in',true); 
          
            redirect('profile/dashboard');
          }else{
                $data['page_title']='Add Facebook Email';
                $data['profile_id']=$user_profile['id'];
                $data['fullname']=$user_profile['first_name']." ".$user_profile['last_name'];
                $this->load->view('add_fb_email_page_view', $data);   
          }            
        } else {
             $login_url = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('user/fb_login'), 
                'scope' => array("email") // permissions here
            ));
            redirect($login_url);
        }
        
    }
    
    function login_submit(){
        $pdata['useremail']=$this->input->post('login_email');
        $pdata['userpass']=$this->input->post('login_password');
        $rem=$this->input->post('remember');
        $row=$this->user_model->check_customer_login($pdata);			
        if($row!='')
        {
            if(($row->userType=='professional') || ($row->status=='N' && $row->userType=='user') || ($row->status=='Y' && $row->userType=='user')){
                if($row->confirmationToken!='' && $row->userType=='professional')
                {
                    echo 'not_confirmed'.'||'.$row->userId;
                    die();
                }
                
                if($row->status=='N' && $row->userType=='professional')
                {
                    echo 'inactive'.'||'.$row->userId;
                    die();
                }
                
                $this->session->set_userdata('user_email',$pdata['useremail']);
                $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
                $this->session->set_userdata('user_photo',$row->userPhoto);
                $this->session->set_userdata('userId',$row->userId);
                $this->session->set_userdata('user_type',$row->userType);
                $this->session->set_userdata('is_logged_in',true);
                $this->session->set_userdata('facebook_id',$row->facebookProfileId);
                $this->session->set_userdata('google_plus_id',$row->googlePlusProfileId);
                $this->session->set_userdata('user_status',$row->status);
                if($rem==1){
                  $this->input->set_cookie('useremail', $pdata['email'], time()+(60*60*24*365));
                  $this->input->set_cookie('password', $pdata['password'], time()+(60*60*24*365));
                }
                $this->load->model('profile_model');
                $notification_res=$this->profile_model->getNotifications($row->userId);
                $this->user_model->updateUser(array('language'=> get_cookie('language')),$row->userId);
                if($notification_res==''){
                    $today=date('Y-m-d H:i:s');
                    $notif_arr=array('userId'=>$row->userId,'updates'=>'Y','tips'=>'Y','offers'=>'Y','userFeedback'=>'Y','createdDate'=>$today,'modifiedDate'=>$today);
                    $this->db->insert('tblemailnotifications', $notif_arr); 
                }
                if($row->userType=='professional'){
                    $this->load->model('pros_model');
                    $resChk=$this->pros_model->checkUserProviderInfoExit($row->userId); 
                    if($resChk==''){
                     $cdata['userId']=$row->userId;              
                     $this->pros_model->insertProvider($cdata);    
                    }
                }
                echo "success||".$row->userType; 
            }
            else
            {
                echo 'inactive||';
            }            			 
        }
        else
        {
            echo "failed||";
        }
    }
    
//    function fbemail()
//    {
//        $data['page_title']='Add Facebook Email';
//        $data['profile_id']='10205568818413845';
//        $data['fullname']='Deepika Desai-Bora';
//        $this->load->view('add_fb_email_page_view', $data);
//    }
    
    function addfbuser()
    {
        //print_r($_POST);
        $name = explode(' ',$this->input->post('fbname'));
        $pdata['firstname']=$name[0];
        $pdata['lastname']=$name[1];
        $pdata['email']=$this->input->post('usr_email');
        $pdata['facebookProfileId']=$this->input->post('fbid');
        $pdata['userType']='user';
        $today=date('Y-m-d H:i:s');
        $pdata['createdDate']=$today;  
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){
             $ip=$_SERVER['HTTP_CLIENT_IP'];
           //Is it a proxy address
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
           $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
           $ip=$_SERVER['REMOTE_ADDR'];
        }
        $pdata['ip_address']=$ip;
        //print_r($pdata);
        if($ip!=''){
           $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            $locStr='';
            if(isset($details->city)){
                $locStr.=$details->city;
            }
            if(isset($details->region)){
                if($locStr!=''){
                    $locStr.=", ";
                }
                $locStr.=$details->region;
            }
            if(isset($details->country)){
                if($locStr!=''){
                    $locStr.=", ";
                }
                $locStr.=$details->country;
            }
            $pdata['locationInfo']=$locStr;
            if(isset($details->loc)){
              $pdata['latlong']=$details->loc;
            }else{
              $pdata['latlong']='';  
            }
        }else{
           $pdata['locationInfo']='';
           $pdata['latlong']=''; 
        }
        $pdata['browserInfo']=$this->browser();
        $pdata['status']='N';
        
        $this->db->insert('tbluser', $pdata);    
        $userId=$this->db->insert_id();
        
        $row=$this->user_model->getUserInfo($userId);
        $this->session->set_userdata('user_email',$user_profile['email']);
        $this->session->set_userdata('userId',$row->userId);
        $this->session->set_userdata('user_type',$row->userType);
        $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
        $this->session->set_userdata('user_photo',$row->userPhoto);
        $this->session->set_userdata('facebook_id',$row->facebookProfileId);
        $this->session->set_userdata('google_plus_id',$row->googlePlusProfileId);
        $this->session->set_userdata('user_status',$row->status);
        $this->session->set_userdata('is_logged_in',true); 
        
        $token=md5($userId.time());
        $this->db->update('tbluser', array('confirmationToken'=>$token), array('userId' => $userId));
        $activate_url=$this->config->config['base_url'].'user/activate/'.$token;
        $htmlMessage=file_get_contents($this->config->config['main_base_url'].'signup-email-template.html');
        $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage);   
        $htmlMessage=str_replace('{FULLNAME}', $pdata['firstname']." ".$pdata['lastname'], $htmlMessage);
        $apikey='EQ4Z41iMdCc7uiznTzzUVw';
        $this->load->library('mandrill', array($apikey));
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init( $apikey );
            $mandrill_ready = TRUE;
        } catch(Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        if($mandrill_ready) {
            //Send us some email!
            $email = array(
                'html' => $htmlMessage, //Consider using a view file
                'text' => '',
                'subject' => 'Your User Account at Seekmi',
                'from_email' => 'support@seekmi.com',
                'from_name' => 'Seekmi',
                'to' => array(array('email' => $pdata['email'])) //Check documentation for more details on this one                
                );

            $result = $this->mandrill->messages_send($email);           
        }
        redirect('profile/dashboard');
    }
	 
    function logout()
    {	        
        $sess_items = array('user_email' => '', 'userId' => '','is_logged_in'=> false,'user_type'=>'','user_full_name'=>'','user_photo'=>'','facebook_id'=>'');
        $this->session->unset_userdata($sess_items);
         $this->load->library('facebook');
        // Logs off session from website
        $this->facebook->destroySession();
        //$this->session->sess_destroy();
        redirect('user/login');
    }
    
    function forgot_password()
    {
       $uemail=$this->input->post('usr_email');       
       $checkValid=$this->user_model->checkEmailExistsWithStatus($uemail);
       
       if($checkValid=='')
       {
          echo 'not_exist';
       }
       else
       {
           if($checkValid->status=='Y' || $checkValid->status=='review'){
            $res=$this->user_model->getUserIdByEmail($uemail);

            if(!empty($res))
            {          
                 $uid=base64_encode($res->userId);       
                 $characters = array(
                     "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M",
                     "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                     "1", "2", "3", "4", "5", "6", "7", "8", "9");           
                 $keys = array();          
                 while (count($keys) < 7) {               
                     $x = mt_rand(0, count($characters) - 1);
                     if (!in_array($x, $keys)) {
                         $keys[] = $x;
                     }
                 }
                 $random_chars='';
                 foreach ($keys as $key) {
                     $random_chars .= $characters[$key];
                 }
                 /*$this->load->library('email');				 			
                 $config['mailtype'] = "html";
                 $this->email->initialize($config);
                 $this->email->set_newline("\r\n");		
                 $this->email->to($uemail); 
                 $this->email->from('support@seekmi.com', 'Seekmi');
                 $this->email->subject('Reset Password at Seekmi');
                 $htmlMessage='<table><tr><td><b>Hi '.$res->firstname.',</b></td></tr></table>';
                 $htmlMessage .='<p>As you requested, your Seekmi password has been reset. To set a new password, click on the link below. </p>';
                 $htmlMessage .="<table><tr><td><a href='".$this->config->config['base_url']."profile/password/".md5($random_chars)."'>Set your new password</a></td></tr>";
                 $htmlMessage .='<tr><td>If the link above doesn\'t work, copy and paste the following into your browser: </td></tr>';
                 $htmlMessage .='<tr><td><a href="'.$this->config->config['base_url'].'profile/password/'.md5($random_chars).'">"'.$this->config->config['base_url'].'profile/password/'.md5($random_chars).'"</a> </td></tr>';
                 $htmlMessage .="<tr><td>&nbsp;</td></tr><tr><td>If you didn't request this password reset, let us know right away at <a href='mailto:support@seekmi.com'>support@seekmi.com</a>. </td></tr><tr><td>&nbsp;</td></tr>";
                 $htmlMessage .="<tr><td>Thanks,</td></tr>		
                 <tr><td>Seekmi Support</td></tr>
                 <tr><td><a href='".$this->config->config['base_url']."'>Seekmi</a></td></tr>";
                 $htmlMessage .='<tr><td></td></tr></table>';
                 $this->email->message($htmlMessage);     */
                 
                if(get_cookie('language')=='english')
                {
                    $template_file = 'forgot-password-template.html';
                }
                else
                {
                    $template_file = 'forgot-password-template-id.html';
                }
                
                $reset_password_url = $this->config->config['base_url']."profile/password/".md5($random_chars);
                 
                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/'.$template_file);
                $htmlMessage=str_replace('{FULLNAME}', $res->firstname.' '.$res->lastname, $htmlMessage);
                $htmlMessage=str_replace('{RESET_PASSWORD_URL}', $reset_password_url, $htmlMessage);   
                $apikey='EQ4Z41iMdCc7uiznTzzUVw';
                $this->load->library('mandrill', array($apikey));
                $mandrill_ready = NULL;
                try {
                    $this->mandrill->init( $apikey );
                    $mandrill_ready = TRUE;
                } catch(Mandrill_Exception $e) {
                    $mandrill_ready = FALSE;
                }
                if($mandrill_ready) {
                    //Send us some email!
                    $email = array(
                        'html' => $htmlMessage, //Consider using a view file
                        'text' => '',
                        'subject' => 'Reset your password for Seekmi',
                        'from_email' => 'support@seekmi.com',
                        'from_name' => 'Seekmi',
                        'to' => array(array('email' => $uemail)) //Check documentation for more details on this one                
                        );

                    $result = $this->mandrill->messages_send($email);      
                    
                    if(get_cookie('language') === 'english'){ 
                       $lang='en'; 
                    }else{
                        $lang='id';
                    }
                    
                     $this->db->update('tbluser', array('resetPasswordToken'=>md5($random_chars)), array('userId' => $res->userId));
                     if(get_cookie('language')=='english')
                     {
                        echo '<div class="confirmation">
                            <div class="alert-closable">
                                <input type="checkbox" class="alert-close">
                                <div class="alert-success">
                                    <p>Email sent!</p>
                                </div>
                            </div>
                            <p>
                                We just sent an email to <strong>'.$res->email.'</strong>.
                                If you <strong>go check your email now</strong>, you should find a
                                message from us with instructions on how to choose a new
                                password for your account.
                            </p>
                            <p>
                                If you don\'t receive an email from us (<a href="'.$this->config->config['main_base_url'].'app/'.$lang.'/contact-us/" target="_blank">support@seekmi.com</a>)
                                within a few minutes, please check your spam folder.
                            </p>
                        </div>';
                     }else{
                         echo '<div class="confirmation">
                            <div class="alert-closable">
                                <input type="checkbox" class="alert-close">
                                <div class="alert-success">
                                    <p>Email sent!</p>
                                </div>
                            </div>
                            <p>
                                Kami baru saja mengirimkan email ke <strong>'.$res->email.'</strong>. 
                                Silahkan periksa segera email dan ikuti petunjuk dari
                                kami untuk mendapatkan password baru bagi akun Seekmi Anda.                                 
                            <p>
                                Jika tidak menerima email dari kami (<a href="'.$this->config->config['main_base_url'].'app/'.$lang.'/contact-us/" target="_blank">support@seekmi.com</a>)
                                silahkan periksa folder spam pada email Anda.
                            </p>
                        </div>';
                     }
                }
                else{
                    
                    echo '<div class="confirmation">
                         <div class="alert-closable">
                             <input type="checkbox" class="alert-close">
                             <div class="alert-error">
                                 <p>Email not sent!</p>
                             </div>
                         </div>                 
                     </div>'; 
                     
                 }
            }
            else
            {
                 echo '<div class="confirmation">
                         <div class="alert-closable">
                             <input type="checkbox" class="alert-close">
                             <div class="alert-error">
                                 <p>Email not sent!</p>
                             </div>
                         </div>                 
                     </div>';
            }	
       }else if($checkValid->status=='banned'){
           echo 'banned';
       }else if($checkValid->status=='suspend'){
          echo 'suspend'; 
       }else if($checkValid->status=='N'){
          echo 'inactive';  
       }
           
       }
     }
    
     function check_email_exists(){
        $email=$this->input->get('usr_email');    
        $checkValid=$this->user_model->checkEmailExists($email);
        if($checkValid==1)
        {
          echo 'true';
        }
        else
        {
          echo 'false';
        }
     }
     
     function check_email_exists_status(){
        $email=$this->input->get('usr_email');    
        $checkValid=$this->user_model->checkEmailExistsWithStatus($email);
        if($checkValid=='')
        {
          echo 'true';
        }
        else
        {
           echo $checkValid->status;
        }
     } 

     function reset_password()
     { 
        $data['page_title']='Reset your password';        
        $this->load->view('reset_password_page_view',$data); 		 
     }
    
     function update_profile()
     {
        $data['firstname']=$this->input->post('profile_fname');
        $data['lastname']=$this->input->post('profile_lname');
        $data['phone']=$this->input->post('profile_phone');
        $data['address']=$this->input->post('profile_address');
        $data['city']=$this->input->post('profile_city');
        $data['state']=$this->input->post('profile_state');
        $data['country']=$this->input->post('profile_country');
        $uid=$this->input->post('prof_userid');
        $res= $this->user_model->updateUser($data,$uid);         
        echo "success";        
     } 
     
     function account_deactivate()
     {
         $data['page_title']='Account Deactivated';
         $this->load->view('account_deactivate_page_view',$data); 	
     }
     
     function not_confirmed($id)
     {
         $data['page_title']='Account Not Confirmed';
         $data['userid']=$id;
         $this->load->view('account_not_confirmed_page_view',$data); 	
     }

     function check_login()
     {
        $pdata['useremail']=$this->input->post('e');
        $pdata['userpass']=$this->input->post('pass');

        $row=$this->webuser_model->check_customer_login($pdata);			
        if($row!='')
        {		    
                echo "success";			 
        }
        else
        {
                echo "failed";
        }
      } 
      
      function activate($token)
      {
         $res=$this->user_model->checkValidConfirmationToken($token); 
         if($res==''){
             $data['status']='invalid';
         }else{
             if($res->status=='Y'){
               $data['status']='already';    
             } else {
               $data['status']='valid';
               $this->db->update('tbluser', array('confirmationToken'=>'','status'=>'Y','modifiedDate'=>date('Y-m-d H:i:s')), array('userId' => $res->userId));
             }
         }
         $data['page_title'] = 'Activated Account';         
         $this->load->view('confirm_account_user_page_view', $data);   
     }
     
     function resend_activation_link()
     {
        if($this->session->userdata('is_logged_in')==true)
        {
            $userId=$this->session->userdata('userId');
            $user_email=$this->session->userdata('user_email');
            $token=md5($userId.time());
            $this->db->update('tbluser', array('confirmationToken'=>$token,'modifiedDate'=>date('Y-m-d H:i:s')), array('userId' => $userId));
            $activate_url=$this->config->config['base_url'].'user/activate/'.$token;
            $htmlMessage=file_get_contents($this->config->config['main_base_url'].'signup-email-template.html');
            $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage); 
            $htmlMessage=str_replace(' {FULLNAME}', '', $htmlMessage);
            $apikey='EQ4Z41iMdCc7uiznTzzUVw';
            $this->load->library('mandrill', array($apikey));
            $mandrill_ready = NULL;
            try {
                 $this->mandrill->init( $apikey );
                 $mandrill_ready = TRUE;
            } catch(Mandrill_Exception $e) {
                 $mandrill_ready = FALSE;
            }
            if($mandrill_ready) {
                 //Send us some email!
                 $email = array(
                     'html' => $htmlMessage, //Consider using a view file
                     'text' => '',
                     'subject' => 'Your User Account at Seekmi',
                     'from_email' => 'support@seekmi.com',
                     'from_name' => 'Seekmi',
                     'to' => array(array('email' => $user_email)) //Check documentation for more details on this one                
                     );

                 $result = $this->mandrill->messages_send($email);  
                 echo 'success';
            } else {
               echo 'failed';   
            }
           
        }else{
           redirect('user/login');
        }
     }
     
     function browser()
     {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $browsers = array(
                            'Chrome' => array('Google Chrome','Chrome/(.*)\s'),
                            'MSIE' => array('Internet Explorer','MSIE\s([0-9\.]*)'),
                            'Firefox' => array('Firefox', 'Firefox/([0-9\.]*)'),
                            'Safari' => array('Safari', 'Version/([0-9\.]*)'),
                            'Opera' => array('Opera', 'Version/([0-9\.]*)')
                            ); 

        $browser_details = array();

            foreach ($browsers as $browser => $browser_info){
                if (preg_match('@'.$browser.'@i', $user_agent)){
                    $browser_details['name'] = $browser_info[0];
                        preg_match('@'.$browser_info[1].'@i', $user_agent, $version);
                    $browser_details['version'] = $version[1];
                        break;
                } else {
                    $browser_details['name'] = 'Unknown';
                    $browser_details['version'] = 'Unknown';
                }
            }

        return $browser_details['name'].' Version: '.$browser_details['version'];
    }
     
}
?>
