<?php
class Uber extends CI_Controller
{
     function __construct()
     {
        parent::__construct();    
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('customer_model');
        $this->load->library('session');
        $this->load->library('pagination');  
        date_default_timezone_set($this->config->config['constTimeZone']);       
     }
 	 
     function register()
     {
	$data['page_title']='Sign Up';       
        $this->load->view('signup_uber_page_view',$data); 
     }
     
     function submit()
     {
        $today=date('Y-m-d H:i:s'); 
        $pdata['firstname']=$this->input->post('firstName');
        $pdata['lastname']=$this->input->post('lastName');
        $pdata['phone']=$this->input->post('phone');
        $pdata['email']=$this->input->post('usr_email');   
        $pdata['password']=md5($this->input->post('password'));
        $pdata['signupSource']='Uber';
        $pdata['userType']='professional';
        $pdata['services']='Private Driver';    
        $pdata['status']='N';
        $pdata['createdDate']=$today;
        $pdata['modifiedDate']=$today;
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){
             $ip=$_SERVER['HTTP_CLIENT_IP'];
           //Is it a proxy address
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
           $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
           $ip=$_SERVER['REMOTE_ADDR'];
        }
        $pdata['ip_address']=$ip;
        if($ip!=''){
           $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            $locStr='';
            if(isset($details->city)){
                $locStr.=$details->city;
            }
            if(isset($details->region)){
                if($locStr!=''){
                    $locStr.=", ";
                }
                $locStr.=$details->region;
            }
            if(isset($details->country)){
                if($locStr!=''){
                    $locStr.=", ";
                }
                $locStr.=$details->country;
            }
            $pdata['locationInfo']=$locStr;
            if(isset($details->loc)){
              $pdata['latlong']=$details->loc;
            }else{
              $pdata['latlong']='';  
            }
        }else{
           $pdata['locationInfo']='';
           $pdata['latlong']=''; 
        }
        $pdata['browserInfo']=$this->browser();
        $this->db->insert('tbluser', $pdata); 
        $userId=$this->db->insert_id();
        $cdata['userId']=$userId;              
        $this->db->insert('tbluserprovider', $cdata); 
        $token=md5($userId.time());
        $this->db->update('tbluser', array('confirmationToken'=>$token), array('userId' => $userId));
        $activate_url=$this->config->config['base_url'].'uber/activate/'.$token;
        $htmlMessage=file_get_contents($this->config->config['base_url'].'signup-uber-email-template.html');
        $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage);
        $apikey='EQ4Z41iMdCc7uiznTzzUVw';
        $this->load->library('mandrill', array($apikey));
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init( $apikey );
            $mandrill_ready = TRUE;
        } catch(Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        if($mandrill_ready) {
            //Send us some email!
            $email = array(
                'html' => $htmlMessage, //Consider using a view file
                'text' => '',
                'subject' => $this->config->config['subject_for_uber'],
                'from_email' => 'support@seekmi.com',
                'from_name' => 'Seekmi',
                'to' => array(array('email' => $pdata['email'])) //Check documentation for more details on this one                
                );

            $result = $this->mandrill->messages_send($email);           
        }
        redirect('uber/thankyou/'.md5($userId));
     }
     
     function thankyou($id)
     {
         $data['page_title'] = 'Thank You';
         $data['status']='new';
         //$this->load->view('thank_you_uber_page_view', $data);   
         $this->load->model('pros_model');
         $user_data=$this->pros_model->getUserInfoFromMd5Id($id);
         if(!empty($user_data)){
            $data['usemail'] = $user_data->email;  
            $this->load->view('thank_you_pros_page_view', $data);               
         } else {
            redirect('/');
         }
     }
     
     function activate($token)
    {
         $res=$this->customer_model->checkValidConfirmationTokenUber($token); 
         if($res==''){
             $data['status']='invalid';
         }else{
             if($res->status=='Y'){
               $data['status']='already';    
             } else {
               $data['status']='valid';
               $this->db->update('tbluser', array('confirmationToken'=>'','status'=>'Y'), array('userId' => $res->userId));
             }
         }
         $data['page_title'] = 'Activated Account';         
         $this->load->view('confirm_account_uber_page_view', $data);   
    }
     
     function browser()
     {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $browsers = array(
                            'Chrome' => array('Google Chrome','Chrome/(.*)\s'),
                            'MSIE' => array('Internet Explorer','MSIE\s([0-9\.]*)'),
                            'Firefox' => array('Firefox', 'Firefox/([0-9\.]*)'),
                            'Safari' => array('Safari', 'Version/([0-9\.]*)'),
                            'Opera' => array('Opera', 'Version/([0-9\.]*)')
                            ); 

        $browser_details = array();

            foreach ($browsers as $browser => $browser_info){
                if (preg_match('@'.$browser.'@i', $user_agent)){
                    $browser_details['name'] = $browser_info[0];
                        preg_match('@'.$browser_info[1].'@i', $user_agent, $version);
                    $browser_details['version'] = $version[1];
                        break;
                } else {
                    $browser_details['name'] = 'Unknown';
                    $browser_details['version'] = 'Unknown';
                }
            }

        return $browser_details['name'].' Version: '.$browser_details['version'];
    }
     
     
}
?>