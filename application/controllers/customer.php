<?php
class Customer extends CI_Controller
{
    function __construct()
    {
        parent::__construct();    
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('customer_model');
        $this->load->library('session');
        $this->load->library('pagination');  
        date_default_timezone_set($this->config->config['constTimeZone']);       
    }
	 
//    function index(){  
//        $apikey='EQ4Z41iMdCc7uiznTzzUVw';
//        $this->load->library('mandrill', array($apikey));
//        $mandrill_ready = NULL;
//        try {
//            $this->mandrill->init( $apikey );
//            $mandrill_ready = TRUE;
//        } catch(Mandrill_Exception $e) {
//            $mandrill_ready = FALSE;
//        }
//
//        if($mandrill_ready) {
//            //Send us some email!
//            $email = array(
//                'html' => '<p>This is my message<p>', //Consider using a view file
//                'text' => 'This is my plaintext message',
//                'subject' => 'This is my subject',
//                'from_email' => 'admin@sphinx-solution.com',
//                'from_name' => 'Me-Oh-My',
//                'to' => array(array('email' => 'deepika@sphinx-solution.com' )) //Check documentation for more details on this one
//                //'to' => array(array('email' => 'joe@example.com' ),array('email' => 'joe2@example.com' )) //for multiple emails
//                );
//
//            $result = $this->mandrill->messages_send($email);
//            print_r($result);
//        }
//    }
    
    function register()
    {
       $data['page_title'] = 'Create your account';
       $this->load->view('register_page_view', $data);       
    }
    
    function register_submit(){
       $today=date('Y-m-d H:i:s');
       //$pdata['fullname']=$this->input->post('usr_first_name')." ".$this->input->post('usr_last_name');
      // $pdata['lastname']=$this->input->post('usr_last_name');
       $pdata['email']=$this->input->post('usr_email');    
      // $pdata['password']=md5($this->input->post('usr_password'));     
       $pdata['createdDate']=$today;  
       if (!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip=$_SERVER['HTTP_CLIENT_IP'];
          //Is it a proxy address
       }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
       }else{
          $ip=$_SERVER['REMOTE_ADDR'];
       }
       $pdata['ip_address']=$ip;
       if($ip!=''){
          $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
          $locStr='';
          if(isset($details->city)){
              $locStr.=$details->city;
          }
          if(isset($details->region)){
              if($locStr!=''){
                  $locStr.=", ";
              }
              $locStr.=$details->region;
          }
          if(isset($details->country)){
              if($locStr!=''){
                  $locStr.=", ";
              }
              $locStr.=$details->country;
          }
          $pdata['locationInfo']=$locStr;
          if(isset($details->loc)){
            $pdata['latlong']=$details->loc;
          }else{
            $pdata['latlong']='';  
          }
       }else{
          $pdata['locationInfo']='';
          $pdata['latlong']=''; 
       }
       $pdata['browserInfo']=$this->browser();
       $pdata['status']='N';
       $this->customer_model->insertUser($pdata); 
       $userId=$this->db->insert_id();
       $token=md5($userId.time());
       $this->db->update('tblseekmeusers', array('confirmationToken'=>$token), array('userId' => $userId));
       $activate_url=$this->config->config['base_url'].'customer/activate/'.$token;
       $htmlMessage=file_get_contents($this->config->config['base_url'].'signup-email-template.html');
       $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage); 
       $htmlMessage=str_replace(' {FULLNAME}', '', $htmlMessage);
       $apikey='EQ4Z41iMdCc7uiznTzzUVw';
       $this->load->library('mandrill', array($apikey));
       $mandrill_ready = NULL;
       try {
            $this->mandrill->init( $apikey );
            $mandrill_ready = TRUE;
       } catch(Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
       }
       if($mandrill_ready) {
            //Send us some email!
            $email = array(
                'html' => $htmlMessage, //Consider using a view file
                'text' => '',
                'subject' => $this->config->config['subject_for_normal_user'],
                'from_email' => 'support@seekmi.com',
                'from_name' => 'Seekmi',
                'to' => array(array('email' => $pdata['email'])) //Check documentation for more details on this one                
                );

            $result = $this->mandrill->messages_send($email);           
       }
       redirect('customer/thankyou');
    }
    
    function activate($token)
    {
         $res=$this->customer_model->checkValidConfirmationToken($token); 
         if($res==''){
             $data['status']='invalid';
         }else{
             if($res->status=='Y'){
               $data['status']='already';    
             } else {
               $data['status']='valid';
               $this->db->update('tblseekmeusers', array('confirmationToken'=>'','status'=>'Y'), array('userId' => $res->userId));
             }
         }
         $data['page_title'] = 'Activated Account';         
         $this->load->view('confirm_account_user_page_view', $data);   
    }
    
    function fb_login(){
        $this->load->library('facebook'); // Automatically picks appId and secret from config        
        $user = $this->facebook->getUser(); 
        if($user) {
            $user_profile = $this->facebook->api('/me');
            if(isset($user_profile['email'])){ 
                $today=date('Y-m-d H:i:s');
                $checkValid=$this->customer_model->checkEmailExists($user_profile['email']);
                if($checkValid==1){
                    $pdata['fullname']=$user_profile['first_name']." ".$user_profile['last_name'];               
                    $pdata['email']=$user_profile['email'];
                    $pdata['facebookProfileId']=$user_profile['id'];                
                    $pdata['createdDate']=$today;  
                    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
                         $ip=$_SERVER['HTTP_CLIENT_IP'];
                       //Is it a proxy address
                    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                       $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
                    }else{
                       $ip=$_SERVER['REMOTE_ADDR'];
                    }
                    $pdata['ip_address']=$ip;
                    if($ip!=''){
                       $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
                        $locStr='';
                        if(isset($details->city)){
                            $locStr.=$details->city;
                        }
                        if(isset($details->region)){
                            if($locStr!=''){
                                $locStr.=", ";
                            }
                            $locStr.=$details->region;
                        }
                        if(isset($details->country)){
                            if($locStr!=''){
                                $locStr.=", ";
                            }
                            $locStr.=$details->country;
                        }
                        $pdata['locationInfo']=$locStr;
                        if(isset($details->loc)){
                          $pdata['latlong']=$details->loc;
                        }else{
                          $pdata['latlong']='';  
                        }
                    }else{
                       $pdata['locationInfo']='';
                       $pdata['latlong']=''; 
                    }
                    $pdata['browserInfo']=$this->browser();
                    $pdata['status']='N';
                    $this->db->insert('tblseekmeusers', $pdata); 
                    $userId=$this->db->insert_id();                    
                    $token=md5($userId.time());
                    $this->db->update('tblseekmeusers', array('confirmationToken'=>$token), array('userId' => $userId));
                    $activate_url=$this->config->config['base_url'].'customer/activate/'.$token;
                    $htmlMessage=file_get_contents($this->config->config['base_url'].'signup-email-template.html');
                    $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage);       
                    $htmlMessage=str_replace('{FULLNAME}', $pdata['fullname'], $htmlMessage);
                    $apikey='EQ4Z41iMdCc7uiznTzzUVw';
                    $this->load->library('mandrill', array($apikey));
                    $mandrill_ready = NULL;
                    try {
                         $this->mandrill->init( $apikey );
                         $mandrill_ready = TRUE;
                    } catch(Mandrill_Exception $e) {
                         $mandrill_ready = FALSE;
                    }
                    if($mandrill_ready) {
                         //Send us some email!
                         $email = array(
                             'html' => $htmlMessage, //Consider using a view file
                             'text' => '',
                             'subject' => 'Your User Account at Seekmi',
                             'from_email' => 'support@seekmi.com',
                             'from_name' => 'Seekmi',
                             'to' => array(array('email' => $pdata['email'])) //Check documentation for more details on this one                
                             );

                         $result = $this->mandrill->messages_send($email);           
                    }
                    redirect('customer/thankyou');
                }else{

                    redirect('customer/exists');
                }
            }else{
                $data['page_title']='Add Facebook Email';
                $data['profile_id']=$user_profile['id'];
                $data['fullname']=$user_profile['first_name']." ".$user_profile['last_name'];
                $this->load->view('add_fb_email_page_view', $data);   
            }
            
        } else {
             $login_url = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('customer/fb_login'), 
                'scope' => array("email") // permissions here
            ));
            redirect($login_url);
        }
        
    }
    
   function fbemail()
    {
        $data['page_title']='Add Facebook Email';
        $data['profile_id']='10205568818413845';
        $data['fullname']='Deepika Desai-Bora';
        $this->load->view('add_fb_email_page_view', $data);
    }
    
    function addfbuser()
    {
        $pdata['fullname']=$this->input->post('fbname');               
        $pdata['email']=$this->input->post('usr_email');
        $pdata['facebookProfileId']=$this->input->post('fbid'); 
        $today=date('Y-m-d H:i:s');
        $pdata['createdDate']=$today;  
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){
             $ip=$_SERVER['HTTP_CLIENT_IP'];
           //Is it a proxy address
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
           $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
           $ip=$_SERVER['REMOTE_ADDR'];
        }
        $pdata['ip_address']=$ip;
        if($ip!=''){
           $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            $locStr='';
            if(isset($details->city)){
                $locStr.=$details->city;
            }
            if(isset($details->region)){
                if($locStr!=''){
                    $locStr.=", ";
                }
                $locStr.=$details->region;
            }
            if(isset($details->country)){
                if($locStr!=''){
                    $locStr.=", ";
                }
                $locStr.=$details->country;
            }
            $pdata['locationInfo']=$locStr;
            if(isset($details->loc)){
              $pdata['latlong']=$details->loc;
            }else{
              $pdata['latlong']='';  
            }
        }else{
           $pdata['locationInfo']='';
           $pdata['latlong']=''; 
        }
        $pdata['browserInfo']=$this->browser();
        $pdata['status']='N';
        $this->db->insert('tblseekmeusers', $pdata);    
        $userId=$this->db->insert_id();
        $token=md5($userId.time());
        $this->db->update('tblseekmeusers', array('confirmationToken'=>$token), array('userId' => $userId));
        $activate_url=$this->config->config['base_url'].'customer/activate/'.$token;
        $htmlMessage=file_get_contents($this->config->config['base_url'].'signup-email-template.html');
        $htmlMessage=str_replace('{ACTIVATE_URL}', $activate_url, $htmlMessage);   
        $htmlMessage=str_replace('{FULLNAME}', $pdata['fullname'], $htmlMessage);
        $apikey='EQ4Z41iMdCc7uiznTzzUVw';
        $this->load->library('mandrill', array($apikey));
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init( $apikey );
            $mandrill_ready = TRUE;
        } catch(Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        if($mandrill_ready) {
            //Send us some email!
            $email = array(
                'html' => $htmlMessage, //Consider using a view file
                'text' => '',
                'subject' => 'Your User Account at Seekmi',
                'from_email' => 'support@seekmi.com',
                'from_name' => 'Seekmi',
                'to' => array(array('email' => $pdata['email'])) //Check documentation for more details on this one                
                );

            $result = $this->mandrill->messages_send($email);           
        }
        redirect('customer/thankyou');
    }
    
    function thankyou()
    {
         $data['page_title'] = 'Thank You';
         $data['status']='new';
         $this->load->view('thank_you_page_view', $data);   
    }
    
    function exists()
    {
         $data['page_title'] = 'Already Registered';
         $data['status']='exists';
         $this->load->view('thank_you_page_view', $data);   
    }
    
    function login_submit(){
        $pdata['useremail']=$this->input->post('login_email');
        $pdata['userpass']=$this->input->post('login_password');
        $rem=$this->input->post('remember');
        $row=$this->customer_model->check_customer_login($pdata);			
        if($row!='')
        {
            if($row->status=='Y'){
                $this->session->set_userdata('user_email',$pdata['useremail']);
                $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
                $this->session->set_userdata('user_photo',$row->userPhoto);
                $this->session->set_userdata('userId',$row->userId);
                $this->session->set_userdata('user_type',$row->userType);
                $this->session->set_userdata('is_logged_in',true);
                $this->session->set_userdata('facebook_id',$row->facebookProfileId);
                if($rem==1){
                  $this->input->set_cookie('useremail', $pdata['email'], time()+(60*60*24*365));
                  $this->input->set_cookie('password', $pdata['password'], time()+(60*60*24*365));
                }
                $this->load->model('profile_model');
                $notification_res=$this->profile_model->getNotifications($row->userId);
                if($notification_res==''){
                    $today=date('Y-m-d H:i:s');
                    $notif_arr=array('userId'=>$row->userId,'updates'=>'Y','tips'=>'Y','offers'=>'Y','userFeedback'=>'Y','createdDate'=>$today,'modifiedDate'=>$today);
                    $this->db->insert('tblemailnotifications', $notif_arr); 
                }
                echo "success"; 
            }
            else
            {
                echo 'inactive';
            }            			 
        }
        else
        {
            echo "failed";
        }
    }
	 
    function logout()
    {	        
        $sess_items = array('user_email' => '', 'userId' => '','is_logged_in'=> false,'user_type'=>'','user_full_name'=>'','user_photo'=>'','facebook_id'=>'');
        $this->session->unset_userdata($sess_items);
         $this->load->library('facebook');
        // Logs off session from website
        $this->facebook->destroySession();
        //$this->session->sess_destroy();
        redirect('/');
    }
    
    function check_email_exists(){
        $email=$this->input->get('usr_email');    
        $checkValid=$this->customer_model->checkEmailExists($email);
        if($checkValid==1)
        {
          echo 'true';
        }
        else
        {
          echo 'false';
        }
     }
     
     function check_email_exists_status(){
        $email=$this->input->get('usr_email');    
        $checkValid=$this->customer_model->checkEmailExistsWithStatus($email);
        if($checkValid==1)
        {
          echo 'false';
        }
        else
        {
          echo 'true';
        }
     } 

     function reset_password()
     { 
        $data['page_title']='Reset your password';        
        $this->load->view('reset_password_page_view',$data); 		 
     }
    
     function update_profile()
     {
        $data['firstname']=$this->input->post('profile_fname');
        $data['lastname']=$this->input->post('profile_lname');
        $data['phone']=$this->input->post('profile_phone');
        $data['address']=$this->input->post('profile_address');
        $data['city']=$this->input->post('profile_city');
        $data['state']=$this->input->post('profile_state');
        $data['country']=$this->input->post('profile_country');
        $uid=$this->input->post('prof_userid');
        $res= $this->customer_model->updateUser($data,$uid);         
        echo "success";        
     } 
     
     function account_deactivate()
     {
         $data['page_title']='Account Deactivated';
         $this->load->view('account_deactivate_page_view',$data); 	
     }
     
     /*function add_service_slug()
     {
         $this->load->model('service_model');
         $services=$this->service_model->getAllServices1();
         foreach($services as $services_row){
             echo $slug=url_title($services_row->subserviceName, 'dash', TRUE);
             $this->db->update('tblsubservices', array('serviceSlug'=>$slug),array('subserviceId'=>$services_row->subserviceId));             
         }         
     }*/
     
     function browser()
     {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $browsers = array(
                            'Chrome' => array('Google Chrome','Chrome/(.*)\s'),
                            'MSIE' => array('Internet Explorer','MSIE\s([0-9\.]*)'),
                            'Firefox' => array('Firefox', 'Firefox/([0-9\.]*)'),
                            'Safari' => array('Safari', 'Version/([0-9\.]*)'),
                            'Opera' => array('Opera', 'Version/([0-9\.]*)')
                            ); 

        $browser_details = array();

            foreach ($browsers as $browser => $browser_info){
                if (preg_match('@'.$browser.'@i', $user_agent)){
                    $browser_details['name'] = $browser_info[0];
                        preg_match('@'.$browser_info[1].'@i', $user_agent, $version);
                    $browser_details['version'] = $version[1];
                        break;
                } else {
                    $browser_details['name'] = 'Unknown';
                    $browser_details['version'] = 'Unknown';
                }
            }

        return $browser_details['name'].' Version: '.$browser_details['version'];
    }
     
     
}
?>