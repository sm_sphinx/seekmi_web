<?php

class Service extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('admin_service_model');
        $this->load->library('session');
        $this->load->library('pagination');
        setcookie('language', 'english', time() + (86400 * 30), "/");
        date_default_timezone_set($this->config->config['constTimeZone']);
    }

    function categorylist() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage Categories';
        $page = (int) $this->session->userdata('categorylistpage');
        $page = $page > 0 ? $page : 1;
        $data['page'] = $page;
        $this->load->view($this->config->config['admin_folder'] . '/category_list_view', $data);
    }

    function categorylist_pagi() {
        $this->logged_in_user();
        $this->session->set_userdata('categorylistpage', $this->input->post('page'));
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $start = $page * $data['per_page'];
        $data['page_title'] = 'Manage Categories';
        $data['list_count'] = $this->admin_service_model->getAllCategoryCount($data['keyword']);
        $data['list_res'] = $this->admin_service_model->getAllCategory($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/category_list_pagi_view', $data);
    }

    function add_category() {
        $this->logged_in_user();
        $data['page_title'] = 'Add Category';
        $this->load->view($this->config->config['admin_folder'] . '/set_category_info', $data);
    }

    function edit_category($id) {
        $this->logged_in_user();
        $data['page_title'] = 'Edit Category';
        $data['cat_info'] = $this->admin_service_model->getCategoryInfo($id);
        $this->load->view($this->config->config['admin_folder'] . '/set_category_info', $data);
    }

    function saveCategoryInfo() {
        $this->logged_in_user();
        $catId = $this->input->post('cat_id');
        $catName = $this->input->post('txtCatname');
        $catNameInd = $this->input->post('txtCatnameind');
        $status = $this->input->post('txtStatus');
        $checkCat = $this->admin_service_model->checkCategoryExists($catName, $catId);
        if ($checkCat == 0) {
            $cat_arr = array('categoryName' => $catName, 'categoryNameIND' => $catNameInd, 'status' => $status);
            if ($catId == 0) {
                $this->db->insert('tblcategory', $cat_arr);
            } else {
                $this->db->update('tblcategory', $cat_arr, array('catId' => $catId));
            }
            echo 'success';
        } else {
            echo 'already';
        }
    }

    function deleteCategory() {
        $this->logged_in_user();
        $cid = $this->input->post('catId');
        $this->db->delete('tblcategory', array('catId' => $cid));
        $this->db->delete('tblservices', array('categoryId' => $cid));
        $this->db->query('delete a from tblservicemapping as a join tblservices as b on(a.serviceId=b.serviceId and b.categoryId="' . $cid . '")');
        echo 'success';
    }

    function sublist() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage Services';
         //$this->session->set_userdata('sublistpage',1);
        $page = (int) $this->session->userdata('sublistpage');
        //var_dump($page);
        //die();
        $page = $page > 0 ? $page : 1;
        $data['page'] = $page;
        $this->load->view($this->config->config['admin_folder'] . '/sub_service_list_view', $data);
    }

    function sublist_pagi() {
        $this->logged_in_user();
        $this->session->set_userdata('sublistpage', $this->input->post('page'));
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $start = $page * $data['per_page'];
        $data['page_title'] = 'Manage Services';
        $data['list_count'] = $this->admin_service_model->getAllSubServicesCount($data['keyword']);
        $data['list_res'] = $this->admin_service_model->getAllSubServices($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/sub_service_list_pagi_view', $data);
    }

    function add_subservice() {
        $this->logged_in_user();
        $data['page_title'] = 'Add Service';
        $data['cat_list'] = $this->admin_service_model->getCategories();
        $this->load->view($this->config->config['admin_folder'] . '/set_sub_service_info', $data);
    }

    function edit_subservice($id) {
        $this->logged_in_user();
        $data['page_title'] = 'Edit Service';
        $data['service_info'] = $this->admin_service_model->getSubServiceInfo($id);
        $data['cat_list'] = $this->admin_service_model->getCategories();
        $data['tag_info'] = $this->admin_service_model->getSubServicesTagsBySubserviceId($id);
        $data['tag_id_info'] = $this->admin_service_model->getSubServicesTagsIDBySubserviceId($id);
        $this->load->view($this->config->config['admin_folder'] . '/set_sub_service_info', $data);
    }

    function saveSubServiceInfo() {
        $this->logged_in_user();
        $subId = $this->input->post('service_id');
        $servName = $this->input->post('txtServiceName');
        $servNameInd = $this->input->post('txtServiceNameIND');
        $servslug = $this->input->post('txtServiceSlug');
        $questionTypeArr = $this->input->post('questionType');
        $questionTypeArrMain = array('informatic', 'details', 'time', 'travel','duration', 'zip');
        $timeeArrMain = array('flexible', 'days', 'asap', 'specific', 'other');
        $status = $this->input->post('txtStatus');
        $avlCredit = $this->input->post('avlCredit');
        $isFeatured = $this->input->post('txtIsFeatured');
        $catId = $this->input->post('txtCategory');
        if ($this->input->post('timeOptionType')) {
            $timeStr = implode(",", $this->input->post('timeOptionType'));
        } else {
            $timeStr = '';
        }
        $checkServ = $this->admin_service_model->checkSubServiceExists($servName, $subId);
        if ($checkServ == 0) {
            for ($k = 0; $k < count($questionTypeArrMain); $k++) {
                if (in_array($questionTypeArrMain[$k], $questionTypeArr)) {
                    $serv_arr[$questionTypeArrMain[$k] . 'Type'] = 'Y';
                } else {
                    $serv_arr[$questionTypeArrMain[$k] . 'Type'] = 'N';
                }
            }
            if (!in_array('time', $questionTypeArr)) {
                $timeStr = '';
            }

            $serv_arr['subserviceName'] = $servName;
            $serv_arr['subserviceNameIND'] = $servNameInd;
            $serv_arr['serviceSlug'] = $servslug;
            $serv_arr['timeOption'] = $timeStr;
            $serv_arr['status'] = $status;
            $serv_arr['avlCredit'] = $avlCredit;
            $serv_arr['isFeatured'] = $isFeatured;
            $serv_arr['categoryId'] = $catId;
            if ($subId == 0) {
                $this->db->insert('tblsubservices', $serv_arr);
                $subId = $this->db->insert_id();
                /* Add SUB Services tags */
                if ($this->input->post('txtServiceTags') != '') {
                    $tagArray = explode(",", $this->input->post('txtServiceTags'));

                    foreach ($tagArray as $k => $row) {
                        $tdata['subServiceId'] = $subId;
                        $tdata['serviceTag'] = $row;
                        $tdata['isEnglish'] = 'Y';
                        $this->db->insert('tblsubserviceTags', $tdata);
                    }
                }
                if ($this->input->post('txtServiceTagsID') != '') {
                    $tagIdArray = explode(",", $this->input->post('txtServiceTagsID'));

                    foreach ($tagIdArray as $k => $row) {
                        $tdata['subServiceId'] = $subId;
                        $tdata['serviceTag'] = $row;
                        $tdata['isEnglish'] = 'N';
                        $this->db->insert('tblsubserviceTags', $tdata);
                    }
                }
            } else {
                $this->db->update('tblsubservices', $serv_arr, array('subserviceId' => $subId));
                /* Edit SUB Services tags */
                $this->db->delete('tblsubserviceTags', array('subServiceId' => $subId));
                if ($this->input->post('txtServiceTags') != '') {
                    $tagArray = explode(",", $this->input->post('txtServiceTags'));
                    foreach ($tagArray as $k => $row) {
                        $tdata['subServiceId'] = $subId;
                        $tdata['serviceTag'] = $row;
                        $this->db->insert('tblsubserviceTags', $tdata);
                        $tagId = $this->db->insert_id();
                    }
                }
                if ($this->input->post('txtServiceTagsID') != '') {
                    $tagIdArray = explode(",", $this->input->post('txtServiceTagsID'));
                    foreach ($tagIdArray as $k => $row) {
                        $tdata['subServiceId'] = $subId;
                        $tdata['serviceTag'] = $row;
                        $tdata['isEnglish'] = 'N';
                        $this->db->insert('tblsubserviceTags', $tdata);
                    }
                }
            }


            $question_detailid = $this->admin_service_model->getQuestionForService($subId, 'details');
            $question_name = 'Anything else the ' . strtolower($servName) . ' should know?';
            $question_name_indonesion = 'Ada lagi yang ' . strtolower($servName) . ' harus tahu?';
            if ($question_detailid == '') {
                if (in_array('details', $questionTypeArr)) {
                    $this->db->insert('tblquestions', array('questionName' => $question_name, 'questionNameIndonesion' => $question_name_indonesion, 'fieldType' => 'textarea', 'serviceId' => $subId, 'type' => 'details', 'active' => 'Y'));
                }
            } else {
                if (in_array('details', $questionTypeArr)) {
                    $this->db->update('tblquestions', array('questionName' => $question_name, 'questionNameIndonesion' => $question_name_indonesion, 'fieldType' => 'textarea', 'serviceId' => $subId, 'type' => 'details', 'active' => 'Y'), array('questionId' => $question_detailid));
                } else {
                    $this->db->update('tblquestions', array('active' => 'N'), array('questionId' => $question_detailid));
                }
            }

            $question_timeid = $this->admin_service_model->getQuestionForService($subId, 'time');
            $question_time_name = 'When do you need ' . strtolower($servName) . '?';
            $question_time_name_indonesion = 'Kapan Anda butuhkan ' . strtolower($servName) . '?';
            if ($question_timeid == '') {
                if (in_array('time', $questionTypeArr)) {
                    $this->db->insert('tblquestions', array('questionName' => $question_time_name, 'questionNameIndonesion' => $question_time_name_indonesion, 'fieldType' => 'select', 'serviceId' => $subId, 'type' => 'time', 'active' => 'Y'));
                    $question_timeid = $this->db->insert_id();
                }
            } else {
                if (in_array('time', $questionTypeArr)) {
                    $this->db->update('tblquestions', array('questionName' => $question_time_name, 'questionNameIndonesion' => $question_time_name_indonesion, 'fieldType' => 'select', 'serviceId' => $subId, 'type' => 'time', 'active' => 'Y'), array('questionId' => $question_timeid));
                } else {
                    $this->db->update('tblquestions', array('active' => 'N'), array('questionId' => $question_timeid));
                }
            }
            if ($timeStr != '' && $question_timeid != '') {
                //$this->db->delete('tblansweroptions', array('questionId' => $question_timeid)); 
                $this->db->update('tblansweroptions', array('active' => 'N'), array('questionId' => $question_timeid));
                $optionListArr = $this->admin_service_model->getAllOptionsByQuestion($question_timeid);
                if (empty($optionListArr)) {
                    if (in_array('flexible', $this->input->post('timeOptionType'))) {
                        $this->db->insert('tblansweroptions', array('optionText' => "I'm flexible", "optionTextIndonesion" => "Saya fleksibel", 'questionId' => $question_timeid, 'optionType' => '', 'active' => 'Y'));
                    } else {
                        $this->db->insert('tblansweroptions', array('optionText' => "I'm flexible", "optionTextIndonesion" => "Saya fleksibel", 'questionId' => $question_timeid, 'optionType' => '', 'active' => 'N'));
                    }
                    if (in_array('days', $this->input->post('timeOptionType'))) {
                        $this->db->insert('tblansweroptions', array('optionText' => "In the next few days", "optionTextIndonesion" => "Dalam beberapa hari ke depan", 'questionId' => $question_timeid, 'optionType' => '', 'active' => 'Y'));
                    } else {
                        $this->db->insert('tblansweroptions', array('optionText' => "In the next few days", "optionTextIndonesion" => "Dalam beberapa hari ke depan", 'questionId' => $question_timeid, 'optionType' => '', 'active' => 'N'));
                    }
                    if (in_array('asap', $this->input->post('timeOptionType'))) {
                        $this->db->insert('tblansweroptions', array('optionText' => "As soon as possible", "optionTextIndonesion" => "secepat mungkin", 'questionId' => $question_timeid, 'optionType' => '', 'active' => 'Y'));
                    } else {
                        $this->db->insert('tblansweroptions', array('optionText' => "As soon as possible", "optionTextIndonesion" => "secepat mungkin", 'questionId' => $question_timeid, 'optionType' => '', 'active' => 'N'));
                    }
                    if (in_array('specific', $this->input->post('timeOptionType'))) {
                        $this->db->insert('tblansweroptions', array('optionText' => "On one particular date", "optionTextIndonesion" => "Di satu tanggal tertentu", 'questionId' => $question_timeid, 'optionType' => 'datetime_hours', 'active' => 'Y'));
                    } else {
                        $this->db->insert('tblansweroptions', array('optionText' => "On one particular date", "optionTextIndonesion" => "Di satu tanggal tertentu", 'questionId' => $question_timeid, 'optionType' => 'datetime_hours', 'active' => 'N'));
                    }
                    if (in_array('other', $this->input->post('timeOptionType'))) {
                        $this->db->insert('tblansweroptions', array('optionText' => "Other (I'd need to describe)", "optionTextIndonesion" => "Lainnya (aku perlu menjelaskan)", 'questionId' => $question_timeid, 'optionType' => 'other', 'active' => 'Y'));
                    } else {
                        $this->db->insert('tblansweroptions', array('optionText' => "Other (I'd need to describe)", "optionTextIndonesion" => "Lainnya (aku perlu menjelaskan)", 'questionId' => $question_timeid, 'optionType' => 'other', 'active' => 'N'));
                    }
                } else {
                    $new_optarr = array();
                    foreach ($optionListArr as $option_list_row) {
                        $new_optarr[] = $option_list_row->optionId;
                    }
                    if (in_array('flexible', $this->input->post('timeOptionType'))) {
                        $this->db->update('tblansweroptions', array('optionText' => "I'm flexible", "optionTextIndonesion" => "Saya fleksibel", 'questionId' => $question_timeid, 'optionType' => '', 'active' => 'Y'), array('optionId' => $new_optarr[0]));
                    }
                    if (in_array('days', $this->input->post('timeOptionType'))) {
                        $this->db->update('tblansweroptions', array('optionText' => "In the next few days", "optionTextIndonesion" => "Dalam beberapa hari ke depan", 'questionId' => $question_timeid, 'optionType' => '', 'active' => 'Y'), array('optionId' => $new_optarr[1]));
                    }
                    if (in_array('asap', $this->input->post('timeOptionType'))) {
                        $this->db->update('tblansweroptions', array('optionText' => "As soon as possible", "optionTextIndonesion" => "secepat mungkin", 'questionId' => $question_timeid, 'optionType' => '', 'active' => 'Y'), array('optionId' => $new_optarr[2]));
                    }
                    if (in_array('specific', $this->input->post('timeOptionType'))) {
                        $this->db->update('tblansweroptions', array('optionText' => "On one particular date", "optionTextIndonesion" => "Di satu tanggal tertentu", 'questionId' => $question_timeid, 'optionType' => 'datetime_hours', 'active' => 'Y'), array('optionId' => $new_optarr[3]));
                    }
                    if (in_array('other', $this->input->post('timeOptionType'))) {
                        $this->db->update('tblansweroptions', array('optionText' => "Other (I'd need to describe)", "optionTextIndonesion" => "Lainnya (aku perlu menjelaskan)", 'questionId' => $question_timeid, 'optionType' => 'other', 'active' => 'Y'), array('optionId' => $new_optarr[4]));
                    }
                }
            } else {
                $this->db->update('tblansweroptions', array('active' => 'N'), array('questionId' => $question_timeid));
            }

            $question_travelid = $this->admin_service_model->getQuestionForService($subId, 'travel');
            $question_travel_name = 'Where can the ' . strtolower($servName) . ' occur?';
            $question_travel_name_indonesion = 'Di mana bisa di ' . strtolower($servName) . ' terjadi?';
            if ($question_travelid == '') {
                if (in_array('travel', $questionTypeArr)) {
                    $this->db->insert('tblquestions', array('questionName' => $question_travel_name, 'questionNameIndonesion' => $question_travel_name_indonesion, 'fieldType' => 'checkbox', 'serviceId' => $subId, 'type' => 'travel', 'active' => 'Y'));
                    $question_travelid = $this->db->insert_id();
                }
            } else {
                if (in_array('travel', $questionTypeArr)) {
                    $this->db->update('tblquestions', array('questionName' => $question_travel_name, 'questionNameIndonesion' => $question_travel_name_indonesion, 'fieldType' => 'checkbox', 'serviceId' => $subId, 'type' => 'travel', 'active' => 'Y'), array('questionId' => $question_travelid));
                } else {
                    $this->db->update('tblquestions', array('active' => 'N'), array('questionId' => $question_travelid));
                }
            }
            if (in_array('travel', $questionTypeArr) && $question_travelid != '') {
                //$this->db->delete('tblansweroptions', array('questionId' => $question_travelid));                    
                $this->db->update('tblansweroptions', array('active' => 'N'), array('questionId' => $question_travelid));
                $optionTravelListArr = $this->admin_service_model->getAllOptionsByQuestion($question_travelid);
                if (empty($optionTravelListArr)) {
                    $this->db->insert('tblansweroptions', array('optionText' => "I travel to the " . $servName, 'optionTextIndonesion' => "Saya berjalan ke " . $servName, 'questionId' => $question_travelid, 'optionType' => 'miles', 'active' => 'Y'));
                    $this->db->insert('tblansweroptions', array('optionText' => "The " . $servName . " travels to me", 'optionTextIndonesion' => "Penyedia jasa datang ke saya", 'questionId' => $question_travelid, 'optionType' => '', 'active' => 'Y'));
                    $this->db->insert('tblansweroptions', array('optionText' => "Phone or internet (no in-person meeting)", 'optionTextIndonesion' => "Telfon atau Internet (tidak bertemu muka ke muka)", 'questionId' => $question_travelid, 'optionType' => '', 'active' => 'Y'));
                } else {
                    $new_travel_arr = array();
                    foreach ($optionTravelListArr as $option_travel_row) {
                        $new_travel_arr[] = $option_travel_row->optionId;
                    }
                    $this->db->update('tblansweroptions', array('optionText' => "I travel to the " . $servName, 'optionTextIndonesion' => "Saya berjalan ke " . $servName, 'questionId' => $question_travelid, 'optionType' => 'miles', 'active' => 'Y'), array('optionId' => $new_travel_arr[0]));
                    $this->db->update('tblansweroptions', array('optionText' => "The " . $servName . " travels to me", 'optionTextIndonesion' => "Penyedia jasa datang ke saya", 'questionId' => $question_travelid, 'optionType' => '', 'active' => 'Y'), array('optionId' => $new_travel_arr[1]));
                    $this->db->update('tblansweroptions', array('optionText' => "Phone or internet (no in-person meeting)", 'optionTextIndonesion' => "Telfon atau Internet (tidak bertemu muka ke muka)", 'questionId' => $question_travelid, 'optionType' => '', 'active' => 'Y'), array('optionId' => $new_travel_arr[2]));
                }
            } else {
                $this->db->update('tblansweroptions', array('active' => 'N'), array('questionId' => $question_travelid));
            }
            
            $question_durationid = $this->admin_service_model->getQuestionForService($subId, 'duration');
            $question_duration_name = 'How long will you been needing this service for?';
            $question_duration_name_indonesion = 'Selama berapa lamakah Anda memerlukan jasa ini?';
            if ($question_durationid == '') {
                if (in_array('duration', $questionTypeArr)) {
                    $this->db->insert('tblquestions', array('questionName' => $question_duration_name, 'questionNameIndonesion' => $question_duration_name_indonesion, 'fieldType' => 'datepicker', 'serviceId' => $subId, 'type' => 'duration', 'active' => 'Y'));
                    $question_durationid = $this->db->insert_id();
                }
            } else {
                if (in_array('duration', $questionTypeArr)) {
                    $this->db->update('tblquestions', array('questionName' => $question_duration_name, 'questionNameIndonesion' => $question_duration_name_indonesion, 'fieldType' => 'datepicker', 'serviceId' => $subId, 'type' => 'duration', 'active' => 'Y'), array('questionId' => $question_durationid));
                } else {
                    $this->db->update('tblquestions', array('active' => 'N'), array('questionId' => $question_durationid));
                }
            }
            
            $question_zipid = $this->admin_service_model->getQuestionForService($subId, 'zipcode');
            $question_zip_name = 'Where do you need the ' . strtolower($servName) . '?';
            $question_zip_name_indonesion = 'Di mana Anda perlu ' . strtolower($servName) . '?';
            if ($question_zipid == '') {
                if (in_array('zip', $questionTypeArr)) {
                    $this->db->insert('tblquestions', array('questionName' => $question_zip_name, 'questionNameIndonesion' => $question_zip_name_indonesion, 'fieldType' => 'address', 'serviceId' => $subId, 'type' => 'zipcode', 'active' => 'Y'));
                }
            } else {
                if (in_array('zip', $questionTypeArr)) {
                    $this->db->update('tblquestions', array('questionName' => $question_zip_name, 'questionNameIndonesion' => $question_zip_name_indonesion, 'fieldType' => 'address', 'serviceId' => $subId, 'type' => 'zipcode', 'active' => 'Y'), array('questionId' => $question_zipid));
                } else {
                    $this->db->update('tblquestions', array('active' => 'N'), array('questionId' => $question_zipid));
                }
            }
            echo 'success';
        } else {
            echo 'already';
        }
    }

    function deleteSubService() {
        $this->logged_in_user();
        $sid = $this->input->post('subId');
        $this->db->delete('tblsubservices', array('subserviceId' => $sid));
        $this->db->delete('tblservicemapping', array('subserviceId' => $sid));
        echo 'success';
    }

    function lists() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage Services';
        $data['cat_list'] = $this->admin_service_model->getCategories();
        $this->load->view($this->config->config['admin_folder'] . '/service_list_view', $data);
    }

    function lists_pagi() {
        $this->logged_in_user();
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $data['catSearch'] = $this->input->post('cat');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $start = $page * $data['per_page'];
        $data['page_title'] = 'Manage Services';
        $data['list_count'] = $this->admin_service_model->getAllServicesCount($data['keyword'], $data['catSearch']);
        $data['list_res'] = $this->admin_service_model->getAllServices($data['per_page'], $start, $data['keyword'], $data['catSearch']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/service_list_pagi_view', $data);
    }

    function add_service() {
        $this->logged_in_user();
        $data['page_title'] = 'Add Service';
        $data['cat_list'] = $this->admin_service_model->getCategories();
        $data['sub_list'] = $this->admin_service_model->getSubServicesList();
        $data['sub_service_list'] = '';
        $this->load->view($this->config->config['admin_folder'] . '/set_service_info', $data);
    }

    function edit_service($id) {
        $this->logged_in_user();
        $data['page_title'] = 'Edit Service';
        $data['cat_list'] = $this->admin_service_model->getCategories();
        $data['sub_list'] = $this->admin_service_model->getSubServicesList();
        $data['service_info'] = $this->admin_service_model->getServiceInfo($id);
        $data['sub_service_list'] = $this->admin_service_model->getSelectedSubServices($id);
        $this->load->view($this->config->config['admin_folder'] . '/set_service_info', $data);
    }

    function saveServiceInfo() {
        $this->logged_in_user();
        $servId = $this->input->post('service_id');
        $servName = $this->input->post('txtServiceName');
        $servNameInd = $this->input->post('txtServiceNameIND');
        $servslug = $this->input->post('txtServiceSlug');
        $catId = $this->input->post('txtCategory');
        $status = $this->input->post('txtStatus');
        $subservices = $this->input->post('txtSubService');
        $checkServ = $this->admin_service_model->checkServiceExists($servName, $servId);
        if ($checkServ == 0) {
            $serv_arr = array('serviceName' => $servName, 'serviceNameIND' => $servNameInd, 'serviceSlug' => $servslug, 'categoryId' => $catId, 'status' => $status);
            if ($servId == 0) {
                $this->db->insert('tblservices', $serv_arr);
                $servId = $this->db->insert_id();
            } else {
                $this->db->update('tblservices', $serv_arr, array('serviceId' => $servId));
            }
            if (!empty($subservices)) {
                $this->db->delete('tblservicemapping', array('serviceId' => $servId));
                for ($k = 0; $k < count($subservices); $k++) {
                    $sub_serv = array();
                    $sub_serv = array('subserviceId' => $subservices[$k], 'serviceId' => $servId);
                    $this->db->insert('tblservicemapping', $sub_serv);
                }
            }
            echo 'success';
        } else {
            echo 'already';
        }
    }

    function deleteService() {
        $this->logged_in_user();
        $sid = $this->input->post('servId');
        $this->db->delete('tblservices', array('serviceId' => $sid));
        $this->db->delete('tblservicemapping', array('serviceId' => $sid));
        echo 'success';
    }

    function questionlist_pagi() {
        $this->logged_in_user();
        $userId = (int) $this->session->userdata('admin_userId');
        $servId = $this->input->post('serviceid');
        if ($servId != '0') {
            $data['question_list'] = $this->admin_service_model->getQuestionsByServiceId($servId);
        } else {
            $data['question_list'] = '';
        }
        $this->load->view($this->config->config['admin_folder'] . '/pagi/question_list_pagi_view', $data);
    }

    function set_question() {
        $this->logged_in_user();
        $userId = (int) $this->session->userdata('admin_userId');
        $data['serviceId'] = $this->input->post('serviceid');
        $type = $this->input->post('type');
        $questionId = $this->input->post('qid');
        if ($questionId != '') {

            $data['question_row'] = $this->admin_service_model->getQuestionDetails($questionId);
            //  $data['option_cnt'] = $this->admin_service_model->getOptionCountByQuestion($questionId);
            $data['option_list'] = $this->admin_service_model->getOptionsByQuestion($questionId);
            if (!empty($data['option_list'])) {
                $data['option_cnt'] = $this->admin_service_model->getOptionCountByQuestion($questionId);
            } else {
                $data['option_cnt'] = 1;
            }
            $data['page_title'] = 'Edit';
        } else {
            $data['question_row'] = '';
            $data['option_list'] = '';
            $data['option_cnt'] = 1;
            $data['page_title'] = 'Add';
        }
        $this->load->view($this->config->config['admin_folder'] . '/set_question_info', $data);
    }

    function saveQuestionInfo() {
        $this->logged_in_user();
        $pdata['serviceId'] = $this->input->post('service_id');
        $questionId = $this->input->post('question_id');
        $pdata['questionName'] = $this->input->post('txtQuestionType');
        $pdata['questionNameIndonesion'] = $this->input->post('txtQuestionTypeIndonesion');
        $pdata['fieldType'] = $this->input->post('txtFieldType');
        $pdata['type'] = 'informatic';
        $pdata['active'] = 'Y';
        $option_cnt = (int) $this->input->post('option_cnt');

        if ($questionId == '') {
            $this->db->insert('tblquestions', $pdata);
            $questionId = $this->db->insert_id();
        } else {
            $this->db->update('tblquestions', $pdata, array('questionId' => $questionId));
        }
        if ($pdata['fieldType'] == 'checkbox' || $pdata['fieldType'] == 'select' || $pdata['fieldType'] == 'radio') {
            $optionArr = $this->admin_service_model->getOptionsByQuestion($questionId);
            if (empty($optionArr)) {

                for ($k = 1; $k <= $option_cnt; $k++) {
                    $optionText = $this->input->post('txtOption' . $k);
                    $optionTextIndonesion = $this->input->post('txtOptionIndonesion' . $k);
                    if ($optionText != '') {
                        $sub_opt = array();
                        if (strtolower($optionText) == 'other') {
                            $optType = 'other';
                        } else {
                            $optType = '';
                        }
                        $sub_opt = array('optionText' => $optionText, 'optionTextIndonesion' => $optionTextIndonesion, 'questionId' => $questionId, 'optionType' => $optType, 'active' => 'Y');
                        $this->db->insert('tblansweroptions', $sub_opt);
                    }
                }
            } else {

                $option_new_arr = array();
                foreach ($optionArr as $optionlist) {
                    $option_new_arr[] = (int) $optionlist->optionId;
                }

                $this->db->update('tblansweroptions', array('active' => 'N'), array('questionId' => $questionId));
                //die;
                for ($k = 1; $k <= $option_cnt; $k++) {
                    $optionText = $this->input->post('txtOption' . $k);
                    $optionTextIndonesion = $this->input->post('txtOptionIndonesion' . $k);
//                    var_dump($option_new_arr);
//                    die();
                    $optionIdHid = $this->input->post('txtOptionHid' . $k);
                    if ($optionText != '') {
                        $sub_opt = array();
                        if (strtolower($optionText) == 'other') {
                            $optType = 'other';
                        } else {
                            $optType = '';
                        }
                        // print_r($option_new_arr);
                        //echo $questionId;
                        $sub_opt = array();
                        if (in_array($optionIdHid, $option_new_arr)) {
                            //echo "update in";
                            $sub_opt = array('optionText' => $optionText, 'optionTextIndonesion' => $optionTextIndonesion, 'optionType' => $optType, 'active' => 'Y');
                            $this->db->update('tblansweroptions', $sub_opt, array('optionId' => $optionIdHid));
                        } else {
                            //echo "insertin";
                            $sub_opt = array('optionText' => $optionText, 'optionTextIndonesion' => $optionTextIndonesion, 'questionId' => $questionId, 'optionType' => $optType, 'active' => 'Y');
                            $this->db->insert('tblansweroptions', $sub_opt);
                        }
                        // echo $this->db->last_query();
                    }
                }
            }
        }
        echo 'success';
    }

    function deleteQuestion() {
        $this->logged_in_user();
        $qid = $this->input->post('questionId');
        $this->db->update('tblquestions', array('active' => 'N'), array('questionId' => $qid));
        $this->db->update('tblansweroptions', array('active' => 'N'), array('questionId' => $qid));
        echo 'success';
    }

    function getServices() {
        $catId = $this->input->post('catId');
        $this->load->model('admin_service_model');
        $service_list = $this->admin_service_model->getServicesByCategoryId($catId);
        $html = '';
        if (!empty($service_list)) {
            foreach ($service_list as $service_row) {
                $html.='<option value="' . $service_row->serviceId . '">' . $service_row->serviceName . '</option>';
            }
        }
        echo $html;
    }

    function getSubServices() {
        $servId = $this->input->post('servId');
        $this->load->model('admin_service_model');
        $service_list = $this->admin_service_model->getSubservicesByServiceId($servId);
        $html = '';
        if (!empty($service_list)) {
            foreach ($service_list as $service_row) {
                $html.='<option value="' . $service_row->subserviceId . '">' . $service_row->subserviceName . '</option>';
            }
        }
        echo $html;
    }

    function jobs() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage Job Requests';
        $page = (int) $this->session->userdata('jobspage');
        $page = $page > 0 ? $page : 1;
        $data['page'] = $page;
        $this->load->view($this->config->config['admin_folder'] . '/jobs_list_view', $data);
    }

    function jobs_pagi() {
        $this->logged_in_user();
        $this->session->set_userdata('jobspage', $this->input->post('page'));
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $data['catSearch'] = $this->input->post('cat');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $start = $page * $data['per_page'];
        $data['page_title'] = 'Manage Job Requests';
        $data['list_count'] = $this->admin_service_model->getAllJobRequestCount($data['keyword']);
        $data['list_res'] = $this->admin_service_model->getAllJobRequest($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/jobs_list_pagi_view', $data);
    }

    function view_job($id) {
        $this->logged_in_user();
        $data['page_title'] = 'View Job Request';
        $this->load->model('profile_model');
        $data['req_data'] = $this->admin_service_model->getRequestQuestions($id);
        $data['project_owner_data'] = $this->profile_model->getProjectOwner($id);

        $provinceId = (int) $data['project_owner_data']->provinceId;
        $cityId = (int) $data['project_owner_data']->cityId;
        $districtId = (int) $data['project_owner_data']->districtId;

        $this->load->model('setting_model');
        $address_row = $this->setting_model->getFullAdrress($provinceId, $cityId, $districtId);
        $data['address'] = $address_row->cityName . "," . $address_row->districtName . "," . $address_row->provName;

        $optionArr = array();
        if ($data['req_data'] != '') {
            $option_extra = '';
            foreach ($data['req_data'] as $req_row) {
                $optArr = array();
                if ($req_row->optionVal == '' || $req_row->optionVal == '0') {
                    $option_data = $this->admin_service_model->getRequestAnswers($req_row->questionId, $id);
                    if (!empty($option_data)) {
                        foreach ($option_data as $option_row) {
                            if ($option_row->optionType == 'miles') {
                                if ($option_row->extras != '') {
                                    $extraArr = json_decode($option_row->extras);
                                    if (!empty($extraArr)) {
                                        $option_extra = $extraArr->miles . " kilometer";
                                    }
                                } else {
                                    $option_extra = '';
                                }
                            } else {
                                $option_extra = '';
                            }
                            $optArr[] = array('optionText' => $option_row->optionText, 'optionType' => $option_row->optionType, 'optionExtra' => $option_extra);
                        }
                    } else {
                        $optArr = array();
                    }
                } else {
                    $optArr[] = array('optionText' => $req_row->optionVal, 'optionType' => '', 'optionExtra' => $option_extra);
                }
                $optionArr[$req_row->questionId] = $optArr;
            }
        }
        $data['req_option_data'] = $optionArr;
        $this->load->view($this->config->config['admin_folder'] . '/view_job_info', $data);
    }

    function service_deatils_pagi() {
        $this->logged_in_user();
        $this->session->set_userdata('jobspage', $this->input->post('page'));
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $data['catSearch'] = $this->input->post('cat');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $start = $page * $data['per_page'];
        $data['page_title'] = 'View Service Details';
        $data['list_count'] = $this->admin_service_model->getAllServiceDetailsCount($data['keyword']);
        $data['list_res'] = $this->admin_service_model->getAllServiceDetails($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/service_deatails_pagi_view', $data);
    }

    function suggested_list() {
        $this->logged_in_user();
        $data['page_title'] = 'Suggested Services';
        $page = (int) $this->session->userdata('suggestedpage');
        $page = $page > 0 ? $page : 1;
        $data['page'] = $page;
        $this->load->view($this->config->config['admin_folder'] . '/suggested_service_list_view', $data);
    }

    function suggested_list_pagi() {
        $this->logged_in_user();
        $this->session->set_userdata('suggestedpage', $this->input->post('page'));
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $data['catSearch'] = $this->input->post('cat');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $start = $page * $data['per_page'];
        $data['page_title'] = 'Suggested Services';
        $data['list_count'] = $this->admin_service_model->getSuggestedServiceCount($data['keyword']);
        $data['list_res'] = $this->admin_service_model->getSuggestedService($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/suggested_service_list_pagi_view', $data);
    }

    function packagelist() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage Packages';
        $this->load->view($this->config->config['admin_folder'] . '/package_list_view', $data);
    }

    function packagelist_pagi() {
        $this->logged_in_user();
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $start = $page * $data['per_page'];
        $data['page_title'] = 'Manage Packages';
        $data['list_count'] = $this->admin_service_model->getAllPackageCount($data['keyword']);
        $data['list_res'] = $this->admin_service_model->getAllPackage($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/package_list_pagi_view', $data);
    }

    function add_package() {
        $this->logged_in_user();
        $data['page_title'] = 'Add Package';
        $this->load->view($this->config->config['admin_folder'] . '/set_package_info', $data);
    }

    function edit_package($id) {
        $this->logged_in_user();
        $data['page_title'] = 'Edit Package';
        $data['package_info'] = $this->admin_service_model->getPackageInfo($id);
        $this->load->view($this->config->config['admin_folder'] . '/set_package_info', $data);
    }

    function savePackageInfo() {
        $this->logged_in_user();
        $packageId = $this->input->post('packageId');
        $packagename_en = $this->input->post('packagename_en');
        $packagename_id = $this->input->post('packagename_id');
        $amount = $this->input->post('amount');
        $credit = $this->input->post('credit');
        $active = $this->input->post('active');
        $checkPackage = $this->admin_service_model->checkPackageExists($packagename_en, $packagename_id, $packageId);
        if ($checkPackage == 0) {
            $cat_arr = array(
                'packagename_en' => $packagename_en,
                'packagename_id' => $packagename_id,
                'amount' => $amount,
                'credit' => $credit,
                'active' => $active,
            );
            if ($packageId == 0) {
                $this->db->insert('tblCreditPackages', $cat_arr);
            } else {
                $this->db->update('tblCreditPackages', $cat_arr, array('packageId' => $packageId));
            }
            echo 'success';
        } else {
            echo 'already';
        }
    }

    function editJobRequest($services) {
        $projectId = $services;
        if ($services != '') {
            $servicesarray = $this->admin_service_model->getservicename((int) $services);
            $services = $servicesarray->subserviceName;

            $data['servicesinfo'] = $servicesarray;

            $data['page_title'] = 'Edit Job Request';
            $data['service'] = urldecode($services);
            //$data['zipcode']=$zipcode;    
            $question_list = $this->admin_service_model->getQuestionariesList($data['service']);
            if (empty($question_list)) {
                $dbServiceRes = $this->admin_service_model->getServiceIdByTag($data['service']);
                if (!empty($dbServiceRes)) {
                    if ($dbServiceRes->isEnglish == 'Y') {
                        $question_list = $this->admin_service_model->getQuestionariesList($dbServiceRes->subserviceName);
                    } else {
                        $question_list = $this->admin_service_model->getQuestionariesList($dbServiceRes->subserviceNameIND);
                    }
                }
            }
            $option_arr = array();
            $question_arr = array();
            $serviceId = '';
            if (!empty($question_list)) {
                foreach ($question_list as $question_row) {
                    $question_arr[$question_row->type][] = $question_row;
                    $option_list = $this->admin_service_model->getOptionList($question_row->questionId);
                    if ($option_list != '') {
                        foreach ($option_list as $option_row) {
                            $option_arr[$question_row->questionId][] = $option_row;
                        }
                    }
                    $serviceId = $question_row->serviceId;
                }
                $srch_keyword_arr = array('searchQueryKeyword' => $data['service'], 'isExists' => 'Y', 'isSuggested' => 'N', 'createdDate' => date('Y-m-d H:i:s'));
                $this->db->insert('tblsearchquery', $srch_keyword_arr);
            } else {
                $srch_keyword_arr = array('searchQueryKeyword' => $data['service'], 'isExists' => 'N', 'isSuggested' => 'Y', 'createdDate' => date('Y-m-d H:i:s'));
                $this->db->insert('tblsearchquery', $srch_keyword_arr);
            }
            $service_arr = explode(' ', $data['service']);
            $data['suggestion_list'] = $this->admin_service_model->getServiceSuggestionList($service_arr);
            /* if(empty($data['suggestion_list'])){
              $services=substr($data['service'], 0, 3);
              $data['suggestion_list']=$this->admin_service_model->getServiceSuggestionList($services,'after');
              } */
            $data['serviceId'] = $serviceId;
            /* $data['pros_count']=$this->admin_service_model->getProsCountForService($serviceId);
              if($data['pros_count']==0){
              $data['pros_service_list']=$this->admin_service_model->getServiceListForPros();
              } */

            $data['is_featured'] = $this->admin_service_model->checkServiceIsFeatured($serviceId);
            $data['answerlist'] = $this->admin_service_model->getRequestallAnswers($projectId);
            $answerarray = array();
            foreach ($data['answerlist'] as $key => $value) {
                $answerarray[$value->questionId] = array('optionId' => $value->optionId, 'optionVal' => $value->optionVal, 'extras' => $value->extras, 'optionType' => $value->optionType);
            }
            $data['answers'] = $answerarray;
//                echo '<pre>';
//                print_r($answerarray);
//                die;

            $data['question_list'] = $question_arr;
            $data['option_list'] = $option_arr;
            $this->load->model('setting_model');
            $data['province_list'] = $this->setting_model->getProvincesOtherList();
            $this->load->view($this->config->config['admin_folder'] . '/edit_job_request_info', $data);
        } else {
            redirect('/');
        }
    }

    function submit_request() {
        $prj_arr = array();
        $questIds = $this->input->post('questionIds');
        $serviceid = $this->input->post('serviceid');
        //$zipcode=$this->input->post('zipcode');
        $provinceId = $this->input->post('province');
        $cityId = $this->input->post('city');
        $districtId = $this->input->post('district');
        $projectId = (int) $this->input->post('projectId');

        $adminComment = $this->input->post('adminComment');

        $this->db->delete('tblprojectdetails', array('projectId' => $projectId));

        $today = date('Y-m-d H:i:s');

        $this->load->model('setting_model');
        $address_row = $this->setting_model->getFullAdrress($provinceId, $cityId, $districtId);
        $address = $address_row->cityName . "," . $address_row->districtName . "," . $address_row->provName;
        $latlongval = $this->getLnt($address);
        if ($latlongval != '') {
            $latitude = $latlongval['lat'];
            $longitude = $latlongval['lng'];
        } else {
            $latitude = '';
            $longitude = '';
        }


        $prj_arr = array('provinceId' => $provinceId, 'cityId' => $cityId, 'districtId' => $districtId, 'adminComment' => $adminComment, 'latitude' => $latitude, 'longitude' => $longitude, 'modifiedDate' => $today);
        $this->db->where('projectId', $projectId);
        $this->db->update('tbluserprojects', $prj_arr);

        $questArr = explode(",", $questIds);
        for ($k = 0; $k < count($questArr); $k++) {
            $optionIds = $this->input->post('question_' . $questArr[$k]);
            $questType = $this->input->post('qustType_' . $questArr[$k]);
            $otherOptionId = $this->input->post('otherOptionId_' . $questArr[$k]);
            $optionVal = $this->input->post('serviceOtherText_' . $questArr[$k]);
            $optionType = $this->input->post('optionType_' . $questArr[$k]);
            if ($questType == 'select' || $questType == 'checkbox' || $questType == 'radio') {
                if (is_array($optionIds)) {
                    for ($i = 0; $i < count($optionIds); $i++) {
                        if ($optionIds[$i] != '') {
                            $dt_data_array = array();
                            $dt_data_array['projectId'] = $projectId;
                            $dt_data_array['questionId'] = $questArr[$k];
                            $dt_data_array['optionId'] = $optionIds[$i];
                            if ($otherOptionId == $optionIds[$i]) {
                                $dt_data_array['optionVal'] = $optionVal;
                            } else {
                                $dt_data_array['optionVal'] = '';
                            }
                            if ($optionType == 'miles') {
                                if ($otherOptionId == $optionIds[$i]) {
                                    $extra_arr = array();
                                    $extra_arr["miles"] = $this->input->post('miles_input_' . $optionIds[$i]);
                                    $dt_data_array['extras'] = json_encode($extra_arr);
                                }
                            }
                            if ($optionType == 'datetime_hours') {
                                $extra_arr = array();
                                $extra_arr["date"] = $this->input->post('date_input_' . $questArr[$k]);
                                $extra_arr["time"] = $this->input->post('time_input_' . $questArr[$k]);
                                $extra_arr["hours"] = $this->input->post('hours_input_' . $questArr[$k]);
                                $dt_data_array['extras'] = json_encode($extra_arr);
                            }
                            $this->db->insert('tblprojectdetails', $dt_data_array);
                        }
                    }
                } else {
                    $dt_data_array = array();
                    $dt_data_array['projectId'] = $projectId;
                    $dt_data_array['questionId'] = $questArr[$k];
                    $dt_data_array['optionId'] = $optionIds;
                    if ($otherOptionId == $optionIds) {
                        $dt_data_array['optionVal'] = $optionVal;
                    } else {
                        $dt_data_array['optionVal'] = '';
                    }
                    if ($optionType == 'miles') {
                        if ($otherOptionId == $optionIds) {
                            $extra_arr = array();
                            $extra_arr["miles"] = $this->input->post('miles_input_' . $optionIds);
                            $dt_data_array['extras'] = json_encode($extra_arr);
                        }
                    }
                    if ($optionType == 'datetime_hours') {
                        $extra_arr = array();
                        $extra_arr["date"] = $this->input->post('date_input_' . $questArr[$k]);
                        $extra_arr["time"] = $this->input->post('time_input_' . $questArr[$k]);
                        $extra_arr["hours"] = $this->input->post('hours_input_' . $questArr[$k]);
                        $dt_data_array['extras'] = json_encode($extra_arr);
                    }
                    $this->db->insert('tblprojectdetails', $dt_data_array);
                }
            } else {
                if ($questType == 'address') {
                    $optVal = $address;
                } else {
                    $optVal = $optionIds;
                }
                $detail_arr = array();
                $detail_arr = array('projectId' => $projectId, 'optionId' => '', 'questionId' => $questArr[$k], 'optionVal' => $optVal);
                $this->db->insert('tblprojectdetails', $detail_arr);
            }
        }
        echo 'success';
    }

    function getLnt($address) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . ",indonesia&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[] = $result['results'][0];
        $result2[] = $result1[0]['geometry'];
        $result3[] = $result2[0]['location'];
        return $result3[0];
    }

    function showchat() {
        $this->logged_in_user();
        $data['page_title'] = 'Show Chat';
        $data['list'] = $this->admin_service_model->getChatDetails();
        $data['msg'] = $this->admin_service_model->getquoteMessage();
        $this->load->view($this->config->config['admin_folder'] . '/show_chat_details', $data);
    }

    function logged_in_user() {
        if ($this->session->userdata('admin_userId') == '') {
            redirect($this->config->config['admin_base_url']);
        }
    }

}

?>