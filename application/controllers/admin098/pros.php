<?php

class Pros extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('admin_pros_model');
        $this->load->library('session');
        $this->load->library('pagination');
        date_default_timezone_set($this->config->config['constTimeZone']);
    }

    function lists() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage Professional';
        $page = (int) $this->session->userdata('listspage');
        $page = $page > 0 ? $page : 1;
        $data['page'] = $page;
        $data['user_count'] = $this->admin_pros_model->getAllProsCount('');
        $this->load->view($this->config->config['admin_folder'] . '/pros_view', $data);
    }

    function lists_pagi() {
        $this->logged_in_user();
        $this->session->set_userdata('listspage', $this->input->post('page'));
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $data['page_title'] = 'Manage Professional';
        $start = $page * $data['per_page'];
        $data['list_count'] = $this->admin_pros_model->getAllProsCount($data['keyword']);
        $data['list_res'] = $this->admin_pros_model->getAllPros($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/pros_pagi_view', $data);
    }

    function add_pros() {
        $this->logged_in_user();
        $data['page_title'] = 'Add Professional';
        $this->load->model('admin_service_model');
        $data['category_list'] = $this->admin_service_model->getCategories();
        $this->load->view($this->config->config['admin_folder'] . '/set_pros_info', $data);
    }

    function edit_pros($uid) {
        $this->logged_in_user();
        $data['page_title'] = 'Edit Professional';
        $this->load->model('admin_service_model');

        $data['category_list'] = $this->admin_service_model->getCategories();
        $data['user_info'] = $this->admin_pros_model->getUserInfo($uid);
        $data['user_pr_info'] = $this->admin_pros_model->getUserProviderInfo($uid);
        $data['user_service'] = $this->admin_service_model->getUserServices($uid);
        $data['user_subservice'] = $this->admin_service_model->getUserSubServices($uid);

        if ($data['user_info']->categoryId != '' && $data['user_info']->categoryId != '0') {
            $data['service_list'] = $this->admin_service_model->getServicesByCategoryId($data['user_info']->categoryId);
        } else {
            $data['service_list'] = '';
        }

        /* if($data['user_service']!=''){
          for($k=0;$k<count($data['user_service']);$k++){
          $data['subservice_list']=$this->admin_service_model->getSubservicesByServiceId($data['user_service'][$k]);
          }
          }else{
          $data['subservice_list']='';
          } */
        $data['u_services_name'] = $this->admin_service_model->getUserServicenamebyId($uid);
        $service_name = array();
        $service_name = explode(",", $data['u_services_name']);
        $data['u_services_name'] = $service_name;
        $data['subservice_list'] = $this->admin_service_model->getSubServicesList();
        $this->load->view($this->config->config['admin_folder'] . '/set_pros_info', $data);
    }

    function saveProsInfo() {
        $this->logged_in_user();
        $data['firstname'] = $this->input->post('txtFirstname');
        $data['lastname'] = $this->input->post('txtLastname');
        $data['email'] = $this->input->post('txtEmail');
        $data['phone'] = $this->input->post('txtPhone');
        $data['credits'] = $this->input->post('txtCredit');
        $data['categoryId'] = $this->input->post('txtCategory');
        $userId = $this->input->post('user_id');
        $data['status'] = $this->input->post('txtActive');
        if ($data['status'] == 'Y') {
            $data['confirmationToken'] = '';
        }
        $today = date('Y-m-d H:i:s');
        if ($userId == 0) {
            $checkEmail = $this->admin_pros_model->checkEmailExists($data['email']);
            if ($checkEmail == 1) {
                $data['password'] = md5($this->input->post('txtPassword'));
                $data['userType'] = 'professional'; // user
                $data['createdDate'] = $today;
                $data['modifiedDate'] = $today;
                if ($this->input->post('services')) {
                    $data['services'] = implode(",", $this->input->post('services'));
                } else {
                    $data['services'] = '';
                }
                $this->admin_pros_model->insertUser($data);
                $userId = $this->db->insert_id();
                $cdata = array();
                $cdata['userId'] = $userId;
                $cdata['businessName'] = $this->input->post('txtBusinessName');
                $cdata['website'] = $this->input->post('txtWebsite');
                $cdata['bussinessDescription'] = $this->input->post('txtBusinessDesc');
                $cdata['bioInfo'] = $this->input->post('txtBio');
                $cdata['foundingYear'] = $this->input->post('txtYearFounded');
                $cdata['employeeCount'] = $this->input->post('txtEmpCount');
                $this->admin_pros_model->insertProvider($cdata);
                /* $servicesArr=$this->input->post('txtService');       
                  if(!empty($servicesArr)){
                  for($k=0;$k<count($servicesArr);$k++){
                  $serv_arr='';
                  $serviceText='';
                  $serv_arr=array('userId'=>$userId,'serviceId'=>$servicesArr[$k],'serviceText'=>$serviceText);
                  $this->db->insert('tbluserservices', $serv_arr);
                  }
                  } */
                if ($this->input->post('services')) {
                    $subServicesArr = $this->input->post('services');
                    if (!empty($subServicesArr)) {
                        for ($m = 0; $m < count($subServicesArr); $m++) {
                            $service_id = (int) $this->admin_pros_model->getServiceIdFromName($subServicesArr[$m]);

                            if ($service_id == 0) {
                                $service_id = (int) $this->admin_pros_model->getServiceTagIdFromName($subServicesArr[$m]);
                            }
                            if ($service_id != 0) {
                                $sserv_arr = array('userId' => $userId, 'serviceId' => $service_id, 'isSubservice' => 'Y');
                                $this->db->insert('tbluserservices', $sserv_arr);
                            }
                            // get service id from name
                            // insert that id in userservices table
                            /* $sserv_arr = '';
                              $sserviceText = '';
                              $sserv_arr = array('userId' => $userId, 'serviceId' => $subServicesArr[$m], 'isSubservice' => 'Y', 'serviceText' => $sserviceText);
                              $this->db->insert('tbluserservices', $sserv_arr); */
                        }
                    }
                }
                echo 'success';
            } else {
                echo 'already';
            }
        } else {

            $data['modifiedDate'] = $today;
            if ($this->input->post('change_password_check') == '1') {
                $data['password'] = md5($this->input->post('txtPassword'));
            }
            if ($this->input->post('services')) {
                $data['services'] = implode(",", $this->input->post('services'));
            } else {
                $data['services'] = '';
            }
            $this->admin_pros_model->updateUser($data, $userId);

            $cdata['businessName'] = $this->input->post('txtBusinessName');
            $cdata['website'] = $this->input->post('txtWebsite');
            $cdata['bussinessDescription'] = $this->input->post('txtBusinessDesc');
            $cdata['bioInfo'] = $this->input->post('txtBio');
            $cdata['foundingYear'] = $this->input->post('txtYearFounded');
            $cdata['employeeCount'] = $this->input->post('txtEmpCount');
            $this->admin_pros_model->updateProvider($cdata, $userId);
            $servicesArr = $this->input->post('services');
            $this->db->delete('tbluserservices', array('userId' => $userId));
            /* if(!empty($servicesArr)){                    
              for($k=0;$k<count($servicesArr);$k++){
              $serv_arr='';
              $serviceText='';
              $serv_arr=array('userId'=>$userId,'serviceId'=>$servicesArr[$k],'serviceText'=>$serviceText);
              $this->db->insert('tbluserservices', $serv_arr);
              }
              } */

            if ($this->input->post('services')) {
                $subServicesArr = $this->input->post('services');


                if (!empty($subServicesArr)) {
                    for ($m = 0; $m < count($subServicesArr); $m++) {

                        $service_id = (int) $this->admin_pros_model->getServiceIdFromName($subServicesArr[$m]);

                        if ($service_id == 0) {
                            $service_id = (int) $this->admin_pros_model->getServiceTagIdFromName($subServicesArr[$m]);
                        }
                        if ($service_id == 0) {
                            $sserv_arr = array('userId' => $userId, 'serviceId' => $service_id, 'isSubservice' => 'Y');
                            $table_name = 'tbluserservices';
                            $no_of_rows = $this->admin_pros_model->getNoOfRowsFromUserservice($table_name, $userId, $service_id);

                            if ($no_of_rows == 0) {
                                $this->db->insert('tbluserservices', $sserv_arr);
                            } else {
                                
                            }
                        }
                        // get service id from name
                        // this service id, userid is present or not in userservices table
                        // if not 
                        // insert that id in userservices table

                        /* $sserv_arr = '';
                          $sserviceText = '';
                          $sserv_arr = array('userId' => $userId, 'serviceId' => $subServicesArr[$m], 'isSubservice' => 'Y', 'serviceText' => $sserviceText);
                          $this->db->insert('tbluserservices', $sserv_arr); */
                    }
                }
            }
            echo 'success';
        }
    }

    function export($keyword = '') {
        //$keyword=$this->input->post('keyword');
        $this->load->dbutil();
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $query = $this->admin_pros_model->getExportAllPros($keyword);
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        $filename = 'user_export_' . time() . '.csv';
        force_download($filename, $data);
    }

    function deletePros() {
        $this->logged_in_user();
        $deleted_date= date('Y-m-d H:i:s');
        $uid = $this->input->post('userId');
        $this->db->update('tbluser', array('status' => 'deleted','deletedAccountDate'=>$deleted_date), array('userId' => $uid));
        $this->db->delete('tbluserprovider', array('userId' => $uid));
        $this->db->update('tbluserprojects', array('status' => 'Expired'), array('userId' => $uid));
        $this->db->update('tblquoterequest', array('status' => 'Expired'), array('prosId' => $uid));
        echo 'success';
    }

    function newlist() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage New Professional';
        $page = (int) $this->session->userdata('newlistpage');
        $page = $page > 0 ? $page : 1;
        $data['page'] = $page;
        $data['user_count'] = $this->admin_pros_model->getAllNewProsCount('');
        $this->load->view($this->config->config['admin_folder'] . '/new_pros_view', $data);
    }

    function newlist_pagi() {
        $this->logged_in_user();
        $this->session->set_userdata('newlistpage', $this->input->post('page'));
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $data['page_title'] = 'Manage New Professional';
        $start = $page * $data['per_page'];
        $data['list_count'] = $this->admin_pros_model->getAllNewProsCount($data['keyword']);
        $data['list_res'] = $this->admin_pros_model->getAllNewPros($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/new_pros_pagi_view', $data);
    }

    function byte_convert($size) {
        # size smaller then 1kb
        if ($size < 1024)
            return $size . '===Byte';
        # size smaller then 1mb
        if ($size < 1048576)
            return sprintf("%4.2f===KB", $size / 1024);
        # size smaller then 1gb
        if ($size < 1073741824)
            return sprintf("%4.2f===MB", $size / 1048576);
        # size smaller then 1tb
        if ($size < 1099511627776)
            return sprintf("%4.2f===GB", $size / 1073741824);
        # size larger then 1tb
        else
            return sprintf("%4.2f===TB", $size / 1073741824);
    }

    function logged_in_user() {
        if ($this->session->userdata('admin_userId') == '') {
            redirect($this->config->config['admin_base_url']);
        }
    }

}

?>