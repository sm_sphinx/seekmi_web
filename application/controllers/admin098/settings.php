<?php

class Settings extends CI_Controller
{
        function __construct()
        {
            parent::__construct();    
            $this->load->helper('url');
	    $this->load->database();
	    $this->load->model('user_model');
	    $this->load->library('session');
	    $this->load->library('pagination');   
            date_default_timezone_set($this->config->config['constTimeZone']);
        }      
	
	function my_profile()
	{	
            $this->logged_in_user();
            $data['page_title'] = 'My Profile';
            $userId = (int)$this->session->userdata('admin_userId');
            $data['user_info'] = $this->user_model->getAdminUserInfo($userId);
            $data['role_list']=$this->user_model->getAdminRoleList();
            $this->load->view($this->config->config['admin_folder'].'/my_profile_view',$data);
	}
        
        function saveAdminInfo()
        {        
            $this->logged_in_user();		
            $data['firstName'] = $this->input->post('txtFirstname');
            $data['lastName'] = $this->input->post('txtLastname'); 
            $data['email'] = $this->input->post('txtEmail'); 
            $data['phone'] = $this->input->post('txtPhone');
            $data['roleId'] = $this->input->post('txtRole');
            $userId = $this->input->post('userId');           
            $today = date('Y-m-d H:i:s');
            $data['modifiedDate'] = $today;           
            $this->db->update('tbladminuser', $data, array('userid'=> $userId));	                
            echo 'success';           
        }
        
        function check_login_email_exists()
        {
           $email=$this->input->get('txtEmail');  
           $user_info=$this->user_model->getAdminUserInfo($this->session->userdata('admin_userId'));           
           if($user_info->email==$email)
           {
               echo 'true'; 
           }
           else
           {               
               $checkValid=$this->user_model->checkAdminEmailExists($email);
               if($checkValid==1)
               {
                 echo 'true';
               }
               else
               {
                 echo 'false';
               }
           }
        }
		
	function change_password()
	{
            $oldpassword = $this->input->post('oPassword');			
            $newpassword = $this->input->post('nPassword');						
            $pdata['cPassword']=$oldpassword;
            $pdata['nPassword']=$newpassword;
            $userId = $this->input->post('userId');
            $check_pwd=$this->user_model->check_admin_old_password($userId, $pdata['cPassword']);	
            if($check_pwd > 0)
            {
                $data = array('password' => md5($pdata['nPassword']));
                $this->db->update('tbladminuser', $data, array('userid'=> $userId));	
                echo 'success';
            }
            else
            {
                echo 'wrong_old';
            }		 
	}
        
        function credit_log() {
            $this->logged_in_user();
            $data['page_title'] = 'Credit Log';
            $page = (int) $this->session->userdata('creditlogpage');
            $page = $page > 0 ? $page : 1;
            $data['page'] = $page;
            $this->load->view($this->config->config['admin_folder'] . '/credit_log_view', $data);
        }

        function credit_log_pagi() {
            $this->logged_in_user();
            $this->session->set_userdata('creditlogpage', $this->input->post('page'));
            $userId = (int) $this->session->userdata('admin_userId');
            $data['data_page'] = $this->input->post('page');
            $data['data_limit'] = $this->input->post('limit');
            $data['keyword'] = $this->input->post('keyword');
            $page = $data['data_page'];
            $data['cur_page'] = $page;
            $page -= 1;
            $data['per_page'] = $data['data_limit'];
            $data['previous_btn'] = true;
            $data['next_btn'] = true;
            $data['first_btn'] = true;
            $data['last_btn'] = true;
            $start = $page * $data['per_page'];
            $data['page_title'] = 'Credit Log';
            $this->load->model('admin_service_model');
            $data['list_count'] = $this->admin_service_model->getAllCreditLogCount($data['keyword']);
            $data['list_res'] = $this->admin_service_model->getAllCreditLog($data['per_page'], $start, $data['keyword']);
            $this->load->view($this->config->config['admin_folder'] . '/pagi/credit_log_pagi_view', $data);
        }
		
	function byte_convert($size) 
	{
            # size smaller then 1kb
            if ($size < 1024) return $size . '===Byte';
            # size smaller then 1mb
            if ($size < 1048576) return sprintf("%4.2f===KB", $size/1024);
            # size smaller then 1gb
            if ($size < 1073741824) return sprintf("%4.2f===MB", $size/1048576);
            # size smaller then 1tb
            if ($size < 1099511627776) return sprintf("%4.2f===GB", $size/1073741824);
            # size larger then 1tb
            else return sprintf("%4.2f===TB", $size/1073741824);
	}
	
	function logged_in_user()
	{
            if($this->session->userdata('admin_userId')=='')
            {
                 redirect($this->config->config['admin_base_url']);
            }
	}	
}
?>