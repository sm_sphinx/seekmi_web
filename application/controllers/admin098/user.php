<?php

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('admin_user_model');
        $this->load->library('session');
        $this->load->library('pagination');
        date_default_timezone_set($this->config->config['constTimeZone']);
    }

    function index() {
        if ($this->session->userdata('admin_is_logged_in') == true) {
            $data['page_title'] = 'Dashboard';
            $data['customer_ttl_count'] = $this->admin_user_model->getTotalCustomerCount();
            $data['active_customer_ttl_count'] = $this->admin_user_model->getTotalActiveCustomerCount();
            $data['inactive_customer_ttl_count'] = $this->admin_user_model->getTotalInactiveCustomerCount();
            $today = date('Y-m-d');
            $yesterday = date('Y-m-d', strtotime("-1 days"));
            $today_cnt = $this->admin_user_model->getCustomerCountForDate($today);
            $data['customer_signup_today_cnt'] = $today_cnt;
            $yesterday_cnt = $this->admin_user_model->getCustomerCountForDate($yesterday);
            if ($yesterday_cnt == 0) {
                $data['register_today_percentage'] = -100;
            } else {
                $data['register_today_percentage'] = (( $today_cnt - $yesterday_cnt ) / $yesterday_cnt ) * 100;
            }

            $this_week_count = $this->admin_user_model->getCustomerCountWeek();
            $data['customer_signup_week_cnt'] = $this_week_count;
            $last_week_date = date('Y-m-d', strtotime("-7 days"));
            $last_last_week_date = date('Y-m-d', strtotime("-14 days"));
            $last_week_count = $this->admin_user_model->getCustomerCountBetweenWeek($last_week_date, $last_last_week_date);
            if ($last_week_count == 0) {
                $data['register_week_percentage'] = -100;
            } else {
                $data['register_week_percentage'] = (( $this_week_count - $last_week_count ) / $last_week_count ) * 100;
            }

            $last_month_count = $this->admin_user_model->getCustomerCountMonth();
            $this_month_count = $this->admin_user_model->getCustomerCountThisMonth();
            $data['customer_signup_month_cnt'] = $this_month_count;
            if ($last_month_count == 0) {
                $data['register_month_percentage'] = -100;
            } else {
                $data['register_month_percentage'] = (( $this_month_count - $last_month_count ) / $last_month_count ) * 100;
            }

            $this->load->model('admin_pros_model');
            $data['pros_ttl_count'] = $this->admin_pros_model->getTotalProsCount();
            $data['active_pros_ttl_count'] = $this->admin_pros_model->getTotalActiveProsCount();
            $data['inactive_pros_ttl_count'] = $this->admin_pros_model->getTotalInactiveProsCount();
            $today = date('Y-m-d');
            $yesterday = date('Y-m-d', strtotime("-1 days"));
            $today_cnt = $this->admin_pros_model->getSignupProfetionalCountForDate($today);
            $data['pros_signup_today_cnt'] = $today_cnt;
            $yesterday_cnt = $this->admin_pros_model->getSignupProfetionalCountForDate($yesterday);
            if ($yesterday_cnt == 0) {
                $data['register_today_percentage_prof'] = -100;
            } else {
                $data['register_today_percentage_prof'] = (( $today_cnt - $yesterday_cnt ) / $yesterday_cnt ) * 100;
            }

            $this_week_count = $this->admin_pros_model->getSignupProfetionalCountWeek();
            $data['pros_signup_week_cnt'] = $this_week_count;
            $last_week_date = date('Y-m-d', strtotime("-7 days"));
            $last_last_week_date = date('Y-m-d', strtotime("-14 days"));
            $last_week_count = $this->admin_pros_model->getSignupProfetionalCountBetweenWeek($last_week_date, $last_last_week_date);
            if ($last_week_count == 0) {
                $data['register_week_percentage_prof'] = -100;
            } else {
                $data['register_week_percentage_prof'] = (( $this_week_count - $last_week_count ) / $last_week_count ) * 100;
            }


            $last_month_count = $this->admin_pros_model->getSignupProfetionalCountMonth();
            $this_month_count = $this->admin_pros_model->getSignupProfetionalCountThisMonth();
            $data['pros_signup_month_cnt'] = $this_month_count;
            if ($last_month_count == 0) {
                $data['register_month_percentage_prof'] = -100;
            } else {
                $data['register_month_percentage_prof'] = (( $this_month_count - $last_month_count ) / $last_month_count ) * 100;
            }

            //For Register count
            //for today
            $today_cnt = $this->admin_pros_model->getProfessionalActiveCountForDate($today);
            $yesterday_cnt = $this->admin_pros_model->getProfessionalActiveCountForDate($yesterday);
            $data['pros_active_today_cnt'] = $today_cnt;
            if ($yesterday_cnt == 0) {
                $data['profetional_active_today_percentage'] = -100;
            } else {
                $data['profetional_active_today_percentage'] = (( $today_cnt - $yesterday_cnt ) / $yesterday_cnt ) * 100;
            }

            //for week
            $this_week_count = $this->admin_pros_model->getProfetionalActiveCountWeek();
            $last_week_date = date('Y-m-d', strtotime("-7 days"));
            $last_last_week_date = date('Y-m-d', strtotime("-14 days"));
            $last_week_count = $this->admin_pros_model->getProfetionalActiveCountBetweenWeek($last_week_date, $last_last_week_date);
            $data['pros_active_week_cnt'] = $this_week_count;
            if ($last_week_count == 0) {
                $data['profetional_active_week_percentage_prof'] = -100;
            } else {
                $data['profetional_active_week_percentage_prof'] = (( $this_week_count - $last_week_count ) / $last_week_count ) * 100;
            }

            //for month
            $last_month_count = $this->admin_pros_model->getProfetionalActiveCountMonth();
            $this_month_count = $this->admin_pros_model->getProfetionalActiveCountThisMonth();
            $data['pros_active_month_cnt'] = $this_month_count;
            if ($last_month_count == 0) {
                $data['profetional_active_month_percentage_prof'] = -100;
            } else {
                $data['profetional_active_month_percentage_prof'] = (( $this_month_count - $last_month_count ) / $last_month_count ) * 100;
            }


            //For Job request Pending
            //for today
            $today_cnt = $this->admin_pros_model->getProfessionalJobPendingCountForDate($today);
            $yesterday_cnt = $this->admin_pros_model->getProfessionalJobPendingCountForDate($yesterday);
            $data['job_pending_today_cnt']=$today_cnt;
            if ($yesterday_cnt == 0) {
                $data['profetional_job_pending_today_percentage'] = -100;
            } else {
                $data['profetional_job_pending_today_percentage'] = (( $today_cnt - $yesterday_cnt ) / $yesterday_cnt ) * 100;
            }

            //For Job request Expired
            //for today
            $today_cnt = $this->admin_pros_model->getProfessionalJobExpiredCountForDate($today);
            $yesterday_cnt = $this->admin_pros_model->getProfessionalJobExpiredCountForDate($yesterday);
            $data['job_expired_today_cnt']=$today_cnt;
            if ($yesterday_cnt == 0) {
                $data['profetional_job_expired_today_percentage'] = -100;
            } else {
                $data['profetional_job_expired_today_percentage'] = (( $today_cnt - $yesterday_cnt ) / $yesterday_cnt ) * 100;
            }

            //For Job request Completed
            //for today
            $today_cnt = $this->admin_pros_model->getProfessionalJobCompletedCountForDate($today);
            $yesterday_cnt = $this->admin_pros_model->getProfessionalJobCompletedCountForDate($yesterday);
            $data['job_completed_today_cnt']=$today_cnt;
            if ($yesterday_cnt == 0) {
                $data['profetional_job_completed_today_percentage'] = -100;
            } else {
                $data['profetional_job_completed_today_percentage'] = (( $today_cnt - $yesterday_cnt ) / $yesterday_cnt ) * 100;
            }

            //for week
            $this_week_count = $this->admin_pros_model->getProfetionalJobCompletedCountWeek();
            $last_week_date = date('Y-m-d', strtotime("-7 days"));
            $last_last_week_date = date('Y-m-d', strtotime("-14 days"));
            $last_week_count = $this->admin_pros_model->getProfetionalJobCompletedCountBetweenWeek($last_week_date, $last_last_week_date);
            $data['job_completed_week_cnt']=$this_week_count;
            if ($last_week_count == 0) {
                $data['profetional_job_completed_week_percentage_prof'] = -100;
            } else {
                $data['profetional_job_completed_week_percentage_prof'] = (( $this_week_count - $last_week_count ) / $last_week_count ) * 100;
            }


            //for month
            $last_month_count = $this->admin_pros_model->getProfetionalJobCompletedCountMonth();
            $this_month_count = $this->admin_pros_model->getProfetionalJobCompletedCountThisMonth();
            $data['job_completed_month_cnt']=$this_month_count;
            if ($last_month_count == 0) {
                $data['profetional_job_completed_month_percentage_prof'] = -100;
            } else {
                $data['profetional_job_completed_month_percentage_prof'] = (( $this_month_count - $last_month_count ) / $last_month_count ) * 100;
            }


            //For Customer Deleted
            //for today
            $today_cnt = $this->admin_user_model->getCustomerDeletedCountForDate($today);
            $data['customer_deleted_today_cnt']=$today_cnt;
            $yesterday_cnt = $this->admin_user_model->getCustomerDeletedCountForDate($yesterday);
            if ($yesterday_cnt == 0) {
                $data['customer_deleted_today_percentage'] = -100;
            } else {
                $data['customer_deleted_today_percentage'] = (( $today_cnt - $yesterday_cnt ) / $yesterday_cnt ) * 100;
            }

            //for week
            $this_week_count = $this->admin_user_model->getCustomerDeletedWeek();
            $data['customer_deleted_week_cnt']=$this_week_count;
            $last_week_date = date('Y-m-d', strtotime("-7 days"));
            $last_last_week_date = date('Y-m-d', strtotime("-14 days"));
            $last_week_count = $this->admin_user_model->getCustomerDeletedCountBetweenWeek($last_week_date, $last_last_week_date);
            if ($last_week_count == 0) {
                $data['customer_deleted_week_percentage_prof'] = -100;
            } else {
                $data['customer_deleted_week_percentage_prof'] = (( $this_week_count - $last_week_count ) / $last_week_count ) * 100;
            }


            //for month
            $last_month_count = $this->admin_user_model->getCustomerDeletedCountMonth();
            $this_month_count = $this->admin_user_model->getCustomerDeletedCountThisMonth();
            $data['customer_deleted_month_cnt']=$this_month_count;

            if ($last_month_count == 0) {
                $data['customer_deleted_month_percentage'] = -100;
            } else {
                $data['customer_deleted_month_percentage'] = (( $this_month_count - $last_month_count ) / $last_month_count ) * 100;
            }

            //for job 20 highest request
            $data['job20fullfill'] = $this->admin_user_model->getJob20Fullfill();
            $data['job20category'] = $this->admin_user_model->getJob20Categories();


            //for top 10 highest  Service offered
            $data['top10service'] = $this->admin_pros_model->getTop10serviceoffered();
            $this->load->view($this->config->config['admin_folder'] . '/dashboard_view', $data);
        } else {
            $data['page_title'] = 'Login Page';
            $this->load->view($this->config->config['admin_folder'] . '/login_view', $data);
        }
    }

    function my_account() {
        $this->logged_in_user();
        $data['page_title'] = 'My Account';
        $userId = (int) $this->session->userdata('admin_userId');
        $data['user_info'] = $this->admin_user_model->getAdminInfo($userId);
        $this->load->view($this->config->config['admin_folder'] . '/my_profile_view', $data);
    }

    function users() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage Users';
        $page = (int) $this->session->userdata('page');
        $page = $page > 0 ? $page : 1;
        $data['page'] = $page;
        $data['user_count'] = $this->admin_user_model->getAllUserCount('');
        $this->load->view($this->config->config['admin_folder'] . '/users_view', $data);
    }

    function users_pagi() {
        $this->logged_in_user();
        $this->session->set_userdata('page', $this->input->post('page'));
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $data['page_title'] = 'Manage Users';
        $start = $page * $data['per_page'];
        $data['list_count'] = $this->admin_user_model->getAllUserCount($data['keyword']);
        $data['list_res'] = $this->admin_user_model->getAllUser($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/users_pagi_view', $data);
    }

    function add_user() {
        $this->logged_in_user();
        $data['page_title'] = 'Add User';
        $this->load->view($this->config->config['admin_folder'] . '/set_user_info', $data);
    }

    function edit_user($uid) {
        $this->logged_in_user();
        $data['page_title'] = 'Edit User';
        $data['user_info'] = $this->admin_user_model->getUserInfo($uid);
        $this->load->view($this->config->config['admin_folder'] . '/set_user_info', $data);
    }

    function saveUserInfo() {
        $this->logged_in_user();
        $data['firstname'] = $this->input->post('txtFirstname');
        $data['lastname'] = $this->input->post('txtLastname');
        $data['email'] = $this->input->post('txtEmail');
        $data['phone'] = $this->input->post('txtPhone');
        $userId = $this->input->post('user_id');
        $data['status'] = $this->input->post('txtActive');
        if ($data['status'] == 'Y') {
            $data['confirmationToken'] = '';
        }
        $today = date('Y-m-d H:i:s');
        if ($userId == 0) {
            $checkEmail = $this->admin_user_model->checkEmailExists($data['email']);
            if ($checkEmail == 1) {
                $data['password'] = md5($this->input->post('txtPassword'));
                $data['userType'] = 'user';
                $data['createdDate'] = $today;
                $data['modifiedDate'] = $today;
                $this->admin_user_model->insertUser($data);
                echo 'success';
            } else {
                echo 'already';
            }
        } else {
            $data['modifiedDate'] = $today;
            if ($this->input->post('change_password_check') == '1') {
                $data['password'] = md5($this->input->post('txtPassword'));
            }
            $this->admin_user_model->updateUser($data, $userId);
            echo 'success';
        }
    }

    function export($keyword = '') {
        //$keyword=$this->input->post('keyword');
        $this->load->dbutil();
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $query = $this->admin_user_model->getExportAllUsers($keyword);
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        $filename = 'user_export_' . time() . '.csv';
        force_download($filename, $data);
    }

    function set_user_password() {
        $this->logged_in_user();
        $password = trim($this->input->post('nPassword'));
        $data['password'] = md5($password);
        $userId = $this->input->post('userId');
        $this->admin_user_model->updateUser($data, $userId);
        echo 'success';
    }

    function login_submit() {
        $pdata['useremail'] = $this->input->post('email');
        $pdata['userpass'] = $this->input->post('password');
        $row = $this->admin_user_model->check_user_login($pdata);
        //echo "<pre>"; print_r($row);
        if ($row->status == 'Y') {
            $right_res = $this->admin_user_model->getRoleRights($row->roleId);
            $right_arr = array();
            if (!empty($right_res)) {
                foreach ($right_res as $right_row) {
                    $right_arr[] = $right_row->menuCode;
                }
            }
            $this->session->set_userdata('admin_user_email', $row->email);
            $this->session->set_userdata('admin_userId', $row->userid);
            $this->session->set_userdata('admin_user_type', $row->roleName);
            $this->session->set_userdata('user_role_id', $row->roleId);
            $this->session->set_userdata('user_rights', $right_arr);
            $this->session->set_userdata('admin_is_logged_in', true);
            echo "success";
        } elseif ($row->status == 'N') {
            echo "inactive";
        } else {
            echo "failed";
        }
    }

    function deleteUser() {
        $this->logged_in_user();
        $uid = $this->input->post('userId');
        $deleted_date = date('Y-m-d H:i:s');
        $this->db->update('tbluser', array('status' => 'deleted', 'deletedAccountDate' => $deleted_date), array('userId' => $uid));
        $this->db->update('tbluserprojects', array('status' => 'Expired'), array('userId' => $uid));
        $this->db->update('tblquoterequest', array('status' => 'Expired'), array('prosId' => $uid));
        $this->db->where('roleId', $uid);
        $this->db->delete('tblrole');
        $this->db->where('roleId', $uid);
        $this->db->delete('tblrolerights');
        $this->db->update('tbladminuser', array('roleId' => '0'), array('roleId' => $uid));
        $this->db->update('tbladminuser', array('status' => 'deleted'), array('userid' => $uid));
        echo 'success';
    }

    function admin_users() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage Categories';
        $page = (int) $this->session->userdata('adminpage');
        $page = $page > 0 ? $page : 1;
        $data['page'] = $page;
        $data['page_title'] = 'Manage Admin Users';
        $data['user_count'] = $this->admin_user_model->getAllAdminUserCount('');
        $this->load->view($this->config->config['admin_folder'] . '/admin_users_view', $data);
    }

    function admin_users_pagi() {
        $this->logged_in_user();
        $this->session->set_userdata('adminpage', $this->input->post('page'));
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $data['page_title'] = 'Manage Admin  Users';
        $start = $page * $data['per_page'];
        $data['list_count'] = $this->admin_user_model->getAllAdminUserCount($data['keyword']);
        $data['list_res'] = $this->admin_user_model->getAllAdminUser($data['per_page'], $start, $data['keyword']);

        $this->load->view($this->config->config['admin_folder'] . '/pagi/admin_users_pagi_view', $data);
    }

    function add_admin_user() {
        $this->logged_in_user();
        $data['page_title'] = 'Add User';
        $data['role_list'] = $this->admin_user_model->getAdminRoleList();
        $this->load->view($this->config->config['admin_folder'] . '/set_admin_user_info', $data);
    }

    function edit_admin_user($uid) {
        $this->logged_in_user();
        $data['page_title'] = 'Edit Admin User';
        $data['user_info'] = $this->admin_user_model->getAdminUserInfo($uid);
        $data['role_list'] = $this->admin_user_model->getAdminRoleList();
        $this->load->view($this->config->config['admin_folder'] . '/set_admin_user_info', $data);
    }

    function saveAdminUserInfo() {
        $this->logged_in_user();
        $data['firstName'] = $this->input->post('txtFirstname');
        $data['lastName'] = $this->input->post('txtLastname');
        $data['email'] = $this->input->post('txtEmail');
        $data['phone'] = $this->input->post('txtPhone');
        $data['roleId'] = $this->input->post('txtRole');
        $userId = $this->input->post('user_id');
        $data['status'] = $this->input->post('txtActive');
        $today = date('Y-m-d H:i:s');
        if ($userId == 0) {
            $checkEmail = $this->admin_user_model->checkAdminEmailExists($data['email']);
            if ($checkEmail == 1) {
                $data['password'] = md5($this->input->post('txtPassword'));
                $data['createdDate'] = $today;
                $data['modifiedDate'] = $today;
                $this->db->insert('tbladminuser', $data);
                echo 'success';
            } else {
                echo 'already';
            }
        } else {
            $data['modifiedDate'] = $today;
            if ($this->input->post('change_password_check') == '1') {
                $data['password'] = md5($this->input->post('txtPassword'));
            }
            $this->db->update('tbladminuser', $data, array('userid' => $userId));
            echo 'success';
        }
    }

    function general_setting() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage General Setting';
        $data['user_count'] = $this->admin_user_model->getAllsettingsCount('');
        $this->load->view($this->config->config['admin_folder'] . '/settings_view', $data);
    }

    function setting_pagi() {
        $this->logged_in_user();
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $data['page_title'] = 'Manage General Setting';
        $start = $page * $data['per_page'];
        $data['list_count'] = $this->admin_user_model->getAllsettingsCount($data['keyword']);
        $data['list_res'] = $this->admin_user_model->getAllSetting($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/setting_pagi_view', $data);
    }

    function edit_settings($id) {
        $this->logged_in_user();
        $data['page_title'] = 'Edit General Setting';
        $data['role_info'] = $this->admin_user_model->getSettingsInfo($id);
        $this->load->view($this->config->config['admin_folder'] . '/set_settings_info', $data);
    }

    function saveSettingInfo() {
        $this->logged_in_user();
        $data['settingName'] = $this->input->post('txtRolename');
        if (empty($this->input->post('txtsettingVal'))) {
            $hid_image = $this->input->post('hid_image');
            if ($_FILES['image_file']['name'] != '') {

                $config['upload_path'] = './setting_images/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '0';
                $config['remove_spaces'] = TRUE;
                $salt = str_replace(".", '_', uniqid(mt_rand(), true));
                $fileName_ext = explode('.', $_FILES['image_file']['name']);
                $rename = $salt . '_.' . end($fileName_ext);
                $config['file_name'] = $rename;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('image_file')) {
                    $rdata['error'] = $this->upload->display_errors();
                    $isUpload = 0;
                } else {
                    $featuredImage = $rename;
                    if ($hid_image != '') {
                        $imageURL_large = './setting_images/' . $hid_image;

                        if (file_exists($imageURL_large)) {
                            unlink($imageURL_large);
                        }
                    }
                }
            } else {
                $featuredImage = $hid_image;
            }

            $data['settingVal'] = $featuredImage;
        } else {
            $data['settingVal'] = $this->input->post('txtsettingVal');
        }
        $roleId = $this->input->post('role_id');
        $this->db->update('tblsettings', $data, array('settingId' => $roleId));
        redirect($this->config->config['admin_base_url'] . '/user/general_setting');
    }

    function roles() {
        $this->logged_in_user();
        $data['page_title'] = 'Manage Roles';
        $data['user_count'] = $this->admin_user_model->getAllRolesCount('');
        $this->load->view($this->config->config['admin_folder'] . '/roles_view', $data);
    }

    function roles_pagi() {
        $this->logged_in_user();
        $userId = (int) $this->session->userdata('admin_userId');
        $data['data_page'] = $this->input->post('page');
        $data['data_limit'] = $this->input->post('limit');
        $data['keyword'] = $this->input->post('keyword');
        $page = $data['data_page'];
        $data['cur_page'] = $page;
        $page -= 1;
        $data['per_page'] = $data['data_limit'];
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $data['page_title'] = 'Manage Roles';
        $start = $page * $data['per_page'];
        $data['list_count'] = $this->admin_user_model->getAllRolesCount($data['keyword']);
        $data['list_res'] = $this->admin_user_model->getAllRoles($data['per_page'], $start, $data['keyword']);
        $this->load->view($this->config->config['admin_folder'] . '/pagi/roles_pagi_view', $data);
    }

    function add_role() {
        $this->logged_in_user();
        $data['page_title'] = 'Add Role';
        $this->load->view($this->config->config['admin_folder'] . '/set_role_info', $data);
    }

    function edit_role($id) {
        $this->logged_in_user();
        $data['page_title'] = 'Edit Role';
        $data['role_info'] = $this->admin_user_model->getRoleInfo($id);
        $data['role_rights'] = $this->admin_user_model->getRoleRights($id);
        $this->load->view($this->config->config['admin_folder'] . '/set_role_info', $data);
    }

    function saveRoleInfo() {
        $this->logged_in_user();
        $data['roleName'] = $this->input->post('txtRolename');
        $roleId = $this->input->post('role_id');
        $data['status'] = $this->input->post('txtActive');
        $role_rights_arr = $this->input->post('txtMenucode');
        $today = date('Y-m-d H:i:s');
        if ($roleId == 0) {
            $checkRole = $this->admin_user_model->checkRoleExists($data['roleName']);
            if ($checkRole == 1) {
                $data['createdDate'] = $today;
                $data['modifiedDate'] = $today;
                $this->db->insert('tblrole', $data);
                $roleId = $this->db->insert_id();
                if (!empty($role_rights_arr)) {
                    for ($k = 0; $k < count($role_rights_arr); $k++) {
                        $this->db->insert('tblrolerights', array('roleId' => $roleId, 'menuCode' => $role_rights_arr[$k]));
                    }
                }
                echo 'success';
            } else {
                echo 'already';
            }
        } else {
            $data['modifiedDate'] = $today;
            $this->db->update('tblrole', $data, array('roleId' => $roleId));
            $this->db->delete('tblrolerights', array('roleId' => $roleId));
            if (!empty($role_rights_arr)) {
                for ($k = 0; $k < count($role_rights_arr); $k++) {
                    $this->db->insert('tblrolerights', array('roleId' => $roleId, 'menuCode' => $role_rights_arr[$k]));
                }
            }
            echo 'success';
        }
    }

    function login() {
        $data['page_title'] = 'Login';
        $this->load->view($this->config->config['admin_folder'] . '/login_view', $data);
    }

    function logout() {
        $usertype = $this->session->userdata('admin_user_type');
        $sess_items = array('admin_user_email' => '', 'admin_userId' => '', 'admin_is_logged_in' => false, 'admin_user_type' => '', 'user_role_id' => '', 'user_rights' => '', 'page' => '');
        $this->session->unset_userdata($sess_items);
        //$this->session->sess_destroy();
        redirect('/' . $this->config->config['admin_folder']);
    }

    function change_password() {
        $oldpassword = $this->input->post('oPassword');
        $newpassword = $this->input->post('nPassword');
        $pdata['cPassword'] = $oldpassword;
        $pdata['nPassword'] = $newpassword;
        $userId = $this->input->post('userId');
        $check_pwd = $this->admin_user_model->check_old_password($userId, $pdata['cPassword']);
        if ($check_pwd > 0) {
            $data = array('password' => md5($pdata['nPassword']));
            $this->admin_user_model->updateAdmin($data, $userId);
            echo 'success';
        } else {
            echo 'wrong_old';
        }
    }

    function byte_convert($size) {
        # size smaller then 1kb
        if ($size < 1024)
            return $size . '===Byte';
        # size smaller then 1mb
        if ($size < 1048576)
            return sprintf("%4.2f===KB", $size / 1024);
        # size smaller then 1gb
        if ($size < 1073741824)
            return sprintf("%4.2f===MB", $size / 1048576);
        # size smaller then 1tb
        if ($size < 1099511627776)
            return sprintf("%4.2f===GB", $size / 1073741824);
        # size larger then 1tb
        else
            return sprintf("%4.2f===TB", $size / 1073741824);
    }

    function logged_in_user() {
        if ($this->session->userdata('admin_userId') == '') {
            redirect($this->config->config['admin_base_url']);
        }
    }

    function forgot_password() {

        $email = $this->input->post('email');
        $user_row = $this->admin_user_model->checkAdminEmailExists1($email);

        if (count($user_row) > 0) {

            $userId = (int) $user_row->userid;
            $token = md5($userId . time());
            $this->db->update('tbladminuser', array('f_p_token' => $token), array('userid' => $userId));
            $activate_url = $this->config->config['admin_base_url'] . 'user/reset_password/' . $token;


            $htmlMessage = file_get_contents($this->config->config['main_base_url'] . 'email-templates/forgot-password-template.html');
            $htmlMessage = str_replace('{RESET_PASSWORD_URL}', $activate_url, $htmlMessage);
            $htmlMessage = str_replace('{FULLNAME}', $user_row->firstName . " " . $user_row->lastName, $htmlMessage);
            $apikey = 'EQ4Z41iMdCc7uiznTzzUVw';
            $this->load->library('mandrill', array($apikey));
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($apikey);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
            if ($mandrill_ready) {
                //Send us some email!
                $email = array(
                    'html' => $htmlMessage, //Consider using a view file
                    'text' => '',
                    'subject' => 'Forget Password Link',
                    'from_email' => 'support@seekmi.com',
                    'from_name' => 'Seekmi',
                    'to' => array(array('email' => $email)) //Check documentation for more details on this one                
                );

                $result = $this->mandrill->messages_send($email);
                echo("success");
                die();
            }
        } 
    }

    function reset_password($token) {
        //$token = md5($token);
        $check = $this->admin_user_model->checkFPTokenExists($token);
        if ($check->num_rows() > 0) {
            $result = $check->row();
            $data['id'] = $result->userid;
            $this->load->view($this->config->config['admin_folder'] . '/reset_password', $data);
        } else {
            
        }
    }

    function update_pass() {
        $pass = $this->input->post('pass');
        $cpass = $this->input->post('cpass');
        $id = $this->input->post('user_id');
        $data = array('password' => md5($pass));

        $this->db->where('userid', $id);
        $this->db->update('tbladminuser', $data);
    }

}

?>