<?php
class Customerlist extends CI_Controller
{
    function __construct()
    {
        parent::__construct();    
        $this->load->helper('url');
        $this->load->database();       
        $this->load->library('session');
        $this->load->library('pagination');  
        date_default_timezone_set($this->config->config['constTimeZone']);       
    }
	 
    function index($servName,$subServName='')
     {
	$data['page_title']='Sign Up';
        $data['service_name']=$servName;
        $data['subservice_name']=$subServName;
        $this->load->view('signup_customerlist_page_view',$data); 
     }
     
     function submit()
     {
        $today=date('Y-m-d H:i:s'); 
        $pdata['firstname']=$this->input->post('firstName');
        $pdata['lastname']=$this->input->post('lastName');
        $pdata['phone']=$this->input->post('phone');
        $pdata['email']=$this->input->post('usr_email');
        $services='';
        if($this->input->post('serviceName')!=''){
          $services.=ucfirst(str_replace("-"," ",$this->input->post('serviceName')));
        }
        if($this->input->post('subserviceName')!=''){
          if($services!=''){
              $services.=",";
          }
          $services.=ucfirst(str_replace("-"," ",$this->input->post('subserviceName')));
        }
        $pdata['services']=$services;        
        $pdata['signupSource']='emailBlast';
        $pdata['userType']='professional';
        $pdata['status']='N';
        $pdata['createdDate']=$today;
        $pdata['modifiedDate']=$today;
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){
             $ip=$_SERVER['HTTP_CLIENT_IP'];
           //Is it a proxy address
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
           $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
           $ip=$_SERVER['REMOTE_ADDR'];
        }
        $pdata['ip_address']=$ip;
        if($ip!=''){
           $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            $locStr='';
            if(isset($details->city)){
                $locStr.=$details->city;
            }
            if(isset($details->region)){
                if($locStr!=''){
                    $locStr.=", ";
                }
                $locStr.=$details->region;
            }
            if(isset($details->country)){
                if($locStr!=''){
                    $locStr.=", ";
                }
                $locStr.=$details->country;
            }
            $pdata['locationInfo']=$locStr;
            if(isset($details->loc)){
              $pdata['latlong']=$details->loc;
            }else{
              $pdata['latlong']='';  
            }
        }else{
           $pdata['locationInfo']='';
           $pdata['latlong']=''; 
        }
        $pdata['browserInfo']=$this->browser();
        $this->db->insert('tbluser', $pdata); 
        $userId=$this->db->insert_id();
        $cdata['userId']=$userId;              
        $this->db->insert('tbluserprovider', $cdata); 
        redirect('customerlist/thankyou/'.md5($userId));
     }
     
     function thankyou($id)
     {
         $data['page_title'] = 'Thank You';
         $data['status']='new';
         $this->load->model('pros_model');
         $user_data=$this->pros_model->getUserInfoFromMd5Id($id);
         //$this->load->view('thank_you_customerlist_page_view', $data); 
         if(!empty($user_data)){
            $data['usemail'] = $user_data->email;  
            $this->load->view('thank_you_pros_page_view', $data);               
         } else {
            redirect('/');
         }
     }
     
     /*function add_service_slug()
     {
         $this->load->model('service_model');
         $services=$this->service_model->getAllServices1();
         foreach($services as $services_row){
             echo $slug=url_title($services_row->subserviceName, 'dash', TRUE);
             $this->db->update('tblsubservices', array('serviceSlug'=>$slug),array('subserviceId'=>$services_row->subserviceId));             
         }         
     }*/
     
     function browser()
     {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $browsers = array(
                            'Chrome' => array('Google Chrome','Chrome/(.*)\s'),
                            'MSIE' => array('Internet Explorer','MSIE\s([0-9\.]*)'),
                            'Firefox' => array('Firefox', 'Firefox/([0-9\.]*)'),
                            'Safari' => array('Safari', 'Version/([0-9\.]*)'),
                            'Opera' => array('Opera', 'Version/([0-9\.]*)')
                            ); 

        $browser_details = array();

            foreach ($browsers as $browser => $browser_info){
                if (preg_match('@'.$browser.'@i', $user_agent)){
                    $browser_details['name'] = $browser_info[0];
                        preg_match('@'.$browser_info[1].'@i', $user_agent, $version);
                    $browser_details['version'] = $version[1];
                        break;
                } else {
                    $browser_details['name'] = 'Unknown';
                    $browser_details['version'] = 'Unknown';
                }
            }

        return $browser_details['name'].' Version: '.$browser_details['version'];
    }
     
     
}
?>