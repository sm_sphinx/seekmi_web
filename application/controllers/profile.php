<?php
class Profile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();    
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('profile_model');
        $this->load->library('session');
        $this->load->library('pagination');  
        date_default_timezone_set($this->config->config['constTimeZone']);       
    }
    
    function dashboard()
    {       
        if($this->session->userdata('is_logged_in')==true)
        {
         $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
         $data['page_title']='Dashboard';
         $data['userId']=$this->session->userdata('userId');         
         $this->load->model('service_model');
         $data['project_list'] = $this->service_model->getUserProjectList($this->session->userdata('userId'));
         $expertArr=array();
         if(!empty($data['project_list'])){
             foreach($data['project_list'] as $project_list){                 
                 $expert_list=$this->service_model->getProjectExperts($project_list->projectId);                 
                 if(!empty($expert_list)){
                     foreach($expert_list as $expert_row){
                         $expertArr[$project_list->projectId][]=$expert_row;
                     }
                 }else{
                     $expertArr[$project_list->projectId]='';
                 }
                 //$expertMainArr[]=$expertArr;
             }
         }else{
             $expertArr=array();
         }
         $data['expert_list']=$expertArr;
        
         $this->load->view('dashboard_page_view',$data); 
        }
        else
        {
          redirect('user/login');
        } 
    }
     
    function settings($id='')
    {       
        if($id==''){
            if($this->session->userdata('is_logged_in')==true)
            {         
             $data['page_title']='Settings';
             $data['userId']=$this->session->userdata('userId');  
             /** Information code **/
             $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId'));
             if($this->session->userdata('user_type')=='professional'){
                $data['user_address']=$this->profile_model->getUserAddress($data['userId']);
             }else{
                $data['user_address']='';
             }
             /** Information code **/
             /** Notification code **/
             $data['notification_data']= $this->profile_model->getNotifications($this->session->userdata('userId')); 
             if($data['notification_data']==''){
                $today=date('Y-m-d H:i:s');
                $notif_arr=array('userId'=>$this->session->userdata('userId'),'updates'=>'Y','tips'=>'Y','offers'=>'Y','userFeedback'=>'Y','createdDate'=>$today,'modifiedDate'=>$today);
                $this->db->insert('tblemailnotifications', $notif_arr); 
                $data['notification_data']= $this->profile_model->getNotifications($this->session->userdata('userId')); 
             }
             /** Notification code **/
             /** referral code **/
             if($data['user_data']->referralCode==''){
                $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                $res = "";
                for ($i = 0; $i < 13; $i++) {
                   $res .= $chars[mt_rand(0, strlen($chars)-1)];
                }
                $data['referralCode']=$res;                
                $this->profile_model->updateUser(array('referralCode'=>$res),$this->session->userdata('userId'));   
             }else{
                $data['referralCode']=$data['user_data']->referralCode;
             }
             /** referral code **/
             $this->load->view('setting_page_view',$data); 
            }
            else
            {
              redirect('user/login');
            } 
        }else{
            $data['page_title']='Settings';
            $row=$this->profile_model->getUserInfo($id);
            $this->session->set_userdata('user_email',$row->email);
            $this->session->set_userdata('userId',$row->userId);
            $this->session->set_userdata('user_type',$row->userType);
            $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
            $this->session->set_userdata('user_photo',$row->userPhoto);
            $this->session->set_userdata('facebook_id',$row->facebookProfileId);
            $this->session->set_userdata('is_logged_in',true);    
            $data['userId']=$this->session->userdata('userId');  
            /** Information code **/
             $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId'));
             if($this->session->userdata('user_type')=='professional'){
                $data['user_address']=$this->profile_model->getUserAddress($data['userId']);
             }else{
                $data['user_address']='';
             }
             /** Information code **/
            $this->load->view('setting_page_view',$data); 
        }
     }

    function information()
     {       
        if($this->session->userdata('is_logged_in')==true)
        {
         $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
         $data['page_title']='Information';
         $data['userId']=$this->session->userdata('userId'); 
         if($this->session->userdata('user_type')=='professional'){
             $data['user_address']=$this->profile_model->getUserAddress($data['userId']);
         }else{
             $data['user_address']='';
         }
         $this->load->view('information_page_view',$data); 
        }
        else
        {
          redirect('/');
        } 
     }
     
     function notifications()
     {       
        if($this->session->userdata('is_logged_in')==true)
        {
         $data['notification_data']= $this->profile_model->getNotifications($this->session->userdata('userId')); 
         if($data['notification_data']==''){
             $today=date('Y-m-d H:i:s');
             $notif_arr=array('userId'=>$this->session->userdata('userId'),'updates'=>'Y','tips'=>'Y','offers'=>'Y','userFeedback'=>'Y','createdDate'=>$today,'modifiedDate'=>$today);
             $this->db->insert('tblemailnotifications', $notif_arr); 
             $data['notification_data']= $this->profile_model->getNotifications($this->session->userdata('userId')); 
         }
         $data['page_title']='Notification Settings';
         $data['userId']=$this->session->userdata('userId');                  
         $this->load->view('notifications_page_view',$data); 
        }
        else
        {
          redirect('/');
        } 
     }
     
     function account_edit()
     {       
        if($this->session->userdata('is_logged_in')==true)
        {
         $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
         $data['page_title']='Edit Account';
         $data['userId']=$this->session->userdata('userId');                  
         $this->load->view('edit_account_page_view',$data); 
        }
        else
        {
          redirect('/');
        } 
     }
     
     function password($token='')
     {
        if($token!=''){
            $data['page_title']='Reset Password';
            $data['token']=$token;
            $row=$this->profile_model->getUserFromToken($token);
            if($row!=''){
                $this->session->set_userdata('user_email',$row->email);
                $this->session->set_userdata('user_full_name',$row->firstname." ".$row->lastname);
                $this->session->set_userdata('user_photo',$row->userPhoto);
                $this->session->set_userdata('userId',$row->userId);
                $this->session->set_userdata('user_type',$row->userType);
                $this->session->set_userdata('is_logged_in',true);
                $this->session->set_userdata('facebook_id',$row->facebookProfileId); 
                $data['userId']=$row->userId;        
                $data['user_data']=$row;
                $this->load->view('change_password_page_view',$data); 
            }else{
                redirect('/');
            }
        }else{
            if($this->session->userdata('is_logged_in')==true)
            {
             $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
             $data['page_title']='Change Password';
             $data['token']='';
             $data['userId']=$this->session->userdata('userId');                  
             $this->load->view('change_password_page_view',$data); 
            }
            else
            {
              redirect('/');
            } 
        }
     }
     
     function check_current_password()
     {       
        $current_pwd=$this->input->get('login_password');           
        $user_data=$this->profile_model->getUserInfo($this->session->userdata('userId'));       
        if($user_data->password==md5($current_pwd)){
            echo 'true';
        }else{
            echo 'false';
        }  
     }
     
     function update_notification()
     {
         if($this->session->userdata('is_logged_in')==true)
         {
           $col=$this->input->post('type');
           $val=$this->input->post('val');
           $this->profile_model->updateNotifications($this->session->userdata('userId'),$col,$val);
           echo 'success';
         }
         else
         {
           redirect('/');
         }          
     }
     
     function delete_account()
     {
         if($this->session->userdata('is_logged_in')==true)
         {
            $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
            $data['page_title']='Delete Account';
            $data['userId']=$this->session->userdata('userId');                  
            $this->load->view('delete_account_page_view',$data); 
         }
         else
         {
           redirect('/');
         }          
     }
     
     function delete_user_account()
     {
        if($this->session->userdata('is_logged_in')==true)
        {
            $userid=$this->input->post('userId');  
            $reason=$this->input->post('reason');  
            $this->profile_model->delete_account($userid,$reason); 
            $sess_items = array('user_email' => '', 'userId' => '','is_logged_in'=> false,'user_type'=>'','user_full_name'=>'','user_photo'=>'','facebook_id'=>'');
            $this->session->unset_userdata($sess_items);
            redirect('user/account_deactivate');            
         }
         else
         {
            redirect('/');
         }   
     }    

     function reset_password($uid)
     {
        //$uid=$this->user_model->decrypt($uid,'Sphinx');
        $uid=base64_decode($uid);
        $data['uid']=$uid; 
        $data['page_title']='Reset Password';
        $data['user_info']=$this->profile_model->getUserInfo($uid);
        $this->load->view('reset_password_page_view',$data); 		 
     }

     function change_password()
     {
        if($this->session->userdata('is_logged_in')==true)
        {
            $pass=$this->input->post('usr_password'); 
            $token=$this->input->post('token'); 
            $res= $this->profile_model->change_password($pass,$this->session->userdata('userId'),$token); 
            if($res==1)
            {               
               redirect('profile/settings');
            }
         }
         else
         {
            redirect('/');
         }        
     }
     
     function check_login_email_exists()
     {
        $email=$this->input->get('usr_email');  
        $user_info=$this->profile_model->getUserInfo($this->session->userdata('userId'));
       
        if($user_info->email==$email)
        {
            echo 'true'; 
        }
        else
        {
             $this->load->model('user_model');
            $checkValid=$this->user_model->checkEmailExists($email);
            if($checkValid==1)
            {
              echo 'true';
            }
            else
            {
              echo 'false';
            }
        }
     }

     function update_profile()
     {
        if($this->session->userdata('is_logged_in')==true)
        {
            $data['firstname']=$this->input->post('usr_first_name');
            $data['lastname']=$this->input->post('usr_last_name');        
            $data['email']=$this->input->post('usr_email');       
            $data['timezone']=$this->input->post('usr_timezone');   
            $referredBy=$this->input->post('referred_by');        
            $this->profile_model->updateUser($data,$this->session->userdata('userId'));    
            $this->session->set_userdata('user_email',$data['email']);                      
            $this->session->set_userdata('user_full_name',$data['firstname']." ".$data['lastname']);            
            redirect('profile/settings');
        }
        else
        {
            redirect('/');
        }
     }     
     
     function upload_image(){
        require('./extras/Uploader.php');
        $upload_dir = './upload_files/';
        //$this->load->library('FileUpload');
        $uploader = new FileUpload('uploadfile');
        // Handle the upload
        $result = $uploader->handleUpload($upload_dir);           

        if (!$result) {
          exit(json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg())));  
        }
        $config['image_library'] = 'gd2';
        $config['source_image'] = $uploader->getSavedFile();
        $config['new_image'] = './user_images/';
        $config['file_name'] = $uploader->getFileName();
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 230;
        $config['height'] = 230;
        $config['thumb_marker'] = "";
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);				  
        $this->image_lib->resize();
        if(file_exists($uploader->getSavedFile())){         
            $config=array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $uploader->getSavedFile();
            $config['new_image'] = './user_images/thumb/';
            $config['file_name'] = $uploader->getFileName();
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 100;
            $config['height'] = 100;
            $config['thumb_marker'] = "";
            $this->image_lib->initialize($config);		
            $this->image_lib->resize();
            unlink($uploader->getSavedFile());
            $user_info=$this->profile_model->getUserInfo($this->session->userdata('userId'));
            if($user_info->userPhoto!=''){
                if(file_exists('./user_images/'.$user_info->userPhoto)){                    
                  unlink('./user_images/'.$user_info->userPhoto);  
                  if(file_exists('./user_images/thumb/'.$user_info->userPhoto)){                    
                    unlink('./user_images/thumb/'.$user_info->userPhoto);    
                  }
                }
            }
            $data['userPhoto']=$uploader->getFileName();
            $this->profile_model->updateUser($data,$this->session->userdata('userId'));   
            
            $this->session->set_userdata('user_photo',$data['userPhoto']);
        }
        echo json_encode(array('success' => true,'file'=>$uploader->getFileName()));
    }
    
    function upload_business_image(){
        require('./extras/Uploader.php');
        $upload_dir = './upload_files/';
        //$this->load->library('FileUpload');
        $uploader = new FileUpload('uploadfile');
        // Handle the upload
        $result = $uploader->handleUpload($upload_dir);           

        if (!$result) {
          exit(json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg())));  
        }
        $config['image_library'] = 'gd2';
        $config['source_image'] = $uploader->getSavedFile();
        $config['new_image'] = './business_logo/';
        $config['file_name'] = $uploader->getFileName();
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 230;
        $config['height'] = 230;
        $config['thumb_marker'] = "";
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);				  
        $this->image_lib->resize();
        if(file_exists($uploader->getSavedFile())){         
            $config=array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $uploader->getSavedFile();
            $config['new_image'] = './business_logo/thumb/';
            $config['file_name'] = $uploader->getFileName();
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 100;
            $config['height'] = 100;
            $config['thumb_marker'] = "";
            $this->image_lib->initialize($config);		
            $this->image_lib->resize();
            unlink($uploader->getSavedFile());
            $user_info=$this->profile_model->getUserProviderInfo($this->session->userdata('userId'));
            if($user_info->businessLogo!=''){
                if(file_exists('./business_logo/'.$user_info->businessLogo)){                    
                  unlink('./business_logo/'.$user_info->businessLogo);  
                  if(file_exists('./business_logo/thumb/'.$user_info->businessLogo)){                    
                    unlink('./business_logo/thumb/'.$user_info->businessLogo);    
                  }
                }
            }
            $data['businessLogo']=$uploader->getFileName();
            $this->profile_model->updateProvider($data,$this->session->userdata('userId'));   
        }
        echo json_encode(array('success' => true,'file'=>$uploader->getFileName()));
    }
    
    function requests($id='')
    {       
        if($this->session->userdata('is_logged_in')==true)
        {
           if($id!=''){
               if($this->session->userdata('userId')==$id){
                   $flag=true;
               }else{
                   $flag=false;
               }
           }  else {
              $flag=true; 
           }
           if($flag==true){
            $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
            $data['page_title']='Recent Request';
            $data['userId']=$this->session->userdata('userId');    
            $data['request_list']= $this->profile_model->getCustomerRequests($this->session->userdata('userId')); 
            $data['business_info']= $this->profile_model->getUserProviderInfo($this->session->userdata('userId'));         
            if($data['business_info']!=''){
                $data['totalProgress']=$data['business_info']->basicInfoProgress+$data['business_info']->mediaProgress+$data['business_info']->bioInfoProgress+$data['business_info']->serviceInfoProgress+$data['business_info']->linksInfoProgress+$data['business_info']->businessInfoProgress+$data['business_info']->qaInfoProgress+$data['business_info']->reviewInfoProgress+$data['business_info']->refereInfoProgress;
            }else{
                $data['totalProgress']=0;
            }
            $this->load->model('setting_model');
            $data['profile_complete_perc_setting']=$this->setting_model->getSettingValue('profile_completion_percentage');
            $this->load->view('request_page_view',$data); 
           }else{
                redirect('user/logout');
           }
        }
        else
        {
          redirect('user/login');
        } 
    }
     
    function services($flag='',$id='')
    {       
        if($this->session->userdata('is_logged_in')==true)
        {
         if($id==''){
            $data['userId']=$this->session->userdata('userId');  
         }else{
            $data['userId']=$id;   
         }
         $this->load->model('setting_model');
         $data['user_data']= $this->profile_model->getUserInfo($data['userId']);          
         if($data['user_data']->city!='' && $data['user_data']->city!='0'){             
             $data['city_nm']= $this->setting_model->getCityName($data['user_data']->city);
             $data['city_list']=$this->setting_model->getCityList($data['user_data']->province); 
         }else{
             $data['city_nm']='';
             $data['city_list']='';
         }
         if($data['user_data']->province!='' && $data['user_data']->province!='0'){             
             $data['province_nm']= $this->setting_model->getProvinceName($data['user_data']->province);
         }else{
             $data['province_nm']='';
         }
         if($data['user_data']->district!='' && $data['user_data']->district!='0'){                          
             $data['district_list']=$this->setting_model->getDistrictList($data['user_data']->city);     
         }else{            
             $data['district_list']='';
         }
         $data['business_info']= $this->profile_model->getUserProviderInfo($data['userId']);
         $data['answer_list']= $this->profile_model->getProviderAnswers($data['userId']);
         $data['answer_count']=$this->profile_model->getProviderAnswerCount($data['userId']);
         $data['media_list']=$this->profile_model->getAllMedia($data['userId']);
         $data['review_list']=$this->profile_model->getAllUserReviews($data['userId']);
         $data['province_list']=$this->setting_model->getProvincesOtherList();   
         $data['page_title']=$data['business_info']->businessName;  
         if($data['business_info']!=''){
             $data['totalProgress']= $this->profile_model->getUserProfileTotalProgress($data['userId']);
         }else{
             $data['totalProgress']=0;
         }
         if($data['user_data']->services!=''){
             $service_arr=explode(",",$data['user_data']->services);
         }else{
             $service_res_arr=$this->profile_model->getProsUserSubServices($data['userId']); 
             
             if(!empty($service_res_arr)){
                $service_arr=array();
                foreach($service_res_arr as $row){
                  $service_arr[]=$row->serviceName;  
                } 
             }else{
                $service_arr=array(); 
             }
         }         
         $data['services']=$service_arr;
         $data['flag']=$flag;
         if($flag=='edit'){             
             $this->load->view('edit_profile_pros_page_view',$data); 
         }else if($flag=='view'){
             $this->load->view('profile_pros_page_view',$data); 
         }else{
             $this->load->view('edit_profile_pros_page_view',$data); 
         }
        }
        else
        {
          redirect('user/login');
        } 
    }
    
    function updateProfileData($type)
    {
        if($this->session->userdata('is_logged_in')==true)
        { 
            $this->load->model('setting_model');
            if($type=='basicInfo'){
                $data['firstname']=$this->input->post('first_name');
                $data['lastname']=$this->input->post('last_name'); 
                $data['streetAddress']=$this->input->post('address1'); 
                $data['province']=$this->input->post('province'); 
                $data['city']=$this->input->post('city'); 
                $data['district']=$this->input->post('district'); 
                $data['zipcode']=$this->input->post('zip_code_id'); 
                $data['phone']=$this->input->post('phone_number'); 
                $data['services']=implode(',', $this->input->post('services'));                
                $this->profile_model->updateUser($data,$this->session->userdata('userId'));
                $pdata='';
                //$pdata['businessName']=$this->input->post('business_name');       
                //$data['tagLine']=$this->input->post('slogan');   
                if($this->input->post('usa_privacy')=='1'){
                    $pdata['showFullAddress']='Y';
                }else{
                    $pdata['showFullAddress']='N';
                }
                $basicInfoCredit=$this->setting_model->getSettingValue('profile_basic_info_credit');
                $checkProfileCredit=$this->profile_model->checkProfileTypeCredit($this->session->userdata('userId'),'profile_basic_info');
                if($data['firstname']!='' && $data['lastname']!='' && $data['streetAddress']!='' && $data['province']!='0' && $data['city']!='0' && $data['district']!='0' && $data['phone']!=''){                    
                    $pdata['basicInfoProgress']=$this->setting_model->getSettingValue('profile_basic_info_percentage');
                    if($checkProfileCredit==''){
                        $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_basic_info','credit'=>$basicInfoCredit,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'Y'));
                        $this->db->where('userId', $this->session->userdata('userId'));                        
                        $this->db->set('credits', 'credits + ' . (int) $basicInfoCredit, FALSE); 
                        $this->db->update('tbluser');
                        // added credit log
                        $creditTypetext=$data['firstname']." ".$data['lastname']." is updated profile basic info";
                        $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_basic_info','credit'=>$basicInfoCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                    }else{                        
                        if($checkProfileCredit->isCreditEarn=='N'){
                            $this->db->where('userId', $this->session->userdata('userId'));                        
                            $this->db->set('credits', 'credits + ' . (int) $basicInfoCredit, FALSE); 
                            $this->db->update('tbluser');   
                            // added credit log
                            $creditTypetext=$data['firstname']." ".$data['lastname']." is updated profile basic info";
                            $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_basic_info','credit'=>$basicInfoCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                        }
                        $this->db->update('tbluserprofilecredits', array('credit'=>$basicInfoCredit,'isCreditEarn'=>'Y'),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_basic_info'));
                    }
                }else{
                    $pdata['basicInfoProgress']=0;
                    if($checkProfileCredit==''){    
                       $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_basic_info','credit'=>0,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'N'));
                    }else{
                       $this->db->update('tbluserprofilecredits', array('credit'=>0,'isCreditEarn'=>$checkProfileCredit->isCreditEarn),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_basic_info')); 
                    }
                }                
               
                $pdata['tagLine']=$this->input->post('slogan');                
                $address_row=$this->setting_model->getFullAdrress($data['province'],$data['city'],$data['district']);
                $address=$address_row->cityName.",".$address_row->districtName.",".$address_row->provName;
                $latlongval=$this->getLnt($address);
                if($latlongval!=''){
                    $pdata['latitude']=$latlongval['lat'];
                    $pdata['longitude']=$latlongval['lng'];
                }else{
                    $pdata['latitude']='';
                    $pdata['longitude']='';
                }
                $this->profile_model->updateProvider($pdata,$this->session->userdata('userId'));   
                $this->load->model('service_model');
                $servicesArr=$this->input->post('services');
                $this->db->delete('tbluserservices', array('userId' => $this->session->userdata('userId'))); 
                if(!empty($servicesArr)){
                    $user_services=array();
                    for($k=0;$k<count($servicesArr);$k++){
                        for($k=0;$k<count($servicesArr);$k++){   
                            $servId='';
                            $servId=$this->service_model->getServicesId($servicesArr[$k]);
                            if($servId!=''){
                                $user_services[]=$servId;
                                $serv_arr=array('userId'=>$this->session->userdata('userId'),'serviceId'=>$servId,'serviceText'=>'');
                                $this->db->insert('tbluserservices', $serv_arr); 
                            }
                        } 
                    }           
                }
                // send job request
                if($pdata['latitude']!='' && $pdata['longitude']!='' && !empty($user_services)){                   
                   $this->load->model('service_model');
                   $distancekm=$this->setting_model->getSettingValue('distanceSetting');
                   $num_of_days_setting=$this->setting_model->getSettingValue('number_of_days_see_request');
                   $user_services=implode(',', $user_services);
                   $date_bet_request=date('Y-m-d', strtotime('-'.$num_of_days_setting.' days'));
                   $request_list=$this->service_model->getRequestsForPros($this->session->userdata('userId'),$pdata['latitude'],$pdata['longitude'],$distancekm,$user_services,$date_bet_request);                  
                   
                   $apikey='EQ4Z41iMdCc7uiznTzzUVw';
                   $this->load->library('mandrill', array($apikey));
                   $mandrill_ready = NULL;
                   try {
                          $this->mandrill->init( $apikey );
                          $mandrill_ready = TRUE;
                   } catch(Mandrill_Exception $e) {
                          $mandrill_ready = FALSE;
                   }
                   if($mandrill_ready) {
                       
                        if(!empty($request_list)){
                            $today=date('Y-m-d H:i:s');
                            foreach($request_list as $request_row){
                                $request_arr=array();
                                $email=array();
                                $us_firstname=$request_row->firstname;
                                $us_lastname=$request_row->lastname;
                                $request_arr=array('prosId'=>$this->session->userdata('userId'),'projectId'=>$request_row->projectId,'status'=>'Pending','addedOn'=>$today,'modifiedOn'=>$today);
                                $this->db->insert('tblquoterequest', $request_arr); 

                                $request_url=$this->config->config['base_url'].'profile/requests/'.$this->session->userdata('userId');
                                if(get_cookie('language') == 'english') {
                                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/new-job-request-template.html');
                                }else{
                                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/new-job-request-template-id.html');    
                                }
                                $htmlMessage=str_replace('{JOB_REQUEST_URL}', $request_url, $htmlMessage); 
                                $htmlMessage=str_replace('{PROS_FULLNAME}', $data['firstname']." ".$data['lastname'], $htmlMessage);  
                                $htmlMessage=str_replace('{FULLNAME}', $us_firstname." ".$us_lastname, $htmlMessage);    
                                $email = array(
                                    'html' => $htmlMessage, //Consider using a view file
                                    'text' => '',
                                    'subject' => 'Work Request at Seekmi',
                                    'from_email' => 'support@seekmi.com',
                                    'from_name' => 'Seekmi',
                                    'to' => array(array('email' => 'deepika123desai@gmail.com')) //Check documentation for more details on this one                
                                    );

                                $this->mandrill->messages_send($email); 
                                // send mail to experts 
                            }
                        }                        
                   }                     
                }
                // send job request
                $this->session->set_userdata('user_full_name',$data['firstname']." ".$data['lastname']);                     
                $total_profile_progress=$this->profile_model->getUserProfileTotalProgress($this->session->userdata('userId'));
                if($total_profile_progress >= 30){
                    $this->db->query('update tbluser set newRegisterProsToken="" where userId="'.$this->session->userdata('userId').'"');
                }
                echo '{"msg":"success","total_progress":'.$total_profile_progress.'}';
            }
            // Update Profile Bio section
            if($type=='bioInfo'){
                $pdata='';
                $userInfo = $this->profile_model->getUserInfo($this->session->userdata('userId'));     
                $pdata['bioInfo']=$this->input->post('bio'); 
                $bioCredit=$this->setting_model->getSettingValue('profile_bio_credit');
                $checkProfileCredit=$this->profile_model->checkProfileTypeCredit($this->session->userdata('userId'),'profile_bio');
                if($pdata['bioInfo']!=''){                    
                    $pdata['bioInfoProgress']=$this->setting_model->getSettingValue('profile_bio_percentage');
                    if($checkProfileCredit==''){
                        $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_bio','credit'=>$bioCredit,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'Y'));
                        $this->db->where('userId', $this->session->userdata('userId'));                        
                        $this->db->set('credits', 'credits + ' . (int) $bioCredit, FALSE); 
                        $this->db->update('tbluser');
                        // added credit log
                        $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile bio";
                        $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_bio','credit'=>$bioCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                    }else{                        
                        if($checkProfileCredit->isCreditEarn=='N'){
                            $this->db->where('userId', $this->session->userdata('userId'));                        
                            $this->db->set('credits', 'credits + ' . (int) $bioCredit, FALSE); 
                            $this->db->update('tbluser'); 
                            // added credit log
                            $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile bio";
                            $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_bio','credit'=>$bioCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                        }
                        $this->db->update('tbluserprofilecredits', array('credit'=>$bioCredit,'isCreditEarn'=>'Y'),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_bio'));
                    }
                }else{
                    $pdata['bioInfoProgress']=0;
                    if($checkProfileCredit==''){    
                       $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_bio','credit'=>0,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'N'));
                    }else{
                       $this->db->update('tbluserprofilecredits', array('credit'=>0,'isCreditEarn'=>$checkProfileCredit->isCreditEarn),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_bio')); 
                    }
                }
                $this->profile_model->updateProvider($pdata,$this->session->userdata('userId'));                                
                $total_profile_progress=$this->profile_model->getUserProfileTotalProgress($this->session->userdata('userId'));
                if($total_profile_progress >= 30){
                    $this->db->query('update tbluser set newRegisterProsToken="" where userId="'.$this->session->userdata('userId').'"');
                }
                echo '{"msg":"success","total_progress":'.$total_profile_progress.'}';
            }
            // Update Profile Service section
            if($type=='description'){
                $pdata='';
                $userInfo = $this->profile_model->getUserInfo($this->session->userdata('userId'));   
                $pdata['bussinessDescription']=$this->input->post('description');
                $servicesCredit=$this->setting_model->getSettingValue('profile_services_credit');
                $checkProfileCredit=$this->profile_model->checkProfileTypeCredit($this->session->userdata('userId'),'profile_services');
                if($pdata['bussinessDescription']!=''){                    
                    $pdata['serviceInfoProgress']=$this->setting_model->getSettingValue('profile_services_percentage');                    
                    if($checkProfileCredit==''){
                        $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_services','credit'=>$servicesCredit,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'Y'));
                        $this->db->where('userId', $this->session->userdata('userId'));                        
                        $this->db->set('credits', 'credits + ' . (int) $servicesCredit, FALSE); 
                        $this->db->update('tbluser');
                        // added credit log
                        $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile services";
                        $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_services','credit'=>$servicesCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                    }else{                        
                        if($checkProfileCredit->isCreditEarn=='N'){
                            $this->db->where('userId', $this->session->userdata('userId'));                        
                            $this->db->set('credits', 'credits + ' . (int) $servicesCredit, FALSE); 
                            $this->db->update('tbluser');
                            // added credit log
                            $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile services";
                            $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_services','credit'=>$servicesCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                        }
                        $this->db->update('tbluserprofilecredits', array('credit'=>$servicesCredit,'isCreditEarn'=>'Y'),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_services'));
                    }
                }else{
                    $pdata['serviceInfoProgress']=0;
                    if($checkProfileCredit==''){    
                       $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_services','credit'=>0,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'N'));
                    }else{
                       $this->db->update('tbluserprofilecredits', array('credit'=>0,'isCreditEarn'=>$checkProfileCredit->isCreditEarn),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_services')); 
                    }
                }
                $this->profile_model->updateProvider($pdata,$this->session->userdata('userId'));                                
                $total_profile_progress=$this->profile_model->getUserProfileTotalProgress($this->session->userdata('userId'));
                if($total_profile_progress >= 30){
                    $this->db->query('update tbluser set newRegisterProsToken="" where userId="'.$this->session->userdata('userId').'"');
                }
                echo '{"msg":"success","total_progress":'.$total_profile_progress.'}';
            }
            // Update Profile Links section
            if($type=='links'){
                $pdata='';
                $userInfo = $this->profile_model->getUserInfo($this->session->userdata('userId')); 
                $pdata['website']=str_replace(array('http://','https://'),'',$this->input->post('website'));
                $linksCredit=$this->setting_model->getSettingValue('profile_links_credit');
                $checkProfileCredit=$this->profile_model->checkProfileTypeCredit($this->session->userdata('userId'),'profile_links');
                if($pdata['website']!=''){                    
                    $pdata['linksInfoProgress']=$this->setting_model->getSettingValue('profile_links_percentage');                    
                    if($checkProfileCredit==''){
                        $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_links','credit'=>$linksCredit,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'Y'));
                        $this->db->where('userId', $this->session->userdata('userId'));                        
                        $this->db->set('credits', 'credits + ' . (int) $linksCredit, FALSE); 
                        $this->db->update('tbluser');
                        // added credit log
                        $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile links";
                        $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_links','credit'=>$linksCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                    }else{                        
                        if($checkProfileCredit->isCreditEarn=='N'){
                            $this->db->where('userId', $this->session->userdata('userId'));                        
                            $this->db->set('credits', 'credits + ' . (int) $linksCredit, FALSE); 
                            $this->db->update('tbluser');  
                            // added credit log
                            $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile links";
                            $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_links','credit'=>$linksCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                        }
                        $this->db->update('tbluserprofilecredits', array('credit'=>$linksCredit,'isCreditEarn'=>'Y'),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_links'));
                    }
                }else{
                    $pdata['linksInfoProgress']=0;
                    if($checkProfileCredit==''){    
                       $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_links','credit'=>0,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'N'));
                    }else{
                       $this->db->update('tbluserprofilecredits', array('credit'=>0,'isCreditEarn'=>$checkProfileCredit->isCreditEarn),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_links')); 
                    }
                }
                $this->profile_model->updateProvider($pdata,$this->session->userdata('userId'));                
                $total_profile_progress=$this->profile_model->getUserProfileTotalProgress($this->session->userdata('userId'));
                if($total_profile_progress >= 30){
                    $this->db->query('update tbluser set newRegisterProsToken="" where userId="'.$this->session->userdata('userId').'"');
                }
                echo '{"msg":"success","total_progress":'.$total_profile_progress.'}';
            }
            // Update Business info section
            if($type=='businessInfo'){
                $pdata='';
                $pdata['businessName']=$this->input->post('company');
                $pdata['foundingYear']=$this->input->post('year_founded');
                $pdata['employeeCount']=$this->input->post('employee_count');
                $preferanceArr=$this->input->post('preferences');
                if(in_array('travel_to_provider',$preferanceArr)){
                    $pdata['travelToProvider']='Y';
                }else{
                    $pdata['travelToProvider']='N';
                }

                if(in_array('travel_remote',$preferanceArr)){
                    $pdata['travelRemote']='Y';
                }else{
                    $pdata['travelRemote']='N';
                }
                $pdata['distanceMiles']='';       
                if(in_array('travel_to_customer',$preferanceArr)){
                    $pdata['travelToCustomer']='Y';
                    $pdata['distanceMiles']=$this->input->post('service-travel-distance');
                }else{
                    $pdata['travelToCustomer']='N';
                    $pdata['distanceMiles']='';
                }
                $pdata['businessInfoProgress']=0;
                /*if($pdata['businessName']!=''){                    
                    $pdata['businessInfoProgress']=10;                     
                }else{
                    $pdata['businessInfoProgress']=0;
                }*/
                $this->profile_model->updateProvider($pdata,$this->session->userdata('userId'));               
                $total_profile_progress=$this->profile_model->getUserProfileTotalProgress($this->session->userdata('userId'));
                if($total_profile_progress >= 30){
                    $this->db->query('update tbluser set newRegisterProsToken="" where userId="'.$this->session->userdata('userId').'"');
                }
                echo '{"msg":"success","total_progress":'.$total_profile_progress.'}';
            }
            // Update Q&A section
            if($type=='qainfo'){
                $pdata='';
                $userInfo = $this->profile_model->getUserInfo($this->session->userdata('userId')); 
                $quest_list=$this->profile_model->getProviderQuestionList();
                $this->db->delete('tblprovideranswers', array('userId' => $this->session->userdata('userId'))); 
                $k=0;
                foreach($quest_list as $quest_row){  
                    if($this->input->post('answer_'.$quest_row->queId)!=''){
                    $ans_arr=array('userId'=>$this->session->userdata('userId'),'questionId'=>$quest_row->queId,'answerText'=>$this->input->post('answer_'.$quest_row->queId));
                    $this->db->insert('tblprovideranswers', $ans_arr); 
                    $k++;
                    }
                }
                $qaCredit=$this->setting_model->getSettingValue('profile_qa_credit');
                $checkProfileCredit=$this->profile_model->checkProfileTypeCredit($this->session->userdata('userId'),'profile_qa');
                if($k > 6){
                    $progressPercentage=$this->setting_model->getSettingValue('profile_qa_percentage');                    
                    if($checkProfileCredit==''){
                        $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_qa','credit'=>$qaCredit,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'Y'));
                        $this->db->where('userId', $this->session->userdata('userId'));                        
                        $this->db->set('credits', 'credits + ' . (int) $qaCredit, FALSE); 
                        $this->db->update('tbluser');
                        // added credit log
                        $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile qa";
                        $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_qa','credit'=>$qaCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                    }else{                        
                        if($checkProfileCredit->isCreditEarn=='N'){
                            $this->db->where('userId', $this->session->userdata('userId'));                        
                            $this->db->set('credits', 'credits + ' . (int) $qaCredit, FALSE); 
                            $this->db->update('tbluser');   
                            // added credit log
                            $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile qa";
                            $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_qa','credit'=>$qaCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                        }
                        $this->db->update('tbluserprofilecredits', array('credit'=>$qaCredit,'isCreditEarn'=>'Y'),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_qa'));
                    }
                }else{ 
                    $progressPercentage=0;  
                    if($checkProfileCredit==''){    
                       $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_qa','credit'=>0,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'N'));
                    }else{
                       $this->db->update('tbluserprofilecredits', array('credit'=>0,'isCreditEarn'=>$checkProfileCredit->isCreditEarn),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_qa')); 
                    }
                }
                $this->db->query('update tbluserprovider set qaInfoProgress='.$progressPercentage.' where userId="'.$this->session->userdata('userId').'"');
                $total_profile_progress=$this->profile_model->getUserProfileTotalProgress($this->session->userdata('userId'));
                if($total_profile_progress >= 30){
                    $this->db->query('update tbluser set newRegisterProsToken="" where userId="'.$this->session->userdata('userId').'"');
                }
                $total_answer_count=$this->profile_model->getProviderAnswerCount($this->session->userdata('userId'));
                echo '{"msg":"success","total_progress":'.$total_profile_progress.',"answer_count":'.$total_answer_count.'}';
            }
            // Update Referral section
            if($type=='referral'){
                $userId=$this->session->userdata('userId');
                $referral_email=$this->input->post('refer_email');  
                if($referral_email!=''){
                    $this->load->model('pros_model');
                     $flag=false; 
                    $user_data_refer=$this->pros_model->getUserIdByEmail($referral_email);
                    if(!empty($user_data_refer)){
                        $checkReferEmail=$this->pros_model->checkAlreadyReferred($user_data_refer->userId,$userId);
                        if($checkReferEmail!=''){
                            echo 'already';   
                            die;
                        }else if($userId==$user_data_refer->userId){
                            echo 'own_refer';  
                            die;
                        }else{
                             $flag=true; 
                        }
                        $fullname=$user_data_refer->firstname." ".$user_data_refer->lastname;
                    }else{
                       $flag=true; 
                       $fullname='';
                    }
                       
                    if($flag==true){  
                        $user_data=$this->profile_model->getUserInfo($userId); 
                        if($user_data->referralCode==''){
                            $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                            $res = "";
                            for ($i = 0; $i < 13; $i++) {
                               $res .= $chars[mt_rand(0, strlen($chars)-1)];
                            }
                            $referralCode=$res;
                            $this->db->update('tbluser', array('referralCode'=>$referralCode), array('userId' => $userId));
                        }else{
                            $referralCode=$user_data->referralCode;
                        }                                                
                        $referral_url=$this->config->config['base_url'].'pros/register/'.$referralCode;
                        $refer_name=$user_data->firstname." ".$user_data->lastname;
                        if(get_cookie('language') == 'english') {
                          $subject='Personal Invitation from '.$refer_name;
                          $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/referral-email-template.html');
                        }else{
                          $subject='Undangan Pribadi dari '.$refer_name;  
                          $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/referral-email-template-id.html');   
                        }                        
                        $htmlMessage=str_replace('{REFERRAL_URL}', $referral_url, $htmlMessage);
                        $htmlMessage=str_replace('{FULLNAME}', $fullname, $htmlMessage);
                        $htmlMessage=str_replace('{REFER_NAME}', $refer_name, $htmlMessage);
                        $apikey='EQ4Z41iMdCc7uiznTzzUVw';
                        $this->load->library('mandrill', array($apikey));
                        $mandrill_ready = NULL;
                        try {
                              $this->mandrill->init( $apikey );
                              $mandrill_ready = TRUE;
                        } catch(Mandrill_Exception $e) {
                              $mandrill_ready = FALSE;
                        }
                        if($mandrill_ready) {
                              //Send us some email!
                              $email = array(
                                  'html' => $htmlMessage, //Consider using a view file
                                  'text' => '',
                                  'subject' => $subject,
                                  'from_email' => 'support@seekmi.com',
                                  'from_name' => 'Seekmi',
                                  'to' => array(array('email' => $referral_email)) //Check documentation for more details on this one                
                                  );

                              $result = $this->mandrill->messages_send($email);           
                        } 
                        echo 'sent';
                       }
                       
                    }else{
                        echo 'success';
                    }
                /*$referral_email=$this->input->post('referred_by');  
                if($referral_email!=''){
                    $this->load->model('pros_model');
                    $checkReferEmail=$this->pros_model->checkProsEmailExists($referral_email);
                    if($checkReferEmail!=''){
                        $user_data=$this->profile_model->getUserInfo($userId); 
                        if($user_data->referredBy==$checkReferEmail->userId){
                           echo 'already';   
                        }else{
                           
                            $referredById=$checkReferEmail->userId;                            
                            $this->load->model('setting_model');
                            $credit=$this->setting_model->getSettingValue('referral_credit');
                            $this->db->where('userId', $userId);
                            $this->db->set('referredBy', $referredById);
                            $this->db->set('credits', 'credits + ' . (int) $credit, FALSE); 
                            $this->db->update('tbluser');

                            $reward_data=array('to'=>$referredById,'from'=>$userId,'voucher'=>'Y','creditto'=>0,'creditfrom'=>$credit,'createdDate'=>date('Y-m-d H:i:s'));
                            $this->db->insert('tblrewardsetting', $reward_data);
                            echo 'success';                                                  
                           
                        }
                    }else{
                        echo 'invalid';
                    }
                }else{
                    echo 'success';
                }*/
            }
        }
        else
        {
          redirect('user/login');
        } 
    }
    
    function uploadMediaPhoto()
    {
        require('./extras/Uploader.php');
        $upload_dir = './upload_files/';
        //$this->load->library('FileUpload');
        $uploader = new FileUpload('uploadfile');

        // Handle the upload
        $result = $uploader->handleUpload($upload_dir);           

        if (!$result) {
          exit(json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg())));  
        }
        
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $uploader->getSavedFile();
        $config['new_image'] = './business_images/';
        $config['file_name'] = $uploader->getFileName();
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 580;
        $config['height'] = 380;
        $config['thumb_marker'] = "";        
        $this->image_lib->initialize($config);				  
        $this->image_lib->resize();
        
        unset($config);
        
        $config['image_library'] = 'gd2';
        $config['source_image'] = './business_images/'.$uploader->getFileName();
        $config['new_image'] = './business_images/thumb/';
        $config['file_name'] = $uploader->getFileName();
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 100;
        $config['height'] = 100;
        $config['thumb_marker'] = "";        
        $this->image_lib->initialize($config);				  
        $this->image_lib->resize();
        
        if(file_exists($uploader->getSavedFile())){
            unlink($uploader->getSavedFile());
        }
        $media_count=$this->profile_model->getMediaCount($this->session->userdata('userId'));
        $order=$media_count+1;
        $img_arr=array('userId'=>$this->session->userdata('userId'),'mediaType'=>'Image','mediaPath'=>$uploader->getFileName(),'mediaTitle'=>'','order'=>$order);
        $this->db->insert('tblbusinessmedia', $img_arr); 
        $mediaId=$this->db->insert_id();
        $html_append='<li id="mediaSubDiv'.$mediaId.'" class="media-list-item">                
                <div class="media-picture-container media-container">
                    <img src="'.$this->config->config['base_url'].'business_images/thumb/'.$uploader->getFileName().'">
                    <div class="picture-caption">
                        <textarea onblur="saveCaption("'.$mediaId.'")" id="mediaCaption'.$mediaId.'"></textarea>
                        <div id="captionsSaving['.$mediaId.']" class="subtext-form" style="display: none;">
                            Saving...
                        </div>
                    </div>
                </div>
                <div class="media-picture-container media-container media-button">
                    <span onclick="deleteMedia('.$mediaId.')" class="media-delete">
                        Delete
                    </span>
                    <!-- <span ng-click="rotateMedia(media.picture.id)" class="media-rotate">
                        Rotate
                    </span>-->
                </div>
            </li>'; 
        $userInfo=$this->profile_model->getUserInfo($this->session->userdata('userId')); 
        $mediaCredit=$this->setting_model->getSettingValue('profile_media_credit');
        $checkProfileCredit=$this->profile_model->checkProfileTypeCredit($this->session->userdata('userId'),'profile_media');
        if($media_count >= 2){
            $progressPercentage=$this->setting_model->getSettingValue('profile_media_percentage');                    
            if($checkProfileCredit==''){
                $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_media','credit'=>$mediaCredit,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'Y'));
                $this->db->where('userId', $this->session->userdata('userId'));                        
                $this->db->set('credits', 'credits + ' . (int) $mediaCredit, FALSE); 
                $this->db->update('tbluser');
                // added credit log
                $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile media";
                $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_media','credit'=>$mediaCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
            }else{                        
                if($checkProfileCredit->isCreditEarn=='N'){
                    $this->db->where('userId', $this->session->userdata('userId'));                        
                    $this->db->set('credits', 'credits + ' . (int) $mediaCredit, FALSE); 
                    $this->db->update('tbluser');  
                    // added credit log
                    $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile media";
                    $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_media','credit'=>$mediaCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                }
                $this->db->update('tbluserprofilecredits', array('credit'=>$mediaCredit,'isCreditEarn'=>'Y'),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_media'));
            }
        }else{ 
            $progressPercentage=0;  
            if($checkProfileCredit==''){    
               $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_media','credit'=>0,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'N'));
            }else{
               $this->db->update('tbluserprofilecredits', array('credit'=>0,'isCreditEarn'=>$checkProfileCredit->isCreditEarn),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_media')); 
            }
        }
        $this->db->query('update tbluserprovider set mediaProgress='.$progressPercentage.' where userId="'.$this->session->userdata('userId').'"');
        $total_profile_progress=$this->profile_model->getUserProfileTotalProgress($this->session->userdata('userId'));        
        echo json_encode(array('success' => true,'append_str'=>$html_append,'total_progress'=>$total_profile_progress));
    }
    
    function uploadVideo()
    {
        $video_url=$this->input->post('video_url');
        $image_url = parse_url($video_url);
        $thumb_url='';
        if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
            $array = explode("&", $image_url['query']);
            $video_url_db='https://www.youtube.com/embed/'.substr($array[0], 2);
            $imageData=  @file_get_contents("https://img.youtube.com/vi/".substr($array[0], 2)."/mqdefault.jpg");
            if($imageData===false){
                $thumb_url='';
            }else{
                $thumb_url="https://img.youtube.com/vi/".substr($array[0], 2)."/mqdefault.jpg";
            }
        } else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
            $video_url_db='http://player.vimeo.com/video/'.substr($image_url['path'], 1);
            $hash = unserialize(@file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
            if($hash===false){
                $thumb_url='';
            }else{
                $thumb_url=$hash[0]["thumbnail_small"];
            }   
        }
        
        if($thumb_url==''){
            echo '{"msg":"failed"}';           
        }else{
            $userInfo=$this->profile_model->getUserInfo($this->session->userdata('userId')); 
            $media_count=$this->profile_model->getMediaCount($this->session->userdata('userId'));
            $order=$media_count+1;
            $video_arr=array('userId'=>$this->session->userdata('userId'),'mediaType'=>'Video','mediaPath'=>$video_url_db,'mediaTitle'=>'','mediaVideoThumbPath'=>$thumb_url,'order'=>$order);
            $this->db->insert('tblbusinessmedia', $video_arr); 
            $mediaId=$this->db->insert_id();
            $mediaCredit=$this->setting_model->getSettingValue('profile_media_credit');
            $checkProfileCredit=$this->profile_model->checkProfileTypeCredit($this->session->userdata('userId'),'profile_media');
            if($media_count >= 2){
                $progressPercentage=$this->setting_model->getSettingValue('profile_media_percentage');                    
                if($checkProfileCredit==''){
                    $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_media','credit'=>$mediaCredit,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'Y'));
                    $this->db->where('userId', $this->session->userdata('userId'));                        
                    $this->db->set('credits', 'credits + ' . (int) $mediaCredit, FALSE); 
                    $this->db->update('tbluser');
                    // added credit log
                    $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile media";
                    $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_media','credit'=>$mediaCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                }else{                        
                    if($checkProfileCredit->isCreditEarn=='N'){
                        $this->db->where('userId', $this->session->userdata('userId'));                        
                        $this->db->set('credits', 'credits + ' . (int) $mediaCredit, FALSE); 
                        $this->db->update('tbluser');  
                        // added credit log
                        $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile media";
                        $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_media','credit'=>$mediaCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                    }
                    $this->db->update('tbluserprofilecredits', array('credit'=>$mediaCredit,'isCreditEarn'=>'Y'),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_media'));
                }
            }else{ 
                $progressPercentage=0;  
                if($checkProfileCredit==''){    
                   $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_media','credit'=>0,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'N'));
                }else{                    
                   $this->db->update('tbluserprofilecredits', array('credit'=>0,'isCreditEarn'=>$checkProfileCredit->isCreditEarn),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_media')); 
                }
            }
            $this->db->query('update tbluserprovider set mediaProgress='.$progressPercentage.' where userId="'.$this->session->userdata('userId').'"');
            $videoStr= "<li id='mediaSubDiv".$mediaId."' class='media-list-item'><div class='media-picture-container media-container'><img src='".$thumb_url."' width='100'></div><div class='media-picture-container media-container media-button'><span onclick='deleteMedia(".$mediaId.")' class='media-delete'> Delete</span></div></li>";
            $total_profile_progress=$this->profile_model->getUserProfileTotalProgress($this->session->userdata('userId'));    
            echo '{"msg":"success","video_str":"'.$videoStr.'","total_progress":'.$total_profile_progress.'}';  
        }
    }
    
    function deleteMedia()
    {
        $mid=$this->input->post('id');
        $media_row=$this->profile_model->getMediaDetail($mid);
        if($media_row->mediaPath!='' && $media_row->mediaType=='Image'){
            if(file_exists('./business_images/'.$media_row->mediaPath)){
                unlink('./business_images/'.$media_row->mediaPath);
            }
            if(file_exists('./business_images/thumb/'.$media_row->mediaPath)){
                unlink('./business_images/thumb/'.$media_row->mediaPath);
            }
        }
        $this->db->delete('tblbusinessmedia', array('mediaId' => $mid));
        $media_count=$this->profile_model->getMediaCount($this->session->userdata('userId'));
        if($media_count < 2){
            $progressPercentage=0;
            $this->db->query('update tbluserprovider set mediaProgress='.$progressPercentage.' where userId="'.$this->session->userdata('userId').'"');
        }
        echo 'true';
    }
    
    function deleteBusinessLogo()
    {
        $user_info=$this->profile_model->getUserProviderInfo($this->session->userdata('userId'));
        if($user_info->businessLogo!=''){
            if(file_exists('./business_logo/'.$user_info->businessLogo)){                    
              unlink('./business_logo/'.$user_info->businessLogo);  
              if(file_exists('./business_logo/thumb/'.$user_info->businessLogo)){                    
                unlink('./business_logo/thumb/'.$user_info->businessLogo);    
              }
            }
        }
        $data['businessLogo']='';
        $this->profile_model->updateProvider($data,$this->session->userdata('userId'));
        echo 'true';
    }
    
    function change_request_status()
    {
        $qid=$this->input->post('qid');
        $status=$this->input->post('sts');
        $projectid=$this->input->post('pid');
        $logged_userid=$this->session->userdata('userId');
        
        $this->profile_model->changeRequestStatus($qid,$status,$projectid);
        
        if($status=='complete'){
            $apikey='EQ4Z41iMdCc7uiznTzzUVw';
            $this->load->library('mandrill', array($apikey));
            $mandrill_ready = NULL;
            try {
                   $this->mandrill->init( $apikey );
                   $mandrill_ready = TRUE;
            } catch(Mandrill_Exception $e) {
                   $mandrill_ready = FALSE;
            }
            if($mandrill_ready){
                $pros_user_data= $this->profile_model->getUserInfo($logged_userid); 
                $proj_user_data=$this->profile_model->getProjectOwner($projectid);
                $firstname=$proj_user_data->firstname;
                $lastname=$proj_user_data->lastname;
                $request_url=$this->config->config['base_url'].'profile/services/view#reviews';
                if(get_cookie('language') == 'english') {
                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/pros-completed-task-template.html');
                }else{
                $htmlMessage=file_get_contents($this->config->config['main_base_url'].'email-templates/pros-completed-task-template-id.html');    
                }
                $htmlMessage=str_replace('{VIEW_REVIEW_URL}', $request_url, $htmlMessage); 
                $htmlMessage=str_replace('{PROS_FULLNAME}', $this->session->userdata('user_full_name'), $htmlMessage);  
                $htmlMessage=str_replace('{USER_FULLNAME}', $firstname." ".$lastname, $htmlMessage);    
                $email = array(
                    'html' => $htmlMessage, //Consider using a view file
                    'text' => '',
                    'subject' => 'Task Completed at Seekmi',
                    'from_email' => 'support@seekmi.com',
                    'from_name' => 'Seekmi',
                    'to' => array(array('email' => $pros_user_data->email)) //Check documentation for more details on this one                
                    );

                $this->mandrill->messages_send($email);  
            }
        }
        echo 'success';
    }
    
    function add_quote($id)
    {
        if($this->session->userdata('is_logged_in')==true)
        {
         $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
         $data['page_title']='Submit a quote';
         $data['userId']=$this->session->userdata('userId');    
         $data['requestId']=$id;    
         $data['request_row']= $this->profile_model->getCustomerRequestDetails($id); 
         $data['quote_count']= $this->profile_model->getProjectQuoteCount($data['request_row']->projectId); 
         $data['project_owner_data']= $this->profile_model->getProjectOwner($data['request_row']->projectId); 
         $this->load->model('service_model');
         $data['req_data']=$this->service_model->getRequestQuestions($data['request_row']->projectId);
         $optionArr=array();
         if($data['req_data']!=''){
            $option_extra=''; 
            foreach($data['req_data'] as $req_row){
               $optArr=array(); 
               if($req_row->optionVal=='' || $req_row->optionVal=='0'){
                 $option_data=$this->service_model->getRequestAnswers($req_row->questionId,$data['request_row']->projectId);
                 if(!empty($option_data)){                        
                     foreach($option_data as $option_row){
                          if($option_row->optionType=='miles'){
                                if($option_row->extras!=''){
                                  $extraArr=json_decode($option_row->extras);
                                  $option_extra=$extraArr->miles." kilometer";
                                }else{
                                  $option_extra='';
                                }
                          }else{
                             $option_extra='';
                          }
                         $optArr[]=array('optionText'=>$option_row->optionText,'optionType'=>$option_row->optionType,'optionExtra'=>$option_extra);
                     }
                 }else{
                     $optArr=array();
                 }
               }else{                     
                  $optArr[]=array('optionText'=>$req_row->optionVal,'optionType'=>'','optionExtra'=>$option_extra); 
               }
              $optionArr[$req_row->questionId]=$optArr;
            }
         }           
         $data['req_option_data']=$optionArr;
         $this->load->view('submit_quote_page_view',$data); 
        }
        else
        {
          redirect('user/login');
        } 
    }
    
    function view_request($id)
    {       
        if($this->session->userdata('is_logged_in')==true)
        {
         $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
         $data['page_title']='View Request';
         $data['userId']=$this->session->userdata('userId');         
         $data['request_row']= $this->profile_model->getCustomerRequestDetails($id); 
         $this->load->model('bid_model');
         $data['message_list'] = $this->bid_model->getQuoteMessage($id);
         $data['quote_count']= $this->profile_model->getProjectQuoteCount($data['request_row']->projectId); 
         $data['project_owner_data']= $this->profile_model->getProjectOwner($data['request_row']->projectId); 
         $this->load->model('service_model');
         $data['req_data']=$this->service_model->getRequestQuestions($data['request_row']->projectId);
         $optionArr=array();
         if($data['req_data']!=''){
            $option_extra=''; 
            foreach($data['req_data'] as $req_row){
               $optArr=array(); 
               if($req_row->optionVal=='' || $req_row->optionVal=='0'){
                 $option_data=$this->service_model->getRequestAnswers($req_row->questionId,$data['request_row']->projectId);
                 if(!empty($option_data)){                        
                     foreach($option_data as $option_row){
                         if($option_row->optionType=='miles'){
                                if($option_row->extras!=''){
                                  $extraArr=json_decode($option_row->extras);
                                  $option_extra=$extraArr->miles." kilometer";
                                }else{
                                  $option_extra='';
                                }
                          }else{
                             $option_extra='';
                          }
                         $optArr[]=array('optionText'=>$option_row->optionText,'optionType'=>$option_row->optionType,'optionExtra'=>$option_extra);
                     }
                 }else{
                     $optArr=array();
                 }
               }else{                     
                  $optArr[]=array('optionText'=>$req_row->optionVal,'optionType'=>'','optionExtra'=>$option_extra); 
               }
              $optionArr[$req_row->questionId]=$optArr;
            }
         }           
         $data['req_option_data']=$optionArr;
         $this->db->update('tblquoterequest', array('readFlag'=>'Y'), array('requestId' => $id));
         $this->load->view('request_detail_page_view',$data); 
        }
        else
        {
          redirect('user/login');
        } 
    }
    
    function work($type='')
    {       
        if($this->session->userdata('is_logged_in')==true)
        {
         $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
         if($type==''){
           $data['page_title']='In Progress';    
           $data['empty_data_message']='You don\'t have any active quotes.';
         }
         if($type=='hired'){
           $data['page_title']='Hired'; 
           $data['empty_data_message']='You don\'t have any quotes for which you\'ve been hired… yet!';
         }
         if($type=='archive'){
           $data['page_title']='Archived';    
           $data['empty_data_message']='You don\'t have any quotes for which you\'ve been archived… yet!';
         }
         $data['userId']=$this->session->userdata('userId');    
         $data['request_list']= $this->profile_model->getCustomerQuotes($this->session->userdata('userId'),$data['page_title']); 
         $this->load->view('request_quote_list_page_view',$data); 
        }
        else
        {
          redirect('user/login');
        } 
    }
    
    function check_credit()
    {
        $userId=$this->input->post('userId');
        $serviceId=$this->input->post('serviceId');
        $this->load->model('service_model');
        $serviceInfo=$this->service_model->getSubServiceInfo($serviceId);
        $user_data= $this->profile_model->getUserInfo($this->session->userdata('userId')); 
        if($user_data->credits >= $serviceInfo->avlCredit){
            echo 'true';
        }else{
            echo 'false';
        }
    }  
    
    function search_service()
    {
        if($this->session->userdata('is_logged_in')==true)
        {
         $data['user_data']= $this->profile_model->getUserInfo($this->session->userdata('userId'));   
         $data['page_title']='Search Service';
         $this->load->view('search_service_dashboard_page_view',$data); 
        }
        else
        {
          redirect('user/login');
        } 
    }
    
    function complete($token){        
      
        if($token!=''){
            $user_row=$this->profile_model->checkNewRegistrationTokenPros($token);           
            if($user_row!=''){  
              if($this->session->userdata('is_logged_in')==false)
              {
                  if($this->session->userdata('userId')==$user_row->userId){
                     if($user_row->status=='N'){
                        $this->profile_model->updateUser(array('status'=>'Y','confirmationToken'=>'','modifiedDate'=>date('Y-m-d H:i:s')),$user_row->userId);   
                     }
                     $this->session->set_userdata('user_email',$user_row->email);
                     $this->session->set_userdata('userId',$user_row->userId);
                     $this->session->set_userdata('user_type',$user_row->userType);
                     $this->session->set_userdata('user_full_name',$user_row->firstname." ".$user_row->lastname);
                     $this->session->set_userdata('user_photo',$user_row->userPhoto);
                     $this->session->set_userdata('facebook_id',$user_row->facebookProfileId);
                     $this->session->set_userdata('is_logged_in',true);      
                     redirect('profile/services');
                   }else{
                      redirect('user/logout');
                   }
              }else {
                  
                 if($user_row->status=='N'){
                    $this->profile_model->updateUser(array('status'=>'Y','confirmationToken'=>'','modifiedDate'=>date('Y-m-d H:i:s')),$user_row->userId);   
                 }
                 $this->session->set_userdata('user_email',$user_row->email);
                 $this->session->set_userdata('userId',$user_row->userId);
                 $this->session->set_userdata('user_type',$user_row->userType);
                 $this->session->set_userdata('user_full_name',$user_row->firstname." ".$user_row->lastname);
                 $this->session->set_userdata('user_photo',$user_row->userPhoto);
                 $this->session->set_userdata('facebook_id',$user_row->facebookProfileId);
                 $this->session->set_userdata('is_logged_in',true);      
                 redirect('profile/services');                  
              }    
                
            }else{
                redirect('user/login');
            }
         }else{
             redirect('user/login');
         }      
    }
    
    function change_first_loggedin_status()
    {
        $userId=$this->input->post('uid');
        $this->profile_model->updateUser(array('isNewLogin'=>'Y'),$userId);   
        echo 'success';
    }
    
    function change_first_profile_loggedin_status()
    {
        $userId=$this->input->post('uid');
        $this->profile_model->updateUser(array('newRegisterProsFlag'=>'N'),$userId);   
        echo 'success';
    }
    
    function saveContinueProfileMedia(){
        $this->load->model('setting_model');
        $userInfo = $this->profile_model->getUserInfo($this->session->userdata('userId'));   
        $media_count=$this->profile_model->getMediaCount($this->session->userdata('userId'));        
        $mediaCredit=$this->setting_model->getSettingValue('profile_media_credit');
        $checkProfileCredit=$this->profile_model->checkProfileTypeCredit($this->session->userdata('userId'),'profile_media');
        if($media_count >= 2){
            $progressPercentage=$this->setting_model->getSettingValue('profile_media_percentage');                    
            if($checkProfileCredit==''){
                $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_media','credit'=>$mediaCredit,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'Y'));
                $this->db->where('userId', $this->session->userdata('userId'));                        
                $this->db->set('credits', 'credits + ' . (int) $mediaCredit, FALSE); 
                $this->db->update('tbluser');
                // added credit log
                $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile media";
                $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_media','credit'=>$mediaCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
            }else{                        
                if($checkProfileCredit->isCreditEarn=='N'){
                    $this->db->where('userId', $this->session->userdata('userId'));                        
                    $this->db->set('credits', 'credits + ' . (int) $mediaCredit, FALSE); 
                    $this->db->update('tbluser');
                    // added credit log
                    $creditTypetext=$userInfo->firstname." ".$userInfo->lastname." is updated profile media";
                    $this->db->insert('tblcreditlog', array('creditType'=>'profile_completion','profileType'=>'profile_media','credit'=>$mediaCredit,'userId' => $this->session->userdata('userId'),'isEarn'=>'Y','creditTypeText'=>$creditTypetext,'createdDate'=>date('Y-m-d H:i:s')));
                }
                $this->db->update('tbluserprofilecredits', array('credit'=>$mediaCredit,'isCreditEarn'=>'Y'),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_media'));
            }
        }else{ 
            $progressPercentage=0;  
            if($checkProfileCredit==''){    
               $this->db->insert('tbluserprofilecredits', array('profileType'=>'profile_media','credit'=>0,'prosId' => $this->session->userdata('userId'),'isCreditEarn'=>'N'));
            }else{                    
               $this->db->update('tbluserprofilecredits', array('credit'=>0,'isCreditEarn'=>$checkProfileCredit->isCreditEarn),array('prosId' => $this->session->userdata('userId'),'profileType'=>'profile_media')); 
            }
        }
        $total_profile_progress=$this->profile_model->getUserProfileTotalProgress($this->session->userdata('userId'));    
        echo '{"msg":"success","total_progress":'.$total_profile_progress.'}';
    }
    
     function getLnt($zip){
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip).",indonesia&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[]=$result['results'][0];
        $result2[]=$result1[0]['geometry'];
        $result3[]=$result2[0]['location'];
        return $result3[0];
    }
}
?>