<?php
class Beta extends CI_Controller
{
       function __construct()
       {
           parent::__construct();    
           $this->load->helper('url');
	   $this->load->database();	   
	   $this->load->library('session');
	   $this->load->library('pagination');   
           //date_default_timezone_set($this->config->config['constTimeZone']);
       }
	 
       function index()
       {          
           $data['page_title'] = 'Home Page';
           $this->load->library('facebook'); 
           $this->facebook->destroySession();
           $this->load->model('service_model');
           $data['categories']=$this->service_model->getCategories(); 
           $this->load->model('setting_model');
           $data['province_list']=$this->setting_model->getProvincesList();
           $this->load->view('home_page_view', $data); 
       }
	
       function logged_in_user()
       {
            if($this->session->userdata('userId')=='')
            {
                 redirect($this->config->config['base_url']);
            }
	}	
}
?>