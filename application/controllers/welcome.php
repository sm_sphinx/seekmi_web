<?php
class Welcome extends CI_Controller
{
       function __construct()
       {
           parent::__construct();    
           $this->load->helper('url');
	   $this->load->database();	   
	   $this->load->library('session');	  
       } 

       function index()
       {          
           $data['page_title'] = 'Welcome to Seekmi';           
           $this->load->model('service_model');
           $data['categories']=$this->service_model->getCategories();            
           $this->load->view('welcome_page_view', $data); 
       }
       	
}
?>