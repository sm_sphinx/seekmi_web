<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


define('GET_THINGS_DONE', 'Indonesia');
define('BY_HIRING', 'Indonesia text');
define('REGISTER_PART', 'Daftar sekarang dan menangkan:');
define('SERVICE_PART', 'Jasa apa yang Anda butuhkan?');
define('GET_FREE_QUOTE', 'Dapatkan penawaran gratis2');
define('YOUR_SERVICE_PROVIDER', 'Anda penyedia jasa?');
define('GET_PART', 'Get up to 5 customer quotes <br/>within hours.<br/>
                                            Compare   & hire the best. <br/>Quick. Easy. Free.<br/><br/><br/><br/>');
define('KODOM_PART', 'Ind-kaodim is now available in Klang Valley, Penang & <br>
                                            Johor Bahru');
define('KAMI_PART', 'Kami akan mempertemukan Anda ke penyedia jasa professional yang akan memberikan penawaran terbaik sesuai kebutuhan Anda');
define('SEEKMI_HELP', 'Bagaimana Seekmi dapat membantu Anda?');
define('FIND_COMPARE', 'Temukan, bandingkan dan pekerjakan penyedia jasa professional terbaik');
define('WHAT_YOU_NEED', 'Beritahukan kami apa yang Anda butuhkan');
define('ONE_MINUTE_TO_ANS', 'Hanya satu menit untuk menjawab pertanyaan singkat tentang kebutuhan Anda.');
define('GET_A_QUOTE', 'Dapatkan penawaran dan bandingkan');
define('RECIEVE_QUOTES', 'Dalam beberapa jam Anda akan menerima penawaran dari penyedia jasa professional yang bersedia dipekerjakan disertai estimasi harga dan informasi kontak untuk dihubungi.');
define('SELECT_SERVICE_PROVIDERS', 'Pilih dan pekerjakan penyedia jasa professional yang cocok');
define('CHOOSE_IMPLEMENTATIONS', 'Pilih dari beberapa pelaksana jasa yang cocok untuk disewa. Pelaksana jasa tersebut akan segera menghubungi Anda untuk bertemu dan memenuhi kebutuhan Anda.');
define('ARE_YOU_SERVICE_PROVIDER', 'Apakah Anda penyedia jasa?');
define('GET_NEW_CLIENT', 'Dapatkan klien baru');
define('SEEKMI_FWD_CLIENT', 'Seekmi akan meneruskan permintaan klien akan kebutuhan penyedia jasa professional di wilayahnya. Anda yang akan memutuskan apakah Anda tertarik atau tidak untuk memberikan penawaran untuk klien tersebut.');
define('REGISTER_FREE', 'Daftar Gratis');
define('FIND_PROFESSIONAL_SERVICE', 'Find professionals for these services');
define('RIGHT_NOW', 'Right now, you can find, compare and hire professionals for these types of services. We are expanding
                                            every day and soon you will be able to find professionals for almost any anything.');
define('CUSTOMER_GET_DONE', 'Thousand of customers got things done through Seekmi');
define('GET_INTRODUCED', 'Get introduced to pros');
define('WHAT_SERVICE', 'What service do you need?');
define('PAINTER_PLACEHOLDAER', 'Painter, DJ, Tutor');
define('CONTINUE_BUTTON', 'Continue');
define('AS_FEATURED', 'As Featured in');
define('HEADING_OUT', 'Get Things Done on the Go!');
define('RECIVE_QUOTES_WHEREVER', 'We\'ve made it even easier for you to get things done. Send requests and receive quotes on the go.');
define('PHONE_NUMBER_PLACEHOLDER', 'Nomor Telepon');
define('EMAIL_PLACEHOLDER','E-mail');
define('THOUSANDS_PROFESSINAL', ' Thousands of professionals are growing their business across the Indonesia.');
define('PRO_STORIES', 'Pro Stories');
define('ARE_YOU_PROFESSIONAL', 'Are you a professional looking to grow your business?');
define('BUTTON_SIGN_UP_PRO', 'Sign up as a pro');
define('MENU_PRIVACT__POLICY', 'Privacy Policy');
define('MENU_TERMS', 'Terms of Use');
define('MENU_ABOUT', 'About');
define('MENU_WORK', 'Pekerjaan');
define('MENU_NEWS', 'News');
define('MENU_HEAD_CUSTOMER', 'Customers');
define('MENU_HOWITWORKS', 'How it Works');
define('MENU_SAFETY', 'Safety');
define('MENU_DOWNLOAD_IPHONE_APP', 'Download iPhone App');
define('MENU_HEAD_PROS', 'Pros');
define('MENU_SIGNUP', 'Sign Up');
define('MENU_HEAD_QUESTIONS_NEED_HELP', 'PERTANYAAN? PERLU BANTUAN?');
define('MENU_FAQ','FAQ');
define('MENU_EMAIL', 'E-mail kami');
define('MENU_CREDIT', 'Credit');
define('MENU_BLOG', 'Blog');
define('HOME_SEARCH_PLACEHOLDER', 'What services do you need?');
define('BUTTON_GET_STARTED', 'Get Started');

/* services/Search Page Constats */
define('SEARCH_WITHIN_TEXT', 'Within hours, we’ll introduce you to several interested and available
                this service near Scotch&nbsp;Plains. You’ll be able to compare
                quotes, messages, reviews and profiles. When you’re ready, hire the
                right professional.');
define('SEARCH_HOWDOES', 'How does Seekmi work?');

/** Header view ** */
define('HEADER_SIGNIN', 'Sign in');
define('HEADER_LOGOUT', 'Logout');

/* Login Page  constatnts */
define('LOGIN_WELCOME_BACK', 'Welcome back to Seekmi');
define('LOGIN_USERNAME_ERROR', 'Your username or password was incorrect.');
define('LOGIN_SIGNUP_TEXT', 'If you don\'t have an account, please');
define('LOGIN_SIGNUP', 'Sign Up');
define('LOGIN_EMAIL', 'Email');
define('LOGIN_PASS', 'Password');
define('LOGIN_REM', 'Remember me');
define('BUTTON_LOGIN', 'Log In');
define('LOGIN_FBCONNECT', 'Facebook Connect');
define('LOGIN_SIGNUP_WITH_GOOGLE', 'Sign up with Google ');
define('LOGIN_FORGOTPASS', 'Forgot your password?');
define('LOGIN_DONTHAVEACC', 'Don\'t have an account?');

/* Register page constatnts */
define('REGISTER_CREATE_TEXT', 'Create your account');
define('REGISTER_FNAME', 'Nama Depan');
define('REGISTER_LNAME', 'Nama Belakang');
define('REGISTER_EMAILADD', 'Alamat Email');
define('REGISTER_CREATEPASS', 'Create password');
define('REGISTER_TERMSTEXT', 'By clicking Create, you indicate that you have read and agree to the');
define('REGISTER_TERMS', 'Terms of Use');
define('REGISTER_PRIVACY', 'Privacy Policy');
define('BUTTON_CREATEACCOUNT', 'Create Account');
define('REGISTER_SIGNUPFB', 'Signup with Facebook');
define('REGISTER_WALLMSG', 'We won\'t post on your wall without your permission.');

/* Reset Password Page Constatnts*/
define('RESET_TEXT','Reset your password');
define('RESET_EMAIL_TEXT','What\'s your email address?');
define('BUTOON_RESETPASS','Reset Your Password');
define('EMAIL_NOT_EXIST_ERROR','There is no account with this email.');
define('USER_INACTIVE_RESET_ERROR','Mohon aktivasi email Anda untuk permintaan reset password');
define('USER_SUSPEND_RESET_ERROR','Maaf, akun Anda kami tangguhkan. Untuk mengativasikan kembali akun Anda, silahkan hubungi kami di <a href="mailto:support@seekmi.com">support@seekmi.com</a>');
define('USER_BANNED_RESET_ERROR','Maaf, akun Anda kami blokir. Untuk meminta akun Anda dievaluasi kembali, silahkan hubungi kami di <a href="mailto:support@seekmi.com">support@seekmi.com</a>');


/*Dashboard Page Constatnts*/
define('DASH_PROJECT_TEXT','Projects');
define('DASH_ADD_PROJECT','Start new project');
define('DASH_UPDATE_STATUS','Update status');
define('DASH_COMPLETE','Complete');
define('DASH_NO_PROFESSIONAL','No professionals available');
define('DASH_UNFORTUNATELY_TEXT','Unfortunately, no one was available, interested, and qualified to meet your');
define('DASH_NEEDS_TEXT','needs');
define('DASH_HIRING_TEXT','Congrats on hiring');
define('DASH_AVAIL_TEXT',' are available.');
define('DASH_HEADING_TEXT',' Heading out? Bring Seekmi with you!');
define('DASH_WEHAVE_TEXT',' We\'ve made it even easier to accomplish your projects. Send requests and
                receive quotes wherever you are.');
define('BUTTON_TEXTMELINK','Text me a link');
define('VAL_REQFNAME','Please enter your first name');
define('VAL_REQLNAME','Please enter your last name');
define('VAL_MINLENGTH','Too short');
define('VAL_REQEMAIL','You must enter an email');
define('VAL_EMAIL','Please enter a valid email');
define('VAL_EMAIL_EXISTS','Sorry, that email is already taken');
define('VAL_REQPASS','You must enter a password');
define('VAL_PASSMIN_LENGTH','Too short (minimum of 5 characters)');

/*User Settings Page Constatnts*/
define('SETTING_ACCOUNT_TITLE', 'Account');
define('BUTTON_ADD', 'Add');
define('BUTTON_EDIT', 'Edit');
define('BUTTON_DELETE_ACC', 'Delete account');
define('SETTING_ANNOUNCE_TEXT', 'Seekmi Announcements');
define('SETTING_UPDATE_TEXT', 'Updates');
define('SETTING_PROFILETIPS_TEXT', 'Profile tips');
define('SETTING_SPECOFFER_TEXT', 'Special offers');
define('SETTING_SURVEYS_TEXT', 'Surveys');
define('VAL_UPLOAD_ERROR', 'An error occurred and the upload failed.');

/* Edit Account Setting page Constatns*/
define('BUTTON_BACKTOSETTINGS','Back to Settings');
define('EDITACC_EDIT_TEXT','Edit Personal Information');
define('EDITACC_NAME_TEXT','Name');
define('EDITACC_TIMEZONE_TEXT','Time zone');
define('BUTTON_SAVECHANGES_TEXT','Save changes »');
define('BUTTON_CANCEL_TEXT','Cancel');
define('EDITACC_REFERRED_BY','Diperkenalkan oleh');
define('EDITACC_REFERRAL_INVALID_ERROR_TEXT','This user not exist with Seekmi.');
define('EDITACC_REFERRAL_ALREADY_ERROR_TEXT','You have already referred by this user.');

/*Change Password page constants*/
define('CHANGEPASS_EDITPASS_TEXT','Edit Password');
define('CHANGEPASS_CURRPASS_TEXT','Current password');
define('CHANGEPASS_NEWPASS_TEXT','New password');
define('CHANGEPASS_CONFPASS_TEXT','Confirm new password');
define('BUTTON_SAVEPASS','Save Password');
define('VAL_CURREPASS','Your current password was not correct.');

/*Delete Account Page Constatnts*/
define('DEL_ACOOUNT_TEXT','Delete Your Account');
define('DEL_AREYOU_TEXT','Are you sure you want to delete your account?');
define('DEL_LOSECONTENT_TEXT','When you delete your account, you will lose all your project and profile
                    information and any reviews you\'ve written or received on Seekmi.');
define('DEL_RECIVEMAIL_TEXT','If you are receiving too many emails from us, then you can');
define('DEL_CHANGE_NOTIFICATION_TEXT','change your notification settings');
define('DEL_HELP_TEXT','We\'d hate to see you go! If there\'s anything we can do to help, email us at');
define('DEL_SUPPORT_EMAIL','support@seekmi.com');
define('DEL_CALL_TEXT','or call us at (415) 779-2191.');
define('DEL_REASON_TEXT',' Reason for deleting your account (optional)');
define('DEL_CONFIRM_TEXT',' I confirm I want to delete');
define('DEL_CONFIRM_REMAIN_TEXT',' \'s account');

/*Project detail page constatnts*/
define('DETAIL_QUOTE_TEXT','Quote from');
define('DETAIL_FOR_TEXT','for ');
define('DETAIL_INTRODUCTION_TEXT','Introductions to');
define('DETAIL_CONTACTSUPPORT_TEXT','Contact support');
define('DETAIL_VIEWREQUEST_TEXT','View my request');
define('DETAIL_REVIEWS_TEXT','Reviews');
define('DETAIL_IDR_TEXT','IDR ');
define('DETAIL_VIEWPROFILE_TEXT','View Profile');
define('DETAIL_PHOTOS_TEXT','Photos');
define('DETAIL_WEBSITE_TEXT','Website');
define('DETAIL_TOTALPRICE_TEXT','total price');
define('BUTTON_SEND','Send');
define('DETAIL_DECLINE_TEXT','You declined this quote, ');
define('BUTTON_REPLY','Reply');
define('BUTTON_CALL','Call');
define('BUTTON_HIRE','Hire');
define('BUTTON_DECLINE','Decline');
define('DETAIL_LEAVE_REVIEW_TEXT','Leave a review');
define('DETAIL_DECLINEREASON_TEXT','Please tell us why you declined this quote.');
define('DETAIL_RESPONSE_TEXT','Your responses here are just between you and Seekmi.');
define('DETAIL_REASON_TEXT1','I\'ve decided on someone else');
define('DETAIL_REASON_TEXT2','I think the price is too high');
define('DETAIL_REASON_TEXT3','The pro is too far away');
define('DETAIL_REASON_TEXT4','This is not what I need');
define('DETAIL_REASON_TEXT5','I don\'t need this work done anymore');
define('DETAIL_REASON_TEXT6','Other');
define('DETAIL_MSGSTEP_TEXT','You\'ll still be able to message the pro after this step.');
define('DETAIL_PROMSG_TEXT','Hi, is this pro a good fit for you?');
define('DETAIL_SEEKMISUPPORT_TEXT','Seekmi Support');
define('DETAIL_HELP_TEXT','We\'re always here to help. Give us a call or send us an email');
define('BUTTON_SUBMIT','Submit');
define('BUTTON_SKIP','Skip');
define('BUTTON_UNDO','Undo');
define('BUTTON_NO','No');
define('BUTTON_YES','Yes');

/* Edit Profile page constants*/
define('EDITPRO_WELCOME_TEXT','Welcome to your profile!');
define('BUTTON_EDIT_PROFILE','Edit your profile');
define('BUTTON_DONE_EDIT','Done editing');
define('EDITPRO_EDITING_TEXT','You are now editing your profile.');
define('EDITPRO_UPLOADPIC_TEXT','Upload Picture.');
define('EDITPRO_BIO_LABEL','Bio.');
define('EDITPRO_TELLSTORY_TEXT','Tell your story!');
define('EDITPRO_SERVICES_TEXT','Services');
define('EDITPRO_SHARE_TEXT','Share your social media and website');
define('EDITPRO_YEARSINBUSS_TEXT','years in business');
define('EDITPRO_EMPLOYEES_TEXT','employees');
define('EDITPRO_TRAVELS_TEXT','Travels up to');
define('EDITPRO_MILES_TEXT','miles');
define('EDITPRO_MILE_TEXT','mile');
define('EDITPRO_CREDENTIALS_TEXT','Credentials');
define('EDITPRO_NOCREDENTIALS_TEXT','No credentials yet on file');
define('EDITPRO_Q&A_TEXT','Questions &amp; Answers');
define('EDITPRO_ANS_TEXT','Answer some frequently asked questions about your service.');
define('EDITPRO_COMPJOB_TEXT','There are no reviews for Horizon House.');
define('EDITPRO_COMPLETEJOB_TEXT','Complete jobs and get reviews.');
define('EDITPRO_EDITINFO_TEXT','Edit Your Basic Info');
define('EDITPRO_BUSSNAME_TEXT','Business name');
define('EDITPRO_BUSSLOGO_TEXT','Business logo');
define('EDITPRO_OPTIONAL_TEXT','Optional');
define('EDITPRO_TAGLINE_TEXT','Tagline');
define('EDITPRO_ADD_CON_TEXT','Address and Contact');
define('EDITPRO_STREETADD_TEXT','Street address');
define('EDITPRO_PROVINCE_TEXT','Province');
define('EDITPRO_SELECT_TEXT','--Select--');
define('EDITPRO_CITY_TEXT','City');
define('EDITPRO_DIST_TEXT','District');
define('EDITPRO_POSTAL_TEXT','Zip code');
define('EDITPRO_ADDRESONPRO_TEXT','Show my full address on my profile');
define('EDITPRO_CITYONPRO_TEXT','Show only the city/state on my profile');
define('EDITPRO_PROFCOMP_TEXT','Profile Completion');
define('EDITPRO_MANAGEMEDIA_TEXT','Manage Your Media');
define('EDITPRO_ADDMEDIA_TEXT','Add media to your gallery');
define('EDITPRO_REFER_TEXT','Refer Friend & Family');
define('EDITPRO_ENTEREMAIL_TEXT','Enter email address');
define('EDITPRO_TIP_TEXT','Tip:');
define('EDITPRO_EXAMPLES_TEXT','Examples:');
define('EDITPRO_QUEANS_TEXT','Questions and Answers');
define('EDITPRO_NOQUEANS_TEXT','0 questions answered');
define('EDITPRO_PICKEXAMPLE_TEXT','Pick 8-10 examples of your best work to demonstrate your capabilities');
define('EDITPRO_ANSEXTRA_TEXT','Answer some extra questions about your services');
define('EDITPRO_CLIENTSTREAT_TEXT','Clients treat your responses as a pre-interview. Professionals who answer 8-10 questions and demonstrate expertise are much more
                    likely to be hired.');
define('EDITPRO_TELLCUSTOMER_PLACEHOLDER','Tell customers about yourself and what you love about your work. Authentic and meaningful information about who you are is important to establishing a relationship and building trust with a new customer.');
define('EDITPRO_IBECAME_TEXT','   “I became a personal trainer to help people transform their
                            lives and I want to see you accomplish more than you can on your
                            own.  It is difficult to find the time and motivation, so I will
                            push you to accomplish your goals.”');
define('EDITPRO_IAM_TEXT',' “I am a San Francisco-based portrait and product
                            photographer.  My career began in HS where I took photos of
                            student groups for the yearbook. Since then, I\'ve been dedicated
                            to capturing the beauty and energy of people and their work.”');
define('EDITPRO_DESCRIBESERVICE_TEXT', 'Describe the Services You Provide');
define('EDITPRO_MANAGEBUSSLINKS_TEXT', 'Manage Your Business Links');
define('EDITPRO_EDITBUSS_TEXT', 'Edit Your Business Info');
define('EDITPRO_UPLOADLOGO_TEXT', 'Upload your logo');
define('EDITPRO_YEARFOUND_TEXT', 'Year founded');
define('EDITPRO_NOOFEMP_TEXT', 'Number of employees');
define('EDITPRO_TRAVELPREF_TEXT', 'Travel preferences (check all that apply)');
define('EDITPRO_ITRAVEL_TEXT', 'I travel to my customers');
define('EDITPRO_CUSTTRAVEL_TEXT', 'My customers travel to me');
define('EDITPRO_NEITHER_TEXT', 'Neither (phone or internet only)');
define('EDITPRO_HOWFAR_TEXT', 'How far are you willing to travel?');
define('EDITPRO_LISTOFSERVICE_PLACEHOLDER', 'List the kinds of services you provide and explain why future clients should hire you.');
define('EDITPRO_LISTOFSERVICE_HELP', '(100 characters minimum)');
define('EDITPRO_IFYOUWANT_TEXT', '  If you want fabulous, professional images that make you and yours
                        look your best &ndash; Jane\'s Photography is right for you.
                        Whether you need executive head-shots, personality photos,
                        model portfolio images, performance images, or family portraits,
                        we will work closely with you to find out exactly how you want
                        to be portrayed and produce the best possible results.
                        Plus, you\'ll have a good time doing it!
                        <br><br>
                        I am a photographer with a love for storytelling. My pricing is
                        reasonable and I can create custom packages to accommodate
                        your budget.
                        <br><br>
                        My clients include the Mayor of San Francisco, numerous members
                        of the city council, and many private clients including
                        the corporate photos for Cisco\'s corporate suite.');
define('BUTTON_EDITMEDIA','Edit media');
define('BUTTON_PICTURE','Picture');
define('BUTTON_SAVING','Saving');
define('BUTTON_DELETING','Deleting');
define('BUTTON_DELETE','Delete');
define('BUTTON_BACK','Back');
define('BUTTON_DELETELOGO','Delete logo');
define('BUTTON_VIDEO','Video');
define('BUTTON_MANAGEREVIEW','Manage Reviews');
define('BUTTON_SAVE_CONT','Save &amp; Continue ');
define('EDITPRO_GETREVIEW_TEXT', 'Get Reviews');
define('EDITPRO_NOREVIEW_TEXT', 'Your service has 0 reviews.');
define('EDITPRO_MANAGEREVIEW_TEXT', 'Manage your reviews.');
define('EDITPRO_SERVICENOREVIEW_TEXT', 'Your service doesn\'t have any reviews yet');
define('EDITPRO_ASINGLEREVIEW_TEXT', 'A single review makes you twice as likely to be hired on Seekmi!');
define('EDITPRO_EMAILPAST_TEXT', 'Email past clients to visit the URL below and leave you a review');
define('EDITPRO_TAKEALOOK_TEXT', 'take a look');
define('EDITPRO_EMAILIT_TEXT', 'Email it');
define('EDITPRO_TWEETIT_TEXT', 'Tweet it');
define('EDITPRO_SHAREIT_TEXT', 'Share it');
define('EDITPRO_TOPASTCLIENT_TEXT', 'to your past clients');
define('EDITPRO_TOFOLLOWERS_TEXT', 'to your followers');
define('EDITPRO_ONFACEBOOK_TEXT', 'on Facebook');
define('MENU_CONTACTUS', 'Contact Us');


/*Add Review Page Constants */
define('ADDREVIEW_LEAVEREVIEW_TEXT','Leave a review for');
define('ADDREVIEW_VIEWPROFILE_TEXT','View full profile');
define('ADDREVIEW_THANKYOU_TEXT','Thank you for submitting review for');
define('ADDREVIEW_ALLREADYREVIEW_TEXT','You\'ve already reviewed this bid.');
define('ADDREVIEW_YOURRATING_TEXT','Your rating');
define('ADDREVIEW_CLICKTORATE_TEXT','click to rate');
define('ADDREVIEW_YOURRATE_TEXT','Your message');
define('ADDREVIEW_POSTREVIEW_TEXT','Post Review');
define('ADDREVIEW_HI_TEXT','Hi');
define('ADDREVIEW_THISIS_TEXT',' This is what your review page looks like. You won\'t be able to leave a
                        review for yourself, but this is what people who come to this page will see
                        (minus this notice, of course).');
define('BUTTON_HIDENOTICE','hide this notice');
define('ADDREVIEW_THANKS_TEXT','Thanks!');
define('ADDREVIEW_ITLOOKS_TEXT','It looks like you\'ve already reviewed');
define('ADDREVIEW_THANKHELP_TEXT','Thank you for helping to improve the Seekmi community.');
define('ADDREVIEW_LETPEOPLE_TEXT','Let people know how your experience went');
define('ADDREVIEW_DESCRIBE_TEXT','Describe the job');
define('ADDREVIEW_WHATDID_TEXT','What did');
define('ADDREVIEW_HELPYOU_TEXT','help you with? Where was the job?<br>How long did it take?');
define('ADDREVIEW_SAY_TEXT','Say what went well');
define('ADDREVIEW_WHATIMPRE_TEXT','What impressed you about the service?<br>Did ');
define('ADDREVIEW_GOABOVE_TEXT','go above and beyond?<br>Was ');
define('ADDREVIEW_ONTIME_TEXT','on time?<br>How was the quality of the work?');
define('ADDREVIEW_POINT_TEXT','Point out what could have been better');
define('ADDREVIEW_DOBETTER_TEXT',' do better in the future?');
define('ADDREVIEW_WHATWOULD_TEXT','What would have improved your experience?
                    What went wrong, if anything?
                    How could ');

/*Profile Request Page Constants*/
define('PROFILEREQ_CUSTREQ', ' Customer Requests');
define('PROFILEREQ_VIEWED', ' Viewed');
define('PROFILEREQ_NOTTHE_TEXT', ' Not the right type of requests?');
define('BUTTON_NEW', ' New');
define('BUTTON_ACCEPT', ' Accept');
define('BUTTON_HIRED', ' Hired');
define('BUTTON_ARCHIVED', 'Archived');
define('LABEL_PAGE', ' page');
define('LABEL_RECORDS', ' records');
define('LABEL_COMPLETED', 'Completed');
define('LABEL_DECLINED', 'Declined');
define('PROFILEREQ_INRECORDS', 'In Progress');


/*Submit Quote Page Constatnts*/
define('SUBMITQUOTE_SUBMIT_TEXT','Submit a quote');
define('SUBMITQUOTE_CHARTOO_TEXT','characters too long');
define('SUBMITQUOTE_TIPS_TEXT','Tips from top pros');
define('SUBMITQUOTE_SUBMITQUOTE_TEXT','quote have been submitted');
define('SUBMITQUOTE_WOULD_TEXT','would still like you to submit a quote.');
define('SUBMITQUOTE_NEED_TEXT','Need help quoting?');
define('SUBMITQUOTE_TIPSTO_TEXT','Tips to write winning messages');
define('SUBMITQUOTE_GREET_TEXT','Greet them by name.');
define('SUBMITQUOTE_TALK_TEXT','Talk about your relevant past work, qualifications, and expertise.');
define('SUBMITQUOTE_DESCRIBE_TEXT','Describe what\'s included in the price.');
define('SUBMITQUOTE_ENCOURAGE_TEXT','Encourage them to take the next step and follow up!');
define('SUBMITQUOTE_YOURMESSAGE_TEXT','Your message forms your customer\'s first impression of you and your business.
                        They love to hire pros who:');
define('LABEL_PRICE','Price');
define('LABEL_FIXEDPRICE','Fixed price');
define('LABEL_HOURLYRATE','Hourly rate');
define('LABEL_NEEDMORE','Need more info');
define('LABEL_MESSAGE','Message');
define('LABEL_REQUESTSUBMIT','Request submitted');
define('LABEL_NOTENOUGH','Not enough info?');
define('LABEL_SUGGEST','Suggest a question');
define('LABEL_CALLUS','Call us at ');
define('BUTTON_RETURNTOQUOTE','Return to Quote');
define('SUBMITQUOTE_CREDIT_ERROR_TEXT','You don\'t have enough credit to submit this quote. Please buy credits.');

/*Signup Pros Register Page Constants*/
define('SIGNUPREGI_HOW_TEXT', 'Bagaimana cara kami menghubungi Anda?');
define('SIGNUPREGI_GETNEWCLIENTS_TEXT', 'Dapatkan klien baru dan kembangkan bisnis Anda');
define('SIGNUPREGI_TELLUS_TEXT', 'Beritahu kami dimana kami bisa mengirimkan permintaan pelanggan untuk layanan Anda.');
define('SIGNUPREGI_PUTALL_TEXT', 'Masukan semua layanan yang Anda berikan dipisah dengan koma ');
define('SIGNUPREGI_NOTE_TEXT', '*Note: Untuk perusahaan, silahkan masukkan pihak yang berwenang dari perusahaan Anda untuk menerima permintaan pekerjaan dari pelanggan yang akan dikirimkan oleh Seekmi. ');
define('LABEL_REPEATPASS','Ulangi Password');
define('LABEL_NOTIFYME',' Beritahu saya melalui SMS apabila ada permintaan pekerjaan  dan pesan baru');
define('LABEL_BYCHECKINGTREMS',' Dengan mencentang kotak “Syarat-syarat dan Ketentuan” di dalam pendaftaran akun kami, 
                            atau mendaftarkan PT. Seekmi Global Services (seperti yang dijelaskan di bawah), 
                            maka Anda setuju bahwa Anda telah membaca, mengerti dan menerima syarat-syarat dan 
                            ketentuan yang dijabarkan di ');
define('LABEL_CONDOFSERVICES',' (“Ketentuan-Ketentuan Layanan”) 
                            dan Anda setuju untuk terikat oleh Ketentuan-Ketentuan Pelayanan tersebut dan seluruh 
                            ketentuan, kebijakan dan pedoman yang dimuat di dalam Ketentuan-Ketentuan Layanan rujukan, 
                            termasuk, namun tidak terbatas pada Kebijakan Privasi PT. Seekmi Global Service yang 
                            ada di ');
define('LABEL_PRIVACYPOLICY','(“Kebijakan Privasi) (atau URL serupa yang mungkin 
                            disediakan oleh Seekmi dari waktu ke waktu) (yang secara bersama-sama, disebut dengan 
                            “Perjanjian”). Bila Anda tidak menyetujui Perjanjian ini, maka Anda tidak dapat menggunakan 
                            Layanan-Layanan (seperti yang dijelaskan di bawah) sama sekali. Layanan-Layanan tersebut 
                            ditawarkan kepada Anda dengan mensyaratkan persetujuan dari Anda atas Perjanjian ini tanpa 
                            perubahan apapun, termasuk tanpa pembatasan, hak Seekmi untuk menggunakan seluruh data yang 
                            dikumpulkan dan dianalisis oleh PT. Seekmi Global Services.');
define('LABEL_LIST','Daftar');
define('LABEL_RETURN','Kembali');
define('SIGNUPREGI_SMS_TEXT',' Layanan operator SMS dan Data Anda akan terdaftar ke pesan 
                            singkat kami. Anda tidak diharuskan untuk menyetujui untuk 
                            menerima pesan teks sebagai syarat menggunakan layanan Seekmi.');
define('SIGNUPREGI_CONTROL_TEXT', 'Kontrol dan saring semua permintaan yang Anda terima');
define('SIGNUPREGI_REGISTER_TEXT', 'Daftar dan gunakan layanan kami secara gratis');


/* End of file constants.php */
/* Location: ./application/config/ind_constants.php */