<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="mainimage2" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$page_title?></title>
<meta content="<?=$this->config->config['base_url']?>pros/register" property="og:url"/>
<meta content="<?=$page_title?>" property="og:title"/>
<meta content="Seekmi" property="og:site_name"/>
<meta property="og:description" content="Get things done by hiring experienced local service professionals." />
<meta property="og:image" content="<?=$this->config->config['base_url']?>images/LogoSymbolWordSquare.png"/>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/signup.css">
 <style type="text/css">
    .help-block {
    color: #d51818;
    display: block;
    margin-bottom: 10px;
    margin-top: 5px;
    font-size: 11.2px;
   }
   .wrapper.content {
    margin: 20px auto !important;    
   }   
   .has-error .checkbox, .has-error .checkbox-inline, .has-error label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label {
    color: #d51818;
   }
   .has-error input,.has-error select,.has-error textarea{
      border-color: #a94442;
      box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset; 
   }
   
      
 </style>
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-responsive primo-fluid box-shadow multiple-backgrounds">
    <?php include('common_view.php'); ?>
<div class="glorious-header glorious-header1" data-section="header">
<div class="wrapper">
    <div class="row header-row">
        <div class="header" style="margin-bottom:10px;">
    <a class="logo" href="<?=$this->config->config['base_url']?>"><img alt="Seekmi" src="<?=$this->config->config['base_url']?>images/seekmi_white.png"></a>
    </div>
  </div>
</div>
</div>
<div class="wrapper content">
    <div>
        <h1 style="" class="header-copy">            
        </h1>
         <div id="main_div_5">
            <form novalidate class="box account" name="signup_uber_form" method="POST" id="signup_uber_form" action="<?=$this->config->config['base_url']?>uber/submit">
                <div class="box-header">
                   <h2>Tingkatkan pendapatanmu dengan menjadi supir!</h2>
                    <h3>Temukan klien yang mencari supir untuk penyewaan sehari atau jangka panjang di Seekmi, rekanan Uber.</h3> 
                    <h3>GRATIS!<br/>
                    Daftar sekarang dan gunakan Seekmi untuk menemukan pekerjaan menyetir (supir) hanya Rp.0 selama 6 bulan!</h3>
                </div>                
                <div class="box-content">
                    <fieldset>
                        <div class="dynamic-row">
                            <div class="form-field-name-segment form-field form-field" expr="account.firstName" field="">
                               <div>
                                <label for="first-name">Nama Depan</label>
                                <input type="text" minlength="2" required="" name="firstName" id="first-name">
                               </div>
                            </div>
                            <div class="form-field-name-segment form-field form-field" expr="account.lastName" field="">
                              <div>
                                <label for="last-name">Nama Belakang</label>
                                <input type="text" minlength="2" required="" name="lastName" id="last-name">
                              </div>
                            </div>
                        </div>
                        <div class="dynamic-row">
                            <div class="form-field-name-segment form-field form-field" expr="account.email" field="">
                              <div>
                                <label for="usr_email">Alamat Email</label>
                                <input type="email" required="" name="usr_email" id="usr_email">
                              </div>
                            </div>
                            <div class="form-field-name-segment form-field form-field" expr="account.phone" field="">
                             <div>
                                <label for="phone">Nomor Telepon</label>
                                <input type="phone" pattern="[0-9]*" required="" name="phone" id="phone">
                              </div>
                            </div>
                        </div> 
                        <div class="dynamic-row">
                            <div class="form-field-name-segment form-field form-field" expr="account.password" field="">
                                <div>
                                <label for="password">Password</label>
                                <input type="password" minlength="5" name="password" id="password">
                                </div>
                            </div>
                            <div class="form-field-name-segment form-field form-field" expr="account.confpassword" field="">
                                <div>
                                <label for="confirm-password">Ulangi Password</label>
                                <input type="password" minlength="5" name="confirmPassword" id="confirmPassword">
                                </div>
                            </div>
                        </div>
                        <div class="form-field-sms-preference form-field form-field" expr="account.sms" field="">
                            <div>
                            <label class="inline">
                                <input type="checkbox" name="sms_notification" value="1">
                                Beritahu saya melalui SMS apabila ada permintaan pekerjaan  dan pesan baru                                
                            </label>
                            </div>
                        </div>
                    </fieldset>                  
                </div>
                <div class="form-field form-field-nav box-footer">
                    <div class="conditions" style="margin-bottom:35px; margin-top:0px;">
                        <p>
                            <input type="checkbox" name="agree_check" value="1" class="radio tocustomer_check" step="vertical-align:top;"/>&nbsp;
                            Dengan mencentang kotak “Syarat-syarat dan Ketentuan” di dalam pendaftaran akun kami, 
                            atau mendaftarkan PT. Seekmi Global Services (seperti yang dijelaskan di bawah), 
                            maka Anda setuju bahwa Anda telah membaca, mengerti dan menerima syarat-syarat dan 
                            ketentuan yang dijabarkan di <a href="<?=$this->config->config['base_url']?>terms/" target="_blank">http://www.seekmi.com/terms</a> (“Ketentuan-Ketentuan Layanan”) 
                            dan Anda setuju untuk terikat oleh Ketentuan-Ketentuan Pelayanan tersebut dan seluruh 
                            ketentuan, kebijakan dan pedoman yang dimuat di dalam Ketentuan-Ketentuan Layanan rujukan, 
                            termasuk, namun tidak terbatas pada Kebijakan Privasi PT. Seekmi Global Service yang 
                            ada di <a href="<?=$this->config->config['base_url']?>privacy/" target="_blank">http://www.seekmi.com/privacy</a> (“Kebijakan Privasi) (atau URL serupa yang mungkin 
                            disediakan oleh Seekmi dari waktu ke waktu) (yang secara bersama-sama, disebut dengan 
                            “Perjanjian”). Bila Anda tidak menyetujui Perjanjian ini, maka Anda tidak dapat menggunakan 
                            Layanan-Layanan (seperti yang dijelaskan di bawah) sama sekali. Layanan-Layanan tersebut 
                            ditawarkan kepada Anda dengan mensyaratkan persetujuan dari Anda atas Perjanjian ini tanpa 
                            perubahan apapun, termasuk tanpa pembatasan, hak Seekmi untuk menggunakan seluruh data yang 
                            dikumpulkan dan dianalisis oleh PT. Seekmi Global Services.
                        </p>
                    </div>
                    <div class="nav-container" role="presentation">                      
                        <button  class="bttn" type="submit" style="width:100%">
                           <span>Dafter Gratis</span>                           
                        </button>                        
                    </div>   
                     <div class="conditions">                       
                        <p>
                            Layanan operator SMS dan Data Anda akan terdaftar ke pesan 
                            singkat kami. Anda tidak diharuskan untuk menyetujui untuk 
                            menerima pesan teks sebagai syarat menggunakan layanan Seekmi.
                        </p>
                    </div>
               </div> 
            </form>
        </div>        
        
    </div>
</div>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.validate.js"></script>
<script type="text/javascript">
var form = $("#signup_uber_form");
var validator = form.validate({
        errorElement: 'span',
        errorClass: 'help-block',                    
        highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-field').addClass("has-error");
        },
        unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-field').removeClass("has-error");
        },
        rules: {
             firstName:{
                required: true 
             },
             lastName:{
                required: true 
             },
             usr_email:{
                required: true,
                email:true
                //remote: "<?=$this->config->config['base_url']?>user/check_email_exists"
             },
             phone:{
                required: true 
             },
             password:{
                required: true,
                minlength: 5
             },
             confirmPassword:{
                required: true,
                equalTo: "#password"
             },
             agree_check:{
                required: true
             }
        },
        messages:{               
               /*usr_email: {                               
                    remote: "Sorry, that email is already taken"
               }*/

        }
});
$('#usr_email').blur(function() {
    $('#usr_email').val($.trim($('#usr_email').val()));
    validator.element('#usr_email');
});
</script>
</body>
</html>
      
      
     