<!doctype html>
<html lang="en" class="no-js">
<!-- Mirrored from nunforest.com/site/element-demo/boxed/pricing.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Jan 2015 06:53:45 GMT -->
<head>
    <title><?=$page_title?> | Montego</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/bootstrap.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/magnific-popup.css" media="screen">	
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/animate.css" media="screen">
<!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/style.css" media="screen">
    
</head>
<body id="top">
<? include('header_view.php'); ?>
<!-- ####################################################################################################### -->
<div id="content">
<!-- page-banner-section -->
<div class="section-content page-banner-section2">
        <div class="container">
                <div class="row">
                        <div class="col-sm-6">
                                <h1>Terms of Service </h1>
                        </div>
                        <div class="col-sm-6">
                                <ul>
                                        <li><a href="<?=$this->config->config['base_url']?>">Home</a></li>
                                        <li>Terms of Service </li>
                                </ul>
                        </div>
                </div>
        </div>
</div>

<!-- blog-section  -->
<div class="section-content blog-section blog-onlytext">
        <div class="container">
                <div class="blog-box">

                        <div class="blog-post">
                                <div class="autor-post">

                                </div>
                                <h2><a href="single-post.html">Mauris molestie nulla eget scelerisque.</a></h2>
                                <ul class="post-tags">
                                        <li><a href="#">16 December 2014</a></li>
                                        <li><a href="#">Business</a></li>
                                        <li><a href="#">Photography</a></li>
                                        <li><a href="#">Leave Comment</a></li>
                                </ul>
                                <p>Duis a justo vitae arcu blandit rhoncus. Aenean blandit, nibh at hendrerit placerat, ante nies dui, ac feugiat liguc maPellentesque venenatis tin cidunt turpis sit amet placerat. Nullam nec vehicula enim, nec ultrices lacus. Vivamus aliquet turpis vitae porta rutrum. Nullam quis libero et arcu tristique auctor eget et ligula. Fusce nulla nibh, mollis quis lacinia in, venenatis ac tortor. Aenean nunc urna, gravida mattis lorem ut, consequat elementum eros. </p>

                        </div>

                        <div class="blog-post">
                                <div class="autor-post">

                                </div>
                                <h2><a href="single-post.html">Fusce dictum risus at commodo consequat.</a></h2>
                                <ul class="post-tags">
                                        <li><a href="#">16 December 2014</a></li>
                                        <li><a href="#">Business</a></li>
                                        <li><a href="#">Photography</a></li>
                                        <li><a href="#">Leave Comment</a></li>
                                </ul>
                                <p>Duis a justo vitae arcu blandit rhoncus. Aenean blandit, nibh at hendrerit placerat, ante nies dui, ac feugiat liguc maPellentesque venenatis tin cidunt turpis sit amet placerat. Nullam nec vehicula enim, nec ultrices lacus. Vivamus aliquet turpis vitae porta rutrum. Nullam quis libero et arcu tristique auctor eget et ligula. Fusce nulla nibh, mollis quis lacinia in, venenatis ac tortor. Aenean nunc urna, gravida mattis lorem ut, consequat elementum eros. </p>

                        </div>

                        <div class="blog-post">
                                <div class="autor-post">

                                </div>
                                <h2><a href="single-post.html">Vestibulum dictum ac eros eget rutrum.</a></h2>
                                <ul class="post-tags">
                                        <li><a href="#">16 December 2014</a></li>
                                        <li><a href="#">Business</a></li>
                                        <li><a href="#">Photography</a></li>
                                        <li><a href="#">Leave Comment</a></li>
                                </ul>
                                <p>Duis a justo vitae arcu blandit rhoncus. Aenean blandit, nibh at hendrerit placerat, ante nies dui, ac feugiat liguc maPellentesque venenatis tin cidunt turpis sit amet placerat. Nullam nec vehicula enim, nec ultrices lacus. Vivamus aliquet turpis vitae porta rutrum. Nullam quis libero et arcu tristique auctor eget et ligula. Fusce nulla nibh, mollis quis lacinia in, venenatis ac tortor. Aenean nunc urna, gravida mattis lorem ut, consequat elementum eros. </p>

                        </div>

                        <div class="blog-post">
                                <div class="autor-post">

                                </div>
                                <h2><a href="single-post.html">Mauris molestie nulla eget scelerisque.</a></h2>
                                <ul class="post-tags">
                                        <li><a href="#">16 December 2014</a></li>
                                        <li><a href="#">Business</a></li>
                                        <li><a href="#">Photography</a></li>
                                        <li><a href="#">Leave Comment</a></li>
                                </ul>
                                <p>Duis a justo vitae arcu blandit rhoncus. Aenean blandit, nibh at hendrerit placerat, ante nies dui, ac feugiat liguc maPellentesque venenatis tin cidunt turpis sit amet placerat. Nullam nec vehicula enim, nec ultrices lacus. Vivamus aliquet turpis vitae porta rutrum. Nullam quis libero et arcu tristique auctor eget et ligula. Fusce nulla nibh, mollis quis lacinia in, venenatis ac tortor. Aenean nunc urna, gravida mattis lorem ut, consequat elementum eros. </p>

                        </div>

                        <div class="blog-post">
                                <div class="autor-post">

                                </div>
                                <h2><a href="single-post.html">Vestibulum dictum ac eros eget rutrum.</a></h2>
                                <ul class="post-tags">
                                        <li><a href="#">16 December 2014</a></li>
                                        <li><a href="#">Business</a></li>
                                        <li><a href="#">Photography</a></li>
                                        <li><a href="#">Leave Comment</a></li>
                                </ul>
                                <p>Duis a justo vitae arcu blandit rhoncus. Aenean blandit, nibh at hendrerit placerat, ante nies dui, ac feugiat liguc maPellentesque venenatis tin cidunt turpis sit amet placerat. Nullam nec vehicula enim, nec ultrices lacus. Vivamus aliquet turpis vitae porta rutrum. Nullam quis libero et arcu tristique auctor eget et ligula. Fusce nulla nibh, mollis quis lacinia in, venenatis ac tortor. Aenean nunc urna, gravida mattis lorem ut, consequat elementum eros. </p>

                        </div>

                </div>
        </div>
</div>

</div>		
<!-- End content -->
<!-- ####################################################################################################### -->
<? include('footer_view.php'); ?>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.migrate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/waypoint.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/plugins-scroll.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/script.js"></script>  	
</body>
</html>
