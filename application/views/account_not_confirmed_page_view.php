<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>
<?php
if(get_cookie('language') === 'english'){ 
    $lang='en'; 
 }else{
     $lang='id';
 }
?>
<div class="content wrapper body-text">
    <h1>Account Not Confirmed</h1>
    
    <div class="alert-closable alert-hide" id="resendProsActivationSuccessDiv">   
        <input class="alert-close" type="checkbox"/>
        <div class="alert-success">
            <p>
                <?php echo HEADER_ACTIVATION_EMAIL_SUCCESS_MSG; ?>
            </p>        
        </div>
    </div>

    <div class="alert-closable alert-hide" id="resendProsActivationFailedDiv">   
        <input class="alert-close" type="checkbox"/>
        <div class="alert-error">
            <p>
                <?php echo HEADER_ACTIVATION_EMAIL_FAILED_MSG; ?>
            </p>        
        </div>
    </div>
    <div class="error-message">
    <p>
    This account is not confirmed yet.<br/> 
    <?php /*Please confirm your account by click on "Confirm Account URL" from the email which you received in your inbox from us.<br/>
    If still problem occurs, please contact
    <a href="<?php echo $this->config->config['main_base_url'];?>app/<?php echo $lang; ?>/contact-us/" target="_blank">support@seekmi.com</a> from your account's email
    address, and we can resolve the problem. Sorry for the inconvenience!*/ ?>
    <?php
            if(get_cookie('language') === 'english'){ 
                $lang='en'; 
             }else{
                 $lang='id';
             }
             ?>
            <?php echo PROS_INACTIVE_NOTIFY_MSG; ?> <a href="<?php echo $this->config->config['main_base_url']; ?>app/<?php echo $lang; ?>/contact-us/">support@seekmi.com</a>
    </p>
    </div>
</div>
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript">
function activateBtn(obj)
{   
     if(obj==true){
         $('#deleteAccountBtn').removeClass('disabled');
     }else{
         $('#deleteAccountBtn').addClass('disabled');
     }
}
$(document).ready(function () { 
   $(".pros-resend-activate-link").click(function(){
         $.ajax({
            type: 'POST',
            url: Host+"pros/resend_activation_link",
            data:"userid=<?php echo $userid; ?>",
            success: function (data) {
               if(data=='success'){
                   $('#resendProsActivationSuccessDiv').show();
                   $('#resendProsActivationFailedDiv').hide();
               }else{
                   $('#resendProsActivationFailedDiv').show();
                   $('#resendProsActivationSuccessDiv').hide();
               }
            }
         });
    });
});
</script>
</body>
</html>

