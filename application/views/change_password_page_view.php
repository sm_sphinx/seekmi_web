<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

<div class="wrapper content">
    <a class="bttn tiny" href="<?=$this->config->config['base_url']?>profile/settings">
        <span aria-hidden="true" class="icon-font icon-font-left-dir"></span>
        <?php echo BUTTON_BACKTOSETTINGS;?>
    </a>
 <div class="dynamic-row settings-second-level">
    <div class="column-7">
      <div class="form-block">                
        <form accept-charset="ISO-8859-1" action="<?=$this->config->config['base_url']?>profile/change_password" method="post" name="change-password" id="change-password" novalidate>
            <input type="hidden" name="token" id="token" value="<?=$token?>">
            <div class="pod pod-primary">
                <div class="pod-header">
                    <h1>
                        <?php echo CHANGEPASS_EDITPASS_TEXT;?>
                    </h1>
                </div>
                <div class="pod-content">
                <fieldset>
                    <?php if($token==''){ ?>
                    <div class="form-field">
                        <label for="login_password"><?php echo CHANGEPASS_CURRPASS_TEXT;?></label>
                        <input type="password" tabindex="100" name="login_password" id="login_password">
                    </div>
                    <?php } ?>
                    <div data-field="password" class="form-field">
                        <label for="usr_password"><?php echo CHANGEPASS_NEWPASS_TEXT;?></label>
                        <input type="password" tabindex="101" name="usr_password" id="usr_password">
                        <div class="subtext-form"></div>                        
                        <!--<div class="password-strength" style="display: block;">
                            <span class="password-strength-indicator strength-1"></span>
                            <span class="password-strength-indicator strength-2"></span>
                            <span class="password-strength-indicator strength-3"></span>
                            <span class="password-strength-indicator strength-4"></span>
                            <span class="password-strength-label"></span>
                        </div>-->
                    </div>
                    <div class="form-field">
                        <label for="usr_password_again"><?php echo CHANGEPASS_CONFPASS_TEXT;?></label>
                        <input type="password" tabindex="102" name="usr_password_again" id="usr_password_again">
                    </div>
                </fieldset>
                <fieldset>
                    <div class="form-field">
                       <button tabindex="103" class="fifty-button blue bttn" type="submit"><?php echo BUTTON_SAVEPASS;?></button>
                    </div>
                </fieldset>
              </div>
            </div>
           </form>
          </div>
        </div>
    </div>
</div> 
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#change-password").validate({
        rules: {
            <?php if($token==''){ ?>
            login_password: {
                required: true,
                remote: "<?=$this->config->config['base_url']?>profile/check_current_password"
            },
            <?php } ?>
            usr_password: {
                required: true,
                minlength: 5
            },
            usr_password_again: {
                required: true,
                equalTo: "#usr_password"
            }
        },
        messages: {
            login_password: {                
                remote: "<?php echo VAL_CURREPASS;?>"
            }
        }
    });    
});
</script>
</body>
</html>

