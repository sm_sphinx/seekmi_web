<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4HXQT"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T4HXQT');</script>
<!-- End Google Tag Manager -->
<div data-section="header" class="glorious-header">
    <div class="wrapper">
        <div class="row header-row">

            <div class="header-logo">
             <?php if (isset($this->session->userdata['userId'])) { ?>
              <?php if ($this->session->userdata('user_type') == 'professional') { ?>
                  <a href="<?= $this->config->config['base_url'] ?>profile/requests">
              <?php }else if ($this->session->userdata('user_type') == 'user'){ ?>
                  <a href="<?= $this->config->config['base_url'] ?>profile/dashboard">  
              <?php }else{ ?>
                  <a href="<?= $this->config->config['base_url'] ?>">     
              <?php }}else{ ?>
                  <a href="<?= $this->config->config['base_url'] ?>">     
              <?php } ?>        
                    <img  alt="Seekmi" src="<?= $this->config->config['base_url'] ?>images/seekmilogo1.png">
                </a>
                <?php
                //echo get_cookie('language');
                //echo get_cookie('qtrans_front_language');
                if (get_cookie('language') == 'english') {
                    include($_SERVER['DOCUMENT_ROOT'].'/config/eng_constants.php');
                } else {
                    include($_SERVER['DOCUMENT_ROOT'].'/config/ind_constants.php');
                }                
                ?>
               </div>
            <div class="header-middle-container">
                <?php if (isset($this->session->userdata['userId'])) { ?>
                    <?php if ($this->session->userdata('user_type') == 'professional') { ?>
                        <a class="middle-tab" href="<?= $this->config->config['base_url'] ?>profile/requests"><?php echo HEADER_MENU_REQUESTS; ?></a>
                        <a class="middle-tab" href="<?= $this->config->config['base_url'] ?>profile/work"><?php echo HEADER_MENU_QUOTES; ?></a>
                        <a class="middle-tab" href="<?= $this->config->config['main_base_url'] ?>app/credits"><?php echo HEADER_MENU_CREDIT; ?></a>
                        <a class="middle-tab" href="<?= $this->config->config['base_url'] ?>services/add_credit"><?php echo HEADER_MENU_BUY_CREDIT; ?></a>
                    <?php } if ($this->session->userdata('user_type') == 'user') { ?>
                        <!--    <a class="middle-tab current" href="/profile/work">Quotes</a>-->
                        <a class="middle-tab" href="<?= $this->config->config['base_url'] ?>profile/dashboard"><?php echo HEADER_MENU_BUY_PROJECTS; ?></a>
                    <?php } ?>    
                <?php } ?>
            </div>
            <div class="header-navigation ">                
                
               <?php if (isset($this->session->userdata['userId'])) { ?>
                    <div class="universal-profile-container">
                        <div data-section="universal-picture" class="universal-picture">
                            <?php if ($this->session->userdata('user_photo') == '') { ?>
                                <img src="<?= $this->config->config['base_url'] ?>images/100x100.png" class="no-border">
                            <?php } else if ($this->session->userdata('facebook_id') != '') { ?>
                                <img src="<?= $this->session->userdata('user_photo') ?>" class="no-border">
                            <?php } else if($this->session->userdata('google_plus_id')!=''){ ?>
                                <img src="<?= $this->session->userdata('user_photo') ?>" class="no-border">
                            <?php } else{ ?>
                                <img src="<?= $this->config->config['base_url'] ?>user_images/thumb/<?= $this->session->userdata('user_photo') ?>" class="no-border">
                            <?php } ?>   
                            <div style="display:none;" data-section="arrow-tip" class="arrow-tip"></div>
                        </div>
                        <p data-section="universal-name" class="universal-name">
                            <span class="name"><?= $this->session->userdata('user_full_name') ?></span>
                            <span class="icon-font-down-dir  arrow-toggle"></span>
                        </p>

                        <div style="display:none;" data-section="profile-box" class="pod profile-box">
                            <div class="pod-content">
                                <a class="picture-big-link" href="#">
                                    <?php if ($this->session->userdata('user_photo') == '') { ?>
                                        <img src="<?= $this->config->config['base_url'] ?>images/100x100.png" class="picture-big">
                                    <?php } else if ($this->session->userdata('facebook_id') != '') { ?>
                                        <img src="<?= $this->session->userdata('user_photo') ?>" class="picture-big">
                                     <?php } else if($this->session->userdata('google_plus_id')!=''){ ?>
                                        <img src="<?= $this->session->userdata('user_photo') ?>" class="picture-big">
                                    <?php } else{ ?>
                                        <img src="<?= $this->config->config['base_url'] ?>user_images/thumb/<?= $this->session->userdata('user_photo') ?>" class="picture-big">
                                    <?php } ?>        
                                </a>
                                <h3 class="name"><?= $this->session->userdata('user_full_name') ?></h3>
                                <span class="email"><?= $this->session->userdata('user_email') ?></span>
                                <?php
                                $this->db->select('credits,status');
                                $this->db->where('userId', $this->session->userdata('userId'));
                                $us_detail = $this->db->get('tbluser');
                                $us_row = $us_detail->row();

                                $this->db->where('userId', $this->session->userdata('userId'));
                                $us_qr = $this->db->get('tbluserprojects');
                                ?>
                                <?php if ($this->session->userdata('user_type') == 'professional') { ?>
                                    <span class="project-count"><?= $us_row->credits ?> 
                                        <?php if($us_row->credits < 2){ echo HEADER_MENU_CREDIT; }else{ echo HEADER_MENU_CREDITS; } ?></span>
                                <?php } else { ?>
                                    <span class="project-count"><?= $us_qr->num_rows() ?> <?php echo HEADER_MENU_BUY_PROJECTS; ?></span>
                                <?php } ?>
                            </div>

                            <div class="pod-footer">
                                <?php if ($this->session->userdata('user_type') == 'professional') { ?>
                                    <a href="<?= $this->config->config['base_url'] ?>profile/services"><?php echo HEADER_MENU_PROFILE; ?></a>
                                <?php } ?>
                                <a href="<?= $this->config->config['base_url'] ?>profile/settings"><?php echo HEADER_MENU_SETTINGS; ?></a>
                                <a href="<?= $this->config->config['base_url'] ?>user/logout"><?php echo HEADER_LOGOUT; ?></a>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                  <a style="vertical-align:middle; margin-top:10px; margin-right:0px;"  href="<?= $this->config->config['base_url'] ?>user/register" rel="nofollow" class="gray-link log-in-link"><?php echo MENU_SIGNUP; ?></a>
                    <a style="vertical-align:middle; margin-top:10px; margin-right:10px;"  href="<?= $this->config->config['base_url'] ?>user/login" rel="nofollow" class="gray-link log-in-link"><?php echo HEADER_SIGNIN; ?></a>
                <?php } ?>
                <?php /*    
                <select style="float: right; width: 69px;" id="language" name="language">
                    <option value="english" <?php if (get_cookie('language') === 'english') { ?>selected="selected" <?php } ?>>EN</option>
                    <option value="indonesia" <?php if (get_cookie('language') === 'indonesia' || get_cookie('language')=='') { ?>selected="selected" <?php } ?>>ID</option>
                </select>
                 */ ?>                    
                   <div class="menu-top-menu-container">
                    <ul id="primary-menu" class="nav-menu">
                      <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                         <?php if(get_cookie('language') == 'indonesia'){ ?>
                            <a title="ID" href="javascript:;"><img src="<?= $this->config->config['base_url'] ?>images/id.png" alt="ID">&nbsp;ID&nbsp;&nbsp;
                         <?php }else{ ?>
                            <a title="EN" href="javascript:;"><img src="<?= $this->config->config['base_url'] ?>images/gb.png" alt="EN">&nbsp;EN&nbsp;&nbsp;
                         <?php } ?>
                             <img width="7" height="6" alt="" src="<?= $this->config->config['base_url'] ?>images/droparrow.png" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a>
                            <ul class="sub-menu" id="language">
                               <li class="qtranxs-lang-menu-item qtranxs-lang-menu-item-en menu-item menu-item-type-custom menu-item-object-custom">
                                   <a title="EN" <?php if(get_cookie('language') == 'indonesia'){ ?>href="javascript:;"<?php } ?> lang="english"><img src="<?= $this->config->config['base_url'] ?>images/gb.png" alt="EN">&nbsp;EN&nbsp;&nbsp;</a>
                               </li>
                               <li class="qtranxs-lang-menu-item qtranxs-lang-menu-item-id menu-item menu-item-type-custom menu-item-object-custom menu-last">
                                   <a title="ID" <?php if(get_cookie('language') == 'english'){ ?>href="javascript:;"<?php } ?> lang="indonesia"><img src="<?= $this->config->config['base_url'] ?>images/id.png" alt="ID">&nbsp;ID&nbsp;&nbsp;&nbsp;</a>
                               </li>
                            </ul>
                        </li>
                    </ul>
                </div>  
             
            </div>
        </div>
    </div>
</div>
<?php
if (isset($this->session->userdata['userId'])) {
    if ($this->session->userdata('user_type') == 'user' && $us_row->status == 'N') {
?>
<div class="alert-closable alert-hide" id="resendActivationSuccessDiv">   
    <input class="alert-close" type="checkbox"/>
    <div class="alert-success">
        <p>
            <?php echo HEADER_ACTIVATION_EMAIL_SUCCESS_MSG; ?>
        </p>        
    </div>
</div>

<div class="alert-closable alert-hide" id="resendActivationFailedDiv">   
    <input class="alert-close" type="checkbox"/>
    <div class="alert-error">
        <p>
            <?php echo HEADER_ACTIVATION_EMAIL_FAILED_MSG; ?>
        </p>        
    </div>
</div>

<div class="alert-closable" id="resendActivationDiv">   
    <input class="alert-close" type="checkbox"/>
    <div class="alert-error">
        <p>
            <?php
            if(get_cookie('language') === 'english'){ 
                $lang='en'; 
             }else{
                 $lang='id';
             }
             ?>
            <?php echo HEADER_INACTIVE_NOTIFY_MSG; ?> <a href="<?php echo $this->config->config['main_base_url']; ?>app/<?php echo $lang; ?>/contact-us/">support@seekmi.com</a>
        </p>        
    </div>
</div>
<?php }} ?>
<label class="mobile-navigation-trigger" for="mobile-navigation-toggle"></label>
<input id="mobile-navigation-toggle" class="mobile-navigation-toggle" type="checkbox"></input>
<div class="glorious-navigation-wrapper  hidden ">
    <div class="glorious-navigation">
        <div class="wrapper browse-wrapper" data-browse-container="">
            <ul class="navigation-row">
                <?php if (isset($this->session->userdata['userId'])) { ?>
                    <?php if ($this->session->userdata('user_type') == 'professional') { ?>
                        <li class="dashboard"><a href="<?= $this->config->config['base_url'] ?>profile/requests"><span><?php echo HEADER_MENU_REQUESTS; ?></span></a></li>
                        <li class="dashboard"><a href="<?= $this->config->config['base_url'] ?>profile/work"><span><?php echo HEADER_MENU_QUOTES; ?></span></a></li>
                        <li class="dashboard"><a href="<?= $this->config->config['main_base_url'] ?>app/credits"><span><?php echo HEADER_MENU_CREDIT; ?></span></a></li>
                        <li class="dashboard"><a href="<?= $this->config->config['base_url'] ?>services/add_credit"><span><?php echo HEADER_MENU_BUY_CREDIT; ?></span></a></li>                        
                    <?php } if ($this->session->userdata('user_type') == 'user') { ?>
                        <li class="dashboard"><a href="<?= $this->config->config['base_url'] ?>profile/dashboard"><span><?php echo HEADER_MENU_DASHBOARD; ?></span></a></li>
                    <?php } ?>
                    <li class="dashboard"><a href="#"><span>Pro Center</span></a></li>
                    <?php if ($this->session->userdata('user_type') == 'professional') { ?>
                        <li class="dashboard"><a href="<?= $this->config->config['base_url'] ?>profile/services"><span><?php echo HEADER_MENU_PROFILE; ?></span></a></li>
                    <?php } ?>
                    <li class="settings right"><a href="<?= $this->config->config['base_url'] ?>profile/settings"><span><?php echo HEADER_MENU_SETTINGS; ?></span></a></li>
                    <li class="mobile-navigation-item"><a href="<?= $this->config->config['base_url'] ?>user/logout"><?php echo HEADER_LOGOUT; ?></a></li>
                <?php } else { ?>
                    <li class="mobile-navigation-item"><a href="<?= $this->config->config['base_url'] ?>user/login"><?php echo HEADER_SIGNIN;?></a></li>
                    <?php } ?>
            </ul>
        </div>
    </div>
</div>
<?php

function get_timeago($ptime) {
    $estimate_time = time() - $ptime;

    if ($estimate_time < 1) {
        return 'less than 1 second ago';
    }

    $condition = array(
        12 * 30 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($condition as $secs => $str) {
        $d = $estimate_time / $secs;

        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
        }
    }
}
?>