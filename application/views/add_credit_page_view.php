<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/pricing.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>
<div class="wrapper content">
    
    <h2>Coming Soon</h2>
    <?php /*if(!empty($package_list)){ ?>
    <section id="pricePlans">
        <ul id="plans">
            <?php foreach($package_list as $package_row){ ?>
                <li class="plan" style="width:32%;">
                        <ul class="planContainer">
                                <li class="title"><h2><?php 
                                if (get_cookie('language') === 'indonesia') {
                                    echo $package_row->packagename_id;
                                }else{
                                    echo $package_row->packagename_en;
                                } ?>
                                </h2></li>
                                <li class="price"><p>Rp. <?php echo number_format($package_row->amount,2); ?></p></li>
                                <li>
                                    <ul class="options">
                                          <li><?php echo $package_row->credit; ?> miCoins</li>                                               
                                    </ul>
                                </li>
                                <li class="button">
                                    <a href="<?=$this->config->config['base_url']?>services/buy_credit/<?php echo $package_row->packageId; ?>"><?php echo CREDIT_PURCHASE_TEXT; ?></a>
                                </li>
                        </ul>
                </li>
            <?php } ?>
        </ul> <!-- End ul#plans -->        
    </section>
    <?php }*/ ?>
</div>                                           
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
</body>
</html>

