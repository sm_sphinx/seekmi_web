<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="mainimage" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/signup.css">
<style type="text/css">
.primo-fluid.landing-page .hook form {
    margin: 0 auto;
    max-width: 650px;
    padding: 0 5px 0 20px;
}
</style>
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-responsive primo-fluid box-shadow multiple-backgrounds landing-page">
<?php include('common_view.php'); ?>    
<div class="glorious-header glorious-header1 inn" data-section="header">
<div class="wrapper">
    <div class="row header-row">
        <div class="header" style="margin-bottom:10px;">
    <a class="logo" href="<?=$this->config->config['base_url']?>"><img alt="Seekmi" src="<?=$this->config->config['base_url']?>images/seekmilogo.png"></a>
<!--    <div class="navigation"><a href="/pros/why-join">Why join Seekmi?</a></div>-->
    </div>
  </div>
</div>
</div>

<div class="wrapper content">
    <div style="" class="hook">
        <form novalidate name="hook" class="">            
            <h1 style="font-size: 38px;">Apakah Anda Ahli Lokal?</h1>
            <h1 style="font-size: 38px;">
                Temukan klien baru
                <div class="subtitle ng-binding" ng-class="{'has-custom-entry-point': customEntryPoint}" style="text-transform:none;">
                     Seekmi akan meneruskan permintaan klien akan kebutuhan ahli lokal di wilayahnya. Anda yang akan memutuskan apakah Anda tertarik atau tidak untuk memberikan penawaran untuk klien tersebut.
                </div>
            </h1>

            <fieldset>
                <label>
                     Jasa apa yang Anda lakukan?
                </label>
                <div class="form-field form-field-category">
                    <select name="category" id="category" onchange="setDisable(this.value);">
                        <option value="">Pilih jenis layanan Anda</option>
                        <?php if($categories!=''){ 
                            foreach($categories as $cat_row){ ?>
                        <option value="<?=$cat_row->catId?>"><?=$cat_row->categoryName?></option>
                        <?php }} ?>
                    </select>                   
                </div>
                <div class="form-field form-field-nav">
                    <button type="button" class="bttn get-started-btn">
                        Get Started
                    </button>
                </div>
<!--                <a class="how-it-works" href="/pros/how-Pintack-works">How Pintack works</a>-->
            </fieldset>            
            </div>
        </form>
    </div>

<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript">
     function setDisable(obj){
         if(obj!=''){
             $('.get-started-btn').attr('disabled',false);
         }else{
             $('.get-started-btn').attr('disabled',true);
         }
     }
     $(document).ready(function () {
         if($('#category').val()==''){
            $('.get-started-btn').attr('disabled',true); 
         }else{
            $('.get-started-btn').attr('disabled',false); 
         }
     });
     $('.get-started-btn').click(function(){
         var catId=$('#category').val();
         if(catId!=''){
            location.href='<?=$this->config->config['base_url']?>pros/register/'+catId;
         }
     })
</script>
</body>
</html>
      
     