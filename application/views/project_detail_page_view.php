<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head><? include('header_view.php'); ?>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?php echo DETAIL_QUOTE_TEXT.' '.$quote_row->businessName; echo DETAIL_FOR_TEXT; echo $project_row->subserviceName; ?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/message.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/work.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/jh-modal.css">
<link rel="stylesheet" href="<?=$this->config->config['base_url']?>css/lib/jquery.raty.css">
<style type="text/css">
.hint {
  text-align: center;
  width: 160px
}

div.hint {
  font-size: 1.4em;
  height: 46px;
  margin-top: 15px;
  padding: 7px
}
.review-form-row label.error{
    clear: both;
    color: #d51818;
    display: block;
    font-size: 11.2px;
    margin: 5px 0 -2px 5px;
}
</style>
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-responsive primo-fluid messenger consumer-messenger box-shadow multiple-backgrounds">

<div class="wrapper">
<div class="request">
<header>
<h1 class="body-text"> <?php echo DETAIL_INTRODUCTION_TEXT." ".$project_row->subserviceName;?></h1>
<div class="buttons">
<a class="bttn gray contact-support" id="show_support" href="javascript:;">
<?php echo DETAIL_CONTACTSUPPORT_TEXT; ?></a>
<a class="gray bttn" href="javascript:void(0);" id="show_request"><?php echo DETAIL_VIEWREQUEST_TEXT;?></a>
</div>
</header>

<div class="pod">
<div class="bid-side-by-side">
<nav class="sidebar">
<ul>
<?php if(!empty($quote_list)){ foreach($quote_list as $quote_list_row){ ?>
<li class="item <?php if($quote_id==$quote_list_row->requestId){ ?>active<?php }?>">
    <a href="<?=$this->config->config['base_url']?>bid/details/<?=$project_id?>/<?=$quote_list_row->requestId?>">
        <?php if($quote_list_row->userPhoto==''){ ?>
        <img class="profile-picture" src="<?=$this->config->config['base_url']?>images/100x100.png">
        <?php }else if($quote_list_row->facebookProfileId!=''){ ?>
        <img class="profile-picture" src="<?=$quote_list_row->userPhoto?>">
        <?php }else{ ?>
        <img class="profile-picture" src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$quote_list_row->userPhoto?>">
        <?php } ?>
        <div class="bid-info nav-info">
            <h3><?=$quote_list_row->businessName?></h3>
            <span class="service-rating service-rating-small">
            <span class="star-rating star-rating-small stars-<?=str_replace('.','_',$quote_list_row->rating)?>"></span>
            <span class="service-rating-review-count"> (<?php echo $quote_list_row->reviewCount." ".DETAIL_REVIEWS_TEXT;?>)</span>
            </span>
            <?php           
            if($quote_list_row->priceType!='no_price')
            {
                ?>
                <div class="bid-estimate bid-estimate-with-total">
                <span class="bid-estimate-total"><?php echo DETAIL_IDR_TEXT. number_format($quote_list_row->quotePrice, 0, ',', ','); ?></span>
                </div>
                <?php
            }
            ?>
        </div>
    </a>
</li>
<?php }} ?>
</ul>
</nav>

<div class="bid-navigation-bar" style="display: none;"></div>

<div class="main-content">
<header class="bid-header">
<div class="bid-header-content">
    <h2><?=$quote_row->businessName?></h2>
    <ul class="bid-header-data" style="width: 100%;">        
        <li class="item" aria-label="Name">
        <span class="datum-icon icon-font-user"></span> <?=$quote_row->firstname?> <?=$quote_row->lastname?>
        </li>
        <li class="item" aria-label="Phone number">
        <span class="datum-icon icon-font-phone"></span> <?=$quote_row->phone?>
        </li>
        <li class="item" aria-label="Location">
        <span class="datum-icon icon-font-location"></span> <?=$quote_row->cityName?>, <?=$quote_row->provName?>
        </li>
    </ul>
    <ul class="bid-buttons">
    <li><a data-profile-information="profile" class="bid-view-profile gray bttn" href="<?=$this->config->config['base_url']?>profile/services/view/<?=$quote_row->professionalId?>" target="_blank"><?php echo DETAIL_VIEWPROFILE_TEXT;?></a></li>
    <?php if($media_count > 0){ ?> 
    <li><a class="bid-pictures gray bttn" href="<?=$this->config->config['base_url']?>profile/services/view/<?=$quote_row->professionalId?>#media" target="_blank">
    <span class="icon-font-picture"></span> View <?=$media_count?> <?php echo DETAIL_PHOTOS_TEXT;?></a>
    </li>
    <?php } ?>
    <?php if($quote_row->website!=''){ ?>
    <li><a class="bid-view-website gray bttn" href="http://<?=$quote_row->website?>" target="_blank"><?php echo DETAIL_WEBSITE_TEXT;?></a></li>
    <?php } ?>
    <li>
    <a data-profile-information="reviews" class="bid-reviews gray bttn" href="<?=$this->config->config['base_url']?>profile/services/view/<?=$quote_row->professionalId?>#reviews" target="_blank">
        <span class="star-rating star-rating-large stars-<?=str_replace('.','_',$quote_row->rating)?>"></span> <?php echo $quote_row->reviewCount." ".DETAIL_REVIEWS_TEXT;?> 
     </a>
    </li>
    </ul>
    <?php if($quote_row->userPhoto==''){ ?>
    <img class="profile-picture" src="<?=$this->config->config['base_url']?>images/100x100.png">
    <?php }else if($quote_row->facebookProfileId!=''){ ?>
    <img class="profile-picture" src="<?=$quote_row->userPhoto?>">
    <?php }else{ ?>
    <img class="profile-picture" src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$quote_row->userPhoto?>">
    <?php } ?>
    <div class="bid-header-estimate with-hours">
        <?php
        if($quote_row->priceType!='no_price')
        {
            ?>
            <div class="estimate-total"><?php echo DETAIL_IDR_TEXT; ?><br/>
                <?php echo number_format($quote_row->quotePrice, 0, ',', ','); ?></div>
            <?php
        }
            
        
        if($quote_row->priceType=='fixed')
        {
            ?>
            <div class="estimate-detail"><?php echo DETAIL_TOTALPRICE_TEXT;?></div>
            <?php
        }
        elseif($quote_row->priceType=='hourly')
        {
            ?>
            <div class="estimate-detail"><?php echo DETAIL_HOURLYPRICE_TEXT;?></div>
            <?php
        }
        else
        {
            ?>
            <div class="estimate-detail"><h3><?php echo DETAIL_INFOPRICE_TEXT;?></h3></div>
            <?php
        }
        ?>
        
    </div>
</div>
</header>

<div class="bid-messaging">
<!--    <a class="bid-expand-messages" href="#">Show 2 previous messages</a>-->
<ul class="items" id="sent-msg-div">
<li class="item message-item" style="display: block;">
    <?php if($quote_row->userPhoto==''){ ?>
    <img class="profile-picture" src="<?=$this->config->config['base_url']?>images/100x100.png">
    <?php }else if($quote_row->facebookProfileId!=''){ ?>
    <img class="profile-picture" src="<?=$quote_row->userPhoto?>">
    <?php }else{ ?>
    <img class="profile-picture" src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$quote_row->userPhoto?>">
    <?php } ?>
    <div class="item-content">
        <time class="item-time"><?php echo get_timeago(strtotime($quote_row->addedOn)); ?></time>
        <h4 class="item-author"><?php echo $quote_row->firstname." ".$quote_row->lastname; ?></h4>
        <div class="bid-message-body">
            <?php echo $quote_row->quoteMessage; ?>
        </div>
    </div>
</li>
<?php if(!empty($message_list)){ foreach($message_list as $message_row){ ?>
<li class="item message-item" style="display: block;">
    <?php if($message_row->userPhoto==''){ ?>
    <img class="profile-picture" src="<?=$this->config->config['base_url']?>images/100x100.png">
    <?php }else if($message_row->facebookProfileId!=''){ ?>
    <img class="profile-picture" src="<?=$message_row->userPhoto?>">
    <?php }else{ ?>
    <img class="profile-picture" src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$message_row->userPhoto?>">
    <?php } ?>
    <div class="item-content">
        <time class="item-time"><?php echo get_timeago(strtotime($message_row->createdOn)); ?></time>
        <h4 class="item-author"><?php echo $message_row->firstname." ".$message_row->lastname; ?></h4>
        <div class="bid-message-body">
            <?php echo $message_row->message; ?>
        </div>
    </div>
</li>
<?php }} ?>
</ul>
<ul class="items" id="reply-msg-div">
<li class="item message-item" id="send-message-div" style="display:none;">
    <div data-locator="reply"></div>
    <?php if($this->session->userdata('user_photo')==''){ ?>
        <img src="<?=$this->config->config['base_url']?>images/100x100.png" class="profile-picture">
    <?php }else if($this->session->userdata('facebook_id')!=''){ ?>
        <img src="<?=$this->session->userdata('user_photo')?>" class="profile-picture">
    <?php }else{ ?>
        <img src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$this->session->userdata('user_photo')?>" class="profile-picture">
    <?php } ?>   
    <div class="item-content">        
        <h4 class="item-author"><?=$this->session->userdata('user_full_name')?></h4>
        <div class="composer">
            <form name="reply-msg-form" id="reply-msg-form">
                <input type="hidden" name="qid" value="<?=$quote_id?>"/>
                <input type="hidden" name="msgFrom" value="<?=$this->session->userdata('userId')?>"/>
                <input type="hidden" name="msgTo" value="<?=$quote_row->professionalId?>"/>
                <textarea name="msg_content" id="msg_content"></textarea>
                <button class="blue bttn" type="submit"><?php echo BUTTON_SEND;?></button>
                <!--<span class="attach-file"><span class="icon-font-attach"></span> 
                <a href="#" data-action="add-attachment">Add Attachment</a></span>-->
                <a href="javascript:void(0);" class="cancel cancel-message-div" data-action="cancel"><?php echo BUTTON_CANCEL_TEXT;?></a>
                <ul></ul>
            </form>
        </div>
    </div>
</li>
</ul>

<div class="bid-action-center button-action-center" style="display: block;">
    <?php
    if($quote_row->status=='Hired' || $quote_row->status=='Declined' || $quote_row->status=='Completed')
    {
        if($quote_row->status=='Hired')
        {
            $details_status_text = DETAIL_HIRE_TEXT;
        }
        elseif($quote_row->status=='Declined')
        {
            $details_status_text = DETAIL_DECLINE_TEXT;
        }
        else
        {
            $details_status_text = DETAIL_COMPLETED_TEXT;
            $details_status_text.=' '.$quote_row->firstname." ".$quote_row->lastname;
            if($project_row->status=='Completed'){
                $details_status_text.=' and you also';
            }
        }
    ?>
        <div class="bid-status bid-status-cancelled">
        <div aria-hidden="true" class="icon icon-24 icon-info"></div> 
        <p><?php echo $details_status_text;?>.
            <?php /*<time><?php echo get_timeago(strtotime($quote_row->modifiedOn)); ?></time>. */ ?>
            <?php
            if($project_row->status=='Pending')
            {
                ?>
            <button data-action="undo cancel" href="#" class="micro gray bttn" id="undo_<?php echo $quote_row->requestId; ?>">Undo</button>
                <?php
            }
            ?>
        </p>
        </div>
    <?php
    }else{
    ?>
    <div class="bid-status bid-status-cancelled"></div>
    <?php } ?>
<div data-contains="actions" class="bid-action-buttons">
<div class="equal-button-container">
<a class="action-button bttn bid-action-reply bid-action-type-interact" href="javascript:;">
<div class="icon-font icon-font-reply" aria-hidden="true"></div>
<span class="label"><?php echo BUTTON_REPLY;?></span>
</a>
<span class="button-spacer"></span>
<a class="action-button bttn bid-action-call bid-action-type-interact mobile-only" href="tel:+18008418047">
<div class="icon-font icon-font-phone" aria-hidden="true"></div>
<span class="label"><?php echo BUTTON_CALL;?></span>
</a>
<?php
/*if($quote_row->status=='Completed')
{ 
    ?>
        <a class="action-button bttn bid-action-review bid-action-type-interact" href="<?=$this->config->config['base_url']?>bid/add_review/<?=$quote_row->professionalId?>" target="_blank" <?php if($quote_row->status=='Accepted'){ ?> style="display:none;"<?php } ?>>
        <div class="icon-font icon-font-star" aria-hidden="true"></div>
        <span class="label"><?php echo DETAIL_LEAVE_REVIEW_TEXT;?></span>
        </a>
    <?php
 }*/
?>

<a class="action-button bttn bid-action-hire bid-action-type-hire" href="javascript:;" <?php if($quote_row->status!='Accepted'){ ?> style="display:none;"<?php } ?>>
<div class="icon-font icon-font-check" aria-hidden="true"></div>
<span class="label"><?php echo BUTTON_HIRE;?></span>
</a>
<span class="button-spacer"></span>
<a class="action-button bttn bid-action-decline bid-action-type-reject" href="javascript:;" <?php if($quote_row->status!='Accepted'){ ?> style="display:none;"<?php } ?>>
<div class="icon-font icon-font-cancel" aria-hidden="true"></div>
<span class="label"><?php echo BUTTON_DECLINE;?></span>
</a>
<span class="button-spacer"></span>

</div>
</div>
</div>
<div class="bid-update-status" style="display: none;">
<div class="spinner" data-is="indicator"></div>
</div>
<div class="bid-explain-decline" style="display: none;">
<div class="bid-declined">
    <h3 class="body-text"><?php echo DETAIL_DECLINEREASON_TEXT;?></h3>
    <p class="body-text">
    <span class="info-icon icon-font-lock" aria-hidden="true"></span>
    <?php echo DETAIL_RESPONSE_TEXT;?>
    </p>
</div>
<form>
<fieldset>
<ul class="bid-decline-reasons">
<li class="item">
<input type="checkbox" id="bid-decline-reason-114" data-action="select">
<div class="bid-decline-reason-content" data-is="content">
<label for="bid-decline-reason-114"><?php echo DETAIL_REASON_TEXT1;?></label>
</div>
</li>
<li class="item">
<input type="checkbox" id="bid-decline-reason-116" data-action="select">
<div class="bid-decline-reason-content" data-is="content">
<label for="bid-decline-reason-116"><?php echo DETAIL_REASON_TEXT2;?></label>
</div>
</li>
<li class="item">
<input type="checkbox" id="bid-decline-reason-118" data-action="select">
<div class="bid-decline-reason-content" data-is="content">
<label for="bid-decline-reason-118"><?php echo DETAIL_REASON_TEXT3;?></label>
</div>
</li>
<li class="item">
<input type="checkbox" id="bid-decline-reason-120" data-action="select">
<div class="bid-decline-reason-content" data-is="content">
<label for="bid-decline-reason-120"><?php echo DETAIL_REASON_TEXT4;?></label>
</div>
</li>
<li class="item">
<input type="checkbox" id="bid-decline-reason-122" data-action="select">
<div class="bid-decline-reason-content" data-is="content">
<label for="bid-decline-reason-122"><?php echo DETAIL_REASON_TEXT5;?></label>
</div>
</li>
<li class="item">
<input type="checkbox" id="bid-decline-reason-125" data-action="select">
<div class="bid-decline-reason-content" data-is="content">
<label for="bid-decline-reason-125"><?php echo DETAIL_REASON_TEXT5;?></label>
</div>
</li>
</ul>
</fieldset>
<div class="bid-explain-decline-confirm">
    <button class="blue bttn" type="submit" data-action="save"><?php echo BUTTON_SUBMIT;?></button>
    <a href="#" class="cancel" data-action="skip"><?php echo BUTTON_SKIP;?></a>
    <a href="#" class="cancel" data-action="undo"><?php echo BUTTON_UNDO;?></a>
</div>
</form>
<p class="bid-decline-is-not-the-end"><?php echo DETAIL_MSGSTEP_TEXT;?></p>
</div>

</div>
<?php
if($quote_row->status=='Completed')
{   
   if($review_already=='N'){
?>
<?php /*<div class="quote-relevancy-footer feedback-pros"style="display:none;">
<p class="quote-relevancy-question"><?php echo DETAIL_PROMSG_TEXT;?></p>
<div class="yes-no-button">
<span class="pseudo-link"><?php echo BUTTON_NO;?></span>
<span class="yes pseudo-link job-review-link"><?php echo BUTTON_YES;?></span>
</div>
</div> */ ?>
<div class="box">     
    <div class="request-header box-header" style="padding: 20px 0 0;">
        <div class="request-name">
            <h4>
                Review the vendor
            </h4>            
        </div>
    </div>
    <div class="alert-closable alert-hide review-success">            
        <div class="alert-success">
            <p>
                 <?php echo ADDREVIEW_THANKYOU_TEXT." ".$quote_row->firstname." ".$quote_row->lastname; ?>
            </p>                        
        </div>
    </div>

    <div class="alert-closable alert-hide review-already">            
        <div class="alert-error">                        
                <?php echo ADDREVIEW_ALLREADYREVIEW_TEXT; ?>                                             
        </div>
    </div>
    <form name="job-review-form" id="job-review-form" method="post" action="#" novalidate>
        <input type="hidden" name="prosid" value="<?php echo $quote_row->professionalId; ?>"/>
        <input type="hidden" name="quoteid" value="<?php echo $quote_id; ?>"/>
        <input type="hidden" name="projectid" value="<?php echo $project_id; ?>"/>
        <div class="box-content">               
            <div class="form-field" style="padding-bottom:30px;">
                 <label><?php echo ADDREVIEW_YOURRATING_TEXT;?>:</label>                            
                 <div id="targetKeep" class="star-input-container review-form-row"></div>
                 <div style="display:block;" class="rating-tip-container score-description" id="targetKeep-hint" name="targetKeep_hint">
                     <div class="rating-tip-arrow"></div>
                     <p class="rating-tip"><?php echo ADDREVIEW_CLICKTORATE_TEXT;?></p>
                     <div class="rating-tip-arrow-back"></div>                                
                  </div>                                                       
                </div>
                <div class="form-field" style="padding-bottom:30px;">                            
                        <label>
                            <?php echo ADDREVIEW_YOURRATE_TEXT; ?>
                        </label>
                        <div class="review-form-row" data-field="text">
                            <textarea id="revMsg" name="revMsg" tabindex="105" placeholder="Describe your experience working with <?=$quote_row->firstname?> <?=$quote_row->lastname?>. 
Please be specific."></textarea>
                            <div style="display: none;" class="tooltip tooltip-error tooltip-wide tooltip-right">
                                <div class="tooltip-content tooltip-content-error"></div>                                        
                            </div>                                    
                        </div>
                 </div>
                 <div class="form-field">    
                        <button type="submit" class="bttn post-review-bttn blue">
                            <?php echo ADDREVIEW_POSTREVIEW_TEXT; ?>
                        </button>
                 </div>
        </div>
    </form>
</div>
<?php }} ?>  
    
</div>
</div>
</div>
</div>
</div> 
<? include('footer_view.php'); ?>
    <div class="support-options curtain contact_support" style="display: none;">
    <div class="box box-modal box-secondary-emphasized curtain-inner">
        <div class="box-header">
            <h2><?php echo DETAIL_SEEKMISUPPORT_TEXT;?></h2>
        <a href="javascript:;" class="modal-close support-close">×</a></div>

        <div class="box-content">
            <p class="body-text">
                <?php echo DETAIL_HELP_TEXT;?>
            </p>

            <ul class="support-contact">
                <li>
                    <div class="icon icon-24 icon-phone"></div>
                    (021) 4000-0289
                </li>
                <li>
                    <div class="icon icon-24 icon-message"></div>
                    <?php echo DEL_SUPPORT_EMAIL;?>
                </li>
            </ul>
        </div>
    </div>
    </div>
    
    <div class="curtain view_request"  style="display: none;">
    <div data-curtain="request" class="homepage-work-request curtain-inner" style="display:block;">
        <div class="work-request box box-modal" style="background-color:#fff;">
            <div class="request-header box-header" style="padding: 20px 0 0;">
                <div class="request-name">
                    <h2>
                        <?=$this->session->userdata('user_full_name')?>
                    </h2>
                    <a href="javascript:;" class="modal-close" id="hide_request" style="right:-12px; top:-12px;">×</a>
                </div>
                 
<!--                <div class="profile-map-container">
                    <div class="profile-container">
                        <img width="80" height="80" alt="profile photo" src="images/100x100.png">
                    </div>
                    <div class="map-container">
                       <img src="images/80s.png">
                    </div>
                </div>-->
             </div>
        
            <div class="what-when-where-container">
                 <dl class="work-request-data" data-component="work-request-data">
                    <?php if(!empty($req_data)){ 
                         foreach($req_data as $req_row){                    
                    ?> 
                    <dt class="request-title"><?=$req_row->questionName?></dt>
                    <dd class="request-info">
                    <?php if(!empty($req_option_data[$req_row->questionId])){ ?>
                        <ul>
                       <?php foreach($req_option_data[$req_row->questionId] as $req_option_row){ ?>
                        <li class="list-bullet">
                            <?php if(count($req_option_data[$req_row->questionId]) > 1){ ?>
                         <div class="check"></div>
                            <?php } ?>
                           <?=$req_option_row['optionText']?> 
                         <?php if($req_option_row['optionExtra']){
                             echo " (".$req_option_row['optionExtra'].")";
                         }
                         ?>
                        </li>
                        <?php }?>
                        </ul>
                       <?php }else{ ?>
                          <?php
                            if($req_row->fieldType=='datepicker'){ 
                                if($req_row->extras!=''){
                                    $ext_arr=array();
                                    $ext_arr=json_decode($req_row->extras);
                                    if($ext_arr->from!=''){
                                        echo "From: ".$ext_arr->from;
                                    }
                                    if($ext_arr->to!=''){
                                        echo "   To: ".$ext_arr->to;
                                    }
                                }

                            } ?>
                       <?php } ?>
                     </dd>
                    <?php }} ?>
                    <!--<dt class="request-title">Type of home</dt>
                    <dd class="request-info">One-story house</dd>                                                     
                    <dt class="request-title">Recurrence</dt>
                    <dd class="request-info">Once a month</dd>
                    <dt class="request-title">Day preference</dt>
                    <dd class="request-info">Tuesday</dd>
                    <dt class="request-title">Time preference</dt>
                    <dd class="request-info">Early morning (before 9am)</dd>
                    <dt class="request-title">Bedroom(s)</dt>
                    <dd class="request-info">2 bedrooms</dd>
                    <dt class="request-title">Bathroom(s)</dt>
                    <dd class="request-info">1.5 bathrooms</dd>
                    <dt class="request-title">Square feet</dt>
                    <dd class="request-info">1501 - 2000</dd>
                    <dt class="request-title">Will client provide cleaning equipment and supplies?</dt>
                    <dd class="request-info">Yes, I will provide all equipment and supplies</dd>
                    <dt class="request-title">Other details</dt>
                    <dd class="request-info">
                     <ul>
                        <li class="list-bullet">
                         <div class="check"></div>
                         I want eco-friendly products
                        </li>
                        <li class="list-bullet">
                        <div class="cancel"></div>
                        I want laundry done
                        </li>
                        <li class="list-bullet cancel-bullet">
                          <div class="cancel"></div>
                          I have pets
                        </li>
                        <li class="list-bullet cancel-bullet">
                            <div class="cancel"></div>
                            I want my refrigerator cleaned
                        </li>
                        <li class="list-bullet cancel-bullet">
                            <div class="cancel"></div>
                            I want my oven cleaned
                        </li>
                        <li class="list-bullet cancel-bullet">
                            <div class="cancel"></div>
                            I want windows cleaned
                        </li>
                     </ul>
                   </dd>
                   <dt class="request-title">Message</dt>
                   <dd class="request-info">grtsy</dd>
                   <dt class="request-title">Where</dt>
                   <dd class="request-info">Scotch Plains, NJ 07076</dd>
                   <dt class="request-title">Phone number</dt>
                   <dd class="request-info">Not available</dd>-->
                </dl>
        </div>
    </div>
 </div>
</div>
<?php /*<div class="curtain job_review_modal" style="display: none;">
    <div class="box box-modal box-secondary-emphasized curtain-inner">     
        <div class="request-header box-header" style="padding: 20px 0 0;">
            <div class="request-name">
                <h2 style="padding-left:20px;">
                    Submit Review
                </h2>
                <a href="javascript:;" class="modal-close" id="hide_review_form" style="right:-12px; top:-12px;">×</a>
            </div>
        </div>
        <div class="alert-closable alert-hide review-success">            
            <div class="alert-success">
                <p>
                     <?php echo ADDREVIEW_THANKYOU_TEXT." ".$quote_row->firstname." ".$quote_row->lastname; ?>
                </p>                        
            </div>
        </div>

        <div class="alert-closable alert-hide review-already">            
            <div class="alert-error">                        
                    <?php echo ADDREVIEW_ALLREADYREVIEW_TEXT; ?>                                             
            </div>
        </div>
        <form name="job-review-form" id="job-review-form" method="post" action="#" novalidate>
            <input type="hidden" name="prosid" value="<?php echo $quote_row->professionalId; ?>"/>
            <input type="hidden" name="quoteid" value="<?php echo $quote_id; ?>"/>
            <input type="hidden" name="projectid" value="<?php echo $project_id; ?>"/>
            <div class="box-content">               
                <div class="form-field" style="padding-bottom:30px;">
                     <label><?php echo ADDREVIEW_YOURRATING_TEXT;?>:</label>                            
                     <div id="targetKeep" class="star-input-container review-form-row"></div>
                     <div style="display:block;" class="rating-tip-container score-description" id="targetKeep-hint" name="targetKeep_hint">
                         <div class="rating-tip-arrow"></div>
                         <p class="rating-tip"><?php echo ADDREVIEW_CLICKTORATE_TEXT;?></p>
                         <div class="rating-tip-arrow-back"></div>                                
                      </div>                                                       
                    </div>
                    <div class="form-field" style="padding-bottom:30px;">                            
                            <label>
                                <?php echo ADDREVIEW_YOURRATE_TEXT;?>
                            </label>
                            <div class="review-form-row" data-field="text">
                                <textarea id="revMsg" name="revMsg" tabindex="105" placeholder="Describe your experience working with <?=$quote_row->firstname?> <?=$quote_row->lastname?>. 
Please be specific."></textarea>
                                <div style="display: none;" class="tooltip tooltip-error tooltip-wide tooltip-right">
                                    <div class="tooltip-content tooltip-content-error"></div>                                        
                                </div>                                    
                            </div>
                     </div>
                     <div class="form-field">    
                            <button type="submit" class="bttn post-review-bttn blue">
                                <?php echo ADDREVIEW_POSTREVIEW_TEXT;?>
                            </button>
                     </div>
            </div>
        </form>
    </div>
</div> */ ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script src="<?=$this->config->config['base_url']?>css/lib/jquery.raty.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#reply-msg-form").validate({
        rules: {
            msg_content: {
                required: true
            }
        },
        submitHandler: function(form) {
            $.post(Host+'bid/reply_message', $("#reply-msg-form").serialize(), function(data) {
               $('#sent-msg-div').append(data);
               $('#send-message-div').hide();
               $('.bid-action-center').show();
               $("#reply-msg-form")[0].reset();
            });     
        }
    });
    
    $("#job-review-form").validate({
        ignore: [],
        rules: {
            score: {
                required: true,
            },
            revMsg: {
                required: true
            }
        },
        submitHandler: function(form) {
            $.post(Host+'bid/submit_job_review', $("#job-review-form").serialize(), function(data) {
               if(data=='already'){
                   $('.review-success').hide();
                   $('.already-reviewed').show();
                   $('.review-already').show();
               }else if(data=='not_login'){
                   location.href=Host+'user/login';
               }else{
                   $('#revMsg').val('');
                   $('.review-success').show();
                   $('.already-reviewed').hide();
                   $('.review-already').hide();                                      
               }
            });     
        }
    });   
    
    $('#targetKeep').raty({
        cancel     : false,        
        targetKeep : true
    });
   
    $('.cancel-message-div').click(function(){
        $('#send-message-div').hide();
        $('.bid-action-center').show();
    });
    
    $('.bid-action-reply').click(function(){
        $('#send-message-div').show();
        $('.bid-action-center').hide();
    });
    
    $('.bid-action-hire').click(function(){
        if(confirm('Are you sure to accept the request?'))
        {
            $('.bid-update-status').show();
            $('.bid-action-center').hide();        
            $.post(Host+'bid/change_project_status', { qid:'<?=$quote_id?>',sts:'Hired',pid:<?=$project_id?> }, function(data) {
                $('.bid-action-hire').hide();
                $('.bid-action-decline').hide();
                $('.bid-action-review').show();
                $('.bid-action-center').show();
                $('.bid-update-status').hide();
             });
         }
    });
    
    $('.bid-action-decline').click(function(){
        if(confirm('Are you sure to decline the request?'))
        {
            $('.bid-update-status').show();
            $('.bid-action-center').hide();        
            $.post(Host+'bid/change_project_status', { qid:'<?=$quote_id?>',sts:'Declined',pid:<?=$project_id?> }, function(data) {
                $('.bid-action-hire').hide();
                $('.bid-action-decline').hide();
                $('.bid-status-cancelled').html(data);
                $('.bid-status-cancelled').show();
                $('.bid-action-review').show();
                $('.bid-action-center').show();
                $('.bid-update-status').hide();
             });
         }
    });
    
    /*$('.job-review-link').click(function(){        
        $('.job_review_modal').show();
    });*/
    
//    $('.contact-support').click(function(){       
//        $('.support-options').css('display','block');
//        $('#fade').css('display','block');							  
//        var popuptopmargin = ($('.support-options').height() + 10) / 2;
//        var popupleftmargin = ($('.support-options').width() + 10) / 2;
//
//        $('.support-options').css({
//                'margin-top' : -popuptopmargin,
//                'margin-left' : -popupleftmargin
//        });
//    });
//    $('.support-close').click(function(){
//        $('.support-options').hide(); 
//        $('#fade').hide();
//    });

    /*$("#hide_review_form").click(function(){
        $(".job_review_modal").hide();
    });*/
    $("#hide_request").click(function(){
        $(".view_request").hide();
    });
    $("#show_request").click(function(){
        $(".view_request").show();
    });
    $("#show_support").click(function(){
        $(".contact_support").show();
    });
    $(".support-close").click(function(){
        $(".contact_support").hide();
    });
    
});
</script>
</body>
</html>

