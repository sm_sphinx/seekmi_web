<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
.glorious-header1{
    background: none repeat scroll 0 0 #fff!important;
    border-bottom: 1px solid #e6e6e6!important;
}

a:link, .pseudo-link:link, .stealth-link:link {
    color: #2962a8!important;
    text-decoration: none;
}
</style>
<title>Pekerjaan</title>
<meta content="<?=$this->config->config['base_url']?>job/" property="og:url"/>
<meta content="Seekmi | Pekerjaan" property="og:title"/>
<meta content="Seekmi" property="og:site_name"/>
<meta property="og:description" content="Get things done by hiring experienced local service professionals." />
<meta property="og:image" content="<?=$this->config->config['base_url']?>images/LogoSymbolWordSquare.png"/>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/terms.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid box-shadow multiple-backgrounds">
    <?php include('common_view.php'); ?>
<div class="glorious-header glorious-header1" data-section="header">
<div class="wrapper"><div class="row header-row">
<div class="header-logo">
<a href="<?=$this->config->config['base_url']?>"><img src="<?=$this->config->config['base_url']?>images/seekmilogo1.png" alt="Seekmi"></a></div>
<div class="header-middle-container"></div>
</div></div></div>

       
    <div class="wrapper text" style="min-height:550px;">

    <div class="row">
        <div class="column-24 position">
            <br>
            <h1 class="title">
                Pekerjaan
                <div class="tack-stamp inline"></div>
            </h1>
                 </div>
    </div>

  
<ul class="bullet-point-section">
    <li><a href="<?=$this->config->config['base_url']?>jobs/bda" title="Software Engineer - Jakarta, Indonesia">Software Engineer - Jakarta, Indonesia</a></li>
    <li><a href="<?=$this->config->config['base_url']?>jobs/bda" title="Business Development Associate - Jakarta, Indonesia">Business Development Associate - Jakarta, Indonesia</a></li>
</ul>
<br/><br/>
</div>
<?php include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script src="<?=$this->config->config['base_url']?>js/jquery.js" type="text/javascript"></script> 
</body>
</html>