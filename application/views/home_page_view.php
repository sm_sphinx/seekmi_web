<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=9">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="google-site-verification" content="1IXSA1lZJBPNyfELzotLkNAU_pKZ6dmSjR7PhyzhrR0" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Get Things Done - Seekmi</title>
        <meta content="<?=$this->config->config['base_url']?>" property="og:url"/>
        <meta content="Seekmi | Get Things Done" property="og:title"/>
        <meta content="Seekmi" property="og:site_name"/>
        <meta property="og:description" content="Get things done by hiring experienced local service professionals." />
        <meta property="og:image" content="<?=$this->config->config['base_url']?>images/LogoSymbolWordSquare.png"/>
        <link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/icons.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/core.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/style.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/zenbox.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/login.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/avenir-next.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/signup.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/land.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/owl.carousel.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/home/owl.theme.css">
        <link rel="stylesheet" href="<?= $this->config->config['base_url'] ?>css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->config->config['base_url'] ?>css/jquery.fancybox.css?v=2.1.5" media="screen" />
        <style type="text/css">
            label.error{
                color: #d51818;
                display: block;
                font-size: 11.2px;        
                margin-top: 5px;
            }
        </style>
        <?php include('before_head_view.php'); ?>
    </head>
    <body class="primo primo-avenir primo-responsive primo-fluid box-shadow multiple-backgrounds landing-page">
        <? include('home_header_view.php'); ?>

        <div class="sticky-cta ng-scope ng-isolate-scope invisible" sticky-header="" sticky-partner-attr="data-sticky-partner" throttle-ms="250" visible="visible">
            <div class="wrapper">
                <div class="logo"></div>
                 <?php if ($this->session->userdata('user_type') == 'professional'){ ?>                    
                 <?php }else{ ?>
                    <form id="homeSearchFormTop" method="post" name="homeSearchFormTop" action="#">            
                        <input name="servicestop" tabindex="2" id="servicestop" class="query" autocomplete="off" placeholder="<?php echo HOME_SEARCH_PLACEHOLDER;?>" type="text">            
                        <button type="button" class="bttn blue searchTopBtn" tabindex="4">
                            <?php echo BUTTON_GET_STARTED;?>
                        </button>           
                    </form>
                 <?php } ?>

            </div>
        </div>
        <div class="overview">
            <div class="hero full-block">
                <div class="wrapper">
                    <div class="dynamic-row">

                        <div class="wrapper  wrap1">
                            <h1 style="color:#fff; visibility:hidden; padding:0px; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; padding-top:0px;" class="title-wrapper choose-account">
                                <b> <span style="font-size:48px;"><?php echo GET_THINGS_DONE; ?></span><br>
                                    <?php echo BY_HIRING; ?>
                                </b><br> 
                                <div><?php echo REGISTER_PART; ?></div>
                            </h1>

                        </div>
                        
                        <div class="wrapper choose-account-wrapper">
          <div style="padding-top:0px;" class="dynamic-row">
            <div class="box service-pro-account bgnone">
              <h1 style="color:#fff; padding:5px; font-size:33px; line-height:41px; padding-top: 0 !important;  padding-top:0px; text-align:left;" class="title-wrapper hire choose-account headtxt1 headingFont"> 
                <b> <span class="get  headnewTxt"><?php echo GET_THINGS_DONE; ?></span><br>
                <?php echo BY_HIRING; ?>
                </b><br>
                <div class="get text01"> 
                    <?php echo GET_PART; ?>
                  <br>
                  <br>
                  <br>
                  <br>
                </div>
              </h1>
            </div>
            <h3 style="visibility:hidden;" class="or choose-account"> or</h3>
            
            <?php if ($this->session->userdata('user_type') == 'professional'){ ?>                    
                 <?php }else{ ?>
            <div class="box standard-account">
             <fieldset> 
                 <?php } ?>
              <?php if ($this->session->userdata('user_type') == 'professional'){ ?>                    
                 <?php }else{ ?>
              <div class="form-field" style="margin-top:8px;">
               <form id="homeSearchFormMid" method="post" name="homeSearchFormMid" action="#">
                <label class=" singnTxt" for="usr_email"><?php echo SERVICE_PART; ?></label>
                 <input type="text" name="servicesmid" id="servicesmid" placeholder="Air Conditioner, Interior Designer, Cleaning" autocomplete="off" class="query error" tabindex="2">
                 <button tabindex="103" id="show" class="bttn blue submit-bttn" type="submit" style="margin-top:8px;"><?php echo GET_FREE_QUOTE; ?></button>
              </form>
             </div> 
             <?php } ?>
           <?php if (!isset($this->session->userdata['userId'])) { ?>
          <div class="form-field login-with-facebook-wrapper">         
            <a class="bttn facebook-bttn" href="<?=$this->config->config['base_url']?>user/fb_login">
                <div class="icon-font icon-font-facebook"></div>&nbsp;
                <span class="separator" style="color:#fff;">
                    |
                </span>
                <?php echo REGISTER_SIGNUP_FB;?>
            </a>
         </div>
     
         <div class="form-field">
            <button tabindex="103" id="show" style="background:#ac0000; border:#ac0000;" class="bttn blue submit-bttn" type="button" onclick="location.href='http://www.seekmi.com/hauth/login/Google'"> <div class="icon-font icon-font-gplus googlp" style="margin-right:0px;"></div>
                <span class="separator" style="color:#fff;">
                    |
                </span>&nbsp;<?php echo LOGIN_SIGN_UP_WITH_GOOGLE;?> &nbsp;&nbsp;&nbsp;
            </button>
                
            <h6 class="txtR"><a href="#professionalDiv" class="link"><br><?php echo ARE_YOU_SERVICE_PROVIDER; ?></a></h6>
          </div>
           <?php }else{ ?>
             <div class="form-field login-with-facebook-wrapper"></div>
           <?php } ?>
             <?php if ($this->session->userdata('user_type') == 'professional'){ ?>                    
                 <?php }else{ ?>
         </fieldset>
        </div>
         <?php } ?>


          </div>
        </div>

                    </div>
                </div>



                <div class="slide slide-0" ng-class="{'current': slide == index}" ng-repeat="index in slides"></div><!-- end ngRepeat: index in slides --><div class="slide slide-1" ng-class="{'current': slide == index}" ng-repeat="index in slides"></div><!-- end ngRepeat: index in slides --><div class="slide slide-2 current" ng-class="{'current': slide == index}" ng-repeat="index in slides"></div><!-- end ngRepeat: index in slides -->
            </div>


            <div class="clearfix"></div>

            <div class="full-block navigation-row">

            </div> 


            <div class="how-it-works">

                <div class="wrapper wrapper-title">
                    <h2><?php echo KAMI_PART; ?></h2>
                    <span class="subtitle"><?php echo SEEKMI_HELP; ?></span>
                </div>

                <div class="container">

                    <div class="row">

                        <div class="col-lg-12 col-lg-offset-1 col-md-12 text-center">
                            <center>
                                <h3><?php echo FIND_COMPARE; ?></h3>
                            </center>

                            <div class="row">

                                <div class="col-md-4 col-sm-4">

                                    <div class="each-step">

                                        <div class="icon icon-match">
                                        </div>

                                        <p><em>1</em><?php echo WHAT_YOU_NEED; ?></p>

                                        <span><?php echo ONE_MINUTE_TO_ANS; ?></span>

                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">

                                    <div class="each-step">

                                        <div class="icon icon-compare">
                                        </div>

                                        <p><em>2</em><?php echo GET_A_QUOTE; ?></p>

                                        <span><?php echo RECIEVE_QUOTES; ?></span>


                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">

                                    <div class="each-step last-step">

                                        <div class="icon icon-hire">
                                        </div>

                                        <p><em>3</em><?php echo SELECT_SERVICE_PROVIDERS; ?></p>

                                        <span><?php echo CHOOSE_IMPLEMENTATIONS; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  


<?php if (!isset($this->session->userdata['userId'])) { ?>
            <div class="content content-section">
                <div class="hook">
                    <form style="padding-top: 70px !important;padding-bottom: 70px;" id="professionalDiv" class="" name="hook" novalidate>
                        <h2><?php echo ARE_YOU_SERVICE_PROVIDER; ?></h2><br>
                        <h1>
                            <?php echo GET_NEW_CLIENT; ?>
                            <div style="text-transform:none;" ng-class="{'has-custom-entry-point': customEntryPoint}" class="subtitle ng-binding">
                                <?php echo SEEKMI_FWD_CLIENT; ?>
                            </div>
                        </h1>

                        <fieldset>
                            <div style="width:100%;" class="form-field form-field-nav">
                                <button onclick="location.href = '<?= $this->config->config['base_url'] ?>pros/register'" class="bttn get-started-btn" type="button">
                                    <?php echo REGISTER_FREE; ?>
                                </button>
                            </div>
                        </fieldset> 

                    </form>
                </div>


            </div>
<?php }?>


<?php if ($this->session->userdata('user_type') == 'professional'){ ?>                    
                 <?php }else{ ?>
            <div class="stories full-block">
                <div class="wrapper wrapper-title">
                    <h2> <?php echo FIND_PROFESSIONAL_SERVICE; ?> </h2>
                    <hr class="hr2">
                    <span class="subtitle"><?php echo RIGHT_NOW; ?> </span> </div>
                <div class="wrapper" id="cards">
                    <div class="dynamic-row"> 
                       
                     <a href="<?= $this->config->config['base_url'] ?>services/search/Architectural%20Services"><span data-card-one="" class="card  card2 padB0">
                        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/pro1.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_ARCHITECT_DESIGNER; ?> </h3></div></span></a>
        
 <a href="<?= $this->config->config['base_url'] ?>services/search/General%20Contracting"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/contractors1.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_CONTRACTOR; ?></h3></div></span></a>     
        
        
 <a href="<?= $this->config->config['base_url'] ?>services/search/Graphic%20Design"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/graphicdesign3.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_GRAPHIC_DESIGN; ?></h3></div></span></a>     
        
  <a href="<?= $this->config->config['base_url'] ?>services/search/General%20Cleaning"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/maidsandcleaning.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_MAID_CLEANING; ?></h3></div></span></a>     
        
  <a href="<?= $this->config->config['base_url'] ?>services/search/Event%20Hair%20and%20Makeup"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/makeup1.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_HAIR_MAKEUP; ?></h3></div></span></a>   
        
         <a href="<?= $this->config->config['base_url'] ?>services/search/Mover"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/mover1.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_MOVER; ?></h3></div></span></a>             
        
   <a href="<?= $this->config->config['base_url'] ?>services/search/Personal%20Training"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/personaltrainer1.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_PERSONAL_TRAINING; ?></h3></div></span></a>         
        
    <a href="<?= $this->config->config['base_url'] ?>services/search/Pest%20Control%20Services"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/pestcontrol.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_PEST_CONTROL; ?></h3></div></span></a>               
                      
    <a href="<?= $this->config->config['base_url'] ?>services/search/Event%20Videography"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/photographers1.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_PHOTOGRAPHERS; ?></h3></div></span></a> 
        
        
      <a href="<?= $this->config->config['base_url'] ?>services/search/Commercial,%20Movie,%20or%20Music%20Videography"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/videographers1.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_VIDEOGRAPHERS; ?></h3></div></span></a>  
        
        <a href="<?= $this->config->config['base_url'] ?>services/search/Yoga%20Lessons"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/yogainstructor1.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_YOGA_INSTRUCTOER; ?></h3></div></span></a> 
        
         
        <a href="<?= $this->config->config['base_url'] ?>services/search/Piano%20Lessons"><span data-card-one="" class="card  card2 padB0">
        <div class="pro1" style=" background:#ffffff url('<?= $this->config->config['base_url'] ?>images/Tutors.jpg') no-repeat scroll center top / cover"><h3><?php echo HOME_TUTORS_LESSONS; ?></h3></div></span></a>             
                                   
                      
                
                       <!-- <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro2"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro3"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro4"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro5"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro6"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro7"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro8"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro9"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro10"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro11"></div></span></a>
                        <a href="#"><span class="card  card2 padB0" data-card-one=""><div class="pro12"></div></span></a>-->
                    </div>
                </div>
            </div>
      <?php } ?>




            <div class="stories full-block">
                <div class="wrapper wrapper-title">
                    <h2>
                        <?php echo CUSTOMER_GET_DONE; ?>
                    </h2>
                    <span class="subtitle"><?php echo CUSTOMER_STORIES; ?></span>
                </div>
                <div class="wrapper" id="cards">
                    <div class="dynamic-row">
                        <a class="fancybox-media" href="https://www.youtube.com/watch?v=KmFqM42Joig">
                            <span class="card" data-card-four="">
                                <div class="norris">
                                    <h3>Rishi Got His <br><span>New Apartment Fixed</span></h3>
                                </div>
                                <div class="professional">
                                    <div class="avatar">
                                        <img src="images/pro-alphonso.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa">
                                    </div>
                                    <div class="clerfix"></div>
                                    <div class="stats">
                                        <p class="name">Rishi</p>
                                    </div>
                                </div>
                                <blockquote>
                                    <p><?php echo HOME_CUSTOMER_TESTIMONIAL_FIRST; ?></p>
                                </blockquote>
                                <center><span class="star-rating star-rating-large stars-5"> 5 reviews</span></center>
                                <div class="info">
                                    <!--<h3>Rishi Got His New Apartment Fixed</h3>-->
                                    <p>View Testimonial</p>
                                </div>
                            </span>
                        </a>

                        <a class="fancybox-media" href="https://www.youtube.com/watch?v=GM9Wez4I14c">                            
                           <span class="card" data-card-five="">
                                <div class="scott">
                                    <h3>Jessica Found <br><span>Contractors She Needs</span></h3>
                                </div>

                                <div class="professional">
                                    <div class="avatar">
                                        <img src="images/pro-scot.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa">
                                    </div>
                                    <div class="clerfix"></div>
                                    <div class="stats">
                                        <p class="name">Jessica</p>
                                    </div>
                                </div>

                                <blockquote>
                                    <p><?php echo HOME_CUSTOMER_TESTIMONIAL_SECOND; ?></p>
                                </blockquote>
                                <center><span class="star-rating star-rating-large stars-5"> 5 reviews</span></center>

                                <div class="info">
                                    <!--<h3>Jessica Found Contractors She Needs</h3>-->
                                    <p>View Testimonial</p>
                                </div>
                            </span>
                        </a>

                        <span class="card padB0" data-card-five="">
                            <div class="beach">
                                <h3>Your Name <br><span>Your Testimonial</span></h3>
                            </div>
                            <div class="professional">
                                <div class="avatar">
                                    <img src="images/pro-scot3.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa">
                                </div>
                                <div class="clerfix"></div>
                                <div class="stats">
                                    <p class="name"><?php echo HOME_JOIN_US; ?></p>
                                </div>
                            </div>
                           <?php if ($this->session->userdata('user_type') == 'professional'){ ?>                    
                           <?php }else{ ?>
                            <div class="">
                                <form id="homeSearchForm" method="post" name="homeSearchForm" action="#">     
                                    <div class="box">
                                        <div class="box-header box-header2" style="padding-top:10px;">
                                            <h2 style="font-size: 23px;"><?php echo GET_INTRODUCED; ?></h2>
                                        </div>
                                        <div class="box-content box-content2">
                                            <fieldset>
                                                <div  class="form-field"  style="padding: 0 0 11px;">
                                                    <label><?php echo WHAT_SERVICE; ?></label>                                     
                                                    <input type="text" name="services" id="services" placeholder="<?php echo PAINTER_PLACEHOLDAER;?>" class="">                                            
                                                </div>
                                                <div class="form-field padB0"  style="padding: 0 0 11px;">
                                                    <button tabindex="202" class="bttn blue full-width" type="submit">
                                                        <?php echo CONTINUE_BUTTON;?>
                                                    </button>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </form>
                            </div>
                           <?php } ?>
                            <!--  <div class="info">
                              <h3>Scot remodeled his kitchen</h3>
                                <p>Read More</p>
                            </div>-->
                            <div class="clearfix"></div>  
                        </span>
                    </div>
                    <div class="marD"></div>

                    <div class="clearfix"></div>
                    <div class="wrapper wrapper-title" style="margin-bottom:0px;">
                        <h2 class="marB10"> <?php echo AS_FEATURED;?></h2>
                            <div id="owl-example" class="owl-carousel">
      <div><a href="https://id.techinasia.com/seekmi-penyedia-jasa-lokal-indonesia/" target="_blank"> <img src="<?= $this->config->config['base_url'] ?>images/logo1.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="https://www.techinasia.com/indonesian-startup-building-thumbtack-southeast-asia/" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo2.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://tekno.liputan6.com/read/2258493/mengenal-seekmi-mak-comblang-penyedia-jasa-profesional" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo3.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://www.vidio.com/watch/93851-mudah-mencari-penyedia-jasa-dengan-seekmi-com" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo4.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://startupbisnis.com/ukm-indonesia-you-better-go-digital-now-interview-dengan-nayoko-wicaksono-ceo-seekmi-com/" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo5.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://www.techno.id/startup/seekmicom-startup-dengan-pelayanan-jasa-profesional-1508078.html" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo6.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://portal.cbn.net.id/cbprtl/cybertech/detail.aspx?x=Site+Info&y=cybertech|0|0|11|304" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo10.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://selular.id/news/2015/06/platform-ini-mudahkan-anda-cari-jasa-profesional-via-internet/" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo8.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://futurestartup.com/2015/07/08/seekmi-an-indonesian-startup-connecting-the-service-providers-and-the-service-seekers/" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo9.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://www.merdeka.com/teknologi/seekmicom-berambisi-jadi-tokopedia-jasa-layanan.html" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo7.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://news.babe.co.id/3910638" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo11.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
      <div><a href="http://e27.co/startup/seekmi" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo12.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
    </div><?php /*?>
                        <div class="featuredlogo">
                            <div class="featlogo featlogo1"><img src="<?= $this->config->config['base_url'] ?>images/logo0.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></div>
                            <div class="featlogo"><a href="https://www.techinasia.com/tag/seekmi/" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo1.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
                            <div class="featlogo"><a href="http://www.bintang.com/tag/seekmi" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo2.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
                            <div class="featlogo"><a href="http://www.liputan6.com/tag/seekmi" target="_blank"><img src="<?= $this->config->config['base_url'] ?>images/logo3.jpg" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa"></a></div>
                            
                            <div class="clearfix"></div>
                        </div> <?php */?></div>
                    <div class="clearfix"></div>
                </div>
            </div>




            <div class="mobile-app-banner full-block">
                <div class="wrapper body-text">
                    <div class="dynamic-row">
                        <div class="copy-column ng-scope" ng-controller="MobileAppBannerController">
                            <h2><?php echo HEADING_OUT;?></h2>
                            <p><?php echo RECIVE_QUOTES_WHEREVER?></p>
                            <form class="ng-pristine ng-valid" ng-submit="send()">
                                <fieldset>
                                    <div class="input-prepend input-append">
                                        <span class="add-on">
                                            <span class="icon-font icon-font-mail" aria-hidden="true">
                                            </span>
                                        </span>
                                        <input class="ng-pristine ng-valid" name="phone_number" placeholder="<?php echo EMAIL_PLACEHOLDER;?>" ng-model="phone_number" type="text">
                                        <button type="submit" class="bttn ng-binding" ng-bind="button_text" ng-disabled="sending">Notify Me</button>
                                    </div>
                                </fieldset>
                            </form>
                            <div class="download-button">
                                <a href="#" style="float:left;" class="downl">
                                  <?php if(get_cookie('language') == 'english'){ ?>
                                    <img src="<?= $this->config->config['base_url'] ?>images/download-on-the-app-store.png" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa">
                                  <?php } else{ ?>
                                    <img src="<?= $this->config->config['base_url'] ?>images/download-on-the-app-store-id.png" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa">
                                  <?php } ?>
                                </a>
                                <a href="#" style="float:left; margin-left:1%;" class="downl">
                                    <?php if(get_cookie('language') == 'english'){ ?>
                                      <img src="<?= $this->config->config['base_url'] ?>images/googlplay.png" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa">
                                    <?php }else{ ?>
                                      <img src="<?= $this->config->config['base_url'] ?>images/googlplay-id.png" alt="tempat terbaik bagi layanan penyedia jasa dan kebutuhan jasa">
                                    <?php } ?>
                                </a></div>
                        </div>

                        <div class="column-5 iphone"></div>
                    </div>
                </div>
            </div>

            <div class="pros full-block">
                <div class="wrapper">
                    <div class="wrapper wrapper-title">
                        <h2 class="pro-header">
                           <?php echo THOUSANDS_PROFESSINAL;?>
                        </h2>
                        <span class="subtitle"><?php echo PRO_STORIES;?></span>
                    </div>
                    <div class="dynamic-row">
                        <div class="main-video">
                            <a class="fancybox-media" href="https://www.youtube.com/watch?v=86c2b469Beg">
                                <div class="play"></div>
                                <!--                    <h3>
                                                        <span>Kevin Sutantyo</span><br>
                                                        Partner at RMKB 
                                                    </h3>-->
                                <div class="info">
                                    <h3>Arne Van Looveren - CCO of Y Digital Asia  </h3>
                                    <p>
                                        <span class="icon-font-play"></span>
                                        Watch the video
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="video-1">
                            <a class="fancybox-media" href="https://www.youtube.com/watch?v=Tuyd43qFjtM">
                                <div class="play"></div>
                                <!--                    <h3>
                                                        <span>Mirtha Roseria</span><br>
                                                        Make Up Artist 
                                                    </h3>-->
                                <div class="info">
                                    <h3>Mirtha Roseria - Make Up Artist</h3>
                                    <p>
                                        <span class="icon-font-play"></span>
                                        Watch the video
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="video-2">
                            <a class="fancybox-media" href="https://www.youtube.com/watch?v=Xov4hDkfF0U">
                                <div class="play"></div>
                                <!--                    <h3>
                                                        <span>Benson Kawengian</span><br>
                                                        Owner of Strategic Pest Control
                                                    </h3>-->
                                <div class="info">
                                    <h3>Benson Kawengian - Owner of Strategic Pest Control</h3>
                                    <p>
                                        <span class="icon-font-play"></span>
                                        Watch the video
                                    </p>
                                </div>
                            </a>
                        </div>
                        <?php if(!isset($this->session->userdata['userId'])) { ?>
                            <div class="sign-up-box" style="cursor: inherit;">
                                <h4><?php echo ARE_YOU_PROFESSIONAL;?></h4>
                                <a href="<?= $this->config->config['base_url'] ?>pros/register" class="bttn blue"><?php echo BUTTON_SIGN_UP_PRO; ?></a>
                            </div>
                        <?php }else{ if ($this->session->userdata('user_type') == 'professional') { ?>
                             <div class="sign-up-box" style="cursor: inherit;">
                                <h4><?php echo HOME_REFER_EARN_TEXT; ?></h4>
                                <a href="<?= $this->config->config['base_url'] ?>profile/settings" class="bttn blue"><?php echo BUTTON_HOME_REFER_EARN_TEXT; ?></a>
                            </div>
                        <?php }} ?>
                    </div>
                </div>

            </div>
        </div> 


        <?php include('footer_view.php'); ?>
        <script src="<?= $this->config->config['base_url'] ?>js/fbds.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/owl.carousel.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/insight.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/conversion_async.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/quant.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/bat.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/tag.js" async=""></script>
        <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>js/jquery-validate.js"></script>
        <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>js/jquery.fancybox.js?v=2.1.5"></script>
        <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>js/jquery.fancybox-media.js?v=1.0.6"></script>
        <script type="text/javascript">
                                            $(window).on("scroll touchmove", function () {
                                                //$('#header_nav').toggleClass('tiny', $(document).scrollTop() > 0);
                                                //console.log('--------------------------sd'+$(document).scrollTop());
                                                if ($(document).scrollTop() > 300) {
                                                    $('.sticky-cta').removeClass('invisible');
                                                    $('.sticky-cta').addClass('visible');
                                                }
                                                else {
                                                    $('.sticky-cta').removeClass('visible');
                                                    $('.sticky-cta').addClass('invisible');

                                                }

                                            });


        </script>
        <style>
            .ui-autocomplete-loading {
                background: white url("<?= $this->config->config['base_url'] ?>css/images/ui-anim_basic_16x16.gif") right center no-repeat;
            }
        </style>
        <script>
            $(function () {
                function split(val) {
                    return val.split(/,\s*/);
                }
                function extractLast(term) {
                    return split(term).pop();
                }
               

                $("#services,#servicestop,#servicesmid")
                        // don't navigate away from the field on tab when selecting an item
                        .bind("keydown", function (event) {
                            if (event.keyCode === $.ui.keyCode.TAB &&
                                    $(this).autocomplete("instance").menu.active) {
                                event.preventDefault();
                            }
                        })
                        .autocomplete({
                            source: function (request, response) {
                                $.getJSON("<?= $this->config->config['base_url'] ?>home/get_services", {
                                    search: extractLast(request.term)
                                }, response);
                            },
                            search: function () {
                                // custom minLength
                                var term = extractLast(this.value);
                                if (term.length < 2) {
                                    return false;
                                }
                            },
                            focus: function () {
                                // prevent value inserted on focus
                                return false;
                            },
                            select: function (event, ui) {
                                this.value = ui.item.value
                                $("#services,#servicestop,#servicesmid").removeClass("ui-autocomplete-loading");
                                return false;
                            }      
                        });
            });

            $(document).ready(function () {
                $("#homeSearchForm").validate({
                    rules: {
                        services: {
                            required: true
                        }
                    },
                    submitHandler: function (form) {
                        location.href = encodeURI(Host + 'services/search/' + $('#services').val());
                    }
                });

                /*$("#homeSearchFormTop").validate({
                 rules: {
                 servicestop: {
                 required: true
                 }           
                 },
                 submitHandler: function(form) {
                 location.href=encodeURI(Host+'services/search/'+$('#servicestop').val());   
                 }
                 });*/

                $(".searchTopBtn").click(function () {
                    if ($('#servicestop').val() != '') {
                        location.href = encodeURI(Host + 'services/search/' + $('#servicestop').val());
                    } else {
                        alert('Please select service');
                        return false;
                    }
                });

                $("#homeSearchFormMid").validate({
                    rules: {
                        servicesmid: {
                            required: true
                        }
                    },
                    submitHandler: function (form) {
                        location.href = encodeURI(Host + 'services/search/' + $('#servicesmid').val());
                    }
                });

                $('.fancybox-media')
                        .attr('rel', 'media-gallery')
                        .fancybox({
                            openEffect: 'none',
                            closeEffect: 'none',
                            prevEffect: 'none',
                            nextEffect: 'none',
                            arrows: false,
                            helpers: {
                                media: {},
                                buttons: {}
                            }
                        });
            });
			
			    $(document).ready(function() {
     
      $("#owl-example").owlCarousel();
     
    });
        </script>
    </body>
</html>

