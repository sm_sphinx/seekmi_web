<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="google-site-verification" content="1IXSA1lZJBPNyfELzotLkNAU_pKZ6dmSjR7PhyzhrR0" />
<title> <?=$page_title?> - Seekmi</title>
<style type="text/css">
.glorious-header1{
    background: none repeat scroll 0 0 #fff!important;
    border-bottom: 1px solid #e6e6e6!important;
}

.primo .pod-request-form fieldset .date-picker-container .form-field-length .input-append {
    margin: 0 8px 0 0!important;
    position: relative!important;
    text-align: left!important;
}

.primo .pod-request-form fieldset .date-picker-container .form-field-length .input-append input {
    font-size: 16.8px!important;
    left: 0!important;
    line-height: 17px!important;
    margin: 0!important;
    max-width: 100%!important;
    min-width: 28px!important;
    padding: 10px!important;
    position: absolute!important;
    width: 100%!important;
}
.primo .pod-request-form fieldset .date-picker-container .form-field-length .input-append .add-on {
    font-size: 15px!important;
}
.primo .pod-request-form fieldset .date-picker-container .form-field-length .input-append .add-on {
    left: 100%!important;
    margin-left: -1px!important;
    position: relative!important;
    width: 60px!important;
}
</style>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/req.css">
<link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/jquery.timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="<?= $this->config->config['base_url'] ?>css/jquery-ui.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
 <?php include('header_view.php'); ?>
 <div class="wrapper builder">
   
    <?php /*if($pros_count==0){ ?>
        <div class="error content wrapper">
            <div class="box form-error">
                <div class="box-header">
                    <h2>While we update and grow our database, you may have better luck with these services.</h2>
                </div>            
            </div>
            
            <div class="form-field" style="margin-top:8px;">
             <?php if(!empty($pros_service_list)){              
                 $p=0;
                 foreach($pros_service_list as $pros_service_row){
                     if($p > 0){
                         echo ", ";
                     }
                 ?>
             <a href="<?=$this->config->config['base_url']?>services/search/<?php echo $pros_service_row->serviceName; ?>"><?php echo $pros_service_row->serviceName; ?></a>
             <?php $p++; }} ?>
             </div>
        </div>        
     <?php }else{*/ ?>
    <?php if(empty($question_list)){ ?>
<!--    <div class="suggestions content wrapper">
        <div class="box form-primary">
            <div class="box-header">
                <h2>Which of the following best matches your needs?</h2>
            </div>
            <div class="box-content body-text">
                <ul><li style="display: list-item;"><a class="suggestion" href="#">DJ</a></li></ul>
                <a href="" style="display: none;">Show More</a>
                <br><br>
            </div>
            <div class="box-content body-text">
                <p>
                    <strong>Not seeing what you're looking for?</strong><br>
                    Browse categories in
                    <a href="/home-improvement">home</a>,
                    <a href="/events">events</a>,
                    <a href="/lessons">lessons</a>,
                    <a href="/wellness">wellness</a>, and
                    <a href="/more-services">more</a>.
                </p>
            </div>
        </div>
    </div>-->

<div class="wrapper pod-request-form">
   <div>
       <div  class="chooser box" style="padding: 30px;margin-top: 20px;">
        <?php if(!empty($suggestion_list)){ ?> 
        <div class="box-header">
            <h2>Which of the following best matches your needs?</h2>
        </div>
        
        <div class="box-content body-text">
            <ul>
              <?php foreach($suggestion_list as $suggestion_row){ ?>
                <li>
                    <a href="<?=$this->config->config['base_url']?>services/search/<?php echo $suggestion_row->serviceName; ?>">
                        <?php echo $suggestion_row->serviceName; ?>
                    </a>
                </li>
              <?php } ?>
            </ul>
<!--            <a class="ng-scope" href="" ng-if="categories.length > chooserShowCount" ng-click="chooserShowMore()">
                Show More
            </a>-->
        </div>
        <?php } ?>

        <div class="box-footer">
            <h4>Not seeing what you're looking for?</h4>
            <p style="margin:5px 0px;">Suggest "<?php echo $service; ?>" as a new service.</p>
            <div class="alert-closable alert-hide suggest-already-error">       
                <input class="alert-close" type="checkbox"/>
                <div class="alert-error">                                
                    <p>
                        <?php echo SEARCH_SUGGEST_ALREADY_TEXT; ?>
                    </p>
                </div>
            </div>
            <div class="alert-closable alert-hide suggest-success">    
                <input class="alert-close" type="checkbox"/>
                <div class="alert-success">                                
                    <p>
                        <?php echo SEARCH_SUGGEST_SUCCESS_TEXT; ?>
                    </p>
                </div>
            </div>
            <div class="form-field" style="margin-top:8px;">
                <form id="user_service_form" method="post" name="user_service_form" action="#">                  
                    <input type="text" name="user_service" id="user_service" autocomplete="off" class="query error" value="<?php echo $service; ?>">                  
                    <input type="hidden" name="searchId" value="<?php echo $searchId; ?>"/>
                  <button id="show" class="bttn blue submit-bttn" type="submit" style="margin-top:8px;"><?php echo BUTTON_SUBMIT; ?></button>
                </form>
            </div>
        </div>
    </div>
   </div>
</div>

<?php /*
    <div class="error content wrapper">
        <div class="box form-error">
            <div class="box-header">
                <h2><?php echo SEARCH_ERROR_MSG; ?> <a href="mailto:support@seekmi.com">support@seekmi.com</a></h2>
            </div>            
        </div>
        <div class="form-field" style="margin-top:8px;">
            <form id="homeSearchFormMid" method="post" name="homeSearchFormMid" action="#">
              <label class=" singnTxt" for="usr_email"><?php echo SERVICE_PART; ?></label>
              <input type="text" name="servicesmid" id="servicesmid" placeholder="A/C Repair, Renovation, Interior Design…" autocomplete="off" class="query error" tabindex="2">
              <input type="hidden" id="searchKeywordHid" value=""/>
              <button tabindex="103" id="show" class="bttn blue submit-bttn" type="submit" style="margin-top:8px;"><?php echo GET_FREE_QUOTE; ?></button>
            </form>
         </div><br/>
         <?php echo SEARCH_DID_MEAN; ?><div class="form-field" style="margin-top:8px;">
         <?php if(!empty($suggestion_list)){              
             $s=0;
             foreach($suggestion_list as $suggestion_row){
                 if($s > 0){
                     echo ", ";
                 }
             ?>
         <a href="<?=$this->config->config['base_url']?>services/search/<?php echo $suggestion_row->serviceName; ?>"><?php echo $suggestion_row->serviceName; ?></a>
         <?php $s++; }} ?>
         </div>
    </div>
*/ ?>
    

    <?php }else{ ?>
    <div class="sp-wrapper">
    <div data-title="" class="body-title">
        <h1 data-success=""><?php echo SERVICE_INTRODUCE_TEXT; ?> <?=$service?></h1>
    </div>
        
     <div class="error" id="inactive_error"  style="display:none;">
        <div class="box form-error">
            <div class="box-header">
                <h2 style="color:#cc0000;">Your account with this email address is inactive. Please contact with support.</h2>
            </div>            
        </div>
    </div>
        
    <div class="error" id="invalid_login_error" style="display:none;">
        <div class="box form-error">
            <div class="box-header">
                <h2 style="color:#cc0000;">Sorry, but the e-mail you entered is already registered. Please use a different e-mail or log into your existing account.</h2>
            </div>            
        </div>
    </div>
        
    <div class="error" id="failed_error" style="display:none;">
        <div class="box form-error">
            <div class="box-header">
                <h2 style="color:#cc0000;">Sorry, but something went wrong on our end. Please try again.</h2>
            </div>            
        </div>
    </div>
    
    <div class="loading-bar" style="display:none;">
        <p class="text">Just a moment…</p>
    </div>
        
    <div class="wrapper content" id="main_content">    
    <div class="dynamic-row">
    <div class="request-block">
        <div class="box request-form-container pod-request-form form-icons">
            <div class="box-header">
                <h2 data-section="form title">
                    <?php echo SERVICE_TELL_US_ABOUT_TEXT; ?>
                </h2>
            </div>
            <div class="box-content">
             <div class="curtain-inner-collapse">
               <form id="requestForm" name="requestForm" class="curtain-inner-collapse" method="post" action="<?=$this->config->config['base_url']?>services/submit_request" accept-charset="ISO-8859-1">
                <?php 
                $icon_arr = array('informatic'=>'icon-font-info','details'=>'icon-font-pencil','time'=>'icon-font-clock','travel'=>'icon-font-truck','duration'=>'icon-font-clock','zipcode'=>'icon-font-location');
                $questId=array(); foreach($question_list as $key=>$value){   
                    $cnt=0;
                    ?>
                <fieldset class="fieldset-questions">
                <?php  foreach($value as $question_row){
                //print_r($question_row);
                $type = $question_row->type;
                if($question_row->$type=='Y')
                {
                    if($cnt==0)
                    {
                    ?>
                    <?php if($question_row->fieldType!='address'){ ?>
                    <legend class="<?php echo $icon_arr[$key]; ?>">&nbsp;</legend>
                    <?php
                    } }
                    $questId[]=$question_row->questionId; ?>
                    <div class="form-field">
                    <?php if($question_row->fieldType!='address'){ ?>
                    <label for="labeled-inputs-182"><?php echo $question_row->questionName; ?></label>
                    <?php } ?>
                    <?php if(($question_row->fieldType=='checkbox' && $key!='travel') || $question_row->fieldType=='radio'){ ?>
                       <div class="checkbox-section">
                    <?php }if($question_row->fieldType=='select'){ ?>  
                        <div class="describable-select">
                            <select name="question_<?=$question_row->questionId?>" required onchange="showTypeDisplay('<?=$question_row->questionId?>',this,this.value);">
                                <option value="">- Select one -</option>
                    <?php } if($question_row->fieldType=='textarea'){ ?>
                             <textarea name="question_<?=$question_row->questionId?>" required></textarea>
                    <?php } if($question_row->fieldType=='textbox'){                             
                        ?>                           
                       <input type="text" name="question_<?=$question_row->questionId?>" required="" value=""/>
                    <?php } if($question_row->fieldType=='address'){  ?>     
                        
                       <?php }                      
                       if(!empty($option_list[$question_row->questionId])){ 
                         foreach($option_list[$question_row->questionId] as $option_row){
                         if($question_row->fieldType=='checkbox'){
                      ?>                       
                        <div role="presentation">
                        <label role="option" class="select-option" aria-selected="false">
                          <?php if($option_row->optionType=='other'){ ?>
                            <input required="" type="checkbox" class="radio" name="question_<?php echo $question_row->questionId; ?>[]" value="<?php echo $option_row->optionId; ?>" onclick="addoptionId(this.checked,'<?php echo $option_row->optionId; ?>','<?php echo $question_row->questionId; ?>');">
                             <input type="text" name="serviceOtherText_<?php echo $question_row->questionId; ?>" tabindex="0" placeholder="Other">
                          <?php } else{ ?>
                             <input required="" type="checkbox" class="radio" name="question_<?php echo $question_row->questionId; ?>[]" value="<?php echo $option_row->optionId; ?>" onclick="displayMilesDiv(this.checked,'<?php echo $question_row->questionId; ?>','<?php echo $option_row->optionType; ?>','<?php echo $option_row->optionId; ?>')">&nbsp;<?php echo $option_row->optionText; ?>
                          <?php } ?>  
                        </label>
                        </div>
                         <?php if($option_row->optionType=='miles'){ ?>
                         <div style="display: none;" id="milesdisplay_<?php echo $question_row->questionId; ?>" class="form-field">
                             <label>I will travel up to</label>
                             <select name="miles_input_<?php echo $option_row->optionId; ?>" id="miles_input_<?php echo $question_row->questionId; ?>">
                                 <option value="5">5 kilometer</option>
                                 <option value="15">15 kilometer</option>
                                 <option value="25">25 kilometer</option>
                                 <option value="50">50 kilometer</option>
                             </select>
                             <div class="subtext-form"></div>                                 
                         </div>
                         <?php } ?>
                      <?php } if($question_row->fieldType=='select'){ ?>                          
                        <option value="<?php echo $option_row->optionId; ?>" tp="<?php echo $option_row->optionType; ?>"><?php echo $option_row->optionText; ?></option>                      
                      <?php } if($question_row->fieldType=='radio'){ ?>
                          <div role="presentation">
                            <label role="option" class="select-option" aria-selected="false">
                                <input type="radio" class="radio" name="question_<?php echo $question_row->questionId; ?>" value="<?php echo $option_row->optionId; ?>"><?php echo $option_row->optionText; ?>
                            </label>
                          </div>
                      <?php } ?>
                    
                    <?php }} ?>
                   <?php if($question_row->fieldType=='select' || ($question_row->fieldType=='checkbox' && $key!='travel') || $question_row->fieldType=='radio'){ ?>
                        </div>
                    <?php } if($question_row->fieldType=='select'){ ?>
                        </select>
                        <input type="text" id="serviceOtherText_<?php echo $question_row->questionId; ?>" name="serviceOtherText_<?php echo $question_row->questionId; ?>" placeholder="Other" style="display:none;">
                    <?php } ?>                    
                    </div>
                     <input type="hidden" id="otherOptionId_<?php echo $question_row->questionId; ?>" name="otherOptionId_<?php echo $question_row->questionId; ?>" value=""/>
                     <input type="hidden" name="qustType_<?php echo $question_row->questionId; ?>" value="<?php echo $question_row->fieldType; ?>"/>
                     <input type="hidden" name="optionType_<?php echo $question_row->questionId; ?>" id="optionType_<?php echo $question_row->questionId; ?>" value=""/>
                    <?php if($question_row->fieldType=='select'){ ?>
                    <div class="subtext-form">&nbsp;</div>
                    <div id="datetimehoursdisplay_<?php echo $question_row->questionId; ?>" style="display: none;" class="form-field">
                        <label>What day do you need the <?=$service?>?</label>
                        <div class="date-picker-container">
                        
                        <div class="form-field form-field-date">
                        <label class="input-prepend">
                        <span class="add-on icon-font-calendar"></span>
                        <input type="text" class="datepicker" name="date_input" id="date_input_append<?php echo $question_row->questionId; ?>" readonly disabled="">
                        </label>
                        </div>

                        <div class="form-field form-field-column-6">
                        <label>At what time?</label>
                        <input type="text" class="timepicker" name="time_input" id="time_input_append<?php echo $question_row->questionId; ?>" disabled="">
                        </div>
                            
                        <div class="form-field form-field-length form-field-column-5">
                        <label>For how long?</label>
                        <div class="input-append">
                            <input type="text" name="hours_input" id="hours_input_append<?php echo $question_row->questionId; ?>" disabled="">
                        <span class="add-on"> hours</span>
                        </div>
                        </div>
                       </div>                       
                    </div>
                    <?php } ?> 
                    <?php if($question_row->fieldType=='datepicker' && $type=='duration'){ ?>
                    <div class="subtext-form">&nbsp;</div>
                    <div class="form-field">
                        <label>From</label>
                        <div class="date-picker-container">                        
                        <div class="form-field form-field-date form-field-column-6">
                        <label class="input-prepend">
                        <span class="add-on icon-font-calendar"></span>
                        <input style="width: 100%;" type="text" class="datepicker" name="question_from_date_<?php echo $question_row->questionId; ?>" id="question_from_date_<?php echo $question_row->questionId; ?>" readonly required="">
                        </label>
                        </div>

                        <div class="form-field form-field-column-5" style="margin-left:30px;">                        
                        <input type="text" class="timepicker" name="question_from_time_<?php echo $question_row->questionId; ?>" id="question_from_time_<?php echo $question_row->questionId; ?>">
                        </div>
                       </div>                       
                    </div>
                    <div class="form-field">
                        <label>To</label>
                        <div class="date-picker-container">                        
                        <div class="form-field form-field-date form-field-column-6">
                        <label class="input-prepend">
                        <span class="add-on icon-font-calendar"></span>
                        <input style="width: 100%;" type="text" class="datepicker" name="question_to_date_<?php echo $question_row->questionId; ?>" id="question_to_date_<?php echo $question_row->questionId; ?>" readonly required="">
                        </label>
                        </div>

                        <div class="form-field form-field-column-5" style="margin-left:30px;">                        
                        <input type="text" class="timepicker" name="question_to_time_<?php echo $question_row->questionId; ?>" id="question_from_time_<?php echo $question_row->questionId; ?>">
                        </div>
                       </div>                       
                    </div>
                    <?php } ?>
                    
                <?php }
                $cnt++;
                } ?>                    
               </fieldset>
              <?php } 
              if(!empty($questId)){
                  $questStr=implode(',',$questId);
              }else{
                  $questStr='';
              }              
              ?>
                   
                   <fieldset class="fieldset-questions">
                
                    <legend class="icon-font-location">&nbsp;</legend>
                   <div class="form-field-city form-field form-field" field="" style="width:100%;">
                        <div>
                        <label for="province"><?php echo EDITPRO_PROVINCE_TEXT; ?></label>
                        <select name="province" id="province" onchange="ajaxcity(this.value)" required>
                            <option value=""><?php echo EDITPRO_SELECT_TEXT; ?></option>
                            <?php 
                            if($province_list!=''){
                                foreach ($province_list as $province_row){
                                    echo '<option value="'.$province_row->provId.'">'.$province_row->provName.'</option>';
                                }
                            }
                            ?>
                        </select>
                        </div>
                    </div>                        
                    <div class="form-field-city form-field form-field" expr="address.city" field="" style="width:100%;">
                        <div>
                        <label for="city"><?php echo EDITPRO_CITY_TEXT; ?></label> 
                        <select name="city" id="cityList" onchange="ajaxdistrict(this.value)" required>
                            <option value=""><?php echo EDITPRO_SELECT_TEXT; ?></option>
                        </select>
                        </div>
                    </div>
                    <div class="form-field-state form-field form-field" field="" style="width:100%;" >
                        <div>
                        <label for="district"><?php echo EDITPRO_DIST_TEXT; ?></label>
                        <select name="district" id="districtList" required>
                            <option value=""><?php echo EDITPRO_SELECT_TEXT; ?></option>
                        </select>
                        </div>
                    </div>
                   </fieldset>
                   
                   <input type="hidden" name="questionIds" value="<?php echo $questStr; ?>"/>
              <?php if($this->session->userdata('userId')==''){ ?> 
                   <fieldset data-fieldset="phone" class="form-field fieldset-phone request-form-delivery-preference">
                     
                    <legend class="icon-font-pin">Contact</legend>                    
                    <div class="form-field"><?php echo SEARCH_WITHIN_HOURS_TEXT; ?> <span data-replace-with="plural taxonym"><?php echo $service; ?><?php if(get_cookie('language') === 'english'){ echo 's'; } ?>
                    </span> <?php echo SEARCH_WITHIN_HOURS_TEXT2; ?>
                    </div>                    
<!--                    <div class="form-field request-form-delivery-preference-email-sms">
                    <label>
                        <input type="radio" id="preference_email_sms" class="" name="user_preference" tabindex="0" value="email_sms" required=""> I want quotes by email and text message
                    </label>                    
                    </div>
                    <div class="form-field request-form-delivery-preference-email">
                    <label>
                        <input type="radio" id="preference_email" class="" name="user_preference" tabindex="0" value="email" required=""> I want quotes by email only
                    </label>                    
                    </div>-->
                    <div class="form-field usr_form_display">
                    <label for="usr_name">Full name</label>
                    <div>
                        <input type="text" class="" id="usr_name" name="usr_name" tabindex="0" required="">                              
                    </div>
                    </div>
                    <div class="form-field usr_form_display">
                    <label for="usr_email">Email address</label>
                    <div>
                        <input type="email" class="" id="usr_email" name="usr_email" tabindex="0" required="" email>                              
                    </div>
                    </div>
                    <div class="form-field usr_form_display usr_form_phone_display">
                    <label for="usr_phone">Phone number</label>
                        <input type="tel" class="" id="usr_phone" name="usr_phone" tabindex="0" required="">                                                    
                    </div> 
                    <div class="form-field usr_form_display">
                    <label for="usr_pwd">Password</label>
                        <input type="password" class="" id="usr_pwd" name="usr_pwd" tabindex="0" required="" minlength="5">                                                       
                    </div>
                                       
                   </fieldset>
               <?php }else{ ?>
                   <fieldset data-fieldset="phone" class="form-field fieldset-phone request-form-delivery-preference">                     
                    <legend class="icon-font-pin">Contact</legend>                    
                    <label class="form-field"><?php echo SEARCH_WITHIN_HOURS_TEXT; ?> <span data-replace-with="plural taxonym"><?php echo $service; ?><?php if(get_cookie('language') === 'english'){ echo 's'; } ?>
                    </span> <?php echo SEARCH_WITHIN_HOURS_TEXT2; ?>
                    </label>  
                    <div class="form-field usr_form_display">
                    <label for="usr_name">Full name</label>
                    <div>
                        <input type="text" class="" id="usr_name" name="usr_name" value="<?php echo $user_data->firstname." ".$user_data->lastname; ?>" tabindex="0" required="">                              
                    </div>
                    </div>
                    <div class="form-field usr_form_display usr_form_phone_display">
                    <label for="usr_phone">Phone number</label>
                    <input type="tel" class="" id="usr_phone" name="usr_phone" tabindex="0" value="<?php echo $user_data->phone; ?>" required="">                                                    
                    </div> 
                   </fieldset>
               <?php } ?>
                   <input type="hidden" name="serviceid" value="<?php echo $serviceId; ?>"/>                  
               <fieldset class="fieldset-submit">
                <div class="form-field">
                    <button class="bttn blue post-bttn submit-request-btn" type="submit">
                        <?php echo SEARCH_SEND_REQUEST_TEXT; ?>
                    </button>
                </div>
                <div class="form-field subtext-form request-disclaimer body-text">
                <p>
                    <?php echo SEARCH_READ_TERMS_TEXT; ?>
                    <a target="_blank" href="<?=$this->config->config['main_base_url']?>terms/"><?php echo REGISTER_TERMS; ?></a>. <?php echo SEARCVH_CALL_TEXT; ?>.
                </p>
                                       

                  
                </div>
               </fieldset>                   
            </form>
         </div>
        </div>
     </div>
   </div>
    <div class="sidebar">
    <div class="box box-process">
        <div class="box-header">
            <h3><?php echo SEARCH_HOWDOES;?></h3>
        </div>
        <div class="box-content">
            <p>
                <?php echo SEARCH_WITHIN_TEXT;?>
            </p>
        </div>
    </div>
    </div>
  </div>
</div>
</div>
    <?php }
//} 
    ?>
</div>
 
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?= $this->config->config['base_url'] ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script src="<?=$this->config->config['base_url']?>js/jquery.timepicker.js"></script>
<script src="<?=$this->config->config['base_url']?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
<?php if(empty($question_list)){ ?>   
$(function () {    

    $("#user_service_form").validate({
        rules: {
            user_service: {
                required: true
            }
        },
        submitHandler: function (form) {
            $.post(Host+'services/add_suggest_service', $("#user_service_form").serialize(), function(data) {                        
               $('.alert-close').attr('checked', false);
               if(data=='already'){
                   $('.suggest-already-error').show();
                   $('.suggest-success').hide();  
                }else{
                   $('.suggest-already-error').hide();
                   $('.suggest-success').show();
                }
            });
        }
    });
});
<?php } ?> 
    $(window).on("scroll touchmove", function () {
            //$('#header_nav').toggleClass('tiny', $(document).scrollTop() > 0);
            //console.log('--------------------------sd'+$(document).scrollTop());
            if($(document).scrollTop()>300){
                    $('.sticky-cta').removeClass('invisible');
                    $('.sticky-cta').addClass('visible');
            }
            else{
                    $('.sticky-cta').removeClass('visible');
                    $('.sticky-cta').addClass('invisible');

            }

    });
   var nowDate = new Date();
   var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
   $('.datepicker').datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true,
        'startDate': today 
   });
   
    $('.timepicker').timepicker({
        'showDuration': true,
        'timeFormat': 'g:i A'
    });

</script>
<style>
  .ui-autocomplete-loading {
    background: white url("<?=$this->config->config['base_url']?>css/images/ui-anim_basic_16x16.gif") right center no-repeat;
  }
 </style>
 <script type="text/javascript">  
  $(document).ready(function () {
    $("#requestForm").validate({        
        errorPlacement: function (error, element) { error.insertAfter($(element).parents('div').prev('label'));  },        
        submitHandler: function(form) {
            $('.submit-request-btn').hide();
            $.post(Host+'services/submit_request', $("#requestForm").serialize(), function(data) {
               $('body,html').animate({
			scrollTop: 0 ,
		 	}, 700
               );
               $('.loading-bar').show();               
               if(data=='inactive'){
                   $('.loading-bar').hide();
                   $('#inactive_error').show();
                   $('#failed_error').hide();
                   $('#invalid_login_error').hide();    
                   $('.submit-request-btn').show();
                   return false; 
                }else if(data=='invalid'){
                   $('.loading-bar').hide();
                   $('#invalid_login_error').show();
                   $('#inactive_error').hide();
                   $('#failed_error').hide();
                   $('.submit-request-btn').show();
                   return false; 
                }else if(data=='failed'){
                   $('.loading-bar').hide();
                   $('#failed_error').show();
                   $('#inactive_error').hide();
                   $('#invalid_login_error').hide();   
                   $('.submit-request-btn').show();
                   return false; 
                }else{
                   $('#inactive_error').hide();
                   $('#failed_error').hide();
                   $('#invalid_login_error').hide();                                     
                   $('.submit-request-btn').hide();                  
                   //$('.loading-bar').hide();
                   location.href=Host+'profile/dashboard';
                }
            });     
        }
    }); 
    
    $('input[name="user_preference"]').click(function(){
        $('.usr_form_display').show();
       /* if(this.value=='email'){            
            $('.usr_form_phone_display').hide();
            $('.usr_form_phone_display').attr('required',false);
        }else{
            $('.usr_form_phone_display').show();
            $('.usr_form_phone_display').attr('required',true);
        }*/
    });
});

function showTypeDisplay(val,obj,optId)
{
    var typ=$('option:selected', obj).attr("tp");       
    if(typ=='other'){       
        $('#serviceOtherText_'+val).show();
        $('#otherOptionId_'+val).val(optId);        
        $('#datetimehoursdisplay_'+val).hide();
        $('#date_input_append'+val).attr('disabled',true);
        $('#time_input_append'+val).attr('disabled',true);
        $('#hours_input_append'+val).attr('disabled',true);
        $('#optionType_'+val).val('other');
    }else if(typ=='datetime_hours'){
        $('#datetimehoursdisplay_'+val).show();
        $('#date_input_append'+val).attr('name','date_input_'+optId);
        $('#time_input_append'+val).attr('name','time_input_'+optId);
        $('#hours_input_append'+val).attr('name','hours_input_'+optId);
        $('#date_input_append'+val).attr('disabled',false);
        $('#time_input_append'+val).attr('disabled',false);
        $('#hours_input_append'+val).attr('disabled',false);
        $('#serviceOtherText_'+val).hide();
        $('#otherOptionId_'+val).val(''); 
        $('#optionType_'+val).val('datetime_hours');
    }else{
        $('#datetimehoursdisplay_'+val).hide();
        $('#serviceOtherText_'+val).hide();
        $('#date_input_append'+val).attr('disabled',true);
        $('#time_input_append'+val).attr('disabled',true);
        $('#hours_input_append'+val).attr('disabled',true);
        $('#otherOptionId_'+val).val(''); 
        $('#optionType_'+val).val('');
    }  
}

function displayMilesDiv(checked,val,tp,optId){
    if(checked==true){
       if(tp=='miles'){
        $('#milesdisplay_'+val).show();
        $('#optionType_'+val).val('miles');
        $('#otherOptionId_'+val).val(optId);
       }
    }else{
        $('#milesdisplay_'+val).hide();
        $('#optionType_'+val).val('');        
    }
}

function addoptionId(check,optIdd,qtId){
    if(check==true){
        $('#otherOptionId_'+qtId).val(optIdd);        
    }else{
        $('#otherOptionId_'+qtId).val('');
    }
}

function ajaxcity(cid){
    
    $.post('<?=$this->config->config['base_url']?>pros/getCityList', { id:cid }, function(data) {
        $('#cityList').html(data);
        if(cid==''){
            $('#districtList').html(data);
        }
    });   
}

function ajaxdistrict(cid){
    
    $.post('<?=$this->config->config['base_url']?>pros/getDistrictList', { id:cid }, function(data) {
        $('#districtList').html(data);
    });   
}
</script>
</body>
</html>

