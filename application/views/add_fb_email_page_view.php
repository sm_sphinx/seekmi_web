<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->

<head>

<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$page_title?> -  Seekmi</title>

<style type="text/css">.
.glorious-header {
    background: none!important;
    border-bottom: none!important;
}

.piede {
    background: none repeat scroll 0 0 #ffffff;
    border-top: 1px solid #e8e8e8;
    color: #000000;
    margin-top: 0;
    padding: 5px 0!important;
}
#language{
     margin-top: 9px;
   }
</style>
   <link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
 <link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/signup1.css">
 <style type="text/css">
    .help-block {
    color: #d51818;
    display: block;
    margin-bottom: 10px;
    margin-top: 5px;
    font-size: 11.2px;
   }   
   .has-error .checkbox, .has-error .checkbox-inline, .has-error label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label {
    color: #d51818;
   }
   .has-error input,.has-error select,.has-error textarea{
      border-color: #a94442;
      box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset; 
   }
 </style>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<?php include('common_view.php'); ?>
<?php include('header_view.php'); ?>

<div class="wrapper content">
    <div>
   <br/><br/>
            <div id="main_div_1" style="display:block">
              <form novalidate class="box" name="fbemailform" id="fbemailform" method="post" action="<?=$this->config->config['base_url']?>user/addfbuser">
                <div class="box-header">
                 <br/><br/><br/>
                 <center> <h2><?php echo ADD_FB_EMAIL_TEXT; ?></h2></center>                
                </div>
                <div class="box-content">
                    <fieldset>
                        <div class="dynamic-row">
                            <div class="form-field-name-segment form-field form-field" field="">
                                <div>
                                <label for="usr_email"><?php echo REGISTER_EMAILADD; ?></label>
                                <input type="text" name="usr_email" id="usr_email" value="">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                 </div>
                  <input type="hidden" name="fbname" value="<?php echo $fullname; ?>"/>
                  <input type="hidden" name="fbid" value="<?php echo $profile_id; ?>"/>
                 <div class="form-field form-field-nav box-footer">
                    <button class="bttn" type="submit"><?php echo LOGIN_SIGNUP; ?></button>
                 </div>
              </form>
        </div>
<!--   <ul class="reasons">
            <li class="clients">
                <h4>Find new customers and grow your business</h4>
            </li>
            <li class="control-requests">
                <h4>Control the requests you receive</h4>
            </li>
            <li class="leads">
                <h4>Free to sign up, pay as you go</h4>
            </li>
        </ul>-->
    </div>
</div>
<?php include('footer_mini_view.php'); ?>      
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script src="<?=$this->config->config['base_url']?>js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.validate.js"></script>
<script type="text/javascript">
(function(){
      var del = 200;
      $('.icontent').hide().prev('a').hover(function(){
        $(this).next('.icontent').stop('fx', true).slideToggle(del);
      });
    })();	
var form = $("#fbemailform");
var validator = form.validate({
    errorElement: 'span',
    errorClass: 'help-block',                    
    highlight: function(element, errorClass, validClass) {
            $(element).closest('.form-field').addClass("has-error");
    },
    unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-field').removeClass("has-error");
    },
    rules: {
         usr_email:{
            required: true,
            email:true,
            remote: "<?=$this->config->config['base_url']?>user/check_email_exists"
         }
    },
    messages:{              
        usr_email: {                               
             remote: "<br/><h4>Sorry, that email is already registered with us.<br/><br/>Please <a href='<?=$this->config->config['base_url']?>user/login'>CLICK HERE</a> to sign in with this email address.<br/><br/>If you forgot your password, please <a href='<?=$this->config->config['base_url']?>user/reset_password'>CLICK HERE</a> to reset your password.</h4>"
        }
    }
});
$('#usr_email').blur(function() {
    $('#usr_email').val($.trim($('#usr_email').val()));
    validator.element('#usr_email');
});
</script>    

</body>
</html>
      
      
      
