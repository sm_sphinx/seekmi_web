<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" href="<?= $this->config->config['base_url'] ?>css/jquery-ui.css">
<?php include('before_head_view.php'); ?>
<style>
    .ui-autocomplete-loading {
        background: white url("<?= $this->config->config['base_url'] ?>css/images/ui-anim_basic_16x16.gif") right center no-repeat;
    }
</style>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

<div class="wrapper content">    
    <div class="page-title">
        <h1><?php echo DASH_SEARCH_SERVICES; ?></h1>        
    </div>
    <div class="form-field" style="margin-top:8px;">
       <form id="homeSearchFormMid" method="post" name="homeSearchFormMid" action="#">
         <label class=" singnTxt" for="usr_email"><?php echo SERVICE_PART; ?></label>
         <input type="text" name="servicesmid" id="servicesmid" placeholder="Air Conditioner, Interior Designer, Cleaning" autocomplete="off" class="query error" tabindex="2">
         <input type="hidden" id="searchKeywordHid" value=""/>
          <button tabindex="103" id="show" class="bttn blue submit-bttn" type="submit" style="margin-top:8px;"><?php echo GET_FREE_QUOTE; ?></button>
       </form>
    </div>
    
</div>                                        
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?= $this->config->config['base_url'] ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
$(function () {
    function split(val) {
        return val.split(/,\s*/);
    }
    
    function extractLast(term) {
        return split(term).pop();
    }

    $("#servicesmid")
            // don't navigate away from the field on tab when selecting an item
            .bind("keydown", function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).autocomplete("instance").menu.active) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function (request, response) {
                    $.getJSON("<?= $this->config->config['base_url'] ?>home/get_services", {
                        search: extractLast(request.term)
                    }, response);
                },
                search: function () {
                    // custom minLength
                    var term = extractLast(this.value);
                    if (term.length < 2) {
                        return false;
                    }
                },
                focus: function () {
                    // prevent value inserted on focus
                    return false;
                },
                select: function (event, ui) {
                    this.value = ui.item.value
                    $("#servicesmid").removeClass("ui-autocomplete-loading");
                    $('#searchKeywordHid').val(this.value);
                    return false;
                }      
    });
    
    $("#homeSearchFormMid").validate({
        rules: {
            servicesmid: {
                required: true
            }
        },
        submitHandler: function (form) {
            location.href = encodeURI(Host + 'services/search/' + $('#servicesmid').val());
        }
    });
    
    $("#servicesmid").blur(function(){
         if($('#searchKeywordHid').val()==''){
             $("#servicesmid").val('');
             $("#servicesmid").removeClass("ui-autocomplete-loading");
         }
    });
});
</script>
</body>
</html>

