<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<meta content="<?=$this->config->config['base_url']?>" property="og:url"/>
<meta content="Help me get feedback - Review me at Seekmi!" property="og:title"/>
<meta content="Seekmi" property="og:site_name"/>
<meta property="og:description" content="Help me get feedback - Review me at Seekmi!" />
<meta property="og:image" content="<?=$this->config->config['base_url']?>images/LogoSymbolWordSquare.png"/>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/message.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/work.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/review.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/jh-modal.css">
<link rel="stylesheet" href=".<?=$this->config->config['base_url']?>css/lib/jquery.raty.css">
<style type="text/css">
.hint {
  text-align: center;
  width: 160px
}

div.hint {
  font-size: 1.4em;
  height: 46px;
  margin-top: 15px;
  padding: 7px
}
.review-form-row label.error{
    clear: both;
    color: #d51818;
    display: block;
    font-size: 11.2px;
    margin: 5px 0 -2px 5px;
}
</style>
<?php include('before_head_view.php'); ?>
</head>
<body class="write-review box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

<div class="wrapper">
    <div class="content">
        <div class="row">
            <h1 class="body-text"><?php echo ADDREVIEW_LEAVEREVIEW_TEXT." ".$user_data->firstname.$user_data->lastname;?></h1>

            <div class="column-12">
               <div class="reviewee-profile reviewee-profile-with-picture">
                    <div class="reviewee-profile-picture">
                        <a href="<?=$this->config->config['base_url']?>profile/services/view/<?=$user_data->userId?>" target="_blank">
                                <?php if($user_data->userPhoto==''){ ?>
                                    <img src="<?=$this->config->config['base_url']?>images/100x100.png">
                                <?php }else if($user_data->facebookProfileId!=''){ ?>
                                    <img src="<?=$user_data->userPhoto?>">
                                <?php }else{ ?>
                                    <img src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$user_data->userPhoto?>">
                                <?php } ?> 
                          </a>
                    </div>
                    <div class="reviewee-profile-names">
                        <div class="reviewee-profile-primary-name">
                            <h1><?=$user_data->firstname?> <?=$user_data->lastname?></h1>
                              <a href="<?=$this->config->config['base_url']?>profile/services/view/<?=$user_data->userId?>" title="Bright Lights Incorporated" class="reviewee-profile-expand-link" target="_blank">
                              <?php echo ADDREVIEW_VIEWPROFILE_TEXT;?></a>
                        </div>
                       <h2 class="reviewee-profile-secondary-name">
                          <img src="<?=$this->config->config['base_url']?>images/icon-suitcase-16x16.png" alt="Business name:">
                          <?=$business_info->businessName?>
                       </h2>
                    </div>
                </div>
                
                <div class="alert-closable alert-hide review-success">            
                    <div class="alert-success">
                        <p>
                             <?php echo ADDREVIEW_THANKYOU_TEXT." ".$user_data->firstname.$user_data->lastname;?>
                        </p>                        
                    </div>
                </div>
                
                <div class="alert-closable alert-hide review-already">            
                    <div class="alert-error">                        
                            <?php echo ADDREVIEW_ALLREADYREVIEW_TEXT;?>                                             
                    </div>
                </div>
                
                <form id="write-review" name="write-review" method="post" action="#" accept-charset="ISO-8859-1" data-action="review/create"><div class="form-error" style="display: none;"></div>
                    <input id="usr" name="usr" value="<?=$user_data->userId?>" type="hidden">                                      
                    <div class="review-form-block">
                        <div class="review-form-rating">
                        <span class="review-form-rating-label"><?php echo ADDREVIEW_YOURRATING_TEXT;?>:</span>                            
                        <div id="targetKeep" class="star-input-container review-form-row"></div>
                          <div style="display:block;" class="rating-tip-container score-description" id="targetKeep-hint" name="targetKeep_hint">
                             <div class="rating-tip-arrow"></div>
                             <p class="rating-tip"><?php echo ADDREVIEW_CLICKTORATE_TEXT;?></p>
                             <div class="rating-tip-arrow-back"></div>                                
                          </div>                                                       
                        </div>
                        <div class="review-form-body">
                                <div class="user-text-length-indicator user-text-length-empty">
                                    <div class="user-text-length-indicator-bar">
                                    <div style="width: 0%;" class="user-text-length-indicator-bar-contents"></div>
                                    </div>
                                </div>
                                <p class="form-header">
                                    <?php echo ADDREVIEW_YOURRATE_TEXT;?>
                                </p>
                                <div class="review-form-row" data-field="text">
                                    <textarea id="revMsg" name="revMsg" tabindex="105" placeholder="Describe your experience working with <?=$user_data->firstname?> <?=$user_data->lastname?>. 
    Please be specific."></textarea>
                                    <div style="display: none;" class="tooltip tooltip-error tooltip-wide tooltip-right">
                                        <div class="tooltip-content tooltip-content-error"></div>                                        
                                    </div>                                    
                                </div>
                                <button type="submit" class="bttn post-review-bttn blue">
                                    <?php echo ADDREVIEW_POSTREVIEW_TEXT;?>
                                </button>
                            </div>
                      </div>
                  </form>
            </div>

            <div class="column-11 push-1 body-text reviewer-tips">
                <div class="fraud-notice inactive">
                    <h3><?php echo ADDREVIEW_HI_TEXT;?>, <?=$user_data->firstname?>.</h3>
                    <p>
                       <?php echo ADDREVIEW_THISIS_TEXT;?>
                    </p>
                    <p>
                        <a href="#" data-action="dismiss notice" class="never-visited"><?php echo BUTTON_HIDENOTICE;?></a>
                    </p>
                </div>

                <div <?php if($check_review > 0){ ?>style="display: block;"<?php }else{ ?> style="display:none;"<?php } ?> class="already-reviewed fraud-notice inactive">
                    <h3><?php echo ADDREVIEW_THANKS_TEXT;?></h3>
                    <p>
                         <?php echo ADDREVIEW_ITLOOKS_TEXT." ".$user_data->firstname.$user_data->lastname.". ".ADDREVIEW_THANKHELP_TEXT; ?>
                        
                    </p>
                    <p>
                        <a href="#" class="never-visited hide-notice"><?php echo BUTTON_HIDENOTICE;?></a>
                    </p>
                </div>

                <h2><?php echo ADDREVIEW_LETPEOPLE_TEXT;?></h2>

                <h3><?php echo ADDREVIEW_DESCRIBE_TEXT;?></h3>
                <p>
                     <?php echo ADDREVIEW_WHATDID_TEXT." ".$user_data->firstname.$user_data->lastname." ".ADDREVIEW_HELPYOU_TEXT;?> 
                </p>

                <h3><?php echo ADDREVIEW_SAY_TEXT;?></h3>
                <p>
                    <?php echo ADDREVIEW_WHATIMPRE_TEXT." ".$user_data->firstname.$user_data->lastname." ".ADDREVIEW_GOABOVE_TEXT; 
                    echo $user_data->firstname.$user_data->lastname.ADDREVIEW_ONTIME_TEXT;?> 

                <h3><?php echo ADDREVIEW_POINT_TEXT;?></h3>
                <p>
                    <?php echo ADDREVIEW_WHATWOULD_TEXT." ".$user_data->firstname.$user_data->lastname." ".ADDREVIEW_DOBETTER_TEXT;?>
                </p>
            </div>
        </div>
    </div>
</div>                                        

<? include('footer_mini_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script src="<?=$this->config->config['base_url']?>css/lib/jquery.raty.js"></script>
<script type="text/javascript">
$(document).ready(function () {
     $("#write-review").validate({
        ignore: [],
        rules: {
            score: {
                required: true,
            },
            revMsg: {
                required: true
            }
        },
        submitHandler: function(form) {
            $.post(Host+'bid/submit_review', $("#write-review").serialize(), function(data) {
               if(data=='already'){
                   $('.review-success').hide();
                   $('.already-reviewed').show();
                   $('.review-already').show();
               }else{
                   $('#revMsg').val('');
                   $('.review-success').show();
                   $('.already-reviewed').hide();
                   $('.review-already').hide();
               }
            });     
        }
    });   
    
    $('#targetKeep').raty({
        cancel     : false,
        target     : '#targetKeep-hint',
        targetKeep : true
    });
    
    $('.hide-notice').click(function(){
        $('.already-reviewed').slideUp();
    });
});
</script>
</body>
</html>

