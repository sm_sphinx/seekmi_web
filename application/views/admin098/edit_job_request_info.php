<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/jquery.timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/bootstrap-datepicker.css" />

<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <?php
    include('header_view.php');
    include($_SERVER["DOCUMENT_ROOT"] . '/config/eng_constants.php');
    ?>      
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
<?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content" id="mainDiv">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid" >
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END BEGIN STYLE CUSTOMIZER --> 
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
<?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">							
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= $this->config->config['admin_base_url'] ?>user/users">Manage Users</a> 
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="#"><?= $page_title ?></a></li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid profile">
                    <div class="span12">
                        <!--BEGIN TABS-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-reorder"></i><?= $page_title ?></div>								
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                    <form class="form-horizontal package-form" name="editrequest-form" id="editrequest-form" method="post" action="">
                                    <?php
//                                        print_r($answers);
                                    $icon_arr = array('informatic' => 'icon-font-info', 'details' => 'icon-font-pencil', 'time' => 'icon-font-clock', 'travel' => 'icon-font-truck', 'zipcode' => 'icon-font-location');
                                    $questId = array();
                                    foreach ($question_list as $key => $value) {
                                        $cnt = 0;
                                        ?>
                                        <fieldset class="fieldset-questions">
                                            <?php
                                            foreach ($value as $question_row) {
//                                                print_r($answers);
//                                                echo $question_row->questionId;
                                                //print_r($question_row);
                                                $type = $question_row->type;
                                                if ($question_row->$type == 'Y') {
                                                    if ($cnt == 0) {
                                                        ?>
                                                        <?php if ($question_row->fieldType != 'address') { ?>
                                                            <legend class="<?php echo $icon_arr[$key]; ?>">&nbsp;</legend>
                                                            <?php
                                                        }
                                                    }
                                                    $questId[] = $question_row->questionId;
                                                    ?>
                                                    <div class="form-field">
                                                        <?php if ($question_row->fieldType != 'address') { ?>
                                                            <label for="labeled-inputs-182"><?php echo $question_row->questionName; ?></label>
                                                        <?php } ?>
                                                            <?php if (($question_row->fieldType == 'checkbox' && $key != 'travel') || $question_row->fieldType == 'radio') { ?>
                                                            <div class="checkbox-section">
            <?php }if ($question_row->fieldType == 'select') { ?>  
                                                                <div class="describable-select">
                                                                    <select name="question_<?= $question_row->questionId ?>" required="" onchange="showTypeDisplay('<?= $question_row->questionId ?>', this, this.value);">
                                                                        <option value="">- Select one -</option>
                                                                    <?php } if ($question_row->fieldType == 'textarea') { ?>
                                                                        <textarea name="question_<?= $question_row->questionId ?>" required=""><?php echo $answers[$question_row->questionId]['optionVal'];?></textarea>
                                                                    <?php } if ($question_row->fieldType == 'textbox') {
                                                                        ?>                           
                                                                        <input type="text" name="question_<?= $question_row->questionId ?>" required="" value="<?php echo $answers[$question_row->questionId]['optionVal'];?>"/>
                                                                    <?php } if ($question_row->fieldType == 'address') { ?>     

                                                                        <?php
                                                                    }
                                                                    if (!empty($option_list[$question_row->questionId])) {
                                                                        foreach ($option_list[$question_row->questionId] as $option_row) {
                                                                            if ($question_row->fieldType == 'checkbox') {
                                                                                ?>                       
                                                                                <div role="presentation">
                                                                                    <label role="option" class="select-option" aria-selected="false">
                        <?php if ($option_row->optionType == 'other') { ?>
                                                                                            <input required="" type="checkbox" class="radio" name="question_<?php echo $question_row->questionId; ?>[]" value="<?php echo $option_row->optionId; ?>" <?php if($answers[$question_row->questionId]['optionId'] === $option_row->optionId){echo 'checked';}?> onclick="addoptionId(this.checked, '<?php echo $option_row->optionId; ?>', '<?php echo $question_row->questionId; ?>');">
                                                                                            <input type="text" name="serviceOtherText_<?php echo $question_row->questionId; ?>" tabindex="0" placeholder="Other">
                                                                                        <?php } else { ?>
                                                                                            <input required="" type="checkbox" class="radio" name="question_<?php echo $question_row->questionId; ?>[]" value="<?php echo $option_row->optionId; ?>" <?php if($answers[$question_row->questionId]['optionId'] === $option_row->optionId){echo 'checked';}?> onclick="displayMilesDiv(this.checked, '<?php echo $question_row->questionId; ?>', '<?php echo $option_row->optionType; ?>', '<?php echo $option_row->optionId; ?>')">&nbsp;<?php echo $option_row->optionText; ?>
                        <?php } ?>  
                                                                                    </label>
                                                                                </div>
                        <?php if ($option_row->optionType == 'miles') { ?>
                                                                                    <div style="<?php if($answers[$question_row->questionId]['optionType']=='miles'){echo 'display:block';}else{ echo 'display:none'; }?>" id="milesdisplay_<?php echo $question_row->questionId; ?>" class="form-field">
                                                                                        <label>I will travel up to</label>
                                                                                        <select name="miles_input_<?php echo $option_row->optionId; ?>" id="miles_input_<?php echo $question_row->questionId; ?>">
                                                                                            <option value="5">5 kilometer</option>
                                                                                            <option value="15">15 kilometer</option>
                                                                                            <option value="25">25 kilometer</option>
                                                                                            <option value="50">50 kilometer</option>
                                                                                        </select>
                                                                                        <div class="subtext-form"></div>                                 
                                                                                    </div>
                                                                                <?php } ?>
                                                                            <?php } if ($question_row->fieldType == 'select') { ?>                          
                                                                                <option value="<?php echo $option_row->optionId; ?>" tp="<?php echo $option_row->optionType; ?>" <?php if($answers[$question_row->questionId]['optionId'] === $option_row->optionId){echo 'selected';};?>><?php echo $option_row->optionText; ?></option>                      
                    <?php } if ($question_row->fieldType == 'radio') { ?>
                                                                                <div role="presentation">
                                                                                    <label role="option" class="select-option" aria-selected="false">
                                                                                        <input type="radio" class="radio" name="question_<?php echo $question_row->questionId; ?>" value="<?php echo $option_row->optionId; ?>" <?php if($answers[$question_row->questionId]['optionId'] === $option_row->optionId){echo 'checked';};?>><?php echo $option_row->optionText; ?>
                                                                                    </label>
                                                                                </div>
                                                                            <?php } ?>

                                                                        <?php }
                                                                    }
                                                                    ?>
                                                            <?php if ($question_row->fieldType == 'select' || ($question_row->fieldType == 'checkbox' && $key != 'travel') || $question_row->fieldType == 'radio') { ?>
                                                                </div>
                                                            <?php } if ($question_row->fieldType == 'select') { ?>
                                                                </select>
                                                                <input type="text" id="serviceOtherText_<?php echo $question_row->questionId; ?>" name="serviceOtherText_<?php echo $question_row->questionId; ?>" placeholder="Other" style="display:none;">
            <?php } ?>                    
                                                        </div>
                                                        <input type="hidden" id="otherOptionId_<?php echo $question_row->questionId; ?>" name="otherOptionId_<?php echo $question_row->questionId; ?>" value=""/>
                                                        <input type="hidden" name="qustType_<?php echo $question_row->questionId; ?>" value="<?php echo $question_row->fieldType; ?>"/>
                                                        <input type="hidden" name="optionType_<?php echo $question_row->questionId; ?>" id="optionType_<?php echo $question_row->questionId; ?>" value="<?php echo $answers[$question_row->questionId]['optionType'];?>"/>
            <?php if ($question_row->fieldType == 'select') { ?>
                                                            <div class="subtext-form">&nbsp;</div>
                                                            <div id="datetimehoursdisplay_<?php echo $question_row->questionId; ?>" style="<?php if($answers[$question_row->questionId]['optionType']=='datetime_hours'){echo 'display:block';}else{ echo 'display:none'; }?>" class="form-field">
                                                                <label>What day do you need the DJ?</label>
                                                                <div class="date-picker-container">

                                                                    <?php 
                                                                    $date = '';
                                                                    $time = '';
                                                                    $hours = '';
                                                                    if($answers[$question_row->questionId]['optionType']=='datetime_hours'){
                                                                         if($answers[$question_row->questionId]['extras']){
                                                                             $extras = json_decode($answers[$question_row->questionId]['extras']);
                                                                            
                                                                             $date = $extras->date;
                                                                             $time = $extras->time;
                                                                             $hours = $extras->hours;
                                                                         }
                                                                        
                                                                    }
                                                                    ?>
                                                                    <div class="form-field form-field-date">
                                                                        <label class="input-prepend">
                                                                            <span class="add-on icon-font-calendar"></span>
                                                                            <input type="text" value="<?php echo $date;?>" class="datepicker" name="date_input_<?php echo $question_row->questionId; ?>" id="date_input_append<?php echo $question_row->questionId; ?>" readonly="" >
                                                                        </label>
                                                                    </div>

                                                                    <div class="form-field form-field-column-6">
                                                                        <label>At what time?</label>
                                                                        <input type="text" value="<?php echo $time;?>" class="timepicker" name="time_input_<?php echo $question_row->questionId; ?>" id="time_input_append<?php echo $question_row->questionId; ?>" >
                                                                    </div>

                                                                    <div class="form-field form-field-length form-field-column-5">
                                                                        <label>For how long?</label>
                                                                        <div class="input-append">
                                                                            <input type="text" value="<?php echo $hours;?>" name="hours_input_<?php echo $question_row->questionId; ?>" id="hours_input_append<?php echo $question_row->questionId; ?>" >
                                                                            <span class="add-on"> hours</span>
                                                                        </div>
                                                                    </div>
                                                                </div>                       
                                                            </div>
                                                        <?php } ?> 


                                                        <?php
                                                    }
                                                    $cnt++;
                                                }
                                                ?>                    
                                        </fieldset>
                                        <?php
                                    }
                                    if (!empty($questId)) {
                                        $questStr = implode(',', $questId);
                                    } else {
                                        $questStr = '';
                                    }
                                    ?>

                                    <fieldset class="fieldset-questions">

                                        <legend class="icon-font-location">&nbsp;</legend>
                                        <div class="form-field-city form-field form-field" field="" style="width:100%;">
                                            <div>
                                                <label for="province"><?php echo EDITPRO_PROVINCE_TEXT; ?></label>
                                                <select name="province" id="province" onchange="ajaxcity(this.value)" required="">
                                                    <option value=""><?php echo EDITPRO_SELECT_TEXT; ?></option>
                                                    <?php
                                                    if ($province_list != '') {
                                                        foreach ($province_list as $province_row) {
                                                            $sel = '';
                                                            if($province_row->provId === $servicesinfo->provinceId){
                                                                $sel = "selected=true";
                                                            }
                                                            echo '<option value="' . $province_row->provId . '" '.$sel.'>' . $province_row->provName . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>                        
                                        <div class="form-field-city form-field form-field" expr="address.city" field="" style="width:100%;">
                                            <div>
                                                <label for="city"><?php echo EDITPRO_CITY_TEXT; ?></label> 
                                                <select name="city" id="cityList" onchange="ajaxdistrict(this.value)" required="">
                                                    <option value=""><?php echo EDITPRO_SELECT_TEXT; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-field-state form-field form-field" field="" style="width:100%;" >
                                            <div>
                                                <label for="district"><?php echo EDITPRO_DIST_TEXT; ?></label>
                                                <select name="district" id="districtList" required="">
                                                    <option value=""><?php echo EDITPRO_SELECT_TEXT; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                         <div class="form-field-state form-field form-field" field="" style="width:100%;" >
                                            <div>
                                                <label for="district">Admin comments</label>
                                                <textarea id="adminComment" name="adminComment"><?php echo $servicesinfo->adminComment;?></textarea>
                                            </div>
                                        </div>
                                        
                                    </fieldset>

                                    <input type="hidden" name="questionIds" value="<?php echo $questStr; ?>"/>

                                    <input type="hidden" name="serviceid" value="<?php echo $serviceId; ?>"/>                  
                                    <fieldset class="fieldset-submit">
                                        <div class="form-field">
                                            
                                            <input type="hidden" name="projectId" id="projectId" value="<?php echo $this->uri->segment(4)?>"/>

                                            <div class="form-actions">
                                                <button type="submit" id="register-submit-btn" class="btn green ">
                                                    Save                                   
                                                </button>
                                                <a href="<?= $this->config->config['admin_base_url'] ?>service/jobs" class="btn">Cancel</a>            
                                            </div>
                                        </div>

                                    </fieldset>                   
                                </form>
                                <!-- END FORM-->  
                            </div>
                        </div>
                        <!--END TABS-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER--> 
        </div>
        <!-- END PAGE -->    
    </div>
    <!-- END CONTAINER -->    
<?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>  
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
    <script src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>    
    <script src="<?= base_url() ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
    
    
    <script src="<?=$this->config->config['base_url']?>js/jquery.timepicker.js"></script>
<script src="<?=$this->config->config['base_url']?>js/bootstrap-datepicker.js"></script>


    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?= base_url() ?>assets/scripts/app.js"></script> 
    <!--<script src="<?= base_url() ?>assets/scripts/editjobScript.js" type="text/javascript"></script>--> 
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
                                                    jQuery(document).ready(function() {
                                                        // initiate layout and plugins
                                                        App.init();
//                                                        Editrequest.init();
                                                        
                                                        
                                                        ajaxcity('<?php echo $servicesinfo->provinceId;?>');
                                                        ajaxdistrict('<?php echo $servicesinfo->cityId;?>');
                                                        
                                                    });

                                                    function ajaxcity(cid) {

                                                        $.post('<?= $this->config->config['base_url'] ?>pros/getCityList', {id: cid, selectedId:'<?php echo $servicesinfo->cityId;?>'}, function(data) {
                                                            $('#cityList').html(data);
                                                            if (cid == '') {
                                                                $('#districtList').html(data);
                                                            }
                                                        });
                                                    }

                                                    function ajaxdistrict(cid) {

                                                        $.post('<?= $this->config->config['base_url'] ?>pros/getDistrictList', {id: cid, selectedId:'<?php echo $servicesinfo->districtId;?>'}, function(data) {
                                                            $('#districtList').html(data);
                                                        });
                                                    }
                                                    
                                                    function displayMilesDiv(checked,val,tp,optId){
    if(checked==true){
       if(tp=='miles'){
        $('#milesdisplay_'+val).show();
        $('#optionType_'+val).val('miles');
        $('#otherOptionId_'+val).val(optId);
       }
    }else{
        $('#milesdisplay_'+val).hide();
        $('#optionType_'+val).val('');        
    }
}

$(document).ready(function () {
    
     var urlPass=HostAdmin + "service/submit_request";
     var urlRedirect=HostAdmin + "service/jobs";
        
    $("#editrequest-form").validate({        
        errorPlacement: function (error, element) { error.insertAfter($(element).parents('div').prev('label'));  },        
        submitHandler: function(form) {
            $.post(urlPass, $("#editrequest-form").serialize(), function(data) {
               $('body,html').animate({
			scrollTop: 0 ,
		 	}, 700
               );
               $('.loading-bar').show();               
//               alert(data);
                   $('#inactive_error').hide();
                   $('#failed_error').hide();
                   $('#invalid_login_error').hide();
                   //$('.loading-bar').hide();
                   location.href=urlRedirect;
            });     
        }
    }); 
    
    $('input[name="user_preference"]').click(function(){
        $('.usr_form_display').show();
        if(this.value=='email'){            
            $('.usr_form_phone_display').hide();
            $('.usr_form_phone_display').attr('required',false);
        }else{
            $('.usr_form_phone_display').show();
            $('.usr_form_phone_display').attr('required',true);
        }
    });
});

function showTypeDisplay(val,obj,optId)
{
    var typ=$('option:selected', obj).attr("tp");       
    if(typ=='other'){       
        $('#serviceOtherText_'+val).show();
        $('#otherOptionId_'+val).val(optId);        
        $('#datetimehoursdisplay_'+val).hide();
        $('#date_input_append'+val).attr('disabled',true);
        $('#time_input_append'+val).attr('disabled',true);
        $('#hours_input_append'+val).attr('disabled',true);
        $('#optionType_'+val).val('other');
    }else if(typ=='datetime_hours'){
        $('#datetimehoursdisplay_'+val).show();
        $('#date_input_append'+val).attr('name','date_input_'+optId);
        $('#time_input_append'+val).attr('name','time_input_'+optId);
        $('#hours_input_append'+val).attr('name','hours_input_'+optId);
        $('#date_input_append'+val).attr('disabled',false);
        $('#time_input_append'+val).attr('disabled',false);
        $('#hours_input_append'+val).attr('disabled',false);
        $('#serviceOtherText_'+val).hide();
        $('#otherOptionId_'+val).val(''); 
        $('#optionType_'+val).val('datetime_hours');
    }else{
        $('#datetimehoursdisplay_'+val).hide();
        $('#serviceOtherText_'+val).hide();
        $('#date_input_append'+val).attr('disabled',true);
        $('#time_input_append'+val).attr('disabled',true);
        $('#hours_input_append'+val).attr('disabled',true);
        $('#otherOptionId_'+val).val(''); 
        $('#optionType_'+val).val('');
    }  
}

function displayMilesDiv(checked,val,tp,optId){
    if(checked==true){
       if(tp=='miles'){
        $('#milesdisplay_'+val).show();
        $('#optionType_'+val).val('miles');
        $('#otherOptionId_'+val).val(optId);
       }
    }else{
        $('#milesdisplay_'+val).hide();
        $('#optionType_'+val).val('');        
    }
}

function addoptionId(check,optIdd,qtId){
    if(check==true){
        $('#otherOptionId_'+qtId).val(optIdd);        
    }else{
        $('#otherOptionId_'+qtId).val('');
    }
}

   var nowDate = new Date();
   var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
   $('.datepicker').datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true,
        'startDate': today 
   });
   
    $('.timepicker').timepicker({
        'showDuration': true,
        'timeFormat': 'g:i A'
    });
    

    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>