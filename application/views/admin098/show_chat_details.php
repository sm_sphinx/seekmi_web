<?php include('style_header.php'); ?>
<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include('header_view.php'); ?>
      
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid" id="mainGridContainer">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
		   <?php include('left_view.php'); ?>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content" id="mainDiv">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid" >
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
                                            <!-- BEGIN STYLE CUSTOMIZER -->
                                            <!-- END BEGIN STYLE CUSTOMIZER --> 
                                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                                            <h3 class="page-title">
                                                    <?=$page_title?>
                                            </h3>
                                            <ul class="breadcrumb">							
                                                <li>
                                                    <i class="icon-home"></i>
                                                    <a href="<?=$this->config->config['admin_base_url']?>service/jobs">Manage Job Requests</a> 
                                                    <i class="icon-angle-right"></i>
                                                </li>
                                                <li><a href="#"><?=$page_title?></a></li>
                                            </ul>
                                            <!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid profile">
            <div class="span12">
                <!--BEGIN TABS-->
                <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?=$page_title?>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <dl class="work-request-data">
                                <dt class="request-title"><?php echo $msg->prosname; ?> : </dt>
                               <dd class="request-info"><?php echo $msg->quoteMessage; ?></dd>
                               <dt class="request-title">Date : </dt>
                               <dd class="request-info"><?php echo $msg->addedOn;?></dd>
                               <hr>
                                <?php foreach ($list as $key => $value) { ?>
                                    <dt class="request-title"><?php echo $value['fromname']; ?> : </dt>
                                     <dd class="request-info"><?php echo $value['message']; ?></dd>
                                     <dt class="request-title">Date : </dt>
                                     <dd class="request-info"><?php echo $value['createdOn']; ?></dd>
                                     <hr>
                               <?php }?>
                            </dl>
                            
                            
                            <div class="form-actions" style="padding-left:10px;">                                   
                                 <a href="<?=$this->config->config['admin_base_url']?>service/view_job/<?php echo $msg->projectId; ?>" class="btn">Cancel</a>            
                            </div>
                        </div>
                </div>
                <!--END TABS-->
              </div>
          </div>
          <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER--> 
</div>
<!-- END PAGE -->    
</div>
<!-- END CONTAINER -->    
<?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>