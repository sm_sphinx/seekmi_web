<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/jquery.tokenize.css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <?php include('header_view.php'); ?>
    <?php
    if (isset($user_info)) {
        $userId = $user_info->userId;
        $firstname = $user_info->firstname;
        $lastname = $user_info->lastname;
        $email = $user_info->email;
        $phone = $user_info->phone;
        $credits = $user_info->credits;
        $status = $user_info->status;
        $services = $user_info->services;
        $catgeory = $user_info->categoryId;
        if ($services != '') {
            $service_arr = explode(',', $services);
        } else {
            $service_arr = array();
        }
    } else {
        $userId = 0;
        $firstname = '';
        $lastname = '';
        $email = '';
        $phone = '';
        $credits = '';
        $status = '';
        $services = '';
        $catgeory = '';
        $service_arr = array();
    }
    if (!empty($user_pr_info)) {
        $businessName = $user_pr_info->businessName;
        $website = $user_pr_info->website;
        $businessDesc = $user_pr_info->bussinessDescription;
        $bio = $user_pr_info->bioInfo;
        $foundingYear = $user_pr_info->foundingYear;
        $employeeCount = $user_pr_info->employeeCount;
    } else {
        $businessName = '';
        $website = '';
        $businessDesc = '';
        $bio = '';
        $foundingYear = '';
        $employeeCount = '';
    }
    ?>        
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
            <?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content" id="mainDiv">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid" >
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END BEGIN STYLE CUSTOMIZER --> 
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            <?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">							
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= $this->config->config['admin_base_url'] ?>pros/lists">Manage Professional</a> 
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="#"><?= $page_title ?></a></li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid profile">
                    <div class="span12">
                        <!--BEGIN TABS-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-reorder"></i><?= $page_title ?></div>								
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal user-form" name="myUserFrm" id="myUserFrm" method="post" action="<?= $this->config->config['admin_base_url'] ?>pros/saveProsInfo">
                                    <!--BEGIN TABS-->
                                    <div class="tabbable tabbable-custom tabbable-full-width">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
                                            <li><a href="#tab_1_2" data-toggle="tab">Provider Info</a></li> 
                                            <li><a href="#tab_1_3" data-toggle="tab">Service Info</a></li> 
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane row-fluid active" id="tab_1_1">
                                                <input type="hidden" name="user_id" id="user_id" value="<?= $userId ?>"/>                               
                                                <div class="alert alert-error hide">
                                                    <button class="close" data-hide="alert" type="button"></button>
                                                    <span>Email is already exists.</span>
                                                </div>
                                                <div class="alert alert-success hide">
                                                    <button class="close" type="button"  data-hide="alert"></button>
                                                    Professional has been created successfully.
                                                </div>                                  
                                                <div class="control-group">
                                                    <label class="control-label">First Name </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <input name='txtFirstname' id='txtFirstname' value="<?= $firstname ?>" class='m-wrap span6' placeholder='First Name'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Last Name </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <input name='txtLastname' id='txtLastname' value="<?= $lastname ?>" class='m-wrap span6' placeholder='Last Name'/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Email<span class="required">*</span> </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <input name='txtEmail' id='txtEmail' value="<?= $email ?>" class='m-wrap span6' placeholder='Email'/>                                        
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Phone Number </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <input name='txtPhone' id='txtPhone' value="<?= $phone ?>" class='m-wrap span6' placeholder='Phone'/>                                        
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php if ($userId != 0) { ?>
                                                    <div class="control-group">
                                                        <label class="control-label">Change Password</label>
                                                        <div class="controls">                                            
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="change_password_check" value="1" id="change_password" onclick="if (this.checked == true) {
                                                                                jQuery('#PasswordDiv,#ConfirmPasswordDiv').show();
                                                                            } else {
                                                                                jQuery('#PasswordDiv,#ConfirmPasswordDiv').hide();
                                                                            }">
                                                            </label>                                            
                                                        </div>
                                                    </div> 
                                                <?php } ?>

                                                <div class="control-group" id="PasswordDiv" <?php if ($userId != 0) { ?>style="display:none;" <?php } ?>>
                                                    <label class="control-label ">Password<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <div class="input-icon">                                                
                                                            <input class="m-wrap span6" type="password" autocomplete="off" id="txtPassword"  placeholder="Password" name="txtPassword"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="control-group" id="ConfirmPasswordDiv" <?php if ($userId != 0) { ?>style="display:none;" <?php } ?>>
                                                    <label class="control-label ">Confirm Password<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <div class="input-icon">                                                
                                                            <input class="m-wrap span6" type="password" autocomplete="off" placeholder="Confirm Password" id="txtCPassword" name="txtCPassword"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Status</label>
                                                    <div class="controls">
                                                        <div class="">                                                                 
                                                            <select name="txtActive" id="txtActive">                                     
                                                                <option value="Y" <?php if ($status == 'Y') { ?> selected<?php } ?>>Active</option> 
                                                                <option value="N" <?php if ($status == 'N') { ?> selected<?php } ?>>Inactive</option> 
                                                                <option value="review" <?php if ($status == 'review') { ?> selected<?php } ?>>Review</option> 
                                                                <option value="suspend" <?php if ($status == 'suspend') { ?> selected<?php } ?>>Suspend</option> 
                                                                <option value="banned" <?php if ($status == 'banned') { ?> selected<?php } ?>>Banned</option> 
                                                            </select>                                             
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <!--end tab-pane-->
                                            <div class="tab-pane row-fluid" id="tab_1_2">                                                
                                                <div class="control-group">
                                                    <label class="control-label">Business Name </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <input name='txtBusinessName' id='txtBusinessName' value="<?= $businessName ?>" class='m-wrap span6' placeholder='Business Name'/>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Website </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <input name='txtWebsite' id='txtWebsite' value="<?= $website ?>" class='m-wrap span6' placeholder='Website'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Business Description </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <textarea name='txtBusinessDesc' id='txtBusinessDesc' class='m-wrap span6'><?= $businessDesc ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Bio </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <textarea name='txtBio' id='txtBio' class='m-wrap span6'><?= $bio ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Year founded </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <input type="number" pattern="/^\d{4}$/" name='txtYearFounded' id='txtYearFounded' value="<?= $foundingYear ?>" class='m-wrap span6' placeholder='Year founded'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Number of employees </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <input type="number" pattern="/^\d{1,4}$/" name='txtEmpCount' id='txtEmpCount' value="<?= $employeeCount ?>" class='m-wrap span6' placeholder='Number of employees'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Credits </label>
                                                    <div class="controls">
                                                        <div class="">
                                                            <input name='txtCredit' id='txtCredit' value="<?= $credits ?>" class='m-wrap span6' placeholder='Credits'/>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                            <div class="tab-pane row-fluid" id="tab_1_3">
                                                <?php /*         <?php if ($services != '') { ?>
                                                  <div class="control-group">
                                                  <label class="control-label">Added services </label>
                                                  <div class="controls">
                                                  <div class="">
                                                  <textarea rows="4" class="span6"><?php echo $services; ?></textarea>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <?php } ?>
                                                 * 
                                                 */ ?>
                                                <div class="box-header">
                                                    <div class="form-field-sms-preference form-field form-field" expr="account.sms" field="">
                                                        <div>
                                                            <label for="services">Add services</label>
                                                            <select name="services[]" id="services" class="tokenize-sample required" multiple="multiple">
                                                                <?php foreach ($u_services_name as $subservice_row) {
                                                                    ?>
                                                                    <option value="<?= $subservice_row ?>"  selected="selected"><?php echo $subservice_row; ?></option> 
                                                                    <?php }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php /* <div class="control-group">
                                                  <label class="control-label">Category </label>
                                                  <div class="controls">
                                                  <div class="">
                                                  <select name="txtCategory" id="txtCategory" class="span6" onchange="getServices(this.value);">
                                                  <option value="">Select Category</option>
                                                  <?php if(!empty($category_list)){
                                                  foreach($category_list as $category_row){ ?>
                                                  <option value="<?=$category_row->catId?>" <?php if($catgeory==$category_row->catId){ ?> selected<?php } ?>><?php echo $category_row->categoryName; ?></option>
                                                  <?php } } ?>
                                                  </select>
                                                  </div>
                                                  </div>
                                                  </div>
                                                  <div class="control-group">
                                                  <label class="control-label">Services </label>
                                                  <div class="controls">
                                                  <select name="txtService[]" id="txtService" multiple="multiple" class="span6" size="8" onclick="getSubServices();">
                                                  <?php if(!empty($service_list)){
                                                  foreach($service_list as $service_row){ ?>
                                                  <option value="<?=$service_row->serviceId?>" <?php if(in_array($service_row->serviceId ,$user_service)){ ?> selected="selected"<?php } ?>><?php echo $service_row->serviceName; ?></option>
                                                  <?php } } ?>
                                                  </select>
                                                  </div>
                                                  </div> */ ?>                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" id="register-submit-btn" class="btn green ">
                                            Save                                    
                                        </button>
                                        <a href="<?= $this->config->config['admin_base_url'] ?>pros/lists" class="btn">Cancel</a>            
                                    </div>
                                </form>
                                <!-- END FORM-->  
                            </div>
                        </div>
                        <!--END TABS-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER--> 
        </div>
        <!-- END PAGE -->    
    </div>
    <!-- END CONTAINER -->    
    <?php include('footer_view.php'); ?>
    <?php include('scripts_footer.php'); ?>	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>  
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
    <script src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>    
    <script src="<?= base_url() ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?= base_url() ?>assets/scripts/app.js"></script> 
    <script src="<?= base_url() ?>assets/scripts/pros.js" type="text/javascript"></script> 
    <script type="text/javascript" src="<?= base_url() ?>js/jquery.tokenize.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
                                                                    jQuery(document).ready(function () {
                                                                        // initiate layout and plugins
                                                                        App.init();
                                                                        Pros.init();
                                                                    });

                                                                    function getServices(catId) {
                                                                        if (catId != '') {
                                                                            var pageContent = jQuery('#mainGridContainer');
                                                                            App.blockUI(pageContent, true);
                                                                            jQuery.ajax
                                                                                    ({
                                                                                        type: "POST",
                                                                                        url: '<?= $this->config->config['admin_base_url'] ?>service/getServices',
                                                                                        data: "catId=" + catId,
                                                                                        success: function (msg)
                                                                                        {
                                                                                            App.unblockUI(pageContent);
                                                                                            jQuery('#txtService').html(msg);
                                                                                        }
                                                                                    });
                                                                        }
                                                                    }
                                                                    function getSubServices()
                                                                    {
                                                                        var pageContent = jQuery('#mainGridContainer');
                                                                        App.blockUI(pageContent, true);
                                                                        jQuery.ajax
                                                                                ({
                                                                                    type: "POST",
                                                                                    url: '<?= $this->config->config['admin_base_url'] ?>service/getSubServices',
                                                                                    data: "servId=" + jQuery('#txtService').val(),
                                                                                    success: function (msg)
                                                                                    {
                                                                                        App.unblockUI(pageContent);
                                                                                        jQuery('#txtSubService').html(msg);
                                                                                    }
                                                                                });
                                                                    }
                                                                    $('#services').tokenize({
                                                                        datas: "<?= base_url() ?>home/get_services_pros"
                                                                    });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>