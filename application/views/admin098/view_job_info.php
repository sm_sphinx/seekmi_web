<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url()?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?=base_url()?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include('header_view.php'); ?>
      
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid" id="mainGridContainer">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
		   <?php include('left_view.php'); ?>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content" id="mainDiv">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid" >
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
                                            <!-- BEGIN STYLE CUSTOMIZER -->
                                            <!-- END BEGIN STYLE CUSTOMIZER --> 
                                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                                            <h3 class="page-title">
                                                    <?=$page_title?>
                                            </h3>
                                            <ul class="breadcrumb">							
                                                <li>
                                                    <i class="icon-home"></i>
                                                    <a href="<?=$this->config->config['admin_base_url']?>service/jobs">Manage Job Requests</a> 
                                                    <i class="icon-angle-right"></i>
                                                </li>
                                                <li><a href="#"><?=$page_title?></a></li>
                                            </ul>
                                            <!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid profile">
            <div class="span12">
                <!--BEGIN TABS-->
                <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?=$page_title?>
                            </div>
                            <div class="actions">
                                    <a class="btn btn-info green" href="<?= $this->config->config['admin_base_url'] ?>service/editJobRequest/<?= $this->uri->segment(4) ?>"><i class="icon-edit icon-white"></i> Edit</a>
                                </div>
                        </div>
                        <div class="portlet-body form">
                            <dl class="work-request-data">
                              <?php
                              if(!empty($project_owner_data)){ ?>
                                <h5><b>Name :</b> <?php echo $project_owner_data->firstname.' '.$project_owner_data->lastname ?></h5>
                                <h5><b>Email : </b><?php echo $project_owner_data->email ?></h5>
                                <h5><b>Service : </b><?php echo $project_owner_data->subserviceName ?></h5>
                                <h5><b>Phone number : </b><?php echo $project_owner_data->phone ?></h5>
                                <h5><b>Full address : </b><?php echo $address; ?></h5>
                                <hr>
                                
                                
                             <?php }
                              if(!empty($req_data)){ 
                                 foreach($req_data as $req_row){                    
                              ?> 
                               <dt class="request-title"><?=$req_row->questionName?></dt>
                               <dd class="request-info">
                               <?php if(!empty($req_option_data[$req_row->questionId])){ ?>
                                  <ul>
                                 <?php foreach($req_option_data[$req_row->questionId] as $req_option_row){ ?>
                                   <li class="list-bullet">
                                      <?php if(count($req_option_data[$req_row->questionId]) > 1){ ?>
                                   <div class="check"></div>
                                      <?php } ?>
                                    <?=$req_option_row['optionText']?>
                                    <?php if($req_option_row['optionExtra']){
                                         echo " (".$req_option_row['optionExtra'].")";
                                     }
                                     ?>
                                   </li>
                                   <?php }?>
                                   </ul>
                                  <?php } ?>
                                </dd>
                              <?php } ?>
                                <dt class="request-title">Request submitted</dt>
                                <dd class="request-info">
                                    At <?php echo date('h:ia',strtotime($project_owner_data->createdDate)); ?>
                                    on
                                    <?php echo date('m-d-y',strtotime($project_owner_data->createdDate)); ?>                            
                                </dd>
                              <?php } ?>  
                                
                                <dt class="request-title">Admin Comments : </dt>
                                <dd class="request-info"><?php echo $project_owner_data->adminComment ?></dd>
                            </dl>
                            
                            
                            <div class="row-fluid" >
                                <div class="span12">      
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-list"></i>View Service Details </div> 
                                            <div class="actions">
                                                <a onclick="showSearchDiv();" class="btn green" href="javascript:void(0)"><i class="icon-collapse"></i> Search</a>	
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div style="display: none;" id="advanced_search">
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="dataTables_length">
                                                            Search: &nbsp;
                                                            <input type="text" class="m-wrap medium ac_input" autocomplete="off" name="keyword" id="keyword" placeholder="Search by name" value="">
                                                            &nbsp;&nbsp;
                                                            <button class="btn blue" style="margin-bottom:10px;" type="button" onclick="loadData(1);"><i class="icon-search"></i> </button>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div id="dataGrid"></div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                            
                            <div class="form-actions" style="padding-left:10px;">                                   
                                 <a href="<?=$this->config->config['admin_base_url']?>service/jobs" class="btn">Cancel</a>            
                            </div>
                        </div>
                </div>
                <!--END TABS-->
              </div>
          </div>
          <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER--> 
</div>
<!-- END PAGE -->    
</div>
<!-- END CONTAINER -->    
<?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>  
<script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script src="<?=base_url()?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/select2.min.js"></script>    
<script src="<?=base_url()?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>assets/scripts/app.js"></script> 
<script src="<?=base_url()?>assets/scripts/roles.js" type="text/javascript"></script> 
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           Role.init();
           loadData(1);
        });
        
        
        function loadDataByPage(page) {
        loadData(page);
    }
    
function loadData(page){
            var limit = 10;
            var pageContent = jQuery('#mainGridContainer');
            var keyword = jQuery('#keyword').val();
            var projectId = '<?php echo $this->uri->segment('4')?>';
            var userId = '<?php echo $project_owner_data->userId ?>';
            App.blockUI(pageContent, true);
            jQuery.ajax
            ({
                    type: "POST",
                    url: '<?=$this->config->config['admin_base_url']?>service/service_deatils_pagi',
                    data: "limit="+limit+"&page="+page+"&keyword="+encodeURIComponent(keyword)+"&projectId="+projectId+"&userId="+userId,
                    success: function(msg)
                    {
                            App.unblockUI(pageContent);
                            jQuery('#dataGrid').html(msg);
                    }
            });
	}
    
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>