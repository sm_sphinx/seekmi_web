<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4HXQT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T4HXQT');</script>
<!-- End Google Tag Manager -->
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
                                <a class="brand" style="margin-left:0px;" href="<?=$this->config->config['admin_base_url']?>user/users" ><img src="<?=$this->config->config['base_url']?>images/Pintack_orange_152x29beta-small.png" style="margin-left:0px;"/>
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:void(0);" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="<?=$this->config->config['base_url']?>assets/img/menu-toggler.png" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->            
				<!-- BEGIN TOP NAVIGATION MENU -->              
						<ul class="nav pull-right">															  
                    		<!-- BEGIN USER LOGIN DROPDOWN -->                   
                          	<li class="dropdown user">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" src="<?=$this->config->config['base_url']?>assets/img/avatar.png" width="29" height="29" />  
                                <span class="username"><?=$this->session->userdata('user_name')?></span>
                                <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">  
                                    <?php /*<li><a href="<?=$this->config->config['base_url']?>admin/user/my_account"><i class="icon-user"></i> My Account</a></li>*/ ?>                                  					
                                    <li><a href="<?=$this->config->config['admin_base_url']?>user/logout"><i class="icon-key"></i> Logout</a></li>
                                </ul>
                            </li>
                    	</ul>
				<!-- END TOP NAVIGATION MENU --> 
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
<!-- END HEADER -->


