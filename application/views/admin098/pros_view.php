<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?= $this->config->config['base_url'] ?>assets/plugins/select2/select2_metro.css" />
<link rel="stylesheet" href="<?= $this->config->config['base_url'] ?>assets/plugins/data-tables/DT_bootstrap.css" />
<!-- END PAGE LEVEL STYLES -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <?php include('header_view.php'); ?>
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
            <?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="portlet-config" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button"></button>
                    <h3>portlet Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here will be a configuration form</p>
                </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->        
            <div class="container-fluid" id="mainGridContainer">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">						
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            <?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">							
                            <li><a href="<?= $this->config->config['admin_base_url'] ?>pros/lists"><?= $page_title ?></a></li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->


                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid" >
                    <div class="span12">      
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-list"></i><?= $page_title ?> </div> 
                                <div class="actions">
                                    <select id="page_limit" name="page_limit" onchange="jsFunction(this.value);">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> 
                                    <?php if ($user_count > 0) { ?>
                                        <a onclick="exportUsers();" class="btn green" href="javascript:void(0)"> <i class="icon-download"> Export</i></a>	
                                    <?php } ?>
                                    <a onclick="showSearchDiv();" class="btn green" href="javascript:void(0)"><i class="icon-collapse"></i> Search</a>	
                                    <a href="<?= $this->config->config['admin_base_url'] ?>pros/add_pros" class="btn green"><i class="icon-plus"></i> Add</a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div style="display: none;" id="advanced_search">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="dataTables_length">
                                                Search: &nbsp;
                                                <input type="text" class="m-wrap medium ac_input" autocomplete="off" name="keyword" id="keyword" aria-controls="sample_editable_1" placeholder="Search by name or email or phone" value="">
                                                &nbsp;&nbsp;
                                                <button class="btn blue" style="margin-bottom:10px;" type="button" onclick="loadData(1);"><i class="icon-search"></i> </button>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div id="dataGrid"></div>
                            </div>
                        </div>
                    </div>
                </div>				
                <!-- END PAGE CONTENT -->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->    
    <?php include('footer_view.php'); ?>
    <!-- BEGIN CORE PLUGINS -->   


    <div id="deleteUserConfirm" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h3>Delete User</h3>
        </div>
        <div class="modal-body">
            <input type="hidden" id="userID" name="userID"  >  
            <input type="hidden" id="currentPage" name="currentPage" >
            <p><span class="icon icon-warning-sign"></span>
                This professional will be permanently deleted and cannot be recovered. Are you sure?
            </p>
        </div>
        <div class="modal-footer">
            <button class="btn red" type="button" onClick="deleteUser()">Delete</button>
            <button type="button" data-dismiss="modal" class="btn">Close</button>
        </div>
    </div>
    <?php include('scripts_footer.php'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>assets/plugins/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>assets/plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?= $this->config->config['base_url'] ?>assets/scripts/app.js"></script>    
    <?php /* ?><script src="<?=$this->config->config['base_url']?>assets/scripts/table-editable.js"></script>    <?php */ ?>
    <script type="text/javascript">
                var p = '<?php echo $page; ?>';
                function show_popup(rid, page)
                {
                    jQuery("#userID").val(rid);
                    jQuery("#currentPage").val(page);
                    jQuery("#deleteUserConfirm").modal("show");
                }


                jQuery(document).ready(function () {
                    App.init();
                    loadData(p);
                });

                function loadDataByPage(page)
                {
                    loadData(page);
                }

                function deleteUser()
                {
                    var userId = parseInt(jQuery("#userID").val());
                    var currentPage = parseInt(jQuery("#currentPage").val());
                    if (userId > 0 && currentPage > 0)
                    {
                        jQuery("#deleteUserConfirm").modal("hide");
                        var pageContent = jQuery('#mainGridContainer');
                        App.blockUI(pageContent, true);
                        jQuery.ajax
                                ({
                                    type: "POST",
                                    url: '<?= $this->config->config['admin_base_url'] ?>pros/deletePros',
                                    data: "userId=" + userId,
                                    success: function (msg)
                                    {
                                        loadData(currentPage);
                                        App.unblockUI(pageContent);
                                    }
                                });
                    }

                }

                function loadData(page) {
                    var limit = $('select#page_limit option:selected').val();
                    var pageContent = jQuery('#mainGridContainer');
                    var keyword = jQuery('#keyword').val();
                    App.blockUI(pageContent, true);
                    jQuery.ajax
                            ({
                                type: "POST",
                                url: '<?= $this->config->config['admin_base_url'] ?>pros/lists_pagi',
                                data: "limit=" + limit + "&page=" + page + "&keyword=" + encodeURIComponent(keyword),
                                success: function (msg)
                                {
                                    App.unblockUI(pageContent);
                                    jQuery('#dataGrid').html(msg);
                                }
                            });
                }

                function exportUsers()
                {
                    var keyword = jQuery('#keyword').val();
                    var pageContent = jQuery('#mainGridContainer');
                    App.blockUI(pageContent, true);
                    location.href = '<?= $this->config->config['admin_base_url'] ?>pros/export/' + encodeURIComponent(keyword);
                    App.unblockUI(pageContent);
                }
                function jsFunction(val)
                {
                    loadData(1);
                }
    </script>
</body>
<!-- END BODY -->
</html>