<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url() ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<style type="text/css">
    #modalPopupDiv{
        width:800px;
    }
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <?php include('header_view.php'); ?>
    <?php
    if (isset($service_info)) {
        $cnt = count($service_info);
        $subId = $service_info->subserviceId;
        $servname = $service_info->subserviceName;
        $servnameind = $service_info->subserviceNameIND;
        $servslug = $service_info->serviceSlug;
        $informaticType = $service_info->informaticType;
        $detailsType = $service_info->detailsType;
        $timeType = $service_info->timeType;
        $travelType = $service_info->travelType;
        $durationType = $service_info->durationType;
        $zipType = $service_info->zipType;
        $timeoption = $service_info->timeOption;
        if ($timeoption != '') {
            $timeArr = explode(",", $timeoption);
        } else {
            $timeArr = array();
        }
        $isFeatured = $service_info->isFeatured;
        $status = $service_info->status;
        $avlCredit = $service_info->avlCredit;
        if (!empty($tag_info)) {
            $tags = $tag_info->tags;
        } else {
            $tags = '';
        }
        if (!empty($tag_id_info)) {
            $tags_id = $tag_id_info->tags;
        } else {
            $tags_id = '';
        }
        $catId = $service_info->categoryId;
    } else {
        $subId = 0;
        $servname = '';
        $servnameind = '';
        $servslug = '';
        $informaticType = '';
        $detailsType = '';
        $travelType = '';
        $durationType = '';
        $timeType = '';
        $zipType = '';
        $timeoption = '';
        $timeArr = '';
        $status = '';
        $avlCredit = 0;
        $tags = '';
        $tags_id = '';
        $isFeatured = '';
        $catId = '';
        $cnt = 0;
    }
    ?>        
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
            <?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content" id="mainDiv">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid" >
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END BEGIN STYLE CUSTOMIZER --> 
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            <?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">							
                            <li>
                                <i class="icon-home"></i>                                
                                <a href="<?= $this->config->config['admin_base_url'] ?>service/sublist">Manage Services</a>                                              
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="#"><?= $page_title ?></a></li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid profile">
                    <div class="span12">
                        <!--BEGIN TABS-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-reorder"></i><?= $page_title ?></div>

                            </div>
                            <div class="portlet-body form">                                
                                <div class="tabbable tabbable-custom tabbable-full-width">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
                                        <?php if ($subId != '0') { ?>
                                            <li><a href="#tab_1_2" data-toggle="tab">Questionnaire</a></li> 
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane row-fluid active" id="tab_1_1">
                                            <!-- BEGIN FORM-->
                                            <form class="form-horizontal sub-service-form" name="mySubserviceFrm" id="mySubserviceFrm" method="post" action="">
                                                <input type="hidden" name="service_id" id="service_id" value="<?= $subId ?>"/>                                       
                                                <div class="alert alert-error hide">
                                                    <button class="close" data-hide="alert" type="button"></button>
                                                    <span>Service is already exists.</span>
                                                </div>
                                                <div class="alert alert-success hide">
                                                    <button class="close" type="button"  data-hide="alert"></button>
                                                    Service has been created successfully.
                                                </div>                                                                   
                                                <div class="control-group">
                                                    <label class="control-label">Service Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <div class="input-icon">
                                                            <input name='txtServiceName' id='txtServiceName' value="<?= $servname ?>" class='m-wrap span6' placeholder='Sub Service Name'/>                                        
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Service Name (Indonesian)</label>
                                                    <div class="controls">
                                                        <div class="input-icon">
                                                            <input name='txtServiceNameIND' id='txtServiceNameIND' value="<?= $servnameind ?>" class='m-wrap span6' placeholder='Sub Service Name (Indonesian)'/>                                        
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Service Slug</label>
                                                    <div class="controls">
                                                        <div class="input-icon">
                                                            <input name='txtServiceSlug' id='txtServiceSlug' value="<?= $servslug ?>" class='m-wrap span6' placeholder='Sub Service Slug'/>                                        
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Category</label>
                                                    <div class="controls">
                                                        <div class="">                                                                 
                                                            <select name="txtCategory" id="txtCategory">  
                                                                <option value="">--Select Category--</option>
                                                                <?php
                                                                if (!empty($cat_list)) {
                                                                    foreach ($cat_list as $cat_row) {
                                                                        ?>
                                                                        <option value="<?php echo $cat_row->catId; ?>" <?php if ($cat_row->catId == $catId) { ?> selected<?php } ?>><?php echo $cat_row->categoryName; ?></option>                                                     
                                                                    <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>                                             
                                                        </div>
                                                    </div>
                                                </div> 

                                                <div class="control-group">
                                                    <label class="control-label">Alternate Names English</label>
                                                    <div class="controls">
                                                        <div class="input-icon">
                                                            <input name='txtServiceTags' id='txtServiceTags' value="<?= $tags ?>" class='m-wrap span6' placeholder='Alternate Names English: eg tag1,tag2'/>                                        
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Alternate Names Indonesian</label>
                                                    <div class="controls">
                                                        <div class="input-icon">
                                                            <input name='txtServiceTagsID' id='txtServiceTagsID' value="<?= $tags_id ?>" class='m-wrap span6' placeholder='Alternate Names Indonesian: eg tag1,tag2'/>                                        
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Questionnaire Type</label>
                                                    <div class="controls">
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="questionType[]" value="informatic" <?php if ($cnt == 0 || $informaticType == 'Y') { ?> checked="checked"<?php } ?>/> Informatic
                                                        </label>
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="questionType[]" value="details" <?php if ($cnt == 0 || $detailsType == 'Y') { ?> checked="checked"<?php } ?>/> Details
                                                        </label>
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="questionType[]" value="time" <?php if ($cnt == 0 || $timeType == 'Y') { ?> checked="checked"<?php } ?> onclick="showTimeDisplay(this.checked);"/> Times
                                                        </label>
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="questionType[]" value="travel" <?php if ($cnt == 0 || $travelType == 'Y') { ?> checked="checked"<?php } ?> /> Travel Preference
                                                        </label>
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="questionType[]" value="duration" <?php if ($cnt == 0 || $durationType == 'Y') { ?> checked="checked"<?php } ?> /> Duration
                                                        </label>
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="questionType[]" value="zip" <?php if ($cnt == 0 || $zipType == 'Y') { ?> checked="checked"<?php } ?>/> Zipcode
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="control-group" id="TimeDisplayDiv" <?php if (empty($timeArr)) { ?>style="display:none;" <?php } ?>>
                                                    <label class="control-label">Schedule Options</label>
                                                    <div class="controls">
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="timeOptionType[]" value="flexible" <?php if ((!empty($timeArr) && in_array('flexible', $timeArr)) || $cnt == 0) { ?> checked="checked" <?php } ?>/> Flexible
                                                        </label>
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="timeOptionType[]" value="days" <?php if ((!empty($timeArr) && in_array('days', $timeArr)) || $cnt == 0) { ?> checked="checked" <?php } ?>/> In the next few days
                                                        </label>
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="timeOptionType[]" value="asap" <?php if ((!empty($timeArr) && in_array('asap', $timeArr)) || $cnt == 0) { ?> checked="checked" <?php } ?>/> As soon as possible
                                                        </label>
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="timeOptionType[]" value="specific" <?php if ((!empty($timeArr) && in_array('specific', $timeArr)) || $cnt == 0) { ?> checked="checked" <?php } ?>/> On one particular date
                                                        </label>
                                                        <label class="checkbox line">
                                                            <input type="checkbox" name="timeOptionType[]" value="other" <?php if ((!empty($timeArr) && in_array('other', $timeArr)) || $cnt == 0) { ?> checked="checked" <?php } ?>/> Other (I'd need to describe)
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Credit : </label>
                                                    <div class="controls">
                                                        <div class="input-icon">
                                                            <input name='avlCredit' id='avlCredit' value="<?php
                                                            if ($cnt == 0) {
                                                                echo '1';
                                                            } else {
                                                                echo $avlCredit;
                                                            }
                                                            ?>" class='m-wrap span6' placeholder='Credit'/>                                        
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Is Featured</label>
                                                    <div class="controls">
                                                        <div class="">                                                                 
                                                            <select name="txtIsFeatured" id="txtIsFeatured">                                     
                                                                <option value="Y" <?php if ($isFeatured == 'Y') { ?> selected<?php } ?>>Y</option> 
                                                                <option value="N" <?php if ($isFeatured == 'N') { ?> selected<?php } ?>>N</option>                                            
                                                            </select>                                             
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Status</label>
                                                    <div class="controls">
                                                        <div class="">                                                                 
                                                            <select name="txtStatus" id="txtStatus">                                     
                                                                <option value="Y" <?php if ($status == 'Y') { ?> selected<?php } ?>>Active</option> 
                                                                <option value="N" <?php if ($status == 'N') { ?> selected<?php } ?>>Inactive</option>                                            
                                                            </select>                                             
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" id="register-submit-btn" class="btn green ">
                                                        Save                                  
                                                    </button>
                                                    <a href="<?= $this->config->config['admin_base_url'] ?>service/sublist" class="btn">Cancel</a>            
                                                </div> 
                                            </form>
                                            <!-- END FORM-->                                  
                                        </div>
<?php if ($subId != '0') { ?>
                                            <div class="tab-pane row-fluid" id="tab_1_2">                                    
                                                <div id="dataGrid"></div>
                                            </div>
<?php } ?>
                                    </div>  

                                </div>

                            </div>
                        </div>
                        <!--END TABS-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->                
            </div>
            <!-- END PAGE CONTAINER--> 
        </div>
        <!-- END PAGE -->    
    </div>

    <div id="modalPopupDiv" class="modal hide fade" tabindex="-1" data-focus-on="input:first"></div>
    <!-- END CONTAINER -->    

    <div id="deleteQuestionConfirm" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h3>Delete Question</h3>
        </div>
        <div class="modal-body">
            <input type="hidden" id="questionID" name="questionID"  >  
            <input type="hidden" id="currentPage" name="currentPage" >
            <p><span class="icon icon-warning-sign"></span>
                This question will be permanently deleted and cannot be recovered. Are you sure?
            </p>
        </div>
        <div class="modal-footer">
            <button class="btn red" type="button" onClick="deleteQuestion()">Delete</button>
            <button type="button" data-dismiss="modal" class="btn">Close</button>
        </div>
    </div>
    <?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>  
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
    <script src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>    
    <script src="<?= base_url() ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
    <script src="<?= base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
    <script src="<?= base_url() ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?= base_url() ?>assets/scripts/app.js"></script> 
    <script src="<?= base_url() ?>assets/scripts/services.js" type="text/javascript"></script> 
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
                jQuery(document).ready(function () {
                    // initiate layout and plugins
                    App.init();
                    Services.init();
                    loadData();
                });


                function show_popup(qid, page)
                {
                    jQuery("#questionID").val(qid);
                    jQuery("#currentPage").val(page);
                    jQuery("#deleteQuestionConfirm").modal("show");
                }

                function deleteQuestion()
                {
                    var questionId = parseInt(jQuery("#questionID").val());
                    if (questionId > 0)
                    {
                        jQuery("#deleteQuestionConfirm").modal("hide");
                        var pageContent = jQuery('#mainGridContainer');
                        App.blockUI(pageContent, true);
                        jQuery.ajax
                                ({
                                    type: "POST",
                                    url: '<?= $this->config->config['admin_base_url'] ?>service/deleteQuestion',
                                    data: "questionId=" + questionId,
                                    success: function (msg)
                                    {
                                        loadData();
                                        App.unblockUI(pageContent);
                                    }
                                });
                    }

                }

                function loadData() {
                    var pageContent = jQuery('#mainGridContainer');
                    App.blockUI(pageContent, true);
                    jQuery.ajax
                            ({
                                type: "POST",
                                url: '<?= $this->config->config['admin_base_url'] ?>service/questionlist_pagi',
                                data: "serviceid=<?= $subId ?>",
                                success: function (msg)
                                {
                                    App.unblockUI(pageContent);
                                    jQuery('#dataGrid').html(msg);
                                }
                            });
                }


                function displayOptionDisplay(vvl) {
                    if (vvl == 'checkbox' || vvl == 'radio' || vvl == 'select') {
                        jQuery('#optionDiv').show();
                    } else {
                        jQuery('#optionDiv').hide();
                    }
                }

                function displayOptionMore() {
                    var num = jQuery('#option_cnt').val();
                    var nextid = parseInt(num) + 1;
                    var nmid = 'txtQuestionType' + nextid;
                    jQuery('#option_cnt').val(nextid);
                    var html = '<label class="input-icon" id="subOptionHtml' + nextid + '"><input name="txtOption' + nextid + '" id="' + nmid + '" value="" class="m-wrap span2"/> &nbsp;[EN] \n\
                <input name="txtOptionIndonesion' + nextid + '" id="' + nmid + '" value="" class="m-wrap span2" style="margin-top:5px"/> &nbsp;[ID]\n\
                <input type="hidden" name="txtOptionHid' + nextid + '" id="txtOptionHid' + nextid + '" value="" />\n\
                <button type="button" class="btn green" onclick="deleteOptionMore(' + nextid + ');">Delete</button></label>'
                    jQuery('#optionHtmlDisplay').append(html);
                }

                function deleteOptionMore(cnt)
                {
                    var curid = parseInt(cnt);
                    //alert(curid);
                    jQuery('#subOptionHtml' + curid).remove();
                    jQuery('#option_cnt').val(curid-1);
                }

                function showTimeDisplay(chk)
                {
                    if (chk == true) {
                        jQuery('#TimeDisplayDiv').show();
                    } else {
                        jQuery('#TimeDisplayDiv').hide();
                    }
                }


                function showPopup(tp, id) {
                    var pageContent = jQuery('#mainGridContainer');
                    App.blockUI(pageContent, true);
                    jQuery.ajax
                            ({
                                type: "POST",
                                url: '<?= $this->config->config['admin_base_url'] ?>service/set_question',
                                data: "serviceid=<?= $subId ?>&type=" + tp + "&qid=" + id,
                                success: function (msg)
                                {
                                    App.unblockUI(pageContent);
                                    jQuery('#modalPopupDiv').html(msg);
                                    jQuery('#modalPopupDiv').modal('show');
                                }
                            });
                }

    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>