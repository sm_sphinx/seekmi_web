 
<?php if(count($list_res) > 0 ){?>
    	<table class="table table-striped table-bordered table-hover" id="sample_1">
        	<thead>
            	<tr>
                    <th width="30%">Name</th>
                    <th width="30%">Email</th>
                    <th width="10%">Role</th>
                    <th width="10%">Status</th>
                    <th width="20%">Action</th>
              	</tr>
            </thead>
            <tbody>
	     <?php foreach($list_res as $row){ ?>
                <tr>
                    <td><?=$row->firstName?> <?=$row->lastName?></td>  
                    <td><?=$row->email?></td>     
                    <td><?=$row->roleName?></td>
                    <td><?php  if($row->astatus=='Y') echo 'Active'; elseif($row->astatus=='N') echo 'Inactive'; else echo ucfirst($row->astatus); ?></td>         
                    <td>
                    <a class="btn btn-info yellow" href="<?=$this->config->config['admin_base_url']?>user/edit_admin_user/<?=$row->userid?>"><i class="icon-edit icon-white"></i> Edit</a>
		    <a class="btn btn-danger red" href="javascript:show_popup('<?=$row->userid?>','<?=$cur_page?>');"><i class="icon-trash icon-white"></i> Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>        
    </table>
     <?php
/* --------------------------------------------- */
$no_of_paginations = ceil($list_count / $per_page);

/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
if ($cur_page >= 3) {
    $start_loop = $cur_page - 1;
    if ($no_of_paginations > $cur_page + 1)
        $end_loop = $cur_page + 1;
    else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 2) {
        $start_loop = $no_of_paginations - 2;
        $end_loop = $no_of_paginations;
    } else {
        $end_loop = $no_of_paginations;
    }
} else {
    $start_loop = 1;
    if ($no_of_paginations > 3)
        $end_loop = 3;
    else
        $end_loop = $no_of_paginations;
}
/* ----------------------------------------------------------------------------------------------------------- */

$msg='';
// FOR ENABLING THE FIRST BUTTON
if ($first_btn && $cur_page > 1) {
    $msg .= "<li onclick='loadDataByPage(1);' class='inactive'><a href='javascript:void(0);' style='color:#0D88CB'>First</a></li>";
} else if ($first_btn) {
    $msg .= "<li  class='active'><a href='javascript:void(0);' >First</a></li>";
}

if ($previous_btn && $cur_page > 1) {
    $pre = $cur_page - 1;
    $msg .= "<li p='$pre' class='inactive' onclick='loadDataByPage(".$pre.");'><a href='javascript:void(0);' style='color:#0D88CB'>Prev</a></li>";
} else if ($previous_btn) {
    $msg .= "<li class='active'><a href='javascript:void(0);'>Prev</a></li>";
}
for ($i = $start_loop; $i <= $end_loop; $i++) {

    if ($cur_page == $i)
        $msg .= "<li p='$i' class='active' onclick='loadDataByPage(".$i.");'><a href='javascript:void(0);' style='color:#0D88CB'>{$i}</a></li>";
    else
        $msg .= "<li p='$i' class='inactive' onclick='loadDataByPage(".$i.");'><a href='javascript:void(0);'>{$i}</a></li>";
}

// TO ENABLE THE NEXT BUTTON
if ($next_btn && $cur_page < $no_of_paginations) {
    $nex = $cur_page + 1;
    $msg .= "<li p='$nex' class='inactive' onclick='loadDataByPage(".$nex.");'><a href='javascript:void(0);' style='color:#0D88CB'>Next</a></li>";
} else if ($next_btn) {
    $msg .= "<li class='active'><a href='javascript:void(0);'>Next</a></li>";
}

// TO ENABLE THE END BUTTON
if ($last_btn && $cur_page < $no_of_paginations) {
    $msg .= "<li onclick='loadDataByPage(".$no_of_paginations.");' class='inactive'><a href='javascript:void(0);'  style='color:#0D88CB'>Last</a></li>";
} else if ($last_btn) {
    $msg .= "<li class='active'><a href='javascript:void(0);'>Last</a></li>";
}
?>       
<div class='row-fluid'>
<div class='span12'>
<div  class="dataTables_paginate paging_bootstrap pagination">
    <ul>
        <?php echo $msg; ?>
    </ul>
</div>
<?php }else{?>
<table class="table table-striped table-bordered table-hover" id="sample_1"><thead><tr><th width="100%">No records found.</th></tr></thead><tbody></tbody></table></div>
<?php }?>

<script type="text/javascript">
jQuery('#keyword').keypress(function(event){
		   var keycode = (event.keyCode ? event.keyCode : event.which);
		   if(keycode == '13'){
			loadData('1');
		   }
		});
function showSearchDiv()
{
	jQuery('#advanced_search').slideToggle();
}
</script>

