<div style="text-align:right; margin-bottom: 10px;">                                                
    <a href="javascript:void(0);" onclick="showPopup('add','');" class="btn green"><i class="icon-plus"></i> Add</a>
</div>

<?php if(!empty($question_list)){ ?>
    <table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
            <tr>
                <th width="70%">Question</th>                                                        
                <th width="30%">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($question_list as $question_row){ ?>
            <tr>
                <td><?=$question_row->questionName?></td>                                                                                 
                <td>
                <a class="btn btn-info yellow" href="javascript:void(0);" onclick="showPopup('edit','<?=$question_row->questionId?>');"><i class="icon-edit icon-white"></i> Edit</a>
                <a class="btn btn-danger red" href="javascript:show_popup('<?=$question_row->questionId?>');"><i class="icon-trash icon-white"></i> Delete</a>
                </td>
            </tr>
            <?php } ?>
    </tbody>        
</table>
<?php }else{ ?>
<table class="table table-striped table-bordered table-hover" id="sample_1"><thead><tr><th width="100%">No question found.</th></tr></thead><tbody></tbody></table>
<?php } ?>