<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <?php include('header_view.php'); ?>
    <?php
    if (isset($cat_info)) {
        $catId = $cat_info->catId;
        $catname = $cat_info->categoryName; 
        $catnameind = $cat_info->categoryNameIND;
        $status = $cat_info->status;
    } else {
        $catId = 0;
        $catname = '';
        $catnameind='';
        $status = '';       
    }
    ?>        
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
<?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content" id="mainDiv">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid" >
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END BEGIN STYLE CUSTOMIZER --> 
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                        <?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">							
                            <li>
                                <i class="icon-home"></i>                                
                                <a href="<?=$this->config->config['admin_base_url']?>service/categorylist">Manage Categories</a>                                              
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="#"><?= $page_title ?></a></li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid profile">
                    <div class="span12">
                        <!--BEGIN TABS-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-reorder"></i><?= $page_title ?></div>

                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal category-form" name="myCategoryFrm" id="myCategoryFrm" method="post" action="">
                                    <input type="hidden" name="cat_id" id="cat_id" value="<?= $catId ?>"/>                                       
                                    <div class="alert alert-error hide">
                                        <button class="close" data-hide="alert" type="button"></button>
                                        <span>Category is already exists.</span>
                                    </div>
                                    <div class="alert alert-success hide">
                                            <button class="close" type="button"  data-hide="alert"></button>
                                            Category has been created successfully.
                                    </div>                                                                   
                                    <div class="control-group">
                                        <label class="control-label">Category Name<span class="required">*</span></label>
                                        <div class="controls">
                                            <div class="input-icon">
                                                <input name='txtCatname' id='txtCatname' value="<?= $catname ?>" class='m-wrap span6' placeholder='Category Name'/>                                        
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="control-group">
                                        <label class="control-label">Category Name (Indonesian)</label>
                                        <div class="controls">
                                            <div class="input-icon">
                                                <input name='txtCatnameind' id='txtCatnameind' value="<?= $catnameind ?>" class='m-wrap span6' placeholder='Category Name (Indonesian)'/>                                        
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Status</label>
                                        <div class="controls">
                                            <div class="">                                                                 
                                                <select name="txtStatus" id="txtStatus">                                     
                                                    <option value="Y" <?php if ($status == 'Y') { ?> selected<?php } ?>>Active</option> 
                                                    <option value="N" <?php if ($status == 'N') { ?> selected<?php } ?>>Inactive</option>                                            
                                                </select>                                             
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" id="register-submit-btn" class="btn green ">
                                            Save                                  
                                        </button>
                                        <a href="<?=$this->config->config['admin_base_url']?>service/categorylist" class="btn">Cancel</a>            
                                    </div>
                                </form>
                                <!-- END FORM-->  
                            </div>
                        </div>
                        <!--END TABS-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->                
            </div>
            <!-- END PAGE CONTAINER--> 
        </div>
        <!-- END PAGE -->    
    </div>
    <!-- END CONTAINER -->    
<?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>  
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
    <script src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>    
    <script src="<?= base_url() ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?= base_url() ?>assets/scripts/app.js"></script> 
    <script src="<?=base_url()?>assets/scripts/category.js" type="text/javascript"></script> 
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
        Category.init();       
    });    
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>