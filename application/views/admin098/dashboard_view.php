<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url() ?>assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <?php include('header_view.php'); ?>
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
            <?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->

        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END BEGIN STYLE CUSTOMIZER --> 
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            <?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= $this->config->config['admin_base_url'] ?>user/index">Dashboard</a> 
                                <i class="icon-angle-right"></i>
                            </li>							
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div id="dashboard">
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row-fluid">                                            
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">Customer Stats</div>                                                        
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">                                             
                        <div class="span4 responsive" data-tablet="span4" data-desktop="span3">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="icon-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><?php echo $customer_ttl_count; ?></div>
                                    <div class="desc">Total Customers</div>
                                </div>								                
                            </div>
                        </div>
                        <div class="span4 responsive" data-tablet="span4" data-desktop="span3">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="icon-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><?php echo $active_customer_ttl_count; ?></div>
                                    <div class="desc">Active Customers</div>
                                </div>								               
                            </div>
                        </div>
                        <div class="span4 responsive" data-tablet="span4" data-desktop="span3">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="icon-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><?php echo $inactive_customer_ttl_count; ?></div>
                                    <div class="desc">Inactive Customers</div>
                                </div>								                
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-calendar"></i>Signups Stats</div>									
                                </div>
                                <div class="portlet-body">
                                    <div class="row-fluid">
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?php echo (int) $register_today_percentage; ?>" class="number transactions easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?php echo (int) $register_today_percentage; ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">Today (<?php echo $customer_signup_today_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $register_week_percentage ?>" class="number visits easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $register_week_percentage ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">This Week (<?php echo $customer_signup_week_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $register_month_percentage ?>" class="number bounce easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $register_month_percentage ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">This Month (<?php echo $customer_signup_month_cnt; ?>)</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-calendar"></i>Deregistered Stats</div>

                                </div>
                                <div class="portlet-body">
                                    <div class="row-fluid">
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?php echo (int) $customer_deleted_today_percentage; ?>" class="number transactions easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?php echo (int) $customer_deleted_today_percentage; ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">Today (<?php echo $customer_deleted_today_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $customer_deleted_week_percentage_prof ?>" class="number visits easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $customer_deleted_week_percentage_prof ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">This Week (<?php echo $customer_deleted_week_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $customer_deleted_month_percentage ?>" class="number bounce easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $customer_deleted_month_percentage ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">This Month (<?php echo $customer_deleted_month_cnt; ?>)</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <!-- Professional Stats -->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row-fluid">                                            
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">Professional Stats</div>                                                        
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">                                             
                        <div class="span4 responsive" data-tablet="span4" data-desktop="span3">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="icon-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><?php echo $pros_ttl_count; ?></div>
                                    <div class="desc">Total Professionals</div>
                                </div>								                
                            </div>
                        </div>
                        <div class="span4 responsive" data-tablet="span4" data-desktop="span3">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="icon-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><?php echo $active_pros_ttl_count; ?></div>
                                    <div class="desc">Active Professionals</div>
                                </div>								               
                            </div>
                        </div>
                        <div class="span4 responsive" data-tablet="span4" data-desktop="span3">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="icon-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><?php echo $inactive_pros_ttl_count; ?></div>
                                    <div class="desc">Inactive Professionals</div>
                                </div>								                
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-calendar"></i>Signups Stats</div>									
                                </div>
                                <div class="portlet-body">
                                    <div class="row-fluid">
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?php echo (int) $register_today_percentage_prof; ?>" class="number transactions easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?php echo (int) $register_today_percentage_prof; ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">Today (<?php echo $pros_signup_today_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $register_week_percentage_prof ?>" class="number visits easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $register_week_percentage_prof ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">This Week (<?php echo $pros_signup_week_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $register_month_percentage_prof ?>" class="number bounce easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $register_month_percentage_prof ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">This Month (<?php echo $pros_signup_month_cnt; ?>)</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-calendar"></i>Activated Stats</div>

                                </div>
                                <div class="portlet-body">
                                    <div class="row-fluid">
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?php echo (int) $profetional_active_today_percentage; ?>" class="number transactions easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?php echo (int) $profetional_active_today_percentage; ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">Today (<?php echo $pros_active_today_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $profetional_active_week_percentage_prof ?>" class="number visits easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $profetional_active_week_percentage_prof ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">This Week (<?php echo $pros_active_week_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $profetional_active_month_percentage_prof ?>" class="number bounce easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $profetional_active_month_percentage_prof ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">This Month (<?php echo $pros_active_month_cnt; ?>)</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="portlet box purple">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-cogs"></i>Service Offered</div>

                        </div>
                        <div class="portlet-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Count</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 0;
                                    foreach ($top10service as $value) {
                                        $count++;
                                        ?>
                                        <tr>
                                            <td><?= $count ?></td>
                                            <td><?= $value->name ?></td>
                                            <td><?= $value->count ?></td>

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>
                    <!-- Job Request Stats -->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row-fluid">                                            
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">Job Requests Stats</div>                                                        
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">                                            
                        <div class="span6">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-calendar"></i>Job Requests Stats</div>

                                </div>
                                <div class="portlet-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?php echo (int) $profetional_job_pending_today_percentage; ?>" class="number transactions easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?php echo (int) $profetional_job_pending_today_percentage; ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">Today Pending (<?php echo $job_pending_today_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span6">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $profetional_job_expired_today_percentage ?>" class="number visits easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $profetional_job_expired_today_percentage ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">Today Expired (<?php echo $job_expired_today_cnt; ?>)</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-calendar"></i>Job Completed Requests Stats</div>

                                </div>
                                <div class="portlet-body">
                                    <div class="row-fluid">
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?php echo (int) $profetional_job_completed_today_percentage; ?>" class="number transactions easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?php echo (int) $profetional_job_completed_today_percentage; ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">Today (<?php echo $job_completed_today_cnt; ?>)</div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $profetional_job_completed_week_percentage_prof ?>" class="number visits easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $profetional_job_completed_week_percentage_prof ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">Week (<?php echo $job_completed_week_cnt; ?>)</div>
                                            </div>
                                        </div>

                                        <div class="margin-bottom-10 visible-phone"></div>
                                        <div class="span4">
                                            <div class="easy-pie-chart">
                                                <div data-percent="<?= (int) $profetional_job_completed_month_percentage_prof ?>" class="number bounce easyPieChart" style="width: 75px; height: 75px; line-height: 75px;"><span><?= (int) $profetional_job_completed_month_percentage_prof ?></span>%<canvas height="75" width="75"></canvas></div>
                                                <div class="title">This Month (<?php echo $job_completed_month_cnt; ?>)</div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="row-fluid">
                        <div class="span6">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-cogs"></i>Service Category</div>

                                </div>
                                <div class="portlet-body">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Service Name</th>
                                                <th>Count</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $count = 0;
                                            foreach ($job20category as $value) {
                                                $count++;
                                                ?>
                                                <tr>
                                                    <td><?= $count ?></td>
                                                    <td><?= $value->name ?></td>
                                                    <td><?= $value->count ?></td>

                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
                        <div class="span6">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-coffee"></i>Service Fullfilled Category</div>

                                </div>
                                <div class="portlet-body">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Service Name</th>
                                                <th>Count</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $count = 0;
                                            foreach ($job20fullfill as $value) {
                                                $count++;
                                                ?>
                                                <tr>
                                                    <td><?= $count ?></td>
                                                    <td><?= $value->name ?></td>
                                                    <td><?= $value->count ?></td>
                                                </tr>
                                            <?php }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix"></div>

                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER--> 
        </div>
        <!-- END PAGE -->    
    </div>
    <!-- END CONTAINER -->
    <?php include('footer_view.php'); ?>        
    <?php include('scripts_footer.php'); ?>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>     
    <script src="<?= base_url() ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/scripts/app.js"></script>  
    <script src="<?= base_url() ?>assets/scripts/index.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function () {
            // initiate layout and plugins
            App.init();
            Index.init();
            Index.initMiniCharts();
        });
    </script>
</body>
<!-- END BODY -->
</html>