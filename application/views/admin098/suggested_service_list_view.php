<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>assets/plugins/select2/select2_metro.css" />
<link rel="stylesheet" href="<?=$this->config->config['base_url']?>assets/plugins/data-tables/DT_bootstrap.css" />
<!-- END PAGE LEVEL STYLES -->    
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<?php include('header_view.php'); ?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid" id="mainGridContainer">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<?php include('left_view.php'); ?>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->        
			<div class="container-fluid" id="mainGridContainer">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">						
                                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                                            <h3 class="page-title">
                                               <?=$page_title?>
                                            </h3>
                                            <ul class="breadcrumb">							
                                               <li><?=$page_title?></li>
                                            </ul>
                                            <!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
                                <div class="row-fluid" >
                                <div class="span12">      
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-list"></i><?= $page_title ?> </div> 
                                            <div class="actions">
                                                <a onclick="showSearchDiv();" class="btn green" href="javascript:void(0)"><i class="icon-collapse"></i> Search</a>	                                                
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div style="display: none;" id="advanced_search">
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="dataTables_length">
                                                            Search: &nbsp;
                                                            <input type="text" class="m-wrap medium ac_input" autocomplete="off" name="keyword" id="keyword" placeholder="Search by service , user" value="">
                                                            &nbsp;&nbsp;
                                                            <button class="btn blue" style="margin-bottom:10px;" type="button" onclick="loadData(1);"><i class="icon-search"></i> </button>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div id="dataGrid"></div>
                                        </div>
                                    </div>
                                </div>
                             </div>				
			     <!-- END PAGE CONTENT -->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->    
	<?php include('footer_view.php'); ?>
        <!-- BEGIN CORE PLUGINS -->   
   	<?php include('scripts_footer.php'); ?>
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="<?=$this->config->config['base_url']?>assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="<?=$this->config->config['base_url']?>assets/plugins/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?=$this->config->config['base_url']?>assets/plugins/data-tables/DT_bootstrap.js"></script>
        <script type="text/javascript" src="<?=$this->config->config['base_url']?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
  
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=$this->config->config['base_url']?>assets/scripts/app.js"></script>    
	<?php /*?><script src="<?=$this->config->config['base_url']?>assets/scripts/table-editable.js"></script>    <?php */?>
	<script type="text/javascript">
	var p = '<?php echo $page;?>';
	jQuery(document).ready(function() {       
	    App.init();
	    loadData(p);	   
	});	
	
	function loadDataByPage(page)
	{
            loadData(page);
	}
	
	function loadData(page){
            var limit = 10;
            var pageContent = jQuery('#mainGridContainer');
            var keyword = jQuery('#keyword').val();            
            App.blockUI(pageContent, true);
            jQuery.ajax
            ({
                    type: "POST",
                    url: '<?=$this->config->config['admin_base_url']?>service/suggested_list_pagi',
                    data: "limit="+limit+"&page="+page+"&keyword="+encodeURIComponent(keyword),
                    success: function(msg)
                    {
                            App.unblockUI(pageContent);
                            jQuery('#dataGrid').html(msg);
                    }
            });
	}
	</script>
</body>
<!-- END BODY -->
</html>