<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <?php include('header_view.php'); ?>
    <?php
    if (isset($service_info)) {
        $servId = $service_info->serviceId;
        $servname = $service_info->serviceName; 
        $servnameind = $service_info->serviceNameIND;
        $servslug = $service_info->serviceSlug;
        $catId = $service_info->categoryId;
        $status = $service_info->status;
    } else {
        $servId = 0;
        $servname = '';
        $servnameind='';
        $servslug='';
        $catId='';
        $status = '';       
    }
    ?>        
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
<?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content" id="mainDiv">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid" >
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END BEGIN STYLE CUSTOMIZER --> 
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                        <?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">							
                            <li>
                                <i class="icon-home"></i>                                
                                <a href="<?=$this->config->config['admin_base_url']?>service/lists">Manage Services</a>                                              
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="#"><?= $page_title ?></a></li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid profile">
                    <div class="span12">
                        <!--BEGIN TABS-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-reorder"></i><?= $page_title ?></div>

                            </div>
                            <div class="portlet-body form">
                                <form class="form-horizontal service-form" name="myServiceFrm" id="myServiceFrm" method="post" action="">
           <div class="tabbable tabbable-custom tabbable-full-width">
            <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
                    <li><a href="#tab_1_2" data-toggle="tab">Sub services</a></li>                   
            </ul>
            <div class="tab-content">
                    <div class="tab-pane row-fluid active" id="tab_1_1">
                        
                        <!-- BEGIN FORM-->
                        
                            <input type="hidden" name="service_id" id="service_id" value="<?= $servId ?>"/>                                       
                            <div class="alert alert-error hide">
                                <button class="close" data-hide="alert" type="button"></button>
                                <span>Service is already exists.</span>
                            </div>
                            <div class="alert alert-success hide">
                                    <button class="close" type="button"  data-hide="alert"></button>
                                    Service has been created successfully.
                            </div>                                                                   
                            <div class="control-group">
                                <label class="control-label">Service Name<span class="required">*</span></label>
                                <div class="controls">
                                    <div class="input-icon">
                                        <input name='txtServiceName' id='txtServiceName' value="<?= $servname ?>" class='m-wrap span6' placeholder='Service Name'/>                                        
                                    </div>
                                </div>
                            </div> 
                            <div class="control-group">
                                <label class="control-label">Service Name (Indonesian)</label>
                                <div class="controls">
                                    <div class="input-icon">
                                        <input name='txtServiceNameIND' id='txtServiceNameIND' value="<?= $servnameind ?>" class='m-wrap span6' placeholder='Service Name (Indonesian)'/>                                        
                                    </div>
                                </div>
                            </div> 
                            <div class="control-group">
                                <label class="control-label">Service Slug</label>
                                <div class="controls">
                                    <div class="input-icon">
                                        <input name='txtServiceSlug' id='txtServiceSlug' value="<?= $servslug ?>" class='m-wrap span6' placeholder='Service Slug'/>                                        
                                    </div>
                                </div>
                            </div> 
                            <div class="control-group">
                                <label class="control-label">Category</label>
                                <div class="controls">
                                    <div class="">                                                                 
                                        <select name="txtCategory" id="txtCategory">  
                                            <option value="">--Select Category--</option>
                                            <?php if(!empty($cat_list)){ 
                                                foreach($cat_list as $cat_row){
                                            ?>
                                            <option value="<?php echo $cat_row->catId; ?>" <?php if ($cat_row->catId == $catId) { ?> selected<?php } ?>><?php echo $cat_row->categoryName; ?></option>                                                     
                                                <?php }} ?>
                                        </select>                                             
                                    </div>
                                </div>
                            </div>                            
                            <div class="control-group">
                                <label class="control-label">Status</label>
                                <div class="controls">
                                    <div class="">                                                                 
                                        <select name="txtStatus" id="txtStatus">                                     
                                            <option value="Y" <?php if ($status == 'Y') { ?> selected<?php } ?>>Active</option> 
                                            <option value="N" <?php if ($status == 'N') { ?> selected<?php } ?>>Inactive</option>                                            
                                        </select>                                             
                                    </div>
                                </div>
                            </div>
                           
                      
                        <!-- END FORM-->  
                     </div>
                       
                <div class="tab-pane row-fluid" id="tab_1_2">
                    
                    
                            <div class="control-group">
                                <table class="span12">
                                    <tr>
                                        <td colspan="2">Search: <input id="firstFilterSearch" type="text">
                                            <input type="button" class="btn" id="boxClear" name="boxClear" value="Reset"><br><br></td>
                                    </tr>
                                    <tr>
                                        <td class="span5"> 

                                            <select multiple="multiple" id="lstBox1" size="7" style="width:300px;height:420px;">                                                           
                                              <?php
                                                 if(!empty($sub_list)){
                                                     foreach($sub_list as $sub_row){
                                              ?>
                                              <option value="<?php echo $sub_row->subserviceId; ?>"><?php echo $sub_row->subserviceName; ?></option>
                                              <?php }} ?>
                                        </select>
                                    </td>
                                    <td class="span2" style='text-align:center;vertical-align:middle;'>
                                        <input type='button' class="btn" id='btnRight' value ='  >  '/>
                                        <br/><br/><input type='button' class="btn" id='btnLeft' value ='  <  '/>
                                    </td>
                                    <td class="span5">                                               
                                        <select multiple="multiple" name="txtSubService[]" id="lstBox2" size="7" style="width:300px;height:420px;">                                                   
                                            <?php if(!empty($sub_service_list)){ 
                                                foreach($sub_service_list as $sub_service_row){ ?>
                                            <option selected="selected" value="<?php echo $sub_service_row->subserviceId?>"><?php echo $sub_service_row->subserviceName?></option>
                                            <?php }} ?>
                                        </select>
                                    </td>
                                </tr>
                                </table>
                            </div> 
                </div>
            </div>
                <div class="form-actions">
                    <button type="submit" id="register-submit-btn" class="btn green ">
                        Save                                  
                    </button>
                    <a href="<?=$this->config->config['admin_base_url']?>service/lists" class="btn">Cancel</a>            
                </div>
           </div>
             </form>                       
           </div>
                    </div>            <!--END TABS-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->                
            </div>
            <!-- END PAGE CONTAINER--> 
        </div>
        <!-- END PAGE -->    
    </div>
    <!-- END CONTAINER -->    
<?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>  
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
    <script src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>    
    <script src="<?= base_url() ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?= base_url() ?>assets/scripts/app.js"></script> 
    <script src="<?=base_url()?>assets/scripts/services.js" type="text/javascript"></script> 
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
        Services.init();       
    }); 
    jQuery(document).ready(function() {
        
        jQuery('#boxClear').click(function(){
           jQuery('#firstFilterSearch').val('');
           var inputVal='';
           var allCells = jQuery('#lstBox1').find('option');
                if(allCells.length > 0)
                {
                    allCells.each(function(index, option)
                    {
                        var regExp = new RegExp(inputVal, 'i');
                        if(regExp.test(jQuery(option).text()))
                        {
                            jQuery(option).show();
                        }

                    });
                }
        });

        jQuery('#firstFilterSearch').keyup(function()
        {
            var searchArea = jQuery('#lstBox1');
            searchFirstList(jQuery(this).val(), searchArea);
        });
        
        // Function for Filtering
        function searchFirstList(inputVal, searchArea)
        {
                var allCells = $(searchArea).find('option');
                if(allCells.length > 0)
                {
                    var found = false;
                    allCells.each(function(index, option)
                    {
                        var regExp = new RegExp(inputVal, 'i');
                        if(regExp.test(jQuery(option).text()))
                        {
                            jQuery(option).show();
                        }
                        else
                        {
                            jQuery(option).hide();
                        }
                    });
                }
        }

        jQuery('#btnRight').click(function(e) {
            var selectedOpts = jQuery('#lstBox1 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }            
            $('#lstBox1 option:selected').remove().appendTo('#lstBox2');  
            //jQuery('#lstBox2').append(jQuery(selectedOpts).clone());
            //jQuery(selectedOpts).remove();
            e.preventDefault();
        });

        jQuery('#btnLeft').click(function(e) {
            var selectedOpts = jQuery('#lstBox2 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }
            jQuery('#lstBox1').append($(selectedOpts).clone());
            jQuery(selectedOpts).remove();
            e.preventDefault();
        });
    });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>