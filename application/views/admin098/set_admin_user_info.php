<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url()?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?=base_url()?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include('header_view.php'); ?>
<?php
	if(isset($user_info))
	{
		$userId = $user_info->userid;
		$firstname = $user_info->firstName;
                $lastname = $user_info->lastName;
		$email = $user_info->email;
                $roleid = $user_info->roleId;
		$phone = $user_info->phone;			
		$status = $user_info->status;	
	}
	else
	{
		$userId = 0;
		$firstname = '';
                $lastname = '';
		$email = '';
                $roleid = '';
		$phone ='';		
		$status = '';	
	}	
?>        
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid" id="mainGridContainer">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<?php include('left_view.php'); ?>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content" id="mainDiv">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid" >
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
                                            <!-- BEGIN STYLE CUSTOMIZER -->
                                            <!-- END BEGIN STYLE CUSTOMIZER --> 
                                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                                            <h3 class="page-title">
                                                    <?=$page_title?>
                                            </h3>
                                            <ul class="breadcrumb">							
                                                <li>
                                                    <i class="icon-home"></i>
                                                    <a href="<?=$this->config->config['admin_base_url']?>user/admin_users">Manage Admin Users</a> 
                                                    <i class="icon-angle-right"></i>
                                                </li>
                                                <li><a href="#"><?=$page_title?></a></li>
                                            </ul>
                                            <!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid profile">
            <div class="span12">
                <!--BEGIN TABS-->
                <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?=$page_title?></div>								
                        </div>
                        <div class="portlet-body form">
								<!-- BEGIN FORM-->
                              <form class="form-horizontal admin-user-form" name="myAdminUserFrm" id="myAdminUserFrm" method="post" action="<?=$this->config->config['admin_base_url']?>user/saveAdminUserInfo">
                                <input type="hidden" name="user_id" id="user_id" value="<?=$userId?>"/>                               
                                <div class="alert alert-error hide">
                                    <button class="close" data-hide="alert" type="button"></button>
                                    <span>Email is already exists.</span>
                                </div>
                                <div class="alert alert-success hide">
                                        <button class="close" type="button"  data-hide="alert"></button>
                                        User has been created successfully.
                                </div>                                  
                                <div class="control-group">
                                   <label class="control-label">First Name </label>
                                   <div class="controls">
                                       <div class="">
                                           <input type="text" name='txtFirstname' id='txtFirstname' value="<?=$firstname?>" class='m-wrap span6' placeholder='First Name'/>

                                       </div>
                                   </div>
                                </div>
                                <div class="control-group">
                                   <label class="control-label">Last Name </label>
                                   <div class="controls">
                                       <div class="">
                                           <input type="text" name='txtLastname' id='txtLastname' value="<?=$lastname?>" class='m-wrap span6' placeholder='Last Name'/>

                                       </div>
                                   </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label">Email<span class="required">*</span> </label>
                                    <div class="controls">
                                        <div class="">
                                           <input type="text" name='txtEmail' id='txtEmail' value="<?=$email?>" class='m-wrap span6' placeholder='Email'/>                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label">Phone Number </label>
                                    <div class="controls">
                                        <div class="">
                                            <input type="text" name='txtPhone' id='txtPhone' value="<?=$phone?>" class='m-wrap span6' placeholder='Phone' autocomplete="off"/>                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label">Role</label>
                                    <div class="controls">
                                        <div class="">                                                                 
                                            <select name="txtRole" id="txtRole">     
                                                <option value="">Select Role</option>
                                                <?php if(!empty($role_list)){ 
                                                    foreach($role_list as $role_row){ 
                                                        if($roleid==$role_row->roleId){
                                                            $sel='selected="selected"';
                                                        }else{
                                                            $sel='';
                                                        }
                                                    ?>
                                                <option value="<?php echo $role_row->roleId; ?>" <?php echo $sel; ?>><?php echo $role_row->roleName; ?></option>    
                                                <?php }} ?>
                                            </select>                                             
                                        </div>
                                    </div>
                                </div>
                                
                                <?php if($userId!=0){ ?>
                                <div class="control-group">
                                    <label class="control-label">Change Password</label>
                                    <div class="controls">                                            
                                        <label class="checkbox">
                                            <input type="checkbox" name="change_password_check" value="1" id="change_password" onclick="if(this.checked==true){ jQuery('#PasswordDiv,#ConfirmPasswordDiv').show();}else{ jQuery('#PasswordDiv,#ConfirmPasswordDiv').hide();}">
                                        </label>                                            
                                    </div>
                                </div> 
                                <?php } ?>

                                <div class="control-group" id="PasswordDiv" <?php if($userId!=0){ ?>style="display:none;" <?php } ?>>
                                    <label class="control-label ">Password<span class="required">*</span></label>
                                    <div class="controls">
                                        <div class="input-icon">                                                
                                            <input class="m-wrap span6" type="password" autocomplete="off" id="txtPassword"  placeholder="Password" name="txtPassword"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group" id="ConfirmPasswordDiv" <?php if($userId!=0){ ?>style="display:none;" <?php } ?>>
                                    <label class="control-label ">Confirm Password<span class="required">*</span></label>
                                    <div class="controls">
                                        <div class="input-icon">                                                
                                            <input class="m-wrap span6" type="password" autocomplete="off" placeholder="Confirm Password" id="txtCPassword" name="txtCPassword"/>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label">Status</label>
                                    <div class="controls">
                                        <div class="">                                                                 
                                            <select name="txtActive" id="txtActive">                                     
                                                <option value="Y" <?php if($status=='Y'){ ?> selected<?php } ?>>Active</option> 
                                                <option value="N" <?php if($status=='N'){ ?> selected<?php } ?>>Inactive</option>                                                 
                                            </select>                                             
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-actions">
                                    <button type="submit" id="register-submit-btn" class="btn green ">
                                    Save                                   
                                    </button>
                                    <a href="<?=$this->config->config['admin_base_url']?>user/admin_users" class="btn">Cancel</a>            
                                </div>
                              </form>
			      <!-- END FORM-->  
                                        </div>
                                </div>
                                <!--END TABS-->
                        </div>
                </div>
                <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER--> 
</div>
<!-- END PAGE -->    
</div>
<!-- END CONTAINER -->    
<?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>  
<script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script src="<?=base_url()?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/select2.min.js"></script>    
<script src="<?=base_url()?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>assets/scripts/app.js"></script> 
<script src="<?=base_url()?>assets/scripts/admin_user.js" type="text/javascript"></script> 
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           AdminUser.init();
        });				
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>