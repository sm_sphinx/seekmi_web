<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h3><?php echo $page_title; ?> Questionnaire</h3>
</div>
<?php
if ($question_row != '') {
    $questionId = $question_row->questionId;
    $qstname = $question_row->questionName;
    $qstnameindo = $question_row->questionNameIndonesion;
    $fieldType = $question_row->fieldType;
    $active = $question_row->active;
} else {
    $questionId = '';
    $qstname = '';
    $qstnameindo = '';
    $fieldType = '';
    $active = '';
}
?>
<form class="form-horizontal question-form" name="myQuestionFrm" id="myQuestionFrm" method="post" action="">
    <div class="modal-body">
        <input type="hidden" name="service_id" id="service_id" value="<?= $serviceId ?>"/>   
        <input type="hidden" name="question_id" id="question_id" value="<?= $questionId ?>"/>                                       
        <div class="alert alert-error hide">
            <button class="close" data-hide="alert" type="button"></button>
            <span>Question is already exists.</span>
        </div>
        <div class="alert alert-success hide">
            <button class="close" type="button"  data-hide="alert"></button>
            Question has been added successfully.
        </div>                                                                   
        <div class="control-group">
            <label class="control-label">Question Title<span class="required">*</span></label>
            <div class="controls">
                <div class="input-icon">
                    <input name="txtQuestionType" id="txtQuestionType" value="<?php echo $qstname; ?>" class="m-wrap span3"/>                                        
                </div>
            </div>
        </div> 
        <div class="control-group">
            <label class="control-label">Question Title(Indonesion)<span class="required">*</span></label>
            <div class="controls">
                <div class="input-icon">
                    <input name="txtQuestionTypeIndonesion" id="txtQuestionTypeIndonesion" value="<?php echo $qstnameindo; ?>" class="m-wrap span3"/>                                        
                </div>
            </div>
        </div> 
        <div class="control-group">
            <label class="control-label">Question Field Type<span class="required">*</span></label>
            <div class="controls">
                <div class="input-icon">
                    <select name='txtFieldType' id='txtFieldType' class='m-wrap span6' onchange="displayOptionDisplay(this.value);">
                        <option value="textbox" <?php if ($fieldType == 'textbox') { ?> selected="selected" <?php } ?>>Textbox(small content)</option>
                        <option value="textarea" <?php if ($fieldType == 'textarea') { ?> selected="selected" <?php } ?>>Textarea(large content)</option>
                        <option value="checkbox" <?php if ($fieldType == 'checkbox') { ?> selected="selected" <?php } ?>>Checkbox</option>
                        <option value="radio" <?php if ($fieldType == 'radio') { ?> selected="selected" <?php } ?>>Radio</option>
                        <option value="select" <?php if ($fieldType == 'select') { ?> selected="selected" <?php } ?>>Dropdown</option>
                    </select>
                </div>
            </div>
        </div>

        <input type="hidden" name="option_cnt" id="option_cnt" value="<?= $option_cnt ?>"/>            
        <div class="control-group" id="optionDiv" <?php if ($fieldType == 'textbox' || $fieldType == 'textarea' || $fieldType == '') { ?>style="display:none;" <?php } ?>>
            <label class="control-label">Options<span class="required">*</span></label>
            <div class="controls" id="optionHtmlDisplay">
                <?php
                if (!empty($option_list)) {
                    $m = 1;
                    foreach ($option_list as $option_row) {
                        ?>
                        <label class="input-icon" id="subOptionHtml<?= $m ?>">
                            <input type="hidden" name="txtOptionHid<?= $m ?>" id="txtOptionHid<?= $m ?>" value="<?= $option_row->optionId ?>"/>
                            <input name="txtOption<?= $m ?>" id="txtQuestionType<?= $m ?>" value="<?= $option_row->optionText ?>" class='m-wrap span2'/> &nbsp;[EN]
                            <input name="txtOptionIndonesion<?= $m ?>" id="txtQuestionType<?= $m ?>" value="<?= $option_row->optionTextIndonesion ?>" class='m-wrap span2' style="margin-top:5px"/> &nbsp;[ID]
                            <?php if ($m == 1) { ?>
                                <button type="button" class="btn green" onclick="displayOptionMore();">Add</button>
                            <?php } else { ?>
                                <button type="button" class="btn green" onclick="deleteOptionMore('<?= $m ?>');">Delete</button>
                            <?php } ?>
                        </label>
                        <?php
                        $m++;
                    }
                } else {
                    ?>
                    <label class="input-icon" id="subOptionHtml1">
                        <input type="hidden" name="txtOptionHid1" id="txtOptionHid1" value=""/>
                        <input name="txtOption1" id="txtQuestionType1" value="" class='m-wrap span2'/> &nbsp;[EN]
                        <input name="txtOptionIndonesion1" id="txtQuestionType1" value="" class='m-wrap span2' style="margin-top:5px"/> &nbsp;[ID]
                        <button type="button" class="btn green" onclick="displayOptionMore();">Add</button>
                    </label>
                <?php } ?>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Close</button>
        <button type="submit" class="btn red">Save</button>
    </div>
</form>
<script src="<?= base_url() ?>assets/scripts/services.js" type="text/javascript"></script> 
<script type="text/javascript">
                            Services.init();
</script>