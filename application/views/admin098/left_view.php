<!-- BEGIN SIDEBAR -->

<!-- BEGIN SIDEBAR MENU -->    
<ul class="page-sidebar-menu">        
    <li <?php if ($page_title == 'Dashboard') { ?>class="active"<?php } ?> style="padding-top:15px;">
        <a href="<?= $this->config->config['admin_base_url'] ?>user/index">
            <i class="icon-table"></i> 
            <span class="title">Dashboard</span>
            <?php if ($page_title == 'Dashboard') { ?>
                <span class="selected"></span>
            <?php } ?>					
        </a>					
    </li> 
    <?php if (in_array('manage_users', $this->session->userdata('user_rights'))) { ?>
        <li <?php if ($page_title == 'Manage Users' || $page_title == 'Edit User' || $page_title == 'Add User') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['admin_base_url'] ?>user/users">
                <i class="icon-user"></i> 
                <span class="title">Manage Users</span>
                <?php if ($page_title == 'Manage Users' || $page_title == 'Edit User' || $page_title == 'Add User') { ?>
                    <span class="selected"></span>
                <?php } ?>					
            </a>					
        </li>
    <?php } if (in_array('manage_new_professional', $this->session->userdata('user_rights'))) { ?>
        <li <?php if ($page_title == 'Manage New Professional' || $page_title == 'Edit New Professional' || $page_title == 'Add New Professional') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['admin_base_url'] ?>pros/newlist">
                <i class="icon-user"></i> 
                <span class="title">Manage New Professional</span>
                <?php if ($page_title == 'Manage New Professional' || $page_title == 'Edit New Professional' || $page_title == 'Add New Professional') { ?>
                    <span class="selected"></span>
                <?php } ?>					
            </a>					
        </li>
    <?php } if (in_array('manage_professional', $this->session->userdata('user_rights'))) { ?>
        <li <?php if ($page_title == 'Manage Professional' || $page_title == 'Edit Professional' || $page_title == 'Add Professional') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['admin_base_url'] ?>pros/lists">
                <i class="icon-user"></i> 
                <span class="title">Manage Professional</span>
                <?php if ($page_title == 'Manage Professional' || $page_title == 'Edit Professional' || $page_title == 'Add Professional') { ?>
                    <span class="selected"></span>
                <?php } ?>					
            </a>					
        </li>
    <?php } if (in_array('manage_job_request', $this->session->userdata('user_rights'))) { ?>
        <li <?php if ($page_title == 'Manage Job Requests' || $page_title == 'View Job Request') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['admin_base_url'] ?>service/jobs">
                <i class="icon-user"></i> 
                <span class="title">Manage Job Requests</span>
                <?php if ($page_title == 'Manage Job Requests' || $page_title == 'View Job Request') { ?>
                    <span class="selected"></span>
                <?php } ?>					
            </a>					
        </li>
    <?php } if (in_array('manage_categories', $this->session->userdata('user_rights'))) { ?>
        <li <?php if ($page_title == 'Manage Categories' || $page_title == 'Edit Category' || $page_title == 'Add Category') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['admin_base_url'] ?>service/categorylist">
                <i class="icon-table"></i> 
                <span class="title">Manage Categories</span>
                <?php if ($page_title == 'Manage Categories' || $page_title == 'Edit Category' || $page_title == 'Add Category') { ?>
                    <span class="selected"></span>
                <?php } ?>					
            </a>					
        </li>
    <?php } /* if(in_array('manage_services',$this->session->userdata('user_rights'))){ ?>
      <li <?php if ($page_title == 'Manage Services' || $page_title == 'Edit Service' || $page_title == 'Add Service') { ?>class="active"<?php } ?>>
      <a href="<?= $this->config->config['admin_base_url'] ?>service/lists">
      <i class="icon-table"></i>
      <span class="title">Manage Services</span>
      <?php if ($page_title == 'Manage Services' || $page_title == 'Edit Service' || $page_title == 'Add Service') { ?>
      <span class="selected"></span>
      <?php } ?>
      </a>
      </li>
      <?php } */ if (in_array('manage_subservices', $this->session->userdata('user_rights'))) { ?>
        <li <?php if ($page_title == 'Manage Services' || $page_title == 'Edit Service' || $page_title == 'Add Service') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['admin_base_url'] ?>service/sublist">
                <i class="icon-table"></i> 
                <span class="title">Manage Services</span>
                <?php if ($page_title == 'Manage Services' || $page_title == 'Edit Service' || $page_title == 'Add Service') { ?>
                    <span class="selected"></span>
                <?php } ?>					
            </a>					
        </li>
    <?php } if (in_array('suggested_service', $this->session->userdata('user_rights'))) { ?>
        <li <?php if ($page_title == 'Suggested Services') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['admin_base_url'] ?>service/suggested_list">
                <i class="icon-table"></i> 
                <span class="title">Suggested Services</span>
                <?php if ($page_title == 'Suggested Services') { ?>
                    <span class="selected"></span>
                <?php } ?>					
            </a>					
        </li>
    <?php } if (in_array('manage_admin', $this->session->userdata('user_rights'))) { ?>
        <li <?php if ($page_title == 'Manage Admin' || $page_title == 'Manage Admin Users' || $page_title == 'Manage Roles' || $page_title == 'Edit Admin User' || $page_title == 'Edit Role' || $page_title == 'Manage General Setting' || $page_title == 'Edit General Setting' || $page_title == 'Credit Log') { ?>class="open"<?php } ?>>
            <a href="javascript:;">
                <i class="icon-user"></i> 
                <span class="title">Manage Admin</span>
                <span class="arrow <?php if ($page_title == 'Manage Admin' || $page_title == 'Manage Admin Users' || $page_title == 'Manage Roles' || $page_title == 'Edit Admin User' || $page_title == 'Edit Role' ||$page_title == 'Manage General Setting' || $page_title == 'Edit General Setting' || $page_title == 'Credit Log') { ?>open<?php } ?>"></span>
                <?php if ($page_title == 'Manage Admin' || $page_title == 'Manage Admin Users' || $page_title == 'Manage Roles' || $page_title == 'Edit Admin User' || $page_title == 'Edit Role' || $page_title == 'Manage General Setting' || $page_title == 'Edit General Setting' || $page_title == 'Credit Log') { ?>
                    <span class="selected"></span>
                <?php } ?>                    
            </a>	
            <ul class="sub-menu" <?php if ($page_title == 'Manage Admin Users' || $page_title == 'Manage Roles' || $page_title == 'Edit Admin User' || $page_title == 'Edit Role' || $page_title == 'Manage General Setting' || $page_title == 'Edit General Setting' || $page_title == 'Credit Log') { ?>style='display: block;'<?php } ?>>
                <li <?php if ($page_title == 'Manage Admin Users' || $page_title == 'Edit Admin User') { ?>class="active"<?php } ?>>
                    <a href="<?= $this->config->config['admin_base_url'] ?>user/admin_users">Manage Admin users</a>
                </li>
                <li <?php if ($page_title == 'Manage Roles' || $page_title == 'Edit Role') { ?>class="active"<?php } ?>>
                    <a href="<?= $this->config->config['admin_base_url'] ?>user/roles">Manage Roles</a>
                </li>
                <li <?php if ($page_title == 'Manage General Setting' || $page_title == 'Edit General Setting') { ?>class="active"<?php } ?>>
                    <a href="<?= $this->config->config['admin_base_url'] ?>user/general_setting">Manage General Setting</a>
                </li>
                <li <?php if ($page_title == 'Credit Log') { ?>class="active"<?php } ?>>
                    <a href="<?= $this->config->config['admin_base_url'] ?>settings/credit_log">Credit Log</a>
                </li>
            </ul>
        </li>
    <?php
    }
    if (in_array('manage_package', $this->session->userdata('user_rights'))) {
        ?>
        <li <?php if ($page_title == 'Manage Packages' || $page_title == 'Edit Package' || $page_title == 'Add Package') { ?>class="active"<?php } ?>>
            <a href="<?= $this->config->config['admin_base_url'] ?>service/packagelist">
                <i class="icon-table"></i> 
                <span class="title">Manage Packages</span>
                <?php if ($page_title == 'Manage Packages' || $page_title == 'Edit Package' || $page_title == 'Add Package') { ?>
                    <span class="selected"></span>
    <?php } ?>					
            </a>					
        </li>
<?php } ?>  
    <li <?php if ($page_title == 'My Profile' || $page_title == 'Private Settings') { ?>class="active"<?php } ?>>
        <a href="javascript:;">
            <i class="icon-cogs"></i> 
            <span class="title">Settings</span>
            <span class="arrow <?php if ($page_title == 'My Profile' || $page_title == 'Private Settings') { ?>open<?php } ?>"></span>
            <?php if ($page_title == 'My Profile' || $page_title == 'Private Settings') { ?>
                <span class="selected"></span>
<?php } ?>                    
        </a>	
        <ul class="sub-menu" <?php if ($page_title == 'My Profile' || $page_title == 'Private Settings') { ?>style='display: block;'<?php } ?>>
            <li <?php if ($page_title == 'My Profile') { ?>class="active"<?php } ?>>
                <a href="<?= $this->config->config['admin_base_url'] ?>settings/my_profile">My Profile</a>
            </li>
            <?php /* <li <?php if ($page_title == 'Private Settings') { ?>class="active"<?php } ?>>
              <a href="<?= $this->config->config['admin_base_url'] ?>settings/private">Private Settings</a>
              </li> */ ?>
        </ul>
    </li>
</ul>
<!-- END SIDEBAR MENU -->

<!-- END SIDEBAR -->