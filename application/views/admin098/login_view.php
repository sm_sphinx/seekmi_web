<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= $this->config->config['base_url'] ?>assets/css/pages/login.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <img src="<?= base_url() ?>images/seekmilogo.png" alt="SeekMi"/>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <?php if (isset($_GET['pass']) && $_GET['pass'] == 'update') {
            ?>   <div style="color: red" >Password Updated Successfully.</div><?php }
        ?>
        <div style="color: red" id="email_not_exist" hidden="true">Email Not Exists</div>
        <form class="form-vertical login-form" action="" method="post">
            <h3 class="form-title">Login to your account</h3>
            <div class="alert alert-error hide">
                <button class="close" data-hide="alert"></button>
                <span>Enter any email and password</span>
            </div>
            <div class="alert alert-error1 hide">
                <button class="close" data-hide="alert"></button>
                <span>You have no account for this email/wrong password.</span>
            </div>
            <div class="alert alert-error2 hide">
                <button class="close" data-hide="alert"></button>
                <span>Your account not activate yet. Please check your mail for account activation.</span>
            </div>
            <div class="alert alert-error-forgot alert-success hide">
                <button class="close" data-hide="alert"></button>
                <span>Please check your email for reset your password.</span>
            </div>
            <div class="control-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="icon-user"></i>
                        <input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="icon-lock"></i>
                        <input class="m-wrap placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <!--<label class="checkbox">
                <input type="checkbox" name="remember" value="1"/> Remember me
                </label>-->
                <a id="forget_pass" onclick="forget_pass();" href="javascript:void(0);">Forgot Password</a>
                <button type="submit" class="btn green pull-right">
                    Login <i class="m-icon-swapright m-icon-white"></i>
                </button>            
            </div>			

        </form>

        <form class="form-vertical forget_pass-form hide" action="" method="post" >
            <div style="color: red" id="email_not_exist" hidden="true">Email Not Exists</div>
            <div style="color: red" id="success"></div>
            <h3 class="form-title">Forgot Passsword</h3>
            <div class="alert alert-error22 hide">
                <button class="close" data-hide="alert"></button>
                <span>You have no account for this email.</span>
            </div>
            <div class="alert alert-error21 hide">
                <button class="close" data-hide="alert"></button>
                <span>Your account not activate yet. Please check your mail for account activation.</span>
            </div>
            <div class="control-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="icon-user"></i>
                        <input class="m-wrap placeholder-no-fix" id="forget_email" type="text" autocomplete="off" placeholder="Email" name="email"/>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <!--<label class="checkbox">
                <input type="checkbox" name="remember" value="1"/> Remember me
                </label>-->
                <a href="<?= $this->config->config['admin_base_url'] ?>">Back To login</a>
                <button type="submit" class="btn green pull-right">
                    Send <i class="m-icon-swapright m-icon-white"></i>
                </button>            
            </div>			

        </form>
        <!-- END LOGIN FORM -->        


    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        <?= date('Y') ?> &copy; Seekmi Admin.
    </div>
    <!-- END COPYRIGHT -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <?php include('scripts_footer.php'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?= $this->config->config['base_url'] ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>assets/plugins/select2/select2.min.js"></script>     
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?= $this->config->config['base_url'] ?>assets/scripts/app.js" type="text/javascript"></script>
    <script src="<?= $this->config->config['base_url'] ?>assets/scripts/login.js" type="text/javascript"></script> 
    <script src="<?= $this->config->config['base_url'] ?>assets/scripts/forget_pass.js" type="text/javascript"></script> 
    <!-- END PAGE LEVEL SCRIPTS --> 
    <script>
                    jQuery(document).ready(function () {
                        $('.forget_pass-form').hide();
                        App.init();
                        Login.init();
                        ForgetPassword.init();

                    });
                    function forget_pass() {
                        $('.login-form').hide();
                        $('.forget_pass-form').show();

                    }
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>