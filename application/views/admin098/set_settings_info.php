<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <?php include('header_view.php'); ?>
    <?php
    if (isset($role_info)) {
        $roleId = $role_info->settingId;
        $rolename = $role_info->settingName;
    } else {
        $roleId = 0;
        $rolename = '';
    }
    ?>
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid" id="mainGridContainer">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
            <?php include('left_view.php'); ?>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content" id="mainDiv">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid" >
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END BEGIN STYLE CUSTOMIZER --> 
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            <?= $page_title ?>
                        </h3>
                        <ul class="breadcrumb">							
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= $this->config->config['admin_base_url'] ?>user/general_setting">Manage General Settings</a> 
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="#"><?= $page_title ?></a></li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid profile">
                    <div class="span12">
                        <!--BEGIN TABS-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-reorder"></i><?= $page_title ?></div>								
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal role-form" name="myRoleFrm" id="myRoleFrm" method="post" action="<?= $this->config->config['admin_base_url'] ?>user/saveSettingInfo" enctype="multipart/form-data">
                                    <input type="hidden" name="role_id" id="role_id" value="<?= $roleId ?>"/>                               
                                    <div class="alert alert-error hide">
                                        <button class="close" data-hide="alert" type="button"></button>
                                        <span>Role is already exists.</span>
                                    </div>
                                    <div class="alert alert-success hide">
                                        <button class="close" type="button"  data-hide="alert"></button>
                                        Role has been created successfully.
                                    </div>                                  
                                    <div class="control-group">
                                        <label class="control-label">Setting Name </label>
                                        <div class="controls">
                                            <div class="">
                                                <input type="text" name='txtRolename' id='txtRolename' value="<?= $rolename ?>" class='m-wrap span6' readonly="true"/>

                                            </div>
                                        </div>
                                    </div>

                                    <?php if ($role_info->settingType == 'image') { ?>
                                        <div class="control-group">
                                            <label class="control-label">Setting Image</label>
                                            <div class="controls">
                                                <div class="">
                                                    <input type="file" name='image_file' id='image_file' value="" class='span6'/>                     
                                                </div>
                                                <input type="hidden" name='hid_image' id='hid_image' value="<?= $role_info->settingVal ?>" class='span6' />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"></label>
                                            <div class="controls">
                                                <div class="">
                                                    <img id="display_image" src="<?= base_url() ?>setting_images/<?= $role_info->settingVal ?>" style="max-height: 90px;background-color: #999;"/>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="control-group">
                                            <label class="control-label">Setting Value</label>
                                            <div class="controls">
                                                <div class="">                                                                 
                                                    <input type="text" name='txtsettingVal' id='txtsettingVal' value="<?= $role_info->settingVal ?>" class='m-wrap span6' />                                            
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-actions">
                                        <button type="submit" id="register-submit-btn" class="btn green ">
                                            Save                                   
                                        </button>
                                        <a href="<?= $this->config->config['admin_base_url'] ?>user/general_setting" class="btn">Cancel</a>            
                                    </div>
                                </form>
                                <!-- END FORM-->  
                            </div>
                        </div>
                        <!--END TABS-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER--> 
        </div>
        <!-- END PAGE -->    
    </div>
    <!-- END CONTAINER -->    
    <?php include('footer_view.php'); ?>
    <?php include('scripts_footer.php'); ?>	
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>  
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
    <script src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>    
    <script src="<?= base_url() ?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?= base_url() ?>assets/scripts/app.js"></script> 
    <?php /* <script src="<?= base_url() ?>assets/scripts/roles.js" type="text/javascript"></script>  */ ?>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // initiate layout and plugins
            App.init();
            //   Role.init();
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>