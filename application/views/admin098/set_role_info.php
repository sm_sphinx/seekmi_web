<?php include('style_header.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url()?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link href="<?=base_url()?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include('header_view.php'); ?>
<?php
	if(isset($role_info))
	{
		$roleId = $role_info->roleId;
		$rolename = $role_info->roleName;               		
		$status = $role_info->status;
                $role_db_rights_arr=array();
                if(!empty($role_rights)){
                   foreach($role_rights as $role_rights_row){
                       $role_db_rights_arr[]=$role_rights_row->menuCode;
                   } 
                }
	}
	else
	{
		$roleId = 0;
		$rolename = '';                	
		$status = '';	
                $role_db_rights_arr=array();
	}
        $role_rights_arr=array('manage_users'=>'Manage Users','manage_new_professional'=>'Manage New Professional','manage_professional'=>'Manage Professional','manage_categories'=>'Manage Categories','manage_subservices'=>'Manage Services','manage_admin'=>'Manage Admin','manage_job_request'=>'Manage Job Requests','manage_package'=>'Manage Package');
?>        
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid" id="mainGridContainer">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<?php include('left_view.php'); ?>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content" id="mainDiv">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid" >
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
                                            <!-- BEGIN STYLE CUSTOMIZER -->
                                            <!-- END BEGIN STYLE CUSTOMIZER --> 
                                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                                            <h3 class="page-title">
                                                    <?=$page_title?>
                                            </h3>
                                            <ul class="breadcrumb">							
                                                <li>
                                                    <i class="icon-home"></i>
                                                    <a href="<?=$this->config->config['admin_base_url']?>user/roles">Manage Roles</a> 
                                                    <i class="icon-angle-right"></i>
                                                </li>
                                                <li><a href="#"><?=$page_title?></a></li>
                                            </ul>
                                            <!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid profile">
            <div class="span12">
                <!--BEGIN TABS-->
                <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?=$page_title?></div>								
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                              <form class="form-horizontal role-form" name="myRoleFrm" id="myRoleFrm" method="post" action="<?=$this->config->config['admin_base_url']?>user/saveRoleInfo">
                                <input type="hidden" name="role_id" id="role_id" value="<?=$roleId?>"/>                               
                                <div class="alert alert-error hide">
                                    <button class="close" data-hide="alert" type="button"></button>
                                    <span>Role is already exists.</span>
                                </div>
                                <div class="alert alert-success hide">
                                        <button class="close" type="button"  data-hide="alert"></button>
                                        Role has been created successfully.
                                </div>                                  
                                <div class="control-group">
                                   <label class="control-label">Role Name </label>
                                   <div class="controls">
                                       <div class="">
                                           <input type="text" name='txtRolename' id='txtRolename' value="<?=$rolename?>" class='m-wrap span6' placeholder='Role Name'/>

                                       </div>
                                   </div>
                                </div>
                                
                               <?php foreach($role_rights_arr as $key=>$value){ ?>
                                <div class="control-group">
                                    <label class="control-label"><?php echo $value; ?></label>
                                    <div class="controls">                                            
                                        <label class="checkbox">
                                            <input type="checkbox" name="txtMenucode[]" value="<?php echo $key; ?>" id="txtMenucode" <?php if(in_array($key,$role_db_rights_arr)){ ?> checked="checked"<?php } ?>>
                                        </label>                                            
                                    </div>
                                </div> 
                               <?php } ?>
                               
                                
                                <div class="control-group">
                                    <label class="control-label">Status</label>
                                    <div class="controls">
                                        <div class="">                                                                 
                                            <select name="txtActive" id="txtActive">                                     
                                                <option value="Y" <?php if($status=='Y'){ ?> selected<?php } ?>>Active</option> 
                                                <option value="N" <?php if($status=='N'){ ?> selected<?php } ?>>Inactive</option>                                                 
                                            </select>                                             
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-actions">
                                    <button type="submit" id="register-submit-btn" class="btn green ">
                                    Save                                   
                                    </button>
                                    <a href="<?=$this->config->config['admin_base_url']?>user/roles" class="btn">Cancel</a>            
                                </div>
                              </form>
			      <!-- END FORM-->  
                                        </div>
                                </div>
                                <!--END TABS-->
                        </div>
                </div>
                <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER--> 
</div>
<!-- END PAGE -->    
</div>
<!-- END CONTAINER -->    
<?php include('footer_view.php'); ?>
<?php include('scripts_footer.php'); ?>	
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>  
<script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script src="<?=base_url()?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/select2.min.js"></script>    
<script src="<?=base_url()?>assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script> 
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?=base_url()?>assets/scripts/app.js"></script> 
<script src="<?=base_url()?>assets/scripts/roles.js" type="text/javascript"></script> 
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           Role.init();
        });				
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>