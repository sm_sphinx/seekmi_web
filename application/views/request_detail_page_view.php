<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/work.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/quets.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/message.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/easy-responsive-tabs.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>
<div class="wrapper content">
<!--    <h1 class="body-text">
        View Request
    </h1>    -->
    <div>
    <a class="back" href="javascript:window.history.back();">← Back</a>
    <br/>
            <div data-section="request-status" class="alert-container alert-accordion">
<!--            <div data-status-dropdown-hover="" class="alert-note alert-dropdown alert-verified">
            <a href="#"><div class="quote-icon tt-credits mark"  aria-hidden="true">&nbsp;</div></a>
            <p>
                You have 0 credits.
                Send for 2 credits.
            </p>
            <div data-status-dropdown-content="" style="display:none;" class="icontent">
                All resume writing quotes cost
                2 credits. If your quote is not viewed
                within 48 hours, we'll automatically refund your quote.<a data-open-curtain="refund-policy-lbox" href="#">
                        Refund policy
                    </a></div>
            </div>-->
            <div class="alert-note alert-quote-count">
                <div class="form-alert-text">
                    <p><?=$quote_count?> quote have been submitted</p>
                    <div style="display: none;">
                        <?php if($project_owner_data->lastname!=''){
                            $lastname=$project_owner_data->lastname[0].".";
                        }else{
                            $lastname='';
                        }
                        echo $project_owner_data->firstname." ".$lastname; ?> would still like you to submit a quote.
                    </div>
                </div>
            </div>
            </div>
    
        <div id="parentHorizontalTab">
            <ul class="resp-tabs-list hor_1">
                <li>View Request</li>
                 <?php if($request_row->status!='Pending'){ ?> 
                  <li>Messages</li>      
                 <?php } ?>
            </ul>
            <div class="resp-tabs-container hor_1">
                <div>
                    <div class="work-request">
                        <div class="request-header">
                            <div class="request-name">
                               <h2>
                                   <?php if($project_owner_data->lastname!=''){
                                       $lastname=$project_owner_data->lastname[0].".";
                                   }else{
                                       $lastname='';
                                   }
                                   echo $project_owner_data->firstname." ".$lastname; ?>
                               </h2>
                           </div>
            <!--                    <div class="profile-map-container">
                            <div class="profile-container">
                                <img width="80" height="80" alt="profile photo" src="images/100x100.png">
                            </div>
                            <div class="map-container">
                               <img src="images/80s.png">
                            </div>
                            </div>-->
                       </div>



                        <div class="what-when-where-container">
                            <dl class="work-request-data">
                              <?php if(!empty($req_data)){ 
                                 foreach($req_data as $req_row){    
                                     //print_r($req_row);
                              ?> 
                               <dt class="request-title"><?=$req_row->questionName?></dt>
                               <dd class="request-info">
                               <?php if(!empty($req_option_data[$req_row->questionId])){ ?>
                                  <ul>
                                 <?php foreach($req_option_data[$req_row->questionId] as $req_option_row){   ?>
                                   <li class="list-bullet">
                                      <?php if(count($req_option_data[$req_row->questionId]) > 1){ ?>
                                   <div class="check"></div>
                                      <?php } ?>
                                    <?=$req_option_row['optionText']?>
                                    <?php if($req_option_row['optionExtra']){
                                         echo " (".$req_option_row['optionExtra'].")";
                                     }
                                     ?>                                     
                                   </li>
                                   <?php }?>
                                   </ul>
                                  <?php }else{ ?>
                                     <?php
                                     if($req_row->fieldType=='datepicker'){ 
                                         if($req_row->extras!=''){
                                             $ext_arr=array();
                                             $ext_arr=json_decode($req_row->extras);
                                             if($ext_arr->from!=''){
                                                 echo "From: ".$ext_arr->from;
                                             }
                                             if($ext_arr->to!=''){
                                                 echo "   To: ".$ext_arr->to;
                                             }
                                         }
                                         
                                     } ?>
                                  <?php } ?>
                                </dd>
                              <?php } ?>
                                <dt class="request-title">Request submitted</dt>
                                <dd class="request-info">
                                    At <?php echo date('h:ia',strtotime($project_owner_data->createdDate)); ?>
                                    on
                                    <?php echo date('m-d-y',strtotime($project_owner_data->createdDate)); ?>                            
                                </dd>
                              <?php } ?>  
                            </dl>
            <!--                 <div class="pq-invite">
                                <span>Not enough info?</span>
                                <a href="#" data-open="pq-question-input" class="pq-opener">
                                    Suggest a question</a>
                            </div>-->
            <div class="need-help-call-us-container">
                            <span class="need-help">
                                Need help quoting?
                            </span>
                            <span class="call-us">
                                Call us at (021) 4000-0289
                            </span>
                        </div>
                        </div>
                        
                    </div>
                    
                    
                    
                </div>
                <?php if($request_row->status!='Pending'){ ?> 
                <div>                    
                        <div class="messenger"><div class="bid-messaging">
                            <!--    <a class="bid-expand-messages" href="#">Show 2 previous messages</a>-->
                            <ul class="items" id="sent-msg-div">
                            <li class="item message-item" style="display: block;">
                                <?php if($request_row->userPhoto==''){ ?>
                                <img class="profile-picture" src="<?=$this->config->config['base_url']?>images/100x100.png">
                                <?php }else if($request_row->facebookProfileId!=''){ ?>
                                <img class="profile-picture" src="<?=$request_row->userPhoto?>">
                                <?php }else{ ?>
                                <img class="profile-picture" src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$request_row->userPhoto?>">
                                <?php } ?>
                                <div class="item-content">
                                    <time class="item-time"><?php echo get_timeago(strtotime($request_row->addedOn)); ?></time>
                                    <h4 class="item-author"><?php echo $request_row->firstname." ".$request_row->lastname; ?></h4>
                                    <div class="bid-message-body">
                                        <?php echo $request_row->quoteMessage; ?>
                                    </div>
                                </div>
                            </li>
                            <?php if(!empty($message_list)){ foreach($message_list as $message_row){ ?>
                            <li class="item message-item" style="display: block;">
                                <?php if($message_row->userPhoto==''){ ?>
                                <img class="profile-picture" src="<?=$this->config->config['base_url']?>images/100x100.png">
                                <?php }else if($message_row->facebookProfileId!=''){ ?>
                                <img class="profile-picture" src="<?=$message_row->userPhoto?>">
                                <?php }else{ ?>
                                <img class="profile-picture" src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$message_row->userPhoto?>">
                                <?php } ?>
                                <div class="item-content">
                                    <time class="item-time"><?php echo get_timeago(strtotime($message_row->createdOn)); ?></time>
                                    <h4 class="item-author"><?php echo $message_row->firstname." ".$message_row->lastname; ?></h4>
                                    <div class="bid-message-body">
                                        <?php echo $message_row->message; ?>
                                    </div>
                                </div>
                            </li>
                            <?php }} ?>
                            </ul>
                            <ul class="items" id="reply-msg-div">
                            <li class="item message-item" id="send-message-div">
                                <div data-locator="reply"></div>
                                <?php if($this->session->userdata('user_photo')==''){ ?>
                                    <img src="<?=$this->config->config['base_url']?>images/100x100.png" class="profile-picture">
                                <?php }else if($this->session->userdata('facebook_id')!=''){ ?>
                                    <img src="<?=$this->session->userdata('user_photo')?>" class="profile-picture">
                                <?php }else{ ?>
                                    <img src="<?=$this->config->config['base_url']?>user_images/thumb/<?=$this->session->userdata('user_photo')?>" class="profile-picture">
                                <?php } ?>   
                                <div class="item-content">        
                                    <h4 class="item-author"><?=$this->session->userdata('user_full_name')?></h4>
                                    <div class="composer">
                                        <form name="reply-msg-form" id="reply-msg-form">
                                            <input type="hidden" name="qid" value="<?=$request_row->requestId?>"/>
                                            <input type="hidden" name="msgFrom" value="<?=$this->session->userdata('userId')?>"/>
                                            <input type="hidden" name="msgTo" value="<?=$request_row->prosId?>"/>
                                            <textarea name="msg_content" id="msg_content"></textarea>
                                            <button class="blue bttn" type="submit">Send</button>
                                            <!--<span class="attach-file"><span class="icon-font-attach"></span> 
                                            <a href="#" data-action="add-attachment">Add Attachment</a></span>-->
<!--                                            <a href="javascript:void(0);" class="cancel cancel-message-div" data-action="cancel">Cancel</a>-->
                                            <ul></ul>
                                        </form>
                                    </div>
                                </div>
                            </li>
                            </ul>
                        </div>
                        </div>                                  
                </div>
                <?php } ?>         
            </div>
           </div>
        
      </div>
   
</div>    
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/easyResponsiveTabs.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1'
    });
    
    $("#hide,#hidePop").click(function(){
        $(".curtain").hide();
    });
    $("#show").click(function(){
        $(".curtain").show();
    });
    $("#quote-on-request").validate({
        rules: {            
            estimate_fixed_price_per_unit: {
                required: true,
                number:true
            },
            message: {
                required: true
            }
        }
    });  
    
    $("#reply-msg-form").validate({
        rules: {
            msg_content: {
                required: true
            }
        },
        submitHandler: function(form) {
            $.post(Host+'bid/reply_message', $("#reply-msg-form").serialize(), function(data) {
               $('#sent-msg-div').append(data);                             
               $("#reply-msg-form")[0].reset();
            });     
        }
    });
});

function showPriceDisplay(vvl){
    if(vvl=='hourly'){
        $('.hourly-price').show();
        $('.form-field-fixed-price').hide();
    }
    if(vvl=='fixed'){
        $('.hourly-price').hide();
        $('.form-field-fixed-price').show();
    }
    if(vvl=='no_price'){
        $('.hourly-price').hide();
        $('.form-field-fixed-price').hide();
    }
}

</script>
</body>
</html>

