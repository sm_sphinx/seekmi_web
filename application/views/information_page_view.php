<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

<div class="settings-navigation ">
    <div class="wrapper">
      <a class="back" href="<?=$this->config->config['base_url']?>profile/settings"> Settings</a>
      <h2>Account</h2>
    </div>
</div>
        
<div class="wrapper content settings">        
        <div class="dynamic-row">
            <div class="column-12"></div>
        </div>

        <div class="dynamic-row">
            <div class="column-4 picture">
            <?php if($user_data->userPhoto==''){ ?>
              <img data-component="profile-picture" src="<?=$this->config->config['base_url']?>images/img-profile-missing.png">
            <?php }else if($user_data->facebookProfileId!=''){ ?>
                <img data-component="profile-picture" src="<?=$user_data->userPhoto?>">
            <?php }else{ ?>
                <img data-component="profile-picture" src="<?=$this->config->config['base_url']?>user_images/<?=$user_data->userPhoto?>">
            <?php } ?> 
                
                 <a class="change-picture" data-action="upload" href="javascript:;" id="uploadBtn" accept="image/*" capture>                    
                    <span aria-hidden="true" class="icon-font icon-font-camera" id="uploadBtn"></span>
                    <br>
                    <span>add</span>
                 </a>
                <div id="msgBox"></div>
               
            </div>

            <div class="column-8 user-info">
                <h3>Account</h3>
                <ul>
                    <li>
                        <a href="<?=$this->config->config['base_url']?>profile/account_edit" title="edit name" class="primary user">
                            <?=$user_data->firstname?> <?=$user_data->lastname?>
                            <span class="edit">edit</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->config->config['base_url']?>profile/account_edit" title="edit email" class="primary email">
                            <?=$user_data->email?>
                            <span class="edit">edit</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=$this->config->config['base_url']?>profile/password" title="edit password" class="primary lock">
                            Password
                            <span class="edit">edit</span>
                        </a>
                    </li>
                    <?php if($this->session->userdata('user_type')=='professional'){ ?>
                    <li>
                        <a class="primary location" title="edit address" href="<?=$this->config->config['base_url']?>profile/services#basic_info">
                            <?php 
                            if($user_address->streetAddress!=''){
                                echo $user_address->streetAddress;
                            }
                            if($user_address->cityName!=''){
                                echo ", ".$user_address->cityName;
                            }
                            if($user_address->districtName!=''){
                                echo ", ".$user_address->districtName;
                            }
                            if($user_address->provName!=''){
                                echo ", ".$user_address->provName;
                            }
                            if($user_address->zipcode!=''){
                                echo " ".$user_address->zipcode;
                            }
                            ?>                            
                            <span class="edit">edit</span>
                        </a>
                    </li>
                    <li>
                        <a class="primary phone" title="edit phone" href="<?=$this->config->config['base_url']?>profile/services#basic_info">
                                <?php echo $user_data->phone; ?>
                            <span class="edit">edit</span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
                <hr>
                <div class="mobile-gap"></div>
                <ul>
                    <li>
                        <a href="<?=$this->config->config['base_url']?>profile/delete_account" title="delete account" class="delete-link primary cancel">
                            Delete account
                        </a>
                    </li>
                </ul>
               </div>
               <div class="mobile-gap">
             </div>
            </div>
        </div>   
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/SimpleAjaxUploader.js"></script>	
<script type="text/javascript">
$(document).ready(function () {
    $('.user_notification').click(function() {
        var val=''
        if(this.checked==true){
            val='Y';
        }else{
            val='N';
        }
        $.post(Host+'profile/update_notification', { type: this.id, val: val }, function(data) {            
        });  
    });
});

var btn = document.getElementById('uploadBtn')
msgBox = document.getElementById('msgBox');

var uploader = new ss.SimpleUpload({
  button: btn,
  url: Host+'profile/upload_image',
  name: 'uploadfile',
  hoverClass: 'hover',
  focusClass: 'focus',
  responseType: 'json',
  startXHR: function() {
     // progressOuter.style.display = 'block'; // make progress bar visible
      //this.setProgressBar( progressBar );
  },
  onSubmit: function() {
      msgBox.innerHTML = ''; // empty the message box
      btn.innerHTML = 'Uploading...'; // change button text to "Uploading..."
    },
  onComplete: function( filename, response ) {
     
      //btn.innerHTML = 'Choose Another File';
      //progressOuter.style.display = 'none'; // hide progress bar when upload is completed

      /*if ( response =='') {
          msgBox.innerHTML = 'Unable to upload file';
          return;
      }*/

      if (response.success == true ) {
          location.reload();
          //msgBox.innerHTML = '<strong>' + escapeTags( filename ) + '</strong>' + ' successfully uploaded.';          
      } else {
          if ( response.msg )  {
              
              msgBox.innerHTML = escapeTags( response.msg );

          } else {
              
              msgBox.innerHTML = 'An error occurred and the upload failed.';
          }
      }
    },
  onError: function() {
     
      //progressOuter.style.display = 'none';
      //msgBox.innerHTML = 'Unable to upload file';
    }
  });

</script>
</body>
</html>

