<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> <?= $page_title ?> - Seekmi</title>
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/icons.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/consume.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/core.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/zenbox.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/login.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/avenir-next.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/work.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= $this->config->config['base_url'] ?>css/quets.css">
        <link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
    </head>
    <body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
        <? include('header_view.php'); ?>

        <div data-section="page" class="content wrapper">
            <div class="alert-closable alert-hide quote-error">            
                    <div class="alert-error">
                        <p>
                             You don't have enough credit to submit this quote. Please buy credits.
                        </p>                        
                    </div>
             </div>
            <form accept-charset="ISO-8859-1" action="<?= $this->config->config['base_url'] ?>bid/create" method="post" name="quote-on-request" id="quote-on-request" novalidate>
                <input type="hidden" value="<?= $requestId ?>" name="req" id="req">  
                <input type="hidden" value="<?= $project_owner_data->serviceId ?>" name="service" id="service">  
                <div data-page="quote form">
                    <div data-section="quote-title">
                        <h1 class="body-text quote-accordeon-title">
                            <?php echo SUBMITQUOTE_SUBMIT_TEXT; ?>
                        </h1>
                    </div>
                    <div data-row="main" class="row quote-form-row">
                        <div data-section="quote" class="column-14 quote-form-left">
                            <div data-section="quote-status" class="alert-container alert-accordion">
                            </div>

                            <div class="price-form-wrapper">
                                <div data-section="quote-form-container" class="box box-primary">
                                    <div class="box-content quote-form">
                                        <div data-section="info" class="form-field info-block">
                                            <div class="service-info">
                                                <div class="profile-image-container">
                                                    <?php if ($this->session->userdata('user_photo') == '') { ?>
                                                        <img src="<?= $this->config->config['base_url'] ?>images/100x100.png" class="profile-image">
                                                    <?php } else if ($this->session->userdata('facebook_id') != '') { ?>
                                                        <img src="<?= $request_row->userPhoto ?>" class="profile-image">
                                                    <?php } else { ?>
                                                        <img src="<?= $this->config->config['base_url'] ?>user_images/thumb/<?= $request_row->userPhoto ?>" class="profile-image">
                                                    <?php } ?>                        
                                                </div>

                                                <div class="profile-text">
                                                    <p class="provider-name"> <?= $request_row->firstname ?> <?= $request_row->lastname ?> </p>
                                                    <span data-info="business-name" class="info business-name">
                                                        <?= $request_row->businessName ?>
                                                    </span>
                                                    <a data-info="website" class="info website"><?= $request_row->website ?></a>
                                                    <span data-info="phone" class="info phone">
                                                        <?= $request_row->phone ?>
                                                    </span>
                                                </div>
                                                <div class="verifications"></div>
                                            </div>
                                        </div>

                                        <fieldset class="estimate-fields" data-section="price">
                                            <label for="estimate_type"><?php echo LABEL_PRICE; ?></label>
                                            <div class="form-field form-field-price-type">
                                                <select class="price-type" tabindex="100" name="estimate_type" id="estimate_type" onchange="showPriceDisplay(this.value);">
                                                    <option selected="selected" value="fixed"><?php echo LABEL_FIXEDPRICE; ?></option>
                                                    <option value="hourly"><?php echo LABEL_HOURLYRATE; ?></option>
                                                    <option value="no_price"><?php echo LABEL_NEEDMORE; ?></option>
                                                </select>
                                            </div>                 
                                            <div class="form-field price-input price-input-fixed-price form-field-fixed-price">
                                                <div class="input-prepend">
                                                    <span class="add-on">Rp</span><input type="text" class="price-field" tabindex="101" name="estimate_fixed_price_per_unit" id="estimate_fixed_price_per_unit" >
                                                </div>
                                            </div>                                
                                            <div style="display: none;" class="form-field hourly-price">
                                                <div class="input-prepend input-append">
                                                    <span class="add-on">Rp</span>
                                                    <input type="number" data-title="A price per hour" data-estimate="hourly" data-input="hourly rate" class="hourly-price-input" tabindex="102" name="estimate_hourly_price_per_unit" id="estimate_hourly_price_per_unit" min="1"><span class="add-on">per hour</span>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset class="message-fields">
                                            <div class="form-field message-field">
                                                <label for="message"><?php echo LABEL_MESSAGE; ?></label>
                                                <textarea data-input="message" class="input-full" tabindex="103" name="message" id="message"></textarea>
                                                <p style="display:none;" data-component="char-count" class="character-count">
                                                    <span class="number"></span>
                                                    <?php echo SUBMITQUOTE_CHARTOO_TEXT; ?>
                                                </p>
                                            </div>
                                        </fieldset>

                                        <div class="toolbar" data-section="toolbar">                                
                                            <a data-curtain-trigger="tips" class="pro-tips" id="show" href="javascript:void(0);">
                                                <?php echo SUBMITQUOTE_TIPS_TEXT; ?>
                                            </a>
                                            <div class="attachment-list" data-section="attachment-list"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button tabindex="104" class="bttn blue" type="submit">Submit Quote</button>
                        </div>

                        <div class="column-9 push-1 right-column">
                            <div data-section="request-status" class="alert-container alert-accordion">
                                <div class="alert-note alert-quote-count">
                                    <div class="form-alert-text">
                                        <p><?= $quote_count ?> <?php echo SUBMITQUOTE_SUBMITQUOTE_TEXT; ?></p>
                                        <div style="display: none;">
                                            <?php
                                            if ($project_owner_data->lastname != '') {
                                                $lastname = $project_owner_data->lastname[0] . ".";
                                            } else {
                                                $lastname = '';
                                            }
                                            echo $project_owner_data->firstname . " " . $lastname;
                                            ?><?php echo SUBMITQUOTE_WOULD_TEXT; ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="work-request">
                                <div class="request-header">
                                    <div class="request-name">
                                        <h2>
                                            <?php
                                            if ($project_owner_data->lastname != '') {
                                                $lastname = $project_owner_data->lastname[0] . ".";
                                            } else {
                                                $lastname = '';
                                            }
                                            echo $project_owner_data->firstname . " " . $lastname;
                                            ?>
                                        </h2>
                                    </div>
                                </div>

                                <div class="what-when-where-container">
                                    <dl class="work-request-data">
                                            <?php
                                            if (!empty($req_data)) {
                                                foreach ($req_data as $req_row) {
                                                    ?> 
                                                <dt class="request-title"><?= $req_row->questionName ?></dt>
                                                <dd class="request-info">
                                                            <?php if (!empty($req_option_data[$req_row->questionId])) { ?>
                                                        <ul>
                                                                <?php foreach ($req_option_data[$req_row->questionId] as $req_option_row) { ?>
                                                                <li class="list-bullet">
                                                                    <?php if (count($req_option_data[$req_row->questionId]) > 1) { ?>
                                                                        <div class="check"></div>
                                                                    <?php } ?>
                                                                <?= $req_option_row['optionText'] ?>
                                                                <?php
                                                                if ($req_option_row['optionExtra']) {
                                                                    echo " (" . $req_option_row['optionExtra'] . ")";
                                                                }
                                                                ?>
                                                                </li>
            <?php } ?>
                                                        </ul>
                                                    <?php } ?>
                                                </dd>
                                            <?php } ?>
                                            <dt class="request-title"><?php echo LABEL_REQUESTSUBMIT; ?></dt>
                                            <dd class="request-info">
                                                At <?php echo date('h:ia', strtotime($project_owner_data->createdDate)); ?>
                                                on
                                                <?php echo date('m-d-y', strtotime($project_owner_data->createdDate)); ?>                            
                                            </dd>
<?php } ?>  
                                    </dl>
                                    <?php /*<div class="pq-invite">
                                        <span><?php echo LABEL_NOTENOUGH; ?></span>
                                        <a href="#" data-open="pq-question-input" class="pq-opener">
<?php echo LABEL_SUGGEST; ?></a>
                                    </div>*/ ?>
                                </div>
                            </div>



                            <div class="need-help-call-us-container">
                                <span class="need-help">
<?php echo SUBMITQUOTE_NEED_TEXT; ?>
                                </span>
                                <span class="call-us">
<?php echo LABEL_CALLUS; ?>(021) 4000-0289
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>       
        </div>
        <div class="curtain"  style="display:none;"><div class="box box-modal tips-modal curtain-inner" data-curtain="tips">
                <div class="box-header">
                    <h1>
                        <?php echo SUBMITQUOTE_TIPSTO_TEXT;?>
                    </h1>
                    <a data-curtain-builtin-close-button="" data-curtain-close="" id="hide" class="modal-close" href="#">×</a></div>
                <div class="box-content body-text">
                    <p>
                        <?php echo SUBMITQUOTE_YOURMESSAGE_TEXT;?>
                    </p>

                    <ul>
                        <li><?php echo SUBMITQUOTE_GREET_TEXT;?></li>
                        <li><?php echo SUBMITQUOTE_TALK_TEXT;?></li>
                        <li><?php echo SUBMITQUOTE_DESCRIBE_TEXT;?></li>
                        <li><?php echo SUBMITQUOTE_ENCOURAGE_TEXT;?></li>
                    </ul>

                    <p class="cta">
                        <button data-curtain-close="" class="bttn" id="hidePop">
                            <?php echo BUTTON_RETURNTOQUOTE;?>
                        </button>
                    </p>
                </div>
            </div></div>    

        <? include('footer_view.php'); ?>
        <script src="<?= $this->config->config['base_url'] ?>js/fbds.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/insight.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/conversion_async.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/quant.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/bat.js" type="text/javascript"></script>
        <script src="<?= $this->config->config['base_url'] ?>js/tag.js" async=""></script>
        <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>js/jquery.js"></script>
        <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>js/jquery-validate.js"></script>
        <script type="text/javascript" src="<?= $this->config->config['base_url'] ?>js/jquery.price_format.2.0.js"></script>

        <script type="text/javascript">
                                $(document).ready(function () {
                                    $("#hide,#hidePop").click(function () {
                                        $(".curtain").hide();
                                    });
                                    $("#show").click(function () {
                                        $(".curtain").show();
                                    });
                                    $("#quote-on-request").validate({
                                        rules: {
                                            estimate_fixed_price_per_unit: {
                                                required: true
                                            },
                                            message: {
                                                required: true
                                            }
                                        },
                                        submitHandler: function(form) {
                                             $.ajax({
                                                type: 'POST',
                                                url: Host+'profile/check_credit',
                                                data:"userId=<?php echo $userId; ?>&serviceId=<?php echo $project_owner_data->serviceId; ?>",
                                                success: function (data) {
                                                       if(data=='false'){
                                                           $('.quote-error').show();
                                                       }else{
                                                           $('.quote-error').hide();
                                                           form.submit();
                                                       }
                                                }
                                             });   
                                        }
                                    });
                                });

                                function showPriceDisplay(vvl) {
                                    if (vvl == 'hourly') {
                                        $('.hourly-price').show();
                                        $('.form-field-fixed-price').hide();
                                    }
                                    if (vvl == 'fixed') {
                                        $('.hourly-price').hide();
                                        $('.form-field-fixed-price').show();
                                    }
                                    if (vvl == 'no_price') {
                                        $('.hourly-price').hide();
                                        $('.form-field-fixed-price').hide();
                                    }
                                }
								$('#estimate_fixed_price_per_unit').priceFormat({
    prefix: '',
    centsSeparator: ',',
    limit: 11,
	centsLimit: 3
});

        </script>
    </body>
</html>

