<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$page_title?> - Seekmi</title>
<style type="text/css">
.glorious-header {
    background: none!important;
    border-bottom: none!important;
}

.piede {
    background: none repeat scroll 0 0 #ffffff;
    border-top: 1px solid #e8e8e8;
    color: #000000;
    margin-top: 0;
    padding: 5px 0!important;
}
</style>
<meta content="<?=$this->config->config['base_url']?>" property="og:url"/>
<meta content="Seekmi | Get Things Done" property="og:title"/>
<meta content="Seekmi" property="og:site_name"/>
<meta property="og:description" content="Get things done by hiring experienced local service professionals." />
<meta property="og:image" content="<?=$this->config->config['base_url']?>images/LogoSymbolWordSquare.png"/>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['main_base_url']?>css1/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['main_base_url']?>css1/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['main_base_url']?>css1/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['main_base_url']?>css1/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['main_base_url']?>css1/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['main_base_url']?>css1/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['main_base_url']?>css1/signup1.css">
 <style type="text/css">
     a.back{        
        display: inline-block;        
        font-style: normal;
        font-weight: normal;
        min-width: 1em;
        text-align: center;
        text-decoration: inherit;
        color: #4092f1 !important;
        text-decoration: none
    }
    h3 a{
       color: #4092f1 !important; 
    }
 </style> 
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
    <?php
                //echo get_cookie('qtrans_front_language');
                if (get_cookie('language') == 'english') {
                    include($_SERVER['DOCUMENT_ROOT'].'/config/eng_constants.php');
                } else {
                    include($_SERVER['DOCUMENT_ROOT'].'/config/ind_constants.php');
                }                
                ?>
<?php include('common_view.php'); ?>
<div class="glorious-header glorious-header1" data-section="header">

<div class="wrapper"><div class="row header-row">
<div class="header" style="margin-bottom:10px;">
    <a class="logo" href="<?=$this->config->config['base_url']?>">
        <img alt="Seekmi" src="<?=$this->config->config['base_url']?>images/seekmilogo.png">
    </a>    
</div>


    <div class="wrapper content">
    <div>
   <br/><br/>

                <div id="main_div_1" style="display:block">
                    
            <form novalidate class="box ng-valid ng-dirty" name="services">
                <div class="box-header">
                    <a href="<?=$this->config->config['base_url']?>" class="back">← <?php echo LABEL_RETURN; ?></a>
                 <br/>
                </div>
                <div class="box-content">
                <br/> <center> 
                <?php if($status=='invalid'){ ?>
                <h3><?php echo CONFIRM_LINK_EXPIRED_TEXT; ?> <?php echo LOGIN_OR_TEXT; ?> <?php echo CONFIRM_ALREADY_ACTIVATED_TEXT; ?></h3>
                <?php } else if($status=='already'){ ?>
                <h3><?php echo CONFIRM_ALREADY_ACTIVATED_TEXT; ?>!</h3>
                <?php }else{ ?>
                <h3><?php echo CONFIRM_CONGRATS_TEXT; ?>! <?php echo CONFIRM_ACTIVATED_SUCCESS_TEXT; ?>!</h3>
                <?php } ?>
                <br/><br/><br/><br/>
                <b><?php echo ARE_YOU_SERVICE_PROVIDER; ?></b><br/>
                <?php echo CONFIRM_PROS_NEW_CLIENT_TEXT; ?><br/>
                <a href="<?=$this->config->config['base_url']?>pros/register/" target="_blank" class="back"><?php echo REGISTER_CREATE_TEXT; ?></a>
                <br/> 
                <br/>
                <br/>
                <b><?php echo CONFIRM_SHARE_TEXT; ?>:</b><br/></center>
                <div class="piede" style="border:none;">
                <center>
                <div class="social">
                    <ul class="social-media">
                    <li>
                        <a target="_blank" href="https://www.facebook.com/seekmi">
                            <span class="facebook"></span>
                        </a>
                    </li>
                     <li>
                        <a target="_blank" href="https://twitter.com/SeekmiApp">
                            <span class="twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://instagram.com/seekmi/">
                            <span class="instagram"></span>
                        </a>
                    </li>   
                    <li>
                        <a target="_blank" href="https://www.linkedin.com/company/seekmi">
                            <span class="linkedin"></span>
                        </a>
                    </li>
                </ul>
                </div></center></div>
              </div>
              <div class="form-field form-field-nav box-footer" style="visibility:hidden;">
                <button class="bttn" type="button" onclick="setPageView(1,2,'next')">Continue →</button>
                <a class="back" onclick="setPageView(1,0,'back')" href="javascript:void(0);">← Back</a>
             </div>
            </form>
        </div>
<!--   <ul class="reasons">
            <li class="clients">
                <h4>Find new customers and grow your business</h4>
            </li>
            <li class="control-requests">
                <h4>Control the requests you receive</h4>
            </li>
            <li class="leads">
                <h4>Free to sign up, pay as you go</h4>
            </li>
        </ul>-->
    </div>
    </div>

<div id="feedback-tab"><a data-show-feedback="" href="#">Contact Us</a></div>
      
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script src="<?=$this->config->config['base_url']?>js/jquery.js" type="text/javascript"></script>

<script type="text/javascript">
(function(){
      var del = 200;
      $('.icontent').hide().prev('a').hover(function(){
        $(this).next('.icontent').stop('fx', true).slideToggle(del);
      });
    })();	
$(document).ready(function(){
    $("#hide").click(function(){
        $(".curtain").hide();
    });
    $("#show,#show1").click(function(){
        $(".curtain").show();
    });
});
</script>    

</body>
</html>
      
      
      
