<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>

<div class="wrapper content">
<a class="bttn tiny" href="<?=$this->config->config['base_url']?>profile/settings">
    <span aria-hidden="true" class="icon-font icon-font-left-dir"></span>Back to Settings
</a>

<div class="dynamic-row settings-second-level">
    <div class="column-7">
        <div class="pod pod-primary">
            <div class="pod-header">
                <h2><?php echo DEL_ACOOUNT_TEXT;?></h2>
            </div>
            <div class="pod-content body-text">
                <p><?php echo DEL_AREYOU_TEXT;?></p>
                <p>
                    <?php echo DEL_LOSECONTENT_TEXT;?>
                </p>
                <p>
                    <?php echo DEL_RECIVEMAIL_TEXT;?>
                    <a href="<?=$this->config->config['base_url']?>profile/notifications"><?php echo DEL_CHANGE_NOTIFICATION_TEXT;?></a>.
                </p>
                <p>
                    <?php echo DEL_HELP_TEXT;?>
                    <?php
                    if(get_cookie('language') === 'english'){ 
                        $lang='en'; 
                     }else{
                         $lang='id';
                     }
                     ?>
                     <a title="Seekmi Contact Us" href="<?php echo $this->config->config['main_base_url']; ?>app/<?php echo $lang; ?>/contact-us/">
                   <?php echo DEL_SUPPORT_EMAIL;?> </a> <?php echo DEL_CALL_TEXT;?>
                </p>
                <form accept-charset="ISO-8859-1" class="good-form large" name="user-delete" id="user-delete" method="post" action="<?=$this->config->config['base_url']?>profile/delete_user_account">
                    <input type="hidden" name="userId" value="<?=$user_data->userId?>"/>
                    <fieldset>
                        <div class="form-field">
                           <label class="" for="reason">
                              <?php echo DEL_REASON_TEXT;?>
                           </label>
                           <input type="text" class="" id="reason" name="reason" value="">
                        </div>
                        <div class="form-field">
                            <input type="checkbox" class="" value="1" id="confirm" name="confirm" onclick="activateBtn(this.checked);">
                           <label class="inline-label" for="confirm">
                                <?php echo DEL_CONFIRM_TEXT.$user_data->firstname.$user_data->lastname.DEL_CONFIRM_REMAIN_TEXT;?>
                           </label>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-field">
                            <a class="bttn blue medium disabled" id="deleteAccountBtn" onclick="(function(btn) { if ($(btn).hasClass('disabled')) { return false; } var frm = $(btn).closest('form'); if(frm.length == 1){  frm.submit();}})(this); return false;" href="#submit">
                                 <span><?php echo BUTTON_DELETE_ACC;?> »</span>
                             </a>
                            <input type="submit" value="" name="__unused__submit__" style="visibility:collapse;width:1px;height:1px;display:block;float:none;padding:0;margin-left:-9999px;position:absolute;">
                            <a class="form-link" href="<?=$this->config->config['base_url']?>profile/account_edit"><?php echo BUTTON_CANCEL_TEXT;?></a>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<? include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript">
function activateBtn(obj)
{   
     if(obj==true){
         $('#deleteAccountBtn').removeClass('disabled');
     }else{
         $('#deleteAccountBtn').addClass('disabled');
     }
}
</script>
</body>
</html>

