<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
.glorious-header1{
    background: none repeat scroll 0 0 #fff!important;
    border-bottom: 1px solid #e6e6e6!important;
}

a:link, .pseudo-link:link, .stealth-link:link {
    color: #2962a8!important;
    text-decoration: none;
}
</style>
<title>Business Development Associate</title>
<meta content="<?=$this->config->config['base_url']?>job/" property="og:url"/>
<meta content="Seekmi | Business Development Associate" property="og:title"/>
<meta content="Seekmi" property="og:site_name"/>
<meta property="og:description" content="Get things done by hiring experienced local service professionals." />
<meta property="og:image" content="<?=$this->config->config['base_url']?>images/LogoSymbolWordSquare.png"/>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/terms.css">
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid box-shadow multiple-backgrounds">
    <?php include('common_view.php'); ?>
<div class="glorious-header glorious-header1" data-section="header">
<div class="wrapper"><div class="row header-row">
<div class="header-logo">
<a href="<?=$this->config->config['base_url']?>"><img src="<?=$this->config->config['base_url']?>images/seekmilogo1.png" alt="Seekmi"></a></div>
<div class="header-middle-container"></div>
</div></div></div>

       
<div class="wrapper text">

    <div class="row">
        <div class="column-24 position">
            <br>
            <h1 class="title">
                Business Development Associate
                <div class="tack-stamp inline"></div>
            </h1>
                 </div>
    </div>

  
<b>Company</b><br/>

Seekmi<br/><br/>

<b>Background</b><br/>

Want to work for a exciting, fun, and fast-growing start-up?<br/><br/>
We are looking for exceptional people to help us grow our company into Asia’s next biggest thing! If you are smart, hard-working, and ambitious, this could be your opportunity to shine!<br/><br/>
Seekmi is the premiere Service Marketplace in Asia. We help simplify the way people find local service providers such as fitness instructors, handymen, language tutors, and more. Seekmi is part of the prominent EMTEK Group, Indonesia’s largest and most successful media holding group, which runs SCTV, Indosiar, Liputan6.com, and more.<br/><br/>
We are looking for a <b>Business Development Associate </b> based in Jakarta to start immediately.<br/><br/>



<b>The Role</b><br/><br/>

The Business Development Associate will actively find and on board new service professionals onto the Seekmi system. As part of the role, the Business Develoment Associate will be responsible for researching prospective service professionals, building and cultivating relationships, and onboarding these service professionals onto our system. Strong sales and business development skills are a must.
To prepare for meetings, the Business Development Officer must also be able to create Power Point presentations, sales materials, and newsletters.
In addition, the role will involve account management, project management, and cost management.
The individual must be detail oriented, extremely proactive, and able to take initiative. University degree is preferred with 2-5 years of sales/partnership experience.
<br/>

<br/>
<hr/>
<br/>

<b>Experience, Skills and Attributes</b><br/><br/>

<ul class="bullet-point-section">
<li><p>Strong sales and business development experience is a must</p></li>
<li><p>Account management and administrative skills (detailed oriented & organized)</p></li>
<li><p>Ability to network and generate leads (obtain relevant contacts, organize and schedule meetings).</p></li>
<li><p>Responsible for implementing sales / partnership campaigns from start to finish</p></li>
<li><p>Proficiency in Microsoft Office required.  Ability to produce client deliverables, including reports and presentations using MS PowerPoint, Word, and Excel.</p></li>
<li><p>Proven track record of supporting multiple accounts</p></li>
<li><p>Comfortable navigating between operations, sales, and project management teams</p></li>
<li><p>Proficiency at written and verbal communication in both English and Bahasa Indonesia is required</p></li>

</ul>
<br/><br/>
</div>
<?php include('footer_view.php'); ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script src="<?=$this->config->config['base_url']?>js/jquery.js" type="text/javascript"></script> 
</body>
</html>

