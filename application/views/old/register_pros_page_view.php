<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="mainimage" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$page_title?></title>
<meta content="<?=$this->config->config['base_url']?>pros/register/<?=$catId?>" property="og:url"/>
<meta content="<?=$page_title?>" property="og:title"/>
<meta content="SeekMi" property="og:site_name"/>
<meta property="og:description" content="Get things done by hiring experienced local service professionals." />
<meta property="og:image" content="<?=$this->config->config['base_url']?>images/seekmilogo1.png"/>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/signup.css">
 <style type="text/css">
    .help-block {
    color: #d51818;
    display: block;
    margin-bottom: 10px;
    margin-top: 5px;
    font-size: 11.2px;
   }
   .wrapper.content {
    margin: 63px auto !important;
    min-height: 420px;
}
   #services\[\]-error,#preferences\[\]-error{
       float:left;
       font-size: 12px;
       margin-left: -28px;
       margin-top: -37px;
   }
   .has-error .checkbox, .has-error .checkbox-inline, .has-error label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label {
    color: #d51818;
   }
   .has-error input,.has-error select,.has-error textarea{
      border-color: #a94442;
      box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset; 
   }
 </style>
</head>
<body class="primo primo-responsive primo-fluid box-shadow multiple-backgrounds">
<div class="glorious-header glorious-header1 inn" data-section="header">
<div class="wrapper">
    <div class="row header-row">
        <div class="header" style="margin-bottom:10px;">
    <a class="logo" href="<?=$this->config->config['base_url']?>"><img alt="SeekMi" src="<?=$this->config->config['base_url']?>images/seekmilogo.png"></a>
<!--    <div class="navigation"><a href="/pros/why-join">Why join Seekmi?</a></div>-->
    </div>
  </div>
</div>
</div>
<div class="wrapper content">
    <div>
        <form name="pros_register_form" method="POST" id="pros_register_form" action="<?=$this->config->config['base_url']?>pros/register_submit">
<!--            <h1 class="header-copy" style="">
                Last week, <strong class="ng-binding">318</strong> customers
                came to SeekMi looking for <strong class="ng-binding">teachers</strong>!
            </h1>           -->
            <input type="hidden" name="category" value="<?=$catId?>"/>

            <div id="main_div_1" class="box" style="display:block">
            
                <div class="box-header">

                    <h2>Pilih dibawah ini yang merupakan jasa yang Anda sediakan?</h2>
                    <h3>Beritahu kami pekerjaan Anda sehingga kami bisa temukan ke klien yang tepat.</h3>

                </div>
                <div class="box-content">
                    <fieldset data-fieldset="services">
                        <div class="form-field form-field-services">
                            <div class="checkbox-section" id="service_class">
                                <!-- Repeat: service in services -->
                                <?php if($services!=''){
                                    foreach($services as $service_row){
                                        if($service_row->serviceName=='Other'){
                               ?>                                
                                <div role="presentation">
                                    <span role="option" class="select-option faux-label" aria-selected="false">
                                        <input type="checkbox" class="radio" name="services[]" value="<?php echo $service_row->serviceId; ?>">
                                        <input type="text" id="" class="" name="serviceOtherText_<?php echo $service_row->serviceId; ?>" tabindex="0" placeholder="Other">
                                    </span>
                                </div>
                                <?php }else{ ?>
                                <div role="presentation">
                                    <label aria-selected="false" role="option" class="select-option">
                                        <input class="services_cls" id="services" type="checkbox" name="services[]" class="radio" value="<?php echo $service_row->serviceId; ?>">
                                        <?php echo $service_row->serviceName; ?>
                                    </label>
                                </div>
                                <?php } }} ?>                                
                                <!--<div role="presentation" class="form-group">
                                    <span aria-selected="false" role="option" class="select-option">
                                        <input type="checkbox" name="services[]" value="other" class="radio">
                                        <input type="text" name="service_other" placeholder="Other">
                                    </span>
                                </div>-->
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="form-field form-field-nav box-footer">

                    <button class="bttn next" type="button" onclick="getSubServices();">Lanjut →</button>
<!--                    <a class="back" href="<?=$this->config->config['base_url']?>#professionalDiv">← Back</a>-->
                    <a class="back" href="javascript:window.history.back();">← Kembali</a>

                 
                </div>            
        </div>
           
        <div class="box" id="main_div_6" style="display: none;">            
                <div class="box-header">
                    <h2>Apakah Anda menyediakan jasa lain yang berkaitan dibawah ini?</h2>
                    <h3>
                        Beritahu kami pekerjaan Anda sehingga bisa kami temukan ke klien yang tepat.
                    </h3>
                       <h3 style="color:#000;">Step 2 of 6</h3>
                </div>
                <div class="box-content">
                    <div class="delayed-fujimoto-loading" data-element-loading="" style="display: none;"></div>
                    <fieldset data-fieldset="suggested-services" data-element-suggested-services="" style="">
                        <div class="form-field form-field-services">
                            <div class="checkbox-section" id="subServiceDiv">                               
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="form-field form-field-nav box-footer">
                    <button class="bttn" type="button" onclick="setPageView(6,2,'next')">Lanjut →</button>
                    <a class="back" href="javascript:void(0);" onclick="setPageView(6,1,'back')">← Kembali</a>
                </div>            
        </div>
            
        <div id="main_div_2" class="box account" style="display: none;">            
                <div class="box-header">
                    <h2>Bagaimana cara kami menghubungi Anda?</h2>
                    <h3>
                        Beritahu kami dimana kami bisa mengirimkan permintaan pelanggan untuk layanan Anda.
                    </h3>
                     <h3 style="color:#000;">Step 3 of 6</h3>
                </div>
                <div class="box-content">
                    <div class="conditions">                      
                        <p>
                            *Note: Untuk perusahaan, silahkan masukkan pihak yang berwenang dari perusahaan Anda untuk menerima permintaan pekerjaan dari pelanggan yang akan dikirimkan oleh Seekmi. 
                        </p><br/>
                    </div>
                    <fieldset>
                        <div class="dynamic-row">
                            <div class="form-field-name-segment form-field form-field" expr="account.firstName" field="">
                                <div>
                                <label for="first-name">Nama Depan</label>
                                <input type="text" minlength="2" name="firstName" id="first-name">
                                </div>
                            </div>
                            <div class="form-field-name-segment form-field form-field" expr="account.lastName" field="">
                                <div>
                                <label for="last-name">Nama Belakang</label>
                                <input type="text" minlength="2" required="" name="lastName" id="last-name">
                                </div>
                            </div>
                        </div>
                        <div class="dynamic-row">
                            <div class="form-field-email form-field form-field" expr="account.usr_email" field="">
                                <div>
                                <label for="usr-email">Alamat Email</label>
                                <input type="email" name="usr_email" id="usr_email">
                                </div>
                            </div>
                            <div class="form-field-phone form-field form-field" expr="account.phone" field="">
                                <div>
                                <label for="phone">Nomor Telepon</label>
                                <input type="text" pattern="[0-9]*" name="phone" id="phone">
                                </div>
                            </div>
                        </div>
                        <div class="dynamic-row">
                            <div class="form-field-email form-field form-field" expr="account.password" field="">
                                <div>
                                <label for="password">Password</label>
                                <input type="password" minlength="5" name="password" id="password">
                                </div>
                            </div>
                            <div class="form-field-phone form-field form-field" expr="account.confpassword" field="">
                                <div>
                                <label for="confirm-password">Ulangi Password</label>
                                <input type="password" minlength="5" name="confirmPassword" id="confirmPassword">
                                </div>
                            </div>
                        </div>
                        <div class="form-field-sms-preference form-field form-field" expr="account.sms" field="">
                            <div>
                            <label class="inline">
                                <input type="checkbox" name="sms_notification" value="1">
                                Beritahu saya melalui SMS apabila ada permintaan pekerjaan  dan pesan baru
                            </label>
                            </div>
                        </div>
                    </fieldset>                    
                </div>
                <div class="form-field form-field-nav box-footer">                                  
                        <button class="bttn" type="button" onclick="setPageView(2,3,'next')">Lanjut →</button>
                        <a class="back" href="javascript:void(0);" onclick="setPageView(2,6,'back')">← Kembali</a>  
                </div>            
        </div>

        <div id="main_div_3" class="box address" style="display: none;">           
                <div class="box-header">

                   <h2>Dimana lokasi Anda?</h2>
                    <h3>Apabila tidak ada alamat kantor, gunakan alamat rumah Anda.</h3>
                     <h3 style="color:#000;">Step 4 of 6</h3>

                </div>
                <div class="box-content">
                    <fieldset>
                        <div class="form-field-address-1 form-field form-field" expr="address.street" field="" style="width:100%;">
                            <div>
                            <label for="street">Jalan</label>
                            <input type="text" required="" name="street" id="street">
                           </div>
                        </div>
<!--                        <div class="form-field-address-2 form-field form-field" field="">
                            <div>
                            <label for="unit">Suite/apt</label>
                            <input type="text" name="unit" id="unit">
                            </div>
                        </div>-->
                        <div class="form-field-city form-field form-field" field="" style="width:50%;">
                            <div>
                            <label for="province">Provinsi</label>
                            <select name="province" id="province" onchange="ajaxcity(this.value)">
                                <option value="">--Select--</option>
                                <?php 
                                if($province_list!=''){
                                    foreach ($province_list as $province_row){
                                        echo '<option value="'.$province_row->provId.'">'.$province_row->provName.'</option>';
                                    }
                                }
                                ?>
                            </select>
                            </div>
                        </div>                        
                        <div class="form-field-city form-field form-field" expr="address.city" field="" style="width:50%;">
                            <div>
                            <label for="city">Kota</label> 
                            <select name="city" id="cityList" onchange="ajaxdistrict(this.value)">
                                <option value="">--Select--</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-field-state form-field form-field" field="" style="width:50%;">
                            <div>
                            <label for="district">Kabupaten</label>
                            <select name="district" id="districtList">
                                <option value="">--Select--</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-field-zip form-field form-field" expr="address.zip" field="" style="width:50%;">
                            <div>
                            <label for="zip">Kode Pos</label>
                            <input type="text" pattern="[0-9]*" required="" name="zip" id="zip">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Kerahasiaan</legend>
                        <div class="form-field-privacy form-field form-field" expr="address.privacy" field="">
                            <div>
                            <label class="inline">
                                <input type="radio" value="2" name="privacy" checked="">
                                Tampilkan hanya nama provinsi dan kota ke pelanggan
                            </label>
                            <label class="inline">
                                <input type="radio" value="1"name="privacy">
                                Tampilkan informasi alamat lengkap ke pelanggan
                            </label>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="form-field form-field-nav box-footer">                    
                    <button class="bttn" type="button" onclick="setPageView(3,4,'next')">Lanjut →</button>
                    <a class="back"  href="javascript:void(0);" onclick="setPageView(3,2,'back')">← Kembali</a>
               </div>          
        </div>

        <div id="main_div_4" class="box travel-preferences" style="display: none;">            
                <div class="box-header">

                  <h2>Dimana Anda melakukan bisnis Anda?</h2>
                    <h3>Bantu kami menemukan ke klien yang tepat dengan memberitahu kami dimana Anda bekerja.</h3>
                     <h3 style="color:#000;">Step 5 of 6</h3>
                  
                </div>
                <div class="box-content">
                    <fieldset>
                        <div class="form-field form-field-travel-preferences">
                            <div class="checkbox-section">
                                <div role="presentation" checkbox="">
                                 <label aria-selected="false" role="option" class="select-option">
                                     <input type="checkbox" name="preferences[]" class="radio tocustomer_check" value="travel_tocustomer" onclick="checkDisable(this.checked,'tocustomer_check');">
                                  <span><span>
                                    Kami mengunjungi pelanggan kami
                                  </span></span>
                                 </label>
                                </div>
                                <div role="presentation" checkbox="">
                                    <label aria-selected="false" role="option" class="select-option">
                                        <input type="checkbox" name="preferences[]" class="radio toprovider_check" value="travel_toprovider" onclick="checkDisable(this.checked,'toprovider_check');">
                                        <span><span>
                                            Pelanggan kami menggunjungi kami
                                        </span></span>
                                    </label>
                                </div>
                                <div role="presentation" checkbox="">
                                    <label aria-selected="false" role="option" class="select-option">
                                        <input type="checkbox" name="preferences[]" class="radio remote_check" value="travel_remote" onclick="checkDisable(this.checked,'remote_check');">
                                        <span><span>
                                            Bukan keduanya (hanya melalui telepon dan internet)
                                        </span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="travel_tocustomerDiv" style="display: none;">
                        <div class="form-field form-field-map">
                            <!--<img width="680" height="338" src="about:blank" radius="service.travel.distance" zip="service.address.zip">-->
                        </div>
                        <div class="form-field-travel-distance form-field form-field" expr="travel.distance" field="">
                            <div>
                            <label>
                                Seberapa jauh Anda bisa mengunjungi pelanggan?
                                <select name="distance">
                                    <option value="1">1 kilometer</option>
                                    <option value="2">2 kilometer</option>
                                    <option value="3">3 kilometer</option>
                                    <option value="5">5 kilometer</option>
                                    <option value="10">10 kilometer</option>
                                    <option value="20">20 kilometer</option>
                                    <option value="30" selected="selected">30 kilometer</option>
                                    <option value="40">40 kilometer</option>
                                    <option value="50">50 kilometer</option>
                                    <option value="100">100 kilometer</option>
                                </select>
                            </label>
                        </div></div>
                    </fieldset>
                </div>
                <div class="form-field form-field-nav box-footer">                    
                    <button class="bttn" type="button" onclick="setPageView(4,5,'next')">Lanjut →</button>
                    <a class="back" href="javascript:void(0);" onclick="setPageView(4,3,'back')">← Kembali</a>
                </div>
        </div>

        <div id="main_div_5" class="box profile" style="display: none;">           
                <div class="box-header">

                    <h2>Jelaskan bisnis Anda.</h2>
                    <h3>Beritahukan tentang bisnis Anda ke pelanggan dan jasa layanan apa yang Anda sediakan.</h3>
                     <h3 style="color:#000;">Step 6 of 6</h3>

                </div>
                <div class="box-content">
                    <fieldset>
                        <div class="form-field-business-name form-field form-field" expr="profile.name" field="">
                            <div>
                            <label for="business-name">Nama Bisnis</label>
                            <input type="text" maxlength="255" required="" name="business_name" id="business-name">
                            <span class="subtext-form">
                                Apabila bisnis Anda tidak bernama, gunakan sesuatu yang mendeskripsikan bisnis Anda seperti “Rita Bersihin Rumah”
                            </span>
                            </div>
                        </div>
                        <div class="form-field-website form-field form-field" expr="profile.website" field="">
                            <div>
                            <label for="website">Website</label>
                            <input type="text" name="website" id="website">
                            <span class="subtext-form">
                                Opsional
                            </span>       
                            </div>
                        </div>
                        <div class="form-field-description form-field form-field" expr="profile.description" field="">
                            <div>
                            <label for="description">
                                Keterangan Perusahaan
                            </label>
                            <textarea minlength="50" no-contact="" name="description" id="description"></textarea>
                            <span class="subtext-form">

                                Jelaskan tentang bisnis Anda dan kehebatannya
                                <a onclick="toggleVisibility();" href="javascript:;">(contoh)</a>
                                <br>(minimal 50 karakter)

                            </span>
                            </div>
                        </div>
                        <div class="form-field form-field-description-example" style="display: none;">
                            <p>
                                Saya seorang photografer produk dan portrait yang 
                                berlokasi di Jakarta Selatan. Karir saya dimulai sewaktu 
                                saya duduk dibangku sekolah dan memotret para siswa 
                                untuk buku tahunan. Semenjak itu saya mendedikasikan 
                                pekerjaaan saya untuk menangkap keindahan dan energi 
                                tentang orang dan pekerjaannya.
                            </p>
                        </div>
                        <div class="form-field form-field-description-example" style="display: none;">
                            <p>
                                Saya menjadi seorang personal trainer yang bisa membantu 
                                mentransformasikan hidup seseorang dan saya menginginkan 
                                Anda mencapai lebih dari yang Anda imipikan. Saya mengerti 
                                sulitnya menyempatkan waktu dan mencari motivasi, maka dari 
                                itu saya akan mendorong Anda untuk mewujudkan apa yang Anda impikan.
                            </p>
                        </div>
                    </fieldset>
                </div>
                <div class="form-field form-field-nav box-footer">
                    <div class="nav-container" role="presentation">                      
                        <button  class="bttn" type="submit">
                           <span>Daftar</span>                           
                        </button>
                        <a class="back" href="javascript:void(0);" onclick="setPageView(5,4,'back')">← Kembali</a>
                    </div>   
                     <div class="conditions">
                       <!--<p>
                            By clicking "Sign Up", you are indicating that you have
                            read and agreed to the <a target="_blank" href="#">terms of use</a>.
                        </p>-->
                        <p>
                            Layanan operator SMS dan Data Anda akan terdaftar ke pesan 
                            singkat kami. Anda tidak diharuskan untuk menyetujui untuk 
                            menerima pesan teks sebagai syarat menggunakan layanan Seekmi.
                        </p>
                    </div>
               </div>
        </div>

        

        <ul class="reasons">
            <li class="clients clients1">
                <h4>Dapatkan klien baru dan kembangkan bisnis Anda</h4>
            </li>
            <li class="control-requests control-requests1">
                <h4>Kontrol dan saring semua permintaan yang Anda terima</h4>
            </li>
            <li class="leads leads1">
                <h4>Daftar dan gunakan layanan kami secara gratis</h4>
            </li>
        </ul>
        </form>
    </div>
</div>
<div id="feedback-tab"><a data-show-feedback="" href="#">Contact Us</a></div>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.validate.js"></script>
<script type="text/javascript">
function setPageView(cur_page,tar_page,state){
	//alert(cur_page+' '+tar_page+' '+state);
	if(state=="next"){
            var form = $("#pros_register_form");
            var validator = form.validate({
                    errorElement: 'span',
                    errorClass: 'help-block',                    
                    highlight: function(element, errorClass, validClass) {
                            $(element).closest('.form-field').addClass("has-error");
                    },
                    unhighlight: function(element, errorClass, validClass) {
                            $(element).closest('.form-field').removeClass("has-error");
                    },
                    rules: {                        
                         'services[]': {                            
                            required: true
                         },
                         firstName:{
                            required: true 
                         },
                         lastName:{
                            required: true 
                         },
                         usr_email:{
                            required: true,
                            email:true,
                            remote: "<?=$this->config->config['base_url']?>user/check_email_exists"
                         },
                         phone:{
                            required: true 
                         },
                         password:{
                            required: true,
                            minlength: 5
                         },
                         confirmPassword:{
                            required: true,
                            equalTo: "#password"
                         },        
                         street:{
                            required: true
                         },
                         province:{
                            required: true
                         },
                         city:{
                            required: true
                         },
                         district:{
                            required: true
                         },
                         zip:{
                            required: true
                         },
                         'preferences[]':{
                            required: true
                         },
                         business_name:{
                            required: true
                         },
                         description:{
                            required: true
                         }
                    },
                    messages:{
                           'services[]': "Please select at least one service",
                           usr_email: {                               
                                remote: "Sorry, that email is already taken"
                           },
                           'preferences[]': "Please select at least one way that you meet clients"
                           
                    }
            });
            $('#usr_email').blur(function() {
                $('#usr_email').val($.trim($('#usr_email').val()));
                validator.element('#usr_email');
            });
            if (form.valid() === true){
		jQuery('#main_div_'+cur_page).hide();
		jQuery('#main_div_'+tar_page).show();
            }
	}
	else if(state=="back"){
		//alert(cur_page+' '+tar_page+' '+state);
		if(tar_page==0){
                    window.location.href="<?=$this->config->config['base_url']?>pros/welcome/<?=$catId?>";
		}else{
                    jQuery('#main_div_'+cur_page).hide();
                    jQuery('#main_div_'+tar_page).show();
		}
		
			
	}
	else{
	}
}

function showMiles(obj){
    if(obj==true){
        $('#travel_tocustomerDiv').show();
        $('.remote_check').attr('checked',false);
    }else{
        $('#travel_tocustomerDiv').hide();
    }
}

function toggleVisibility(){
    $('.form-field-description-example').toggle();    
}

function ajaxcity(cid){
    
    $.post('<?=$this->config->config['base_url']?>pros/getCityList', { id:cid }, function(data) {
        $('#cityList').html(data);
        if(cid==''){
            $('#districtList').html(data);
        }
    });   
}

function ajaxdistrict(cid){
    
    $.post('<?=$this->config->config['base_url']?>pros/getDistrictList', { id:cid }, function(data) {
        $('#districtList').html(data);
    });   
}

function getSubServices()
{  
   var selectedGroups  = new Array();
   $("#service_class input[name^'services[]']:checked").each(function() {     
        selectedGroups.push($(this).val());
   });
    $.post('<?=$this->config->config['base_url']?>pros/getSubservices', { services:selectedGroups }, function(data) {
        if(data!=''){
            $('#subServiceDiv').html(data);
            setPageView(1,6,'next');
        }else{
            setPageView(1,2,'next');
        }
    });  
}

function checkDisable(chk,cls){
    if(cls=='remote_check'){
        if(chk==true){
            $('.tocustomer_check,.toprovider_check').attr('checked',false);
            $('#travel_tocustomerDiv').hide();
        }
    }
    
    if(cls=='toprovider_check'){
        if(chk==true){
            $('.remote_check').attr('checked',false);            
        }
    }
    
    if(cls=='tocustomer_check'){
        if(chk==true){
            $('#travel_tocustomerDiv').show();
            $('.remote_check').attr('checked',false);
        }else{
            $('#travel_tocustomerDiv').hide();
        }
    }
}
</script>
</body>
</html>
      
      
     