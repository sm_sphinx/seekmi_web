<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->

<head>

<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Seekmi - Accomplish your personal projects</title>
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds ng-scope">
<div class="glorious-header" data-section="header">
<div class="wrapper"><div class="row header-row">
<div class="header-logo">
<a href="<?=$this->config->config['base_url']?>"><img src="<?=$this->config->config['base_url']?>images/Pintack_orange_152x29beta.png" alt="Seekmi "></a></div>
<div class="header-middle-container"></div>
</div></div></div>

<div data-overview="" class="overview ng-scope" ng-controller="SlidesController">

      
    <div class="wrapper content">
   <h1 style="padding-bottom:20px;" class="title-wrapper dynamic-row step-2">
           Coming soon
        </h1>
        
    

 <div class="dynamic-row step-1">
                <div style="float:left;" class="image">
                <img width="270" alt="" src="<?=$this->config->config['base_url']?>images/apple_watch.jpg"> </div>
                     <div style="float:left;" class="text">
                <img width="380" alt="" src="<?=$this->config->config['base_url']?>images/iphone_6plus.jpg"> </div>
                     <div style="float:left;" class="image">
                <img width="280" alt="" src="<?=$this->config->config['base_url']?>images/samsung_s6.jpg"> </div>
               
            </div>  </div>
            
            <h1 class="title-wrapper">Sign up today and enter for your chance to win</h1>
            
            <div style="padding:5px;" class="bottom-link-wrapper">
            <a href="#">
               <div class="icon-font icon-font-star"></div> 1 of 2  Apple watches
            </a>
            <span class="separator">
                |
            </span>
            <a href="#" id="register-instead">
               <div class="icon-font icon-font-star"></div> 1 of 4  iPhone 6 Plus
            </a>
             <span class="separator">
                |
            </span>
            <a href="#" id="register-instead">
               <div class="icon-font icon-font-star"></div> 1 of 4 Samsung s6
            </a>
        </div>
        
        <div class="wrapper content login-wrapper">
    <div class="dynamic-row">

      
        
        
                <div class="box">
            
<form novalidate id="login" name="Sign up with Email" method="post" action="#" accept-charset="ISO-8859-1">
              
                <fieldset>
                   
                     
                        <div style="margin-right: 14px;" class="login-with-facebook-wrapper">
         
            <a class="bttn facebook-bttn" href="<?=$this->config->config['base_url']?>customer/fb_login">
                <div class="icon-font icon-font-facebook"></div>
                <span class="separator">
                    |
                </span>
                Sign up with Facebook
            </a>
        </div>
      <!--   <div class="login-with-facebook-wrapper" style="margin-top:10px;">
         
            <a style="background-color:#ce1126;" onclick="TT.facebookLogin({target: this, permissions: 'email'}); return false;" class="bttn facebook-bttn" href="#">
                <div class="icon-font-gplus"></div>
                <span class="separator">
                    |
                </span>
                Sign up with Google
            </a>
        </div>-->
        <h3 class="or">
                or
            </h3>
         <div class="form-field">
                        <button type="button" class="bttn blue submit-bttn" id="show" tabindex="103">Sign up with Email</button>

                    </div>
        
          
                </fieldset>
            
</form>

        </div>
        <h1 class="title-wrapper"> 
        Are you a professional?
Get customer job request</h1>

<div class="login-with-facebook-wrapper">
          
            <a class="bttn facebook-bttn" href="<?=$this->config->config['base_url']?>pros/welcome">
               
               Sign up as Professional
            </a>
        </div>



            

        </div>
    </div><br/><br/>
    <center><span class="subtitle">Seekmi helps you hire professionals
At a price that’s right for you</span></center>

<br/><br/>


    <div class="how-works full-block">
        <div class="wrapper wrapper-title">
            <h2>We help you hire experienced professionals at a price that's right for you</h2>
            <span class="subtitle">How does Seekmi  help?</span>
        </div>
        <div class="wrapper">
            <div class="dynamic-row step-1">
                <div class="image">
                    <img src="<?=$this->config->config['base_url']?>images/step-1.jpg">
                </div>
                <div class="text">
                    <h3>1. Get introduced to pros</h3>
                    <p>
                        Tell us about your needs and we'll introduce you to several experienced
                        professionals in your area who are ready to complete your project. </p>
                </div>
            </div>
            <div class="dynamic-row step-2">
                <div class="image">
                    <img src="<?=$this->config->config['base_url']?>images/step-2.jpg">
                </div>
                <div class="text">
                    <h3>2. Compare professionals</h3>
                    <p>Within hours, interested and available professionals will send you custom
                     quotes. Each quote includes:
                    </p>
                    <div class="dynamic-row">
                        <ul class="column-6">
                            <li>Price estimate</li>
                            <li class="reviews">Customer reviews</li>
                            <li class="contact">Contact info</li>
                        </ul>
                        <ul class="column-6">
                            <li class="message">Personalized message</li>
                            <li class="profile">Business profile</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="dynamic-row step-3">
                <div class="image">
                    <img src="<?=$this->config->config['base_url']?>images/step-3.jpg">
                </div>
                <div class="text">
                    <h3>3. Hire the right pro</h3>
                    <p>When you're ready, hire an experienced professional at a price that's right
                    for you.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="states full-block">
        <div class="wrapper">
            <h2>Find us in all <?=count($province_list)?> province</h2>

                            <ul>
                    <?php if($province_list!=''){ 
                        foreach($province_list as $province_row){
                        ?>
                    <li><a href="#"><?=$province_row->provinceName?></a></li>   
                   <?php }} ?>                                    
                     </ul>
                    </div>
    </div>


</div> 
 
<div class="wrapper content login-wrapper">
    <div class="dynamic-row">
                <div class="box">
              
                <fieldset>
                     
                 <div style="margin-right: 14px;" class="login-with-facebook-wrapper">         
                    <a href="<?=$this->config->config['base_url']?>customer/fb_login" class="bttn facebook-bttn">
                        <div class="icon-font icon-font-facebook"></div>
                        <span class="separator">
                            |
                        </span>
                        Sign up with Facebook
                    </a>
                  </div>
      <!--   <div class="login-with-facebook-wrapper" style="margin-top:10px;">
         
            <a style="background-color:#ce1126;" onclick="TT.facebookLogin({target: this, permissions: 'email'}); return false;" class="bttn facebook-bttn" href="#">
                <div class="icon-font-gplus"></div>
                <span class="separator">
                    |
                </span>
                Sign up with Google
            </a>
        </div>-->
        <h3 class="or">
                or
            </h3>
            <div class="form-field">
              <button type="button" class="bttn blue submit-bttn" id="show1" tabindex="103">Sign up with Email</button>

                    </div>
        
          
                </fieldset>


        </div>
       


        </div>
    </div>

<div class="piede"><div class="wrapper"><div class="dynamic-row navigation">
<center>

<div class="column-3"><div class="copyright">
        <p><a href="<?=$this->config->config['base_url']?>" class="logo1">Seekmi </a></p>
        <p class="disclaimer">© 2015 Seekmi , Inc.</p>
        <a href="#" >Privacy Policy</a><a href="#" class="last">Terms of Use</a>
</div></div>

<div class="column-3" style="height:150px;">&nbsp;</div>
<div class="column-3" style="height:150px;">&nbsp;</div>

<div class="column-3" style="margin-left:0px;"><div class="social">
 <ul class="social-media">
    <li>
        <a href="http://www.facebook.com/seekmi" target="_blank">
            <span class="facebook"></span>
        </a>
    </li>
    <li>
        <a href="#" target="_blank">
            <span class="gplus"></span>
        </a>
    </li>
    <li>
        <a href="#" target="_blank">
            <span class="twitter"></span>
        </a>
    </li>
    <li>
        <a href="#" target="_blank">
            <span class="blog"></span>
        </a>
    </li>
    <li>
        <a href="#" target="_blank">
            <span class="pinterest"></span>
        </a>
    </li>
</ul>
</div></div>


</center>

</div></div></div>
<div style="display:none;" class="curtain"><div data-curtain="tips" class="box box-modal tips-modal curtain-inner">
    <div class="box-header">
        <h1>
            Sign Up
        </h1>
    <a href="#" class="modal-close" id="hide" data-curtain-close="" data-curtain-builtin-close-button="">×</a></div>
    <div class="box-content body-text">
      <div class="box">
            
<form novalidate="" id="register" name="register" method="post" action="<?=$this->config->config['base_url']?>customer/register_submit" accept-charset="ISO-8859-1">
         <fieldset>
                    <ul>
                        <ul class="dynamic-row">
                            <li class="column-6">
                                <div class="form-field">
                                    <label for="usr_first_name">First name</label>
                                    <input type="text" id="usr_first_name" name="usr_first_name" tabindex="100">
                                </div>
                            </li>
                            <li class="column-6">
                                <div class="form-field">
                                    <label for="usr_last_name">Last name</label>
                                    <input type="text" id="usr_last_name" name="usr_last_name" tabindex="101">
                                </div>
                            </li>
                        </ul>
                        <div class="form-field">
                            <label for="usr_email">Email address</label>
                            <input type="text" id="usr_email" name="usr_email" tabindex="102">
                        </div>
<!--                        <div class="form-field">
                            <label for="usr_password">Password</label>
                            <input type="password" id="usr_password" name="usr_password" tabindex="103">
                        </div>-->
                    </ul>
                </fieldset>       

                <fieldset>
                    <div class="form-field">
                        <button type="submit" class="bttn blue submit-bttn" tabindex="104">Signup</button>

                    </div>
                </fieldset>

            </form>

        </div>
    </div>
</div></div>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script src="<?=$this->config->config['base_url']?>js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
(function(){
      var del = 200;
      $('.icontent').hide().prev('a').hover(function(){
        $(this).next('.icontent').stop('fx', true).slideToggle(del);
      });
    })();	
$(document).ready(function(){
    $("#hide").click(function(){
        $(".curtain").hide();        
    });
    $("#show,#show1").click(function(){
        $(".curtain").show();
    });
    
    var validator = $("#register").validate({
        rules: {
            usr_first_name: {
                required: true,
                minlength: 2
            },
            usr_last_name: {
                required: true,
                minlength: 2
            },
            usr_email: {
                required: true,
                email: true,
                remote: "<?=$this->config->config['base_url']?>customer/check_email_exists"
            }
        },
        messages: {
            usr_first_name: {
                required: "Please enter your first name",
                minlength: "Too short"
            },
            usr_last_name: {
                required: "Please enter your last name",
                minlength: "Too short"
            },
            usr_email: {
                required: "You must enter an email",
                email: "Please enter a valid email",
                remote: "Sorry, that email is already taken"
            }
        }
    });

    $('#usr_email').blur(function() {
        $('#usr_email').val($.trim($('#usr_email').val()));
        validator.element('#usr_email');
    });
});
</script>    

</body>
</html>
      
      
      
