<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Seekmi- Accomplish your personal projects</title>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/icons.css">

<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/signup.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/land.css">

</head>

<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds ng-scope landing-page">

<div class="glorious-header" data-section="header">
<div class="wrapper"><div class="row header-row">
<div class="header-logo">
<a href="<?=$this->config->config['base_url']?>home/index1"><img src="<?=$this->config->config['base_url']?>images/seekmilogo.png"  class="resp" alt="SeekMi"></a>

</div>
<div class="header-middle-container"></div>


<div class="sticky-cta ng-scope ng-isolate-scope invisible" sticky-header="" sticky-partner-attr="data-sticky-partner" ng-class="{visible: visible, invisible: !visible}" ng-controller="SmartyController" throttle-ms="250" visible="visible">
    <div class="wrapper">
        <div class="logo"></div>
        <form class="ng-pristine ng-invalid ng-invalid-required" action="/request" method="get" ng-submit="submitClicked()">
         <!--   <input name="query" tabindex="2" class="query ng-isolate-scope ng-pristine ng-invalid ng-invalid-required" required="" autocomplete="off" placeholder="What services do you need?" smarty-input="" select="setSelected(x)" index="selected" list-items="suggestions" close="suggestionPicked()" selection-made="selectionMade" ng-model="prefix" type="text">
-->
                                <!--[if gt IE 8]><!-->
                    <div smarty-suggestions-box="" class="autocomplete-suggestions-menu outer ng-isolate-scope invisible" sticky-header="" ><!-- ngIf: suggestions.length > 0 --></div>
                <!--<![endif]-->
          
            <button type="button" class="bttn blue demo" tabindex="4" style="float:left;" onClick="gototop();">
               Sign Up
            </button>
          </div></div>
            
            <div class="flagInd"><!--<button tabindex="4" class="bttn blue" type="submit">
                Sign Up
            </button>&nbsp;&nbsp;--><span style="margin-top:5px;  float:right; color:#fff;"><img alt="SeekMi" src="http://seekmi.com/images/flagn.png">&nbsp;&nbsp;&nbsp;IND</span></div>
            </div>
            <input name="origin" value="homepage" type="hidden">
        </form>
    
    </div>
</div>
<div data-overview="" class="overview ng-scope" ng-controller="SlidesController">
    <div class="hero full-block ng-scope" data-hero="" ng-controller="SmartyController">
        <div class="wrapper">
            <div class="dynamic-row">
<div class="wrapper">
    <h1 class="title-wrapper choose-account" style="color:#fff; padding:25px; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; padding-top:0px;">
      <b> Sign up Today</b><br/> 
and enter for your chance to win
    </h1>
    
    <div class="bottom-link-wrapper" style="padding:5px;">
            <a href="#">
              &nbsp;  &nbsp;  1 of 2  Apple Watches&nbsp;  &nbsp; 
            </a>
          
            <a id="register-instead" href="#">
               <div class="icon-font icon-font-star"></div> &nbsp;  &nbsp; 1 of 4  iPhone 6 Plus&nbsp;  &nbsp; 
            </a>
         
            <a id="register-instead" href="#">
               <div class="icon-font icon-font-star"></div> &nbsp;  &nbsp; 1 of 4 Samsung S6&nbsp;  &nbsp; 
            </a>
        </div>
</div>
<div class="wrapper choose-account-wrapper">
    <div class="dynamic-row" style="padding-top:0px;">

                <div class="box service-pro-account promotionimg" style="border:none;">
                
         
   
        </div>

                <h3 class="or choose-account">
            or
        </h3>

        <div class="box standard-account">
        <fieldset>
          <div class="login-with-facebook-wrapper" style="margin-right: 14px;">         
            <a  class="bttn facebook-bttn" href="<?=$this->config->config['base_url']?>customer/fb_login">
                <div class="icon-font icon-font-facebook"></div>
                <span class="separator">
                    |
                </span>
                Sign up with Facebook
            </a>
         </div>
     
         <div class="form-field" style="margin-top:8px;">
            <button tabindex="103" id="show" style="background:#ac0000; border:#ac0000;" class="bttn blue submit-bttn" type="button" onclick="location.href='<?=$this->config->config['base_url']?>hauth/login/Google'"> <div class="icon-font icon-font-gplus"></div>
                <span class="separator">
                    |
                </span>Sign up with Google</button>
          </div>
                    
                     <h3 class="or">
                or
            </h3>
           <div class="form-field" style="margin-top:8px;">
           <form novalidate id="register" name="register" method="post" action="<?=$this->config->config['base_url']?>customer/register_submit" accept-charset="ISO-8859-1">
           <input type="text" placeholder="Sign up with Email" autocomplete="off" class="query" tabindex="2" name="usr_email" id="usr_email">
           <button tabindex="103" id="show" class="bttn blue submit-bttn" type="submit" style="margin-top:8px;">Submit</button>
           </form>
           </div>
          </fieldset>
            



        </div>   <div>
<center><h1 style="font-size:17px;"><a href="#professionalDiv" class="areyou" style=""><br/>Are you a professional?</a></h1></center></div>
    </div>
</div>
            </div>
        </div>
        <!-- ngRepeat: index in slides --><div class="slide slide-0" ng-class="{'current': slide == index}" ng-repeat="index in slides"></div><!-- end ngRepeat: index in slides --><div class="slide slide-1" ng-class="{'current': slide == index}" ng-repeat="index in slides"></div><!-- end ngRepeat: index in slides --><div class="slide slide-2 current" ng-class="{'current': slide == index}" ng-repeat="index in slides"></div><!-- end ngRepeat: index in slides -->
    </div>
        <div class="full-block navigation-row">
     
    </div>
    


<div class="how-it-works">

<div class="wrapper wrapper-title">
            <h2>We help you hire experienced professionals at a price that's right for you</h2>
            <span class="subtitle">How does Seekmi  help?</span>
        </div>

<div class="container">

<div class="row">

<div class="col-lg-12 col-lg-offset-1 col-md-12 text-center">
<center>
<h3>Find, compare and hire the right service providers</h3>
</center>

<div class="row">

<div class="col-md-4 col-sm-4">

<div class="each-step">

<div class="icon icon-match">
</div>

<p><em>1</em>Get matched</p>

<span> Tell us what you need. It only takes a minute. Get up to 5 quotes &amp; introductions in just a few hours
</span>

<span class="smaller"><strong><i class="fa fa-check"></i>Custom quote</strong>
<br><strong><i class="fa fa-check"></i>Personal message</strong>
</span>
</div>
</div>

<div class="col-md-4 col-sm-4">

<div class="each-step">

<div class="icon icon-compare">
</div>

<p><em>2</em>Compare</p>

<span>Compare prices &amp; profiles. All service providers are qualified &amp; verified. Each intro includes:
</span>

<span class="smaller"><strong><i class="fa fa-check"></i>Business profile</strong><strong><i class="fa fa-check"></i>Contact details</strong><strong><i class="fa fa-check"></i>Customer reviews</strong>
</span>
</div>
</div>

<div class="col-md-4 col-sm-4">

<div class="each-step last-step">

<div class="icon icon-hire">
</div>

<p><em>3</em>Hire</p>

<span>You decide who's best. Book the service provider of your choice, finalise details and fix an appointment.
</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


    <div class="states full-block">
        <div class="wrapper">
            <h2>Find us in all <?=count($province_list)?> provinces</h2>

                            
                            <ul>
                        <?php if($province_list!=''){ 
                            foreach($province_list as $province_row){
                            ?>
                        <li><?=$province_row->provinceName?></li>   
                       <?php }} ?>                                    
                         </ul>
                    </div>
    </div>
    


    <div ng-controller="SignupController" class="content ng-scope meetnew" id="professionalDiv">
    <div style="" ng-show="step == 'initial'" ng-controller="HookController" class="hook ng-scope">
        <form novalidate ng-submit="next()" name="hook" class="ng-pristine ng-invalid ng-invalid-required"><br/><br/>
            <h2>Are you a professional?</h2><br/>
            <h1>
                Meet new customers
                <div class="subtitle ng-binding" ng-class="{'has-custom-entry-point': customEntryPoint}" style="text-transform:none;">
                    Seekmi sends pros like you
                     requests from customers. You 
                    decide who to respond to and send a quote.
                </div>
            </h1>

            <fieldset>
                <label>
                    What do you do?
                </label>
                <div class="form-field form-field-category">
                     <select name="category" id="category" onchange="setDisable(this.value);">
                        <option value="">Select a profession</option>
                        <?php if($categories!=''){ 
                            foreach($categories as $cat_row){ ?>
                        <option value="<?=$cat_row->catId?>"><?=$cat_row->categoryName?></option>
                        <?php }} ?>
                    </select>  
                </div>
                <div class="form-field form-field-nav">
                   <button type="button" class="bttn get-started-btn" >
                        Sign up as a professional
                    </button>
                </div>
                <a class="how-it-works" href="/pros/how-Pintack-works" style="color:#55b7fe!important;">How Seekmi works</a><br/><br/><br/>

            </fieldset>
            <p ng-show="customEntryPoint" class="nevermind" style="display: none;">
                <a target="_self" href="/welcome">Start in a different profession</a>
            </p>

            <!-- ngIf: !customEntryPoint --><div ng-if="!customEntryPoint" class="ng-scope">
                <!-- ngRepeat: index in slides --><div ng-repeat="index in slides" ng-class="{'current': slide == index}" class="slide slide-0"></div><div ng-repeat="index in slides" ng-class="{'current': slide == index}" class="slide slide-1"></div><div ng-repeat="index in slides" ng-class="{'current': slide == index}" class="slide slide-2 current"></div>
            </div>
        </form>
    </div>

    
</div>


<div id="feedback-tab"><a data-show-feedback="" href="#">Contact Us</a></div>
 
</div> 
 

<div class="piede"><div class="wrapper"><div class="dynamic-row navigation">
            <div class="column-3">
                <div class="copyright">
                    <p><a href="<?=$this->config->config['base_url']?>home/index1" class="logo">SeekMi</a></p>
                    <p class="disclaimer">© 2015 SeekMi, Inc.</p>
                    <a href="<?=$this->config->config['base_url']?>privacy/" >Privacy Policy</a>
                    <a href="<?=$this->config->config['base_url']?>terms/" class="last">Terms of Use</a>
                    <div class="social" data-footer-load="/ajax/footer/social?piede">
                        <ul class="social-media">
                        <li>
                            <a href="http://www.facebook.com/seekmi" target="_blank">
                                <span class="facebook"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <span class="gplus"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <span class="twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <span class="blog"></span>
                            </a>
                        </li>
                     
                    </ul>
</div></div></div>
            <div class="column-2">
                <h4>SeekMi</h4>
                <ul>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Jobs</a></li
                    ><li><a href="#">Team</a></li>
                    <li><a href="#">Blog</a></li>
                </ul>
            </div>
            <div class="column-2">
                <h4>Customers</h4>
                <ul>
                    <li><a href="#">How it Works</a></li>
                    <li><a href="#">Safety</a></li>
                    <li><a href="#">Download iPhone App</a></li>
                </ul>
            </div>
            <div class="column-2">
                <h4>Pros</h4>
                <ul>
                    <li><a href="#">How it Works</a></li>
                    <li><a href="#">Sign Up</a></li>
                    <li><a href="#">Pro Center</a></li>
                    <li><a href="#">Success Stories</a></li>
                </ul>
            </div>
            <div class="column-3">
                <h4>Questions? Need help?</h4>
                <ul>
                    <li><span data-show-feedback="" class="pseudo-link">E-mail us</span></li>
                    <li class="phone-number">Call (800) 343-1710
                            <span>(5am - 8pm PT)</span>
                    </li>
                </ul>
            </div>
        </div>
    </div></div>
<div id="feedback-tab"><a data-show-feedback="" href="#">Contact Us</a></div>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript">
    $(window).on("scroll touchmove", function () {
            //$('#header_nav').toggleClass('tiny', $(document).scrollTop() > 0);
            //console.log('--------------------------sd'+$(document).scrollTop());
            if($(document).scrollTop()>700){
                    $('.sticky-cta').removeClass('invisible');
                    $('.sticky-cta').addClass('visible');
            }
            else{
                    $('.sticky-cta').removeClass('visible');
                    $('.sticky-cta').addClass('invisible');

            }

    });
    
    $(document).ready(function(){
       
    var validator = $("#register").validate({
        rules: {            
            usr_email: {
                required: true,
                email: true,
                remote: "<?=$this->config->config['base_url']?>customer/check_email_exists"
            }
        },
        messages: {            
            usr_email: {
                required: "You must enter an email",
                email: "Please enter a valid email",
                remote: "Sorry, that email is already taken"
            }
        }
    });

    $('#usr_email').blur(function() {
        $('#usr_email').val($.trim($('#usr_email').val()));
        validator.element('#usr_email');
    });
    
    if($('#category').val()==''){
       $('.get-started-btn').attr('disabled',true); 
    }else{
       $('.get-started-btn').attr('disabled',false); 
    }
});

function setDisable(obj){
    if(obj!=''){
        $('.get-started-btn').attr('disabled',false);
    }else{
        $('.get-started-btn').attr('disabled',true);
    }
}

$('.get-started-btn').click(function(){
    var catId=$('#category').val();
    if(catId!=''){
       location.href='<?=$this->config->config['base_url']?>pros/register/'+catId;
    }
})
</script>

<script>

function gototop(){
var body = $("html, body");
body.animate({scrollTop:0}, '500', 'swing', function() { 
});
}
</script>
</body>
</html>