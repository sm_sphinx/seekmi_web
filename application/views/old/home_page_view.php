<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Seekmi | Get Things Done</title>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/icons.css">

<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/signup.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css1/land.css">

</head>

<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds ng-scope landing-page">

<div class="glorious-header" data-section="header">
<div class="wrapper"><div class="row header-row">
<div class="header-logo">
<a href="<?=$this->config->config['base_url']?>"><img src="<?=$this->config->config['base_url']?>images/seekmilogo.png"  class="resp" alt="Seekmi"></a>

</div>
<div class="header-middle-container"></div>


<div class="sticky-cta invisible" sticky-header="" sticky-partner-attr="data-sticky-partner"  throttle-ms="250" visible="visible">
    <div class="wrapper">
        <div class="logo"></div>
        <form class="ng-pristine ng-invalid ng-invalid-required" action="/request" method="get" ng-submit="submitClicked()">
         <!--   <input name="query" tabindex="2" class="query ng-isolate-scope ng-pristine ng-invalid ng-invalid-required" required="" autocomplete="off" placeholder="What services do you need?" smarty-input="" select="setSelected(x)" index="selected" list-items="suggestions" close="suggestionPicked()" selection-made="selectionMade" ng-model="prefix" type="text">
-->
                                <!--[if gt IE 8]><!-->
                    <div smarty-suggestions-box="" class="autocomplete-suggestions-menu outer ng-isolate-scope invisible" sticky-header="" ><!-- ngIf: suggestions.length > 0 --></div>
                <!--<![endif]-->
          
            <button type="button" class="bttn blue1 demo" tabindex="4" onClick="gototop();" style="width:150px!important;">
               Sign Up
            </button>
          </div></div>
            
            <div class="flagInd"><!--<button tabindex="4" class="bttn blue" type="submit">
                Sign Up
            </button>&nbsp;&nbsp;--><span style="margin-top:5px;  float:right; color:#fff;"><img alt="SeekMi" src="http://seekmi.com/images/flagn.png">&nbsp;&nbsp;&nbsp;ID</span></div>
            </div>
            <input name="origin" value="homepage" type="hidden">
        </form>
    
    </div>
</div>
<div data-overview="" class="overview ng-scope" ng-controller="SlidesController">
    <div class="hero full-block ng-scope" data-hero="" ng-controller="SmartyController">
        <div class="wrapper">
            <div class="dynamic-row">
<div class="wrapper  wrap1">
    <h1 class="title-wrapper choose-account" style="color:#fff; visibility:hidden; padding:0px; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; padding-top:0px;">
      <b> <span style="font-size:48px;">Get things done</span><br/>
by hiring experienced<br/>
local service pros<br/>
</b><br/> 
<div>Daftar sekarang dan menangkan:</div>
    </h1>
 <!--   
    <div class="bottom-link-wrapper" style="padding:5px;">
            <a href="#">
              <div class="icon-font icon-font-star"></div>&nbsp;  1 dari 2 Apple Watch&nbsp;  &nbsp; 
            </a>
          
            <a id="register-instead" href="#">
               <div class="icon-font icon-font-star"></div>&nbsp; 1 dari 4 iPhone 6Plus&nbsp;  &nbsp; 
            </a>
         
            <a id="register-instead" href="#">
               <div class="icon-font icon-font-star"></div>&nbsp; serta 1 dari 4 Samsung S6&nbsp;  &nbsp; 
            </a>
        </div>-->
</div>
<div class="wrapper choose-account-wrapper">
    <div class="dynamic-row" style="padding-top:0px;">

                <div class="box service-pro-account promotionimg" style="border:none; padding-top:0px; margin-top:0px;">
                <h1 style="color:#fff; padding:5px; font-size:33px; line-height:41px; padding-top: 0 !important; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; padding-top:0px;" class="title-wrapper hire choose-account">
      <b> <span style="font-size:46px;" class="get">Get things done</span><br/>
by hiring experienced<br/>
local service pros
</b><br/> 
<div style="font-size:25px; color:#c91425" class="get">Daftar sekarang <span  style="color:#fff; font-size:18px;"><br class="break">dan menangkan:</br></span></div>
    </h1>
         
   
        </div>

                <h3 class="or choose-account">
            or
        </h3>

        <div class="box standard-account">
        <fieldset>
          <div class="form-field login-with-facebook-wrapper">         
            <a  class="bttn facebook-bttn" href="<?=$this->config->config['base_url']?>customer/fb_login">
                <div class="icon-font icon-font-facebook"></div>&nbsp;
                <span class="separator" style="color:#fff;">
                    |
                </span>
                Sign up with Facebook
            </a>
         </div>
     
         <div class="form-field" style="margin-top:8px;">
            <button tabindex="103" id="show" style="background:#ac0000; border:#ac0000;" class="bttn blue submit-bttn" type="button" onclick="location.href='<?=$this->config->config['base_url']?>hauth/login/Google'"> <div class="icon-font icon-font-gplus googlp" style="margin-right:0px;"></div>
                <span class="separator">
                    |
                </span>&nbsp;Sign up with Google &nbsp;&nbsp;&nbsp;</button>
          </div>
                    
                     <h3 class="or">
                or
            </h3>
           <div class="form-field" style="margin-top:8px;">
           <form novalidate id="register" name="register" method="post" action="<?=$this->config->config['base_url']?>customer/register_submit" accept-charset="ISO-8859-1">
           <input type="text" placeholder="Sign up with Email" autocomplete="off" class="query" tabindex="2" name="usr_email" id="usr_email">
           <button tabindex="103" id="show" class="bttn blue submit-bttn" type="submit" style="margin-top:8px;">Submit</button>
           </form>
           </div>
          </fieldset>
            



        </div>   <div>
<center><h1 style="font-size:17px;"><a href="#professionalDiv" class="areyou page-scroll" style=""><br/>Anda ahli lokal?</a></h1></center></div>

    </div>
</div>
            </div>
        </div>
        <!-- ngRepeat: index in slides --><div class="slide slide-0" ng-class="{'current': slide == index}" ng-repeat="index in slides"></div><!-- end ngRepeat: index in slides --><div class="slide slide-1" ng-class="{'current': slide == index}" ng-repeat="index in slides"></div><!-- end ngRepeat: index in slides --><div class="slide slide-2 current" ng-class="{'current': slide == index}" ng-repeat="index in slides"></div><!-- end ngRepeat: index in slides -->
    </div>
        <div class="full-block navigation-row">
     
    </div>
    


<div class="how-it-works">

<div class="wrapper wrapper-title">

            <h2>Kami akan mempertemukan Anda ke ahli lokal yang akan memberikan penawaran terbaik sesuai kebutuhan Anda</h2>

           
            <span class="subtitle">Bagaimana Seekmi dapat membantu Anda?</span>
        </div>

<div class="container">

<div class="row">

<div class="col-lg-12 col-lg-offset-1 col-md-12 text-center">
<center>

<h3>Temukan, bandingkan dan pekerjakan ahli lokal terbaik</h3>

</center>

<div class="row">

<div class="col-md-4 col-sm-4">

<div class="each-step">

<div class="icon icon-match">
</div>

<p><em>1</em>Beritahukan kami apa yang Anda butuhkan</p>

<span>Hanya satu menit untuk menjawab pertanyaan singkat tentang kebutuhan Anda.
</span>

</div>
</div>

<div class="col-md-4 col-sm-4">

<div class="each-step">

<div class="icon icon-compare">
</div>

<p><em>2</em>Dapatkan penawaran dan bandingkan</p>


<span>Dalam beberapa jam Anda akan menerima beberapa penawaran dari Ahli Lokal yang bersedia dipekerjakan disertai estimasi harga dan informasi kontak untuk dihubungi.  

</span>


</div>
</div>

<div class="col-md-4 col-sm-4">

<div class="each-step last-step">

<div class="icon icon-hire">
</div>


<p><em>3</em>Pilih dan pekerjakan ahli lokal yang cocok</p>


<span>Pilih dan pekerjakan ahli lokal terbaik.
</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
 

    <div class="states full-block">
        <div class="wrapper">
            <h2>Temukan kami di <?=count($province_list)?> Provinsi</h2>

                            
                            <ul>
                        <?php if($province_list!=''){ 
                            foreach($province_list as $province_row){
                            ?>
                        <li><?=$province_row->provinceName?></li>   
                       <?php }} ?>                                    
                         </ul>
                    </div>
    </div>
    


    <div class="content content-section">
    <div class="hook">
        <form novalidate name="hook" class=""  id="professionalDiv" style="padding-top: 70px !important;padding-bottom: 70px;">

            <h2>Apakah Anda Ahli Lokal?</h2><br/>
            <h1>
                Temukan klien baru
                <div class="subtitle ng-binding" ng-class="{'has-custom-entry-point': customEntryPoint}" style="text-transform:none;">
                     Seekmi akan meneruskan permintaan klien akan kebutuhan ahli lokal di wilayahnya. Anda yang akan memutuskan apakah Anda tertarik atau tidak untuk memberikan penawaran untuk klien tersebut.

                </div>
            </h1>

            <fieldset>
                <label>
                    Jasa apa yang Anda lakukan?
                </label>
                <div class="form-field form-field-category">
                     <select name="category" id="category" onchange="setDisable(this.value);">
                        <option value="">Pilih jenis layanan Anda</option>
                        <?php if($categories!=''){ 
                            foreach($categories as $cat_row){ ?>
                        <option value="<?=$cat_row->catId?>"><?=$cat_row->categoryName?></option>
                        <?php }} ?>
                    </select>  
                </div>
                <div class="form-field form-field-nav">
                   <button type="button" class="bttn get-started-btn" >
                        Daftar sebagai ahli lokal
                    </button>
                </div>
                <!--<a class="how-it-works" href="/pros/how-Pintack-works" style="color:#55b7fe!important;">How Seekmi works</a><br/><br/><br/>-->

            </fieldset> 
           
        </form>
    </div>

    
</div>


<div id="feedback-tab"><a data-show-feedback="" href="#">Contact Us</a></div>
 
</div> 
 

<div class="piede"><div class="wrapper"><div class="dynamic-row navigation">
            <div class="column-3">
                <div class="copyright">
                    <p><a href="<?=$this->config->config['base_url']?>" class="logo">Seekmi</a></p>
                    <p class="disclaimer">© Copyright 2015 Seekmi.com</p>
                    <a href="<?=$this->config->config['base_url']?>privacy/" >Privacy Policy</a>
                    <a href="<?=$this->config->config['base_url']?>terms/" class="last">Terms of Use</a>
                    <div class="social">
                        <ul class="social-media">
                        <li>
                            <a href="http://www.facebook.com/seekmi" target="_blank">
                                <span class="facebook"></span>
                            </a>
                        </li>
                        <!--<li>
                            <a href="#" target="_blank">
                                <span class="gplus"></span>
                            </a>
                        </li>-->
                        <li>
                            <a href="https://twitter.com/SeekmiApp" target="_blank">
                                <span class="twitter"></span>
                            </a>
                        </li>
                        <!--<li>
                            <a href="#" target="_blank">
                                <span class="blog"></span>
                            </a>
                        </li>-->
                     
                    </ul>
</div></div></div>
            <!--<div class="column-2">
                <h4>Seekmi</h4>
                <ul>
                    <li><a href="#">Tentang Kami</a></li>
                    <li><a href="#">Pekerjaan</a></li
                    ><li><a href="#">Tim</a></li>
                    <li><a href="#">Blog</a></li>
                </ul>
            </div>
            <div class="column-2">
                <h4>Customers</h4>
                <ul>
                    <li><a href="#">Kegunaan Seekmi</a></li>
                    <li><a href="#">Keamanan</a></li>
                    <li><a href="#">Download dari iPhone App</a></li>
                </ul>
            </div>
            <div class="column-2">
                <h4>Pros</h4>
                <ul>
                    <li><a href="#">Kegunaan Seekmi</a></li>
                    <li><a href="#">Daftar</a></li>
                    <li><a href="#">Pro Center</a></li>
                    <li><a href="#">Testimonial</a></li>
                </ul>
            </div>-->
            <div class="column-3" style="border:0px;">
                <h4>Pertanyaan? Perlu bantuan?</h4>
                <ul>
                    <li><a href="mailto:support@seekmi.com">E-mail kami</a></li>
                    <!--<li class="phone-number">Call (800) 343-1710
                            <span>(5am - 8pm PT)</span>
                    </li>-->
                </ul>
            </div>
        </div>
    </div></div>
<div id="feedback-tab"><a data-show-feedback="" href="#">Contact Us</a></div>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.easing.min.js"></script>
<script type="text/javascript">
    $(window).on("scroll touchmove", function () {
            //$('#header_nav').toggleClass('tiny', $(document).scrollTop() > 0);
            //console.log('--------------------------sd'+$(document).scrollTop());
            if($(document).scrollTop()>700){
                    $('.sticky-cta').removeClass('invisible');
                    $('.sticky-cta').addClass('visible');
            }
            else{
                    $('.sticky-cta').removeClass('visible');
                    $('.sticky-cta').addClass('invisible');

            }

    });
    
    $(document).ready(function(){
       
    var validator = $("#register").validate({
        rules: {            
            usr_email: {
                required: true,
                email: true,
                remote: "<?=$this->config->config['base_url']?>customer/check_email_exists"
            }
        },
        messages: {            
            usr_email: {
                required: "You must enter an email",
                email: "Please enter a valid email",
                remote: "Sorry, that email is already taken"
            }
        }
    });

    $('#usr_email').blur(function() {
        $('#usr_email').val($.trim($('#usr_email').val()));
        validator.element('#usr_email');
    });
    
    if($('#category').val()==''){
       $('.get-started-btn').attr('disabled',true); 
    }else{
       $('.get-started-btn').attr('disabled',false); 
    }
});

function setDisable(obj){
    if(obj!=''){
        $('.get-started-btn').attr('disabled',false);
    }else{
        $('.get-started-btn').attr('disabled',true);
    }
}

$('.get-started-btn').click(function(){
    var catId=$('#category').val();
    if(catId!=''){
       location.href='<?=$this->config->config['base_url']?>pros/register/'+catId;
    }
})
</script>

<script>

function gototop(){
var body = $("html, body");
body.animate({scrollTop:0}, '500', 'swing', function() { 
});
}

$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});
</script>
</body>
</html>