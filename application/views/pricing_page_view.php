<!doctype html>
<html lang="en" class="no-js">
<!-- Mirrored from nunforest.com/site/element-demo/boxed/pricing.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Jan 2015 06:53:45 GMT -->
<head>
    <title><?=$page_title?> | Montego</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/bootstrap.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/magnific-popup.css" media="screen">	
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/animate.css" media="screen">
<!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/style.css" media="screen">
    
</head>
<body id="top">
<? include('header_view.php'); ?>
<!-- ####################################################################################################### -->
<div id="content">
			
		
<div class="section-content page-banner-section2">
        <div class="container">
                <div class="row">
                        <div class="col-sm-6">
                                <h1>Pricing</h1>
                        </div>
                        <div class="col-sm-6">
                                <ul>
                                        <li><a href="<?=$this->config->config['base_url']?>">Home</a></li>
                                        <li>Pricing</li>
                                </ul>
                        </div>
                </div>
        </div>
</div>


<!-- pricing-section -->
<div class="section-content pricing-section">
        <div class="pricing-box1">
                <div class="container">
                        <div class="row">
                                <div class="col-md-3 col-sm-6">
                                        <ul class="pricing-table basic">
                                                <li>
                                                        <h2>Standart</h2>
                                                </li>
                                                <li class="title">
                                                        <p><span>$15</span>/ per month</p>
                                                </li>
                                                <li>
                                                        <p>Feature 1</p>
                                                </li>
                                                <li>
                                                        <p>Feature 2</p>
                                                </li>
                                                <li>
                                                        <p>Feature 3</p>
                                                </li>
                                                <li>
                                                        <p>Feature 4</p>
                                                </li>
                                                <li>
                                                        <a href="#">Order Now</a>
                                                </li>
                                        </ul>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                        <ul class="pricing-table corporate">
                                                <li>
                                                        <h2>Corporate</h2>
                                                </li>
                                                <li class="title">
                                                        <p><span>$34</span>/ per month</p>
                                                </li>
                                                <li>
                                                        <p>Feature 1</p>
                                                </li>
                                                <li>
                                                        <p>Feature 2</p>
                                                </li>
                                                <li>
                                                        <p>Feature 3</p>
                                                </li>
                                                <li>
                                                        <p>Feature 4</p>
                                                </li>
                                                <li>
                                                        <a href="#">Order Now</a>
                                                </li>
                                        </ul>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                        <ul class="pricing-table premium">
                                                <li>
                                                        <h2>Premium</h2>
                                                </li>
                                                <li class="title">
                                                        <p><span>$95</span>/ per month</p>
                                                </li>
                                                <li>
                                                        <p>Feature 1</p>
                                                </li>
                                                <li>
                                                        <p>Feature 2</p>
                                                </li>
                                                <li>
                                                        <p>Feature 3</p>
                                                </li>
                                                <li>
                                                        <p>Feature 4</p>
                                                </li>
                                                <li>
                                                        <a href="#">Order Now</a>
                                                </li>
                                        </ul>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                        <ul class="pricing-table unlimited">
                                                <li>
                                                        <h2>Unlimited</h2>
                                                </li>
                                                <li class="title">
                                                        <p><span>$102</span>/ per month</p>
                                                </li>
                                                <li>
                                                        <p>Feature 1</p>
                                                </li>
                                                <li>
                                                        <p>Feature 2</p>
                                                </li>
                                                <li>
                                                        <p>Feature 3</p>
                                                </li>
                                                <li>
                                                        <p>Feature 4</p>
                                                </li>
                                                <li>
                                                        <a href="#">Order Now</a>
                                                </li>
                                        </ul>
                                </div>
                        </div>
                </div>
        </div>

</div>

        <div class="pricing-box2">
                <div class="container">
                        <div class="row">
                                <div class="col-md-4">
                                        <ul class="pricing-table basic">
                                                <li class="title">
                                                        <h2>Standart</h2>
                                                        <h1>
                                                                $<span>15</span>/ mo
                                                        </h1>
                                                        <p>Created for Professional People</p>
                                                </li>
                                                <li>
                                                        <p>Feature 1</p>
                                                </li>
                                                <li>
                                                        <p>Feature 2</p>
                                                </li>
                                                <li>
                                                        <p>Feature 3</p>
                                                </li>
                                                <li>
                                                        <p>Feature 4</p>
                                                </li>
                                                <li>
                                                        <a href="#">Order Now</a>
                                                </li>
                                        </ul>
                                </div>
                                <div class="col-md-4">
                                        <ul class="pricing-table premium">
                                                <li class="title">
                                                        <h2>Premium</h2>
                                                        <h1>
                                                                $<span>88</span>/ mo
                                                        </h1>
                                                        <p>Created for Professional People</p>
                                                </li>
                                                <li>
                                                        <p>Feature 1</p>
                                                </li>
                                                <li>
                                                        <p>Feature 2</p>
                                                </li>
                                                <li>
                                                        <p>Feature 3</p>
                                                </li>
                                                <li>
                                                        <p>Feature 4</p>
                                                </li>
                                                <li>
                                                        <a href="#">Order Now</a>
                                                </li>
                                        </ul>
                                </div>
                                <div class="col-md-4">
                                        <ul class="pricing-table unlimited">
                                                <li class="title">
                                                        <h2>Unlimited</h2>
                                                        <h1>
                                                                $<span>111</span>/ mo
                                                        </h1>
                                                        <p>Created for Professional People</p>
                                                </li>
                                                <li>
                                                        <p>Feature 1</p>
                                                </li>
                                                <li>
                                                        <p>Feature 2</p>
                                                </li>
                                                <li>
                                                        <p>Feature 3</p>
                                                </li>
                                                <li>
                                                        <p>Feature 4</p>
                                                </li>
                                                <li>
                                                        <a href="#">Order Now</a>
                                                </li>
                                        </ul>
                                </div>
                        </div>
                </div>
        </div>

</div>
<!-- End content -->
<!-- ####################################################################################################### -->
<? include('footer_view.php'); ?>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.migrate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/waypoint.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/plugins-scroll.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/script.js"></script>  	
</body>
</html>
