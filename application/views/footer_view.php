<div class="piede"><div class="wrapper"><div class="dynamic-row navigation">
             <?php 
            if(get_cookie('language') === 'english'){ 
               $lang='en'; 
            }else{
                $lang='id';
            }
            ?>
            
            <div class="column-3">
                <div class="copyright">
                    <p>
                        <?php if (isset($this->session->userdata['userId'])) { ?>
                        <?php if ($this->session->userdata('user_type') == 'professional') { ?>
                            <a href="<?= $this->config->config['base_url'] ?>profile/requests" class="logo">
                        <?php }else if ($this->session->userdata('user_type') == 'user'){ ?>
                            <a href="<?= $this->config->config['base_url'] ?>profile/dashboard" class="logo">  
                        <?php }else{ ?>
                            <a href="<?= $this->config->config['base_url'] ?>" class="logo">     
                        <?php }}else{ ?>
                            <a href="<?= $this->config->config['base_url'] ?>" class="logo">     
                        <?php } ?>                        
                            Seekmi</a></p>
                    <p class="disclaimer">&copy; Copyright 2015 Seekmi.com</p>
                    <a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/privacy/" ><?php echo MENU_PRIVACT__POLICY;?></a>
                    <a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/terms-of-use/" class="last"><?php echo MENU_TERMS;?></a>
                    <div class="social">
                        <ul class="social-media">
                            <li>
                                <a href="http://www.facebook.com/seekmi" target="_blank">
                                    <span class="facebook"></span>
                                </a>
                            </li>                           
                            <li>
                                <a href="https://twitter.com/SeekmiApp" target="_blank">
                                    <span class="twitter"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
           
            <div class="column-2">
                <h4>Seekmi</h4>
                <ul><li><a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/about/"><?php echo MENU_ABOUT; ?></a></li>
                    <li><a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/jobs/"><?php echo MENU_WORK; ?></a></li>
                    <li><a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/team/"><?php echo MENU_TEAM; ?></a></li>
                    <li><a href="<?=$this->config->config['main_base_url']?>app/id/news/"><?php echo MENU_NEWS; ?></a></li>
                </ul>
            </div>
            <div class="column-2">
                <h4><?php echo MENU_HEAD_CUSTOMER; ?></h4>
                <ul>
                    <li><a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/how-it-works/"><?php echo MENU_HOWITWORKS; ?></a></li>
                    <li><a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/safety/"><?php echo MENU_SAFETY; ?></a></li>                    
                </ul>
            </div>
            <div class="column-2">
                <h4><?php echo MENU_HEAD_PROS; ?></h4>
                <ul>
                    <li><a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/how-it-works-professional/"><?php echo MENU_HOWITWORKS; ?></a></li>
                    <?php if(!isset($this->session->userdata['userId'])) { ?>
                    <li><a href="<?=$this->config->config['base_url']?>pros/register/"><?php echo MENU_SIGNUP; ?></a></li>
                    <?php } ?>
                    <li><a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/credits/"><?php echo MENU_CREDIT; ?></a></li>
                    <li><a href="<?=$this->config->config['main_base_url']?>mag/"><?php echo MENU_BLOG; ?></a></li>
                </ul>
            </div>
            <div class="column-3">
                <h4><?php echo MENU_HEAD_QUESTIONS_NEED_HELP; ?></h4>
                <ul>
                    <li><a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/faq/"><?php echo MENU_FAQ; ?></a></li>
                    <li><a href="<?=$this->config->config['main_base_url']?>app/<?php echo $lang; ?>/contact-us/"><?php echo MENU_EMAIL; ?></a></li>
                    <li class="phone-number">Call (021) 4000-0289</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--<div id="feedback-tab"><a data-show-feedback="" href="#">Contact Us</a></div>-->
 

<script type="text/javascript">
     var Host='<?=$this->config->config['base_url']?>';    
</script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/common.js"></script>