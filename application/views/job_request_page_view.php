<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/message.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/work.css">
<div data-curtain="request" class="homepage-work-request curtain-inner" style="display:block;">
        <div class="work-request box box-modal" style="background-color:#fff;">
            <div class="request-header box-header" style="padding: 20px 0 0;">
                <div class="request-name">
                    <h2>
                        <?php echo $project_row->subserviceName; ?>
                    </h2>
                    <a href="javascript:;" class="modal-close hide_request" style="right:-12px; top:-12px;">×</a>
                </div>
             </div>     
             <div class="what-when-where-container">
                 <dl class="work-request-data" data-component="work-request-data">
                    <?php if(!empty($req_data)){ 
                         foreach($req_data as $req_row){                    
                    ?> 
                    <dt class="request-title"><?=$req_row->questionName?></dt>
                    <dd class="request-info">
                    <?php if(!empty($req_option_data[$req_row->questionId])){ ?>
                        <ul>
                       <?php foreach($req_option_data[$req_row->questionId] as $req_option_row){ ?>
                        <li class="list-bullet">
                            <?php if(count($req_option_data[$req_row->questionId]) > 1){ ?>
                         <div class="check"></div>
                            <?php } ?>
                           <?=$req_option_row['optionText']?> 
                         <?php if($req_option_row['optionExtra']){
                             echo " (".$req_option_row['optionExtra'].")";
                         }
                         ?>
                        </li>
                        <?php }?>
                        </ul>
                       <?php }else{ ?>
                          <?php
                            if($req_row->fieldType=='datepicker'){ 
                                if($req_row->extras!=''){
                                    $ext_arr=array();
                                    $ext_arr=json_decode($req_row->extras);
                                    if($ext_arr->from!=''){
                                        echo "From: ".$ext_arr->from;
                                    }
                                    if($ext_arr->to!=''){
                                        echo "   To: ".$ext_arr->to;
                                    }
                                }
                            } ?>
                       <?php } ?>
                     </dd>
                    <?php }} ?>                    
                </dl>
        </div>
    </div>
 </div>
<script type="text/javascript">
$(".hide_request").click(function(){
    $(".view_request").hide();
});
</script>