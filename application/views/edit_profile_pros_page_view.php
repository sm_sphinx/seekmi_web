<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> <?=$page_title?> - Seekmi</title>
<link rel="icon" type="image/png" href="<?= $this->config->config['base_url'] ?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/consume.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/settings.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/message.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/profile.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="<?=$this->config->config['base_url']?>css/jquery.tokenize.css" />
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-avenir primo-responsive primo-fluid  box-shadow multiple-backgrounds">
<? include('header_view.php'); ?>
    <div class="profile-content">
      <div class="profile-header">
        <div style="display: none;" class="alert-info">
            <p>
                <?php echo EDITPRO_WELCOME_TEXT;?>
                <a href="edit"><?php echo BUTTON_EDIT_PROFILE;?></a>
            </p>
        </div>
        <div style="" class="alert-info">
            <p>
                <?php echo EDITPRO_EDITING_TEXT;?>
                <a href="<?=$this->config->config['base_url']?>profile/services/view"><?php echo BUTTON_DONE_EDIT;?></a>
            </p>
        </div>
          
       <div style="display:none;" class="alert-info" id="msgBoxImage"></div>   

        <div class="profile-basics editable">
            <div class="profile-picture-container">
                <div wizard-target="profile-picture" wizard-editable-section="wizard.editable" class="profile-picture">
                  <?php if($user_data->userPhoto==''){ ?>
                    <img alt="<?php echo $user_data->firstname; ?> <?php echo $user_data->lastname; ?>" src="<?=$this->config->config['base_url']?>images/img-profile-missing.png">                
                  <?php }else{ ?>
                      <img alt="<?php echo $user_data->firstname; ?> <?php echo $user_data->lastname; ?>" src="<?=$this->config->config['base_url']?>user_images/<?=$user_data->userPhoto?>">
                  <?php } ?>                    
                  <a class="edit-section edit-section-profile bttn micro blue" id="uploadProfileBtn" style="" href="javascript:void();">
                         <?php echo EDITPRO_UPLOADPIC_TEXT;?>
                  </a>
                </div>
                
                <div onClick="getNvigatePage('cls_basic_popup','cls_basic_info');" wizard-target="basic-info" wizard-editable-section="wizard.editable" class="profile-basic-details">
                    <div class="profile-identity">
                        <h1 class="profile-title"><?php echo $business_info->businessName; ?></h1>
                        <div class="profile-business-relationship"><?php echo $user_data->firstname; ?> <?php echo $user_data->lastname; ?></div>
                    </div>                    
                    <ul class="profile-attributes">
                        <li class="location"><?php echo $city_nm; ?>, <?php echo $province_nm; ?></li>
                        <li class="phone"><?php echo $user_data->phone; ?></li>
                    </ul>
                </div>
            </div>
            <a class="edit-section bttn micro blue" onClick="getNvigatePage('cls_basic_popup','cls_basic_info');"><?php echo BUTTON_EDIT;?></a>
         </div>
      </div>

      <div stampede="" class="profile-media">
        <div data-stampede-carousel="" class="stampede-carousel" style="height: 412px;">
            
                <?php if(!empty($media_list)){ ?>
                    <ul class="stampede-content bxslider">
                 <?php foreach($media_list as $media_row){ ?>
                    <li class="stampede-current">
                      <div class="stampede-picture-container">
                          <?php if($media_row->mediaType=='Image'){  ?>
                          <img src="<?php echo $this->config->config['base_url'].'business_images/'.$media_row->mediaPath; ?>">
                          <?php }else{ ?>                          
                          <iframe src="<?=$media_row->mediaPath?>" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                          <?php } ?>
                      </div>                  
                    </li>
                <?php } ?>
                  </ul>     
                <?php } ?>
            
        </div>
        
        <a class="edit-section bttn micro blue" style="" onClick="getNvigatePage('cls_basic_popup','cls_media');"><?php echo BUTTON_EDITMEDIA;?></a>
       </div>

       <div class="full-block service-info-block">
        <div class="service-info-page-section">
            <div class="service-info-container">
                <div class="service-info">
                    <div onClick="getNvigatePage('cls_basic_popup','cls_bio');" wizard-target="bio" wizard-editable-section="wizard.editable" class="service-bio-section editable">
                        <h3 class="section-header"><?php echo EDITPRO_BIO_LABEL;?></h3>                                               
                            <?php if($business_info->bioInfo==''){ ?>
                             <p class="empty-section">
                                <?php echo EDITPRO_TELLSTORY_TEXT;?>
                             </p>
                            <?php }else{ ?>
                             <p class="service-bio body-text">
                                <?php echo $business_info->bioInfo; ?>
                             </p>
                           <?php } ?>                      
                        <a class="edit-section bttn micro blue" style="" onClick="getNvigatePage('cls_basic_popup','cls_bio');"><?php echo BUTTON_EDIT;?></a>
                    </div>
                    <div onClick="getNvigatePage('cls_basic_popup','cls_services');" wizard-target="services" wizard-editable-section="wizard.editable" class="editable">
                        <h3 class="section-header"><?php echo EDITPRO_SERVICES_TEXT;?></h3>
                        <p class="service-description body-text" style="white-space:pre-line;">
                         <?php echo $business_info->bussinessDescription; ?>                         
                        </p>
                        <a class="edit-section bttn micro blue" style="" onClick="getNvigatePage('cls_basic_popup','cls_services');">Edit</a>
                    </div>
                    <div onClick="getNvigatePage('cls_basic_popup','cls_links');" wizard-target="links" wizard-editable-section="wizard.editable" class="service-external-links editable">
                        <?php if($business_info->website!=''){ ?>
                        <a class="offsite-link offsite-link-website" title="<?=$business_info->website?>" target="_blank" href="http://<?=$business_info->website?>">
                            <?php echo $business_info->website; ?>
                            <span class="offsite-link-metric"><?php echo DETAIL_WEBSITE_TEXT;?></span>
                        </a>
                        <?php }else{ ?>
                        <div class="empty-section">
                            <span class="offsite-link offsite-link-facebook empty"></span>
                            <span class="offsite-link offsite-link-twitter empty"></span>
                            <span class="offsite-link offsite-link-yelp empty"></span>
                            <span class="edit-prompt">
                                <?php echo EDITPRO_SHARE_TEXT;?>
                            </span>
                        </div>
                        <?php } ?>
                        <a class="edit-section bttn micro blue" style="" onClick="getNvigatePage('cls_basic_popup','cls_links');"><?php echo BUTTON_EDIT;?></a>
                    </div>
                </div>
                <div class="service-sidebar">
                    <div onClick="getNvigatePage('cls_basic_popup','cls_business_info');" wizard-target="business-info" wizard-editable-section="wizard.editable" class="editable">
                        <h3 class="section-header"><?php echo MENU_ABOUT;?></h3>
                        <?php if($business_info->businessLogo!=''){ ?>
                          <img src="<?=$this->config->config['base_url']?>business_logo/thumb/<?=$business_info->businessLogo?>" alt="Business logo" class="service-logo">
                        <?php } ?>
                        <h3 class="business-name"><?php echo $business_info->businessName; ?></h3>  
                        <?php if($business_info->foundingYear!=''){ 
                            $currentYear=date('Y');
                            if($currentYear > $business_info->foundingYear){
                               $year_founding=$currentYear-$business_info->foundingYear;
                            ?>
                        <p class="years-in-business"><?php echo $year_founding.' '.EDITPRO_YEARSINBUSS_TEXT;?></p>
                        <?php }} ?>
                        <?php if($business_info->employeeCount!='' && $business_info->employeeCount!='0'){ ?>
                        <p class="employee-count"><?php echo $business_info->employeeCount.' '.EDITPRO_EMPLOYEES_TEXT; ?> </p>
                        <?php } if($business_info->travelToCustomer=='Y'){ ?>
                        <p class="service-travels"><?php echo EDITPRO_TRAVELS_TEXT;?>
                           <span><?php echo $business_info->distanceMiles; ?></span> <?php echo EDITPRO_MILES_TEXT;?>
                        </p>
                        <?php } ?>
                        <!--<img src="#" class="service-map">-->
                        <a class="edit-section bttn micro blue" style="" onClick="getNvigatePage('cls_basic_popup','cls_business_info');"><?php echo BUTTON_EDIT;?></a>
                    </div>
                    <?php /* <div onClick="getNvigatePage('cls_basic_popup','cls_credentials');" wizard-target="credentials" wizard-editable-section="wizard.editable" class="credentials editable">
                        <h3 class="section-header"><?php echo EDITPRO_CREDENTIALS_TEXT;?></h3>             
                        <p class="empty-section">
                            <?php echo EDITPRO_NOCREDENTIALS_TEXT;?>
                        </p>
                        <a class="edit-section bttn micro blue" style="" onClick="getNvigatePage('cls_basic_popup','cls_credentials');"><?php echo BUTTON_EDIT;?></a>
                    </div> */ ?>
                </div>
            </div>
        </div>
        </div>
        
        <?php if($answer_count== 0){ ?>       
        <div class="full-block service-questions-edit-container">
            <div onClick="getNvigatePage('cls_basic_popup','cls_qa');" wizard-target="q-and-a" wizard-editable-section="wizard.editable" class="service-questions-edit editable">
                <h3 class="section-header"><span><?php echo EDITPRO_Q_AND_A_TEXT;?></span></h3>                
                <p class="empty-section">
                    <?php echo EDITPRO_ANS_TEXT;?>
                </p>
                <a class="edit-section bttn micro blue" style="" onClick="getNvigatePage('cls_basic_popup','cls_qa');"><?php echo BUTTON_EDIT;?></a>
            </div>
        </div>
        <?php }else{ ?>
         <div class="full-block service-questions-container">
            <div onClick="getNvigatePage('cls_basic_popup','cls_qa');" wizard-target="q-and-a" wizard-editable-section="wizard.editable" class="service-questions editable">
                <h3 class="section-header"><span><?php echo EDITPRO_Q_AND_A_TEXT;?></span></h3> 
                
                    <?php
                    $i=1;                    
                    foreach($answer_list as $answer_row){
                        if($answer_row->answerText!=''){?>
                    <?php if($i==1){ ?><div><?php } ?>
                    <div class="service-question-column">
                        <div class="service-question-answer">
                            <h3><?=$answer_row->questionText?></h3>
                            <p><?=$answer_row->answerText?></p>
                        </div>
                    </div>
                   <?php if($i%3==0){ ?></div><div><?php } ?>
                   <?php if($i==$answer_count){ ?></div><?php } ?>
                   <?php  $i++;}} ?>                        
                           
                <a class="edit-section bttn micro blue" style="" onClick="getNvigatePage('cls_basic_popup','cls_qa');"><?php echo BUTTON_EDIT;?></a>
            </div>
        </div>
        <?php } ?>
        
        <div class="full-block reviews-block" id="reviews">
            <div onClick="getNvigatePage('cls_basic_popup','cls_reviews');" wizard-target="reviews" wizard-editable-section="wizard.editable" class="reviews-container editable">
                <h3 class="section-header"><?php echo DETAIL_REVIEWS_TEXT;?></h3>
                <p>
                    <?php echo EDITPRO_COMPJOB_TEXT;?>
                </p>
                <div class="empty-section">
                  <?php echo EDITPRO_COMPLETEJOB_TEXT;?>
                </div>
                <a class="edit-section bttn micro blue" style="" onClick="getNvigatePage('cls_basic_popup','cls_reviews');"><?php echo BUTTON_MANAGEREVIEW;?></a>
            </div>
        </div>
    </div>


<div class="curtain" data-curtain="active" style="display:none;">
    <div style="display: block;" curtain-close-url="edit" class="wizard curtain-inner pod pod-modal" curtain-name="wizard" curtain-visible="wizard.visible" curtain="">
            
        <div class="pod-header">
            <div class="wizard-header">
                    <a onClick="getNvigatePage('cls_basic_popup','cls_basic_info');" title="Basic Info" class="p_cls_basic_popup p_cls_basic_info wizard-header-tab basic-info active" href="javascript:void(0);" label="Basic Info" key="basic-info" wizard-header-tab="">Basic Info</a>
                    <a onClick="getNvigatePage('cls_basic_popup','cls_media');" title="Media" class="p_cls_basic_popup p_cls_media wizard-header-tab media" href="javascript:void(0);" active="wizard.current" label="Media" key="media" wizard-header-tab="">Media</a>
                    <a onClick="getNvigatePage('cls_basic_popup','cls_bio');" title="Bio" class="p_cls_basic_popup p_cls_bio wizard-header-tab bio" href="javascript:void(0);" label="Bio" key="bio" wizard-header-tab="">Bio</a>
                    <a onClick="getNvigatePage('cls_basic_popup','cls_services');" title="Services" class="p_cls_basic_popup p_cls_services wizard-header-tab services" href="javascript:void(0);" label="Services" key="services" wizard-header-tab="">Services</a>
                    <a onClick="getNvigatePage('cls_basic_popup','cls_links');" title="Links" class="p_cls_basic_popup p_cls_links wizard-header-tab links" href="javascript:void(0);" label="Links" key="links" wizard-header-tab="">Links</a>
                    <a onClick="getNvigatePage('cls_basic_popup','cls_business_info');" title="Business Info" class="p_cls_basic_popup p_cls_business_info wizard-header-tab business-info" href="javascript:void(0);" label="Business Info" key="business-info" wizard-header-tab="">Business Info</a>
                    <a onClick="getNvigatePage('cls_basic_popup','cls_refer');" title="Refer" class="p_cls_basic_popup p_cls_refer wizard-header-tab credentials" href="javascript:void(0);" label="Refer" key="refer" wizard-header-tab="">Refer</a>
                    <a onClick="getNvigatePage('cls_basic_popup','cls_qa');" title="Q &amp;amp; A" class="p_cls_basic_popup p_cls_qa wizard-header-tab q-and-a" href="javascript:void(0);" label="Q &amp;amp; A" key="q-and-a" wizard-header-tab="">Q &amp; A</a>
                    <a onClick="getNvigatePage('cls_basic_popup','cls_reviews');" title="Reviews" class="p_cls_basic_popup p_cls_reviews wizard-header-tab reviews" href="javascript:void(0);" label="Reviews" key="reviews" wizard-header-tab="">Reviews</a>
            </div>
            <a  id="hide"  style="cursor:pointer;" class="modal-close" >×</a>
        </div>

     <div style="display:block;" class="cls_basic_popup cls_basic_info">
       <form class="form-basic-info" name="basicInfo" id="basicInfo">
        <div class="form-content">
            <h3 class="form-section-header">
                <?php echo EDITPRO_EARN_BASIC_INFO_TEXT; ?>
            </h3><br/>
            <h2 class="form-section-header">
                <?php echo EDITPRO_EDITINFO_TEXT; ?>
            </h2>
            <fieldset>
                <!--<div class="form-field" optional="true" field="basicInfo.slogan" form-field="">
                    <div>
                    <label><?php //echo EDITPRO_BUSSNAME_TEXT;?></label>
                    <input type="text" autocomplete="off" value="<?=$business_info->businessName;?>" placeholder="Leave blank if you don't represent a company" name="business_name">
                    </div>
                    <span class="subtext-form"><?php //echo EDITPRO_OPTIONAL_TEXT;?></span>
                </div>-->
                <div class="form-field-first-name form-field" field="basicInfo.first_name" form-field="">
                    <div>
                    <label><?php echo REGISTER_FNAME;?></label>
                    <input type="text" autocomplete="off" value="<?=$user_data->firstname;?>" id="first_name" name="first_name">
                    </div>
                </div>
                <div class="form-field-last-name form-field" field="basicInfo.last_name" form-field="">
                    <div>
                    <label><?php echo REGISTER_LNAME;?></label>
                    <input type="text" autocomplete="off" value="<?=$user_data->lastname;?>" id="last_name" name="last_name">
                    </div>
                </div>
                <div class="form-field" optional="true" field="basicInfo.slogan" form-field="">
                    <div>
                    <label><?php echo EDITPRO_TAGLINE_TEXT;?></label>
                    <input type="text" autocomplete="off" value="<?=$business_info->tagLine;?>" placeholder="Write a memorable phrase or quote that describes yourself or your work." id="slogan" name="slogan">
                    </div>
                    <span class="subtext-form"><?php echo EDITPRO_OPTIONAL_TEXT;?></span>
                </div>
                <div class="form-field">
                    <div>
                      <label for="services"><?php echo SIGNUPREGI_PUTALL_TEXT;?>(,)</label>
                      <select name="services[]" id="services" class="tokenize-sample" multiple="multiple">
                          <?php if(!empty($services)){ 
                              for($s=0;$s<count($services);$s++){ ?>
                          <option value="<?php echo $services[$s]; ?>" selected="selected"><?php echo $services[$s]; ?></option>
                          <?php }} ?>
                      </select>
                    </div>
                </div>
            </fieldset>
            <h2 class="form-section-header">
                <?php echo EDITPRO_ADD_CON_TEXT;?>
            </h2>
            <fieldset>
                <div class="form-field-address-1 form-field" field="basicInfo.address1" form-field="">
                    <div>
                    <label for="address1"><?php echo EDITPRO_STREETADD_TEXT;?></label>
                    <input type="text" autocomplete="off" value="<?=$user_data->streetAddress;?>" name="address1">
                    </div>
                </div>
                <div class="form-field-address-2 form-field form-field" style="width:50%;">
                    <div>
                    <label for="province"><?php echo EDITPRO_PROVINCE_TEXT;?></label>                   
                    <select name="province" id="province" onchange="ajaxcity(this.value)" autocomplete="off">
                        <option value=""><?php echo EDITPRO_SELECT_TEXT;?></option>
                        <?php 
                        if($province_list!=''){
                            foreach ($province_list as $province_row){
                                if($user_data->province==$province_row->provId){
                                    $sel='selected';
                                }else{
                                    $sel='';
                                }
                                echo '<option value="'.$province_row->provId.'" '.$sel.'>'.$province_row->provName.'</option>';
                            }
                        }
                        ?>
                    </select>
                    </div>
                </div>  
                <div class="form-field-address-1 form-field form-field" expr="address.city" style="width:50%;">
                    <div>
                    <label for="city"><?php echo EDITPRO_CITY_TEXT;?></label> 
                    <select name="city" id="cityList" onchange="ajaxdistrict(this.value)" autocomplete="off">
                        <option value=""><?php echo EDITPRO_SELECT_TEXT;?></option>
                        <?php if(!empty($city_list) && $user_data->city!='' && $user_data->city!='0'){
                            foreach($city_list as $city_row){
                                if($user_data->city==$city_row->cityId){
                                    $selCity='selected';
                                }else{
                                    $selCity='';
                                }
                                echo '<option value="'.$city_row->cityId.'" '.$selCity.'>'.$city_row->cityName.'</option>';
                            }                            
                        }
                        ?>
                    </select>
                    </div>
                </div>
                <div class="form-field-address-2 form-field form-field" style="width:50%;">
                    <div>
                    <label for="district"><?php echo EDITPRO_DIST_TEXT;?></label>
                    <select name="district" id="districtList" autocomplete="off">
                        <option value=""><?php echo EDITPRO_SELECT_TEXT;?></option>
                        <?php if(!empty($district_list) && $user_data->district!='' && $user_data->district!='0'){
                            foreach($district_list as $district_row){
                                if($user_data->district==$district_row->districtId){
                                    $selDist='selected';
                                }else{
                                    $selDist='';
                                }
                                echo '<option value="'.$district_row->districtId.'" '.$selDist.'>'.$district_row->districtName.'</option>';
                            }                            
                        }
                        ?>
                    </select>
                    </div>
                </div>
                <div class="form-field-zip form-field" field="basicInfo.zip_code_id" form-field="">
                    <div>
                    <label for="zip_code_id"><?php echo EDITPRO_POSTAL_TEXT;?></label>
                    <input type="text" pattern="[0-9]*" inputmode="numeric" autocomplete="off" name="zip_code_id" value="<?=$user_data->zipcode;?>">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-field-address-privacy form-field" field="basicInfo.usa_privacy" form-field="">
                    <div>
                    <label>
                        <input type="radio" value="1" name="usa_privacy" <?php if($business_info->showFullAddress=='Y'){?> checked="checked"<?php } ?>>
                        <?php echo EDITPRO_ADDRESONPRO_TEXT;?>
                    </label>
                    <label>
                        <input type="radio" value="2" name="usa_privacy" <?php if($business_info->showFullAddress=='N'){?> checked="checked"<?php } ?>>
                        <?php echo EDITPRO_CITYONPRO_TEXT;?>
                    </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-field-column-6 form-field" optional="true" form-field="">
                    <div>
                    <label for="phone_number"><?php echo PHONE_NUMBER_PLACEHOLDER;?></label>
                    <input type="text" inputmode="tel" autocomplete="off" placeholder="(555) 555-5555" name="phone_number" value="<?=$user_data->phone?>">
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="wizard-footer" saving="saving" points="47" wizard-footer="">
            <div class="dynamic-row">
              <div class="back-button">
                <span>&nbsp;</span>
              </div>
              <div class="progress-points">
                   <strong><?php echo EDITPRO_PROFCOMP_TEXT;?></strong>
                    <div class="points-bg">
                      <div class="points-bar total_prograss_cls" style="width: <?php echo $totalProgress; ?>%;">
                          <span class="total_prograss"><?php echo $totalProgress; ?>%</span>
                      </div>
                    </div>
              </div>
              <div class="save-button">
                    <button class="bttn blue full-width" data-wizard-footer-button="" type="submit"><?php echo BUTTON_SAVE_CONT;?></button>
              </div>
            </div>
        </div>
       </form>
    </div>

    <div class="form-media cls_basic_popup cls_media" style="display: none;">
    <div scroll-anchor="" class="form-content">
        <h3 class="form-section-header">
            <?php echo EDITPRO_EARN_MEDIA_TEXT; ?>
        </h3><br/>
        <h2 class="form-section-header">
            <?php echo EDITPRO_MANAGEMEDIA_TEXT; ?>
        </h2>
        <div class="media-chooser-container">
            <!-- ngIf: state.message -->
            <!-- ngIf: state.errorMessage -->            
            <div id="mediaPictureDiv">
                <div>
                    <span value="service.id" upload-extra="sav"></span>
                    <!-- ngSwitchWhen: loading -->
                    <!-- ngSwitchWhen: start -->
                    <div class="media-start">
                        <div class="media-type-chooser-column">
                            <div class="media-type-chooser">
                                <h3><?php echo EDITPRO_ADDMEDIA_TEXT;?></h3>
                                <div class="media-type-buttons">
                                    <button upload-trigger="" class="media-type-picture bttn gray small" id="uploadBtn"><?php echo BUTTON_PICTURE;?></button>
                                    <button onclick="showVideo();" class="media-type-video bttn gray small"><?php echo BUTTON_VIDEO;?></button>
                                </div>
                            </div>
                        </div>
                        <div class="media-tip-column">
                            <div class="explanation body-text">
                                <p>
                                    <strong><?php echo EDITPRO_TIP_TEXT;?></strong> <?php echo EDITPRO_PICKEXAMPLE_TEXT;?>
                            </p></div>
                        </div>
                        <div id="progressOuter" class="progress progress-striped active" style="display:none;">
                            <div id="progressBar" class="progress-bar progress-bar-custom"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            </div>
                        </div>
                        <br/><div id="msgBox" style="float:left;padding-top:10px;"></div>
                    </div>
                    <!-- ngSwitchWhen: video -->
                    <!-- ngSwitchWhen: soundcloud -->
                    <!-- ngSwitchWhen: soundcloud-tracks -->
                    <!-- ngSwitchWhen: mp3 -->
                </div><!-- end switch -->
            </div>  
            <div id="mediaVideoDiv" style="display:none;">
                <div>
                    <span value="service.id" upload-extra="sav"></span>
                      <div ng-switch-when="video" class="ng-scope">
                          <form name="videoForm" id="videoForm" method="post">
                            <fieldset>
                                <div class="form-field">
                                <label for="video_url">
                                    Enter the URL or embed code from Youtube or Vimeo
                                </label>
                                <textarea class="video-url" id="video_url" name="video_url"></textarea>
                                </div>
                            </fieldset>
                            <fieldset>
                                <button type="submit" class="bttn gray small">Save</button>
                            </fieldset>
                        </form>
                    </div>                   
                </div>
                <div class="media-start-over">
                   <span onclick="startOver()" class="media-action-link">Start over</span>
                </div>
            </div>
        </div><!-- end chooser -->

        <ul id="mediaMainDiv"> 
            <?php if($media_list!=''){ 
                foreach($media_list as $media_row){ ?>
                    <li class="media-list-item" id="mediaSubDiv<?=$media_row->mediaId?>">
                    <?php if($media_row->mediaType=='Image'){ ?>
                    <div class="media-picture-container media-container">
                        <div style="width: 100px; height: 100px; float:left; overflow: hidden;margin-right: 10px;text-align: center;"><img src="<?=$this->config->config['base_url']?>business_images/thumb/<?=$media_row->mediaPath?>"></div>
                        <div class="picture-caption">
                            <textarea onblur="saveCaption('<?=$media_row->mediaId?>')" id="mediaCaption"></textarea>
                            <div id="captionsSaving[<?=$media_row->mediaId?>]" class="subtext-form" style="display: none;">
                                <?php echo BUTTON_SAVING;?>...
                            </div>
                        </div>
                    </div>
                    <?php }else{ ?>
                    <div class="media-picture-container media-container">
                        <img src="<?=$media_row->mediaVideoThumbPath?>" width="100">                        
                    </div>
                    <?php } ?>
                    <div class="media-picture-container media-container media-button">
                        <span onclick="deleteMedia('<?=$media_row->mediaId?>')" class="media-delete">
                            <?php echo BUTTON_DELETE;?>
                        </span>
                    </div>
                </li>
            <?php }} ?>
        </ul>
    </div>
    
    <div class="wizard-footer" next="bio" previous="basic-info" points="47" wizard-footer="">
        <div class="dynamic-row">
            <div class="back-button">
                <button type="button" class="bttn white full-width " onclick="getNvigatePage('cls_basic_popup','cls_basic_info');"> <?php echo BUTTON_BACK;?></button>
            </div>
            <div class="progress-points">
                <div class="progress-points">
                   <strong><?php echo EDITPRO_PROFCOMP_TEXT;?></strong>
                    <div class="points-bg">
                      <div class="points-bar total_prograss_cls" style="width: <?php echo $totalProgress; ?>%;">
                          <span class="total_prograss"><?php echo $totalProgress; ?>%</span>
                      </div>
                    </div>
                </div>
            </div>
            <div class="save-button">
                <button type="button" class="bttn bttn-last blue full-width" onclick="saveProfileMedia();"><?php echo BUTTON_SAVE_CONT;?> </button>
            </div>
        </div>
    </div>
    </div>
                            
   <div class="cls_basic_popup cls_bio" style="display: none;">
    <form class="form-bio" name="bioForm" id="bioForm">
        <div class="form-content">
            <h3 class="form-section-header">
                <?php echo EDITPRO_EARN_BIO_TEXT; ?>
            </h3><br/>
            <h2 class="form-section-header">
                <?php echo EDITPRO_TELLSTORY_TEXT;?>
            </h2>
            <fieldset>
                <div class="form-field" optional="" field="bioForm.bio" form-field="">
                    <div>
                    <label for="bio" class="ng-scope"><?php echo EDITPRO_BIO_LABEL;?></label>
                    <textarea placeholder="<?php echo EDITPRO_TELLCUSTOMER_PLACEHOLDER;?>" maxlength="240" id="bio" name="bio"><?=$business_info->bioInfo?></textarea>
                    </div>
                </div>
            </fieldset>
            <div class="examples body-text">
                <h4><?php echo EDITPRO_EXAMPLES_TEXT;?></h4>
                <div class="dynamic-row">
                    <div class="column-6 example">
                        <p>
                           <?php echo EDITPRO_IAM_TEXT;?>
                        </p>
                    </div>
                    <div class="column-6 example">
                        <p>
                         <?php echo EDITPRO_IBECAME_TEXT;?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="wizard-footer" previous="media" saving="saving" points="47" wizard-footer="">
            <div class="dynamic-row">
                <div class="back-button">
                    <button type="button" prevent-default="click" class="bttn white full-width" onClick="getNvigatePage('cls_basic_popup','cls_media');"> <?php echo BUTTON_BACK;?></button>
                </div>
                <div class="progress-points">
                  <div class="progress-points">
                   <strong><?php echo EDITPRO_PROFCOMP_TEXT;?></strong>
                    <div class="points-bg">
                      <div class="points-bar total_prograss_cls" style="width: <?php echo $totalProgress; ?>%;">
                          <span class="total_prograss"><?php echo $totalProgress; ?>%</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="save-button">
                    <button class="bttn blue full-width" data-wizard-footer-button="" type="submit"><?php echo BUTTON_SAVE_CONT;?></button>
                </div>
            </div>
        </div>
      </form>
    </div>
   
   <div class="cls_basic_popup cls_services" style="display: none;">
    <form class="form-services" name="servicesForm" id="servicesForm">
        <div class="form-content">
            <h3 class="form-section-header">
                <?php echo EDITPRO_EARN_SERVICES_TEXT; ?>
            </h3><br/>
            <h2 class="form-section-header">
                <?php echo EDITPRO_DESCRIBESERVICE_TEXT;?>
            </h2>
            <fieldset>
                <div class="form-field" form-field="">
                    <div>
                    <label for="description"><?php echo EDITPRO_SERVICES_TEXT;?></label>
                    <textarea placeholder="<?php echo EDITPRO_LISTOFSERVICE_PLACEHOLDER;?>" minlength="100" id="description" name="description"><?=$business_info->bussinessDescription?></textarea>
                    <span class="subtext-form"><?php echo EDITPRO_LISTOFSERVICE_HELP;?></span>                    
                    </div>
                </div>
            </fieldset>
            <div class="examples body-text">
                <h4><?php echo EDITPRO_EXAMPLES_TEXT;?></h4>
                <div class="example">
                    <p>
                      <?php echo EDITPRO_IFYOUWANT_TEXT;?>
                    </p>
                </div>
            </div>
        </div>
        <div class="wizard-footer" previous="bio" saving="saving" points="47" wizard-footer="">
            <div class="dynamic-row">
                <div class="back-button">
                    <button type="button" prevent-default="click" class="bttn white full-width" onClick="getNvigatePage('cls_basic_popup','cls_bio');"> <?php echo BUTTON_BACK;?></button>
                </div>
                <div class="progress-points">
                  <div class="progress-points">
                   <strong><?php echo EDITPRO_PROFCOMP_TEXT;?></strong>
                    <div class="points-bg">
                      <div class="points-bar total_prograss_cls" style="width: <?php echo $totalProgress; ?>%;">
                          <span class="total_prograss"><?php echo $totalProgress; ?>%</span>
                      </div>
                    </div>
                 </div>
                </div>
                <div class="save-button">
                    <button class="bttn blue full-width" data-wizard-footer-button="" type="submit"><?php echo BUTTON_SAVE_CONT;?> </button>
                </div>
            </div>
        </div>
     </form>
    </div>
   
   <div class="cls_basic_popup cls_links" style="display: none;">
    <form class="form-links" name="linksForm" id="linksForm">
        <div class="form-content">
            <h3 class="form-section-header">
                <?php echo EDITPRO_EARN_LINKS_TEXT; ?>
            </h3><br/>
            <h2 class="form-section-header">
                <?php echo EDITPRO_MANAGEBUSSLINKS_TEXT;?>
            </h2>
            <fieldset>
                <div class="form-field" field="linksForm.website" optional="true" form-field="">
                  <div>
                    <label for="website"><?php echo DETAIL_WEBSITE_TEXT;?></label>
                    <input type="text" placeholder="www.your-service.com" id="website" name="website" value="<?=$business_info->website?>">
                  </div>
                  <span class="subtext-form"><?php echo EDITPRO_OPTIONAL_TEXT;?></span></div>
            </fieldset>
<!--            <div class="body-text">
                <h4>Social Media Connections</h4>
                <p>
                    <a target="_blank" href="/profile/credentials">Manage your connections to
                        Twitter, LinkedIn, and Facebook</a> (opens in a new window)
                </p>
            </div>-->
        </div>
        <div class="wizard-footer" previous="services" saving="saving" points="47" wizard-footer="">
            <div class="dynamic-row">
                <div class="back-button">
                    <button type="button" prevent-default="click" class="bttn white full-width" onClick="getNvigatePage('cls_basic_popup','cls_services');"> <?php echo BUTTON_BACK;?></button>
                </div>
                <div class="progress-points">
                    <div class="progress-points">
                        <strong><?php echo EDITPRO_PROFCOMP_TEXT;?></strong>
                         <div class="points-bg">
                           <div class="points-bar total_prograss_cls" style="width: <?php echo $totalProgress; ?>%;">
                          <span class="total_prograss"><?php echo $totalProgress; ?>%</span>
                      </div>
                         </div>
                    </div>
                </div>
                <div class="save-button">
                    <button class="bttn blue full-width" type="submit"><?php echo BUTTON_SAVE_CONT;?> </button>
                </div>
            </div>
        </div>
     </form>
    </div>
    
    <div class="cls_basic_popup cls_business_info" style="display: none;">
     <form class="form-business-info" name="businessInfoForm" id="businessInfoForm">
        <div class="form-content">
            <h2 class="form-section-header">
                <?php echo EDITPRO_EDITBUSS_TEXT;?>
            </h2>
            <fieldset>
                <div class="form-field" optional="true" form-field="">
                   <div>
                    <label for="company"><?php echo EDITPRO_BUSSNAME_TEXT;?></label>
                    <input type="text" id="company" name="company" value="<?=$business_info->businessName?>">
                   </div>
                    <?php /*<span class="subtext-form"><?php echo EDITPRO_OPTIONAL_TEXT;?></span>*/ ?>
                </div>
                <div class="form-field" form-field="">
                   <div>
                    <label><?php echo EDITPRO_BUSSLOGO_TEXT;?></label>
                    <div id="businessLogoDiv">
                    <?php if($business_info->businessLogo!=''){ ?>
                    <div class="media-list-item">                       
                        <div class="media-picture-container media-container">
                            <img alt="Business logo" src="<?=$this->config->config['base_url']?>business_logo/thumb/<?=$business_info->businessLogo?>">
                        </div>
                        <span onclick="deleteLogo()" class="media-delete">
                            <?php echo BUTTON_DELETELOGO;?>
                        </span>
                        <span class="media-delete" style="display: none;">
                            <?php echo BUTTON_DELETING;?>...
                        </span>
                     </div>
                     <?php } ?>
                    </div>
                     <div>                         
                        <div>
                        <div class="upload-logo">
                            <button class="bttn gray tiny" id="uploadBusinessLogoBtn"><?php echo EDITPRO_UPLOADLOGO_TEXT;?></button>
                        </div>
                        <div style="display:none;" class="alert-info" id="msgBoxBusinessImage"></div>   
                        </div>                                
                    </div>
                   </div>                       
                </div>
                <div class="dynamic-row">
                    <div class="form-field-year-founded form-field" optional="" form-field="">
                       <div>
                        <label for="year-founded"><?php echo EDITPRO_YEARFOUND_TEXT;?></label>
                        <input type="number" pattern="/^\d{4}$/" id="year-founded" name="year_founded" value="<?=$business_info->foundingYear?>">
                       </div>
                    </div>
                    <div class="form-field-employee-count form-field" optional="" form-field="">
                       <div>
                        <label for="employee-count"><?php echo EDITPRO_NOOFEMP_TEXT;?></label>
                        <input type="number" pattern="/^\d{1,4}$/" id="employee-count" name="employee_count" value="<?=$business_info->employeeCount?>">
                       </div>
                    </div>
                </div>
                <div class="form-field" form-field="">
                   <div>
                    <label><?php echo EDITPRO_TRAVELPREF_TEXT;?></label>
                    <label class="block">
                        <input type="checkbox" name="preferences[]" value="travel_to_customer" onclick="showMiles(this.checked);" <?php if($business_info->travelToCustomer=='Y'){ ?> checked<?php } ?>>
                        <?php echo EDITPRO_ITRAVEL_TEXT;?>
                    </label>
                    <label class="block">
                        <input type="checkbox" name="preferences[]" value="travel_to_provider" <?php if($business_info->travelToProvider=='Y'){ ?> checked<?php } ?>>
                        <?php echo EDITPRO_CUSTTRAVEL_TEXT;?>
                    </label>
                    <label class="block">
                        <input type="checkbox" name="preferences[]" value="travel_remote" <?php if($business_info->travelRemote=='Y'){ ?> checked<?php } ?>>
                        <?php echo EDITPRO_NEITHER_TEXT;?>
                    </label>
                   </div>
                </div>              
                <div id="travel_tocustomerDiv" class="dynamic-row" <?php if($business_info->travelToCustomer=='Y'){ ?>  <?php }else{ ?> style=" display: none;"<?php } ?>>
                    <div class="column-7">
                        <div class="form-field" form-field="">
                           <div>
                            <label for="service-travel-distance">
                                <?php echo EDITPRO_HOWFAR_TEXT;?>
                            </label>
                            <select name="service-travel-distance" id="service-travel-distance">                                
                                <option value="1" <?php if($business_info->distanceMiles=='1'){ ?> selected<?php } ?>>1 <?php echo EDITPRO_MILE_TEXT;?></option>
                                <option value="2" <?php if($business_info->distanceMiles=='2'){ ?> selected<?php } ?>>2 <?php echo EDITPRO_MILES_TEXT;?></option>
                                <option value="3" <?php if($business_info->distanceMiles=='3'){ ?> selected<?php } ?>>3 <?php echo EDITPRO_MILES_TEXT;?></option>
                                <option value="5" <?php if($business_info->distanceMiles=='5'){ ?> selected<?php } ?>>5 <?php echo EDITPRO_MILES_TEXT;?></option>
                                <option value="10" <?php if($business_info->distanceMiles=='10'){ ?> selected<?php } ?>>10 <?php echo EDITPRO_MILES_TEXT;?></option>
                                <option value="20" <?php if($business_info->distanceMiles=='20'){ ?> selected<?php } ?>>20 <?php echo EDITPRO_MILES_TEXT;?></option>
                                <option value="30" <?php if($business_info->distanceMiles=='30'){ ?> selected<?php } ?>>30 <?php echo EDITPRO_MILES_TEXT;?></option>
                                <option value="40" <?php if($business_info->distanceMiles=='40'){ ?> selected<?php } ?>>40 <?php echo EDITPRO_MILES_TEXT;?></option>
                                <option value="50" <?php if($business_info->distanceMiles=='50'){ ?> selected<?php } ?>>50 <?php echo EDITPRO_MILES_TEXT;?></option>
                                <option value="100" <?php if($business_info->distanceMiles=='100'){ ?> selected<?php } ?>>100 <?php echo EDITPRO_MILES_TEXT;?></option>
                            </select>
                         </div>
                        </div>
                    </div>
                   <!-- <div class="column-5">
                       <img src="images/80s.png" ng-if="service.travel_to_customer" class="service-map ng-scope">
                    </div>-->
                </div>
            </fieldset>
        </div>
        <div class="wizard-footer" previous="links" saving="saving" points="47" wizard-footer="">
            <div class="dynamic-row">
                <div class="back-button">
                    <button type="button" prevent-default="click" class="bttn white full-width" onClick="getNvigatePage('cls_basic_popup','cls_links');"> <?php echo BUTTON_BACK;?></button>
                </div>
                <div class="progress-points">
                    <div class="progress-points">
                        <strong><?php echo EDITPRO_PROFCOMP_TEXT;?></strong>
                         <div class="points-bg">
                           <div class="points-bar total_prograss_cls" style="width: <?php echo $totalProgress; ?>%;">
                                <span class="total_prograss"><?php echo $totalProgress; ?>%</span>
                           </div>
                         </div>
                   </div>
                </div>
                <div class="save-button">
                    <button class="bttn blue full-width" type="submit"><?php echo  BUTTON_SAVE_CONT;?></button>
                </div>
            </div>
        </div>
      </form>
    </div>
 
    <div class="form-refer cls_basic_popup cls_refer"  style="display: none;">
     <div class="alert-hide refer-invalid-error">            
        <div class="alert-error">
            <p><?php echo EDITACC_REFERRAL_INVALID_ERROR_TEXT; ?></p>                        
        </div>
     </div>
        <div class="alert-hide refer-own-error">            
        <div class="alert-error">
            <p><?php echo EDITACC_REFERRAL_OWN_ERROR_TEXT; ?></p>                        
        </div>
     </div>
     <div class="alert-hide refer-already-error">            
        <div class="alert-error">
            <p><?php echo PROFILE_REFERRAL_ALREADY_ERROR_TEXT; ?></p>                        
        </div>
     </div>
     <div class="alert-hide refer-sent-success">            
        <div class="alert-success">
            <p><?php echo PROFILE_REFERRAL_SUCCESS_TEXT; ?></p>                        
        </div>
     </div>
     <form class="form-referral-info" name="businessReferralForm" id="businessReferralForm">
     <div class="form-content">
        <h2 class="form-section-header">
            <?php echo EDITPRO_REFER_TEXT;?>
        </h2>
        <fieldset>            
          <div class="form-field" optional="true" form-field="">
            <div>
             <label for="refer_email"><?php echo EDITPRO_ENTEREMAIL_TEXT;?></label>
             <input type="text" placeholder="<?php echo REGISTER_EMAILADD;?>" id="refer_email" name="refer_email" value="" autocomplete="off">
            </div>           
          </div>
          <div class="form-field" optional="true" form-field="">
            <div>             
             <button type="submit" class="bttn bttn-last blue"><?php echo BUTTON_SEND; ?></button>
            </div>           
          </div>  
        </fieldset>
    </div>
    <div class="wizard-footer" next="q-and-a" previous="business-info">
        <div class="dynamic-row">
            <div class="back-button">
                <button type="button" class="bttn white full-width" onClick="getNvigatePage('cls_basic_popup','cls_business_info');"> <?php echo BUTTON_BACK;?></button>
            </div>
            <div class="progress-points">
              <div class="progress-points">
                   <strong><?php echo EDITPRO_PROFCOMP_TEXT;?></strong>
                    <div class="points-bg">
                      <div class="points-bar total_prograss_cls" style="width: <?php echo $totalProgress; ?>%;">
                          <span class="total_prograss"><?php echo $totalProgress; ?>%</span>
                      </div>
                    </div>
              </div>
            </div>
            <div class="save-button">
                <button type="button" class="bttn bttn-last blue full-width" onClick="getNvigatePage('cls_basic_popup','cls_qa');"><?php echo CONTINUE_BUTTON;?> </button>
            </div>
        </div>
     </div>
    </form>
   </div>

   <div class="cls_basic_popup cls_qa" style="display: none;">
    <form class="form-q-and-a" name="qaForm" id="qaForm">
        <div class="form-content">
            <h3 class="form-section-header">
                <?php echo EDITPRO_EARN_QA_TEXT; ?>
            </h3><br/>
            <h2 class="form-section-header">
                <?php echo EDITPRO_QUEANS_TEXT;?>
                <span class="num-questions-answered">
                <?php echo '<span id="qa_ans_count">'.$answer_count.'</span>'." ".EDITPRO_NOQUEANS_TEXT;?>
                </span>
            </h2>
            <div class="explanation body-text">
                <p>
                    <strong><?php echo EDITPRO_ANSEXTRA_TEXT;?></strong><br>
                    <?php echo EDITPRO_CLIENTSTREAT_TEXT;?>
                </p>
            </div>
            
            <?php if($answer_list!=''){ 
                foreach($answer_list as $answer_row){ ?>
            <fieldset>
                <div class="pod">
                    <div class="pod-content">
                        <label for="answer-lBcMHBzenEspsw">
                            <?php echo $answer_row->questionText; ?>
                        </label>
                        <textarea id="answer_<?=$answer_row->queId?>" name="answer_<?=$answer_row->queId?>"><?php echo $answer_row->answerText; ?></textarea>
                        <span class="subtext-form"><?php echo EDITPRO_OPTIONAL_TEXT;?></span>
                    </div>
                </div>
            </fieldset>
            <?php }} ?>
        </div>
        <div class="wizard-footer" previous="credentials" saving="saving" points="47" wizard-footer="">
            <div class="dynamic-row">
                <div class="back-button">
                    <button type="button" class="bttn white full-width" onClick="getNvigatePage('cls_basic_popup','cls_refer');"> <?php echo BUTTON_BACK;?></button>
                </div>
                <div class="progress-points">
                    <div class="progress-points">
                        <strong><?php echo EDITPRO_PROFCOMP_TEXT;?></strong>
                         <div class="points-bg">
                           <div class="points-bar total_prograss_cls" style="width: <?php echo $totalProgress; ?>%;">
                                <span class="total_prograss"><?php echo $totalProgress; ?>%</span>
                           </div>
                         </div>
                   </div>
                </div>
                <div class="save-button">
                    <button class="bttn blue full-width" data-wizard-footer-button="" type="submit"><?php echo BUTTON_SAVE_CONT;?> </button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="form-reviews cls_basic_popup cls_reviews" style="display: none;">
    <div class="form-content">
        <h3 class="form-section-header">
            <?php echo EDITPRO_EARN_REVIEWS_TEXT; ?>
        </h3><br/>
        <h2 class="form-section-header">
            <?php echo EDITPRO_GETREVIEW_TEXT;?>
        </h2>
        <div class="explanation body-text">
            <p style="display: none;">
                <strong class="ng-binding">
                    <?php echo EDITPRO_NOREVIEW_TEXT;?>
                </strong>
                <a target="_blank" href="/profile/reviews"><?php echo EDITPRO_MANAGEREVIEW_TEXT;?></a>
            </p>
            <p ng-show="service.reviews.length == 0">
                <strong><?php echo EDITPRO_SERVICENOREVIEW_TEXT;?></strong>
                <br>
                <?php echo EDITPRO_ASINGLEREVIEW_TEXT;?>
            </p>
        </div>
        <p>
            <?php echo EDITPRO_EMAILPAST_TEXT;?>
            <!--(<a target="_blank" href="/reviews/services/euuJcYe2meE0ZQ/write"><?php //echo EDITPRO_TAKEALOOK_TEXT;?></a>)-->
        </p>
        <?php
        $share_url = $this->config->config['base_url'].'bid/add_review/'.$this->session->userdata('userId');
        ?>
        <fieldset>
            <div class="form-field form-field-review-link">
                <input type="text" value="<?php echo $share_url; ?>" readonly>
            </div>
        </fieldset>
        <ul class="link-distribution-tips">
            <li class="email"><strong><a href="mailto:?Subject=Share%20URL&Body=Review me at SeekMi <?php echo $share_url; ?>" target="_blank"><?php echo EDITPRO_EMAILIT_TEXT;?></a></strong> <?php echo EDITPRO_TOPASTCLIENT_TEXT;?></li>
            <li class="twitter"><strong><a href="http://twitter.com/home?status=Review me at SeekMi <?php echo $share_url; ?>" target="_blank"><?php echo EDITPRO_TWEETIT_TEXT;?></a></strong> <?php echo EDITPRO_TOFOLLOWERS_TEXT;?></li>
            <li class="facebook"><strong><a href="http://www.facebook.com/share.php?u=<?php echo $share_url; ?>" target="_blank"><?php echo EDITPRO_SHAREIT_TEXT;?></a></strong> <?php echo EDITPRO_ONFACEBOOK_TEXT;?></li>
        </ul>
    </div>
    <div class="wizard-footer" next="reset" previous="q-and-a" points="47" wizard-footer="">
        <div class="dynamic-row">
            <div class="back-button">
                <button type="button" class="bttn white full-width" onClick="getNvigatePage('cls_basic_popup','cls_qa');" > <?php echo BUTTON_BACK;?></button>
           </div>
              <div class="progress-points">
                    <div class="progress-points">
                        <strong><?php echo EDITPRO_PROFCOMP_TEXT;?></strong>
                         <div class="points-bg">
                           <div class="points-bar total_prograss_cls" style="width: <?php echo $totalProgress; ?>%;">
                                <span class="total_prograss"><?php echo $totalProgress; ?>%</span>
                           </div>
                         </div>
                   </div>
                </div>
            <div class="save-button">
                <button class="bttn bttn-last blue full-width" id="hide1"><?php echo FINISHED_BUTTON;?> </button>
            </div>
        </div>
    </div>
</div>
</div></div>
<? include('footer_mini_view.php'); ?>
<?php if($user_data->newRegisterProsFlag=='Y'){ ?>   
<div class="support-options curtain profile-loggedin-popup" style="display: none;">
    <div class="box box-modal box-secondary-emphasized curtain-inner">        

        <div class="box-content">
            <p class="body-text">
              
                <?php echo PROFILE_WELCOME." ".$user_data->firstname." ".$user_data->lastname."!"; ?><br/><?php echo PROFILE_FIRST_LOGGEDIN_STARTUP_TEXT; ?>
             
            </p>
            <p style="text-align: center;"><button class="bttn blue close-profile-loggedin-popup" type="button">Close</button></p>
        </div>
    </div>
</div>   
 <?php } ?>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery-validate.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/SimpleAjaxUploader.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/profile.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.bxslider.js"></script> 
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.tokenize.js"></script>
<script type="text/javascript">  
<?php if($flag=='edit'){ ?>
  $(document).ready(function () { 
    getNvigatePage('cls_basic_popup','cls_basic_info');
  });
<?php } ?>
<?php if($user_data->newRegisterProsFlag=='Y'){ ?>
     $(document).ready(function () {  
           $(".profile-loggedin-popup").show();
           
           $(".close-profile-loggedin-popup").click(function(){
                $.post(Host+'profile/change_first_profile_loggedin_status', { uid:'<?=$user_data->userId?>' }, function(data) {
                    if(data=='success'){
                     location.href='<?=$this->config->config['base_url']?>profile/services/edit';
                    }
                 });
               
           });
     });
<?php } ?>
$('#services').tokenize({
    datas: "<?=$this->config->config['base_url']?>home/get_services_pros"
  });
$('.bxslider').bxSlider({    
    auto: true,
    video: true,
    captions: true,
    slideWidth: 580,
    minSlides: 2,
    maxSlides: 3,
    slideMargin: 10,
    hideControlOnEnd: true
});

function ajaxcity(cid){
    
    $.post('<?=$this->config->config['base_url']?>pros/getCityList', { id:cid }, function(data) {
        $('#cityList').html(data);
        $('#districtList').html('<option value="">--Select--</option>');
        if(cid==''){
            $('#districtList').html(data);
        }        
    });   
}

function ajaxdistrict(cid){
    
    $.post('<?=$this->config->config['base_url']?>pros/getDistrictList', { id:cid }, function(data) {
        $('#districtList').html(data);
    });   
}

</script>
</body>
</html>

