<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="mainimage" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$page_title?></title>
<meta content="<?=$this->config->config['base_url']?>pros/register" property="og:url"/>
<meta content="<?=$page_title?>" property="og:title"/>
<meta content="Seekmi" property="og:site_name"/>
<meta property="og:description" content="Get things done by hiring experienced local service professionals." />
<meta property="og:image" content="<?=$this->config->config['base_url']?>images/LogoSymbolWordSquare.png"/>
<link rel="icon" type="image/png" href="<?=$this->config->config['base_url']?>images/fev.png">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/signup.css">
 <style type="text/css">
    .help-block {
    color: #d51818;
    display: block;
    margin-bottom: 10px;
    margin-top: 5px;
    font-size: 11.2px;
   }
   .wrapper.content {
    margin: 63px auto !important;
    min-height: 450px;
}
   #services\[\]-error,#preferences\[\]-error{
       float:left;
       font-size: 12px;
       margin-left: -28px;
       margin-top: -37px;
   }
   .has-error .checkbox, .has-error .checkbox-inline, .has-error label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label {
    color: #d51818;
   }
   .has-error input,.has-error select,.has-error textarea{
      border-color: #a94442;
      box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset; 
   }
   .tokenize-sample{
       width:100%;
   }
    .glorious-header1{
    background: none repeat scroll 0 0 #fff!important;    
    }
    html body {
    background:#fff!important;
    }

    .glorious-header {
    background: #fff!important;   
    height: 80px;
    position: relative;
    z-index: 800;
    }
 </style>
<?php include('before_head_view.php'); ?>
</head>
<body class="primo primo-responsive primo-fluid box-shadow multiple-backgrounds">
    <?php include('common_view.php'); ?>
<div class="glorious-header glorious-header1" data-section="header">
<div class="wrapper">
    <div class="row header-row">
        <div class="header" style="margin-bottom:10px;">
    <a class="logo" href="<?=$this->config->config['base_url']?>"><img alt="Seekmi" src="<?=$this->config->config['base_url']?>images/seekmilogo1.png"></a>
    </div>
  </div>
</div>
</div>
<div class="wrapper content">
    <div class="newimage">
        <h1 style="" class="header-copy">            
        </h1>
         <div id="main_div_5">
            <form novalidate class="box account" name="signup_customerlist_form" method="POST" id="signup_customerlist_form" action="<?=$this->config->config['base_url']?>customerlist/submit" style="background-color:#fff; margin-top:57px !important; margin-left:40px !important; margin-right:40px !important; -webkit-box-shadow: 0px -1px 23px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 0px -1px 23px 0px rgba(0,0,0,0.75);
box-shadow: 0px -1px 23px 0px rgba(0,0,0,0.75); border-radius: 4px;">
                <div class="box-header">
                    <h2>Raih kesempatan mendapatkan pelanggan!</h2>
		    <h3>Agar mudah dihubungi pelanggan, lengkapi data dan layanan jasa Anda.</h3>                  
                </div>
                <input type="hidden" name="serviceName" value="<?=$service_name?>"/>
                <input type="hidden" name="subserviceName" value="<?=$subservice_name?>"/>
                <div class="box-content">
                    <fieldset>
                        <div class="dynamic-row">
                            <div class="form-field-name-segment form-field form-field" expr="account.firstName" field="">
                               <div>
                                <label for="first-name">Nama Depan</label>
                                <input type="text" minlength="2" required="" name="firstName" id="first-name">
                               </div>
                            </div>
                            <div class="form-field-name-segment form-field form-field" expr="account.lastName" field="">
                              <div>
                                <label for="last-name">Nama Belakang</label>
                                <input type="text" minlength="2" required="" name="lastName" id="last-name">
                              </div>
                            </div>
                        </div>
                        <div class="dynamic-row">
                            <div class="form-field-email form-field form-field" expr="account.email" field="">
                              <div>
                                <label for="usr_email">Alamat Email</label>
                                <input type="email" required="" name="usr_email" id="usr_email">
                              </div>
                            </div>
                            <div class="form-field-phone form-field form-field" expr="account.phone" field="">
                             <div>
                                <label for="phone">Nomor Telepon</label>
                                <input type="phone" pattern="[0-9]*" required="" name="phone" id="phone">
                              </div>
                            </div>
                        </div>                       
                    </fieldset>
                  
                </div>
                <div class="form-field form-field-nav box-footer">
                    <div class="nav-container" role="presentation">
                        <button  class="bttn" type="submit" style="width:100%">
                           <span>Lihat Daftar Client Sekarang</span>                           
                        </button>
                    </div>
                </div>
            </form>
        </div>
        
        
    </div>
</div>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.validate.js"></script>
<script type="text/javascript">
var form = $("#signup_customerlist_form");
var validator = form.validate({
        errorElement: 'span',
        errorClass: 'help-block',                    
        highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-field').addClass("has-error");
        },
        unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-field').removeClass("has-error");
        },
        rules: {
             firstName:{
                required: true 
             },
             lastName:{
                required: true 
             },
             usr_email:{
                required: true,
                email:true
                //remote: "<?=$this->config->config['base_url']?>user/check_email_exists"
             },
             phone:{
                required: true 
             }
        },
        messages:{               
               /*usr_email: {                               
                    remote: "Sorry, that email is already taken"
               }*/

        }
});
$('#usr_email').blur(function() {
    $('#usr_email').val($.trim($('#usr_email').val()));
    validator.element('#usr_email');
});
</script>
</body>
</html>
      
      
     