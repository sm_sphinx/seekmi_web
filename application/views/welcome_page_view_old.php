<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie7 "><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 "><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="mainimage" lang="en"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?=$page_title?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/icons.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/core.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/style.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/zenbox.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/login.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/avenir-next.css">
<link rel="stylesheet" type="text/css" media="all" href="<?=$this->config->config['base_url']?>css/signup.css">
</head>
<body class="primo primo-responsive primo-fluid box-shadow multiple-backgrounds landing-page">
<div class="glorious-header glorious-header1" data-section="header">
<div class="wrapper"><div class="row header-row">
<div class="header"><a class="logo" href="<?=$this->config->config['base_url']?>"><img alt="SeekMi" src="<?=$this->config->config['base_url']?>images/Pintack_orange_152x29beta.png"></a>
    </div>
<div class="header-middle-container"></div>
</div></div></div>

<div class="wrapper content">
    <div style="" class="hook">
        <form novalidate name="hook" class="">            
            <h1>
                Meet new customers
                <div class="subtitle">
                    SeekMi sends pros like you
                     requests from customers. You 
                    decide who to respond to and send a quote.
                </div>
            </h1>

            <fieldset>
                <label>
                    What do you do?
                </label>
                <div class="form-field form-field-category">
                    <select name="category" id="category" onchange="setDisable(this.value);">
                        <option value="">Select a profession</option>
                        <?php if($categories!=''){ 
                            foreach($categories as $cat_row){ ?>
                        <option value="<?=$cat_row->catId?>" <?php if($catId==$cat_row->catId){ ?> selected="selected"<?php } ?>><?=$cat_row->categoryName?></option>
                        <?php }} ?>
                    </select>                   
                </div>
                <div class="form-field form-field-nav">
                    <button type="button" class="bttn get-started-btn">
                        Get Started
                    </button>
                </div>
<!--                <a class="how-it-works" href="/pros/how-Pintack-works">How Pintack works</a>-->
            </fieldset>            
            </div>
        </form>
    </div>

    
</div>
<div id="feedback-tab"><a data-show-feedback="" href="#">Contact Us</a></div>
<script src="<?=$this->config->config['base_url']?>js/fbds.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/insight.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/conversion_async.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/quant.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/bat.js" type="text/javascript"></script>
<script src="<?=$this->config->config['base_url']?>js/tag.js" async=""></script>
<script type="text/javascript" src="<?=$this->config->config['base_url']?>js/jquery.js"></script>
<script type="text/javascript">
     function setDisable(obj){
         if(obj!=''){
             $('.get-started-btn').attr('disabled',false);
         }else{
             $('.get-started-btn').attr('disabled',true);
         }
     }
     $(document).ready(function () {
         if($('#category').val()==''){
            $('.get-started-btn').attr('disabled',true); 
         }else{
            $('.get-started-btn').attr('disabled',false); 
         }
     });
     $('.get-started-btn').click(function(){
         var catId=$('#category').val();
         if(catId!=''){
            location.href='<?=$this->config->config['base_url']?>pros/register/'+catId;
         }
     })
</script>
</body>
</html>
      
     