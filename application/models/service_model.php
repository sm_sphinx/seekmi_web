<?php
class Service_model extends CI_Model
{    
    function __construct()
    {
        parent:: __construct();
    }

    function getCategories()
    {
        $this->db->select('catId,categoryName');
        $this->db->from('tblcategory');       
        $this->db->where('status','Y');
        $query = $this->db->get();		
        return $query->result();
    }
    
    function getServicesByCategoryId($id){
        $this->db->select('serviceId,serviceName');
        $this->db->from('tblservices');   
        $this->db->where('categoryId',$id); 
        $this->db->where('parentId','0');  
        $this->db->where('status','Y');  
        $this->db->order_by("order", "asc");
        $this->db->order_by("serviceName", "asc");
        $query = $this->db->get();		
        return $query->result();
    }
    
    function getSubservicesByServiceId($id){
        $this->db->select('tblsubservices.subserviceId,tblsubservices.subserviceName');
        $this->db->from('tblservicemapping');   
        $this->db->join('tblsubservices', 'tblsubservices.subserviceId = tblservicemapping.subserviceId');  
        $this->db->where_in('tblservicemapping.serviceId',$id);  
        $this->db->where('tblsubservices.status','Y');  
        $this->db->group_by('tblsubservices.subserviceName');        
        $this->db->order_by("tblsubservices.subserviceName", "asc");
        $query = $this->db->get();		
        return $query->result();
    }
    
    function getAllServicesByKeyword($term){
       // if (get_cookie('language') === 'indonesia') {
            
            $sql='select * from (select subserviceId as serviceId,subserviceNameIND as serviceName from tblsubservices where status="Y"';
            if($term!=''){
                $sql.=' and subserviceNameIND like "%'.$term.'%"';
            }
            $sql.=' group by subserviceNameIND';
            $sql.=' union select subserviceId as serviceId,subserviceName as serviceName from tblsubservices where status="Y"';
            if($term!=''){
                $sql.=' and subserviceName like "%'.$term.'%"';
            }
            $sql.=' union select tagId as serviceId,serviceTag as serviceName from tblsubserviceTags';
            if($term!=''){
                $sql.=' where serviceTag like "%'.$term.'%"';
            }
            $sql.=' group by serviceTag) as t order by t.serviceName asc';  
            
            $query=$this->db->query($sql);
            //echo $this->db->last_query();
            return $query->result();
        /*}else{
            $this->db->select('subserviceId as serviceId,subserviceName as serviceName');
            $this->db->from('tblsubservices');         
            $this->db->where('status','Y'); 
            if($term!=''){
              $this->db->like('subserviceName', $term);   
            }
            $this->db->group_by('subserviceName');
            $this->db->order_by('subserviceName','asc');
            $query = $this->db->get();
            //echo $this->db->last_query();
            return $query->result();
        }*/
        
    }
    
    function getAllCategory($start=1, $per_page=10,$keyword)
    {            
        $this->db->select();
        $this->db->from('tblcategory');	
        //$this->db->where('status','Y');
        if($keyword!=''){
           $where = " (categoryName LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        } 
        $this->db->order_by('categoryName', 'ASC');
        $this->db->limit($start, $per_page);
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->result();
    }

    function getAllCategoryCount($keyword)
    {           
        $this->db->select();
        $this->db->from('tblcategory');	
        //$this->db->where('status','Y');	
        if($keyword!=''){
           $where = " (categoryName LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        }       
        $query = $this->db->get();		
        return $query->num_rows();
    }    
    
    function getCategoryInfo($id)
    {
        $this->db->select();
        $this->db->from('tblcategory');		
        $this->db->where('tblcategory.catId', $id); 
        $query = $this->db->get();		
        return $query->row();
    }
    
    function checkCategoryExists($catName,$catId)
    {		
        $this->db->select();
        $this->db->from('tblcategory');		
        $this->db->where('categoryName', $catName); 
        if($catId!=0)
        {
           $this->db->where('catId !=', $catId); 
        }        
        $query = $this->db->get();		
        return $query->num_rows();
    }
    
    function getSubServicesList()
    {            
        $this->db->select();
        $this->db->from('tblsubservices');	
        $this->db->where('status','Y');        
        $this->db->order_by('subserviceName', 'ASC');       
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->result();
    }
    
    function getAllSubServices($start=1, $per_page=10,$keyword)
    {            
        $this->db->select();
        $this->db->from('tblsubservices');	
        //$this->db->where('status','Y');
        if($keyword!=''){
           $where = " (subserviceName LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        } 
        $this->db->order_by('subserviceName', 'ASC');
        $this->db->limit($start, $per_page);
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->result();
    }

    function getAllSubServicesCount($keyword)
    {           
        $this->db->select();
        $this->db->from('tblsubservices');	
        //$this->db->where('status','Y');	
        if($keyword!=''){
           $where = " (subserviceName LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        }       
        $query = $this->db->get();		
        return $query->num_rows();
    }    
    
    function getSubServiceInfo($id)
    {
        $this->db->select();
        $this->db->from('tblsubservices');		
        $this->db->where('tblsubservices.subserviceId', $id); 
        $query = $this->db->get();		
        return $query->row();
    }
    
    function checkSubServiceExists($servName,$servId)
    {		
        $this->db->select();
        $this->db->from('tblsubservices');		
        $this->db->where('subserviceName', $servName); 
        if($servId!=0)
        {
           $this->db->where('subserviceId !=', $servId); 
        }        
        $query = $this->db->get();		
        return $query->num_rows();
    }
    
    function getAllServices($start=1, $per_page=10,$keyword,$cat)
    {            
        $this->db->select('tblservices.serviceId,tblservices.serviceName,tblservices.status,tblcategory.categoryName');
        $this->db->from('tblservices');	      
        $this->db->join('tblcategory', 'tblcategory.catId = tblservices.categoryId');        
        if($keyword!=''){
           $where = " (tblservices.serviceName LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        }
        if($cat!=''){
           $this->db->where('tblservices.categoryId',$cat);
        }
        $this->db->order_by('tblservices.serviceName', 'ASC');
        $this->db->limit($start, $per_page);
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->result();
    }

    function getAllServicesCount($keyword,$cat)
    {           
        $this->db->select();
        $this->db->from('tblservices');	
        $this->db->join('tblcategory', 'tblcategory.catId = tblservices.categoryId');   
        //$this->db->where('status','Y');	
        if($keyword!=''){
           $where = " (tblservices.serviceName LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        }
        if($cat!=''){
           $this->db->where('tblservices.categoryId',$cat);
        }
        $query = $this->db->get();		
        return $query->num_rows();
    }    
    
    function getServiceInfo($id)
    {
        $this->db->select('tblservices.serviceId,tblservices.serviceName,tblservices.serviceNameIND,tblservices.status,tblservices.categoryId,tblservices.serviceSlug');
        $this->db->from('tblservices');	
        $this->db->join('tblcategory', 'tblcategory.catId = tblservices.categoryId');  
        $this->db->where('tblservices.serviceId', $id); 
        $query = $this->db->get();		
        return $query->row();
    }
    
    function checkServiceExists($servName,$servId)
    {		
        $this->db->select();
        $this->db->from('tblservices');		
        $this->db->where('serviceName', $servName); 
        if($servId!=0)
        {
           $this->db->where('serviceId !=', $servId); 
        }        
        $query = $this->db->get();		
        return $query->num_rows();
    }
    
    function getSelectedSubServices($id)
    {
        $this->db->select('tblservicemapping.subserviceId,tblsubservices.subserviceName');
        $this->db->from('tblservicemapping');
        $this->db->join('tblsubservices', 'tblsubservices.subserviceId = tblservicemapping.subserviceId');  
        $this->db->where('tblservicemapping.serviceId',$id);
        $this->db->where('tblsubservices.status','Y');
        $query = $this->db->get();	        
        return $query->result();
    }
    
    function getQuestionsByServiceId($id)
    {
        $this->db->select();
        $this->db->from('tblquestions');	       
        $this->db->where('serviceId',$id);
        $this->db->where('type','informatic');
        $this->db->where('active','Y');
        $query = $this->db->get();	        
        return $query->result();
    }
    
    function getQuestionForService($id,$type){
        $this->db->select();
        $this->db->from('tblquestions');	       
        $this->db->where('serviceId',$id);
        $this->db->where('type',$type);       
        $query = $this->db->get();	        
        if($query->num_rows()==0)
        {
            return false;
        }else{
            $rw=$query->row();
            return $rw->questionId;
        }
    }
    
    function getQuestionDetails($id){
        $this->db->select();
        $this->db->from('tblquestions');	       
        $this->db->where('questionId',$id);         
        $query = $this->db->get();
        return $query->row();
    }
    
    function getOptionsByQuestion($qid){
        $this->db->select();
        $this->db->from('tblansweroptions');	       
        $this->db->where('questionId',$qid);
        $this->db->where('active','Y');       
        $query = $this->db->get();	        
        return $query->result();
    }
    
    function getAllOptionsByQuestion($qid){
        $this->db->select();
        $this->db->from('tblansweroptions');	       
        $this->db->where('questionId',$qid);              
        $query = $this->db->get();	        
        return $query->result();
    }
    
    function getOptionCountByQuestion($qid){
        $this->db->select();
        $this->db->from('tblansweroptions');	       
        $this->db->where('questionId',$qid);
        $this->db->where('active','Y');       
        $query = $this->db->get();	        
        return $query->num_rows();
    }
    
    function checkOptionExists($optiontext,$qid){
        $this->db->select();
        $this->db->from('tblansweroptions');	       
        $this->db->where('questionId',$qid);
        $this->db->where('optionText',$optiontext);       
        $query = $this->db->get();	        
        if($query->num_rows()==0)
        {
            return false;
        }else{
            $rw=$query->row();
            return $rw->questionId;
        }
    }
    
    function getAllServicesName(){
        $this->db->select('tblsubservices.subserviceId,tblsubservices.subserviceName');
        $this->db->from('tblsubservices');                  
        $this->db->where('tblsubservices.status','Y');  
        $this->db->group_by('tblsubservices.subserviceName');        
        $this->db->order_by("tblsubservices.subserviceName", "asc");
        $query = $this->db->get();		
        return $query->result();
    }
    
    
    function getUserServices($uid)
    {
        $this->db->select('serviceId,,serviceText');
        $this->db->from('tbluserservices');	       
        $this->db->where('userId',$uid);
        $this->db->where('isSubservice','N'); 
        $query = $this->db->get();	        
        if($query->num_rows()==0)
        {
            return false;
        }else{
            $arr=array();
            foreach($query->result() as $row){
               $arr[]=$row->serviceId;  
            }   
            return $arr;
        }
    }
    
    function getUserSubServices($uid)
    {
        $this->db->select('serviceId,,serviceText');
        $this->db->from('tbluserservices');	       
        $this->db->where('userId',$uid);
        $this->db->where('isSubservice','Y'); 
        $query = $this->db->get();	        
        if($query->num_rows()==0)
        {
            return false;
        }else{
            $arr=array();
            foreach($query->result() as $row){
               $arr[]=$row->serviceId;  
            }   
            return $arr;
        }
    }
    
    function getAllServices1(){
        $this->db->select('subserviceId,subserviceName');
        $this->db->from('tblsubservices');	       
        $this->db->where('serviceSlug','');
        $this->db->where('status','Y'); 
        $query = $this->db->get();
        return $query->result();
    }
    
    function getAllJobRequestCount($keyword)
    {
        $this->db->select('tbluserprojects.projectId,tbluserprojects.status,tbluserprojects.createdDate,tblsubservices.subserviceName,tbluser.firstname,tbluser.lastname');
        $this->db->from('tbluserprojects');	
        $this->db->join('tblsubservices', 'tbluserprojects.serviceId = tblsubservices.subserviceId');    
        $this->db->join('tbluser', 'tbluserprojects.userId = tbluser.userId');           
        if($keyword!=''){
           $where = " (tblsubservices.subserviceName LIKE '%" . $keyword . "%' OR "
                        . "tbluser.firstname LIKE '%" . $keyword . "%' OR "
                        . "tbluser.lastname LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        }        
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->num_rows();
    }
    
    function getAllJobRequest($start=1,$per_page=10,$keyword)
    {
        $this->db->select('tbluserprojects.projectId,tbluserprojects.status,tbluserprojects.createdDate,tblsubservices.subserviceName,tbluser.firstname,tbluser.lastname');
        $this->db->from('tbluserprojects');	
        $this->db->join('tblsubservices', 'tbluserprojects.serviceId = tblsubservices.subserviceId');    
        $this->db->join('tbluser', 'tbluserprojects.userId = tbluser.userId');           
        if($keyword!=''){
           $where = " (tblsubservices.subserviceName LIKE '%" . $keyword . "%' OR "
                        . "tbluser.firstname LIKE '%" . $keyword . "%' OR "
                        . "tbluser.lastname LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        }
        $this->db->limit($start, $per_page);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
    
   
    function getCustomerRequestDetails($id)
    {
        $this->db->select('a.projectId,a.prosId,a.status,b.firstname,b.lastname,a.addedOn,a.readFlag,b.userPhoto,c.businessName,c.website,b.phone,a.quoteMessage,a.requestId');
        $this->db->from('tblquoterequest as a');           
        $this->db->join('tbluser as b','a.prosId=b.userId');
        $this->db->join('tbluserprovider as c','c.userId=b.userId');           
        $this->db->where('a.requestId', $id); 
        $query = $this->db->get();	           
        return $query->row();
    }
    
    function getAllPackage($start=1, $per_page=10,$keyword)
    {            
        $this->db->select();
        $this->db->from('tblCreditPackages');	
        //$this->db->where('status','Y');
        if($keyword!=''){
           $where = " (packagename_en LIKE '%" . $keyword . "%' OR "
                   . "packagename_id LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        } 
        $this->db->order_by('packagename_en', 'ASC');
        $this->db->limit($start, $per_page);
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->result();
    }

    function getAllPackageCount($keyword)
    {           
        $this->db->select();
        $this->db->from('tblCreditPackages');	
        //$this->db->where('status','Y');	
        if($keyword!=''){
           $where = " (packagename_en LIKE '%" . $keyword . "%' OR "
                   . "packagename_id LIKE '%" . $keyword . "%')";
           $this->db->where($where, NULL, FALSE);
        }       
        $query = $this->db->get();		
        return $query->num_rows();
    }
    
    function getPackageInfo($id)
    {
        $this->db->select();
        $this->db->from('tblCreditPackages');		
        $this->db->where('tblCreditPackages.packageId', $id); 
        $query = $this->db->get();		
        return $query->row();
    }
    
    function checkPackageExists($packagename_en,$packagename_id,$packageId)
    {
        $where = "(packagename_en = '" . $packagename_en . "' OR "
                   . "packagename_id = '" . $packagename_id . "')";
        $this->db->select();
        $this->db->from('tblCreditPackages');		
        $this->db->where($where); 
        if($packageId!=0)
        {
           $this->db->where('packageId !=', $packageId); 
        }        
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    function getSubServicesTagsBySubserviceId($id){
        $this->db->select("GROUP_CONCAT(serviceTag) as tags");
        $this->db->from('tblsubserviceTags');	       
        $this->db->where('subServiceId',$id);
        $this->db->where('isEnglish','Y');
        $query = $this->db->get();	        
        if($query->num_rows() > 0){
          return $query->row();
        }else{
          return false;  
        }
    }
    
    function getSubServicesTagsIDBySubserviceId($id){
        $this->db->select("GROUP_CONCAT(serviceTag) as tags");
        $this->db->from('tblsubserviceTags');	       
        $this->db->where('subServiceId',$id);
        $this->db->where('isEnglish','N');
        $query = $this->db->get();	
        if($query->num_rows() > 0){
          return $query->row();
        }else{
          return false;  
        }
    }
   
    function getSuggestedServiceCount($keyword)
    {
        $this->db->select();
        $this->db->from('tblsuggestedservices');	           
        if($keyword!=''){
           $where = " serviceName LIKE '%" . $keyword . "%'";
           $this->db->where($where, NULL, FALSE);
        }        
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->num_rows();
    }
    
    function getSuggestedService($start=1,$per_page=10,$keyword)
    {
        $this->db->select();
        $this->db->from('tblsuggestedservices');	           
        if($keyword!=''){
           $where = " serviceName LIKE '%" . $keyword . "%'";
           $this->db->where($where, NULL, FALSE);
        }     
        $this->db->limit($start, $per_page);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
    
    /** beta098 **/
    function getQuestionariesList($serviceText)
    {        
//        $this->db->select('a.*');
//        $this->db->from('tblquestions a');  
//        $this->db->join('tblsubservices b','a.serviceId=b.subserviceId');         
//        $this->db->where('a.active','Y'); 
//        if($serviceText!=''){
//          $this->db->where('b.subserviceName', $serviceText);   
//        }         
//        $query = $this->db->get();
        if($serviceText!=''){
          $str=' and ((b.subserviceName="'.$serviceText.'") or (b.subserviceNameIND="'.$serviceText.'"))';
        }else{
          $str='';  
        }
        if (get_cookie('language') === 'indonesia') {
          $query=$this->db->query("SELECT a.questionId,a.questionNameIndonesion as questionName,a.type,a.fieldType,a.serviceId,a.active, b.informaticType as informatic, b.detailsType as details, b.timeType as `time`,b.travelType as travel, b.durationType as duration, b.zipType as zipcode FROM tblquestions as a join tblsubservices as b on(a.serviceId=b.subserviceId) where a.active='Y' ".$str." ORDER BY FIELD (a.type,'informatic','details','travel','time','duration','zipcode')");        
        }else{
          $query=$this->db->query("SELECT a.questionId,a.questionName,a.type,a.fieldType,a.serviceId,a.active, b.informaticType as informatic, b.detailsType as details, b.timeType as `time`, b.travelType as travel, b.durationType as duration, b.zipType as zipcode FROM tblquestions as a join tblsubservices as b on(a.serviceId=b.subserviceId) where a.active='Y' ".$str." ORDER BY FIELD (a.type,'informatic','details','travel','time','duration','zipcode')");          
        }
        //echo $this->db->last_query();
        return $query->result();
    }
    
    function getOptionList($id)
    {
        if (get_cookie('language') === 'indonesia') {
          $this->db->select('optionId,optionTextIndonesion as optionText,questionId,optionType,active');
        }else{
          $this->db->select('optionId,optionText,questionId,optionType,active'); 
        }
        $this->db->from('tblansweroptions');  
        $this->db->where('questionId', $id);       
        $this->db->where('active','Y');                     
        $query = $this->db->get();       
        return $query->result();
    }
    
    function getUserProjectList($uid)
    {        
        $this->db->select('a.projectId,a.serviceId,b.subserviceName as serviceName,a.status,a.createdDate');
        $this->db->from('tbluserprojects a');  
        $this->db->join('tblsubservices b','a.serviceId=b.subserviceId');         
        $this->db->where('a.userId',$uid);          
        $query = $this->db->get();        
        return $query->result();
    }
    
    function getExpertsByProject($id,$lat,$lng,$miles)
    {
        $query=$this->db->query("SELECT a.userId,b.firstname,b.lastname,b.email,( 3959 * acos( cos( radians(".$lat.")) * cos( radians(a.latitude)) * cos(radians(a.longitude) - radians(".$lng.")) + sin(radians(".$lat.")) * sin(radians(a.latitude)))) AS miles "
                . "FROM tbluserprovider as a join tbluser as b on(a.userId=b.userId) left join tbluserservices as c on(a.userId=c.userId) "
                . "where c.serviceId='".$id."' and b.status='Y' group by a.userId HAVING miles <= '".$miles."' ORDER BY a.rating desc");        
        //echo $this->db->last_query();
        //die;
        return $query->result();
    }
    
    function getProjectExperts($id){
        $this->db->select('c.businessName,a.quotePrice,a.priceType,a.quoteMessage,a.addedOn,a.prosId as professionalId,c.reviewCount,c.rating,a.status,a.requestId,c.providerId,b.userPhoto,b.facebookProfileId');
        $this->db->from('tblquoterequest a');  
        $this->db->join('tbluser b','a.prosId=b.userId');         
        $this->db->join('tbluserprovider c','c.userId=b.userId');         
        $this->db->where('a.projectId',$id);   
        $this->db->where('a.status !=','Pending');   
        $query = $this->db->get(); 
       // echo $this->db->last_query();
        return $query->result();
    }
    
    function getUserProjectDetails($id)
    {
        $this->db->select('a.projectId,a.serviceId,b.subserviceName,a.status,a.createdDate,a.zipcode');
        $this->db->from('tbluserprojects a');  
        $this->db->join('tblsubservices b','a.serviceId=b.subserviceId');         
        $this->db->where('a.projectId',$id);          
        $query = $this->db->get();        
        return $query->row();
    }
    
    function getExpertDetails($id)
    {
        $this->db->select('b.firstname,b.lastname,d.cityName,e.provName,b.phone,c.businessName,a.quotePrice,a.priceType,a.quoteMessage,a.addedOn,a.prosId as professionalId,c.reviewCount,c.rating,a.status,a.requestId,c.providerId,b.userPhoto,b.facebookProfileId,c.website,a.modifiedOn');
        $this->db->from('tblquoterequest a');  
        $this->db->join('tbluser b','a.prosId=b.userId');         
        $this->db->join('tbluserprovider c','c.userId=b.userId');    
        $this->db->join('tblcitylist as d','b.city=d.cityId');
        $this->db->join('tblprovincelist as e','b.province=e.provId');
        $this->db->where('a.requestId',$id);           
        $query = $this->db->get();    
        //echo $this->db->last_query();
        return $query->row();
    }
    
    function getRequestQuestions($id)
    {
        $this->db->select('a.projectId,a.optionId,a.questionId,a.optionVal,a.extras,b.questionName,b.fieldType');
        $this->db->from('tblprojectdetails a');        
        $this->db->join('tblquestions b','a.questionId=b.questionId');       
        $this->db->where('a.projectId',$id);   
        $this->db->group_by('a.questionId');  
        $query = $this->db->get();
       // echo $this->db->last_query();
        return $query->result();
    }
    
    function getRequestAnswers($id,$pid)
    {
        $this->db->select('b.optionText,b.optionType,a.extras');
        $this->db->from('tblprojectdetails a');  
        $this->db->join('tblansweroptions b','a.optionId=b.optionId');
        $this->db->where('a.questionId',$id); 
        $this->db->where('a.projectId',$pid);          
        $query = $this->db->get();  
        //echo $this->db->last_query();
        return $query->result();
    }
    
    function getProjectExpertList($id){
        $this->db->select('c.businessName,a.quotePrice,a.priceType,a.quoteMessage,a.addedOn,a.prosId as professionalId,c.reviewCount,c.rating,a.status,a.requestId,c.providerId,b.firstname,b.lastname,b.userPhoto,b.facebookProfileId');
        $this->db->from('tblquoterequest a');  
        $this->db->join('tbluser b','a.prosId=b.userId');         
        $this->db->join('tbluserprovider c','c.userId=b.userId');         
        $this->db->where('a.projectId',$id);   
        $this->db->where('a.status !=','Pending');
        $this->db->order_by('a.modifiedOn','desc');   
        $query = $this->db->get(); 
       // echo $this->db->last_query();
        return $query->result();
    }
    
    function getOrderDetails($order_id)
    {
        $this->db->select();
        $this->db->from('tblcredittranscations');          
        $this->db->where('order_id',$order_id);           
        $query = $this->db->get();    
        //echo $this->db->last_query();
        return $query->row();
    }
    
    function getServiceSuggestionList($service)
    {
        /*if (get_cookie('language') === 'indonesia') {
            $this->db->select('subserviceId as serviceId,subserviceNameIND as serviceName');
            $this->db->from('tblsubservices');         
            $this->db->where('status','Y'); 
            if($service!='' && $type==''){
              $this->db->like('subserviceNameIND', $service);   
            }else{
              $this->db->like('subserviceNameIND', $service,'after');     
            }
            $this->db->group_by('subserviceNameIND');
            $this->db->order_by('subserviceNameIND','asc');
            $query = $this->db->get();
            //echo $this->db->last_query();
            return $query->result();
        }else{
            $this->db->select('subserviceId as serviceId,subserviceName as serviceName');
            $this->db->from('tblsubservices');         
            $this->db->where('status','Y'); 
            if($service!='' && $type==''){
              $this->db->like('subserviceName', $service);   
            }else{
              $this->db->like('subserviceName', $service,'after');     
            }
            $this->db->group_by('subserviceName');
            $this->db->order_by('subserviceName','asc');
            $query = $this->db->get();
            //echo $this->db->last_query();
            return $query->result();
        }*/
        
        $sql='select * from (select subserviceId as serviceId,subserviceNameIND as serviceName from tblsubservices where status="Y" and (';        
        /*if($service!='' && $type==''){
            $sql.=' and subserviceNameIND like "%'.$service.'%"';
        }else{
            $sql.=' and subserviceNameIND like "%'.$service.'"';
        }*/
        $p=0;
        for($k=0;$k<count($service);$k++){
           if($service[$k]!=''){
            if($p > 0){
                $sql.=' or ';
            }
            $sql.='subserviceNameIND like "%'.trim($service[$k]).'%"'; 
            $p++;
           }
        }
        $sql.=') group by subserviceNameIND';
        $sql.=' union select subserviceId as serviceId,subserviceName as serviceName from tblsubservices where status="Y" and (';
        /*if($service!='' && $type==''){
            $sql.=' and subserviceName like "%'.$service.'%"';
        }else{
            $sql.=' and subserviceName like "%'.$service.'"';
        }*/
        $p=0;
        for($l=0;$l<count($service);$l++){
            if($service[$l]!=''){
                if($p > 0){
                   $sql.=' or ';
                }
                $sql.='subserviceName like "%'.trim($service[$l]).'%"'; 
                $p++;
            }
        }
        $sql.=') group by subserviceName';
        $sql.=' union select subServiceId as serviceId,	serviceTag as serviceName from tblsubserviceTags where isEnglish="Y" and (';
        /*if($service!='' && $type==''){
            $sql.=' and subserviceName like "%'.$service.'%"';
        }else{
            $sql.=' and subserviceName like "%'.$service.'"';
        }*/
        $p=0;
        for($a=0;$a<count($service);$a++){
             if($service[$a]!=''){
                if($p > 0){
                   $sql.=' or ';
                }
                $sql.='serviceTag like "%'.trim($service[$a]).'%"'; 
                $p++;
             }
        }
        $sql.=') group by serviceTag';
        $sql.=' union select subServiceId as serviceId,	serviceTag as serviceName from tblsubserviceTags where isEnglish="N" and (';
        /*if($service!='' && $type==''){
            $sql.=' and subserviceName like "%'.$service.'%"';
        }else{
            $sql.=' and subserviceName like "%'.$service.'"';
        }*/
        $p=0;
        for($b=0;$b<count($service);$b++){
            if($service[$b]!=''){
                if($p > 0){
                   $sql.=' or ';
                }
                $sql.='serviceTag like "%'.trim($service[$b]).'%"'; 
                $p++;
            }
        }
        $sql.=') group by serviceTag) as t order by t.serviceName asc';            
        $query=$this->db->query($sql); 
        //echo $this->db->last_query();
        return $query->result();
    }
    
    function getProsCountForService($id)
    {
        $query=$this->db->query("SELECT count(*) as pros_count FROM tbluserprovider as a join tbluser as b on(a.userId=b.userId) join tbluserservices as c on(a.userId=c.userId) "
                . "where c.serviceId='".$id."' and b.status='Y' group by a.userId");  
        if($query->num_rows() > 0){
            $row=$query->row();
            return $row->pros_count;
        }else{
            return '0';
        }
    }
    
    function getServiceListForPros()
    {
        $query=$this->db->query("SELECT d.subServiceName as serviceName FROM tbluserprovider as a join tbluser as b on(a.userId=b.userId) join tbluserservices as c on(a.userId=c.userId) join tblsubservices as d on(c.serviceId=d.subserviceId) "
                . "where b.status='Y' group by a.userId");        
        return $query->result();
    }
    
    function checkServiceIsFeatured($id){
        $this->db->select();
        $this->db->from('tblsubservices');		
        $this->db->where('subserviceId', $id);        
        $this->db->where('isFeatured', 'Y');                 
        $query = $this->db->get();		
        return $query->num_rows();
    }
    
    function checkSuggestedService($service){
        $this->db->select();
        $this->db->from('tblsuggestedservices');		
        $this->db->where('serviceName', $service);                
        $query = $this->db->get();		
        return $query->num_rows();
    }   
    
    function getServiceIdByTag($service)
    {
        $this->db->select('b.subserviceName,b.subserviceNameIND,a.isEnglish');
        $this->db->from('tblsubserviceTags as a');	
        $this->db->join('tblsubservices as b','a.subServiceId=b.subserviceId');		
        $this->db->where('serviceTag', $service);                
        $query = $this->db->get();		
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }
    
    function getServicesId($service_name){
        $this->db->select('tblsubservices.subserviceId as serviceId');
        $this->db->from('tblsubservices');          
        $this->db->where('tblsubservices.subserviceName',$service_name);  
        $this->db->or_where('tblsubservices.subserviceNameIND',$service_name);  
        $this->db->group_by('tblsubservices.subserviceName');                
        $query = $this->db->get();        
        if($query->num_rows() > 0){
            $r=$query->row();
            return $r->serviceId;
        }else{
            $this->db->select('subServiceId as serviceId');
            $this->db->from('tblsubserviceTags');          
            $this->db->where('serviceTag',$service_name);
            $this->db->group_by('serviceTag');                
            $query1 = $this->db->get(); 
            //echo $this->db->last_query();
            if($query1->num_rows() > 0){
                $r1=$query1->row();
                return $r1->serviceId;
            }else{
              return false;                
            }
        }
    }
    
    function getRequestsForPros($id,$lat,$lng,$miles,$services,$past_date)
    {       
        $query=$this->db->query("SELECT a.*,c.subserviceName,c.subserviceNameIND, d.firstname, d.lastname,b.requestId,( 3959 * acos( cos( radians(".$lat.")) * cos( radians(a.latitude)) * cos(radians(a.longitude) - radians(".$lng.")) + sin(radians(".$lat.")) * sin(radians(a.latitude)))) AS miles "
                . "FROM tbluserprojects as a join tblsubservices c on(a.serviceId=c.subserviceId) "
                . "left join tblquoterequest as b on(a.projectId=b.projectId and b.prosId=".$id.") "
                . "JOIN tbluser AS d ON ( a.userId = d.userId ) "
                . "where a.serviceId IN (".$services.") and a.status  NOT IN ('Completed','Hired','Expired') and b.requestId IS NULL and c.status='Y' and d.userId != 'deleted' and DATE_FORMAT(a.createdDate,'%Y-%m-%d') between '".$past_date."' and '".date('Y-m-d')."' group by a.projectId HAVING miles <= '".$miles."'"); 
        //echo $this->db->last_query();
        //die;
        return $query->result();
    }
}
?>