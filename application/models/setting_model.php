<?php
class Setting_model extends CI_Model
{    
    function __construct()
    {
        parent:: __construct();
    }

    function getProvincesList()
    {
        $this->db->select('provinceId,provinceName');
        $this->db->from('tblprovinces');       	
        $this->db->where('active','Y');                   
        $query = $this->db->get();		
        return $query->result();
    }
    
    function getProvincesOtherList()
    {
        $this->db->select('provId,provName');
        $this->db->from('tblprovincelist');
        $this->db->order_by("provName", "asc");
        $query = $this->db->get();		
        return $query->result();
    }
    
    function getCityList($id)
    {
        $this->db->select('cityId,cityName');
        $this->db->from('tblcitylist');
        $this->db->where('provinceId',$id); 
        $this->db->order_by("cityName", "asc");
        $query = $this->db->get();		
        return $query->result();
    }
    
    function getDistrictList($id)
    {
        $this->db->select('districtId,districtName');
        $this->db->from('tbldistrictlist');
        $this->db->where('cityId',$id); 
        $this->db->order_by("districtName", "asc");
        $query = $this->db->get();		
        return $query->result();
    }
    
    function getCityName($id)
    {
        $this->db->select('cityName');
        $this->db->from('tblcitylist');
        $this->db->where('cityId',$id); 
        $query = $this->db->get();		
        $rw=$query->row();
        return $rw->cityName;
    }
    
    function getProvinceName($id)
    {
        $this->db->select('provName');
        $this->db->from('tblprovincelist');
        $this->db->where('provId',$id); 
        $query = $this->db->get();		
        $rw=$query->row();
        return $rw->provName;
    }  
    
    function getSettingValue($seeting_code)
    {
        $this->db->select('settingVal');
        $this->db->from('tblsettings');
        $this->db->where('settingName',$seeting_code); 
        $query = $this->db->get();		
        $rw=$query->row();
        return $rw->settingVal;
    }
    
    function getFullAdrress($provId,$cityId,$districtId){        
        $query = $this->db->query("SELECT a.provName,b.cityName,c.districtName from tblprovincelist as a, tblcitylist as b,tbldistrictlist as c where a.provId='".$provId."' and b.cityId='".$cityId."' and c.districtId='".$districtId."'");		
        return $query->row();
    }
    
    function getCreditPackages(){
        $this->db->select('*');
        $this->db->from('tblCreditPackages');
        $this->db->where('active','Y');         
        $query = $this->db->get();		
        return $query->result();
    }
    
    function getCreditPackageDetails($id){
        $this->db->select('*');
        $this->db->from('tblCreditPackages');
        $this->db->where('packageId',$id);        
        $this->db->where('active','Y');         
        $query = $this->db->get();		
        return $query->row();
    }
}
?>