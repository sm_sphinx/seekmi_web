<?php

class Profile_model extends CI_Model
{
    
	function __construct()
	{
	    parent:: __construct();
	}
	
	function insertUser($data)
	{
            $table='tbluser';
            $this->db->insert($table, $data); 
	}
        
        function insertProvider($data)
	{
            $table='tbluserprovider';
            $this->db->insert($table, $data); 
	}
	
	function updateUser($data,$id)
	{
            $table='tbluser';
            $this->db->update($table, $data, array('userId'=> $id));		
	}
        
        function updateProvider($data,$id)
	{
            $table='tbluserprovider';
            $this->db->update($table, $data, array('userId'=> $id));		
	}
	
	function getUserInfo($uid)
	{
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.userId', $uid); 
            $query = $this->db->get();		
            return $query->row();
	}
        
        function getNotifications($uid)
	{
            $this->db->select();
            $this->db->from('tblemailnotifications');		
            $this->db->where('tblemailnotifications.userId', $uid); 
            $query = $this->db->get();           
            if($query->num_rows()==0)
            {	
                return '';
            }
            else
            {
                return $query->row();						
            }
	}
        
        function updateNotifications($id,$col,$value)
        {
            $data = array($col => $value);
            $this->db->where('userId',$id);
            $this->db->update('tblemailnotifications', $data);
            return true;
        }
        
        function change_password($p,$u,$token)
	{
	    $data = array('password' => md5($p),'resetPasswordToken'=>'');
            $this->db->where('userId',$u);
            $this->db->update('tbluser', $data);
            return true;
	}

	function getUserIdByEmail($useremail)
	{
            $query = $this->db->get_where('tbluser', array('email' => $useremail));
            return $query->row();				
	}
			
	function updatePassword($pwd,$uid)
	{
            $this->db->query('update tbluser set password="'.$pwd.'" where userId="'.$uid.'"');	
	}
        
        function delete_account($uid,$reason)
        {            
            $this->db->query('update tbluser set status="deleted", deleteReason="'.$reason.'",deletedAccountDate="'.date('Y-m-d H:i:s').'" where userId="'.$uid.'"');	
        }
        
        function getUserProviderInfo($uid)
	{
            $this->db->select();
            $this->db->from('tbluserprovider');		
            $this->db->where('tbluserprovider.userId', $uid); 
            $query = $this->db->get();		
            return $query->row();
	}
        
        function getProviderAnswers($uid)
	{
            $this->db->select('b.answerText,a.questionText,a.queId');
            $this->db->from('tblprofilequestions as a');	
            $this->db->join('tblprovideranswers as b','a.queId=b.questionId and b.userId='.$uid, 'left');
            //$this->db->where('a.userId', $uid); 
            $query = $this->db->get();	           
            return $query->result();
	}
        
        function getProviderQuestionList()
	{
            $this->db->select('queId');
            $this->db->from('tblprofilequestions as a');	
            $query = $this->db->get();	           
            return $query->result();
	}
        
        function getProviderAnswerCount($uid)
	{
            $this->db->select('b.answerText,a.questionText,a.queId');
            $this->db->from('tblprofilequestions as a');	
            $this->db->join('tblprovideranswers as b','a.queId=b.questionId');
            $this->db->where('b.userId', $uid); 
            $query = $this->db->get();	           
            return $query->num_rows();
	}
	
	function checkUserToken($token)
	{
            $this->db->select();
            $this->db->from('tbmstusers');		
            $this->db->where('tbmstusers.resetPwdToken', $token); 
            $query = $this->db->get();		
            if($query->num_rows()==0)
            {	
                    return false;	
            }
            else
            {
                    return true;							
            }		
        }
        
        function getUserFromToken($token)
        {
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.resetPasswordToken', $token); 
            $this->db->where('tbluser.status','Y'); 
            $query = $this->db->get();		
            if($query->num_rows()==0)
            {	
                return false;	
            }
            else
            {
                return $query->row();							
            }
        }
	
	function check_old_password($id,$old_pwd)
	{
	    $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.userId', $id); 		 
            $this->db->where('tbluser.password', md5($old_pwd)); 			
            $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {		 		
		return true;
	    }
	    else
	    {
		return false;		 
	    }
	}
        
        function getMediaCount($id)
        {
            $this->db->select('mediaId');
            $this->db->from('tblbusinessmedia');	            
            $this->db->where('userId', $id); 
            $query = $this->db->get();	           
            return $query->num_rows();
        }
        
        function getAllMedia($id)
        {
            $this->db->select('*');
            $this->db->from('tblbusinessmedia');	            
            $this->db->where('userId', $id); 
            $query = $this->db->get();	           
            return $query->result();
        }
        
        function getMediaDetail($id)
        {
            $this->db->select('*');
            $this->db->from('tblbusinessmedia');	            
            $this->db->where('mediaId', $id); 
            $query = $this->db->get();	           
            return $query->row();
        }
        
        function getCustomerRequests($id)
        {
            $this->db->select('a.requestId,a.status,c.subserviceName,d.firstname,d.lastname,e.cityName,f.provName,a.addedOn,a.modifiedOn,a.readFlag,b.zipcode');
            $this->db->from('tblquoterequest as a');	            
            $this->db->join('tbluserprojects as b','a.projectId=b.projectId');
            $this->db->join('tblsubservices as c','b.serviceId=c.subserviceId');
            $this->db->join('tbluser as d','b.userId=d.userId');
            $this->db->join('tblcitylist as e','d.city=e.cityId','left');
            $this->db->join('tblprovincelist as f','d.province=f.provId','left');
            $this->db->where('a.prosId', $id); 
            $this->db->where('a.status', 'Pending');
            $this->db->order_by('a.addedOn', 'desc');
            $query = $this->db->get();	           
            return $query->result();
        }
        
        function getCustomerRequestDetails($id)
        {
            $this->db->select('a.projectId,a.prosId,a.status,b.firstname,b.lastname,a.addedOn,a.readFlag,b.userPhoto,c.businessName,c.website,b.phone,a.quoteMessage,a.requestId,b.facebookProfileId,b.email');
            $this->db->from('tblquoterequest as a');           
            $this->db->join('tbluser as b','a.prosId=b.userId');
            $this->db->join('tbluserprovider as c','c.userId=b.userId');           
            $this->db->where('a.requestId', $id); 
            $query = $this->db->get();	           
            return $query->row();
        }
        
        function changeRequestStatus($id,$status,$pid)
        {
            if($status=='accept')
            {
               $pdata['status']='Accepted';    
            }
            if($status=='decline')
            {
               $pdata['status']='Declined';    
            }
            if($status=='complete')
            {
               $pdata['status']='Completed';    
               //$this->db->update('tbluserprojects', $pdata, array('projectId'=> $pid));
            }
            $pdata['modifiedOn']=date('Y-m-d H:i:s');    
            $this->db->update('tblquoterequest', $pdata, array('requestId'=> $id));	
        }
        
        function getAllUserReviews($id)
        {
            /*$this->db->select('a.reviewId,a.rating,a.reviewContent,a.createdOn,b.firstname,b.lastname,b.userPhoto,b.facebookProfileId');
            $this->db->from('tblbusinessreview a');
            $this->db->join('tbluser as b','a.userId=b.userId');
            $this->db->where('a.prosId', $id); 
            $query = $this->db->get();	*/
            $query=$this->db->query('SELECT `a`.`reviewId`, `a`.`rating`, `a`.`reviewContent`, `a`.`createdOn`, `b`.`firstname`, `b`.`lastname`, `b`.`userPhoto`, `b`.`facebookProfileId` FROM (`tblbusinessreview` a) JOIN `tbluser` as b ON `a`.`userId`=`b`.`userId` WHERE `a`.`prosId` = '.$id.' UNION
SELECT `a`.`jobReviewId` as `reviewId`, `a`.`rating`, `a`.`reviewComment` as `reviewContent`, `a`.`createdDate` as `createdOn`, `b`.`firstname`, `b`.`lastname`, `b`.`userPhoto`, `b`.`facebookProfileId` FROM (`tblprojectreview` a) JOIN `tbluser` as b ON `a`.`userId`=`b`.`userId` WHERE `a`.`prosId` = '.$id);
            //echo $this->db->last_query();
            return $query->result();
        }
        
        function getProjectQuoteCount($pid)
        {
            $this->db->select('requestId');
            $this->db->from('tblquoterequest');	            
            $this->db->where('projectId', $pid); 
            $this->db->where('status !=', 'Pending');
            $query = $this->db->get();	           
            return $query->num_rows();
        }
        
        function getProjectOwner($pid)
        {
            $this->db->select('a.userId,b.firstname,b.lastname,b.email,b.phone,a.provinceId,a.cityId,a.districtId,'
                    . ' a.adminComment,a.createdDate,a.serviceId,c.subserviceName,c.subserviceNameIND');
            $this->db->from('tbluserprojects a');            
            $this->db->join('tbluser as b','a.userId=b.userId');
            $this->db->join('tblsubservices as c','a.serviceId=c.subserviceId');
            $this->db->where('a.projectId', $pid);             
            $query = $this->db->get();	           
            return $query->row();
        }
        
        function getCustomerQuotes($id,$type)
        {
            $this->db->select('a.requestId,a.status,c.subserviceName,d.userId,d.firstname,d.lastname,e.cityName,f.provName,a.addedOn,a.modifiedOn,a.readFlag,b.projectId');
            $this->db->from('tblquoterequest as a');	            
            $this->db->join('tbluserprojects as b','a.projectId=b.projectId');
            $this->db->join('tblsubservices as c','b.serviceId=c.subserviceId');
            $this->db->join('tbluser as d','b.userId=d.userId');
            $this->db->join('tblcitylist as e','d.city=e.cityId','left');
            $this->db->join('tblprovincelist as f','d.province=f.provId','left');
            $this->db->where('a.prosId', $id); 
            if($type=='In Progress'){
              $this->db->where('a.status', 'Accepted');              
            }
            if($type=='Hired'){
              $this->db->where('a.status', 'Hired');
            }
            if($type=='Archived'){
              $this->db->where_in('a.status', array('Completed', 'Declined', 'Expired'));
            }
            $query = $this->db->get();	           
            return $query->result();
        }
        
        function getUserAddress($id)
        {
            $this->db->select('a.streetAddress,a.zipcode,b.cityName,c.provName,d.districtName');
            $this->db->from('tbluser as a');
            $this->db->join('tblcitylist as b','a.city=b.cityId','left');
            $this->db->join('tblprovincelist as c','a.province=c.provId','left');
            $this->db->join('tbldistrictlist as d','a.district=d.districtId','left');
            $this->db->where('a.userId', $id);             
            $query = $this->db->get();	           
            return $query->row();
        }
        
        function getUserProfileTotalProgress($uid){
            $this->db->select('(signupInitialProgress + basicInfoProgress + mediaProgress + bioInfoProgress + serviceInfoProgress + linksInfoProgress + qaInfoProgress + refereInfoProgress + reviewInfoProgress) as total_progress');
            $this->db->from('tbluserprovider');		
            $this->db->where('tbluserprovider.userId', $uid); 
            $query = $this->db->get();		
            if($query->num_rows() > 0){
                $rw=$query->row();
                return $rw->total_progress;
            }else{
                return "0";
            }
        }
        
        function getProsUserSubServices($uid)
        {
            if (get_cookie('language') == 'english') {
              $this->db->select('serviceId,subserviceName as serviceName');
            }else{
                $this->db->select('serviceId,subserviceNameIND as serviceName');
            }
            $this->db->from('tbluserservices as a');	 
            $this->db->join('tblsubservices as b','a.serviceId=b.subserviceId');	 
            $this->db->where('userId',$uid);
            $this->db->where('isSubservice','Y'); 
            $query = $this->db->get();	        
            if($query->num_rows()==0)
            {
                return false;
            }else{
                return $query->result();
            }
        }
        
        function getProjectCompletedPros($pid)
        {
            $this->db->select('a.prosId,b.firstname,b.lastname,b.email');
            $this->db->from('tblquoterequest as a');
            $this->db->join('tbluser as b','a.prosId=b.userId');            
            $this->db->where('a.projectId', $pid); 
            $this->db->where_in('a.status', array('Completed','Hired'));            
            $query = $this->db->get();	           
            if($query->num_rows()==0)
            {
                return false;
            }else{
                return $query->row();
            }
        }
       
        function checkNewRegistrationTokenPros($token)
        {
            $this->db->select('*');
            $this->db->from('tbluser');            
            $this->db->where('newRegisterProsToken', $token);                       
            $query = $this->db->get();	           
            if($query->num_rows()==0)
            {
                return false;
            }else{
                return $query->row();
            }
        }
        
        function checkProfileTypeCredit($prosId,$type){
            $this->db->select('credit,isCreditEarn');
            $this->db->from('tbluserprofilecredits');            
            $this->db->where('prosId', $prosId);  
            $this->db->where('profileType', $type);        
            $query = $this->db->get();	           
            if($query->num_rows()==0)
            {
                return false;
            }else{
                return $query->row();
            }
        }
        
}
?>