<?php

class Customer_model extends CI_Model
{    
    function __construct()
    {
        parent:: __construct();
    }

    function insertUser($data)
    {
        $table='tblseekmeusers';
        $this->db->insert($table, $data); 
    }

    function updateUser($data,$id)
    {
        $table='tblseekmeusers';
        $this->db->update($table, $data, array('userId'=> $id));		
    }

    function checkEmailExists($useremail)
    {		
        $this->db->select();
        $this->db->from('tblseekmeusers');		
        $this->db->where('tblseekmeusers.email', $useremail); 				 
        $query = $this->db->get();		
        if($query->num_rows()==0)
        { 
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('email', $useremail);
            $this->db->where('userType', 'professional');   
            //$this->db->where('status', 'Y');   
            $query1 = $this->db->get(); 
            if($query1->num_rows()==0)
            {
                return true;
            }else{
                return false; 
            }
        }
        else
        {
            return false;
        }		
    }
    
    function checkValidConfirmationToken($token)
    {
        $this->db->select('userId,status');
        $this->db->from('tblseekmeusers');		
        $this->db->where('tblseekmeusers.confirmationToken', $token);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            $r=$query->row();		 	
            return $r;
        }
        else
        {
            return false;		 
        }
    }
    
    function checkValidConfirmationTokenUber($token)
    {
        $this->db->select('userId,status');
        $this->db->from('tbluser');		
        $this->db->where('tbluser.confirmationToken', $token);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            $r=$query->row();		 	
            return $r;
        }
        else
        {
            return false;		 
        }
    }
}
?>