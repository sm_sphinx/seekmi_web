<?php

class User_model extends CI_Model
{
    
	function __construct()
	{
	    parent:: __construct();
	}
	
	function insertUser($data)
	{
            $table='tbluser';
            $this->db->insert($table, $data); 
	}
	
	function updateUser($data,$id)
	{
            $table='tbluser';
            $this->db->update($table, $data, array('userId'=> $id));		
	}
	
	function getUserInfo($uid)
	{
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.userId', $uid); 
            $query = $this->db->get();		
            return $query->row();
	}
	
	function getAllUser($start=1, $per_page=10,$keyword)
	{            
            $this->db->select();
            $this->db->from('tbluser');	
            $this->db->where('userType','user');
            if($keyword!=''){
               $where = " (firstname LIKE '%" . $keyword . "%' OR "
                        . "lastname LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            }    
            $this->db->where('status != ','deleted');
            $this->db->order_by('userId', 'DESC');
            $this->db->limit($start, $per_page);
            $query = $this->db->get();	
            //echo $this->db->last_query();
            return $query->result();
	}
	
	function getAllUserCount($keyword)
	{           
            $this->db->select();
            $this->db->from('tbluser');	
            $this->db->where('userType','user');	
            if($keyword!=''){
               $where = " (firstname LIKE '%" . $keyword . "%' OR "
                        . "lastname LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            }  
            $this->db->where('status != ','deleted');
            $query = $this->db->get();		
            return $query->num_rows();
	}
	
	function getAllUserEmail()
	{
            $this->db->select('email,userId');
            $this->db->from('tbluser');	
            $this->db->where('user_type','user');	
            $this->db->where('isActive','Y');            
            $this->db->order_by('email','ASC');	
            $query = $this->db->get();		
            return $query->result();
	}
	
	function check_user_login($adata)
	{	   
            $this->db->select('tbladminuser.email,tbladminuser.userid,tbladminuser.status,tbladminuser.roleId,tblrole.roleName');
            $this->db->from('tbladminuser');
            $this->db->join('tblrole', 'tbladminuser.roleId = tblrole.roleId');            
            $this->db->where('email', $adata['useremail']); 
           // $this->db->where('tbladminuser.status', 'Y'); 
            $this->db->where('password', md5($adata['userpass']));             
            $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {
                $r=$query->row();		 	
                return $r;
	    }
	    else
	    {
		return false;		 
	    }
	}
        
        function check_customer_login($adata)
	{	   
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.email', $adata['useremail']); 
            $this->db->where('tbluser.password', md5($adata['userpass']));
            $this->db->where('tbluser.status != ', 'deleted'); 
            $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {
                $r=$query->row();		 	
                return $r;
	    }
	    else
	    {
		return false;		 
	    }
	}
        
        function checkEmailExists($useremail)
	{		
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.email', $useremail); 
            $this->db->where('tbluser.status != ', 'deleted'); 
            $query = $this->db->get();		
            if($query->num_rows()==0)
            {        	
                return true;					
            }
            else
            {
                return false;
            }		
	}
        
        function checkEmailExistsWithStatus($useremail)
	{		
            $this->db->select('status');
            $this->db->from('tbluser');		
            $this->db->where('tbluser.email', $useremail); 
            $this->db->where('tbluser.status != ', 'deleted'); 
            //$this->db->where_in('tbluser.status', array('Y','review')); 	 		
            $query = $this->db->get();
            //echo $this->db->last_query();
            if($query->num_rows() > 0)
            {        	
                $r=$query->row();		 	
                return $r;				
            }
            else
            {
                return false;
            }		
	}
        
        function change_password($p,$u)
	{
	    $data = array('password' => md5($p));
            $this->db->where('userId',$u);
            $this->db->update('tbluser', $data);
            return true;
	}

	function encrypt($str,$key)
	{	
	   if($str!=''){
                $iv = '372adfc77ec8913b';
		$td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
                mcrypt_generic_init($td, $key, $iv);		
                $encrypted = mcrypt_generic($td, $str);
                mcrypt_generic_deinit($td);
                mcrypt_module_close($td);
                return bin2hex($encrypted);
            }else
            {
             return '';
            }
        }
	
	function decrypt($code,$key) 
	{
            $code = $this->hex2bin($code);
            $iv = '372adfc77ec8913b';
            $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
            mcrypt_generic_init($td, $key, $iv);
            $decrypted = mdecrypt_generic($td, $code);
            mcrypt_generic_deinit($td);
            mcrypt_module_close($td);
            return utf8_encode(trim($decrypted));
        }
	
	function hex2bin($hexdata)
	{
	    $bindata = '';
            for ($i = 0; $i < strlen($hexdata); $i += 2) {
             $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
            }
            return $bindata;
        }

	function getUserIdByEmail($useremail)
	{
            $query = $this->db->get_where('tbluser', array('email' => $useremail));
            return $query->row();				
	}
			
	function updatePassword($pwd,$uid)
	{
            $this->db->query('update tbluser set password="'.$pwd.'" where userId="'.$uid.'"');	
	}
		
	
	function checkUserToken($token)
	{
            $this->db->select();
            $this->db->from('tbmstusers');		
            $this->db->where('tbmstusers.resetPwdToken', $token); 
            $query = $this->db->get();		
            if($query->num_rows()==0)
            {	
                    return false;	
            }
            else
            {
                    return true;							
            }		
	}
	function getAdminInfo($uid)
	{
            $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.userId', $uid); 
            $query = $this->db->get();		
            return $query->row();
	}
	
	function updateAdmin($data,$id)
	{
            $table='tbluser';
            $this->db->update($table, $data, array('userId'=> $id));
	}
	
	function check_old_password($id,$old_pwd)
	{
	    $this->db->select();
            $this->db->from('tbluser');		
            $this->db->where('tbluser.userId', $id); 		 
            $this->db->where('tbluser.password', md5($old_pwd)); 			
            $query = $this->db->get();
	    if($query->num_rows() > 0)
	    {		 		
		return true;
	    }
	    else
	    {
		return false;		 
	    }
	}
        
        function checkValidConfirmationToken($token)
        {
            $today=date('Y-m-d'); 
            $query=$this->db->query("SELECT `userId`, `status` FROM (`tbluser`) WHERE `tbluser`.`confirmationToken` = '".$token."' AND DATE_FORMAT(tbluser.modifiedDate,'%Y-%m-%d') >= DATE_SUB( '".$today."' , INTERVAL 2 WEEK )");
            //echo $this->db->last_query();
            if($query->num_rows() > 0)
            {
                $r=$query->row();		 	
                return $r;
            }
            else
            {
                return false;		 
            }
        }
        
        function getExportAllUsers($keyword)
	{            
            $this->db->select("firstname,lastname,email,phone,(CASE WHEN status = 'Y' THEN 'Active' WHEN status = 'N' THEN 'Inactive' ELSE CONCAT(UCASE(SUBSTRING(status, 1, 1)),SUBSTRING(status, 2)) END) status",false);
            $this->db->from('tbluser');	
            $this->db->where('userType','user');
            if($keyword!=''){
               $where = " (firstname LIKE '%" . $keyword . "%' OR "
                        . "lastname LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            }            
            $this->db->order_by('userId', 'DESC');           
            return $query = $this->db->get();	
           // echo $this->db->last_query();           
            //return $query->result();
	}
        
        function getAllAdminUser($start=1, $per_page=10,$keyword)
	{            
            $this->db->select("tbladminuser.*,tbladminuser.status as astatus,tblrole.*");
            $this->db->from('tbladminuser');	
            $this->db->join('tblrole', 'tbladminuser.roleId = tblrole.roleId');  
            if($keyword!=''){
               $where = " (firstName LIKE '%" . $keyword . "%' OR "
                        . "lastName LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            }     
            $this->db->where('tbladminuser.userid !=', $this->session->userdata('admin_userId')); 	
            $this->db->order_by('userid', 'DESC');
            $this->db->limit($start, $per_page);
            $query = $this->db->get();	
            //echo $this->db->last_query();
            return $query->result();
	}
	
	function getAllAdminUserCount($keyword)
	{           
            $this->db->select();
            $this->db->from('tbladminuser');	
            $this->db->join('tblrole', 'tbladminuser.roleId = tblrole.roleId');  	
            if($keyword!=''){
               $where = " (firstName LIKE '%" . $keyword . "%' OR "
                        . "lastName LIKE '%" . $keyword . "%' OR "
                        . "email LIKE '%" . $keyword . "%' OR "
                        . "phone LIKE '%" . $keyword . "%')";
               $this->db->where($where, NULL, FALSE);
            } 
            $this->db->where('tbladminuser.userid !=', $this->session->userdata('admin_userId')); 	
            $query = $this->db->get();		
            return $query->num_rows();
	}
        
        function getAdminRoleList()
        {
            $this->db->select();
            $this->db->from('tblrole');		
            $this->db->where('status', 'Y'); 		             
            $query = $this->db->get();
	    return $query->result();
        }
        
        function getAdminUserInfo($uid)
	{
            $this->db->select();
            $this->db->from('tbladminuser');		
            $this->db->where('userid', $uid); 
            $query = $this->db->get();		
            return $query->row();
	}        
        
        function checkAdminEmailExists($useremail)
	{		
            $this->db->select();
            $this->db->from('tbladminuser');		
            $this->db->where('tbladminuser.email', $useremail); 				 
            $query = $this->db->get();		
            if($query->num_rows()==0)
            {        	
                return true;					
            }
            else
            {
                return false;
            }		
	}
        
        function getAllRoles($start=1, $per_page=10,$keyword)
	{            
            $this->db->select();
            $this->db->from('tblrole');	            
            if($keyword!=''){
               $where = " (roleName LIKE '%" . $keyword . "%)";
               $this->db->where($where, NULL, FALSE);
            }            
            $this->db->order_by('roleId', 'DESC');
            $this->db->limit($start, $per_page);
            $query = $this->db->get();	
            //echo $this->db->last_query();
            return $query->result();
	}
	
	function getAllRolesCount($keyword)
	{           
            $this->db->select();
            $this->db->from('tblrole');	            
            if($keyword!=''){
               $where = " (roleName LIKE '%" . $keyword . "%)";
               $this->db->where($where, NULL, FALSE);
            }  
            $query = $this->db->get();		
            return $query->num_rows();
	}
        
        function getRoleInfo($id)
	{
            $this->db->select();
            $this->db->from('tblrole');		
            $this->db->where('roleId', $id); 
            $query = $this->db->get();		
            return $query->row();
	}
        
        function checkRoleExists($rolename)
	{		
            $this->db->select();
            $this->db->from('tblrole');		
            $this->db->where('tblrole.roleName', $rolename); 				 
            $query = $this->db->get();		
            if($query->num_rows()==0)
            {        	
                return true;					
            }
            else
            {
                return false;
            }		
	}
        
        function getRoleRights($id)
        {
            $this->db->select('menuCode');
            $this->db->from('tblrolerights');	            
            $this->db->where('tblrolerights.roleId', $id);             
            $query = $this->db->get();	
            //echo $this->db->last_query();
            return $query->result();
        }
        
        function check_admin_old_password($id,$old_pwd)
	{
	    $this->db->select();
            $this->db->from('tbladminuser');		
            $this->db->where('tbladminuser.userid', $id); 		 
            $this->db->where('tbladminuser.password', md5($old_pwd)); 			
            $query = $this->db->get();
            //echo $this->db->last_query();
            //die;
	    if($query->num_rows() > 0)
	    {		 		
		return true;
	    }
	    else
	    {
		return false;		 
	    }
	}
}
?>