<?php
class Bid_model extends CI_Model
{    
    function __construct()
    {
        parent:: __construct();
    }

    function getQuoteMessage($id)
    {
        $this->db->select('a.message,a.messageId,b.firstname,b.lastname,b.userPhoto,b.facebookProfileId,a.createdOn');
        $this->db->from('tblquotemessage a');   
        $this->db->join('tbluser b','a.messageFrom=b.userId');        
        $this->db->where('quoteId',$id);
        $query = $this->db->get();	
        //echo $this->db->last_query();
        return $query->result();
    }   
    
    function checkAlreadyReviewed($id,$prosId)
    {
        $this->db->select('reviewId');
        $this->db->from('tblbusinessreview');          
        $this->db->where('userId',$id);
        $this->db->where('prosId',$prosId);
        $query = $this->db->get();      
        return $query->num_rows();
    }
    
    function getReviewCount($prosId)
    {
        /*$this->db->select('reviewId');
        $this->db->from('tblbusinessreview');                  
        $this->db->where('prosId',$prosId);
        $query = $this->db->get();      
        return $query->num_rows();*/
        $query=$this->db->query('select (a+b) as total_cnt from (select (select IFNULL(count(reviewId),0) as cnt from tblbusinessreview where prosId="'.$prosId.'") as a, (select IFNULL(count(jobReviewId),0) as cnt from tblprojectreview where prosId="'.$prosId.'") as b) as c');
        return $query->row();       
    }
    
    function getUserRating($id)
    {
        /*$this->db->select('rating');
        $this->db->from('tblbusinessreview');
        $this->db->where('prosId',$id);
        $query = $this->db->get();
        return $query->result();*/
        $query=$this->db->query('SELECT (a+b) as ratingSum FROM (SELECT (SELECT IFNULL(SUM(b.rating),0) FROM tblbusinessreview as b WHERE b.prosId = "'.$id.'") as a, (SELECT IFNULL(SUM(p.rating),0) FROM tblprojectreview as p WHERE p.prosId = "'.$id.'") as b) as c');
        return $query->row();
    }
    
    function updateProjectStatus($uid,$pid,$status,$comment)
    {
        $data = array('status' => $status, 'comment' => $comment, 'modifiedDate' => date('Y-m-d H:i:s'));
        $this->db->where('projectId',$pid);
        $this->db->where('userId',$uid);
        $this->db->update('tbluserprojects', $data);
        return true;
    }
    
    function checkAlreadyJobReviewed($id,$prosId,$projectId,$quoteid)
    {
        $this->db->select('jobReviewId');
        $this->db->from('tblprojectreview');          
        $this->db->where('userId',$id);
        $this->db->where('prosId',$prosId);
        $this->db->where('projectId',$projectId);
        $this->db->where('requestId',$quoteid);
        $query = $this->db->get();      
        return $query->num_rows();
    }
}
?>