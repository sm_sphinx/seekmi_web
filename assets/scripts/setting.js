// JavaScript Document

var Setting = function () {


	var handleProfile = function () {

     
		 
		 $('.profile_info_form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
	                dev_fname: {
	                    required: true
	                },
	                dev_lname: {
	                    required: true	                  
	                },					
	                dev_website: {
	                    url: true	                  
	                }
	               
	            },
												
	            invalidHandler: function (event, validator) { //display error alert on form submit   			
	               $('.alert-success', $('.profile_info_form')).hide();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.control-group').addClass('error'); // set error class to the control group						
	            },				
				

	            success: function (label) {
					
					$('.alert-success', $('.profile_info_form')).hide();
	                label.closest('.control-group').removeClass('error');					
	                label.remove();
	            },
				

	            submitHandler: function (form) {
					App.blockUI($('#pageContent'));
					var formData = $('.profile_info_form').serialize();					
						$.ajax({
						type:'post',
						url:Host+"developer/user/save_user_info",
						data:formData,
						success : function(data)
						{						
							if(data=='success')
							{
								App.scrollTo($('#pageContent'), -200);
								App.unblockUI($('#pageContent'));
							  	$('.alert-success', $('.profile_info_form')).show();
								$('.alert-error', $('.profile_info_form')).hide();
							}			
							
						}
					});	
					
				}
	        });

			$('.profile_info_form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.profile_info_form').validate().form()) {
	                    var formData = $('.profile_info_form').serialize();					
						$.ajax({
						type:'post',
						url:Host+"developer/user/save_user_info",
						data:formData,
						success : function(data)
						{						
							if(data=='success')
							{
								App.scrollTo($('#pageContent'), -200);
								App.unblockUI($('#pageContent'));
							  	$('.alert-success', $('.profile_info_form')).show();
								$('.alert-error', $('.profile_info_form')).hide();
							}			
							
						}
					    });	
	                }
	                return false;
	            }
	        });

	}
	
	var handleChangePassword = function () {

      $('.password_form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                cPassword: {
	                    required: true	                  
	                },
					 nPassword: {
	                    required: true
	                },
	                rtPassword: {
	                    equalTo: "#nPassword"
	                }
	            },
								
	            invalidHandler: function (event, validator) { //display error alert on form submit   			
	              
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.control-group').addClass('error'); // set error class to the control group
	            },				
				

	            success: function (label) {
					$('.alert-success', $('.password_form')).hide();
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },
				

	            submitHandler: function (form) {
					var formData = $('.password_form').serialize();					
						$.ajax({
						type:'post',
						url:Host+"developer/user/save_password",
						data:formData,
						success : function(data)
						{						
							if(data=='success')
							{								
							  	$('.alert-success', $('.password_form')).show();
								$('.alert-error', $('.password_form')).hide();
								$('.alert-error1', $('.password_form')).hide();
							}
							else
							{								
								$('.alert-success', $('.password_form')).hide();
								$('.alert-error', $('.password_form')).hide();
								$('.alert-error1', $('.password_form')).show(); 
							}			
							
						}
					 });
				}
	        });

			$('.password_form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.password_form').validate().form()) {
	                    var formData = $('.password_form').serialize();					
						$.ajax({
						type:'post',
						url:Host+"developer/user/save_password",
						data:formData,
						success : function(data)
						{							
							if(data=='success')
							{								
							  	$('.alert-success', $('.password_form')).show();
								$('.alert-error', $('.password_form')).hide();
								$('.alert-error1', $('.password_form')).hide();
							}
							else
							{								
								$('.alert-success', $('.password_form')).hide();
								$('.alert-error', $('.password_form')).hide();
								$('.alert-error1', $('.password_form')).show(); 
							}
							
						}
					  });	
	                }
	                return false;
	            }
	        });

	}
	
	var handleAccessRight = function () {

      $('.access_form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {	                
	            },
								
	            invalidHandler: function (event, validator) { //display error alert on form submit   			
	              
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.control-group').addClass('error'); // set error class to the control group
	            },				
				

	            success: function (label) {
					$('.alert-success', $('.access_form')).hide();
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },
				

	            submitHandler: function (form) {
					var formData = $('.access_form').serialize();					
						$.ajax({
						type:'post',
						url:Host+"developer/user/save_access_rights",
						data:formData,
						success : function(data)
						{						
							if(data=='success')
							{								
							  	$('.alert-success', $('.access_form')).show();
								$('.alert-error', $('.access_form')).hide();								
							}							
						}
					 });
				}
	        });

			$('.access_form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.access_form').validate().form()) {
	                    var formData = $('.access_form').serialize();					
						$.ajax({
						type:'post',
						url:Host+"developer/user/save_access_rights",
						data:formData,
						success : function(data)
						{						
							if(data=='success')
							{								
							  	$('.alert-success', $('.access_form')).show();
								$('.alert-error', $('.access_form')).hide();								
							}							
						}
					 });	
	                }
	                return false;
	            }
	        });

	}
	
	var handleProfilePhoto = function () {

      $('.user_photo_form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {					 
					 profile_photo: {
	                    required: true	                  
	                }
	            },
								
	            invalidHandler: function (event, validator) { //display error alert on form submit   			
	              
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.control-group').addClass('error'); // set error class to the control group
	            },				
				

	            success: function (label) {
					$('.alert-success', $('.user_photo_form')).hide();
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },
				

	            submitHandler: function (form) {
					$('.user_photo_form').submit();
				}
	        });

			

	}
	  var handleDatePickers = function () {
		var ToEndDate = new Date();
		if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl : App.isRTL(),
				  endDate: ToEndDate,
				  	format : 'mm/dd/yyyy'
            });
        }
    }
	var handleInviteEmail = function () {

     
		 
		 $('.invite_form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules:{	                
	                invite_email: {
	                    required: true,
						email:true
	                }
	            },
												
	            invalidHandler: function (event, validator) { //display error alert on form submit   			
	               $('.alert-success', $('.invite_form')).hide();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.control-group').addClass('error'); // set error class to the control group						
	            },				
				

	            success: function (label) {
					$('.alert-success', $('.invite_form')).hide();
	                label.closest('.control-group').removeClass('error');					
	                label.remove();
	            },
				

	            submitHandler: function (form) {					
					var formData = $('.invite_form').serialize();					
						$.ajax({
						type:'post',
						url:Host+"developer/user/invite_user",
						data:formData,
						success:function(data)
						{	
							
							if(data=='success')
							{								
							  	$('.alert-success', $('.invite_form')).show();
								$('.alert-error', $('.invite_form')).hide();
								setTimeout( function(){
									 location.href=Host+'developer/user/setting#tab_1_2'
								 }, 2000); //2 seconds
							}
							else
							{
								$('.alert-success', $('.invite_form')).hide();
								$('.alert-error', $('.invite_form')).show();
							}
						}
					});	
				}
	        });

			$('.invite_form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.invite_form').validate().form()) {
	                    var formData = $('.invite_form').serialize();					
						$.ajax({
						type:'post',
						url:Host+"developer/user/invite_user",
						data:formData,
						success:function(data)
						{						
							if(data=='success')
							{								
							  	$('.alert-success', $('.invite_form')).show();
								$('.alert-error', $('.invite_form')).hide();
								setTimeout( function(){
									 location.href=Host+'developer/user/setting#tab_1_2'
								 }, 2000); //2 seconds
							}
							else
							{
								$('.alert-success', $('.invite_form')).hide();
								$('.alert-error', $('.invite_form')).show();
							}
						}
					    });	
	                }
	                return false;
	            }
	        });

	}
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleProfile(); 
			handleChangePassword();
	        handleAccessRight();
            handleProfilePhoto();
			handleInviteEmail();
			handleDatePickers();
        }

    };

}();