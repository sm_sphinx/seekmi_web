var Role = function() {
    
    var handleRole = function() {
        var userid=$('#role_id').val();        
        var urlPass,urlRedirect;
        urlPass=HostAdmin + "user/saveRoleInfo";
        urlRedirect=HostAdmin + "user/roles";
        
        if(userid==0){
        $('.role-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {                                
                txtRolename: {
                    required: true                    
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-error', $('.role-form')).hide();
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //form.submit();
                var pageContent = $('#mainDiv');

                App.blockUI(pageContent);
                var formData = $('.role-form').serialize();
                $.ajax({
                    type: 'post',
                    url: urlPass,
                    data: formData,
                    success: function(data)
                    {
                        if (data == 'success')
                        {
                            $('.alert-error', $('.role-form')).hide();
                            $('.alert-success', $('.role-form')).show();
                            window.location = urlRedirect;
                        }
                        else
                        {
                           if(data=='already'){
                            $('.alert-success', $('.role-form')).hide();
                            $('.alert-error', $('.role-form')).show();
                           }
                        }
                        App.unblockUI(pageContent);
                    }
                });

            }
        });
       }else{
          $('.role-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {                
                txtRolename: {
                    required: true                    
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-error', $('.role-form')).hide();
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //form.submit();
                var pageContent = $('#mainDiv');

                App.blockUI(pageContent);
                var formData = $('.role-form').serialize();
                $.ajax({
                    type: 'post',
                    url: urlPass,
                    data: formData,
                    success: function(data)
                    {
                        if (data == 'success')
                        {
                            $('.alert-error', $('.role-form')).hide();
                            $('.alert-success', $('.role-form')).show();
                            window.location = urlRedirect;
                        }
                        else
                        {
                           if(data=='already'){
                            $('.alert-success', $('.role-form')).hide();
                            $('.alert-error', $('.role-form')).show();
                           }
                        }
                        App.unblockUI(pageContent);
                    }
                });

            }
        }); 
       }

        $('.role-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.role-form').validate().form()) {
                    var pageContent = $('#mainDiv');

                    App.blockUI(pageContent);
                    var formData = $('.role-form').serialize();
                    $.ajax({
                        type: 'post',
                        url: urlPass,
                        data: formData,
                        success: function(data)
                        {
                            if (data == 'success')
                            {
                                $('.alert-error', $('.role-form')).hide();
                                $('.alert-success', $('.role-form')).show();
                                window.location = urlRedirect;
                            }
                            else
                            {
                                if(data=='already'){
                                    $('.alert-success', $('.role-form')).hide();
                                    $('.alert-error', $('.role-form')).show();
                                }
                            }
                            App.unblockUI(pageContent);
                        }
                    });
                }
                return false;
            }
        });
    }
    
    return {
        //main function to initiate the module
        init: function() {           
            handleRole();  
        }

    };

}();
