var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {	                
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {	                
	                password: {
	                    required: "Password is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit  
				    $('.alert-error1', $('.login-form')).hide();
	                $('.alert-error', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
					//form.submit();
					var formData = $('.login-form').serialize();
					$.ajax({
					type:'post',
					url:Host+"developer/user/invite_login_submit",
					data:formData,
					success : function(data)
					{						
						if(data=='success')
						{						 
						  location.href=Host+'developer/user/my_apps';						  
						}											
						else
						{	
						  $('.alert-error', $('.login-form')).hide();
						  $('.alert-error1', $('.login-form')).show();
						}
					}
				});			
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if(e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    var formData = $('.login-form').serialize();
						$.ajax({
						type:'post',
						url:Host+"developer/user/invite_login_submit",
						data:formData,
						success : function(data)
						{						
							if(data=='success')
							{						 
							  location.href=Host+'developer/user/my_apps';						  
							}											
							else
							{	
							  $('.alert-error', $('.login-form')).hide();
							  $('.alert-error1', $('.login-form')).show();
							}
						}
					    });		
	                }
	                return false;
	            }
	        });
	}

var handleRegister = function () {

		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }


		$("#select2_sample4").select2({
		  	placeholder: '<i class="icon-map-marker"></i>&nbsp;Select a Country',
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });


			$('#select2_sample4').change(function () {
                $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });



         $('.register-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
	                ufirstname: {
	                    required: true
	                },	                
	                upassword: {
	                    required: true
	                },
	                urpassword: {
	                    equalTo: "#register_password"
	                },
	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.addClass('help-small no-left-padding').insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.addClass('help-small no-left-padding').insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {					
					var formData = $('.register-form').serialize();
					$.ajax({
					type:'post',
					url:Host+"developer/user/invite_register",
					data:formData,
					success : function(data)
					{						
						if(data=='success')
						{						 
						  location.href=Host+'developer/user/my_apps';		
						}	
						
					}
				    });		
	                
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    var formData = $('.register-form').serialize();
						$.ajax({
						type:'post',
						url:Host+"developer/user/invite_register",
						data:formData,
						success : function(data)
						{						
							if(data=='success')
							{						 
							  location.href=Host+'developer/user/my_apps';		
							}						
						}
						});	
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.register-form').hide();
	        });
	}
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();            
            handleRegister();        
	       
        }

    };

}();