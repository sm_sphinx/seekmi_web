var Login = function () {
    var handleLogin = function () {
        $('.login-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                pass: {
                    required: true,
                    minlength: 6
                },
                cpass: {
                    required: true,
                    equalTo: "#pass"
                }
            },
            messages: {
                pass: {
                    required: "Password is required."
                },
                cpass: {
                    required: "Confirm Password is required."
                }
            },
            submitHandler: function (form) {
                //form.submit();
                var formData = $('.login-form').serialize();
                $.ajax({
                    type: 'post',
                    url: HostAdmin + "user/update_pass",
                    data: formData,
                    success: function ()
                    {

                        location.href = HostAdmin + 'user/index?pass=update';

                    }
                });
            }
        });


    }


    return {
        //main function to initiate the module
        init: function () {
            handleLogin();
        }

    };

}();