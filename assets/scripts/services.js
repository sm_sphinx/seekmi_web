var Services = function() {

    var handleSubServices = function() {             
        var urlPass,urlRedirect;
        urlPass=HostAdmin + "service/saveSubServiceInfo";
        urlRedirect=HostAdmin + "service/sublist";
        
        
        $('.sub-service-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {                
                txtServiceName: {
                    required: true
                },
                'questionType[]':{
                    required: true
                 },
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-error', $('.sub-service-form')).hide();
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //form.submit();
                var pageContent = $('#mainDiv');                
                App.blockUI(pageContent);
                var formData = $('.sub-service-form').serialize();
                $.ajax({
                    type: 'post',
                    url: urlPass,
                    data: formData,
                    success: function(data)
                    {
                        if (data == 'success')
                        {
                            $('.alert-error', $('.sub-service-form')).hide();
                            $('.alert-success', $('.sub-service-form')).show();
                            window.location = urlRedirect;
                        }
                        else
                        {
                           if(data=='already'){
                            $('.alert-success', $('.sub-service-form')).hide();
                            $('.alert-error', $('.sub-service-form')).show();
                           }
                        }
                        App.unblockUI(pageContent);
                    }
                });

            }
        });
       

        $('.sub-service-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.sub-service-form').validate().form()) {
                    var pageContent = $('#mainDiv');

                    App.blockUI(pageContent);
                    var formData = $('.sub-service-form').serialize();
                    $.ajax({
                        type: 'post',
                        url: urlPass,
                        data: formData,
                        success: function(data)
                        {
                            if (data == 'success')
                            {
                                $('.alert-error', $('.sub-service-form')).hide();
                                $('.alert-success', $('.sub-service-form')).show();
                                window.location = urlRedirect;
                            }
                            else
                            {
                                if(data=='already'){
                                    $('.alert-success', $('.sub-service-form')).hide();
                                    $('.alert-error', $('.sub-service-form')).show();
                                }
                            }
                            App.unblockUI(pageContent);
                        }
                    });
                }
                return false;
            }
        });
    }
    
    var handleServices = function() {             
        var urlPass,urlRedirect;
        urlPass=HostAdmin + "service/saveServiceInfo";
        urlRedirect=HostAdmin + "service/lists";
        
        
        $('.service-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {                
                txtServiceName: {
                    required: true
                },
                txtCategory: {
                    required: true
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-error', $('.service-form')).hide();
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //form.submit();
                var pageContent = $('#mainDiv');
                App.blockUI(pageContent);
                $("#lstBox2[multiple] option").prop("selected", "selected"); 
                var formData = $('.service-form').serialize();
                $.ajax({
                    type: 'post',
                    url: urlPass,
                    data: formData,
                    success: function(data)
                    {
                        if (data == 'success')
                        {
                            $('.alert-error', $('.service-form')).hide();
                            $('.alert-success', $('.service-form')).show();
                            window.location = urlRedirect;
                        }
                        else
                        {
                           if(data=='already'){
                            $('.alert-success', $('.service-form')).hide();
                            $('.alert-error', $('.service-form')).show();
                           }
                        }
                        App.unblockUI(pageContent);
                    }
                });

            }
        });
       

        $('.service-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.service-form').validate().form()) {
                    var pageContent = $('#mainDiv');
                    App.blockUI(pageContent);
                    $("#lstBox2[multiple] option").prop("selected", "selected"); 
                    var formData = $('.service-form').serialize();
                    $.ajax({
                        type: 'post',
                        url: urlPass,
                        data: formData,
                        success: function(data)
                        {
                            if (data == 'success')
                            {
                                $('.alert-error', $('.service-form')).hide();
                                $('.alert-success', $('.service-form')).show();
                                window.location = urlRedirect;
                            }
                            else
                            {
                                if(data=='already'){
                                    $('.alert-success', $('.service-form')).hide();
                                    $('.alert-error', $('.service-form')).show();
                                }
                            }
                            App.unblockUI(pageContent);
                        }
                    });
                }
                return false;
            }
        });
    }
    
    var handleQuestions = function() {             
        var urlPass,urlRedirect;
        urlPass=HostAdmin + "service/saveQuestionInfo";        
        
        
        $('.question-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {                
                txtQuestionType: {
                    required: true
                },
                txtOption1: {
                    required: {
                        depends: function(element) {
                            return ($('#txtFieldType').val() == 'checkbox' || 
                                    $('#txtFieldType').val() == 'radio' ||
                                    $('#txtFieldType').val() == 'select');
                        }
                    }
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-error', $('.question-form')).hide();
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //form.submit();
                var pageContent = $('#mainDiv');
                App.blockUI(pageContent);                
                var formData = $('.question-form').serialize();
                $.ajax({
                    type: 'post',
                    url: urlPass,
                    data: formData,
                    success: function(data)
                    {
                        if (data == 'success')
                        {
                            $('.alert-error', $('.question-form')).hide();
                            $('.alert-success', $('.question-form')).show();
                            loadData();
                            jQuery('#modalPopupDiv').modal('hide');
                        }
                        else
                        {                            
                        }
                        App.unblockUI(pageContent);
                    }
                });

            }
        });
       

        $('.question-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.question-form').validate().form()) {
                    var pageContent = $('#mainDiv');
                    App.blockUI(pageContent);
                    
                    var formData = $('.question-form').serialize();
                    $.ajax({
                        type: 'post',
                        url: urlPass,
                        data: formData,
                        success: function(data)
                        {
                            if (data == 'success')
                            {
                                $('.alert-error', $('.question-form')).hide();
                                $('.alert-success', $('.question-form')).show();
                                loadData();
                            }
                            else
                            {                               
                            }
                            App.unblockUI(pageContent);
                        }
                    });
                }
                return false;
            }
        });
    }
    
    return {
        //main function to initiate the module
        init: function() {
            handleSubServices();   
            handleServices(); 
            handleQuestions();   
        }

    };

}();
