var Category = function() {

    var handleCategory = function() {             
        var urlPass,urlRedirect;
        urlPass=HostAdmin + "service/saveCategoryInfo";
        urlRedirect=HostAdmin + "service/categorylist";
        
        
        $('.category-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {                
                txtCatname: {
                    required: true
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-error', $('.category-form')).hide();
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //form.submit();
                var pageContent = $('#mainDiv');

                App.blockUI(pageContent);
                var formData = $('.category-form').serialize();
                $.ajax({
                    type: 'post',
                    url: urlPass,
                    data: formData,
                    success: function(data)
                    {
                        if (data == 'success')
                        {
                            $('.alert-error', $('.category-form')).hide();
                            $('.alert-success', $('.category-form')).show();
                            window.location = urlRedirect;
                        }
                        else
                        {
                           if(data=='already'){
                            $('.alert-success', $('.category-form')).hide();
                            $('.alert-error', $('.category-form')).show();
                           }
                        }
                        App.unblockUI(pageContent);
                    }
                });

            }
        });
       

        $('.category-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.user-form').validate().form()) {
                    var pageContent = $('#mainDiv');

                    App.blockUI(pageContent);
                    var formData = $('.category-form').serialize();
                    $.ajax({
                        type: 'post',
                        url: urlPass,
                        data: formData,
                        success: function(data)
                        {
                            if (data == 'success')
                            {
                                $('.alert-error', $('.category-form')).hide();
                                $('.alert-success', $('.category-form')).show();
                                window.location = urlRedirect;
                            }
                            else
                            {
                                if(data=='already'){
                                    $('.alert-success', $('.category-form')).hide();
                                    $('.alert-error', $('.category-form')).show();
                                }
                            }
                            App.unblockUI(pageContent);
                        }
                    });
                }
                return false;
            }
        });
    }
    
    return {
        //main function to initiate the module
        init: function() {
            handleCategory();            
        }

    };

}();
