var Login = function () {

    var handleLogin = function () {
        $('.login-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                email: {
                    required: "Email is required.",
                    email: "Email is invalid."
                },
                password: {
                    required: "Password is required."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit  
                $('.alert-error1', $('.login-form')).hide();
                $('.alert-error', $('.login-form')).show();
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function (form) {
                //form.submit();
                var formData = $('.login-form').serialize();
                $.ajax({
                    type: 'post',
                    url: HostAdmin + "user/login_submit",
                    data: formData,
                    success: function (data)
                    {
                        if (data == 'success')
                        {
                            location.href = HostAdmin + 'user/index';
                        }
                        else if (data == 'inactive')
                        {
                            $('.alert-error1', $('.login-form')).hide();
                            $('.alert-error2', $('.login-form')).show();
                            $('.alert-error', $('.login-form')).hide();
                        }
                        else
                        {
                            $('.alert-error1', $('.login-form')).show();
                            $('.alert-error2', $('.login-form')).hide();
                            $('.alert-error', $('.login-form')).hide();
                        }
                    }
                });
            }
        });

        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    var formData = $('.login-form').serialize();
                    $.ajax({
                        type: 'post',
                        url: HostAdmin + "user/login_submit",
                        data: formData,
                        success: function (data)
                        {
                            if (data == 'success')
                            {
                                location.href = HostAdmin + 'user/index';
                            }
                            else if (data == 'inactive')
                            {
                                $('.alert-error1', $('.login-form')).hide();
                                $('.alert-error2', $('.login-form')).show();
                                $('.alert-error', $('.login-form')).hide();
                            }
                            else
                            {
                                $('.alert-error1', $('.login-form')).show();
                                $('.alert-error2', $('.login-form')).hide();
                                $('.alert-error', $('.login-form')).hide();
                            }
                        }
                    });
                }
                return false;
            }
        });
    }


    return {
        //main function to initiate the module
        init: function () {
            handleLogin();
        }

    };

}();