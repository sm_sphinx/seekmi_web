var Pros = function() {

    var handlePros = function() {
        var userid=$('#user_id').val();        
        var urlPass,urlRedirect;
        urlPass=HostAdmin + "pros/saveProsInfo";
        urlRedirect=HostAdmin + "pros/lists";
        
        if(userid==0){
        $('.user-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {                
                txtEmail: {
                    required: true,
                    email: true
                },
                txtPassword: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                txtCPassword: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#txtPassword"
                }

            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-error', $('.user-form')).hide();
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //form.submit();
                var pageContent = $('#mainDiv');

                App.blockUI(pageContent);
                var formData = $('.user-form').serialize();
                $.ajax({
                    type: 'post',
                    url: urlPass,
                    data: formData,
                    success: function(data)
                    {
                        if (data == 'success')
                        {
                            $('.alert-error', $('.user-form')).hide();
                            $('.alert-success', $('.user-form')).show();
                            window.location = urlRedirect;
                        }
                        else
                        {
                           if(data=='already'){
                            $('.alert-success', $('.user-form')).hide();
                            $('.alert-error', $('.user-form')).show();
                           }
                        }
                        App.unblockUI(pageContent);
                    }
                });

            }
        });
       }else{
          $('.user-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {                
                txtEmail: {
                    required: true,
                    email: true
                },
                txtPassword: {
                    required: '#change_password:checked',
                    minlength: 6,
                    maxlength: 20
                },
                txtCPassword: {
                    required: '#change_password:checked',
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#txtPassword"
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-error', $('.user-form')).hide();
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //form.submit();
                var pageContent = $('#mainDiv');

                App.blockUI(pageContent);
                var formData = $('.user-form').serialize();
                $.ajax({
                    type: 'post',
                    url: urlPass,
                    data: formData,
                    success: function(data)
                    {
                        if (data == 'success')
                        {
                            $('.alert-error', $('.user-form')).hide();
                            $('.alert-success', $('.user-form')).show();
                            window.location = urlRedirect;
                        }
                        else
                        {
                           if(data=='already'){
                            $('.alert-success', $('.user-form')).hide();
                            $('.alert-error', $('.user-form')).show();
                           }
                        }
                        App.unblockUI(pageContent);
                    }
                });

            }
        }); 
       }

        $('.user-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.user-form').validate().form()) {
                    var pageContent = $('#mainDiv');

                    App.blockUI(pageContent);
                    var formData = $('.user-form').serialize();
                    $.ajax({
                        type: 'post',
                        url: urlPass,
                        data: formData,
                        success: function(data)
                        {
                            if (data == 'success')
                            {
                                $('.alert-error', $('.user-form')).hide();
                                $('.alert-success', $('.user-form')).show();
                                window.location = urlRedirect;
                            }
                            else
                            {
                                if(data=='already'){
                                    $('.alert-success', $('.user-form')).hide();
                                    $('.alert-error', $('.user-form')).show();
                                }
                            }
                            App.unblockUI(pageContent);
                        }
                    });
                }
                return false;
            }
        });
    }
    
    return {
        //main function to initiate the module
        init: function() {
            handlePros();            
        }

    };

}();
