// JavaScript Document

var Setting = function () {


	var handleProfile = function () {

      $('.profile_info_form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                 txtUsername: {
	                    required: true,
						maxlength: 20
	                  
	                },
					txtEmail: {
	                    required: true,
						email: true
	                }
	                          
	                
	            },
								
	            invalidHandler: function (event, validator) { //display error alert on form submit   			
	                 $('.alert-success', $('.profile_info_form')).hide();
	            },

	            highlight: function (element) { // hightlight error inputs
	               
					$(element).closest('.control-group').addClass('error'); // set error class to the control group
					
	            },				
				

	            success: function (label) {
	                $('.alert-success', $('.profile_info_form')).hide();
				   label.closest('.control-group').removeClass('error');
	                label.remove();
	            },
				

	            submitHandler: function (form) {
					
	               // form.submit();
				   	var userId = $.trim($("#userId", $('.profile_info_form')).val());
					var pageContent = $('.page-content');
					App.blockUI(pageContent);
				   	var formData = $('.profile_info_form').serialize();
					$.ajax({
					type:'post',
					url:Host+"user/saveUserInfo",
					data:formData,
					success : function(data)
					{	
					
						if(data=='success')
						{						 
						  $('.alert-success', $('.profile_info_form')).show();
						  $('.alert-error', $('.profile_info_form')).hide();
						  
						}
						App.unblockUI(pageContent);
					
					}
				    });		
				   
	            
				}
	        });

			$('.profile_info_form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.profile_info_form').validate().form()) {
	                   // form.submit();
					  	var userId = $.trim($("#userId", $('.profile_info_form')).val());
						var pageContent = $('.page-content');
						App.blockUI(pageContent);
						var formData = $('.profile_info_form').serialize();
						$.ajax({
						type:'post',
						url:Host+"user/saveUserInfo",
						data:formData,
						success : function(data)
						{		
							if(data=='success')
							{						 
							 	$('.alert-success', $('.profile_info_form')).show();
								$('.alert-error', $('.profile_info_form')).hide();
								 App.unblockUI(pageContent);
							}
						}
						});		
					   
	                }
	                return false;
	            }
	        });

	}
	var handleChangePassword = function () {

      $('.password_form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	               nPassword: {
	                    required: true,
						 minlength: 6,
						 maxlength: 20
	                },
	                rtPassword: {
						required: true,
						 minlength: 6,
						 maxlength: 20,
	                    equalTo: "#nPassword"
	                }
	            },
								
	            invalidHandler: function (event, validator) { //display error alert on form submit   			
	               
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.control-group').addClass('error'); // set error class to the control group
	            },				
				

	            success: function (label) {
					$('.alert-success', $('.password_form')).hide();
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },
				

	            submitHandler: function (form) {
					// form.submit();
				 
					var newPassword = $.trim($("#nPassword", $('.password_form')).val());
					var enc_newpassword = newPassword;
					var userId = $.trim($("#userId", $('.password_form')).val());
					
					var formData = 'nPassword='+enc_newpassword+'&userId='+userId;
					
					var pageContent = $('.page-content');
					
					App.blockUI(pageContent);
					
					$.ajax({
					type:'post',
					url:Host+"user/set_user_password",
					data:formData,
					success : function(data)
					{						
						if(data=='success')
						{						 
						  	$('.alert-success', $('.password_form')).show();
							$('.alert-error', $('.password_form')).hide();
							$('.alert-error1', $('.password_form')).hide();
							 App.unblockUI(pageContent);
						}
						else
						{
							$('.alert-success', $('.password_form')).hide();
							$('.alert-error', $('.password_form')).hide();
							$('.alert-error1', $('.password_form')).show(); 
							 App.unblockUI(pageContent);
						}
					}
				    });		
				   
				}
	        });

			$('.password_form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.password_form').validate().form()) {
	                   
					   
					   	var newPassword = $.trim($("#nPassword", $('.password_form')).val());
						var enc_newpassword = newPassword;
						var userId = $.trim($("#userId", $('.password_form')).val());
						var pageContent = $('.page-content');
					
						App.blockUI(pageContent);
					  	
						var formData = 'nPassword='+enc_newpassword+'&userId='+userId;
						
						$.ajax({
						type:'post',
						url:Host+"user/set_user_password",
						data:formData,
						success : function(data)
						{							
							if(data=='success')
							{								
							  	$('.alert-success', $('.password_form')).show();
								$('.alert-error', $('.password_form')).hide();
								$('.alert-error1', $('.password_form')).hide();
								 App.unblockUI(pageContent);
							}
							else
							{								
								$('.alert-success', $('.password_form')).hide();
								$('.alert-error', $('.password_form')).hide();
								$('.alert-error1', $('.password_form')).show();
								 App.unblockUI(pageContent);
							}
							
						}
					  });	
	                }
	                return false;
	            }
	        });

	}
    var handleProfilePhoto = function () {

      $('.user_photo_form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {					 
					 profile_photo: {
	                    required: true	                  
	                }
	            },
								
	            invalidHandler: function (event, validator) { //display error alert on form submit   			
	              
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.control-group').addClass('error'); // set error class to the control group
	            },				
				

	            success: function (label) {
					$('.alert-success', $('.user_photo_form')).hide();
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },
				

	            submitHandler: function (form) {
					var pageContent = $('.page-content');
					
					App.blockUI(pageContent);
					
					$('.user_photo_form').submit();
				}
	        });

			

	}
 
 
 var handleSelect2 = function () {
   $('#select2_sample1').select2({
            placeholder: "Select an option",
            allowClear: true
        });

        $('#select2_sample2').select2({
            placeholder: "Select a State",
            allowClear: true
        });

        $("#select2_sample3").select2({
            allowClear: true,
            minimumInputLength: 1,
            query: function (query) {
                var data = {
                    results: []
                }, i, j, s;
                for (i = 1; i < 5; i++) {
                    s = "";
                    for (j = 0; j < i; j++) {
                        s = s + query.term;
                    }
                    data.results.push({
                        id: query.term + i,
                        text: s
                    });
                }
                query.callback(data);
            }
        });

        function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }
        $("#select2_sample4").select2({
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });

        $("#select2_sample5").select2({
            tags: ["red", "green", "blue", "yellow", "pink"]
        });


        function movieFormatResult(movie) {
            var markup = "<table class='movie-result'><tr>";
            if (movie.posters !== undefined && movie.posters.thumbnail !== undefined) {
                markup += "<td valign='top'><img src='" + movie.posters.thumbnail + "'/></td>";
            }
            markup += "<td valign='top'><h5>" + movie.title + "</h5>";
            if (movie.critics_consensus !== undefined) {
                markup += "<div class='movie-synopsis'>" + movie.critics_consensus + "</div>";
            } else if (movie.synopsis !== undefined) {
                markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
            }
            markup += "</td></tr></table>"
            return markup;
        }

        function movieFormatSelection(movie) {
            return movie.title;
        }

        $("#select2_sample6").select2({
            placeholder: "Search for a movie",
            minimumInputLength: 1,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "http://api.rottentomatoes.com/api/public/v1.0/movies.json",
                dataType: 'jsonp',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 10,
                        apikey: "ju6z9mjyajq2djue3gbvv26t" // please do not use so this example keeps working
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {
                        results: data.movies
                    };
                }
            },
            initSelection: function (element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected movie's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the movie name is shown preselected
                var id = $(element).val();
                if (id !== "") {
                    $.ajax("http://api.rottentomatoes.com/api/public/v1.0/movies/" + id + ".json", {
                        data: {
                            apikey: "ju6z9mjyajq2djue3gbvv26t"
                        },
                        dataType: "jsonp"
                    }).done(function (data) {
                        callback(data);
                    });
                }
            },
            formatResult: movieFormatResult, // omitted for brevity, see the source of this page
            formatSelection: movieFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            escapeMarkup: function (m) {
                return m;
            } // we do not want to escape markup since we are displaying html in results
        });
 }
   var handleDatePickers = function () {
			var ToEndDate = new Date();

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl : App.isRTL(),
				endDate: ToEndDate,
				format : 'mm/dd/yyyy'
            });
        }
    }
  var handleSelect2Modal = function () {

        $('#select2_sample_modal_1').select2({
            placeholder: "Select an option",
            allowClear: true
        });

        $('#select2_sample_modal_2').select2({
            placeholder: "Select a State",
            allowClear: true
        });

        $("#select2_sample_modal_3").select2({
            allowClear: true,
            minimumInputLength: 1,
            query: function (query) {
                var data = {
                    results: []
                }, i, j, s;
                for (i = 1; i < 5; i++) {
                    s = "";
                    for (j = 0; j < i; j++) {
                        s = s + query.term;
                    }
                    data.results.push({
                        id: query.term + i,
                        text: s
                    });
                }
                query.callback(data);
            }
        });

        function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }
        $("#select2_sample_modal_4").select2({
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });

        $("#select2_sample_modal_5").select2({
            tags: ["red", "green", "blue", "yellow", "pink"]
        });


        function movieFormatResult(movie) {
            var markup = "<table class='movie-result'><tr>";
            if (movie.posters !== undefined && movie.posters.thumbnail !== undefined) {
                markup += "<td valign='top'><img src='" + movie.posters.thumbnail + "'/></td>";
            }
            markup += "<td valign='top'><h5>" + movie.title + "</h5>";
            if (movie.critics_consensus !== undefined) {
                markup += "<div class='movie-synopsis'>" + movie.critics_consensus + "</div>";
            } else if (movie.synopsis !== undefined) {
                markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
            }
            markup += "</td></tr></table>"
            return markup;
        }

        function movieFormatSelection(movie) {
            return movie.title;
        }

        $("#select2_sample_modal_6").select2({
            placeholder: "Search for a movie",
            minimumInputLength: 1,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "http://api.rottentomatoes.com/api/public/v1.0/movies.json",
                dataType: 'jsonp',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 10,
                        apikey: "ju6z9mjyajq2djue3gbvv26t" // please do not use so this example keeps working
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {
                        results: data.movies
                    };
                }
            },
            initSelection: function (element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected movie's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the movie name is shown preselected
                var id = $(element).val();
                if (id !== "") {
                    $.ajax("http://api.rottentomatoes.com/api/public/v1.0/movies/" + id + ".json", {
                        data: {
                            apikey: "ju6z9mjyajq2djue3gbvv26t"
                        },
                        dataType: "jsonp"
                    }).done(function (data) {
                        callback(data);
                    });
                }
            },
            formatResult: movieFormatResult, // omitted for brevity, see the source of this page
            formatSelection: movieFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            escapeMarkup: function (m) {
                return m;
            } // we do not want to escape markup since we are displaying html in results
        });
    }

    var handleMultiSelect = function () {
        $('#my_multi_select1').multiSelect();
        $('#my_multi_select2').multiSelect({
            selectableOptgroup: true
        });        
    }
 
 return {
        //main function to initiate the module
        init: function () {
        	
            handleProfile();        
	     	handleChangePassword();
			handleDatePickers();
		 //	handleProfilePhoto();
			/*handleSelect2();
			handleSelect2Modal();
			handleMultiSelect();*/
        }

    };

}();



/**
 * Function generates a random string for use in unique IDs, etc
 *
 * @param <int> n - The length of the string
 */
function randString(n)
{
    if(!n)
    {
        n = 5;
    }

    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for(var i=0; i < n; i++)
    {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

/**
 *
 *  Base64 encode / decode
 *  http://www.webtoolkit.info/
 *
 **/

var Base64 = {

    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
            this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}
