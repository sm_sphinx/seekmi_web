var Editrequest = function() {

    var handleUser = function() {
        var urlPass,urlRedirect;
        urlPass=HostAdmin + "service/submit_request";
        urlRedirect=HostAdmin + "service/jobs";
        
        $('.editrequest-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {                
                txtEmail: {
                    required: true,
                    email: true
                },
                txtPassword: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                txtCPassword: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#txtPassword"
                }

            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-error', $('.editrequest-form')).hide();
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
               
                
                //form.submit();
                var pageContent = $('#mainDiv');

                App.blockUI(pageContent);
                var formData = $('.editrequest-form').serialize();
                $.ajax({
                    type: 'post',
                    url: urlPass,
                    data: formData,
                    success: function(data)
                    {
                        if (data == 'success')
                        {
                            $('.alert-error', $('.editrequest-form')).hide();
                            $('.alert-success', $('.editrequest-form')).show();
                            window.location = urlRedirect;
                        }
                        else
                        {
                           if(data=='already'){
                            $('.alert-success', $('.editrequest-form')).hide();
                            $('.alert-error', $('.editrequest-form')).show();
                           }
                        }
                        App.unblockUI(pageContent);
                    }
                });

            }
        });

        $('.editrequest-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.editrequest-form').validate().form()) {
                    var pageContent = $('#mainDiv');

                    App.blockUI(pageContent);
                    var formData = $('.editrequest-form').serialize();
                    $.ajax({
                        type: 'post',
                        url: urlPass,
                        data: formData,
                        success: function(data)
                        {
                            if (data == 'success')
                            {
                                $('.alert-error', $('.editrequest-form')).hide();
                                $('.alert-success', $('.editrequest-form')).show();
                                window.location = urlRedirect;
                            }
                            else
                            {
                                if(data=='already'){
                                    $('.alert-success', $('.editrequest-form')).hide();
                                    $('.alert-error', $('.editrequest-form')).show();
                                }
                            }
                            App.unblockUI(pageContent);
                        }
                    });
                }
                return false;
            }
        });
    }
    
    
    return {
        //main function to initiate the module
        init: function() {
            handleUser();  
        }

    };

}();
