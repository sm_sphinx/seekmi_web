var ForgetPassword = function () {

    var handleForgetPassword = function () {
        $('.forget_pass-form').validate({
            errorElement: 'label', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email: {
                    required: "Email is required.",
                    email: "Email is invalid."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit  
                $('.alert-error22', $('.forget_pass-form')).show();
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.control-group').addClass('error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.control-group').removeClass('error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function (form) {

                var formData = $('.forget_pass-form').serialize();
                $.ajax({
                    type: 'post',
                    url: HostAdmin + "user/forgot_password",
                    data: formData,
                    success: function (data)
                    {
                        if (data == "success"){
                            $('#success').text('Email is Send');
                            $('#forget_email').text('');
                        }
                        else{
                            $('#email_not_exist').show();
                            $('#forget_email').val('');
                        }

                    }
                });


            }
        });

        $('.forget_pass-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget_pass-form').validate().form()) {
                    var formData = $('.forget_pass-form').serialize();
                    $.ajax({
                        type: 'post',
                        url: HostAdmin + "user/forgot_password",
                        data: formData,
                        success: function (data)
                        {
                            if (data == 'success')
                            {
                                location.href = HostAdmin + 'user/forgot_password';
                            }
                            else if (data == 'inactive')
                            {
                                $('.alert-error21', $('.forget_pass-form')).hide();
                                $('.alert-error22', $('.forget_pass-form')).hide();
                            }
                            else
                            {
                                $('.alert-error21', $('.forget_pass-form')).show();
                                $('.alert-error22', $('.forget_pass-form')).hide();
                            }
                        }
                    });
                }
                return false;
            }
        });
    }


    return {
        //main function to initiate the module
        init: function () {
            handleForgetPassword();
        }

    };

}();