var Business = function () {
	
	$.validator.addMethod("uploadFile", function(val, element) {
      if(val!='')
	  {
		var ext = val.split('.').pop().toLowerCase();
		var allow = new Array('gif','png','jpg','jpeg');
		if(jQuery.inArray(ext, allow) == -1) {			
		   return false;
		}else{			
		   return true;
		}
	  }
	  else
	  {
		  return true;
	  }
	}, "File extension error");
	
	$.validator.addMethod("phoneNumber", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, ""); 
	return this.optional(element) ||
		phone_number.match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/);
	}, "Please specify a valid phone number");

	var handleBusiness = function() {
		$('.business-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
					txtBusinessName: {
	                    required: true
	                },
					txtParentCategory: {
	                    required: true
	                },
					txtSubCategory: {
	                    required: true
	                },
					txtCounty: {
	                    required: true
	                },
					txtDistrict: {
	                    required: true
	                },
					txtCityID: {
	                    required: true
	                },
					txtPhoneNo: {
						phoneNumber:true						
					},
					txtMobileNumber:{						
					    number: true
					},
					txtBusinessEmail: {
						email: true
					},
					txtBusinessImage1: {
						uploadFile: true
					},
					txtBusinessImage2: {
						uploadFile: true
					},
					txtBusinessImage3: {
						uploadFile: true
					},
					txtBusinessImage4: {
						uploadFile: true
					},
					txtWebsiteurl: {
						url: true
					},
					txtFburl: {
						url: true
					},
					txtTwturl: {
						url: true
					},					
					txtTripadvisorUrl: {
						url: true
					}/*,
					txtAddress: {
						required: true
					},
					txtLatLng: {
						required: true
					}*/
			    },
	            invalidHandler: function (event, validator) { //display error alert on form submit   
	               // $('.alert-error', $('.business-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	               	error.insertAfter(element);
	            },

	            submitHandler: function (form) {
					
	                $('#register-submit-btn').hide();
					var pageContent = $('#mainDiv');
					App.blockUI(pageContent);
				
					//var business_image_1 = $.trim($('#txtBusinessImage1').val());
					//var business_image_2 = $.trim($('#txtBusinessImage2').val());
					//var business_image_3 = $.trim($('#txtBusinessImage3').val());
					//var business_image_4 = $.trim($('#txtBusinessImage4').val());
					var business_id = $('#business_id').val();
					
					if(business_id == 0)
					{	
						if($('#txtBusinessImage1').val()=='')
						{					
							$('#div_imageNotSet', $('.business-form')).show();	
							$('#txtBusinessImage1').closest('.control-group').addClass('error'); 
							App.unblockUI(pageContent); 
							$('#register-submit-btn').show(); 
							return false;
						}
						else
						{
							$('#txtBusinessImage1').closest('.control-group').removeClass('error');
						}
					}
					$('#div_imageNotSet', $('.business-form')).hide();	
					
									
					var business_name = $('#txtBusinessName').val();
						$.ajax({
						type:'post',
						url:Host+"business/checkDupliBusiness",
						data:"business_id="+business_id+"&business_name="+encodeURIComponent(business_name),
						success : function(data)
						{	
							if(data=='success')
							{						 
								document.myBusinessFrm.submit();
							}	
							else
							{	
								$('#register-submit-btn').show();
								$('#span_errorMsg').html(data);
								$('.alert-success', $('.business-form')).hide();
								//$('.alert-error', $('.register-form')).show();
								$('#div_errorMsg').show();
								App.unblockUI(pageContent);
							}
							
						}
					});	
					//document.myBusinessFrm.submit();
	            }
	        });
	}
	
    return {
        init: function () {
          handleBusiness();
        }
    };

}();
