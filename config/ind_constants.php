<?php
define('GET_THINGS_DONE', 'Get things done');
define('BY_HIRING','by hiring experienced <br>
                local service pros<br>');
define('REGISTER_PART', 'Daftar sekarang dan menangkan:');
define('SERVICE_PART', 'Jasa apa yang Anda butuhkan?');
define('GET_FREE_QUOTE', 'Dapatkan penawaran gratis');
define('YOUR_SERVICE_PROVIDER', 'Anda penyedia jasa?');
define('GET_PART', 'Dapatkan sampai dengan <br/>3 penawaran dalam waktu <br/>beberapa jam dan pilih ahli <br/>jasa yang terbaik. <br/>Mudah, simpel, dan gratis.');
define('KODOM_PART', 'Ind-kaodim is now available in Klang Valley, Penang & <br>
                                            Johor Bahru');
define('KAMI_PART', 'Kami akan mempertemukan Anda ke penyedia jasa professional yang akan memberikan penawaran terbaik sesuai kebutuhan Anda');
define('SEEKMI_HELP', 'Bagaimana Seekmi dapat membantu Anda?');
define('FIND_COMPARE', 'Temukan, bandingkan dan pekerjakan penyedia jasa professional terbaik');
define('WHAT_YOU_NEED', 'Beritahukan kami apa yang Anda butuhkan');
define('ONE_MINUTE_TO_ANS', 'Hanya satu menit untuk menjawab pertanyaan singkat tentang kebutuhan Anda.');
define('GET_A_QUOTE', 'Dapatkan penawaran dan bandingkan');
define('RECIEVE_QUOTES', 'Dalam beberapa jam Anda akan menerima penawaran dari penyedia jasa professional yang bersedia dipekerjakan disertai estimasi harga dan informasi kontak untuk dihubungi.');
define('SELECT_SERVICE_PROVIDERS', 'Pilih dan pekerjakan penyedia jasa professional yang cocok');
define('CHOOSE_IMPLEMENTATIONS', 'Pilih dari beberapa pelaksana jasa yang cocok untuk disewa. Pelaksana jasa tersebut akan segera menghubungi Anda untuk bertemu dan memenuhi kebutuhan Anda.');
define('ARE_YOU_SERVICE_PROVIDER', 'Apakah Anda penyedia jasa?');
define('GET_NEW_CLIENT', 'Dapatkan klien baru');
define('SEEKMI_FWD_CLIENT', 'Seekmi akan meneruskan permintaan klien akan kebutuhan penyedia jasa professional di wilayahnya. Anda yang akan memutuskan apakah Anda tertarik atau tidak untuk memberikan penawaran untuk klien tersebut.');
define('REGISTER_FREE', 'Daftar Gratis');
define('FIND_PROFESSIONAL_SERVICE', 'Carilah ahli untuk jasa berikut');
define('RIGHT_NOW', 'SEKARANG ANDA BISA MENCARI, MEMBANDINGKAN, DAN MENYEWA AHLI UNTUK TIPE JASA BERIKUT. KAMI SETIAP HARI BERKEMBANG DAN SEBENTAR LAGI ANDA AKAN BISA MENCARI AHLI UNTUK HAMPIR SEMUA JASA.');
define('CUSTOMER_GET_DONE', 'Ribuan konsumen menyelesaikan banyak hal lewat Seekmi');
define('GET_INTRODUCED', 'Get introduced to pros');
define('WHAT_SERVICE', 'Jasa apa yang anda perlukan?');
define('PAINTER_PLACEHOLDAER', 'Painter, DJ, Tutor');
define('CONTINUE_BUTTON', 'Lanjut');
define('FINISHED_BUTTON',  'Selesai');
define('AS_FEATURED', 'Diulas di');
define('HEADING_OUT', 'Dapatkan kemudahan bersama kami.');
define('RECIVE_QUOTES_WHEREVER', 'Kami membuat semuanya mudah bagi Anda. Kirimkan permintaan Anda dan kami akan segera mengirimkan notifikasi untuk mendapatkan mobile Apps Seekmi.');
define('PHONE_NUMBER_PLACEHOLDER', 'Nomor Telepon');
define('EMAIL_PLACEHOLDER','E-mail');
define('THOUSANDS_PROFESSINAL', 'Ribuan penyedia jasa mengembangkan bisnis mereka lewat Seekmi.');
define('CUSTOMER_STORIES','CERITA KONSUMEN');
define('PRO_STORIES', 'CERITA PARA PENYEDIA JASA');
define('ARE_YOU_PROFESSIONAL', 'Apakah Anda ingin mengembangkan usaha Anda?');
define('BUTTON_SIGN_UP_PRO', 'Daftar Sekarang');
define('MENU_PRIVACT__POLICY', 'Kebijakan Privasi');
define('MENU_TERMS', 'Syarat dan Ketentuan');
define('MENU_ABOUT', 'Tentang Kami');
define('MENU_WORK', 'Lowongan');
define('MENU_TEAM', 'Tim Kami');
define('MENU_NEWS', 'Berita');
define('MENU_HEAD_CUSTOMER', 'Pelanggan');
define('MENU_HOWITWORKS', 'Cara Kerja');
define('MENU_SAFETY', 'Keamanan');
define('MENU_DOWNLOAD_IPHONE_APP', 'Download iPhone App');
define('MENU_HEAD_PROS', 'Pros');
define('MENU_SIGNUP', 'Daftar');
define('MENU_HEAD_QUESTIONS_NEED_HELP', 'PERTANYAAN? PERLU BANTUAN?');
define('MENU_FAQ','FAQ');
define('MENU_EMAIL', 'E-mail kami');
define('MENU_CREDIT', 'Kredit (miCoins)');
define('MENU_BLOG', 'Magazine');
define('HOME_SEARCH_PLACEHOLDER', 'Jasa apa yang diperlukan?');
define('BUTTON_GET_STARTED', 'Daftarlah');
define('HOME_JOIN_US', 'Bergabunglah');
define('HOME_CUSTOMER_TESTIMONIAL_FIRST', 'Rishi adalah seorang social entrepreneur yang membantu masyarakat tidak beruntung memasang panel tenaga surya di rumah mereka secara gratis. Ketika pindah ke apartemen baru, Rishi mendapatkan bantuan dari pandai ahli untuk memperbaiki beberapa perlengkapan interior apartemen.');
define('HOME_CUSTOMER_TESTIMONIAL_SECOND', 'Ketika mendegar berita kehamilannya Jessica membutuhkan jasa untuk merenovasi salah satu ruangan di apartemen namun mengalami kesulitan dalam mencari kontraktor. Seekmi membantu Jessica menemukan apa yang dicari dengan mudah dan nyaman.');
define('HOME_REFER_EARN_TEXT','Mau mendapatkan miCoins dengan gratis?');
define('BUTTON_HOME_REFER_EARN_TEXT','Perkenalkan kami');

/* services/Search Page Constats */
define('SEARCH_WITHIN_TEXT', 'Dalam waktu yang cepat, kami akan memberikan daftar layanan terdekat dengan lokasi dan sesuai dengan yang Anda butuhkan. Anda dapat membandingkan penawaran, ulasan, dan profil layanan yang Anda butuhkan. Ketika Anda siap, pekerjakan layanan yang Anda butuhkan.');
define('SEARCH_HOWDOES', 'Bagaimana Seekmi bekerja?');
define('SEARCH_SEND_REQUEST_TEXT','Kirim Permintaan');
define('SEARCH_READ_TERMS_TEXT','Saya sudah membaca dan mengerti');
define('SEARCVH_CALL_TEXT','Hubungi kami di (021) 4000-0289 jika Anda ada pertanyaan');
define('SEARCH_ERROR_MSG', 'We\'re sorry, but the service you are looking for is not yet available. If you would like to suggest a service to be available on Seekmi.com, please email us at');
define('SEARCH_DID_MEAN','Did you mean these?');
define('SEARCH_SUGGEST_ALREADY_TEXT','This service is already suggested.');
define('SEARCH_SUGGEST_SUCCESS_TEXT','Thank you for suggesting this service.');
define('SEARCH_WITHIN_HOURS_TEXT','Dalam waktu beberapa jam, kami akan memperkenalkan Anda ke beberapa');
define('SEARCH_WITHIN_HOURS_TEXT2','dan Anda akan menerima beberapa penawaran dari mereka yang siap dan tertarik untuk menerima permintaan Anda.');
define('SERVICE_INTRODUCE_TEXT','Kami akan memperkenalkan Anda ke penyedia jasa untuk');
define('SERVICE_TELL_US_ABOUT_TEXT','Beritahukan kami tentang keperluan Anda.');

/** Header view ** */
define('HEADER_SIGNIN', 'Masuk');
define('HEADER_LOGOUT', 'Logout');
//define('HEADER_INACTIVE_NOTIFY_MSG', 'Mohon aktivasi email Anda. Jika anda sudah mengaktivasikan email Anda tetapi masih menerima pesan ini, silahkan hubungi');
define('HEADER_INACTIVE_NOTIFY_MSG', 'You have not yet activated your registered e-mail. Please do so by clicking "Activate my e-mail" in our welcome e-mail. To resend this e-mail, please <a class="resend-activate-link" href="javascript:;">click here</a>. If you are having troubles activating your e-mail, please contact Seekmi at');
define('PROS_INACTIVE_NOTIFY_MSG', 'You have not yet activated your registered e-mail. Please do so by clicking "Activate my e-mail" in our welcome e-mail. To resend this e-mail, please <a class="pros-resend-activate-link" href="javascript:;">click here</a>. If you are having troubles activating your e-mail, please contact Seekmi at');
define('HEADER_ACTIVATION_EMAIL_SUCCESS_MSG', 'Activation link sent to your email, please check your email.');
define('HEADER_ACTIVATION_EMAIL_FAILED_MSG', 'There are some problem with sending link to email, please try again later.');
define('HEADER_MENU_REQUESTS','Requests');
define('HEADER_MENU_QUOTES','Quotes');
define('HEADER_MENU_CREDIT','miCoin');
define('HEADER_MENU_CREDITS','miCoins');
define('HEADER_MENU_BUY_CREDIT','Buy miCoin');
define('HEADER_MENU_BUY_PROJECTS','Layanan');
define('HEADER_MENU_PROFILE','Profile');
define('HEADER_MENU_SETTINGS','Settings');
define('HEADER_MENU_DASHBOARD','Dashboard');

/* Login Page  constatnts */
define('LOGIN_WELCOME_BACK', 'Selamat datang kembali ke Seekmi!');
define('LOGIN_WELCOME_INSTRUCTION','Daftar sekarang untuk membuat dan memeriksa permintaan perkerjaan.');
define('LOGIN_USERNAME_ERROR', 'Your username or password was incorrect.');
define('LOGIN_SIGNUP_TEXT', 'If you don\'t have an account, please');
define('LOGIN_SIGNUP', 'Sign Up');
define('LOGIN_EMAIL', 'Email');
define('LOGIN_PASS', 'Password');
define('LOGIN_REM', 'Ingat saya');
define('BUTTON_LOGIN', 'Masuk');
define('LOGIN_FBCONNECT', 'Facebook Connect');
define('LOGIN_SIGNUP_WITH_GOOGLE', 'Masuk dengan Google ');
define('LOGIN_SIGN_UP_WITH_GOOGLE','Sign up with Google ');
define('LOGIN_FORGOTPASS', 'Lupa password Anda?');
define('LOGIN_DONTHAVEACC', ' Belum Punya Akun?');
define('LOGIN_OR_TEXT', 'atau');

/* Register page constatnts */
define('REGISTER_CREATE_TEXT', 'Daftar dan Get Things Done');
define('REGISTER_HOW_GET_THINGS_DONE','Bagaimana memudahkan kebutuhan saya melalui Seekmi?');
define('REGISTER_FNAME', 'Nama Depan');
define('REGISTER_LNAME', 'Nama Belakang');
define('REGISTER_EMAILADD', 'Alamat Email');
define('REGISTER_CREATEPASS', 'Masukan Kata Sandi Anda');
define('REGISTER_TERMSTEXT', 'Dengan mengklik Daftar, Anda telah membaca dan mengerti');
define('REGISTER_TERMS', 'Syarat dan Ketentuan');
define('REGISTER_PRIVACY', 'Ketentuan Privasi');
define('BUTTON_CREATEACCOUNT', 'Daftar');
define('REGISTER_SIGNUPFB', 'Masuk dengan Facebook');
define('REGISTER_SIGNUP_FB','Sign up with Facebook');
define('REGISTER_AS_PROFESSIONAL', 'Anda Penyedia Jasa?');
define('REGISTER_WALLMSG', 'Kami tidak akan memposting ke wall Anda tanpa ijin Anda.');

/* add facebook email page constants */
define('ADD_FB_EMAIL_TEXT','Anda akan diperlukan untuk memasukan alamat e-mail Anda untuk menggunakan Seekmi. <br/>Mohon masukan e-mail Anda sekarang.');

/* Reset Password Page Constatnts*/
define('RESET_TEXT','Reset password Anda');
define('RESET_EMAIL_TEXT','Apa alamat email Anda?');
define('BUTOON_RESETPASS','Reset Password Anda');
define('EMAIL_NOT_EXIST_ERROR','There is no account with this email.');
define('USER_INACTIVE_RESET_ERROR','Mohon aktivasi email Anda untuk permintaan reset password');
define('USER_SUSPEND_RESET_ERROR','Maaf, akun Anda kami tangguhkan. Untuk mengativasikan kembali akun Anda, silahkan hubungi kami di <a href="mailto:support@seekmi.com">support@seekmi.com</a>');
define('USER_BANNED_RESET_ERROR','Maaf, akun Anda kami blokir. Untuk meminta akun Anda dievaluasi kembali, silahkan hubungi kami di <a href="mailto:support@seekmi.com">support@seekmi.com</a>');


/*Dashboard Page Constatnts*/
define('DASH_PROJECT_TEXT','Layanan');
define('DASH_ADD_PROJECT','Start new project');
define('DASH_UPDATE_STATUS','Update status');
define('DASH_COMPLETE','Complete');
define('DASH_NO_PROFESSIONAL','Layanan Profesional Segera Hadir!');
//define('DASH_UNFORTUNATELY_TEXT','Unfortunately, no one was available, interested, and qualified to meet your event ');
define('DASH_UNFORTUNATELY_TEXT','Nikmati hari Anda sejenak, Seekmi akan mencari serta mengirimkan layanan profesional yang tepat untuk Anda!');
define('DASH_NEEDS_TEXT',' needs');
define('DASH_HIRING_TEXT','Congrats on hiring');
define('DASH_COMPLETED_TEXT','Congrats on completed');
define('DASH_CANCELLED_TEXT','has been cancelled');
define('DASH_AVAIL_TEXT',' are available.');
define('DASH_IS_AVAIL_TEXT',' is available.');
define('DASH_HEADING_TEXT',' Heading out? Bring Seekmi with you!');
define('DASH_WEHAVE_TEXT',' We\'ve made it even easier to accomplish your projects. Send requests and
                receive quotes wherever you are.');
define('BUTTON_TEXTMELINK','Text me a link');
define('VAL_REQFNAME','Please enter your first name');
define('VAL_REQLNAME','Please enter your last name');
define('VAL_MINLENGTH','Too short');
define('VAL_REQEMAIL','You must enter an email');
define('VAL_EMAIL','Please enter a valid email');
define('VAL_EMAIL_EXISTS','Sorry, that email is already taken');
define('VAL_REQPASS','You must enter a password');
define('VAL_PASSMIN_LENGTH','Too short (minimum of 5 characters)');
define('DASH_SEARCH_SERVICES','Search service');

/*User Settings Page Constatnts*/
define('SETTING_ACCOUNT_TITLE', 'Account');
define('BUTTON_ADD', 'Add');
define('BUTTON_EDIT', 'Edit');
define('BUTTON_DELETE_ACC', 'Delete account');
define('SETTING_ANNOUNCE_TEXT', 'Seekmi Announcements');
define('SETTING_UPDATE_TEXT', 'Updates');
define('SETTING_PROFILETIPS_TEXT', 'Profile tips');
define('SETTING_SPECOFFER_TEXT', 'Special offers');
define('SETTING_SURVEYS_TEXT', 'Surveys');
define('VAL_UPLOAD_ERROR', 'An error occurred and the upload failed.');

/* Edit Account Setting page Constatns*/
define('BUTTON_BACKTOSETTINGS','Back to Settings');
define('EDITACC_EDIT_TEXT','Edit Personal Information');
define('EDITACC_NAME_TEXT','Name');
define('EDITACC_TIMEZONE_TEXT','Time zone');
define('BUTTON_SAVECHANGES_TEXT','Save changes »');
define('BUTTON_CANCEL_TEXT','Cancel');
define('EDITACC_REFERRED_BY','Diperkenalkan oleh');
define('EDITACC_REFERRAL_INVALID_ERROR_TEXT','Pengguna ini belum terdaftar.');
define('EDITACC_REFERRAL_ALREADY_ERROR_TEXT','Anda sudah diperkenalkan oleh anggota ini.');
define('EDITACC_REFERRAL_OWN_ERROR_TEXT','You can not referred to own.');

/*Change Password page constants*/
define('CHANGEPASS_EDITPASS_TEXT','Edit Password');
define('CHANGEPASS_CURRPASS_TEXT','Current password');
define('CHANGEPASS_NEWPASS_TEXT','New password');
define('CHANGEPASS_CONFPASS_TEXT','Confirm new password');
define('BUTTON_SAVEPASS','Save Password');
define('VAL_CURREPASS','Your current password was not correct.');

/*Delete Account Page Constatnts*/
define('DEL_ACOOUNT_TEXT','Delete Your Account');
define('DEL_AREYOU_TEXT','Are you sure you want to delete your account?');
define('DEL_LOSECONTENT_TEXT','When you delete your account, you will lose all your project and profile
                    information and any reviews you\'ve written or received on Seekmi.');
define('DEL_RECIVEMAIL_TEXT','If you are receiving too many emails from us, then you can');
define('DEL_CHANGE_NOTIFICATION_TEXT','change your notification settings');
define('DEL_HELP_TEXT','We\'d hate to see you go! If there\'s anything we can do to help, email us at');
define('DEL_SUPPORT_EMAIL','support@seekmi.com');
define('DEL_CALL_TEXT','or call us at (415) 779-2191.');
define('DEL_REASON_TEXT',' Reason for deleting your account (optional)');
define('DEL_CONFIRM_TEXT',' I confirm I want to delete');
define('DEL_CONFIRM_REMAIN_TEXT',' \'s account');

/*Project detail page constatnts*/
define('DETAIL_QUOTE_TEXT','Quote from');
define('DETAIL_FOR_TEXT','for ');
define('DETAIL_INTRODUCTION_TEXT','Quote for');
define('DETAIL_CONTACTSUPPORT_TEXT','Contact support');
define('DETAIL_VIEWREQUEST_TEXT','Lihat  permintaan saya');
define('DETAIL_REVIEWS_TEXT','Reviews');
define('DETAIL_IDR_TEXT','IDR ');
define('DETAIL_VIEWPROFILE_TEXT','View Profile');
define('DETAIL_PHOTOS_TEXT','Photos');
define('DETAIL_WEBSITE_TEXT','Website');
define('DETAIL_TOTALPRICE_TEXT','total price');
define('DETAIL_HOURLYPRICE_TEXT','hourly price');
define('DETAIL_INFOPRICE_TEXT','Informasi lebih lanjut');
define('BUTTON_SEND','Send');
define('DETAIL_DECLINE_TEXT','You declined this quote');
define('DETAIL_COMPLETED_TEXT','Completed this quote by');
define('DETAIL_HIRE_TEXT','You hired this quote');
define('BUTTON_REPLY','Balas');
define('BUTTON_CALL','Call');
define('BUTTON_HIRE','Sewa');
define('BUTTON_DECLINE','Tolak');
define('DETAIL_LEAVE_REVIEW_TEXT','Tinggalkan pesan');
define('DETAIL_DECLINEREASON_TEXT','Please tell us why you declined this quote.');
define('DETAIL_RESPONSE_TEXT','Your responses here are just between you and Seekmi.');
define('DETAIL_REASON_TEXT1','I\'ve decided on someone else');
define('DETAIL_REASON_TEXT2','I think the price is too high');
define('DETAIL_REASON_TEXT3','The pro is too far away');
define('DETAIL_REASON_TEXT4','This is not what I need');
define('DETAIL_REASON_TEXT5','I don\'t need this work done anymore');
define('DETAIL_REASON_TEXT6','Other');
define('DETAIL_MSGSTEP_TEXT','You\'ll still be able to message the pro after this step.');
define('DETAIL_PROMSG_TEXT','Halo apakah Anda puas dengan layanan ini?');
define('DETAIL_SEEKMISUPPORT_TEXT','Seekmi Support');
define('DETAIL_HELP_TEXT','We\'re always here to help. Give us a call or send us an email');
define('BUTTON_SUBMIT','Submit');
define('BUTTON_SKIP','Skip');
define('BUTTON_UNDO','Undo');
define('BUTTON_NO','No');
define('BUTTON_YES','Yes');

/* Edit Profile page constants*/
define('EDITPRO_WELCOME_TEXT','Welcome to your profile!');
define('BUTTON_EDIT_PROFILE','Edit your profile');
define('BUTTON_DONE_EDIT','Done editing');
define('EDITPRO_EDITING_TEXT','You are now editing your profile.');
define('EDITPRO_UPLOADPIC_TEXT','Upload Picture.');
define('EDITPRO_BIO_LABEL','Profil.');
define('EDITPRO_TELLSTORY_TEXT','Ceritakan pengalaman Anda!');
define('EDITPRO_SERVICES_TEXT','Layanan');
define('EDITPRO_SHARE_TEXT','Share your social media and website');
define('EDITPRO_YEARSINBUSS_TEXT','years in business');
define('EDITPRO_EMPLOYEES_TEXT','employees');
define('EDITPRO_TRAVELS_TEXT','Travels up to');
define('EDITPRO_MILES_TEXT','kilometers');
define('EDITPRO_MILE_TEXT','kilometer');
define('EDITPRO_CREDENTIALS_TEXT','Credentials');
define('EDITPRO_NOCREDENTIALS_TEXT','No credentials yet on file');
define('EDITPRO_Q_AND_A_TEXT','Questions &amp; Answers');
define('EDITPRO_ANS_TEXT','Answer some frequently asked questions about your service.');
define('EDITPRO_COMPJOB_TEXT','There are no reviews for Horizon House.');
define('EDITPRO_COMPLETEJOB_TEXT','Complete jobs and get reviews.');
define('EDITPRO_EDITINFO_TEXT','Edit Your Basic Info');
define('EDITPRO_BUSSNAME_TEXT','nama usaha');
define('EDITPRO_BUSSLOGO_TEXT','Logo usaha Anda');
define('EDITPRO_OPTIONAL_TEXT','Optional');
define('EDITPRO_TAGLINE_TEXT','Tagline');
define('EDITPRO_ADD_CON_TEXT','Alamat dan Kontak');
define('EDITPRO_STREETADD_TEXT','Alamat Jalan');
define('EDITPRO_PROVINCE_TEXT','Provinsi');
define('EDITPRO_SELECT_TEXT','--Select--');
define('EDITPRO_CITY_TEXT','Kota');
define('EDITPRO_DIST_TEXT','Kelurahan');
define('EDITPRO_POSTAL_TEXT','Kode Pos');
define('EDITPRO_ADDRESONPRO_TEXT','Tampilkan alamat lengkap di profil saya');
define('EDITPRO_CITYONPRO_TEXT','Tampilkan kota/negara saja di profil saya ');
define('EDITPRO_PROFCOMP_TEXT','Profile Completion');
define('EDITPRO_MANAGEMEDIA_TEXT','Manage Your Media');
define('EDITPRO_ADDMEDIA_TEXT','Add media to your gallery');
define('EDITPRO_REFER_TEXT','Refer Friend & Family');
define('EDITPRO_ENTEREMAIL_TEXT','Enter email address');
define('EDITPRO_TIP_TEXT','Tip:');
define('EDITPRO_EXAMPLES_TEXT','Contoh:');
define('EDITPRO_QUEANS_TEXT','Questions and Answers');
define('EDITPRO_NOQUEANS_TEXT','questions answered');
define('EDITPRO_PICKEXAMPLE_TEXT','Pick 8-10 examples of your best work to demonstrate your capabilities');
define('EDITPRO_ANSEXTRA_TEXT','Silahkan Menjawab beberapa pertanyaan tambahan tentang layanan Anda');
define('EDITPRO_CLIENTSTREAT_TEXT','Menjawab pertanyaan dari pelanggan merupakan tahap awal komunikasi yang baik untuk Anda. Penyedia jasa yang menjawab 8-10 pertanyaan dan menampilkan keahliannya memiliki peluang besar untuk dipekerjakan.');
define('EDITPRO_TELLCUSTOMER_PLACEHOLDER','Tell customers about yourself and what you love about your work. Authentic and meaningful information about who you are is important to establishing a relationship and building trust with a new customer.');
define('EDITPRO_IBECAME_TEXT','   “I became a personal trainer to help people transform their
                            lives and I want to see you accomplish more than you can on your
                            own.  It is difficult to find the time and motivation, so I will
                            push you to accomplish your goals.”');
define('EDITPRO_IAM_TEXT',' “I am a San Francisco-based portrait and product
                            photographer.  My career began in HS where I took photos of
                            student groups for the yearbook. Since then, I\'ve been dedicated
                            to capturing the beauty and energy of people and their work.”');
define('EDITPRO_DESCRIBESERVICE_TEXT', 'Ceritakan tentang layanan Anda');
define('EDITPRO_MANAGEBUSSLINKS_TEXT', 'Manage Your Business Links');
define('EDITPRO_EDITBUSS_TEXT', 'Edit jenis usaha Anda');
define('EDITPRO_UPLOADLOGO_TEXT', 'Unggah logo Anda');
define('EDITPRO_YEARFOUND_TEXT', 'Berdiri sejak');
define('EDITPRO_NOOFEMP_TEXT', 'Jumlah karyawan');
define('EDITPRO_TRAVELPREF_TEXT', 'Layanan bisnis yang Anda lakukan (Pilih semuanya)');
define('EDITPRO_ITRAVEL_TEXT', 'Langsung menuju pelanggan');
define('EDITPRO_CUSTTRAVEL_TEXT', 'Pelanggan akan ke kantor saya');
define('EDITPRO_NEITHER_TEXT', 'Melalui telpon atau internet');
define('EDITPRO_HOWFAR_TEXT', 'How far are you willing to travel?');
define('EDITPRO_LISTOFSERVICE_PLACEHOLDER', 'List the kinds of services you provide and explain why future clients should hire you.');
define('EDITPRO_LISTOFSERVICE_HELP', '(100 characters minimum)');
define('EDITPRO_IFYOUWANT_TEXT', '  If you want fabulous, professional images that make you and yours
                        look your best &ndash; Jane\'s Photography is right for you.
                        Whether you need executive head-shots, personality photos,
                        model portfolio images, performance images, or family portraits,
                        we will work closely with you to find out exactly how you want
                        to be portrayed and produce the best possible results.
                        Plus, you\'ll have a good time doing it!
                        <br><br>
                        I am a photographer with a love for storytelling. My pricing is
                        reasonable and I can create custom packages to accommodate
                        your budget.
                        <br><br>
                        My clients include the Mayor of San Francisco, numerous members
                        of the city council, and many private clients including
                        the corporate photos for Cisco\'s corporate suite.');
define('BUTTON_EDITMEDIA','Edit media');
define('BUTTON_PICTURE','Picture');
define('BUTTON_SAVING','Saving');
define('BUTTON_DELETING','Deleting');
define('BUTTON_DELETE','Delete');
define('BUTTON_BACK','Back');
define('BUTTON_DELETELOGO','Delete logo');
define('BUTTON_VIDEO','Video');
define('BUTTON_MANAGEREVIEW','Manage Reviews');
define('BUTTON_SAVE_CONT','Simpan dan Lanjutkan');
define('EDITPRO_GETREVIEW_TEXT', 'Dapatkan ulasan');
define('EDITPRO_NOREVIEW_TEXT', 'Your service has 0 reviews.');
define('EDITPRO_MANAGEREVIEW_TEXT', 'Manage your reviews.');
define('EDITPRO_SERVICENOREVIEW_TEXT', 'Layanan Anda belum mendapatkan ulasan.');
define('EDITPRO_ASINGLEREVIEW_TEXT', 'Satu ulasan tentang layanan Anda akan berpeluang mendapatkan permintaan layanan dari Seekmi!');
define('EDITPRO_EMAILPAST_TEXT', 'Hubungi pelanggan Anda untuk mengunjungi URL berikut dan meminta ia mengulas layanan Anda');
define('EDITPRO_TAKEALOOK_TEXT', 'take a look');
define('EDITPRO_EMAILIT_TEXT', 'Email it');
define('EDITPRO_TWEETIT_TEXT', 'Tweet it');
define('EDITPRO_SHAREIT_TEXT', 'Share it');
define('EDITPRO_TOPASTCLIENT_TEXT', 'to your past clients');
define('EDITPRO_TOFOLLOWERS_TEXT', 'to your followers');
define('EDITPRO_ONFACEBOOK_TEXT', 'on Facebook');
define('MENU_CONTACTUS', 'Contact Us');
define('PROFILE_REFERRAL_ALREADY_ERROR_TEXT','Anda telah dirujuk ke pengguna ini.');
define('PROFILE_REFERRAL_SUCCESS_TEXT','Anda telah berhasil mengirim undangan Anda.');
define('EDITPRO_EARN_BASIC_INFO_TEXT', 'Lengkapi profil, dapatkan 10 MiCoins');
define('EDITPRO_EARN_MEDIA_TEXT', 'Tampilkan Profil foto, dan dapatkan 10 MiCoins');
define('EDITPRO_EARN_BIO_TEXT', 'Ceritakan profil dan dapatkan 10 MiCoins');
define('EDITPRO_EARN_SERVICES_TEXT', 'Lengkapi jenis bisnis Anda dapatkan 10 MiCoins');
define('EDITPRO_EARN_LINKS_TEXT', 'Bagi situs Anda dan mendapatkan 10 MiCoins');
define('EDITPRO_EARN_QA_TEXT', 'Lengkapi Q&A untuk 20 MiCoins!');
define('EDITPRO_EARN_REVIEWS_TEXT', '3 Ulasan dari pelanggan akan menambah 10 MiCoins');

/*Add Review Page Constants */
define('ADDREVIEW_LEAVEREVIEW_TEXT','Tinggalkan ulasan untuk');
define('ADDREVIEW_VIEWPROFILE_TEXT','Lihat Profil Penuh');
define('ADDREVIEW_THANKYOU_TEXT','Thank you for submitting review for');
define('ADDREVIEW_ALLREADYREVIEW_TEXT','You\'ve already reviewed this bid.');
define('ADDREVIEW_YOURRATING_TEXT','Penilaian Anda');
define('ADDREVIEW_CLICKTORATE_TEXT','click to rate');
define('ADDREVIEW_YOURRATE_TEXT','Pesan Anda');
define('ADDREVIEW_POSTREVIEW_TEXT','Masukan Ulasan');
define('ADDREVIEW_HI_TEXT','Hi');
define('ADDREVIEW_THISIS_TEXT',' This is what your review page looks like. You won\'t be able to leave a
                        review for yourself, but this is what people who come to this page will see
                        (minus this notice, of course).');
define('BUTTON_HIDENOTICE','hide this notice');
define('ADDREVIEW_THANKS_TEXT','Terima Kasih!');
define('ADDREVIEW_ITLOOKS_TEXT','Sepertinya Anda telah mengulas');
define('ADDREVIEW_THANKHELP_TEXT','Kami sangat menghargai pendapat Anda.');
define('ADDREVIEW_LETPEOPLE_TEXT','Bantulah orang lain dengan memberikan pendapat Anda!');
define('ADDREVIEW_DESCRIBE_TEXT','Deskripsikan permintaan pekerjaan Anda');
define('ADDREVIEW_WHATDID_TEXT','Apa yang');
define('ADDREVIEW_HELPYOU_TEXT','telah membantu Anda dengan? Dimanakah pekerjaan ini telah terjadi?<br>Berapa lamakah pekerjaan ini diselesaikan?');
define('ADDREVIEW_SAY_TEXT','Say what went well');
define('ADDREVIEW_WHATIMPRE_TEXT','What impressed you about the service?<br>Did ');
define('ADDREVIEW_GOABOVE_TEXT','go above and beyond?<br>Was ');
define('ADDREVIEW_ONTIME_TEXT','on time?<br>How was the quality of the work?');
define('ADDREVIEW_POINT_TEXT','Berikan masukan tentang apa yang bisa dilakukan lebih baik lagi.');
define('ADDREVIEW_DOBETTER_TEXT',' do better in the future?');
define('ADDREVIEW_WHATWOULD_TEXT','Apa yang terjadi yang kurang memuaskan?
                    What went wrong, if anything?
                    How could ');

/*Profile Request Page Constants*/
define('PROFILEREQ_CUSTREQ', ' Customer Requests');
define('PROFILEREQ_VIEWED', ' Viewed');
define('PROFILEREQ_NOTTHE_TEXT', ' Not the right type of requests?');
define('BUTTON_NEW', ' New');
define('BUTTON_ACCEPT', ' Terima');
define('BUTTON_HIRED', ' Hired');
define('BUTTON_ARCHIVED', 'Archived');
define('LABEL_PAGE', ' page');
define('LABEL_RECORDS', ' records');
define('LABEL_COMPLETED', 'Completed');
define('LABEL_DECLINED', 'Declined');
define('PROFILEREQ_INRECORDS', 'Sedang dalam pencarian');
define('REQUEST_FIRST_LOGGEDIN_STARTUP_TEXT', 'Selamat datang di layanan jasa profesional Seekmi! Untuk memulai, silahkan mengisi semua informasi yang diperlukan agar kami dapat mengirim permintan pekerjaan untuk Anda!<br/> Lengkapi profil Anda dan beritahukan teman-teman dan keluarga untuk mendapatkan 100 miCoins gratis!');
define('REQUEST_MORE_LOGGEDIN_STARTUP_TEXT', 'Selamat datang kembali ke Seekmi! Kami melihat profil Anda masih belum lengkap. Silahkan mengisi semua informasi yang diperlukan agar kami dapat mulai mengirimkan permintan pekerjaan untuk Anda!');
define('PROFILE_FIRST_LOGGEDIN_STARTUP_TEXT', 'Terima kasih telah bergabung bersama Seekmi.com.<br/>Kami mengajak untuk melengkapi profil bisnis Anda agar segera mendapatkan lebih banyak permintaan pekerjaan dari pelanggan.<br/>Lengkapi profil bisnis Anda sekarang juga, dapatkan (kredit) 100 miCoins secara gratis!<br/>Jangan sampai terlewatkan, ayo mulai sekarang juga!');
define('PROFILE_WELCOME','Selamat Datang');
define('ARE_YOU_SURE_DECLINE_TEXT','Apakah Anda yakin Anda ingin menolak permintaan pekerjaan ini?');
define('ARE_YOU_SURE_COMPLETE_TEXT','Apakah Anda yakin ingin menyelesaikan pekerjaan ini?');

/*Submit Quote Page Constatnts*/
define('SUBMITQUOTE_SUBMIT_TEXT','Tuliskan penawaran Anda');
define('SUBMITQUOTE_CHARTOO_TEXT','characters too long');
define('SUBMITQUOTE_TIPS_TEXT','Tips from top pros');
define('SUBMITQUOTE_SUBMITQUOTE_TEXT','quote have been submitted');
define('SUBMITQUOTE_WOULD_TEXT','would still like you to submit a quote.');
define('SUBMITQUOTE_NEED_TEXT','Need help quoting?');
define('SUBMITQUOTE_TIPSTO_TEXT','Tips to write winning messages');
define('SUBMITQUOTE_GREET_TEXT','Greet them by name.');
define('SUBMITQUOTE_TALK_TEXT','Talk about your relevant past work, qualifications, and expertise.');
define('SUBMITQUOTE_DESCRIBE_TEXT','Describe what\'s included in the price.');
define('SUBMITQUOTE_ENCOURAGE_TEXT','Encourage them to take the next step and follow up!');
define('SUBMITQUOTE_YOURMESSAGE_TEXT','Your message forms your customer\'s first impression of you and your business.
                        They love to hire pros who:');
define('LABEL_PRICE','Harga');
define('LABEL_FIXEDPRICE','Fixed price');
define('LABEL_HOURLYRATE','Tarif per jam');
define('LABEL_NEEDMORE','Need more info');
define('LABEL_MESSAGE','Pesan');
define('LABEL_REQUESTSUBMIT','Request submitted');
define('LABEL_NOTENOUGH','Not enough info?');
define('LABEL_SUGGEST','Suggest a question');
define('LABEL_CALLUS','Call us at ');
define('BUTTON_RETURNTOQUOTE','Return to Quote');
define('SUBMITQUOTE_CREDIT_ERROR_TEXT','You don\'t have enough miCoin to submit this quote. Please buy miCoins.');

/*Signup Pros Register Page Constants*/
define('SIGNUPREGI_HOW_TEXT', 'Kembangkan bisnis Anda bersama Seekmi!');
define('SIGNUPREGI_GETNEWCLIENTS_TEXT', 'Dapatkan klien baru dan kembangkan bisnis Anda');
define('SIGNUPREGI_TELLUS_TEXT', 'Berbagi dengan kami tentang layanan bisnis Anda dan kami akan mengirimkan permintaan pekerjaan dari pelanggan.');
define('SIGNUPREGI_BONUS', '*BONUS: Daftar sekarang dan dapatkan credit senilai Rp 1.000.000!');
define('SIGNUPREGI_PUTALL_TEXT', 'Masukan semua layanan yang Anda berikan dipisah dengan koma ');
define('SIGNUPREGI_NOTE_TEXT', '*Note: Untuk perusahaan, silahkan masukkan pihak yang berwenang dari perusahaan Anda untuk menerima permintaan pekerjaan dari pelanggan yang akan dikirimkan oleh Seekmi. ');
define('LABEL_REPEATPASS','Ulangi Password');
define('LABEL_NOTIFYME',' Beritahu saya melalui SMS apabila ada permintaan pekerjaan  dan pesan baru');
define('LABEL_BYCHECKINGTREMS','Saya setuju dengan syarat dan ketentuan yang ditetapkan dalam ');
define('LABEL_CONDOFSERVICES',' serta terikat dengan semua peraturan, kebijakan dan pedoman yang terkandung dalam semua persyaratan layanan, namun tidak terbatas pada kebijakan privasi PT. Seekmi Global Service yang terdapat pada ');
define('LABEL_PRIVACYPOLICY','(atau URL yang akan selalu di update oleh Seekmi dari waktu ke waktu). Jika Anda tidak setuju dengan perjanjian ini, maka Anda tidak dapat menggunakan layanan ini.');
define('LABEL_LIST','Daftar');
define('LABEL_RETURN','Kembali');
define('SIGNUPREGI_SMS_TEXT',' Layanan operator SMS dan Data Anda akan terdaftar ke pesan 
                            singkat kami. Anda tidak diharuskan untuk menyetujui untuk 
                            menerima pesan teks sebagai syarat menggunakan layanan Seekmi.');
define('SIGNUPREGI_CONTROL_TEXT', 'Kontrol dan saring semua permintaan yang Anda terima');
define('SIGNUPREGI_REGISTER_TEXT', 'Daftar dan gunakan layanan kami secara gratis');
//define('SIGNUP_REFERRAL_CODE_TEXT','Ingin bergabung dengan kami? Silahkan masukkan kode referral.');
define('SIGNUP_REFERRAL_CODE_TEXT','Apakah Anda diperkenalkan ke kami oleh Teman atau Keluarga? Silahkan masukkan kode referal atau alamat e-mail Teman atau Keluarga Anda.');
define('SIGNUP_REFERRAL_CODE_ERROR_TEXT','You have been entered wrong referral code or email');

/* add credit page constants */
define('CREDIT_PURCHASE_TEXT', 'Beli');

/* thank you page constants */
define('THANKYOU_BACK_TEXT', 'Kembali');
define('THANKYOU_WELCOME_TEXT', 'Selamat Datang di Seekmi!');
define('THANKYOU_WELCOME_TEXT2', 'Klien Anda menunggu.');
define('THANKYOU_CONGRATULATIONS_TEXT', 'Selamat, Anda telah menyelesaikan tahap pertama!<br/>Silahkan cek email untuk mengaktivasi akun Anda.');
define('THANKYOU_ACTIVATION_TEXT', 'Email aktivasi dikirim ke');
define('THANKYOU_SHARE_TEXT', 'Ceritakan kepada kerabat, Anda menemukan pelanggan melalui Seekmi!');
define('THANKYOU_NEXT_TEXT', 'Lanjut');

/* confirm account page constants */
define('CONFIRM_CONGRATS_TEXT', 'Selamat');
define('CONFIRM_LINK_EXPIRED_TEXT', 'Link kadaluarsa');
define('CONFIRM_ALREADY_ACTIVATED_TEXT', 'Akun Anda sudah aktif. Silahkan <a href="http://www.seekmi.com/user/login">login</a> sekarang');
define('CONFIRM_ACTIVATED_SUCCESS_TEXT', 'Account Anda berhasil diaktifkan');
define('CONFIRM_PROS_NEW_CLIENT_TEXT', 'Dapatkan klien-klien baru dan tumbuhkan bisnis kamu dengan cepat!');
define('CONFIRM_SHARE_TEXT', 'Share Seekmi ke keluarga dan kawan lewat');

define('HOURS', 'hari yang');
define('AGO', 'lalu');


/* home service section constants */
define('HOME_ARCHITECT_DESIGNER', 'Arsitek & Interior Designer');
define('HOME_CONTRACTOR', 'Kontraktor & tukang');
define('HOME_GRAPHIC_DESIGN', 'Desain Grafis');
define('HOME_MAID_CLEANING', 'Pembantu dan Pembersihan');
define('HOME_HAIR_MAKEUP', 'Rambut & Makeup Artis');
define('HOME_MOVER', 'Jasa Pindahan');
define('HOME_PERSONAL_TRAINING', 'Pelatih pribadi');
define('HOME_PEST_CONTROL', 'Pengendalian hama');
define('HOME_PHOTOGRAPHERS', 'fotografer');
define('HOME_VIDEOGRAPHERS', 'Videographers');
define('HOME_YOGA_INSTRUCTOER', 'yoga Instructor');
define('HOME_TUTORS_LESSONS', 'Tutor & Pelajaran');

/* End of file constants.php */
/* Location: ./application/config/ind_constants.php */