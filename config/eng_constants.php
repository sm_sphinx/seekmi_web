<?php 

define('GET_THINGS_DONE','Get things done');
define('BY_HIRING','by hiring experienced <br>
                local service pros<br>');
define('REGISTER_PART','Register now and win:');
define('SERVICE_PART','What services do you need?');
define('GET_FREE_QUOTE','Get a free quote');
define('YOUR_SERVICE_PROVIDER','Your service provider?');
define('GET_PART',' Get up to 3 quotes <br>
                  within hours.<br>
                  And choose the best<br/> service professional. <br>
                  It is easy, simple, and free to use.');
define('KODOM_PART','kaodim is now available in Klang Valley, Penang & <br>Johor Bahru');
define('KAMI_PART','We will bring you to a professional service provider who will give you the best deals to suit your needs');
define('SEEKMI_HELP','How Seekmi can help you?');
define('FIND_COMPARE','Find, compare and hire the best professional service providers');
define('WHAT_YOU_NEED','Tell us what you need');
define('ONE_MINUTE_TO_ANS','Only one minute to answer short questions about your needs.');
define('GET_A_QUOTE','Get a quote and compare');
define('RECIEVE_QUOTES','In a few hours you will receive quotes from professional service providers who are willing to be employed with the estimated price and contact information to be contacted.');
define('SELECT_SERVICE_PROVIDERS','Select and employ suitable professional services provider');
define('CHOOSE_IMPLEMENTATIONS','Choose from some of the implementing services that are suitable for rent. Executing the service will contact you to meet and satisfy your needs.');
define('ARE_YOU_SERVICE_PROVIDER','Are you a service provider?');
define('GET_NEW_CLIENT','Get new clients');
define('SEEKMI_FWD_CLIENT','Seekmi will forward the client request will need professional services provider in the region. You will decide whether you are interested or not to provide a quote for the client .');
define('REGISTER_FREE','Register Free');
define('FIND_PROFESSIONAL_SERVICE','Find professionals for these services');
define('CUSTOMER_GET_DONE','Thousand of customers got things done through Seekmi');
define('RIGHT_NOW','Right now, you can find, compare and hire professionals for these types of services. We are expanding
                                            every day and soon you will be able to find professionals for almost any anything.');
define('GET_INTRODUCED',  'Get introduced to pros');
define('WHAT_SERVICE',  'What service do you need?');
define('PAINTER_PLACEHOLDAER',  'Painter, DJ, Tutor');
define('CONTINUE_BUTTON',  'Continue');
define('FINISHED_BUTTON',  'Finished');
define('AS_FEATURED',  'As Featured in');
define('HEADING_OUT',  'Get Things Done on the Go!');
define('RECIVE_QUOTES_WHEREVER',  'We\'ve made it even easier for you to get things done. Send requests and receive quotes on the go.');
define('PHONE_NUMBER_PLACEHOLDER','Phone number');
define('EMAIL_PLACEHOLDER','Email');
define('CUSTOMER_STORIES','Customer Stories');
define('PRO_STORIES','Service Pro Stories');
define('THOUSANDS_PROFESSINAL','Thousands of professionals are growing their business through Seekmi.');
define('ARE_YOU_PROFESSIONAL','Are you a professional looking to grow your business?');
define('BUTTON_SIGN_UP_PRO','Sign up as a pro');
define('MENU_PRIVACT__POLICY','Privacy Policy');
define('MENU_TERMS','Terms of Use');
define('MENU_ABOUT','About');
define('MENU_WORK','Jobs');
define('MENU_TEAM', 'Team');
define('MENU_NEWS','News');
define('MENU_HEAD_CUSTOMER','Customers');
define('MENU_HOWITWORKS','How it Works');
define('MENU_SAFETY','Safety');
define('MENU_DOWNLOAD_IPHONE_APP','Download iPhone App');
define('MENU_HEAD_PROS','Pros');
define('MENU_SIGNUP','Sign Up');
define('MENU_HEAD_QUESTIONS_NEED_HELP','Questions? Need help?');
define('MENU_FAQ','FAQ');
define('MENU_EMAIL','E-mail us');
define('MENU_CREDIT', 'Credit (miCoins)');
define('MENU_BLOG', 'Magazine');
define('HOME_SEARCH_PLACEHOLDER','What services do you need?');
define('BUTTON_GET_STARTED','Get Started');
define('HOME_JOIN_US', 'Join Us');
define('HOME_CUSTOMER_TESTIMONIAL_FIRST', 'Rishi is a busy Social Entrepreneur, helping the unfortunate by installing solar panels on their homes for free. When he moved in to his new apartment, he got his handyman to fix some minor things.');
define('HOME_CUSTOMER_TESTIMONIAL_SECOND', 'When Jessica was expecting another baby, she knew she needed to renovate one of the room in the apartment but had difficulty in finding the right contractors. With Seekmi, she found what she was looking for easily and conveniently.');
define('HOME_REFER_EARN_TEXT','Want to earn free miCoins?');
define('BUTTON_HOME_REFER_EARN_TEXT','Refer us');

/*services/search Page constants*/
define('SEARCH_WITHIN_TEXT', 'Within hours, we will provide a list of the nearest service with location and according to that you need. You can compare offers, reviews, and profiles the services you need. When you are ready, hire the services you need.');
define('SEARCH_HOWDOES', 'How does Seekmi work?');
define('SEARCH_ERROR_MSG', 'We\'re sorry, but the service you are looking for is not yet available. If you would like to suggest a service to be available on Seekmi.com, please email us at');
define('SEARCH_DID_MEAN','Did you mean these?');
define('SEARCH_SEND_REQUEST_TEXT','Send My Request');
define('SEARCH_READ_TERMS_TEXT','I have read and agreed to the');
define('SEARCVH_CALL_TEXT','Call us at 021-4000-0289 if you have any questions or concerns');
define('SEARCH_SUGGEST_ALREADY_TEXT','This service is already suggested.');
define('SEARCH_SUGGEST_SUCCESS_TEXT','Thank you for suggesting this service.');
define('SEARCH_WITHIN_HOURS_TEXT','Within hours, you\'ll be introduced to interested and available');
define('SEARCH_WITHIN_HOURS_TEXT2','who will send you custom quotes based on your answers to the previous questions.');
define('SERVICE_INTRODUCE_TEXT','We will introduce you to service professionals for');
define('SERVICE_TELL_US_ABOUT_TEXT','Tell us about your needs');

/** Header view ***/
define('HEADER_SIGNIN', 'Sign in');
define('HEADER_LOGOUT', 'Logout');
//define('HEADER_INACTIVE_NOTIFY_MSG', 'Please activate your email. If you have activated your email but still receives this message, please contact us at');
define('HEADER_INACTIVE_NOTIFY_MSG', 'You have not yet activated your registered e-mail. Please do so by clicking "Activate my e-mail" in our welcome e-mail. To resend this e-mail, please <a class="resend-activate-link" href="javascript:;">click here</a>. If you are having troubles activating your e-mail, please contact Seekmi at');
define('PROS_INACTIVE_NOTIFY_MSG', 'You have not yet activated your registered e-mail. Please do so by clicking "Activate my e-mail" in our welcome e-mail. To resend this e-mail, please <a class="pros-resend-activate-link" href="javascript:;">click here</a>. If you are having troubles activating your e-mail, please contact Seekmi at');
define('HEADER_ACTIVATION_EMAIL_SUCCESS_MSG', 'Activation link sent to your email, please check your email.');
define('HEADER_ACTIVATION_EMAIL_FAILED_MSG', 'There are some problem with sending link to email, please try again later.');
define('HEADER_MENU_REQUESTS','Requests');
define('HEADER_MENU_QUOTES','Quotes');
define('HEADER_MENU_CREDIT','miCoin');
define('HEADER_MENU_CREDITS','miCoins');
define('HEADER_MENU_BUY_CREDIT','Buy miCoin');
define('HEADER_MENU_BUY_PROJECTS','Projects');
define('HEADER_MENU_PROFILE','Profile');
define('HEADER_MENU_SETTINGS','Settings');
define('HEADER_MENU_DASHBOARD','Dashboard');

/*Login Page  constatnts*/
define('LOGIN_WELCOME_BACK','Welcome back to Seekmi!');
define('LOGIN_WELCOME_INSTRUCTION','Log in now to create or check job requests.');
define('LOGIN_USERNAME_ERROR','Your username or password was incorrect.');
define('LOGIN_SIGNUP_TEXT','If you don\'t have an account, please');
define('LOGIN_SIGNUP','Sign Up');
define('LOGIN_EMAIL','Email');
define('LOGIN_PASS','Password');
define('LOGIN_REM','Remember me');
define('BUTTON_LOGIN','Log In');
define('LOGIN_FBCONNECT','Facebook Connect');
define('LOGIN_SIGNUP_WITH_GOOGLE','Login with Google ');
define('LOGIN_SIGN_UP_WITH_GOOGLE','Sign up with Google ');
define('LOGIN_FORGOTPASS','Forgot your password?');
define('LOGIN_DONTHAVEACC','Don\'t have an account?');
define('LOGIN_OR_TEXT', 'or');

/*Register page constatnts*/
define('REGISTER_CREATE_TEXT','Register and Get Things Done');
define('REGISTER_HOW_GET_THINGS_DONE','How do I get things done with Seekmi?');
define('REGISTER_FNAME','First name');
define('REGISTER_LNAME','Last name');
define('REGISTER_EMAILADD','Email address');
define('REGISTER_CREATEPASS','Create password');
define('REGISTER_TERMSTEXT','By clicking Create, you indicate that you have read and agree to the');
define('REGISTER_TERMS','Terms of Use');
define('REGISTER_PRIVACY','Privacy Policy');
define('BUTTON_CREATEACCOUNT','Create Account');
define('REGISTER_SIGNUPFB','Login with Facebook');
define('REGISTER_SIGNUP_FB','Sign up with Facebook');
define('REGISTER_AS_PROFESSIONAL', 'Are you a Professional?');
define('REGISTER_WALLMSG','We won\'t post on your wall without your permission.');

/* add facebook email page constants */
define('ADD_FB_EMAIL_TEXT','You will be required to enter your e-mail address in order for you to use Seekmi. <br/>Please enter your e-mail address now.');


/* Reset Password Page Constatnts*/
define('RESET_TEXT','Reset your password');
define('RESET_EMAIL_TEXT','What\'s your email address?');
define('BUTOON_RESETPASS','Reset Your Password');
define('EMAIL_NOT_EXIST_ERROR','There is no account with this email.');
define('USER_INACTIVE_RESET_ERROR','Please activate your email in order to request for password reset.');
define('USER_BANNED_RESET_ERROR','Sorry, your account has been banned. To have your account evaluated, please contact us at <a href="mailto:support@seekmi.com">support@seekmi.com</a>');
define('USER_SUSPEND_RESET_ERROR','Sorry, your account has been suspended. To reactivate your account, please contact us at <a href="mailto:support@seekmi.com">support@seekmi.com</a>');

/*Dashboard Page Constatnts*/
define('DASH_PROJECT_TEXT','Projects');
define('DASH_ADD_PROJECT','Start new project');
define('DASH_UPDATE_STATUS','Update status');
define('DASH_COMPLETE','Complete');
define('DASH_NO_PROFESSIONAL','Professionals responses are coming!');
//define('DASH_UNFORTUNATELY_TEXT','Unfortunately, no one was available, interested, and qualified to meet your event ');
define('DASH_UNFORTUNATELY_TEXT','Just sit back, relax, and let Seekmi search for and send you suitable professionals!');
define('DASH_NEEDS_TEXT',' needs');
define('DASH_HIRING_TEXT','Congrats on hiring');
define('DASH_COMPLETED_TEXT','Congrats on completed');
define('DASH_CANCELLED_TEXT','has been cancelled');
define('DASH_AVAIL_TEXT',' are available.');
define('DASH_IS_AVAIL_TEXT',' is available.');
define('DASH_HEADING_TEXT',' Heading out? Bring Seekmi with you!');
define('DASH_WEHAVE_TEXT',' We\'ve made it even easier to accomplish your projects. Send requests and
                receive quotes wherever you are.');
define('BUTTON_TEXTMELINK','Text me a link');
define('VAL_REQFNAME','Please enter your first name');
define('VAL_REQLNAME','Please enter your last name');
define('VAL_MINLENGTH','Too short');
define('VAL_REQEMAIL','You must enter an email');
define('VAL_EMAIL','Please enter a valid email');
define('VAL_EMAIL_EXISTS','Sorry, that email is already taken');
define('VAL_REQPASS','You must enter a password');
define('VAL_PASSMIN_LENGTH','Too short (minimum of 5 characters)');
define('DASH_SEARCH_SERVICES','Search service');

/*User Settings Page Constatnts*/
define('SETTING_ACCOUNT_TITLE', 'Account');
define('BUTTON_ADD', 'Add');
define('BUTTON_EDIT', 'Edit');
define('BUTTON_DELETE_ACC', 'Delete account');
define('SETTING_ANNOUNCE_TEXT', 'Seekmi Announcements');
define('SETTING_UPDATE_TEXT', 'Updates');
define('SETTING_PROFILETIPS_TEXT', 'Profile tips');
define('SETTING_SPECOFFER_TEXT', 'Special offers');
define('SETTING_SURVEYS_TEXT', 'Surveys');
define('VAL_UPLOAD_ERROR', 'An error occurred and the upload failed.');

/* Edit Account Setting page Constatns*/
define('BUTTON_BACKTOSETTINGS','Back to Settings');
define('EDITACC_EDIT_TEXT','Edit Personal Information');
define('EDITACC_NAME_TEXT','Name');
define('EDITACC_TIMEZONE_TEXT','Time zone');
define('BUTTON_SAVECHANGES_TEXT','Save changes »');
define('BUTTON_CANCEL_TEXT','Cancel');
define('EDITACC_REFERRED_BY','Referred By');
define('EDITACC_REFERRAL_INVALID_ERROR_TEXT','This user is not yet registered.');
define('EDITACC_REFERRAL_ALREADY_ERROR_TEXT','You have already been referred by this user.');
define('EDITACC_REFERRAL_OWN_ERROR_TEXT','You can not referred to own.');

/*Change Password page constants*/
define('CHANGEPASS_EDITPASS_TEXT','Edit Password');
define('CHANGEPASS_CURRPASS_TEXT','Current password');
define('CHANGEPASS_NEWPASS_TEXT','New password');
define('CHANGEPASS_CONFPASS_TEXT','Confirm new password');
define('BUTTON_SAVEPASS','Save Password');
define('VAL_CURREPASS','Your current password was not correct.');

/*Delete Account Page Constatnts*/
define('DEL_ACOOUNT_TEXT','Delete Your Account');
define('DEL_AREYOU_TEXT','Are you sure you want to delete your account?');
define('DEL_LOSECONTENT_TEXT','When you delete your account, you will lose all your project and profile
                    information and any reviews you\'ve written or received on Seekmi.');
define('DEL_RECIVEMAIL_TEXT','If you are receiving too many emails from us, then you can');
define('DEL_CHANGE_NOTIFICATION_TEXT','change your notification settings');
define('DEL_HELP_TEXT','We\'d hate to see you go! If there\'s anything we can do to help, email us at');
define('DEL_SUPPORT_EMAIL','support@seekmi.com');
define('DEL_CALL_TEXT','or call us at (415) 779-2191.');
define('DEL_REASON_TEXT',' Reason for deleting your account (optional)');
define('DEL_CONFIRM_TEXT',' I confirm I want to delete');
define('DEL_CONFIRM_REMAIN_TEXT',' \'s account');

/*Project detail page constatnts*/
define('DETAIL_QUOTE_TEXT','Quote from');
define('DETAIL_FOR_TEXT','for ');
define('DETAIL_INTRODUCTION_TEXT','Quote for');
define('DETAIL_CONTACTSUPPORT_TEXT','Contact support');
define('DETAIL_VIEWREQUEST_TEXT','View my request');
define('DETAIL_REVIEWS_TEXT','Reviews');
define('DETAIL_IDR_TEXT','IDR ');
define('DETAIL_VIEWPROFILE_TEXT','View Profile');
define('DETAIL_PHOTOS_TEXT','Photos');
define('DETAIL_WEBSITE_TEXT','Website');
define('DETAIL_TOTALPRICE_TEXT','total price');
define('DETAIL_HOURLYPRICE_TEXT','hourly price');
define('DETAIL_INFOPRICE_TEXT','Need more information');
define('BUTTON_SEND','Send');
define('DETAIL_DECLINE_TEXT','You declined this quote');
define('DETAIL_COMPLETED_TEXT','Completed this quote by');
define('DETAIL_HIRE_TEXT','You hired this quote');
define('BUTTON_REPLY','Reply');
define('BUTTON_CALL','Call');
define('BUTTON_HIRE','Hire');
define('BUTTON_DECLINE','Decline');
define('DETAIL_LEAVE_REVIEW_TEXT','Leave a review');
define('DETAIL_DECLINEREASON_TEXT','Please tell us why you declined this quote.');
define('DETAIL_RESPONSE_TEXT','Your responses here are just between you and Seekmi.');
define('DETAIL_REASON_TEXT1','I\'ve decided on someone else');
define('DETAIL_REASON_TEXT2','I think the price is too high');
define('DETAIL_REASON_TEXT3','The pro is too far away');
define('DETAIL_REASON_TEXT4','This is not what I need');
define('DETAIL_REASON_TEXT5','I don\'t need this work done anymore');
define('DETAIL_REASON_TEXT6','Other');
define('DETAIL_MSGSTEP_TEXT','You\'ll still be able to message the pro after this step.');
define('DETAIL_PROMSG_TEXT','Hi, is this pro a good fit for you?');
define('DETAIL_SEEKMISUPPORT_TEXT','Seekmi Support');
define('DETAIL_HELP_TEXT','We\'re always here to help. Give us a call or send us an email');
define('BUTTON_SUBMIT','Submit');
define('BUTTON_SKIP','Skip');
define('BUTTON_UNDO','Undo');
define('BUTTON_NO','No');
define('BUTTON_YES','Yes');

/* Edit Profile page constants*/
define('EDITPRO_WELCOME_TEXT','Welcome to your profile!');
define('BUTTON_EDIT_PROFILE','Edit your profile');
define('BUTTON_DONE_EDIT','Done editing');
define('EDITPRO_EDITING_TEXT','You are now editing your profile.');
define('EDITPRO_UPLOADPIC_TEXT','Upload Picture.');
define('EDITPRO_BIO_LABEL','Bio.');
define('EDITPRO_TELLSTORY_TEXT','Tell your story!');
define('EDITPRO_SERVICES_TEXT','Services');
define('EDITPRO_SHARE_TEXT','Share your social media and website');
define('EDITPRO_YEARSINBUSS_TEXT','years in business');
define('EDITPRO_EMPLOYEES_TEXT','employees');
define('EDITPRO_TRAVELS_TEXT','Travels up to');
define('EDITPRO_MILES_TEXT','kilometers');
define('EDITPRO_MILE_TEXT','kilometer');
define('EDITPRO_CREDENTIALS_TEXT','Credentials');
define('EDITPRO_NOCREDENTIALS_TEXT','No credentials yet on file');
define('EDITPRO_Q_AND_A_TEXT','Questions &amp; Answers');
define('EDITPRO_ANS_TEXT','Answer some frequently asked questions about your service.');
define('EDITPRO_COMPJOB_TEXT','There are no reviews for Horizon House.');
define('EDITPRO_COMPLETEJOB_TEXT','Complete jobs and get reviews.');
define('EDITPRO_EDITINFO_TEXT','Edit Your Basic Info');
define('EDITPRO_BUSSNAME_TEXT','Business name');
define('EDITPRO_BUSSLOGO_TEXT','Business logo');
define('EDITPRO_OPTIONAL_TEXT','Optional');
define('EDITPRO_TAGLINE_TEXT','Tagline');
define('EDITPRO_ADD_CON_TEXT','Address and Contact');
define('EDITPRO_STREETADD_TEXT','Street address');
define('EDITPRO_PROVINCE_TEXT','Province');
define('EDITPRO_SELECT_TEXT','--Select--');
define('EDITPRO_CITY_TEXT','City');
define('EDITPRO_DIST_TEXT','District');
define('EDITPRO_POSTAL_TEXT','Zip code');
define('EDITPRO_ADDRESONPRO_TEXT','Show my full address on my profile');
define('EDITPRO_CITYONPRO_TEXT','Show only the city/state on my profile');
define('EDITPRO_PROFCOMP_TEXT','Profile Completion');
define('EDITPRO_MANAGEMEDIA_TEXT','Manage Your Media');
define('EDITPRO_ADDMEDIA_TEXT','Add media to your gallery');
define('EDITPRO_REFER_TEXT','Refer Friend & Family');
define('EDITPRO_ENTEREMAIL_TEXT','Enter email address');
define('EDITPRO_TIP_TEXT','Tip:');
define('EDITPRO_EXAMPLES_TEXT','Examples:');
define('EDITPRO_QUEANS_TEXT','Questions and Answers');
define('EDITPRO_NOQUEANS_TEXT','questions answered');
define('EDITPRO_PICKEXAMPLE_TEXT','Pick 8-10 examples of your best work to demonstrate your capabilities');
define('EDITPRO_ANSEXTRA_TEXT','Answer some extra questions about your services');
define('EDITPRO_CLIENTSTREAT_TEXT','Clients treat your responses as a pre-interview. Professionals who answer 8-10 questions and demonstrate expertise are much more
                    likely to be hired.');
define('EDITPRO_TELLCUSTOMER_PLACEHOLDER','Tell customers about yourself and what you love about your work. Authentic and meaningful information about who you are is important to establishing a relationship and building trust with a new customer.');
define('EDITPRO_IBECAME_TEXT','   “I became a personal trainer to help people transform their
                            lives and I want to see you accomplish more than you can on your
                            own.  It is difficult to find the time and motivation, so I will
                            push you to accomplish your goals.”');
define('EDITPRO_IAM_TEXT',' “I am a San Francisco-based portrait and product
                            photographer.  My career began in HS where I took photos of
                            student groups for the yearbook. Since then, I\'ve been dedicated
                            to capturing the beauty and energy of people and their work.”');
define('EDITPRO_DESCRIBESERVICE_TEXT', 'Describe the Services You Provide');
define('EDITPRO_MANAGEBUSSLINKS_TEXT', 'Manage Your Business Links');
define('EDITPRO_EDITBUSS_TEXT', 'Edit Your Business Info');
define('EDITPRO_UPLOADLOGO_TEXT', 'Upload your logo');
define('EDITPRO_YEARFOUND_TEXT', 'Year founded');
define('EDITPRO_NOOFEMP_TEXT', 'Number of employees');
define('EDITPRO_TRAVELPREF_TEXT', 'Travel preferences (check all that apply)');
define('EDITPRO_ITRAVEL_TEXT', 'I travel to my customers');
define('EDITPRO_CUSTTRAVEL_TEXT', 'My customers travel to me');
define('EDITPRO_NEITHER_TEXT', 'Neither (phone or internet only)');
define('EDITPRO_HOWFAR_TEXT', 'How far are you willing to travel?');
define('EDITPRO_LISTOFSERVICE_PLACEHOLDER', 'List the kinds of services you provide and explain why future clients should hire you.');
define('EDITPRO_LISTOFSERVICE_HELP', '(100 characters minimum)');
define('EDITPRO_IFYOUWANT_TEXT', '  If you want fabulous, professional images that make you and yours
                        look your best &ndash; Jane\'s Photography is right for you.
                        Whether you need executive head-shots, personality photos,
                        model portfolio images, performance images, or family portraits,
                        we will work closely with you to find out exactly how you want
                        to be portrayed and produce the best possible results.
                        Plus, you\'ll have a good time doing it!
                        <br><br>
                        I am a photographer with a love for storytelling. My pricing is
                        reasonable and I can create custom packages to accommodate
                        your budget.
                        <br><br>
                        My clients include the Mayor of San Francisco, numerous members
                        of the city council, and many private clients including
                        the corporate photos for Cisco\'s corporate suite.');
define('BUTTON_EDITMEDIA','Edit media');
define('BUTTON_PICTURE','Picture');
define('BUTTON_SAVING','Saving');
define('BUTTON_DELETING','Deleting');
define('BUTTON_DELETE','Delete');
define('BUTTON_BACK','Back');
define('BUTTON_DELETELOGO','Delete logo');
define('BUTTON_VIDEO','Video');
define('BUTTON_MANAGEREVIEW','Manage Reviews');
define('BUTTON_SAVE_CONT','Save &amp; Continue');
define('EDITPRO_GETREVIEW_TEXT', 'Get Reviews');
define('EDITPRO_NOREVIEW_TEXT', 'Your service has 0 reviews.');
define('EDITPRO_MANAGEREVIEW_TEXT', 'Manage your reviews.');
define('EDITPRO_SERVICENOREVIEW_TEXT', 'Your service doesn\'t have any reviews yet');
define('EDITPRO_ASINGLEREVIEW_TEXT', 'A single review makes you twice as likely to be hired on Seekmi!');
define('EDITPRO_EMAILPAST_TEXT', 'Email past clients to visit the URL below and leave you a review');
define('EDITPRO_TAKEALOOK_TEXT', 'take a look');
define('EDITPRO_EMAILIT_TEXT', 'Email it');
define('EDITPRO_TWEETIT_TEXT', 'Tweet it');
define('EDITPRO_SHAREIT_TEXT', 'Share it');
define('EDITPRO_TOPASTCLIENT_TEXT', 'to your past clients');
define('EDITPRO_TOFOLLOWERS_TEXT', 'to your followers');
define('EDITPRO_ONFACEBOOK_TEXT', 'on Facebook');
define('MENU_CONTACTUS', 'Contact Us');
define('PROFILE_REFERRAL_ALREADY_ERROR_TEXT','You have already been referred to this user.');
define('PROFILE_REFERRAL_SUCCESS_TEXT','You have successfully sent your invitation.');
define('EDITPRO_EARN_BASIC_INFO_TEXT', 'Complete this information & earn 10 MiCoins');
define('EDITPRO_EARN_MEDIA_TEXT', 'Upload 2 Pictures and get 10 MiCoins');
define('EDITPRO_EARN_BIO_TEXT', 'Completed your bio and get 10 MiCoins');
define('EDITPRO_EARN_SERVICES_TEXT', 'Completed your Services & get 10 Micoins');
define('EDITPRO_EARN_LINKS_TEXT', 'Share your site and get 10 MiCoins');
define('EDITPRO_EARN_QA_TEXT', 'Complete Q&A get 20 MiCoins');
define('EDITPRO_EARN_REVIEWS_TEXT', 'Go get a 3 reviews and earn 10 MiCoins');

/*Add Review Page Constants */
define('ADDREVIEW_LEAVEREVIEW_TEXT','Leave a review for');
define('ADDREVIEW_VIEWPROFILE_TEXT','View full profile');
define('ADDREVIEW_THANKYOU_TEXT','Thank you for submitting review for');
define('ADDREVIEW_ALLREADYREVIEW_TEXT','You\'ve already reviewed this bid.');
define('ADDREVIEW_YOURRATING_TEXT','Your rating');
define('ADDREVIEW_CLICKTORATE_TEXT','click to rate');
define('ADDREVIEW_YOURRATE_TEXT','Your message');
define('ADDREVIEW_POSTREVIEW_TEXT','Post Review');
define('ADDREVIEW_HI_TEXT','Hi');
define('ADDREVIEW_THISIS_TEXT',' This is what your review page looks like. You won\'t be able to leave a
                        review for yourself, but this is what people who come to this page will see
                        (minus this notice, of course).');
define('BUTTON_HIDENOTICE','hide this notice');
define('ADDREVIEW_THANKS_TEXT','Thanks!');
define('ADDREVIEW_ITLOOKS_TEXT','It looks like you\'ve already reviewed');
define('ADDREVIEW_THANKHELP_TEXT','Thank you for helping to improve the Seekmi community.');
define('ADDREVIEW_LETPEOPLE_TEXT','Let people know how your experience went');
define('ADDREVIEW_DESCRIBE_TEXT','Describe the job');
define('ADDREVIEW_WHATDID_TEXT','What did');
define('ADDREVIEW_HELPYOU_TEXT','help you with? Where was the job?<br>How long did it take?');
define('ADDREVIEW_SAY_TEXT','Say what went well');
define('ADDREVIEW_WHATIMPRE_TEXT','What impressed you about the service?<br>Did ');
define('ADDREVIEW_GOABOVE_TEXT','go above and beyond?<br>Was ');
define('ADDREVIEW_ONTIME_TEXT','on time?<br>How was the quality of the work?');
define('ADDREVIEW_POINT_TEXT','Point out what could have been better');
define('ADDREVIEW_DOBETTER_TEXT',' do better in the future?');
define('ADDREVIEW_WHATWOULD_TEXT','What would have improved your experience?
                    What went wrong, if anything?
                    How could ');

/*Profile Request Page Constants*/
define('PROFILEREQ_CUSTREQ', ' Customer Requests');
define('PROFILEREQ_VIEWED', ' Viewed');
define('PROFILEREQ_NOTTHE_TEXT', ' Not the right type of requests?');
define('BUTTON_NEW', ' New');
define('BUTTON_ACCEPT', ' Accept');
define('BUTTON_HIRED', ' Hired');
define('BUTTON_ARCHIVED', 'Archived');
define('LABEL_PAGE', ' page');
define('LABEL_RECORDS', ' records');
define('LABEL_COMPLETED', 'Completed');
define('LABEL_DECLINED', 'Declined');
define('PROFILEREQ_INRECORDS', 'In Progress');
define('REQUEST_FIRST_LOGGEDIN_STARTUP_TEXT', 'Welcome to Seekmi Professional! To get started, please fill in all the required information so that we can start sending you job requests!<br/>Complete your profile and refer Seekmi to your friends and family to get up to 100 free miCoins!');
define('REQUEST_MORE_LOGGEDIN_STARTUP_TEXT', 'Welcome back to Seekmi Professional! We noticed that your profile is still incomplete. Please fill in all the required information so that we can start sending you job requests!');
define('PROFILE_FIRST_LOGGEDIN_STARTUP_TEXT', 'Thank you for logging into Seekmi.com.<br/>We invite you to complete your profile to get customer job requests immediately.<br/>By completing your profile today, you can earn up to 100 miCoins (credits) free!<br/>Let\'s Get Started!');
define('PROFILE_WELCOME','Welcome');
define('ARE_YOU_SURE_DECLINE_TEXT','Are you sure you want to reject the job request?');
define('ARE_YOU_SURE_COMPLETE_TEXT','Are you sure you want to complete this job?');

/*Submit Quote Page Constatnts*/
define('SUBMITQUOTE_SUBMIT_TEXT','Submit a quote');
define('SUBMITQUOTE_CHARTOO_TEXT','characters too long');
define('SUBMITQUOTE_TIPS_TEXT','Tips from top pros');
define('SUBMITQUOTE_SUBMITQUOTE_TEXT','quote have been submitted');
define('SUBMITQUOTE_WOULD_TEXT','would still like you to submit a quote.');
define('SUBMITQUOTE_NEED_TEXT','Need help quoting?');
define('SUBMITQUOTE_TIPSTO_TEXT','Tips to write winning messages');
define('SUBMITQUOTE_GREET_TEXT','Greet them by name.');
define('SUBMITQUOTE_TALK_TEXT','Talk about your relevant past work, qualifications, and expertise.');
define('SUBMITQUOTE_DESCRIBE_TEXT','Describe what\'s included in the price.');
define('SUBMITQUOTE_ENCOURAGE_TEXT','Encourage them to take the next step and follow up!');
define('SUBMITQUOTE_YOURMESSAGE_TEXT','Your message forms your customer\'s first impression of you and your business.
                        They love to hire pros who:');
define('LABEL_PRICE','Price');
define('LABEL_FIXEDPRICE','Fixed price');
define('LABEL_HOURLYRATE','Hourly rate');
define('LABEL_NEEDMORE','Need more info');
define('LABEL_MESSAGE','Message');
define('LABEL_REQUESTSUBMIT','Request submitted');
define('LABEL_NOTENOUGH','Not enough info?');
define('LABEL_SUGGEST','Suggest a question');
define('LABEL_CALLUS','Call us at ');
define('BUTTON_RETURNTOQUOTE','Return to Quote');
define('SUBMITQUOTE_CREDIT_ERROR_TEXT','You don\'t have enough miCoin to submit this quote. Please buy miCoins.');

/*Signup Pros Register Page Constants*/
define('SIGNUPREGI_HOW_TEXT', 'Grow your business with Seekmi!');
define('SIGNUPREGI_TELLUS_TEXT', 'Tell us about your business and we will send you customer jobs.');
define('SIGNUPREGI_BONUS', '*BONUS: Sign up now and earn up to Rp 1.000.000 of credits!');
define('SIGNUPREGI_PUTALL_TEXT', 'Put all of the services you provide separated by commas');
define('SIGNUPREGI_NOTE_TEXT', '* Note : For the company, please enter the authorities of your company to receive work requests from customers that will be delivered by Seekmi.');
define('LABEL_REPEATPASS','Repeat Password');
define('LABEL_NOTIFYME','Notify me via SMS when there are job requests and new messages');
define('LABEL_BYCHECKINGTREMS','By clicking Submit, you indicate that you understand and agree to the "Terms and Conditions" as set out in ');
define('LABEL_CONDOFSERVICES','("Conditions of Service") and agree to be bound by the Terms of Service and all regulations, policies and guidelines contained in the Terms of Service, including, but not limited to the Privacy Policy of PT. Seekmi Global Service as set out in ');
define('LABEL_PRIVACYPOLICY','( "Privacy Policy") or similar URLs provided by Seekmi. If you do not agree to this Agreement, then you cannot use the Service at all.');
define('LABEL_LIST','List');
define('LABEL_RETURN','Return');
define('SIGNUPREGI_SMS_TEXT','SMS and data service provider you will be registered to the message
                            Our brief . You are not required to agree to
                            receive text messages as a condition of using the service Seekmi .');
define('SIGNUPREGI_GETNEWCLIENTS_TEXT', 'Get new clients and expand your business');
define('SIGNUPREGI_CONTROL_TEXT', 'Control and filter all requests you receive');
define('SIGNUPREGI_REGISTER_TEXT', 'Register and use our services for free');
define('SIGNUP_REFERRAL_CODE_TEXT','Were you referred to us? Please enter referrer\'s referral code or e-mail address.');
define('SIGNUP_REFERRAL_CODE_ERROR_TEXT','You have been entered wrong referral code or email');

/* add credit page constants */
define('CREDIT_PURCHASE_TEXT', 'Purchase');

/* thank you page constants */
define('THANKYOU_BACK_TEXT', 'Back');
define('THANKYOU_WELCOME_TEXT', 'Welcome to Seekmi!');
define('THANKYOU_WELCOME_TEXT2', 'Your clients await you!');
define('THANKYOU_CONGRATULATIONS_TEXT', 'Congratulations, you have completed your first step!<br/>Please check your email to activate your account.');
define('THANKYOU_ACTIVATION_TEXT', 'Activation e-mail sent to');
define('THANKYOU_SHARE_TEXT', 'Share Seekmi to your family and friends</br>so to help them get more opportunities!');
define('THANKYOU_NEXT_TEXT', 'Next');


/* confirm account page constants */
define('CONFIRM_CONGRATS_TEXT', 'Congratulations');
define('CONFIRM_LINK_EXPIRED_TEXT', 'Link expired');
define('CONFIRM_ALREADY_ACTIVATED_TEXT', 'Your account is already active. Please <a href="http://www.seekmi.com/user/login">login</a> now.');
define('CONFIRM_ACTIVATED_SUCCESS_TEXT', 'Your account is successfully activated');
define('CONFIRM_PROS_NEW_CLIENT_TEXT', 'Get new clients and grow your business quickly!');
define('CONFIRM_SHARE_TEXT', 'Share Seekmi to family and friends via');

define('HOURS', 'hour');
define('AGO', 'ago');


/* home service section constants */
define('HOME_ARCHITECT_DESIGNER', 'Architect & Interior Designer');
define('HOME_CONTRACTOR', 'Contractors & Handyman');
define('HOME_GRAPHIC_DESIGN', 'Graphic Design');
define('HOME_MAID_CLEANING', 'Maids and Cleaning');
define('HOME_HAIR_MAKEUP', 'Hair & Makeup Artists');
define('HOME_MOVER', 'Movers');
define('HOME_PERSONAL_TRAINING', 'Personal Trainer');
define('HOME_PEST_CONTROL', 'Pest Control');
define('HOME_PHOTOGRAPHERS', 'Photographers');
define('HOME_VIDEOGRAPHERS', 'Videographers');
define('HOME_YOGA_INSTRUCTOER', 'Yoga Instructor');
define('HOME_TUTORS_LESSONS', 'Tutors & Lessons');

/* End of file constants.php */
/* Location: ./application/config/eng_constants.php */