$(document).ready(function () {   
    
    $('#language li a').click(function(){       
        var language = $(this).attr('lang');
             $.ajax({
                type: 'POST',
                url: Host+"home/changeLanguage",
                data:"language="+language,
                success: function (data) {
                       window.location.reload(); 
                    }
             });   
    });

    $('.universal-profile-container').on('click', function () {
         $('.profile-box, .arrow-tip-down, .arrow-tip').toggle();
    });
    
    $(document).mouseup(function (e) {
         var popup = $('.profile-box, .arrow-tip-down, .arrow-tip');
         if (!$('.universal-profile-container').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
             popup.hide();
         }    
    });
    
    $(".resend-activate-link").click(function(){
         $.ajax({
            type: 'POST',
            url: Host+"user/resend_activation_link",
            data:"",
            success: function (data) {
               if(data=='success'){
                   $('#resendActivationSuccessDiv').show();
                   $('#resendActivationFailedDiv').hide();
               }else{
                   $('#resendActivationFailedDiv').show();
                   $('#resendActivationSuccessDiv').hide();
               }
            }
         });
    });
    
    
      
});

