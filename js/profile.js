$(document).ready(function () {   
       
    $("#basicInfo").validate({
        rules: {
            first_name: {
                required: true,
                minlength: 2
            },
            last_name: {
                required: true,
                minlength: 2
            },
            address1: {
                required: true
            },
            province: {
                required: true
            },
            city: {
                required: true
            },
            district: {
                required: true
            },
            zip_code_id: {
                required: true,
                minlength: 5,
                maxlength: 5
            },
            'services[]': {                            
                minlength: 1
             },
        },
        messages:{
               'services[]': "Please add at least one service"
        },
        submitHandler: function(form) {       
        
            $.ajax({
                type:'post',
                url:Host+"profile/updateProfileData/basicInfo",
                data:$("#basicInfo").serialize(),
                success : function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj.msg=='success')
                    {
                        $('.total_prograss').html(obj.total_progress+"%");
                        $('.total_prograss_cls').css('width',obj.total_progress+"%");
                        getNvigatePage('cls_basic_popup','cls_media');
                    }
                }
            });                  
        }
    });  
    
    $("#bioForm").validate({
        rules: {             
        },
        submitHandler: function(form) {       
        
            $.ajax({
                type:'post',
                url:Host+"profile/updateProfileData/bioInfo",
                data:$("#bioForm").serialize(),
                success : function(data)
                {   
                    var obj = jQuery.parseJSON(data);
                    if(obj.msg=='success')
                    {
                        $('.total_prograss').html(obj.total_progress+"%");
                        $('.total_prograss_cls').css('width',obj.total_progress+"%");
                        getNvigatePage('cls_basic_popup','cls_services');
                    }                    
                }
            });                  
        }
    });
    
    $("#servicesForm").validate({
        rules: { 
            description: {
                required: true,
                minlength: 100
            }
        },
        submitHandler: function(form) {       
        
            $.ajax({
                type:'post',
                url:Host+"profile/updateProfileData/description",
                data:$("#servicesForm").serialize(),
                success : function(data)
                {   
                    var obj = jQuery.parseJSON(data);
                    if(obj.msg=='success')
                    {
                        $('.total_prograss').html(obj.total_progress+"%");
                        $('.total_prograss_cls').css('width',obj.total_progress+"%");
                        getNvigatePage('cls_basic_popup','cls_links');
                    }
                }
            });                  
        }
    });
    
    $("#linksForm").validate({
        rules: { 
            description: {
                required: true,
                minlength: 100
            }
        },
        submitHandler: function(form) {       
        
            $.ajax({
                type:'post',
                url:Host+"profile/updateProfileData/links",
                data:$("#linksForm").serialize(),
                success : function(data)
                {   
                    var obj = jQuery.parseJSON(data);
                    if(obj.msg=='success')
                    {
                        $('.total_prograss').html(obj.total_progress+"%");
                        $('.total_prograss_cls').css('width',obj.total_progress+"%");
                        getNvigatePage('cls_basic_popup','cls_business_info');
                    }
                }
            });                  
        }
    });
    
    $("#businessInfoForm").validate({
        rules: {
            company:{
               required : true
            },
            year_founded:{
                required : true,
                maxlength : 4,
                minlength : 4,
            },
            'preferences[]':{
                            required: true
                         }
        },
        messages:{ 
               year_founded:{
                 minlength: 'Please enter valid year',
                 maxlength: 'Please enter valid year'
               },
               'preferences[]': "You must select at least one travel type."
        },
        submitHandler: function(form) {       
        
            $.ajax({
                type:'post',
                url:Host+"profile/updateProfileData/businessInfo",
                data:$("#businessInfoForm").serialize(),
                success : function(data)
                {   
                    var obj = jQuery.parseJSON(data);
                    if(obj.msg=='success')
                    {
                        $('.total_prograss').html(obj.total_progress+"%");
                        $('.total_prograss_cls').css('width',obj.total_progress+"%");
                        getNvigatePage('cls_basic_popup','cls_refer');
                    }
                }
            });                  
        }
    });
    
    $("#qaForm").validate({       
        submitHandler: function(form) {       
        
            $.ajax({
                type:'post',
                url:Host+"profile/updateProfileData/qainfo",
                data:$("#qaForm").serialize(),
                success : function(data)
                {   
                    var obj = jQuery.parseJSON(data);
                    if(obj.msg=='success')
                    {
                        $('.total_prograss').html(obj.total_progress+"%");
                        $('.total_prograss_cls').css('width',obj.total_progress+"%");
                        $('#qa_ans_count').html(obj.answer_count);
                        getNvigatePage('cls_basic_popup','cls_reviews');
                    }
                }
            });                  
        }
    });
    
    $("#videoForm").validate({  
        rules: {
            video_url:{
                required : true                
            }
        },
        submitHandler: function(form) {
            $.ajax({
                type:'post',
                url:Host+"profile/uploadVideo",
                data:$("#videoForm").serialize(),
                success : function(data)
                { 
                    var obj = jQuery.parseJSON(data);
                    if(obj.msg=='success')
                    {
                        $('#mediaMainDiv').append(obj.video_str);
                        $('#mediaPictureDiv').show();
                        $('#mediaVideoDiv').hide();
                        $('#videoForm')[0].reset();
                        $('.total_prograss').html(obj.total_progress+"%");
                        $('.total_prograss_cls').css('width',obj.total_progress+"%");
                        $('#videoForm')[0].reset();
                    }
                    else
                    {
                        alert('We could not verify that the link you entered is valid.');
                        return false;
                    }
                }
            });                  
        }
    });
    
    $("#businessReferralForm").validate({
        rules: { 
            refer_email: {
                required: true,
                email: true
            }
        },
        submitHandler: function(form) {       
          
            $.ajax({
                type:'post',
                url:Host+"profile/updateProfileData/referral",
                data:$("#businessReferralForm").serialize(),
                success : function(data)
                {	
                    /*if($.trim(data)=='invalid'){                         
                        $('.refer-invalid-error').show();
                        $('.refer-already-error').hide();
                        return false;
                    }else*/
                    if($.trim(data)=='own_refer'){                         
                        $('.refer-own-error').show();
                        $('.refer-already-error').hide();
                        $('.refer-sent-success').hide();
                        return false;
                    }else if($.trim(data)=='already'){                        
                        $('.refer-already-error').show();
                        $('.refer-sent-success').hide();
                        $('.refer-own-error').hide();
                        return false;
                    }else if($.trim(data)=='sent'){
                        $('.refer-already-error').hide();
                        $('.refer-sent-success').show();
                        $('.refer-own-error').hide(); 
                        return false;
                    }else{
                        $('.refer-already-error').hide();
                        $('.refer-sent-success').hide();
                        $('.refer-own-error').hide();  
                        return false;
                    }
                }
            });
          
        }
    });
    
    
    
    $("#hide,#hide1").click(function(){
        $(".curtain").hide();
        location.href=Host+'profile/services/'; 
    });
    $("#show").click(function(){
        $(".curtain").show();
    });
    
      var btn = document.getElementById('uploadBtn'),
      progressBar = document.getElementById('progressBar'),
      progressOuter = document.getElementById('progressOuter'),
      msgBox = document.getElementById('msgBox');

        var uploader = new ss.SimpleUpload({
          button: btn,
          url: Host+'profile/uploadMediaPhoto',
          name: 'uploadfile',
          hoverClass: 'hover',
          focusClass: 'focus',
          responseType: 'json',
          startXHR: function() {
              progressOuter.style.display = 'block'; // make progress bar visible
              this.setProgressBar( progressBar );
          },
          onSubmit: function() {
              msgBox.innerHTML = ''; // empty the message box
              btn.innerHTML = 'Uploading...'; // change button text to "Uploading..."
            },
          onComplete: function( filename, response ) {
              //btn.innerHTML = 'Choose Another File';
              progressOuter.style.display = 'none'; // hide progress bar when upload is completed
              btn.innerHTML = 'Picture';
              if ( !response ) {
                  msgBox.innerHTML = 'Unable to upload file';
                  return;
              }

              if ( response.success === true ) {
                 // msgBox.innerHTML = '<strong>' + escapeTags( filename ) + '</strong>' + ' successfully uploaded.';                
                  $('#mediaMainDiv').append(response.append_str);
                  $('.total_prograss').html(response.total_progress+"%");
                  $('.total_prograss_cls').css('width',response.total_progress+"%");
              } else {
                  if ( response.msg )  {
                      msgBox.innerHTML = escapeTags( response.msg );

                  } else {
                      msgBox.innerHTML = 'An error occurred and the upload failed.';
                  }
              }
            },
          onError: function() {
              progressOuter.style.display = 'none';
              msgBox.innerHTML = 'Unable to upload file';
            }
          });
          
            var btn1 = document.getElementById('uploadProfileBtn'),
            msgBox1 = document.getElementById('msgBoxImage');

            var uploader = new ss.SimpleUpload({
              button: btn1,
              url: Host+'profile/upload_image',
              name: 'uploadfile',
              hoverClass: 'hover',
              focusClass: 'focus',
              responseType: 'json',
              startXHR: function() {
                 // progressOuter.style.display = 'block'; // make progress bar visible
                  //this.setProgressBar( progressBar );
              },
              onSubmit: function() {
                  msgBox1.innerHTML = ''; // empty the message box
                  btn1.innerHTML = 'Uploading...'; // change button text to "Uploading..."
                },
              onComplete: function( filename, response ) {

                  //btn.innerHTML = 'Choose Another File';
                  //progressOuter.style.display = 'none'; // hide progress bar when upload is completed

                  /*if ( response =='') {
                      msgBox.innerHTML = 'Unable to upload file';
                      return;
                  }*/

                  if (response.success == true ) {
                      msgBox1.style.display='none';
                      location.reload();
                      //msgBox.innerHTML = '<strong>' + escapeTags( filename ) + '</strong>' + ' successfully uploaded.';          
                  } else {
                      if ( response.msg )  {
                          msgBox1.style.display='';
                          msgBox1.innerHTML = escapeTags( response.msg );

                      } else {
                          msgBox1.style.display='';
                          msgBox1.innerHTML = 'An error occurred and the upload failed.';
                      }
                  }
                },
              onError: function() {

                  //progressOuter.style.display = 'none';
                  //msgBox.innerHTML = 'Unable to upload file';
                }
              });
              
                var btn2 = document.getElementById('uploadBusinessLogoBtn'),
                   msgBox2 = document.getElementById('msgBoxBusinessImage');

                new ss.SimpleUpload({
                  button: btn2,
                  url: Host+'profile/upload_business_image',
                  name: 'uploadfile',
                  hoverClass: 'hover',
                  focusClass: 'focus',
                  responseType: 'json',
                  startXHR: function() {
                     // progressOuter.style.display = 'block'; // make progress bar visible
                      //this.setProgressBar( progressBar );
                  },
                  onSubmit: function() {
                      msgBox2.innerHTML = ''; // empty the message box
                      btn2.innerHTML = 'Uploading...'; // change button text to "Uploading..."
                    },
                  onComplete: function( filename, response ) {

                      //btn.innerHTML = 'Choose Another File';
                      //progressOuter.style.display = 'none'; // hide progress bar when upload is completed

                      /*if ( response =='') {
                          msgBox.innerHTML = 'Unable to upload file';
                          return;
                      }*/

                      if (response.success == true ) {
                          msgBox2.style.display='none';
                          //location.reload();
                          var fileStr='<div class="media-list-item"><div class="media-picture-container media-container"><img alt="Business logo" src="'+Host+'business_logo/thumb/'+response.file+'"></div>';
                          fileStr+='<span onclick="deleteLogo()" class="media-delete">Delete Logo</span>';
                          fileStr+='<span class="media-delete" style="display: none;">Deleting...</span></div>';
                          $('#businessLogoDiv').html(fileStr);
                          btn2.innerHTML = 'Upload your logo';
                      } else {
                          if ( response.msg )  {
                              msgBox2.style.display='';
                              msgBox2.innerHTML = escapeTags( response.msg );

                          } else {
                              msgBox2.style.display='';
                              msgBox2.innerHTML = 'An error occurred and the upload failed.';
                          }
                      }
                    },
                   onError: function() {

                      //progressOuter.style.display = 'none';
                      //msgBox.innerHTML = 'Unable to upload file';
                    }
                  });
    
});

function escapeTags( str ) {
  return String( str )
           .replace( /&/g, '&amp;' )
           .replace( /"/g, '&quot;' )
           .replace( /'/g, '&#39;' )
           .replace( /</g, '&lt;' )
           .replace( />/g, '&gt;' );
}
	
function getNvigatePage(common_cls,target_cls){
    
    var parent_common_cls = "p_"+common_cls;
    var parent_target_cls = "p_"+target_cls;
    $('.'+parent_common_cls).removeClass('active');
    $('.'+parent_target_cls).addClass('active');
    //alert(target_cls)
    $('.'+common_cls).hide();
    $('.'+target_cls).show();
    $(".curtain").show();
}

function saveProfileMedia(){
        $.ajax({
            type:'post',
            url:Host+"profile/saveContinueProfileMedia",
            data:$("#qaForm").serialize(),
            success : function(data)
            {   
                var obj = jQuery.parseJSON(data);
                if(obj.msg=='success')
                {
                    $('.total_prograss').html(obj.total_progress+"%");
                    $('.total_prograss_cls').css('width',obj.total_progress+"%");                        
                    getNvigatePage('cls_basic_popup','cls_bio');
                }
            }
        });                  

}

function showMiles(obj){
    if(obj==true){
        $('#travel_tocustomerDiv').show();
    }else{
        $('#travel_tocustomerDiv').hide();
    }
}

function deleteMedia(id){
   if(id!=''){
       $.ajax({
            type:'post',
            url:Host+"profile/deleteMedia",
            data:"id="+id,
            success : function(data)
            {	
                if(data=='true')
                {
                    $('#mediaSubDiv'+id).remove();
                }
            }
        }); 
   }
}

function deleteLogo(){
  
    $.ajax({
         type:'post',
         url:Host+"profile/deleteBusinessLogo",
         data:"",
         success : function(data)
         {	
             if(data=='true')
             {
                 $('#businessLogoDiv').html('');
             }
         }
     }); 
   
}

function startOver(){
    $('#mediaPictureDiv').show();
    $('#mediaVideoDiv').hide();
}

function showVideo(){
    $('#mediaVideoDiv').show();
    $('#mediaPictureDiv').hide();
}

