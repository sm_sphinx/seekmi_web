<?php
require_once(dirname(__FILE__) . '/../../Veritrans.php');
Veritrans_Config::$serverKey = "VT-server-0ur4nciaYuNBwHfMLF3JS8af";
// Uncomment for production environment
 Veritrans_Config::$isProduction = true;

// Uncomment to enable sanitization
 Veritrans_Config::$isSanitized = false;

// Uncomment to enable 3D-Secure
 Veritrans_Config::$is3ds = true;

$params = array(
    "vtweb" => array (
          "enabled_payments" => array("credit_card","bank_transfer","telkomsel_cash","xl_tunai","bbm_money","indosat_dompetku")
        ),
    'transaction_details' => array(
      'order_id' => rand(),
      'gross_amount' => 20000,
    )
  );
try {
  // Redirect to Veritrans VTWeb page
  header('Location: ' . Veritrans_VtWeb::getRedirectionUrl($params));
}
catch (Exception $e) {
  echo $e->getMessage();
  if(strpos ($e->getMessage(), "Access denied due to unauthorized")){
      echo "<code>";
      echo "<h4>Please set real server key from sandbox</h4>";
      echo "In file: " . __FILE__;
      echo "<br>";
      echo "<br>";
      echo htmlspecialchars('Veritrans_Config::$serverKey = \'VT-server-0ur4nciaYuNBwHfMLF3JS8af\';');
      die();
}

}

?>
