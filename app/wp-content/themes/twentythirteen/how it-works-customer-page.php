<?php
/*
 Template Name: How It Works Customer Page
 */

get_header(); ?>

	<div id="primary" class="content-area primo-avenir">
		<div id="content" class="site-content text" role="main">
                <link rel="stylesheet" href="<?php echo WEB_ADDRESS; ?>/beta098/css/jquery-ui.css">
                <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/safty.css">
                <style type="text/css">
                  .box.box-cta .form-field label {
                        left: 0;
                        position: inherit;
                        top: 0;
                    }
                    
                 .form-field label.error {
                    clear: both;
                    color: #d51818;
                    display: block;
                    font-size: 11.2px;
                    margin: 5px 0 -2px 5px;
                 }
                 #main .form-field input.error {
                        border: 1px solid #d51818 !important;
                  }
                </style>
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
                <?php if ( has_post_thumbnail() && ! post_password_required() ) : 
                    $topImage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                    endif; 
                    $custom_fields = get_post_custom($post->ID);
                    $top_main_title_arr = $custom_fields['top_banner_main_title'];
                    if(!empty($top_main_title_arr)){
                        $top_main_title=$top_main_title_arr[0];
                    }else{
                        $top_main_title='';
                    }
                    $top_banner_sub_content_arr = $custom_fields['top_banner_sub_content'];
                    if(!empty($top_banner_sub_content_arr)){
                        $top_banner_sub_content=$top_banner_sub_content_arr[0];
                    }else{
                        $top_banner_sub_content='';
                    }
                      
                    ?>
                <div class="full-block team-hero1" style="background:url('<?php echo $topImage; ?>')  no-repeat scroll center top / cover;">
                                <div class="wrapper">
                                   <?php if($top_main_title!=''){ ?>
                                    <div class="column-14">
                                        <?php echo $top_main_title; ?>
                                    </div>
                                   <?php } if($top_banner_sub_content!=''){ ?>
                                    <div class="column-12 body-text">
                                       <?php echo $top_banner_sub_content; ?>
                                    </div>
                                   <?php } ?>
                                </div>
                            </div>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<?php //comments_template(); ?>
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
<script type="text/javascript" src="<?php echo WEB_ADDRESS; ?>/beta098/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo WEB_ADDRESS; ?>/beta098/js/jquery-validate.js"></script>
<script type="text/javascript">
            jQuery(function () {
                function split(val) {
                    return val.split(/,\s*/);
                }
                function extractLast(term) {
                    return split(term).pop();
                }
               

                jQuery("#services")
                    // don't navigate away from the field on tab when selecting an item
                    .bind("keydown", function (event) {
                        if (event.keyCode === jQuery.ui.keyCode.TAB &&
                                jQuery(this).autocomplete("instance").menu.active) {
                            event.preventDefault();
                        }
                    })
                    .autocomplete({
                        source: function (request, response) {
                            jQuery.getJSON("<?php echo WEB_ADDRESS; ?>/beta098/home/get_services", {
                                search: extractLast(request.term)
                            }, response);
                        },
                        search: function () {
                            // custom minLength
                            var term = extractLast(this.value);
                            if (term.length < 2) {
                                return false;
                            }
                        },
                        focus: function () {
                            // prevent value inserted on focus
                            return false;
                        },
                        select: function (event, ui) {
                            this.value = ui.item.value
                            jQuery("#services").removeClass("ui-autocomplete-loading");
                            return false;
                        }      
                    });
                        
                    jQuery("#serviceSearchForm").validate({
                        rules: {
                            services: {
                                required: true
                            }
                        },
                        submitHandler: function (form) {
                            location.href = encodeURI('<?php echo WEB_ADDRESS; ?>/beta098/services/search/' + jQuery('#services').val());
                        }
                    });
            }); 
        </script>
<?php get_footer(); ?>