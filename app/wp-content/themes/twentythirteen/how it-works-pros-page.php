<?php
/*
 Template Name: How It Works Pros Page
 */
session_start();
get_header(); ?>

	<div id="primary" class="content-area primo-avenir">
		<div id="content" class="site-content text" role="main">
                <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/safty.css">
                <?php 
                if (isset($_COOKIE['ci_session']))
                {
                   $cisession = $_COOKIE['ci_session'];
                   $cisession = unserialize(stripslashes($cisession));
                   if (is_array($cisession))
                   {
                       if($cisession['userId']!=''){ ?>
                           <style type="text/css">
                                .app-pros-btn{
                                    display:none;
                                }
                            </style>
                      <?php  }
                   }
                }
                ?>
                <?php /* The loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
                <?php if ( has_post_thumbnail() && ! post_password_required() ) : 
                    $topImage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                    endif; 
                    $custom_fields = get_post_custom($post->ID);
                    $top_main_title_arr = $custom_fields['top_banner_main_title'];
                    if(!empty($top_main_title_arr)){
                        $top_main_title=$top_main_title_arr[0];
                    }else{
                        $top_main_title='';
                    }
                    $top_banner_sub_content_arr = $custom_fields['top_banner_sub_content'];
                    if(!empty($top_banner_sub_content_arr)){
                        $top_banner_sub_content=$top_banner_sub_content_arr[0];
                    }else{
                        $top_banner_sub_content='';
                    }
                      
                    ?>
                <div class="full-block team-hero1" style="background:url('<?php echo $topImage; ?>')  no-repeat scroll center top / cover;">
                                <div class="wrapper">
                                   <?php if($top_main_title!=''){ ?>
                                    <div class="column-14">
                                        <?php echo $top_main_title; ?>
                                    </div>
                                   <?php } if($top_banner_sub_content!=''){ ?>
                                    <div class="column-12 body-text">
                                       <?php echo $top_banner_sub_content; ?>
                                    </div>
                                   <?php } ?>
                                </div>
                            </div>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<?php //comments_template(); ?>
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->        
<?php get_footer(); ?>