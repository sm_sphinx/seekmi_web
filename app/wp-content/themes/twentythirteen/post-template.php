<?php
/*
 Template Name: Post Page
 */

get_header(); ?>
<style type="text/css">
    .menu-top-menu-container{
        display:none;
    }
</style>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<?php 
                global $post;
                $pageslug = $post->post_name; // name of the page (slug) 
                //$query= 'category_name=' . $pageslug. '';
               // query_posts($query); the_post();  // run the query
                $query = new WP_Query( 'category_name='.$pageslug );
                if ( $query->have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_footer(); ?>
