<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<?php include("../config/configure.php"); ?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="icon" href="<?php echo WEB_ADDRESS; ?>/app/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo WEB_ADDRESS; ?>/app/favicon.ico" type="image/x-icon" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/icons.css">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
        
        <!--Start of Zopim Live Chat Script-->
        <script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?39X6oiJONnEbV2pq9IcXjxCXgUd07QJv";z.t=+new Date;$.
        type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
        </script>
        <!--End of Zopim Live Chat Script-->
	<?php wp_head(); ?>
        <style type="text/css">
            .header-navigation {
    display: inline;   
    margin: 0 5px;
    min-height: 1px;
    text-align: right;
    width: 275px;
}
 .header-navigation a.gray-link  {
    display: inline-block !important;
    margin-left: 10px !important;
    padding: 5px !important;
    vertical-align: middle !important;
}
.menu-top-menu-container{
    float: right;
    padding-top:8px;
    width: 68px;
}
.header-middle-container {
    display: inline;
    float: none;
    margin: 0 5px;
    min-height: 1px;
    padding-left: 30px;
    width: 440px;
}
.header-middle-container .middle-tab {
    box-sizing: border-box;
    color: #666 !important;   
    height: 64px;
    line-height: 71px;
    padding: 0 14px;
    position: relative;
    text-align: center;
    transition: color 0.3s ease 0s;
}
        </style>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4HXQT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T4HXQT');</script>
<!-- End Google Tag Manager -->

<div id="page" class="hfeed site">
            <div class="glorious-header glorious-header1" data-section="header">
            <div class="wrapper">
                <div class="row header-row" style="margin: 10px;">
                <div class="header-logo">
                    <a href="<?php echo WEB_ADDRESS; ?>/"><img src="<?php echo get_template_directory_uri(); ?>/images/headers/seekmilogo1.png" alt="Seekmi"></a>
                </div>
                    
                    <div class="header-middle-container">
                           <?php 
                        if (isset($_COOKIE['ci_session']))
                        {
                           $cisession = $_COOKIE['ci_session'];
                           $cisession = unserialize(stripslashes($cisession));
                           if (is_array($cisession))
                           {
                               if($cisession['userId']!=''){ 
                                   if ($cisession['user_type'] == 'professional') { ?>
                                    <a class="middle-tab" href="<?php echo WEB_ADDRESS; ?>/profile/requests"><?php echo HEADER_MENU_REQUESTS; ?></a>
                                    <a class="middle-tab" href="<?php echo WEB_ADDRESS; ?>/profile/work"><?php echo HEADER_MENU_QUOTES; ?></a>
                                    <a class="middle-tab" href="<?php echo site_url(); ?>/credits"><?php echo HEADER_MENU_CREDIT; ?></a>
                                    <a class="middle-tab" href="<?php echo WEB_ADDRESS; ?>/services/add_credit"><?php echo HEADER_MENU_BUY_CREDIT; ?></a>
                               <?php  }else{ ?>
                                   <a class="middle-tab" href="<?php echo WEB_ADDRESS; ?>/profile/dashboard"><?php echo HEADER_MENU_BUY_PROJECTS; ?></a> 
                        <?php } } } }  ?>
                    </div>
                    
                <div class="header-navigation" style="float: right;"> 
                    
                     <?php 
                        if (isset($_COOKIE['ci_session']))
                        {
                           $cisession = $_COOKIE['ci_session'];
                           $cisession = unserialize(stripslashes($cisession));
                           if (is_array($cisession))
                           {
                               if($cisession['userId']!=''){ 
                                   
                                                                
                           }else{ ?>
                             <a style="vertical-align:middle; margin-top:10px; margin-right:0px;"   href="<?php echo WEB_ADDRESS; ?>/user/register" rel="nofollow" class="gray-link log-in-link"><?php echo MENU_SIGNUP; ?></a>
                    <a style="vertical-align:middle; margin-top:10px; margin-right:10px;" href="<?php echo WEB_ADDRESS; ?>/user/login" rel="nofollow" class="gray-link log-in-link"><?php echo HEADER_SIGNIN; ?></a>            
                           <?php } }
                        }else{ ?>                                
                          <a style="vertical-align:middle; margin-top:10px; margin-right:0px;"href="<?php echo WEB_ADDRESS; ?>/user/register" rel="nofollow" class="gray-link log-in-link"><?php echo MENU_SIGNUP; ?></a>
                    <a style="vertical-align:middle; margin-top:10px; margin-right:10px;" href="<?php echo WEB_ADDRESS; ?>/user/login" rel="nofollow" class="gray-link log-in-link"><?php echo HEADER_SIGNIN; ?></a>      
                        <?php } ?>
                    
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); ?>
                </div>
                </div>                        
            </div>                    
           </div>
            <div id="main" class="site-main">
