<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<?php 
    if($_COOKIE['language'] === 'english'){ 
       $lang='en'; 
    }else{
       $lang='id';
    }
 ?>

		</div><!-- #main -->
		<div class="piede">
                    <div class="wrapper">
                        <div class="dynamic-row navigation">
                            <div class="column-3">
                                <div class="copyright">
                                    <p><a href="<?php echo WEB_ADDRESS; ?>/" class="logo">Seekmi</a></p>
                                    <p class="disclaimer">&copy; Copyright 2015 Seekmi.com</p>
                                    <a href="<?php echo site_url(); ?>/privacy/" ><?php echo MENU_PRIVACT__POLICY;?></a>
                                    <a href="<?php echo site_url(); ?>/terms-of-use/" class="last"><?php echo MENU_TERMS;?></a>
                                    <div class="social">
                                        <ul class="social-media">
                                            <li>
                                                <a href="http://www.facebook.com/seekmi" target="_blank">
                                                    <span class="facebook"></span>
                                                </a>
                                            </li>                           
                                            <li>
                                                <a href="https://twitter.com/SeekmiApp" target="_blank">
                                                    <span class="twitter"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="column-2">
                                <h4>Seekmi</h4>
                                <ul><li><a href="<?php echo site_url(); ?>/about/"><?php echo MENU_ABOUT;?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/jobs/"><?php echo MENU_WORK;?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/team/">Team</a></li>
                                    <li><a href="<?php echo site_url(); ?>/news/"><?php echo MENU_NEWS;?></a></li>
                                </ul>
                            </div>
                            <div class="column-2">
                                <h4><?php echo MENU_HEAD_CUSTOMER;?></h4>
                                <ul>
                                    <li><a href="<?php echo site_url(); ?>/how-it-works/"><?php echo MENU_HOWITWORKS;?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/safety/"><?php echo MENU_SAFETY;?></a></li>                    
                                </ul>
                            </div>
                            <div class="column-2">
                                <h4><?php echo MENU_HEAD_PROS;?></h4>
                                <ul>
                                    <li><a href="<?php echo site_url(); ?>/how-it-works-professional/"><?php echo MENU_HOWITWORKS;?></a></li>
                                    <li><a href="<?php echo WEB_ADDRESS; ?>/pros/register/"><?php echo MENU_SIGNUP;?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/credits/"><?php echo MENU_CREDIT;?></a></li>
                                    <li><a href="<?php echo WEB_ADDRESS; ?>/mag/"><?php echo MENU_BLOG;?></a></li>
                                </ul>
                            </div>
                            <div class="column-3">
                                <h4><?php echo MENU_HEAD_QUESTIONS_NEED_HELP;?></h4>
                                <ul>
                                    <li><a href="<?php echo site_url(); ?>/faq/"><?php echo MENU_FAQ;?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/contact-us/"><?php echo MENU_EMAIL;?></a></li>
                                    <li class="phone-number">Call (021) 4000-0289</li>
                                </ul>
                            </div>
                            
			    <?php /*<div class="column-3">
                            <div class="copyright">
                                <p><a href="<?php echo WEB_ADDRESS; ?>/" class="logo">Seekmi</a></p>
                                <p class="disclaimer">© Copyright 2015 Seekmi.com</p>
                                <a href="<?php echo WEB_ADDRESS; ?>/privacy/" >Privacy Policy</a>
                                <a href="<?php echo WEB_ADDRESS; ?>/terms/" class="last">Terms of Use</a>
                                <div class="social">
                                   <ul class="social-media">
                                    <li>
                                        <a href="http://www.facebook.com/seekmi" target="_blank">
                                            <span class="facebook"></span>
                                        </a>
                                    </li>                                   
                                    <li>
                                        <a href="https://twitter.com/SeekmiApp" target="_blank">
                                            <span class="twitter"></span>
                                        </a>
                                    </li>                                    
                                   </ul>
                                </div>
                            </div>
                           </div>
                           <div class="column-2">
                                <h4>Seekmi</h4>
                                <ul>               
                                    <li><a href="<?php echo site_url(); ?>/news/">News</a></li>
                                    <li><a href="<?php echo site_url(); ?>/jobs/">Pekerjaan</a></li>
                                    <li><a href="<?php echo site_url(); ?>/blog/">Blog</a></li>
                                </ul>
                           </div>
                           <div class="column-3" style="border:0px;">
                                <h4>Pertanyaan? Perlu bantuan?</h4>
                                <ul>
                                    <li><a href="<?php echo site_url(); ?>/faq/">FAQ</a></li>
                                    <li><a href="mailto:support@seekmi.com">E-mail kami</a></li>                                                        
                                </ul>
                            </div> */ ?>
                        </div>
                    </div>
                </div>
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>